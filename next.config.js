/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["localhost", "prosesindia.in"],
  },
  // // optimizeFonts: false,


  // basePath: '/Fincart/website',
  // // assetPrefix: '/Fincart/website',
  trailingSlash: true,
};
