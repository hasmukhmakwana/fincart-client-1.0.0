import { NextPage } from "next";
import React from "react";
import SignUpModule from "App/modules/SignUpModule/signup";
import Head from "next/head";

const SignUp: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Sign Up</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <SignUpModule />
  </>;
};

export default SignUp;
