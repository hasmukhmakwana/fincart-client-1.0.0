import React from 'react'
import CartModule from "App/modules/CartStatusModule/CartStatus"
import { NextPage } from 'next'
import Head from 'next/head'
import WithAuth from 'App/HOC/withAuth'

const CartStatus: NextPage = () => {
    return (
        <>
            <Head>
                <title>Fincart - Cart Status</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <CartModule />
        </>
    )
}

export default WithAuth(CartStatus)