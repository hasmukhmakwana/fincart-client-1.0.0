import type { NextPage } from "next";
import React from 'react'
import UpcomingTransactionsN from "App/modules/NotificationModule/UpcomingTransactions";
import Head from "next/head";
import WithAuth from "App/HOC/withAuth";

const UpcomingTransactions: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Upcoming Transactions</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <UpcomingTransactionsN />
    </>
  )
}

export default WithAuth(UpcomingTransactions)