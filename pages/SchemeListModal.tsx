import WithAuth from "App/HOC/withAuth";
import SchemeListModalModule from "App/modules/SchemeListModule/SchemeList";
import type { NextPage } from "next";
import Head from "next/head";

const SchemeListModal: NextPage = () => {
  return (<>
    <Head>
      <title>Fincart - Scheme List</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <SchemeListModalModule />
  </>
  )
}

export default WithAuth(SchemeListModal)