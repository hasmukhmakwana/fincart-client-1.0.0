import WithAuth from "App/HOC/withAuth";
import FinancialPlanP from "App/modules/FinancialPlanModule/FinancialPlan";

import type { NextPage } from "next";
import Head from "next/head";

const FinancialPlan: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Financial Plan</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <FinancialPlanP />
  </>;
};
export default WithAuth(FinancialPlan);
