import WithAuth from "App/HOC/withAuth";
import InvestModule from "App/modules/InvestModule/Invest";
import type { NextPage } from "next";
import Head from "next/head";

const Invest: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Invest</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <InvestModule />
    </>
  )
}

export default WithAuth(Invest)