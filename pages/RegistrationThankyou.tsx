import Module from "App/modules/RegistrationModule/components/ThankYou";
import type { NextPage } from "next";
import Head from "next/head";

const RegistrationThankyou: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Registration Status</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Module />
  </>;
};

export default RegistrationThankyou;
