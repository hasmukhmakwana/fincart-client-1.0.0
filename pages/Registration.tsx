import WithAuth from "App/HOC/withAuth";
import type { NextPage } from "next";
import Head from "next/head";
import RegistrationModule from "./../App/modules/RegistrationModule/Registration";

const Registration: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Registration</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <RegistrationModule />
  </>;
};

export default WithAuth(Registration);
