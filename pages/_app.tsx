import type { AppProps } from "next/app";
import "bootstrap/dist/css/bootstrap.css";
import "../styles/globals.scss";
import { UiProvider } from "App/contexts/ui/ui.provider";
import { AuthProvider } from "App/contexts/auth/auth.provider";

function MyApp({ Component, pageProps }: any) {
  return (
    <UiProvider>
      <AuthProvider>
        <Component {...pageProps} />
      </AuthProvider>
    </UiProvider>
  );
}

export default MyApp;
