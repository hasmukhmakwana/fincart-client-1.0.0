import WithAuth from "App/HOC/withAuth";
import SwitchTransactionModule from "App/modules/SwitchTransactionModule/SwitchTransaction";
import type { NextPage } from "next";

const SwitchTransaction: NextPage = () => {
  return (
    <SwitchTransactionModule/>
  )
}

export default WithAuth(SwitchTransaction)