import { NextPage } from 'next'
import React from 'react'
import RecommendedSchemesModule from 'App/modules/RecommendedSchemesModule/RecommendedSchemesModule';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';
const RecommendedSchemes: NextPage = () => {
    return <>
        <Head>
            <title>Fincart - Recommended Schemes</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <RecommendedSchemesModule />
    </>
}

export default WithAuth(RecommendedSchemes);