import WithAuth from "App/HOC/withAuth";
import GoalPlaningModule from "App/modules/GoalPlanningModule/GoalPlaning";
import type { NextPage } from "next";
import Head from "next/head";

const GoalPlanning: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Goal Planning</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <GoalPlaningModule />
    </>
  )
}

export default WithAuth(GoalPlanning)