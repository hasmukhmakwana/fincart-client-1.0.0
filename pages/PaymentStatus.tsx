import WithAuth from "App/HOC/withAuth";
import PaymentStatusPage from "App/modules/MobileModule/PaymentStatusPage";
import PortfolioModule from "App/modules/PortfolioModule/Portfolio";
import type { NextPage } from "next";
import Head from "next/head";

const PaymentStatus: NextPage = () => {
    return (
        <>
            <Head>
                <title>Fincart - Payment Status</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <PaymentStatusPage />
        </>
    )
}

// export default WithAuth(PaymentStatus)
export default PaymentStatus