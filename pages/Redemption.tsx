import WithAuth from "App/HOC/withAuth";
import RedemptionModule from "App/modules/RedemptionModule/Redemption";
import type { NextPage } from "next";

const Redemption: NextPage = () => {
  return (
    <RedemptionModule />
  )
}

export default WithAuth(Redemption)