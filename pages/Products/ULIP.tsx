import React from 'react'
import type { NextPage } from "next";
import ULIPModule from 'App/modules/ProductModule/ULIPModule/ULIP';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';
const ULIP: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - ULIP</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <ULIPModule />
    </>
  )
}

export default WithAuth(ULIP)
