import React from 'react'
import EPFModule from 'App/modules/ProductModule/EPFModule/EPF'
import type { NextPage } from "next";
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';

const EPF: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - EPF</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <EPFModule />
    </>
  )
}

export default WithAuth(EPF)