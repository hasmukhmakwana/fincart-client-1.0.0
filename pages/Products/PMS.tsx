import React from 'react'
import type { NextPage } from "next";
import PMSModule from 'App/modules/ProductModule/PMSModule/PMS';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';
const PMS: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - PMS</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <PMSModule />
    </>
  )
}

export default WithAuth(PMS)