import React from 'react'
import type { NextPage } from "next";
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';
import PMSPerformanceDetailsPage from 'App/modules/ProductModule/PMSModule/components/PMSPerformanceDetPage';
const PMSPerformancePage: NextPage = () => {
    return (
        <>
            <Head>
                <title>Fincart - PMS</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <PMSPerformanceDetailsPage />
        </>
    )
}

export default WithAuth(PMSPerformancePage)