import { NextPage } from 'next'
import React from 'react'
import LiquiloanModule from 'App/modules/ProductModule/LiquiloanModule/Liquiloan'
import Head from 'next/head'
import WithAuth from 'App/HOC/withAuth'

const Liquiloan: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - LiquiLoan</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <LiquiloanModule />
    </>
  )
}

export default WithAuth(Liquiloan)