import React from 'react'
import type { NextPage } from "next";
import ProtectionModule from 'App/modules/ProductModule/ProtectionModule/Protection';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';
const Protection: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Protection</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <ProtectionModule />
    </>
  )
}

export default WithAuth(Protection)
