import WithAuth from "App/HOC/withAuth";
import MutualFundModule from "App/modules/ProductModule/MutualFundModule/MutualFund";
import type { NextPage } from "next";
import Head from "next/head";

const MutualFund: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Mutual Funds</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <MutualFundModule />
    </>
  )
}
export default WithAuth(MutualFund);