import React from 'react'
import UploadCAS from 'App/modules/ProductModule/UploadCas/UploadCas'
import type { NextPage } from "next";
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';

const UploadCas: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Upload CAS</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <UploadCAS />
    </>
  )
}

export default WithAuth(UploadCas)