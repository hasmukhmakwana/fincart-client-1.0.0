import React from 'react'
import InsuranceModule from 'App/modules/ProductModule/InsuranceModule/Insurance'
import type { NextPage } from "next";
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';

const Insurance: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Insurance</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <InsuranceModule />
    </>
  )
}

export default WithAuth(Insurance)