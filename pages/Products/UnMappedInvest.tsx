import React from 'react'
import UnMappedInvests from 'App/modules/ProductModule/UnMappedInvest/UnMappedInvests'
import type { NextPage } from "next";
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';

const UnMappedInvest: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - UnMapped Investment</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <UnMappedInvests />
    </>
  )
}

export default WithAuth(UnMappedInvest)