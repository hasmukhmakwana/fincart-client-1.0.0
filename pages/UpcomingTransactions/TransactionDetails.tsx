import type { NextPage } from "next";
import React from 'react'
import TransactionDetail from "App/modules/NotificationModule/components/TransactionDetails";
import Head from "next/head";
import WithAuth from "App/HOC/withAuth";

const TransactionDetails = () => {
  return (
    <>
      <Head>
        <title>Fincart - Upcoming Transactions</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <TransactionDetail />
    </>
  )
}

export default WithAuth(TransactionDetails)
