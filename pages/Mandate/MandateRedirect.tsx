import Module from "App/modules/MandateModule/MandateRedirect/MandateRedirect";
import type { NextPage } from "next";
import Head from "next/head";

const MandateRedirect: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Mandate Status</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Module />
  </>;
};

export default MandateRedirect;
