import WithAuth from "App/HOC/withAuth";
import Module from "App/modules/MandateModule/MandateList/MandateList";
import type { NextPage } from "next";
import Head from "next/head";

const MandateList: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Mandates</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Module />
  </>;
};

export default WithAuth(MandateList);
