import WithAuth from "App/HOC/withAuth";
import Module from "App/modules/MandateModule/RegisterMandate/RegisterMandate";
import type { NextPage } from "next";
import Head from "next/head";

const RegisterMandate: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Register Mandate</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Module /></>;
};

export default WithAuth(RegisterMandate);
