import WithAuth from "App/HOC/withAuth";
import Module from "App/modules/SupportModule/FAQModule/FAQ";
import type { NextPage } from "next";
import Head from "next/head";

const FAQ: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - FAQ</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Module />
  </>;
};

export default WithAuth(FAQ);
