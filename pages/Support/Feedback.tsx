import { NextPage } from 'next'
import React from 'react'
import FeedbackModule from "App/modules/SupportModule/FeedbackModule/Feedback";
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';

const Feedback: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Feedback</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <FeedbackModule />
    </>
  )
}

export default WithAuth(Feedback)