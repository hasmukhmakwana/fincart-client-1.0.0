import WithAuth from "App/HOC/withAuth";
import RaiseTicketModule from "App/modules/SupportModule/RaiseTicket/RaiseTicket";
import type { NextPage } from "next";
import Head from "next/head";

const RaiseTicket: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Raise a Ticket</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <RaiseTicketModule />
  </>;
};

export default WithAuth(RaiseTicket);