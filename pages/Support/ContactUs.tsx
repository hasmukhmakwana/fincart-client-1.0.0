import WithAuth from "App/HOC/withAuth";
import ContactUsModule from "App/modules/SupportModule/ContactUs/ContactUs";
import type { NextPage } from "next";
import Head from "next/head";

const ContactUs: NextPage = () => {
    return <>
        <Head>
            <title>Fincart - Contact Us</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <ContactUsModule />
    </>;
};

export default WithAuth(ContactUs);