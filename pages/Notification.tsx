import { NextPage } from 'next'
import React from 'react'
import NotificationN from "App/modules/NotificationModule/Notification";
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';


const Notification: NextPage = () => {
    return (
        <>
            <Head>
                <title>Fincart - Notification</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <NotificationN />
        </>
    )
}

export default WithAuth(Notification)
