import { NextPage } from 'next'
import React from 'react'
import OverallReportModule from 'App/modules/OverallReportModule/OverallReport';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';
const OverallReport: NextPage = () => {
    return (
        <>
            <Head>
                <title>Fincart - Overall Report</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <OverallReportModule />
        </>
    )
}

export default WithAuth(OverallReport)