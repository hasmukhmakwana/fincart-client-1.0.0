import GoalPlanningStepsModule from "App/modules/GoalPlanningStepsModule/GoalPlanningSteps";
import type { NextPage } from "next";
import Head from "next/head";

const GoalPlanningSteps: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Goal Planning</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <GoalPlanningStepsModule />
    </>
  )
}

export default GoalPlanningSteps