import WithAuth from "App/HOC/withAuth";
import ChangePassModule from "App/modules/ChangePassModule/ChangePass";
import type { NextPage } from "next";
import Head from "next/head";

const ChangePass: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Change Password</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <ChangePassModule />
    </>
  )
}
export default WithAuth(ChangePass);