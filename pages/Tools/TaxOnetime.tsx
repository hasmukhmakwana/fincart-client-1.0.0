import { NextPage } from 'next'
import React from 'react'
import TaxSavingModule from 'App/modules/ToolsModule/TaxSavingOnetime/TaxOnetimePage';
import WithAuth from 'App/HOC/withAuth';
const TaxOnetime: NextPage = () => {
    return (
        <TaxSavingModule />
    )
}

export default WithAuth(TaxOnetime)