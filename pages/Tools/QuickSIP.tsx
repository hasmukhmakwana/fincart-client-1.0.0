import WithAuth from "App/HOC/withAuth";
import QuickSIPModule from "App/modules/ToolsModule/QuickSIPModule/QuickSIP";
import type { NextPage } from "next";
import Head from "next/head";

const QuickSIP: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Quick SIP</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <QuickSIPModule />
  </>
}
export default WithAuth(QuickSIP);