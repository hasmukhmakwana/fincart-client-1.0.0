import React from 'react'
import CalculatorBody from 'App/modules/ToolsModule/CalculatorModule/Calculator'
import Head from 'next/head'
import WithAuth from 'App/HOC/withAuth'
const Calculator = () => {
  return (
    <>
      <Head>
        <title>Fincart - Calculator</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <CalculatorBody />
    </>
  )
}

export default WithAuth(Calculator)
