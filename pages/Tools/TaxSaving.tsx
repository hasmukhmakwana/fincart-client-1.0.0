import { NextPage } from 'next'
import React from 'react'
import TaxSavingModule from 'App/modules/ToolsModule/TaxSavingModule/TaxSaving';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';
const TaxSaving: NextPage = () => {
    return <>
        <Head>
            <title>Fincart - Tax Saving</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <TaxSavingModule />
    </>
}

export default WithAuth(TaxSaving)