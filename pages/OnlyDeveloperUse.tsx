import { convertBase64 } from "App/utils/helpers";
import type { NextPage } from "next";
import React, { useEffect, useState } from "react";
import Webcam from "react-webcam";

// import { createFFmpeg, fetchFile } from "@ffmpeg/ffmpeg";

const PHOTO_HW = {
  height: 300,
  width: 400,
};

const OnlyDeveloperUse: NextPage = () => {
  const [recordedVideo, setrecordedVideo] = useState<any>(null);
  const webcamRef: any = React.useRef(null);
  const [capturing, setCapturing] = React.useState(false);
  const mediaRecorderRef: any = React.useRef(null);
  const [recordedChunks, setRecordedChunks] = React.useState([]);

  // const ffmpeg = createFFmpeg({
  //   log: true,
  // });

  //*functions

  const startRecording = React.useCallback(() => {
    setCapturing(true);
    
    mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
      mimeType: "video/webm;codecs=vp8,opus",
      // mimeType: "video/mp4",
    });

    // mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
    //   mimeType: "video/mp4; codecs=mpeg4",
    // });

    mediaRecorderRef.current.addEventListener(
      "dataavailable",
      handleDataAvailable
    );
    mediaRecorderRef.current.start();
  }, [webcamRef, setCapturing, mediaRecorderRef]);

  const handleDataAvailable = React.useCallback(
    ({ data }) => {
      if (data.size > 0) {
        setRecordedChunks((prev) => prev.concat(data));
      }
    },
    [setRecordedChunks]
  );

  const stopRecording = React.useCallback(() => {
    mediaRecorderRef.current.stop();
    setCapturing(false);
  }, [mediaRecorderRef, webcamRef, setCapturing]);

  //submit the page
  const clickOnSubmit = async () => {
    console.log(recordedChunks);

    // const blob = new Blob(recordedChunks, { type: 'video/mp4' });
    const blob = new Blob(recordedChunks);

    console.log(await convertBase64(blob));

    console.log(blob);

    // const file = new File(recordedChunks, "tesing.mp4", { type: "video/mp4" });

    // console.log(await convertBase64(file));

    // console.log(file);
  };
  useEffect(()=>{
    const types = [
      "video/webm",
      "audio/webm",
      "video/webm;codecs=vp8",
      "video/webm;codecs=daala",
      "video/webm;codecs=h264",
      "audio/webm;codecs=opus",
      "video/mpeg",
      "video/mkv",
    ];
    
    for (const type of types) {
      console.log(`Is ${type} supported? ${MediaRecorder.isTypeSupported(type) ? "Maybe!" : "Nope :("}`);
    }
  })

  return (
    <div className="container">
      <div className="text-center">
        <h2>You are in developer testing page</h2>
        <p>created by hasmukh</p>
      </div>

      <div className="m-5">
        <Webcam
          ref={webcamRef}
          audio={true}
          width={PHOTO_HW.width}
          height={PHOTO_HW.height}
          videoConstraints={{
            width: PHOTO_HW.width,
            height: PHOTO_HW.height,
            facingMode: "user",
          }}
        />
        <br />
        <br />
        {capturing ? (
          <button onClick={stopRecording}>Stop Recording</button>
        ) : (
          <button onClick={startRecording}>Start Recording</button>
        )}
        <br />
        <br />
        <button onClick={clickOnSubmit}>submit</button>
      </div>
    </div>
  );
};

export default OnlyDeveloperUse;
