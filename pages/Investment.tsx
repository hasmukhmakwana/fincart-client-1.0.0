import WithAuth from "App/HOC/withAuth";
import InvestmentModule from "App/modules/InvestmentModule/Investment";
import type { NextPage } from "next";
import Head from "next/head";

const Investment: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Investment</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <InvestmentModule />
    </>
  )
}
export default WithAuth(Investment);


