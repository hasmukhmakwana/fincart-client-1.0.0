import { NextPage } from 'next'
import React from 'react'
import PassbookPageModule from 'App/modules/PassbookModule/Passbook';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';
const Passbook: NextPage = () => {
    return (
        <>
            <Head>
                <title>Fincart - View Transactions</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <PassbookPageModule />
        </>
    )
}

export default WithAuth(Passbook)