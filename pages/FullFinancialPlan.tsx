import WithAuth from "App/HOC/withAuth";
import FullFinancialPlann from "App/modules/FullFinancialPlanModule/FullFinancialPlan";

import type { NextPage } from "next";
import Head from "next/head";

const FullFinancialPlan: NextPage = () => {
    return <>
        <Head>
            <title>Fincart - View Financial Plan</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <FullFinancialPlann />
    </>;
};
export default WithAuth(FullFinancialPlan);