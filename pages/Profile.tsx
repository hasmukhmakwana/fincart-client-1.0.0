import WithAuth from "App/HOC/withAuth";
import ProfileModule from "App/modules/ProfileModule/Profile";
import type { NextPage } from "next";
import Head from "next/head";

const Profile: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Profile</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <ProfileModule />
    </>
  )
}

export default WithAuth(Profile)