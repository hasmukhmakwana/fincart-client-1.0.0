import DashboardModule from "App/modules/DashboardModule/Dashboard";
import type { NextPage } from "next";
import WithAuth from "App/HOC/withAuth";
import Head from "next/head";

const Dashboard: NextPage = () => {
    return <>
        <Head>
            <title>Fincart - Dashboard</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <DashboardModule />
    </>;
};

export default WithAuth(Dashboard);

