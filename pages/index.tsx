import { NextPage } from "next";
import React from "react";
import LoginModule from "App/modules/LoginModule/login";
import Head from "next/head";

const Login: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Login</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <LoginModule />
  </>;
};

export default Login;
