import { NextPage } from 'next';
import React from 'react';
import GoalPortfolioSingleModule from 'App/modules/GoalPortfolioSingleModule/GoalPortfolioSingleModule';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';

const GoalPortfolio: NextPage = () => {
    return <>
        <Head>
            <title>Fincart - Goal Portfolio</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <GoalPortfolioSingleModule />
    </>
}

export default WithAuth(GoalPortfolio);