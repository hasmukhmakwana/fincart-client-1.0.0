import CheckoutModule from "App/modules/CheckoutModule/Checkout";
import type { NextPage } from "next";
import Head from "next/head";

const Checkout: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Checkout</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <CheckoutModule />
    </>
  )
}

export default Checkout