import WithAuth from "App/HOC/withAuth";
import PortfolioModule from "App/modules/PortfolioModule/Portfolio";
import type { NextPage } from "next";
import Head from "next/head";

const Portfolio: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Portfolio</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <PortfolioModule />
    </>
  )
}

export default Portfolio