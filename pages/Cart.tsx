import React from 'react'
import CartModule from "App/modules/CartModule/Cart"
import { NextPage } from 'next'
import Head from 'next/head'
import WithAuth from 'App/HOC/withAuth'

const Cart: NextPage = () => {
    return (
        <>
            <Head>
                <title>Fincart - Cart</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <CartModule />
        </>

    )
}

export default WithAuth(Cart)