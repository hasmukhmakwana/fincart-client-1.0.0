import WithAuth from "App/HOC/withAuth";
import MemberModule from "App/modules/MembersModule/Member";
import type { NextPage } from "next";

const Member: NextPage = () => {
  return (
    <MemberModule/>
  )
}

export default WithAuth(Member)