import WithAuth from "App/HOC/withAuth";
import Module from "App/modules/GoalsModule/Goals";
import type { NextPage } from "next";
import Head from "next/head";

const MyGoal: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Goal Planning</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Module />
  </>
};

export default WithAuth(MyGoal);
