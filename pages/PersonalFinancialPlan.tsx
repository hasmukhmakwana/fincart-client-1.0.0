import WithAuth from "App/HOC/withAuth";
import PersoalFinancialPlanP from "App/modules/PersonalFinancialPlanModule/PersonalFinancialPlan";

import type { NextPage } from "next";
import Head from "next/head";

const PersonalFinancialPlan: NextPage = () => {
    return <>
        <Head>
            <title>Fincart - Personal Financial Plan</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <PersoalFinancialPlanP />
    </>;
};
export default WithAuth(PersonalFinancialPlan);