import GoalPlanningInitiateGoalModule from "App/modules/GoalPlanningInitiateGoalModule/GoalPlanningInitiateGoal";
import type { NextPage } from "next";
import Head from "next/head";

const GoalPlanningInitiateGoal: NextPage = () => {
  return (
    <>
      <Head>
        <title>Fincart - Initiate Goal Plan</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <GoalPlanningInitiateGoalModule />
    </>
  )
}

export default GoalPlanningInitiateGoal