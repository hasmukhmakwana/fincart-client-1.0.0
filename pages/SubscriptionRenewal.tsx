import SubscriptionRenewalModule from "App/modules/SubscriptionRenewalModule/SubscriptionRenewal";
import type { NextPage } from "next";

const SubscriptionRenewal: NextPage = () => {
  return (
   <SubscriptionRenewalModule/>
  )
}

export default SubscriptionRenewal