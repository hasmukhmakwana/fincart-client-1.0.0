import WithAuth from "App/HOC/withAuth";
import SystematicTransferPlanModule from "App/modules/SystematicTransferPlanModule/SystematicTransferPlan";
import type { NextPage } from "next";

const SystematicTransferPlan: NextPage = () => {
  return (
    <SystematicTransferPlanModule/>
  )
}

export default WithAuth(SystematicTransferPlan)