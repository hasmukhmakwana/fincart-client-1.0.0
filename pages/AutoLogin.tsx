import React from 'react'
import { NextPage } from 'next'
import Head from 'next/head'
import AutoLogin from 'App/modules/LoginModule/components/AutoLogin'

const AutoLoginPage: NextPage = () => {
    return (
        <>
            <Head>
                <title>Fincart - Auto Login</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <AutoLogin />
        </>

    )
}

export default AutoLoginPage