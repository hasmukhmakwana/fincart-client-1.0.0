import WithAuth from "App/HOC/withAuth";
import SchemeListModule from "App/modules/SchemeListModule/SchemeList";
import type { NextPage } from "next";
import Head from "next/head";

const SchemeList: NextPage = () => {
  return <>
    <Head>
      <title>Fincart - Search Scheme</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <SchemeListModule />
  </>;
};

export default WithAuth(SchemeList);
