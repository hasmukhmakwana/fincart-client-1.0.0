import { NextPage } from 'next';
import React from 'react';
import GoalsInvestmentsModule from 'App/modules/GoalsInvestmentsModule/GoalsInvestmentsModule';
import Head from 'next/head';
import WithAuth from 'App/HOC/withAuth';

const GoalsInvestment: NextPage = () => {
    return <>
        <Head>
            <title>Fincart - Goal Investments</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <GoalsInvestmentsModule />
    </>
}

export default WithAuth(GoalsInvestment);