import React from 'react';
import WithIcons from "./withProps";

function Bussiness(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAzCAMAAADvo9thAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACdlBMVEUAAAAAW7EMY7gXa70V
abwcb8AAWrMKY7gTabsecL8Ybb4AVcYAW7MRaboRaLsZbr4AWrQOZrkod8QRarsQaLoccL8se8YA
W7IAWrQQZ7oTabs2gskRaLsAXrMGYLYabr4AXLIJY7gbb74AVbsIYLYUarsMZbhXmdkJYrcAXbk0
gsoAXLQAUq0FYLULZbgAXbIkdcIAVbYGYbUidMIAWrUPZ7omdsMab70KZLg5hMsJYrcAWbMEX7UA
TbMbbr5PlNUGYbZAic8OZ7kRZ7pvquMIYrcAXq4KZLgRaboNZblwq+MhcsBtqeIgcsEAVbEOZrpo
pd9dntsRaLoRabpuqeIccL9qp+AEX7UNZbhEi9ApecUAWrQVbL8ufMcnd8QAWrQAXbIRZ7o6hMsF
YLVUl9cKY7cnesMJYrcIYrcGYbUFX7UDXrQNZLlpp+AYbL0YbbxTl9dsqeERaLten9tdn9sPZrlt
qeIhdMJqp+EPaLlmpN8Tarobb741gckNZroSabsRar0TaLwCXbQAXLEabr8KZLkFYLUEX7UDXrUB
XbQAXLONwO8VarwAVbgAVb8AVaoAVaoAQL8AAP8TarxrqOETabsGYLUKYrcRaLoLZbcIYrhnpN8W
a7wAgP8BXbNZm9kabr0AZswCXbMAYL8DXrQAYrEAYK8FYLUHYbYAXa4MZbhTl9UVa7wtecVtqOKX
xvSz2v9kot2hzfefzPaJvOx2r+SEuesAXLN6suar1PttqOGgzfegzPaQwfB/temMvu6l0PmCt+mi
zvep0/ujz/iXxvOo0vqs1fyw2P6t1vyy2f6ey/au1/2m0fmizvidyvWu1vz///8oxCDxAAAArnRS
TlMAO4TF5JolhNbs4AlGwPDMM83tk5vy8UlY46zulRvu7zKI3Q/uy8P29hb0Th/y1ULiFe/vPmju
r4Lt9xT6Cpnv8PJttfT3E+bDvfSN9NcnvPX2mLL0lPX3d+rfIlfw8kEe8O3x94Ni6vP19/rs9Z6h
9/Ww9/a59Yr1tvWule528FmV+yec8Pf4+vz+8ukSDAkGBAGu9a/28bHk8PaKAv34mwX8CPkNEPb1
Fu75qmEwFQumAAAAAWJLR0TRedH/CgAAAAd0SU1FB+YKAwYXK/l+5BQAAAHjSURBVEjHldL5N5RR
GMDxRynRwkSWKMsQskShyE6y7y2ErDUl2SXZqUGFlLUoa6HlZuzJ3or0J6k5M+blet/33u9P94fn
c+5z7rkAVCnt2q28h0rsVdmn+vETUttPTg4cPPR5BCEkUScmGoLDo0jamCap0ToyjmRp65AR3YlJ
OUFTemTmqD5SNG1AQo7B8S8MZGhEYIxNhGA6s2m+mhGYE+bIwpJx10krXmJtM/tvkKHmbHnNKbt5
6aiFPZwekZ7OOPAZRyf5UnK1cJbPnHNWvJnLealydeMx7h6Mh/6vPBe9vHmMjy/a0gU/uOjPYwIC
EcIUT0HBCCsklNuEheMmIpLbREXjZlzAbS5dxs3SFW5zNQY3KPYap4mL38EkXOc0iUk7mGRH7uVS
lnGTmsY2feOm6Nbt9DvfcJNxl81kZkkW57//+Imb7BxWk4tYysunNwX36E3hfXpT9IDexBTTm5JS
avOrrJzNVMi/9O9tZqWyivXbVD98JK6prK2bED3eYlafAEdPhfUNjc+aAJ6/YKIlMRDVzFRrLWQI
crIV6I+IEDGVpJUUQdvmhuvtxAg6/srQy1fEprNLZl6/Ib+ou4feQG8NvQG3PnoD/WJ6AwNv6Q34
vaM3MDg0TG0A3n8gGNoAKZIfo2DmRc4AAAAldEVYdGRhdGU6Y3JlYXRlADIwMjItMTAtMDNUMDY6
MjM6NDMrMDA6MDDIf+m+AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIyLTEwLTAzVDA2OjIzOjQzKzAw
OjAwuSJRAgAAACh0RVh0ZGF0ZTp0aW1lc3RhbXAAMjAyMi0xMC0wM1QwNjoyMzo0MyswMDowMO43
cN0AAAAASUVORK5CYII=" />
    {/* <!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --> */}
    {/* <path d="M512.9 192c-14.9-.1-29.1 2.3-42.4 6.9L437.6 144H520c13.3 0 24-10.7 24-24V88c0-13.3-10.7-24-24-24h-45.3c-6.8 0-13.3 2.9-17.8 7.9l-37.5 41.7-22.8-38C392.2 68.4 384.4 64 376 64h-80c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h66.4l19.2 32H227.9c-17.7-23.1-44.9-40-99.9-40H72.5C59 104 47.7 115 48 128.5c.2 13 10.9 23.5 24 23.5h56c24.5 0 38.7 10.9 47.8 24.8l-11.3 20.5c-13-3.9-26.9-5.7-41.3-5.2C55.9 194.5 1.6 249.6 0 317c-1.6 72.1 56.3 131 128 131 59.6 0 109.7-40.8 124-96h84.2c13.7 0 24.6-11.4 24-25.1-2.1-47.1 17.5-93.7 56.2-125l12.5 20.8c-27.6 23.7-45.1 58.9-44.8 98.2.5 69.6 57.2 126.5 126.8 127.1 71.6.7 129.8-57.5 129.2-129.1-.7-69.6-57.6-126.4-127.2-126.9zM128 400c-44.1 0-80-35.9-80-80s35.9-80 80-80c4.2 0 8.4.3 12.5 1L99 316.4c-8.8 16 2.8 35.6 21 35.6h81.3c-12.4 28.2-40.6 48-73.3 48zm463.9-75.6c-2.2 40.6-35 73.4-75.5 75.5-46.1 2.5-84.4-34.3-84.4-79.9 0-21.4 8.4-40.8 22.1-55.1l49.4 82.4c4.5 7.6 14.4 10 22 5.5l13.7-8.2c7.6-4.5 10-14.4 5.5-22l-48.6-80.9c5.2-1.1 10.5-1.6 15.9-1.6 45.6-.1 82.3 38.2 79.9 84.3z"/> */}
    </svg>
    
  )
}

export default WithIcons(Bussiness);