import React from 'react';
import WithIcons from "./withProps";

function BikeNew(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAkCAYAAAAkcgIJAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
    AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAJ
    v0lEQVRYw81YaVQUVxb+qqr3DZp9laCYoEQBkxiNqDE2ioAa9RgjuESNiozjmEmY5BxNQhI9mRzH
    0RzHJXGNCsYzkxhFEFnUiI5o4uiILMHIIogs3fTeXd3V1TU/JjS0gIDBzHz/6r5773vfe/e9uvcS
    GCyk5IZJJeQVM+0IfJSan4f4WutO1YuDNm8X8AbNE8U+FaCUSbavnPRItcpG7djNzNlK/ZfTRww2
    GXLw9+fRGBGixOKpkUO80wpO9tto4akoLDgx7P+ODABMjx0imTwqZJpsRf5HfekqVucflEr517yV
    sv196Q5emAFwOkHoTLYex/g8ElIR3/W9LH6EqK7VsK4+vfCuZlf84W4GqaeXEyT35fyXnrZ5y4WS
    A4WVzr7mJwaNyaL8QKWUKLHa2YCehmVivv7DhWODnvJXuMmXby/Wtultk5GVVAYASDn9nFRMZT0/
    PCBo/ezRch5FouR2Ew4UVp5X71a98tuQ6QckK/Kbt70Z5x/kLXWTv/pJLsvSXnKZp+GQQiyMf2du
    jHJ4kKdrvL9kftM7Y9mfELB653nuYXl2xnQqOIiuX64a8doXa192IzIQ/A8eAOeIpduK9F0lEhEP
    e343xTc+dsiv8szD/Dxf8Bx+fWryKQaHk6oH5D01dxScrPvFZQGGdW7OOHD5/S3LJ8gfNrHZWbTo
    LG4ytcEK9ONK8BSevB8kQoFPnwsjANHbF96v2/rytj51Z+ZIZP78IxGBnmObtSZlTyo2hkV9qwFh
    fu4PwvGSO3TO1VoDQcDFiONAySTifX0vcVEOl/NBcr82esu3N9Q3a1s3Gb5I+Lw3HdnKMxutNvb9
    dbOiBa+MDumX34fx1r5L2p8b9fOQnXx+IHYDujMZc2N9fOTiTKTkds9ZUk6rJMvPtEyKCn7nu41J
    j00EALa9GaeUSng5eP1k0BMjAwCfr5rkKZGQ+Vh40h8AsPisn3zl2eJnQj2Pb1810W9N4iiPx2bR
    BYfWT5VSQl7NQGwILMrhPl06vsdBg9kGFsDEke4bZLExSNlSYBUKeJ86ndy7f3w1Wjo+MrAf0w0M
    dx/osfFo6V0T7ZgJhpWCL7qHI9NbeyXjvaZwl51lX+oqtNtZJcshgCRAKWVCat+6qd0MLTYHvjpX
    Ra+Z8axoMBb+2mf5Fn9PMbNj9WS3k21qNxs2HCmFn0LMtOgtNhvDWk1m5mNkz+yWAnV77pRpBZdU
    MaEj40YGKocGDErE9Imimw3YX1j+D5IgS2LCfT7ImDfGuzfduw/0uFTepD13q6msfY9qcq9kBMvy
    dJ8tHe8R8Zh/4MfF4q0FRp2RiUF2cs3T75V8ER8duirhubBH2lTf12HDV1e09KFErw6Z6wHwTi+8
    /nHqi785kYu378POOi8gO7lGsSp/faBSOrcvIgDwdLAnNi58QemTXnStQ0YBgOTNsxvmT4iYMykq
    iN/VgLY7UH6vHXYHCw+p8ImQ+TD7ms5gYlMQvZgn5BMntq6I67ab9a1G3GszQikTgiI7H+AApQQc
    B2W1X5KTuX70Eg8ALLR906vjwt0cFN5ssBwurjJSFFFhtjrCQ32l1MeLxoXKRHwMFkqrmsFxuI2s
    xArZqvys3yePlnUdN1jsyMy+1tCgNjmkQl49xyFy8dRIhSo6RNKhM+vFcMnBoorNAP7Mw+LTK+aO
    H6oD4NqRW7Ua48HCiuvGvQlTOmR35uf5pu28ULo7ffJQuVgwKGQOnasythutf8Ab50VW2vzauMjO
    UshgsSN99/c1+lZyLE7M0NC/yPezZy/4KcRjRod7ywGAIgnMHjfU8B176g3SQ8xPHRGqdBFp01uR
    +fVVW1ciAIC/J7bpTdYl7x26ohkMItd/boXJaq/E0Vn/gt2cODEq2Np1POPA5Ta9hUnBCZXbfKa9
    01/OPHaV0Rg61SODlZ4eUmEqCSA8IsDFBSdL7+oZ2vmnHleQNfuyiWYqqhq1v/5Uiiv1ejPzFgCA
    cI4J8ZG6Mujye+2wMWwFjiRd7cmWcbDvnv6hvuOwEBHkCQ4II802R7BM0nkPyht0ThBEXW+LaDfS
    P91rNf4qImV1GrQbbLU4mvxPABAIqDAvWecD06g2QWOgH1FucPVldRpXVq2Q8GG1M2GkkCKtFNH5
    uyEJcAAMj3CkYbqUKCXlTZizKc8xoFMpqtQaaNvbHd9ykUAs4FOdO8+yANB7ODt4BpIkXBUrRZIQ
    8iiaZJ0cq7fYXXpBXjI+SGdsb34kQr5qiG9nTdWis8DhZA8s+at79dgbqhq1uK8xPcCRWec6ZBoD
    XddudEUNQn3kkIj4vdf7lDM2yFvqCied2QbWCYaUiKnKB+1ml17CmFC5VCxY26OT1LwQHklGjHqq
    M9toVJsMcHIXeTze6g2HS/skdLCoUmO2OjLcpURZg9rkiobocB8QQCTm5/bY6ZFKeOkJsaGuqq5J
    Y4KAT90k23X2727UqF3Nrqgwb8wbP8xLuOxMtwJMJiaPb14yzi1hu1mrdoLgf9/2tynHa1p0f9mT
    d7tXQjXNejSqTWZkz8xzG7DaLtyoUbuF6qdLxytkHtQ3D/sQLsvbMWfcMGVkqFfnGmrUtN5E5/IA
    5+HcH2s/WvLKM67B+XERoU0a88LraYWxWgNdzBdQUb4eopHrZ8UM6dr3ulWnBuNga5GV2AgApr0z
    NpWmF4xxsFySn6eo28+otLqFNph+ecG64tt59fTK/Mbyeo1XVNh/Tz08QIENr78waqf0fFmL3lrJ
    ONgKL4V4yvPDfCMXTBzu1rPI/bEe4DkOEwCgTCvYszrh2dUTRrrXJD81anGnSQe5WIDooT7wfCil
    2fLNjfsXbzUuwbHO+AcALMpdCI6N6rZolruGr2ef6vHYUk5PnxwdvO+dOTFuJWq7kcatOjVMtAPD
    Az3wTIh7S6GkvAl7Cyp3aner1rqeMXJpLn08Y5pQJOhfxzb7+2q64EbDGc0u1dx+GfQDyjUFp5Je
    CFctiIsQ90ffTDNYtLXQ6vgqSQJ0yZqd9RLZG9uLLKyzz5YuKu5p8OPPbZcGkwgAaHdPm3Wl4sHV
    qob2PnUZ1oll24utjvBEVz7nXpwtyQn2EAlupM141jcuqnsvoU1vxbGL1YayOnVJ8w5V/1o6jwH/
    tcW5MUN94xZMjFD4enQ/pAtlTeYDhRVGrd4Sg2OzW3omAwCZHKlsLtqhM9Npz0X4tQV7yzzUetrh
    YFn1v2s1cgGf+tDw5bSdT4pIB2SrCtc6GEdmdLi3kU9RPkqFkN/cbtH+cKfFXykX7tL6x69DJuEW
    Rr13CTM5EtW58YAzECTBgHPWIWv25SdNohtST04AqHBwBA/gHiA7+Wxvqv8BOJTsC6md9yMAAAAl
    dEVYdGRhdGU6Y3JlYXRlADIwMjItMTAtMDNUMDY6MTc6NDcrMDA6MDCsOQvWAAAAJXRFWHRkYXRl
    Om1vZGlmeQAyMDIyLTEwLTAzVDA2OjE3OjQ3KzAwOjAw3WSzagAAACh0RVh0ZGF0ZTp0aW1lc3Rh
    bXAAMjAyMi0xMC0wM1QwNjoxNzo0NyswMDowMIpxkrUAAAAASUVORK5CYII=" />
    
        </svg>
    
  )
}

export default WithIcons(BikeNew);