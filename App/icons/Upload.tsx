import React from 'react'
import WithIcons from "./withProps";
function Upload(props: any) {
    return (
        <svg {...props}
            xmlns="http://www.w3.org/2000/svg"
            style={props.style}
            width={props.width}
            height={props.height}
            className={`bi bi-upload ${props.className}`}
            viewBox="0 0 16 16">
            <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
            <path d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z" />
        </svg>
    )
}

export default WithIcons(Upload);
