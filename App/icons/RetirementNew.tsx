import React from 'react';
import WithIcons from "./withProps";

function RetirementNew(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAA0CAYAAAAXKBGzAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
    AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAL
    rklEQVRo3s2ZeXxU1RXHf2+f92bLZJJJQkgggbBFTChLZGlRoCzRgBaxkAiiRitRsRatfrRStHX5
    YFXqR6GgIlYCaEohBAgooIKAIi5BSIRICNmXyTL7zJs37/WPmIFkJpkZBD89/717zz33e5fzznnn
    EbhWsnhnlsCyOU6ntFWjZp+xOz0fgiRolqGzxXezl0Ziir5mkArlJgjy0cy02PuWThtp2vVVdXZV
    QyfZbHG9F6kp8ppBbs4pI4Cq3KnDTEMG6PHovAytxemRXaL4WqSmrt1OArA7peffKj2zKWNoLNfS
    6YKs4Bw2z/vx/woSsu9wndlhr261QlZA+hRy/ZWYCX7cqxQSCz7R/GxIknIpJFifDMYng9aqOe+V
    mCF6N6jzS5+kSPJhp0cyqlVMvcXDpGPTTe4rMZ7wyKGvHpwzehxDd+3FzuMXGo6XN96OrTnHrxjy
    uqePLk426VbfM2NkPMdQuNhiwwtFX7U0NItZ2DanOhLDpoKPtyy7efSicWlxPdoL1n1WU1unpKPo
    Jnu4ti4d94IPo9tt7heXzbkunmMoAMAgkxZ3TRthEtRERB6pf+DjZVNHD5zdGxAAHrs1IzlxgFIS
    ib1LkDQ3UMXQ6t4K6clGSLI8I2yLSz9RUcAzS6aPMACA3eXF4lc+su7+shoAkJoQhfFpcZm6+/cv
    jxxSgbnJ4grwdr2aAwFQWLAvOhyDHOl67daJqXoA8MkKVrx9tNbmEosLP6v87kxNGwAg78ZhUQ63
    9+XIIbfNaxC9PsbpkQKUBkSrRbCekSGtLVAoNctk3zYxVQCAtz46Y2kw24vGpiXMHRKvM9BU13Qq
    lkZOVooDi4rvigwSAMdQVXVmW4BSaoJeBYIIDckWzx2dYhS6HwfFaGlORVGt7Y7PDTqWHJ5o8Ktm
    pZkMeq0qN2JIglDO1LQGOt1gk5YDMCoMc2OTYjQx3U+zxyarF08bnifKUsynp+qe+9fe083dfckm
    Hdxe35SIIe0u6UStORAyKUYDvZqbHNoaiQSDfyNxsKyhubnTRWWlmbL0Gn52RV27/y7pBBYxOr4R
    q5SQUa+ngqJUnK3raANg7A0pen2pISEl2V532SLNVidVerL6G0mSz2oFfvzyuZna7r52mxs2l0eN
    fxJSKLM9w6JEV9S3O5jeSqYoAaKk6LH0E1W/1iii9JNT9Zbux4W/SYu5f3b6dF5FL5o/afD4IfE6
    XXffkTMN8HjlHSEXHgD5QfZ5i0NUSz45QDE+WnDDbe3feTbnlMlQSjYeKPeH0TljB2P9g9MMv5s0
    1K9WXtOGos9/vOB5N7sgckgAAkvXBruXg2K0BCgypIe3vDFjcXlNx8srC79sOVbRgJZOJwSOgtXp
    xunqNqzfd9qy+r/ffWPZMCv09flJAi4tTZNn68z2wSlxuh7tqQk6zbGKhjA8HDj70pSVWLj746pG
    y1KDjkvziHKSoigeSVZOmTvt21F46wfhAgJBsiDk7Vq5cOqwZ/NuHN6j+czFNrxY9PUFBbCEa7w/
    ETi6tsns3IitOTsj3kmAKK9ssHQCiLq8NX2QEc/mZaU4xZDOGJZQBJG548uqiecLDnCta2f0u7OB
    kLJSUdtqC5oMD0nQXxXAbonWqmL+vOnYQwD6hQyEGZ5T0Wp1/fysPAz56lwTZFk+G0ovcCdXEbLq
    ntKmhjbHgAHGnpnb/m9qHBtKT5MESSi9h5mieNezuVnGWD0PADhR2eJ46cOTJBlEFwDi9EK9QuKk
    Zf2s/MghAahY6kKN2RYA2dLpVIuS/CJkvNqjg5BHuTzszm5AAPjb1hMCfDD1NXGNx+7Dlls6wtnx
    oJAdNs+Rulb7ZPR0cCSbtNBr2DGW9bPMl7dr8vc9svim4f4UZ+cXVeBYep1n4xwzroIE/1pUlPLz
    TdaAN3pyjBaiJA/t0bioJMYnK9nTMgb6m7YdrnR6ROLvVwOwb0iKqrjYYguIjQNjNfCIUsrlbTzP
    PD1/8lD/vTt8uh4KlIPYPLvxakH2kSbRFY3t9oBkgqFIjEiKbjm/dE8DCMgAoFczxIIpQ/yX8fCZ
    RockyeO5u/fUhQMgcGwVQSrmdrvnYfw7pz6YDtHXYNXdpea1BVONlzsDALhFHzrslz7DdQILtYrp
    sz+UkASBujY7Nuwvb24wu8ahMDtgcX0mnF2fEvYASBVLISFa3dewkP3BJM4gYHWCPm7FO5+XNANj
    AhbS10CL03MsWDZ0rUSv5qAVmBjk7hkUNiR8ypHqZqv1F6MEUNfq0KNBCLiXfUNunbv95I+t1SfO
    NeOXkH/s+FZkaPJ5fHpTQAZDhBoc9cD+oxRBXh8XLbh9kkwBQKJR47tvdnqM5ieHOdfQaXuz5JSP
    ocmgIdArKcSqvAlRBg0HAGizurCmuMziEiU5NkpwVTV0gqCJN+tfm/ZCsPEhIQEAuSVpoJQk+EgS
    MqVlVfLW7U9lc93dC1d/5HA4vfkAgkcYWhkzMtHwzOq7J2sBoKnDgQfXHe4QRfkOyIoEp1yBXfP6
    PLLwiqhbcioBVAIA8nY9PD1zsL/r9MU2MBRRjS23bOvHwoHKu/Y85/RIEDga8QY1ojUc2dTpbMWW
    nLJQ00dcM9equYdnZCT5d3H/t7W2Tod7TahxKpbcfaziUhCakZmk5zn6znDm7IJcWDwmatnHO/j8
    0tX9aueWpNEkaRqWGOVvOnK6nget3RxqIrtDevdgWZ3/bTFpZDxIgggss+TtnRNbcHC/seBAEXJ3
    GwCATlpxaJaa5QrnT0o1dtjd2K46OLP5jemZwSbieSp/5phktvv5+A+NEDjmsO2tMCrBW3L2li/Z
    zYuSDyxNISlWC56ltY5FJSOwNecHAEh94siUKJ4pXHLjCIPbK2Ht3u+n1Ny993rSZveu/csd44w3
    DI/HnLGD8cq9kzOoJbudQSdSiKUzMhP9Iaj069pOm1N8I5wjAwCBo/dcfuRTRycyIJX5AKD9Q+mf
    4rSq4mdzswxDBuiRPsiIBZPT4nmCfIpkKQrdlV2g682/5fFZfMLyQ1W4s/i3/o7Fe7KMOp6ON3SF
    PFHyoexCqxqFOWFVIbqOXNx08LtLFY7fpCeo1Dx7L3NX6fsLfz1i+VN3jOtRAyUJAi6v6CBpklqx
    bu/3Yq8VY+2yqSnDE43bogsOrMGikhiORv4t4wf5jXx6qh6CitkeLmDXkc8t/q66VeuTu16nqfF6
    pCdF87dPTp0/d8LggHBYdLSyGm72Vcp24r0fpPTfD2i3i6MzUmL8rySSJDDzV8m8jmduKKtuW06T
    5LBH5mWquv8kvF5yqrO5zb0C32+5GAmnMH7JhFg9P6y7+DAuzaQZO9QUUH+6/YW9YpuZGIKiWZ00
    ANSumVaAPx6CwNFL508a0iPtmZ6RhOkZSaoLzVaVwHWtoc3qQm2rnQRJVYZFdmfxcMjERI3A5jg8
    0iy94Pe9HmkeANSZ7XjyveNWDykNRNHNNqBXxFHnlz6uYuiVzywcr+nvG9vmEvH6rrL2UxfbWa/k
    IzQ8e9Lnk9UMRTpkoJwERrE03WFxusdSJMUzFJjrBhvJCcPitJNGJoChgr+et352zr3j+Pk2F21K
    wYZx/h9TgWExtyRNI7D7ZmQOTLx35igOIcTqFNHU4YDDI8HpkSDLMkiShMDSMOpUiDMI4GiqXxuN
    7Q6s3PylxeL0vO3amP1Y7/4+Y7eQX/pXFUM/NGVUgjZnQgoXf1kF92rJ8R8a8Z/Pz3dYnN7y5k7H
    g9gcPET2n2DM3K9GrPcBnqOeSInXc7dNTNENjdcjple2HomU17Tjx0aLq/CzcwpFEF/Y3J7n8f7c
    Q/2NCS8LAoC8kpl6DbdQFKV5FEWqRg6MFieOTIjSqGhoBAY6noWGZ6DmGHQ6PLA6RVidIuxuLy42
    2/D1+WbLhWabXsuz3/sU3wdOi/IOim5uCmfq8CF7AO8dCPgm8zw7lmfIGySfrPVKPp0CwmhUcx0d
    Do+aIIk2liItPhkXrU73t4ByBIXzjl7JdP8DlV1zV4Ha1WMAAAAldEVYdGRhdGU6Y3JlYXRlADIw
    MjItMTAtMDNUMDY6MzA6NDUrMDA6MDCBFrhHAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIyLTEwLTAz
    VDA2OjMwOjQ1KzAwOjAw8EsA+wAAACh0RVh0ZGF0ZTp0aW1lc3RhbXAAMjAyMi0xMC0wM1QwNjoz
    MDo0NSswMDowMKdeISQAAAAASUVORK5CYII=" />
    </svg>
    
  )
}

export default WithIcons(RetirementNew);