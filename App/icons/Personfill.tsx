import React from "react";
import WithIcons from "./withProps";
const Personfill = (props: any) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi bi-person-fill ${props.className}`}

      viewBox="0 0 16 16"
    >
      <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
    </svg>
  );
};

export default WithIcons(Personfill);
