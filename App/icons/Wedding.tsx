import React from 'react';
import WithIcons from "./withProps";

function Wedding(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
     href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAA0CAYAAAAew7HJAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
     AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAN
     wUlEQVRo3s2aeVRUd5bHv+/Vq1d7FVVQ7MoSIQIBJypGY4IajYoixnNiOoImpk3bk0nmTE+OyZmx
     T8+k57SZ7tPZZpI2S9utGUXbmMQRxD1NXKJGQQ0GcWORKrZiqf29evv8AVWKIAI258z973fv7973
     qft+v/tbXhG4j0SvP7qZEYRii4GWAqwQUKuoj92fPf3R/fzGU4h7WtZXq01Kj+PnhTmmSQkWfaLN
     iOutbhy54PBeb/V83vz+nH/6fwetX3ew+delj6VMTrYOsn20/zJ3sq71PeZPhRvH/OTSA2ZAKgSQ
     AkJpBqGpxPZFwZG4kkNqSyrXLnx0gmUoYAB4rShXYzVq12BZRcyYgNeUz4mzamrzH479+NWi3N/l
     Z8R9Yjer6rGq/ImRuFNDKWkNUZAYbYwaztGgpQwwYwqAb0YFXFJZkDvRtu/tF2dawqrF01KsAKwb
     Pz+7//Kq8iLsKj416kzTKlJDkcRwfqBUpAyCJDEaWVlltFs1O+4EvlPefnGmJcaq/wvmVmlHDR0I
     8VWtPUxoOMcef4iDJNaOClrjX5IaazIO12Wi3WRAYnDJqKGxo3jLwZpmptPDDGn+/JurIV4QK7Fr
     eeeooBUiNT8z1jpcl8cyY6NAIn300AAYnzbltU+O+6tvuuAOcAAAZ3cAW47Wu8/dcO1zf7Jo/aiA
     ++RWTUP3sBXiQmOXABmNw/Wh7mnZMy8QWn3o4Q/2/bApxAvP2ky6bk+QU5MkNgcJ+ztjAAZ49pDD
     5fcAMNyrS2OHrxNthgPDhbk921ZVTDab6F8SCjI5SbbRauIUL8jpBEGksJzwJijpiJ7SbQjx0puy
     KL9h1Kl/IimKXauhWiHjmrvX/QvseY69L3jJ/nl56dF7N70weDJu/Pys183yJc735twfOmtj9dT2
     np6Trxbl6RNteogS8LsvL7g9wVDj8wUZ03681RO86vBwC6dO1Ld0+aULDa6TxTPSFs/JTQalItDi
     8uPTw1d6PKHAJGxb4RkJeJxNuy0l1mTKz4izXmzs5hs6fD1qterl+wH3Qa+qWPZkbvzWN5+dHn2n
     YffJG9h1/PqZqQ/Zp1ffdKmSoo0fc7z0gkKADDCc/92Xn4yfaDcNCLbuv77pdDWHslBZ5B78KIVA
     aeVLgJwChRAgQwUVpkIBBYCHIt8CQTgBUgXIndhZvO1e0FS0VfvO0hnp0XcbZmTE4evTDYnX2txl
     yvZlLzkBYM3+P6hVqn/V09QzdwMDwBM5iZavPQ1/D+A/77ZF/8OxuvlTJqVSJKEzaCmoVSQYToRG
     TYEXRQAEtGoVAiEBLCexJ2zHXnd9tCBvSGg1SWpzJtoGGdLizSAIIsbn5X4ZUW4vqhdKy88//nep
     zw0VbPqkWO3p+s5ZHXfpzT8/svWnT2dlFeQk3Xfk9IsuzqrL3RI8+Htua+EbdxtJgiDke3nOyIyT
     QamK7tQZdeqSWZPjNUP1jzFrEQhx2XfrfUHuxSeyE0cKDAB4PCsBsqK8MpSNbHcHU8ONiw1d+MfP
     TnRJsgIAeCI7wWTSqUsivddXqxlOyn803Q4AqHf04tWPv+3wszwAIMFmAC9IyVh59HZlWF1ROjsr
     IUgStwvV/vPN+Pp0wwCQr767iUM1t26/HT2NzKQoCaXlhYOgjVoqMmmOXXIGen2ss97RCwDIz4iF
     n+ULIr2D7UVT0mP84WZtcw9auoIndp+8yYV12RNsIajZuZE3o6VeWzRtYmTp/u/9l1u2Hat/d+ux
     K2+8svlbByeIePnDqvZtx65s2HL06gd/PHIlMrqWTk8xW4zaQft2MhASI8uqjwmxsgJPeAUkCAI5
     KTY/VlcuBgCjnl5VkJMYFe7f2Olrg4QDZ662B8K62dkJFo2G6ttirqyMl0Hkhd/MzuPXcKau8/fc
     1iUbsKP4HWenv2Tt+8fcnb3sIpQtf5f78+J/PlrreP+LUzcZAHgyJxE+hl+AuVUDFkHSqFF7wg0v
     K6oDrNDrDUYShyezE81mfd8Q4QRpUX5GbMTW7WF1oGRHMCT6mjp8AIC8tBioSHINAICSni+cmhIZ
     F/Nyk2EyqNZGApgSvw+wwhJkLKkLq/QqVWFWcpQ+3C7ISWKQFHxhYKY5IZI5KAogw0ncMf7yM2Ih
     iGIhSsvzYy06yWK4PQfNBpqHQLmDrFBW0+ACACTaDLAYaBJrytP0evUv5uYm6gCgqrY12N4bRGaS
     NRMl5esAAJ9NF1BWfBZv9ReD1eXPTUq05Nstetxy9SVh8bQJpigj/foAaLtF5wg3REkhQSikj+Ej
     HWKj9DDpaBrAunm5SQN2aC4vo4Uih6AQf/2uviMyN/JSo82QiZ+adHRUapwZAFB2/Br7H7urudI5
     GSazUfsrDCHRZv2//WxhtuHNP38X/PWu8z4AeCQlGnFWvRGlB5Ij0F1edkK4YTNpOCiKwPLigGCz
     shNotZpamZ8ZO0AfDIk0QPLYWVTl8XOMovRVnWmT7BoNrVq7ZHqKBeirSiFOatKrqV/V3HRhcpIF
     KC2fOSDYqvKnJidbredvdDEygV0hQWpr6PCGkxAHQn4pAp1g1UfqjJfhaBAq1qAZuPl7/OF4rd2s
     MabHD9zjxFr0HlDgAICXpM7a5p5+6Fho1KrYpx/ty8fJK20+r198WyYxzaijQRCEBrL6rh2mQqsI
     aHS0Sk8RRIw/wL19qq7dDQALpkzQGjTUqxHodjeTEm4kWA0MCEWU+jMWluyJNnz62lP03a+TCQk+
     8H3QvkBoa1N/ZmhKhbINC2mTjgYvSrjU2N0FSr6mpchFaXFm1DncEnYVDjwH7lp+qKahy5ceb4Hd
     rCuArPR+e9kpAkBitAE2o4YOvx3SZtBGTh+9fk4DRSE63KwTI5AQL+ph0PXXbaq26nKb5+4+xy45
     wAnSGbNOvfGVwkeitlddCwZY7vWh4gVZvuTdfRe9b5XOsOkM6u0MJ547WdcGAFg2M82qpamfAQDZ
     GwzFhZ3sUToWBKkJMDyNEUiPPxSHYP9BYufSEz6GH3QqOXzB0eVjhb0USS6bYDeisd3XhB3FXwwZ
     sKz4rC/A7f6hqRuLp6bqBEnuPfljWy8AzMtLhiDJqwGAjDZp28M+AYanoSi6kQAPJSwneK86b+9K
     23oDCAnSLatBvXD94mxL+ffNXJeP2TJcDDcj/nFH1XXfs7Mf0iqKsqKx0+fwszy0ahXyUmMYlFas
     IHv8oYSwQ3KMSQ1CVjBGUUB82dxfXwHg9NVOtq0rsN3HCOtmZyfi8IUWAgQ9LDS2L612B7lbzu4A
     Zj6cgJ5AqPlMfd/K/szMtCh7lK6UjLfqb8n9nCShEFDuc+ExjDCceO660+sKt89dd3UAMM+fksyc
     utIGvUb15UiuvgIMv+nQxRbfillpRj2tyjhR1+4AgNQ4M6AoE0mrQf2b9/ZeCuw724hzN1yNIHBu
     rNDYUVR5xdnTvO9sA36750KA5fgP46MNi5bmp5oP1zi8vgD/xYji9Kr+90Rtqz4zyQpelFN7fey+
     zw7VeT49VNejplSbqPrfFmypX13BH69tjcGuZe9hdUXpmKEBtH7w1GNbSso3gCA7wJOHtSb8Jj3e
     gktNXRbsLN43oiAHl3DG9Uf+ev5G58JZkxOkqh+ctQ4X8y8glSDKivb1zfwdy/5nuBiHqlvY8vNN
     wr8/P8McZx3BPN1Z3HfFsLrihemTEuWz19ph0dOHvKP48d4gV37uumvO/Lxk0+Xm7vndm59+Pmwb
     0V3c3rMNksPlP/Ht5RGV74hYDNrZBTkJxvM3ujlvkB9ZliNCnrjY0BVKizfDxwpLB1hG5E4SCghi
     dFdgAHhRXDLBbsKlRhcHkjg1KueypZc5QQqQZN+BF2vKJ44KekxSeEDD8VK81ahBr59TY3vRj6MN
     IctKV1OHD5MSokRI5PTxh7YRebmp0e6b7R4YtOrR3a72CyeINS1dfmRNiIoCIWeNPzQhZpl0tK6p
     04+QINWMCVqUv2v3sCG7RQejnn50/KEVJTU5xmC82eoNcLxYPbYgZPt1Zy8TF6UHCGLSuEMb9XRe
     vFUPd5BjIYt1YwoiSQ53gNPER+lh1lLqcYfWaVSmRJsBDR1ePWRydLUyQkc5/IwgWgw0Otxs5rhD
     +4JClkGrBsMJOvxleduYgpQt8bG8YARBgCQgY81hw7hCy4oSI0oy4q2Giw8SR02pgn6Gh0Gr5iGJ
     MeMHvfILlSjJGlGS4WN4/YOEIgiix8/ySI0zs1BEy/hBw2DXqKmgjxFAU+RothyDRKMmO4Ihoe/Q
     rVIZxw+aVKwpMSaB4QTIijKmhSUSigACIRE0pZLDp6rxgaZkS0gSVd4gD2+ADz1QLIVwM5wAq0kb
     gkJQ4wdNKCYtRUkMJwKA70FCuf3cOVFWwHIiBULRA0N9kpPJgMlA83eqbCat19nl79LSA3+jPUrf
     1s7G8oMfRZi1tErqv7dOf6CDhYIsJiSGs6saGloIHrzU4KJYXoCOVuOasweObn83RLHsyEXn2uUz
     H4oHgMrqJvCCdAF7cgZDyzBZDLR22Yw0tHQHFlAkMX+szKIkE3MeSUJNg0sJD4+hD7Erq4z2OOl0
     lF6jdQc4T/fmBTMAYOqmmjxnm+cro5bSMbxY0/Hh/OVD+q/eO/exzKSvJsSYbPgbyfU2t7O23vUs
     vnrm++FP3j/Z+xB2rxj4neEthURdRTL2FLcM61tasRBQpv6toCHjVPgvFf8Hwc3l23neK6kAAAAl
     dEVYdGRhdGU6Y3JlYXRlADIwMjItMTAtMDNUMDY6MzE6NDIrMDA6MDCrc+33AAAAJXRFWHRkYXRl
     Om1vZGlmeQAyMDIyLTEwLTAzVDA2OjMxOjQyKzAwOjAw2i5VSwAAACh0RVh0ZGF0ZTp0aW1lc3Rh
     bXAAMjAyMi0xMC0wM1QwNjozMTo0MiswMDowMI07dJQAAAAASUVORK5CYII=" />
    </svg>
    
  )
}

export default WithIcons(Wedding);