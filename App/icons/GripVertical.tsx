import React from "react";
import WithIcons from "./withProps";
const GripVertical = (props:any) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi bi-grip-vertical ${props.className}`}
      viewBox="0 0 16 16"
    >
      <path d="M7 2a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM7 5a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM7 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm-3 3a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm-3 3a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
    </svg>
  );
};

export default WithIcons(GripVertical);
