import React from 'react';
import WithIcons from "./withProps";

function FamilyPlanning(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAzCAYAAAAU5BG4AAAABGdBTUEAALGPC/xhBQAAACBjSFJN
    AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAJ
    H0lEQVRYw9WZeVAUdxbHv90zwxw99wyiGBUSFcWjkkgUIx4YDy4VN4WCuB6JKJjatUJ2k3JjLN2c
    mtWYUFGjwZwSjOsBeMCAcSPeB15BDhERlXuYYWaYnuk5ev+wYnnMNDMoqcr7a6p/7/t+n37dv9+8
    92sCT2JrWBLVeZlaJRVjppmxMnGAkWVhYRjXVbPZthU/J5Y8SXiiu0JFxpElJrP1q/mTwpiXBvcW
    aRVCSIQCdHQyuFTbijNVzY21zaaypqzJCX8oXNCKkoTntIrvVs6NUHP5ld9qxyd7LtQYt04dDBCs
    v/OQfmdsmS69v1r+Q1dgADBsgBofzI8cKE/T1XUnCTy/vJN+FjtBHtvyxiTKV4mSEoJxuoQ1gXEq
    58Wduh7LnEQhX/nalHCnvxlImThYRIkEKUgqD+g5uABebMTgXmJ/4QBgZKg2ALzr8T0GpzfZXghS
    SrrDBq1MpAWPDO8ZuJl5siCl5A6P7N7uo5VLAJbwS+w7XP4sM213UDTj6hZcm4kG4Db2DBwAlxuV
    5fX6bsGduNZogBunewzOQju/LKtpNfkL1myk4Xa7biF31nl/dPz7v5aeF6CzMQmsOxAsWYufZhQ8
    5p2TkHtGXbIiLiIk8hmtz1sdcn6tamvU06s9DqbkBYElZ4PHCuAkj2JXwm+/D917QZMO9g4KEp2O
    GNhLxSMJubHTrr/eYKxvbLW8jtxZFx8KNqNAouorqs9eMVkj4HWd+Kz8K8yVev26pi8mPwanSi/e
    bGcci2ZEPkcE8AhRUVm9CUBBy5dT5t+DW7BXE6xSVmzOmBT44EpsNtJY/cNpo9Fq32LNjv3Xo4BU
    L379kunDNFOe7+cRqt1iR87/qlp/q9Nn3f1i8vsPDc49MFQkIX9ZPGWoIu6lkIf2zX2nbtjOVDZ9
    XP5x1L8JxdKiooy44dPGhQd7nOT7XypthRfqb5uNrnHYHdd6f2ANS0rvFuYqKeHElwb1FsolAoVS
    KoTRwqC0vMFosTrutBg61+CnmXsejCdNK1ol5vPe/mjRWFlvlec9c8GG4k6D0fEiIVtSpN/+92g1
    JRJ4fTTXGzrwRf6luhajNce6I+7dhwaT84JBYCpIcgjcbhYE0Q6SPIMfE0of8puf/6KCEm6JGhoc
    lh43XMH1KmTrrmH/8doMvkwsaOGRBGeFMShYgaz0iSHfH6nMPCYpiTfTjgIrLd2InPEG5M5qAPCd
    V3FqfqxKJkqXCAXRb81+QTYomJMLACAO4NvBg4uQLy369q/RYQtjRg3oUgQAerMdBWdv2s5WNXW0
    W+xWp9NdYmecFQChh9ttBI8MBsCTigVzxUJ+cC+lJDAx8ll5ZFiQT/EBYN76ok6znXiWAICARQcN
    Hy54WTnkGaXPAQDgZrMJVXeMuNNmRksH3c4jSZtEyNf01UqF/bVSDO6rglwi8Cvm2pyzxuoGw1rT
    tphN95enOkN3MT4idMic8QNFfkV7SqY32/DOjpMdIiFv9q2Nk44Cj5TpqgzdFqVEmPLRwrEKqci/
    O34SO3Sujv6mpIKx0Y7oB/fVx6uElPwoQQCvaHn8CIm3PexpmcFix7clFW3na5p1pm0xqY+Oey1h
    lMt0X0vFgjlvvzpKFhoke+pgPx6tsu0/VcvYnexc/Bhf6MmHu76ad2CURiHcoJGJRs4ZP0g1xo8V
    58nMtAN7TtTYS8sbjGba8Q29I3Yll79vxV9qwStSMf+9Yf21YauSI3p3B+xWixn/yD7eTpLEZqtJ
    tA67oy1dafi+BMbOGUcsScXny260NHU3a5drW2FjnN9j58z3fNX43bf+keZb5gAABoYSKhoXbiju
    052JGKeLhIu96o/Gv24lqVANHi3vdiqGJNZjDeHutv7Pacn7e3ZH9mC+LYj5hX36BMqOydJ0mU8y
    mfi1w40DMo99+lTvgL/ggHXXVQcbva6sNXB5ca7fAVL3hyO1wL35pJmdvvFKhyyt8J2nAiZfWlj2
    oa6NLahm2YJqlk37odYqX1pY5qtek65bpMkoNvyuL6hm2dC3fjUgJT+qKy3nalWl67YmjA5dMCdq
    4ENNyJU6PdbmnO1g7K6B+GlGmze9+PXCT8L7qZatmTda+ehY4geHHC66U4Hdc2hveq/vnDqjePHA
    Poo5j4IBwMgQDbJXvKIQi/m3kbwvxJNekVZUPHN0yApPYACQlT5BQMnlNVzJ8Qw3d+9goYC/bnXK
    aJU3oZIKwNY3okVSSvx48518ID6sn2rE/Ogwr4VrP60Uy+KGKzUZuj3efDzCUZQk759/eSEQXdiS
    rCN2i8097rGB3PhDlXfbD+edvklz6aNH9JX0UlITkZof6xtc6sERCmlA7666pDe3lxodjDMWO+M8
    nJ0QrOmrmMW5x6pvVNw2cMaZGRmqUVDCv/mYOXefkCA55x0fvnALJIH/ImfWUS4/i9U54ZuSCr2b
    4xw9SCEBn0d6fPyPw7n41QaLjeGaNHbUAKik4lfVGbolXH4SEa8yM/F5Ddd5Y0sHDTeLSt/gcmPr
    mgzWwtLyRq55sSo5QmV3uDchOc/jOYYmXbd30dRwqbcjh99tz/HrdwxGy3rf4AAYtk5L/zzvotnY
    yZlAbFwSRUmogAuPXlenF6cN6quaFDuqPyfZ53mXzU1G22fInV3nMxwA2Gl2TOb2UjNX8GA1hdRJ
    QxSS1ws/uH8x6WcpSWLNu3MjVFza0vIGnKtpOWnePm2jNx/vf/y7EipsTufaz/MvcwLOHBMipkT8
    ZCw9f6/R5YuXJ459jhOso5PBprzL5o6vpsVw+XFWJeZtMRvOVbecOn6N+/2zMa5gGBp7AYBKIXqZ
    EvI5v1W8+XWpiXEwkejCuiyZOrZNm/7ZvksWk5XhCELYQUn1AGB1OKpou/ePPFn5V8w2xvU+diZe
    e2I4AGBs7rFv7zjpcTe9UNMKlnTvx7fRNgCwW4mdpyqbaj35nrjWiHPXm0vN26f/x5d5fe4hnn2n
    NEoA5KVNG6YO7aOAsdOO/NM3mSs3287c3DhxwoO+/d88OoUSCXZlxI1U99VQoO1O6C7etp2ubDxR
    vX78FF/n9K/BSd0frpXJNoNwD6IZB88NIofOjs305quSij8V8nkj+DzSbOy077Vkx/rcswLA/wH5
    9IdQ30TjgwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMi0xMC0wM1QwNjozMDoxMCswMDowMJvOmYQA
    AAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMTAtMDNUMDY6MzA6MTArMDA6MDDqkyE4AAAAKHRFWHRk
    YXRlOnRpbWVzdGFtcAAyMDIyLTEwLTAzVDA2OjMwOjEwKzAwOjAwvYYA5wAAAABJRU5ErkJggg==" />
    </svg>
    
  )
}

export default WithIcons(FamilyPlanning);