import React from 'react';
import WithIcons from "./withProps";

function Travel(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
      href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAtCAYAAADhoUi4AAAABGdBTUEAALGPC/xhBQAAACBjSFJN
      AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAS
      d0lEQVRo3s2ad1xVV7bHf+ec2yuXXpUiIliwRqwjFkBESIxoKCbGitEkJlFjZp4xk4wzk4wTk4lJ
      FLFFUBPMOIBIUcSGPVhQUFRUOgj3cus5t5x73h+OKAKC85L33vqP+1t37fXda+/PXntzCfxvWFLO
      XKWU/yFJkPWarRExv+VQxG/N4rz86Be+roq3XhnjJ9WZLPj8QCnAIQkZM/f+FuORvwlFQpYb5mX7
      AUBLm6m4RWeyDfF1xvgQT+R8HIPRQe6b5YvzTyH+iPLXHvrXqdDcwwGgbLEKmTCJJAhvC8sqPFTS
      lrv31VHIfLlcvqRg9ehAt9XvxoW6PP7KpdvN+EvmJbNQyP+9fmvEl/8/gOZlT3aWi39vs3OjYkb5
      8gd4q8QDvFUQ8ikAwMt/yrWxpFSOXeGMKqUwKzl8wNSIYT6Sp0N8nX1Fd/5W8wM9Y47Bntjq/xug
      hKwhjg7i3S5KScCSyIHy/l4OAICr91pQdr+VqajR2De8Hia536TD2t1n641pUV4AIF2Yf+8v88f4
      +rkpOoQrr1bjT/sv6jkOmwzbo9b3Oo/knFDYCUeITVexfY76PwJSLT/yDxFFvbls+iDZsAAXVD/U
      I+tcla7oWq1QyONd5eyccHH04IAhfVSytMJydfhgb8F3edf2abdGLsEnHEncPWTNXhfT5d7dcaSC
      KSx90GRk7DORMaOsywRey/N1UvHet3OIkwopB5GQz7boGNbdSRp3c8PYMy8EpEo5cjXhd/0Dpo/o
      I9UYGGw+VNZ47V4rw5jZD0DxC5QSHHhzasiEYB+VdMWWkzqKAPXBK8Ok3x8uK1FviRgPAJiXO9LL
      UVK4ZfkkVVdjPGjW47N9F3Q62raD3hH1Xrvwem5/mZD6nCKIKYsjQ+RjQzzBp57MyxubjtarteZB
      vF6RTCrmCfxozQevDJWF+jkjv7TatPNoudbE2NYifeYPAOD73vGvZ43rNyHYRyV9N/VUk9VsDaVE
      /LudYu2ZcUm/JP/Tjf8s/a9Vs4Y7PSv3dZUj7d0pih9P3k75mcubR9PcNImU/EzIJ8cvmBasnDTY
      u8sUOY5jIXM39ArIIcjStDllskwpFeIfWVcfnq1sLDalTZ/7WHddfjR9iL/zq0P8nESrdpSUm7ZH
      DURCllt38XSpUV9dX3Zk/OGLD6ZHj+or6cpn7sRA0eRQb9G2ghtHJw7ydBwf4tltfv86W2Uy2+zb
      kDbS2iOQYknBg0+TwhyVUiFS8643ldxq/IcpLerPT/TC5YFeDlEJv+svmvf3QpNtd8zA3kxS6/fT
      Zqfb8m8Hein7BXo6dOnjohTj93NGOj4vzhc/l1pa9MyfTWlRGwDguUCqZYXZ8ycH9/FzV+DjPee1
      dxrVG01p0RvbHZJzQiUCasPa2SOUs/+aZ7ZZ0WE92O0c9bz4+m1Rge/TOfasdTMIkuj9dm5qMyHv
      l2rmbEWDvs1k/c6UFrnhsdZ9lMScpBlhvt+mRA1S7im+xeScr9KQBKF72kUuFSq2rQj3WLW9RH/r
      vm4Ffoz+4WndaXnhvDa9dTtFEgY+RTQDgI21y8w221akx30GAEjIGuKskp7euXKKvCeQE2V1+Nf5
      e5q6Fj1LkOT3JoN5C/bH1T/t022FBEIqNXFifwnHAa+OCRCN7u/mAcDjsU6SBNxVEhhoK7RGsxpN
      4ke9WdLhEBDcBPAku1u/Dd+DeQX/pAg2lbGys4P7qHQPmvV1ZhuTBgCI/4lSKERrnOVCY5vRLHeQ
      CruF+Ta3THf8ev15hrb9BXtjirvz6xJIsihv/bRhfUiFRIA1O0saKqo17s+W8s2IEEIlFeDd1FNt
      SpnkSxyfYAtac3phs87099jR/vz04lvfCBbkf0s3sGstedFJiC9eev2uOhz7ZuYAgHxJwVoDbdmQ
      MKE/E/OSrwQ9WEWNmmMYy8fYG3vueX6dgeI5SiIoWrxoWojo4LkquqpJl4WMmcu4p1xkiwtqhwe4
      eH2ccUGnNZojtKmRFwGgXk/bh/k6W2ePC1DOHheAH0/dWbr3+K0VwoV539CU/EOkhucgOTdKIiB3
      jxvgIVoeM5gE0CMMANjsHAmWM/fk1/nEFh5aNjzAWQ4AhZdrms0NWNlBTzw0a4C3yuHBQx00RvNB
      ZMRefCzpv5+289Ld5u/SCsu1ADB3Qj9x1roZvFljA1JIuoH2X3UiO9jHIf2rJRNcl8cM7tD/6GkL
      9p2ohJW1d5koy9pJEISlJ6BOFfJQSefOGOWrKL3bjDY9U4e86A6z4uwgXPbGlCDpn/Zf0ur1TKcu
      2bQtav3JlCOhPi6yaZHD+kgA4LUJgeLXJgTibEXDzDHBHh38W3Q09hRX6k/dqCcpijwj5FPTZo0N
      6Fwh1k6AetEKxR9RturpUf7uShwvq28xMLb0DnrSYW+zlR1Dm20wMtYa7Iu79kz1RgCAZsu0l3cd
      vXm3vFrdQX4apq7VgM8PlBqWfXtcfexq9WrrrmgZw5j/WHS1trWrRC02u703FeoIJKDHThzopQOA
      i5VNQnBkzjOFj/N1lZP/lX7eZDCapz6ZiKx+qpTCs25OohwkHloIAIZtkUM2ZV1tMJltnQb9dP9F
      4/tppxtPX697i9kZ7YT02K0AgIy4kgaNUWygO+dtsdkB2wvvIWKoSi50eailAQJaZETXPq0Khbzg
      e826XywGgzP2xTUBgCKlYFc/X9Wx5PCgMK3BQmJvzHbMLxapUgqrP0kc5SERdlzVu45W4GJl0w+m
      7dM9sDd2z7MJCflU3vnKpk6J2u12CgT1YntIJRP4+rsp8FBLAyA0zzqbGXadeW/Mo8+Tc970cJSu
      mzHS1y0uzE+SUXwTjMW+AfE/UTIec2biQB+fnAv3zeXVGvqz5JcclP8+YypqNa2w41B3CRloa/qJ
      a/VTp4T6dLies3aOgr7nCnWcPoIYyOeR0NEWUASncH3raCFttRXpt0V9DgDYG6NB4iF/F5VoS7CP
      Kmz1rOHtpzufRwEEx0fmHNaQfGjFsbK6JXqj+RehgBp49ErN0lfH9QMANLfRAnCoQvxPAk93p602
      lvMCAJIimEY9OQe+k7Kv3D3UqWuwc6Cgl73YHuLzKKNUxEdYkDtmjvbv26yjp+oZtuixLlmUv85B
      Ibi8dvbwaU/DAEDMS36QingfYcYhFdJjzuhTI+cjI/Ybs8WeeeZmY3u11XpGDBtdBYE4Virmxc+b
      MmDavCkDpg32dZopJOlN+ISwS0X8kou3nyw7juPAcRyJ4+E29GAdKmS1sQrGwiK9+JYm+8K9+0iP
      GQEQHGb97C9RSY69Oqafy5wJAV0ehBIhD6krJjt/uv/i5Za+R1ptrN2qVXPRyIgqqkzOUQFAq56B
      gE8ZmT1zLEg+RDe2MbzdRRU0ALAcB3CIAbDMYLLuOXWjYeioQDc5ANhYO0iC6OaEeg4QRVHGrfk3
      DFojs4PeEb0KAORLClY7yUSrP3hlqIvvM28Bz5pCIsDGBeP6VjXp+mafq8LJ6/WbrUCiTMQ7d7nq
      YZiQR4FHkY8eQtJjcvVzDw/U85lHXRVBBRBADhKyhoAUZJ2vbPy6faJZDiT5HwC1qE3rAM4H+2J/
      AgDnZUd/Gt7POfLtmaGKXsQCAFy604yiK3V1pVXNhFVjWwUABpo9cOl28zA9bYWzQrjT8Nj5x+gn
      N9p52X3cHKTmRrVxJvZEbqAWF9RW1mkC+3upYGPtoEiC7XG9PQuEfTPPAjiLhBxnvpC6+07sYMWw
      ANdegRRdrcHBs1WNbUbLA62eXoO9cSfbRb50+4XK5gSj2fqjPjVyU5cB7Jg81N9ZVsLY3tADG/S0
      ddeZm03r+nupRFbWDpIA25s8umhOjyi9XHnXP58/RqF8Tjv/2I5drUFqQbmRIJBvaDOvRWbcHQDA
      JxyprC9IUyklu+//beKJxvnF47ErkukujrNSHDlliA8qqjV2ffxhF7C2rFPX6z6cP2WAyGqzw87B
      9OJACVluXq7UtS3LJ/VYlsLL1dh+pMIkEVBbjQbzxmcvWriV7UILqNf6y0Th1vePLar7Mryo22CJ
      h/xtrD3IwrLQGBkTMqMfAnioX5BnbtSYcPJGPS0T80t6PISeBRKK+be2LA9/7nvzuZuN+Pns3Qe1
      rcZzJi270pQZ1didL0mS9ohhPr6bc6/9GcDo7vyEIurd+AmB8tPXG7Q6A9u+JHkU+cWmg1feq1Ub
      i3SpEa+/UIVki/LLPk4Y1S1Mg9qIrfnljZX1mgo9rZ2PPXN6/WxLEYS1W3FBlpxl7UsnD/bCvC+P
      8rB3Rns7pN8WubH8tawi7I+7DACI/0mMzDl0j0DypXnvx73k7xHs0/UDS8bxSibv0oMGrZGeh4y4
      EgBAYnacTCZYbzDafgbQeW9wnMxut5MAwFhsbkjO/rCr2CLwopfOGCTcnFNmsNnYonY/O0GDteWC
      Nd9EUs5iENgCDrMBHHweEAEA1Ou5lgMfRfF5VMde1WS24q+ZpTU3azX76R3T1wAAknIHKyS8H/p5
      KvuvjBsqyTx9GxTZ+Z7IccDo/q4Y6OuMnPP3oDF0xcwhyFuFscEeuFPfhtPlDe2aVMynS8obWk1m
      m6WPi9yloqZVrUuN8u1pNfCQmL1wfIg7w6NI/tNCbYsBH+0+W9ums0/Hvuk3HlXl0CyKx+2VifnW
      Vh1j41MEYkf7Y3vBdU2L3kx0TBaQiQVcXzeFqvTuwzadqfOWHh/iKRoV6Cr6uaRKfeJGLUURRPtN
      XyERYs2s4d4ejhKs3Ha6TWdge7eHXBwl06YO7dOhLzMwVny46+wDXWrEoxlJOhzmrBS+06Ixfcqy
      iKpXGwGC4974qigr7e1wpcHMUnVqYxpttj25P9lYVW2rPt3XVYbb9ZoHetq68ukxxALeDm9nmd+O
      ogrtkSvVu8xm+5Pvctw4Pw/lak8nKU6U1RsNtDUHe2ecRG9MtbTwXOp5I5dTyXE5lRz3QynDub9d
      dOuxHvyH02Euy49qvzzRppUtyjvd4cvxh13EC/JMOZUc57/qhAaJuRPbtYQsN9Gbhw1/yG7gHFMK
      O3xPmVJw+L3MGsN7B2qMyqUFeR1iJmS9MfKP52/mVHLcpuMaTrm04F6vQP5tJGNlneinbpV7j1fq
      GtXMB4//vlOvSxkb4iEI9FAqvnhz3Dj+/FwdkvMf3aUzox/SFkvcmp0l+q+XTHRQyHi5eC1r2PMG
      lC3Ky4wZ5Tcp0FMpTc2/0abdGjn9sSZZlLc+qI/jX9cnvBRktbFYtaOE1W6N9HshID6P+OVuQ1v7
      Bw91NA3S3N5jWXdGz29oMazZePCy2cdFjl0rp8olIlQiMXs+ACA97ojJyr68PuNCW8aqCJmbi6wQ
      iblDuxrMdfnR9OUxocNeCfMXb8j8pcaYFvno6Tg+1939nWPbJw3yXrZx4Xj35jYTFm8ubmNpU4+v
      qc8aZe43p6qNtsyJGN5HDAD9vRxkJ8oagy1B8QW4vt8IAHXH0y6o/WYxJRUNY2NH+wnix/UTtOiY
      yXWeM1ewgxLvaLdE5BuC5tgqazUj/7ZgnNOxazXxRoat4PGI2LHBHoJLt5trBCOS42Ne8p8eFuTq
      mvT3I2bd1kgnfPIJKfQb/S1fSGxbGjUw7NWxAbILlU34OONCm1at9ezpzOnKCACQLy74aKif0wdr
      Zo9wAoDSO834/J+lNI+iTuoMzGaQwgvYE9nsvfL4dBtr374wItgjLMgd9Wojvsm52tKooe+1aOki
      sYgiB3g6Lv103mjlqu2n9febdfxVrwwXfZV1Rbtk+mBeiI+D9K0tJ7VWs+1tiYifpJAKg6JG9nF7
      dUyAmLVz2Heikim8/OCyZkvk2BcF6QAEAJ7vHZ/pIhd+Fz28j/cQfxfIRHyUlNfj6r1WbXFZHc9J
      LirXGhibgbFdV8mFIU4yUWhK9CBZkLcK1c06nLnVRJ8oq6eNjMUwop+r27uxocLLdx4iNMAFlXUa
      DPBWYdaGwxYA9pGBrqbwIV6OYwY82ooHSu7gh2M37RKR4EPjtsiN/ylMB6BHa/lfISon8Uo9bU1W
      yUTWQE8HQ4PGJBHzKfZOo1bAcRxLkeR5Zsf0KCRljXOQSXaH+jkpRwa6OE8Y6AWKJKAxMKhrMWKQ
      b6d/zuF+kw6PL4n1aiMKL9cwl+40q3Umc7bm+4hl/xOQroGetsTcvuDsngBHwU7oIbVXYUecvrNf
      VrhMKnxHQFFhSqlAHBbkpnRSiOGukkDApyDh86FjzLDY7LjfpEObwaI9dq2GkokFN+tbjQdhFG3F
      wamt+JXs1/1pTFLWOBDkMIGAHOEoEXoYGJu3iEe2WuycQiEV3KttNtwGUAqSO/tr/CahK/tveWAz
      +cmnv7IAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjItMTAtMDNUMDY6MzE6MTUrMDA6MDAmNN0dAAAA
      JXRFWHRkYXRlOm1vZGlmeQAyMDIyLTEwLTAzVDA2OjMxOjE1KzAwOjAwV2lloQAAACh0RVh0ZGF0
      ZTp0aW1lc3RhbXAAMjAyMi0xMC0wM1QwNjozMToxNSswMDowMAB8RH4AAAAASUVORK5CYII=" />
      
    </svg>
    
  )
}

export default WithIcons(Travel);