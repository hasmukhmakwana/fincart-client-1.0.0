import React from 'react';
import WithIcons from "./withProps";

function Other(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAzCAMAAAANf8AYAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
    AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABU1BMVEUAAAAQZrsabr4ZbL4W
    a70XbL0Zbb4SZ7sQZ7oZbb4Rabsod8MAUrYAYK8oecQabr0SaLoAWLcAV7YTabsZbr4oeMQAXaop
    eMQccMAWa7wWbL0QaLgabr4cb78EXrQYbb4cb74cb78AVcYAVb8AYL8AWLEAW7QAW7MAW7EAWrQA
    WrQAXbIAW7UAWrMnd8MvfccfdMEcdMMufccndsQAXLETaLoTarsTabsBXbMUarsAWrUAXbIMZLgC
    XbQMY7gBW7MBXLMTaboWbL0SabsATbMUarwTZ7sAV7UpeMUvfccbcMEbcsQmd8QAWq8AXLMAXLIA
    XLQAW7MAXLQAW7IAWrIAWbEAXLNnpeCz2v95suZ3sOWy2f9lo95sp+CDuOqEuOpqpd9npN5fn9tp
    puBopN9opd+m0fmkz/mx2P4AW7On0vpqpuBgoNtrp+BdndpnpOD////ppEV3AAAAVnRSTlMAnd7g
    urnfmZzAmeEcIODf9yAj99/gHuCvxsaisNz5w92sCQwIMXOHdkdEeC0l5+lCQOnnJNrl2P32Hx7u
    /PD+/vbb5Qrk2Sbs6kJB6DN1iHdGdIRxLmpQKGMAAAABYktHRHDYAGx0AAAAB3RJTUUH5goDBh4j
    JmfXbwAAAR1JREFUSMftlNk3AmEYh38tVEha7FJCStYkmWxlSdlbRhk7CYX//87MGDff9+rGOY7T
    mefyfc5z8y0vYDCKLCYzvmgzca7dAljPyzwVm5rYLnglXXbAWKbo7JITezfpHBDJeY9Tblxu0ol6
    ozd684vGQ857++TG2U86DwauiPH1oLoPhm4IdzsMjHhHWbw+be/4eOcfU0RgnAPf8GoC/5zJ4BRL
    KKy56QjngjPA7NzdPcvD/IKaLEYfOVddimH5ibq3eEA50DilaitIkHe9KshNco1S0nqz9yZsUOp5
    s7UaaavFmhe9ab5H/6BJCT83CaRfqfn2jvK3dylV20Nmv95gecvm1B1yEK2+s3wcxoCj4xOW0zNt
    V4XzBZZiCZ+mZsctZQOJZAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMi0xMC0wM1QwNjozMDozNSsw
    MDowMIvTsV4AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMTAtMDNUMDY6MzA6MzUrMDA6MDD6jgni
    AAAAKHRFWHRkYXRlOnRpbWVzdGFtcAAyMDIyLTEwLTAzVDA2OjMwOjM1KzAwOjAwrZsoPQAAAABJ
    RU5ErkJggg==" />
        </svg>
    
  )
}

export default WithIcons(Other);