import React from 'react';
import WithIcons from "./withProps";

function Dash(props: any) {
  return (
    <svg {...props}
      xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi bi-dash-lg ${props.className}`}
    viewBox="0 0 16 16">
  <path fillRule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11A.5.5 0 0 1 2 8Z"/>
</svg>
  )
}

export default WithIcons(Dash);