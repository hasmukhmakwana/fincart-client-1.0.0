import React from 'react';
import WithIcons from "./withProps";

function House(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAApCAYAAACY7BHXAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
    AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAI
    eklEQVRo3u2YaVCT1xrH/28WEhKyEQFZVBRBLiiiYl0ARTAkimJHBEUp9t6yuCBa52I7th2Z6369
    raK1SHDpKGrV6oClmoBAkeIyIgWtDrLUDWxlC0lYspHcD0ivQgKUxcsH/58yz3me55zfm7M85xAY
    SkVkCHhMxkYyCUwDDMoGhToJpxfn9BRiF597tEHZJnrdRqNQ6qy59KOV++Ye7imWGCoOq3XXvrNk
    0QTLfZ0t7fkWeNHYirP5j2QypUZalxwQbhz+8ifB08ftWTTD8Q0zlUzCl+klDb8+blyFM4ukpvqk
    DAUIK1pSGeHvauvvYc/otNnxmfBytuLllFQvPqaTlitThS7dAg2Eo4OVBWwtmd2azM3IOhCw6qnf
    wYWJ/NGFpNc//FfETPJ4W45RlwBPB+YoK6ZzgjpTrVeT3HBuYdVgdU8arET8tddW8xjUOxc/W2gS
    pFMu9jyc/0RkxuVRihHxw/vDCsb8oyt7Ha1ZB05uFrAp5L6lpFHJOLVZwPYcZ3WCESXZOSxgONHS
    nCUzxsUnrnqPa6xd3qLG+YIKKNs0RuO3R8zgBnmN2cyJkWYPdCz9XzMf5tEZFNXTNUGTmD5utnRj
    Lg+fNWL3haLqpmbttozbj3d8sWK6rasDr5tfpL8r3dGGPSspo8SLIPV/g+0fzKrL08n6lhsHovwo
    xnYeAMi886TtbH7FE4VY6AYACuD4dkjLVs2bMGrhtDGMrv5z3O2YLnYcjOQx0V/95WnGjZVusOdb
    SNM/DzIJ8lV6Sf3FG1WZCnGg2+t2hVjoev56hSQpo7TOWNxAQP4yDDc26/ikMfxtR+Lm8Uz5xIsL
    moqq6nbXH54fBgCMqKuJtvG5vzGiJF8AQEOyIOR2xct9m1ILmgY08oHAcGIkxUu9nVZsWTaNb6y9
    Tt6GkF1XNI9rmhYrUwK/6oiRZod6OyeI4+aNXTrL6VNObJYEAJRi4b6qP5reD90jUTUqVYMG0/ua
    Cc+wodIoFVuWTWN5OI4w6lJUUYu93xc3aLTEOJwNVsAvj8Icr34Ws2Aie467HQMAlvuOZ9hwzH2O
    tEtqWhyEo5BI5KtCJfbRX+dWfR42nTvFyarncRh6L716hlmZOZ9BJ10Wbwgw5zDNjLqc+7myLf1m
    1V3ViQW+AIAVGVMIavOd//zDj+wwwuINXz8Pe6aTLZu5/kim1hDxw1SkiUo1AO/fZMmNEJ/xnstm
    O5mbGoo1j2EASd8jMdlUAytKsnWsDWt/anwAi25m3G3HuSLZrUe/n1akCEMAgBObtYbPMT91JkHI
    YDOMw3OYNITPdSHySqvDqdMiG1VFJ+9qitOOPRsZNKbsuWysr7udUSAvZ2uLO+W1M7Tuy6EpTis0
    5mP0r+Ovycp4b4LN3HVBHibrkqiDuXJte/s/G5MDjwIAb41U7OpgGbI1zMsSfdTOc0WNj140fS9L
    FsR2fgwmjbI7JW4e11RMUkZp3S+P639u+Gb+0l5hWNGSytXz/2YrnDKaYSzZ01ol4o7kG2AgTcbp
    oPsAYBElubHUe7xnqLfpaWJKFwor2y4VVv3SfFTkDeDVNCUVfbPWj9R1mnbqatHTtrSfyp4rxKIJ
    xmFeVbxfRvuaLBTz7te0iiUPZM2pQgcAQKjE0syivW8LuAcVV9Vi57m7Mo1G74Qzi2QIPU9mctjP
    1wVNYs9xtzN6+JTXNGHLiUJN+2uVNwEA/HXZkXq94dDxjQEmC8Vvr5WprpU++0meIlwAAPjg8lw6
    lSpJWe9Ht2TRMVA1KFWIPZynVmt1QpwKzgcATqw0K3DKaN9If1ejHag17Yg6lKNsatFEIm1xOjF6
    y/UACwr50t4PZ7NNdfTZyVvyypfypNZU0TYAYMVIE6zZ5lsPxPhyB0zRRZvEBU21irZdSrFwHwAw
    oiQ7XGy5cds/mGFy/SYcL1SqdNpggr82u+zYxoAJZCMFXptGh+iDuc3yFnU4TgdnAgB/bfbFyeNG
    +H68xLP/86oX7c8oqSv9rb6gIVkQAgBYeXkJl0U7dTQ+gEWjdt9ZtTo94o5cf0JSa3VnLxZWqrs6
    lD2XIXyfVC1v1k3sBGHHSMvCfJxFQwkCAB8v8bQK83EWsWOkZQCAM8EZTUqtZ9heiaa8RtbN/9LN
    KpWiVXucAABebJbk7wK3OfM87M0B4Mqdp62n88ueK8QiVwDAiquOZJq+bM/qWTRjJfxQqaxahk+/
    vaFpN+hdcSr4MQCwoqXlkQGu9qKpHbtt3r2athPZD6/LUgJFf84tmw05JctmO01+2dT2Iu9e9c2G
    ZMEyAEDojyPZXHL5sY0BLLrZkLx/9Ci1th0fJeUo5QqVK75b8gIA+OuuXRJMdvCz4TJ4Z6+XP6g9
    PH8i0OWcsV6fm6bStd9WpAgOddo4sVk31y6YONPbzfatg3Sq4NcXSJU+LJSlCHw6bewYabyZGWlm
    /deClZ22Nz517WH/iK6JKCRCjWEgMol4496tEAsPAjj4um3QXmeGg4ZsEbTrDWhVa7vZGTQqyAO4
    5791mHtP6pv3p5colK2ablsf09xMlhAyhT1xNN+iP7nfKkyzSotdF4rkLamiUQBh6NquhoHYqc36
    /cQmf4vB3h0Hfc00KFRo1xm0xkA6RBh0unZ1vXzwrstDBvP/1DuY4ap3MMNV72CGq3qFaVHrbj6q
    lvUlFwDgbsVL0GmUez35mNOoxbXy1j7nfFQtQ6tKW9ibX+9FUqKBNKopX9Kq0brLlCoboPOl1EAA
    gOG1Z1MKmaTmscwq6zS8qRB7aU3mDD1PthtpdatW3uKh0xuofw6GwKuDljB0/rRk0f9g0qn3n7Ln
    BiGR0A8MplOBUiYc+R1leLnSAKs6A9xDDUj83wA6AE2d/D2p48MgEQQeXCBQZ0XAhdVhe9Jghixh
    S1+y/BfY7DntAOT+OAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMi0xMC0wM1QwNjozMDoyMyswMDow
    MCSphPoAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMTAtMDNUMDY6MzA6MjMrMDA6MDBV9DxGAAAA
    KHRFWHRkYXRlOnRpbWVzdGFtcAAyMDIyLTEwLTAzVDA2OjMwOjIzKzAwOjAwAuEdmQAAAABJRU5E
    rkJggg==" />
    </svg>
    
  )
}

export default WithIcons(House);