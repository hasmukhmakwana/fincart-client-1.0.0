import React from 'react';
import WithIcons from "./withProps";

function Study(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi ${props.className}`}
      viewBox="0 0 52 51">
    <image id="image0" width="52" height="51" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAA0CAYAAAAnpACSAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
    AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAG
    yklEQVRo3u2ae2wc1RXGvzv7mMe+vPbu+v2KcZzYwQZcJ8VpGzBBsYPd9IFpXkhVoZhWVBVColKr
    FokKqQRVbUNoqwgoCrFjiIQwdowbhbikVCYmj8Z5EDtx7NhZ25v1etezu7MzO7sz/SMmBamBxTPG
    VuVPmr/mnu+c352ro7l3hmAh1PymG2bmEU8aV8cL8XVOGzMTDIvpds58/HoodgQG9hXs2xjQOy3R
    25B7tPs5G0O31K7OtmxYk8Ok2xg4rTSCEQkBXsQ/znnFf12YiAhS4q/CKw2/XrIwtsf+PrT9Wytz
    G9cWcV80trN/RGg/dmmc37tp1ZKDyXzi6OmW+vI7alZmphzz4cUpvHrk45OTu+u+pkcNBj1MXD99
    r62lvuLutWWZ5i8Tl+eywmVj7YMZDRXRj/a9pbUOSjPJw+9scHCmhnVlmdb5hN+9OstqYYyN2NGx
    ftFhnBzz9KObytO0eDxyf4XDaWV/sbgwDd10OCZvXFOYocmmaoULfCzegOY3NS17bTAZytoCj03Q
    5DGnPLc1CjNTs3gwSSW7wG2T9YDJcVoEqEi9FeoOQxF5JiLqwYJZIW4GiKaJ0fpkxilCVD1gTAai
    ANTE4sG0bzlx9mrAIcYTmmyioozzY0Er2h749+LBALCzxtdODfs1eZwe9sPGmv+mtRbNMEEh/ts9
    nQNRLR4vHhqIBWeFZxcdBvuavApRn3v58IXQfML39pwPEeAZHNji01qKLu9m8qnWD0jVdnp4kq+t
    KfUYU4176dBZ6aqPf967u+53etShCwwATH/w6vvTRd/L/nh8Jq/AbbOlWehbjh318djTfXayf8j3
    8sTuOt32NP/dAvyguwREyYQxGYBE+3GwfmZejtu7Njod9AusyVjYtK7QydEmuO0M/LyIqCijq380
    FIsnrgb5+JNoa+ydV47mnnTQkhtJKh2E+NDWeOUmTP6TR39pNhof96RxhssTwTRekIkkJ5ksJzcU
    ERNx2kgNB3ixD6o6CMp0EfsbBlOAqgalbnBamXtZ2qDEpCQVjIi9SCi9aN9y+gvjd3aUQSGrQEiZ
    08bUxhPJEhtjNE+FYqW0ySDaOZN6W44z5A8JSVFW9lz74z27CPujd3fdd0fBz1rqy5lPeymqCl6I
    YyIQhTcQgTcQxfDk7KwvFFP9swJns9D9wbD4BlTDUbRuvqB5jWx7pwKE3Oe00w/xQrzGZeeEbCdL
    SrIdjjyXFbkZFmSnW2DnzKDIZ/eUf+k+J70/cO0PxPZYz+TeJ+qyrIwp5byxeAJnRqZx5so0jg/5
    eF6QCEeb9wej0p/xeuO51AE6Kp1p7E+iYmJHupWmako9lqpiF6qKXWDMKfcRhGNxPP5S7xTBzk61
    8zeNmiY1GJFwYSyA194bjCiqOno9KDyF1qbDtwzYeaje5aB/bzJQBT+sW2UtL0zH5zWMVNT0bBdS
    x/8cOa001pfnYH15jvX0Ff+ag/+8/PbIj3sCEUn+OvY1eW8O3Pp2vsXG9pVk2p0PffM2rqrYpUf6
    m9IF5tO6c4Ubd65ws1ev83l7e84fH9jW2YwDTX3Y0bG+stjT3tJQkVvgtumdFoAebwC3UKHHjvL8
    9FxQuGsu1e2r89PyFgpkQWGAG40CIDc6CwEdiycXMt3CwnzVWoZZqlqGWapahlmqWoZZqlqGWapa
    hlmqWoZZqvr/gslyWMYXuwg9lOlgxykVyqQvpMtnyUXTRCAKqJTX6AtEHnym7fiJh+8p8+S7rGBp
    I+wcDdqk2zG07pLkJHghjlg8gTF/GPt7h/y+SPRBI9q/M+7d0V29p2vgTwBZaTJSdl6QswlB0sHR
    ssvOyLcXZXAOzswUuK0o9NiQYWcXvOBgRMSoL4wxfxjBaFw8Nzodm+Yl42xUpFUQYmeNPjmpBAEM
    RiLxn6N9y8SNo6bWzdciwPc/49bUyQVUKS8QjhYOXgu6QVDtsjNVUVGuJhRlri3LihZn2d2VRRko
    yrRrLn7Ex2NgZBqjU7y/b8hnSSSVhI01n5yeFc6AoB8K5YeRjEE1efH6pmjwf3jc+tyss0kAMDR3
    AUDb9Cf3mg9lHTkxXklbDN81ENLA0kZH7aps9t7KXLo0Jy1lgCFvCL1nvVLfxSlBlBKzCUV5V5KU
    t0AZz2P/5knpS07I/A4BDz4wBWBKAg4DgLC1I6ezf2Trqcu+RoPBUL1tQ6l9XVkWZiJSGCR5EQCg
    JC+FolJYTiRtHw760H7sEp9UlBNef6QLsuHAnKcm6f7zHLZ3ruUspqcdjPkuC2M+cPmFb/zqk1sl
    Tx17XpATzeGYdDIiyLvQ+u2PdM//1UrVfwLn9B9zIKLwnJw7TAAAACV0RVh0ZGF0ZTpjcmVhdGUA
    MjAyMi0xMC0wM1QwNjoyOTo1MSswMDowMIRoaMAAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMTAt
    MDNUMDY6Mjk6NTErMDA6MDD1NdB8AAAAKHRFWHRkYXRlOnRpbWVzdGFtcAAyMDIyLTEwLTAzVDA2
    OjI5OjUxKzAwOjAwoiDxowAAAABJRU5ErkJggg==" />
    </svg>
    
  )
}

export default WithIcons(Study);