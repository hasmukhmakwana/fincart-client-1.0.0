import React from "react";
import WithIcons from "./withProps";
const ChevronDoubleRight = (props: any) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi bi-chevron-double-right ${props.className}`}

      viewBox="0 0 28 28"
    >
      {/* <path
        fillRule="evenodd"
        d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z"
      />
      <path
        fillRule="evenodd"
        d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z"
      /> */}
      <path id="ic_chevron_Background_Mask_" data-name="ic_chevron (Background/Mask)" d="M0,0H28V28H0Z" fill="none" />
      <path id="background" d="M14,0A14,14,0,1,1,0,14,14,14,0,0,1,14,0Z" fill="#fff" />
      <path id="Vector" d="M0,10,5,5,0,0" transform="translate(16 9)" fill="none" stroke="#6418c3" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke-dasharray="0 0" />
      <path id="Vector-2" data-name="Vector" d="M0,10,5,5,0,0" transform="translate(9 9)" fill="none" stroke="#c2c2c2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke-dasharray="0 0" />
    </svg>
  );
};

export default WithIcons(ChevronDoubleRight);
