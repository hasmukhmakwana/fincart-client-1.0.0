import { getLS } from "@ui/DataGrid/utils";
import {
  MESSAGES,
  MENU_PREFIX,
  ENC_SECRET_KEY,
  USER_DETAILS,
  TOKEN_PREFIX,
} from "./constants";
import HttpStatusCode from "./httpStatus";
import CryptoJS from "crypto-js";

/** @get_logged_in_user_details */
export const getUser = () => {
  let user = getLS(USER_DETAILS);
  if (!user) return null;
  return user;
  // return decryptData(user)?.data || null;
};

export const getMessageFromStatus = (
  status: HttpStatusCode,
  msg: string | null
) => {
  switch (status) {
    case 401:
      return msg || MESSAGES.INVALID_LOGIN;
    case 409:
      return msg || MESSAGES.ALREADY_EXIST;
    case 500:
      return msg || MESSAGES.SERVER_ERROR;
    default:
      return MESSAGES.SERVER_ERROR;
  }
};

//format err
export const formatError = (err: any) => {
  let val: any = { msg: "", status: null, field: null };
  if (typeof err?.data == "string") {
    val.msg = getMessageFromStatus(err?.status, err?.data);
    val.status = err.status;
  } else if (err?.data?.err?.errors) {
    //format sequelize error
    let e = err?.data?.err?.errors;

    val = [];
    for (let iterator of e) {
      val.push({
        msg: iterator.message,
        field: iterator.path,
        status: err.status,
      });
    }
  } else if (err?.data?.required) {
    //format express validator error
    let e = err?.data?.required.errors;

    val = [];
    for (let iterator of e) {
      val.push({
        msg: iterator.msg,
        field: iterator.param,
        status: err.status,
      });
    }
  } else {
    val.msg = getMessageFromStatus(err?.status, null)
      ? getMessageFromStatus(err?.status, null)
      : err.data.msg;
    val.status = err?.status;
  }
  return val;
};

// for formate date
export const dateFormater = (dateValue: Date) => {
  return new Date(dateValue).toLocaleDateString();
};

export const formatDate = (date: any, format: string = "") => {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  if (format == "m-d-y") {
    return [month, year, day].join("-");
  } else if (format == "d-m-y") {
    return [day, month, year].join("-");
  } else if (format == "d-m") {
    return [day, month].join("-");
  } else if (format == "d/m/y") {
    return [day, month, year].join("/");
  } else if (format == "m/d/y") {
    return [month, day, year].join("/");
  } else {
    return [year, month, day].join("-");
  }
};

// generate unique number
export const getUniqueNumber = (prefix: string | null) => {
  var date = new Date();
  var components = [
    prefix,
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
    date.getSeconds(),
    date.getMilliseconds(),
  ];

  return components.join("");
};

// checking object is empty or not
export const isEmptyObj = (obj: any): boolean => {
  return Object.keys(obj).length === 0;
};

/**
 *
 * @ENCRYPTION_DECRYPTION
 * //* algorithm = "AES/CBC/PKCS5PADDING"
 */
export const encryptData = (data: any, noString = false) => {
  if (!data) return null;
  try {
    const stringified: string = noString ? data : JSON.stringify(data);
    // const stringified: any = CryptoJS.lib.WordArray.create(data);
    const key = CryptoJS.enc.Utf8.parse(ENC_SECRET_KEY);
    const iv = CryptoJS.lib.WordArray.create();
    return CryptoJS.AES.encrypt(stringified, key, { iv }).toString();
  } catch (e) {
    console.log(e);
  }
};

export const decryptData = (cipherText: any) => {
  console.log();

  if (!cipherText) return null;
  try {
    const key = CryptoJS.enc.Utf8.parse(ENC_SECRET_KEY);
    const iv = CryptoJS.lib.WordArray.create();
    return JSON.parse(
      CryptoJS.AES.decrypt(cipherText, key, { iv }).toString(CryptoJS.enc.Utf8)
    );
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const getArryOfDMY = (date: any): number[] => {
  if (!date) return [];

  const d = new Date(date);
  const day = d.getDate();
  const month = d.getMonth() + 1;
  const year = d.getFullYear();
  return [day, month, year];
};

export const compareDates = (
  date1: any,
  date2: any,
  check: "lt" | "gt" | "lte" | "gte" | "eq"
): boolean | null => {
  if (!date1 || !date2) return null;

  const d1 = new Date(date1);
  const d2 = new Date(date2);

  switch (check) {
    case "lt":
      return d1 < d2;
      break;

    case "gt":
      return d1 > d2;
      break;

    case "lte":
      return d1 <= d2;
      break;

    case "gte":
      return d1 >= d2;
      break;

    case "eq":
      return +d1 === +d2;
      break;

    default:
      return null;
  }
};

export const convertBase64 = (file: any) => {
  if (!file) return null;
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onload = () => {
      resolve(fileReader.result);
    };
    fileReader.onerror = (error) => {
      reject(error);
    };
  });
};

export const openTab = (url: string, self = false) => {
  const link = document.createElement("a");
  link.href = url;
  if (!self) link.target = "_blank";
  document.body.appendChild(link);
  link.click();
  link.remove();
};

export const downloadFile = (dataurl: string, filename: string) => {
  const link = document.createElement("a");
  link.href = dataurl;
  link.download = filename;
  link.click();
};

export const checkSessionTimer = () => {
  // *****CHECK TOKEN EXPIRY***** //
  const TOKEN: any = getLS(TOKEN_PREFIX) || null;
  if (!TOKEN) return { status: false, remainingTime: 0, TOKEN };
  const current: number = new Date().getTime();
  const iat: number = TOKEN?.local_iat || 0;
  const remainingTime: number = iat - current;
  if (remainingTime <= 60) {
    return { status: false, remainingTime, TOKEN };
  }
  return { status: true, remainingTime, TOKEN };
};

/**
 * @GET_ARRA_FROM_KEY_VALUE_PAIR
 * @param data {[],[]}
 * @param key ""
 * @returns []
 */
export const getArrayFromKey = (data: any[], key: string) => {
  let result: any[] = [];
  if (!data || !key) return result;

  const arr = data.filter((i) => i.Key === key);

  return arr?.[0]?.Value || [];
};

export const plainBase64 = (base64File: string) => {
  if (!base64File) return null;
  return base64File.split("base64,")[1];
};

export const calculateAge = (
  birthdate: string
): { years: number; months: number; days: number } => {
  const now: Date = new Date();

  const yearNow: number = now.getFullYear();
  const monthNow: number = now.getMonth();
  const dateNow: number = now.getDate();

  const dob: Date = new Date(birthdate);

  const yearDob: number = dob.getFullYear();
  const monthDob: number = dob.getMonth();
  const dateDob: number = dob.getDate();

  let years: number = yearNow - yearDob;
  let months: number;

  if (monthNow >= monthDob) {
    months = monthNow - monthDob;
  } else {
    years--;
    months = 12 + monthNow - monthDob;
  }

  let days: number;
  if (dateNow >= dateDob) {
    days = dateNow - dateDob;
  } else {
    months--;
    days = 31 + dateNow - dateDob;

    if (months < 0) {
      months = 11;
      years--;
    }
  }

  return { years, months, days };
};

export const isNumeric = (value: string) => {
  if (value == undefined) return false;

  return !isNaN(+value);
}

export const Capitalize = (strValue: string) => {
  if (strValue == undefined) return false;

  return strValue.charAt(0).toUpperCase() + strValue.slice(1);
}