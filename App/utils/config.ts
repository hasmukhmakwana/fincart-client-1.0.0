let configData: any = {
  development: {
    ApiUrl: "https://uat-api-fincart.azurewebsites.net/api/v2",
    WebUrl: "http://localhost:3000"
  },
  test: {
    ApiUrl: "https://uat-api-fincart.azurewebsites.net/api/v2",
    WebUrl: "http://localhost:3000"
  },
  production: {
    ApiUrl: "https://uat-api-fincart.azurewebsites.net/api/v2",
    WebUrl: "https://uat-app-fincart.azurewebsites.net"
  },
  live: {
    ApiUrl: "https://api2.fincart.com/api/v2",
    WebUrl: "https://uat-app-fincart.azurewebsites.net"
  }
};

export default function (env: any) {
  return configData[env];
}
