import axios, { AxiosResponse } from "axios";
import {
  CLIENT_ID,
  CLIENT_SECRET_KEY,
  NODE_API_URL,
  TOKEN_PREFIX,
  WEB_URL,
} from "./constants";
import {
  checkSessionTimer,
  decryptData,
  encryptData,
  formatError,
} from "./helpers";
import qs from "qs";
import { setLS } from "@ui/DataGrid/utils";

let originalConfig: any = null;

const redirectLogin = () => {
  if (typeof window !== "undefined") {
    window.location.href = WEB_URL + "/Login";
  }
};

let originalRequest = async (token: string) => {
  if (originalConfig) {
    try {
      const result: any = await axios({
        method: originalConfig.method,
        url: originalConfig.baseURL + originalConfig.url,
        data: originalConfig.body,
        headers: {
          ...originalConfig.headers,
          Authorization: token,
        },
      });
      console.log("originalRequest RESULT: ", result);
      if (result?.data) {
        if (typeof result?.data === "string") {
          const res = decryptData(result.data);

          if (res?.status === "Error") {
            return Promise.reject(res);
          }
          return Promise.resolve(res);
        } else if (result?.data?.status === "Error") {
          return Promise.reject(result.data);
        }
        return Promise.resolve(result.data);
      }
    } catch (error) {
      return Promise.reject(formatError(error));
    }
  }
};

//refresh token
export const refreshToken = async () => {
  console.count("*****************GET REFRESH TOKEN*******************");
  const sessionData: any = checkSessionTimer() || {};
  console.log({ sessionData, TOKEN: sessionData?.TOKEN?.access_token });

  if (!sessionData?.TOKEN?.access_token) {
    return Promise.reject({
      msg: "No access_token",
      status: false,
    });
  }
  try {
    const queryParams = {
      Payload: encryptData({
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET_KEY,
      }),
      refresh_token: sessionData?.TOKEN?.refresh_token,
      grant_type: "refresh_token",
    };
    // console.log(queryParams);

    const result = await axios({
      method: "post",
      url: NODE_API_URL + "/fintoken",
      data: qs.stringify(queryParams),
      headers: {
        "content-type": "application/x-www-form-urlencoded",
      },
    });

    // console.log("TOKEN DATA ===", result);
    if (result?.data?.access_token) {
      setLS(TOKEN_PREFIX, {
        ...result.data,
        local_iat: new Date().getTime() + result.data.expires_in * 1000,
      });
      return Promise.resolve(result.data.access_token);
    } else {
      console.log({ "REFRESH TOKEN API": "No Token from refreshToken api" });
      redirectLogin();
      return null;
    }
  } catch (error: any) {
    console.log({ "REFRESH TOKEN API": error });
    redirectLogin();
    return null;
  }
};

const fetchClient = () => {
  const defaultOptions = {
    baseURL: NODE_API_URL,
    headers: {
      "Content-Type": "application/json",
    },
  };

  let instance = axios.create(defaultOptions);

  instance.interceptors.request.use(async (request: any) => {
    try {
      if (request.headers?.NoAuth === "Y") {
        request.headers.Authorization = request.headers.Authorization || "";
      } else {
        const sessionData: any = checkSessionTimer() || {};
        if (sessionData?.status) {
          request.headers.Authorization = sessionData?.TOKEN?.access_token
            ? `${sessionData?.TOKEN?.token_type} ${sessionData?.TOKEN?.access_token}`
            : "";
        } else {
          request.headers.Authorization = await refreshToken();
        }
      }

      originalConfig = request;
    } catch (error) {
      console.log(JSON.stringify(error));
      /**
       * TODO:: redirect to login
       */
      return null;
    }
    return request;
  });

  instance.interceptors.response.use(
    (response: AxiosResponse) => {
      // console.log("Check Response: ", JSON.stringify(response));

      if (typeof response?.data === "string") {
        const result = decryptData(response?.data);
        if (result?.status === "Error") {
          return Promise.reject(result);
        }
        return result;
      } else if (response?.data?.status === "Error") {
        return Promise.reject(response.data);
      }
      return response;
    },
    async (err: any) => {
      console.log(err.response);
      if (
        err?.response?.status === 401 &&
        err?.response?.statusText === "Unauthorized"
      ) {
        try {
          const tokenRes = await refreshToken();
          return await originalRequest(tokenRes);
        } catch (error: any) {
          console.log(
            "ERROR: REFRESH TOKEN IN RESPONSE:: ",
            JSON.stringify(error)
          );
          /**
           * TODO:: redirect to login
           */
          return Promise.reject(formatError(error));
        }
      }
      return Promise.reject(formatError(err.response));
    }
  );

  return instance;
};

export default fetchClient();
