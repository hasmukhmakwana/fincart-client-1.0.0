import environment from "App/environment";
import configFunc from "./config";
const config = configFunc(environment);

export const NODE_API_URL = config.ApiUrl;
export const WEB_URL = config.WebUrl;

export const TOKEN_PREFIX = `${NODE_API_URL}fincart__`;
export const PERM_PREFIX = `fincart__PERM__`;
export const MENU_PREFIX = `fincart__MENU__`;
export const MANDATE_TXN_ID = `fincart__MANDATE_TXN_ID__`;

export const TOKEN_S = "@_FINCART_JWT@@$$$$";

export const MOTILAL_PMS_PARTNER_ID = "73";


export const ENC_SECRET_KEY = "p2s5v8y/B?E(H+MbQeShVmYq3t6w9z$C";
export const CLIENT_SECRET_KEY = "264e3285b0934defa5067bfb4bc9d682";
export const CLIENT_ID = "fincartApp";

export const STATUS_VALUES = [
  { value: "Active", id: "1" },
  { value: "Inactive", id: "0" },
];

export const INITIAL_PAGE = 1;
export const INITIAL_LIMIT = 10;

export const MESSAGES = {
  INVALID_LOGIN: "Invalid credentials, Please try again.",
  ALREADY_EXIST: "Name already exist !",
  SERVER_ERROR: "Something went wrong !",
};

export const USER_DETAILS = `fincart__USER_DETAILS__`;
export const GOAL_DETAILS = "USER_GOAL_DETAILS";


export const MANDATE_TYPES = {
  Physical: "Physical",
  Emandate: "E-Mandate",
};

export const PAN_NO_REGEX = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
export const AGE_REGEX = /^([0-9]){2}?$/;

export type GoalsSections = "GOAL_PLANNING" | "TAX_SAVING" | "QUICK_SIP";

export const NRI_PROFILE_TYPE_VALUE = "24";
export const NRI_PROFILE_TYPE_TEXT = "NON-RESIDENT INDIAN (NRI)";