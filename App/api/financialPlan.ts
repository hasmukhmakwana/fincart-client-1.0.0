import api from "App/utils/api";

//Create Plan
export const createNewPlan = (body: string) => {
  return api.post("/fullfinplan/create", body);
};

export const getDropdownDataAll = () => {
  return api.get("/fullfinplan/MasterDropDownAll");
};

export const getDropDownInsurance = () => {
  return api.get("/fullfinplan/MasterDropDownInsurance");
};

export const getDropdownDataOT = () => {
  return api.get("/fullfinplan/MasterDropDownOT");
};

export const getDropdownDataLN = () => {
  return api.get("/fullfinplan/MasterDropDownLN");
};

export const getRecommendedMemberList = (checksum: string) => {
  return api.get("/fullfinplan/Recommended_Member_List", {
    headers: { checksum },
  });
};

//get Basic Details
export const getBasicDetails = (checksum: string) => {
  return api.get("/fullfinplan/getBasicDetails", {
    headers: { checksum },
  });
};

//save basic details
export const saveBasicDetails = (body: string) => {
  return api.post("/fullfinplan/saveBasicDetails", body);
};

// all goal scheme
export const getAllGoalScheme = (checksum: string) => {
  return api.get("/Goal/getAllGoalSchemeUAT", {
    headers: { checksum },
  });
};

//delete member from saved data
export const deleteMembers = (body: string) => {
  return api.post("/fullfinplan/DeleteMemberBasicDetail", body);
};

//fetch Plan id
export const getFPPlanId = (checksum: string) => {
  return api.get("/fullfinplan/getplanid", {
    headers: { checksum },
  });
};

// Get Income (Financial Plan)
export const fetchIncomeDetails = (profileId: string) => {
  return api.get("/fullfinplan/getAllCashFlowIn", {
    headers: { checksum: profileId },
  });
};

//Goal Api
export const fetchGoalDropDown = (checksum: string) => {
  return api.get("/Goal/Goal_DropDown_By_BasicID", {
    headers: { checksum },
  });
};

//create Income
export const createIncome = (body: string) => {
  return api.post("/fullfinplan/saveCashFlowInEntry", body);
}

// Delete Income (Financial Plan)
export const deleteIncome = (body: string) => {
  return api.post("/fullfinplan/DeleteCashFlowIn", body);
}

// Get Expenses (Financial Plan)
export const fetchExpenseDetails = (checksum: string) => {
  return api.get("/fullfinplan/getAllExpenseFlow", {
    headers: { checksum },
  });
};

//fetch goal scheme UAT
export const fetchAllGoalSchemeUAT = (checksum: string) => {
  return api.get("/Goal/getAllGoalSchemeUAT", {
    headers: { checksum },
  });
};
// fetch goal inflation
export const fetchGoalInflationAndRor = (checksum: string) => {
  return api.get("/fullfinplan/GetGoalInflationAndRor", {
    headers: { checksum },
  });
};

//calulate goals
export const goalCalculate = (body: string) => {
  return api.post("/Goal/Calculate", body);
};

///Save Goals
export const SaveGoal = (body: string) => {
  return api.post("/Goal/Save_Single_Goal", body);
};
//Edit Goals
export const EditGoal = (body: string) => {
  return api.post("/Goal/Edit_Single_Goal", body);
};
///Fetch Goals
export const fetchGoalDetails = (checksum: string) => {
  return api.get("/Goal/Goal_Details", {
    headers: { checksum },
  });
};
// Create Expenses (Financial Plan)
export const createExpenses = (body: string) => {
  return api.post("/fullfinplan/SaveCashFlowOutEntry", body);
}

// Delete Expenses (Financial Plan)
export const deleteExpense = (body: string) => {
  return api.post("/fullfinplan/DeleteExpenseFlow", body);
}

// Get Other Assets (Financial Plan)
export const fetchAssetsDetails = (checksum: string) => {
  return api.get("/fullfinplan/GetAllNetAssetEntry", {
    headers: { checksum },
  });
};

//Post Add Insurance
export const createInsurance = (body: string) => {
  return api.post("/fullfinplan/SaveNetAssetEntry", body);
}

///
export const createOtherAsset = (body: string) => {
  return api.post("/fullfinplan/SaveNetAssetEntry", body);
}

// Get Insurance Policy name Search
export const insurancePolicySearch = (checksum: string) => {
  return api.get("/fullfinplan/Master/InsuranceTypeHeadSearch", {
    headers: { checksum },
  });
};

// delete selected/by_id goal
export const deleteGoal = (checksum: string) => {
  return api.get("/Goal/Delete_Goal", {
    headers: { checksum },
  });
};

// Delete Other Asset (Financial Plan)
export const deleteOtherAsset = (body: string) => {
  return api.post("/fullfinplan/DeleteNetAssetEntry", body);
}

//seach other code by passing text
export const searchOtherGoal = (checksum: string) => {
  return api.get("/Goal/Other_Goal_DropDown", {
    headers: { checksum },
  });
};

export const mutualFundSchemeSearchFP = (checksum: string) => {
  return api.get("/fullfinplan/Master/Scheme_List/Search_By_Text", {
    headers: { checksum },
  });
};

// Get Mutual Fund (Financial Plan)
export const fetchMutualFundDetails = (profileId: string) => {
  return api.get("/fullfinplan/GetAllMFLumpsumSipExists", {
    headers: { checksum: profileId },
  });
};

//Upload File API Mutual Fund
export const uploadFilePDF = (body: string) => {
  return api.post("/document/userCasPdfUpload", body);
}

//Get Insurance Un-(Financial Plan)
export const getUploadCASHistory = (checksum: string) => {
  return api.get("/document/userCasPdfHistory", {
    headers: { checksum },
  });
};

//Get Insurance Un-(Financial Plan)
export const getInsuranceUnmapped = (checksum: string) => {
  return api.get("/fullfinplan/Running_Insurances", {
    headers: { checksum },
  });
};

//Get MF Un-Mapped(Financial Plan)
export const getInsuranceMutualFund = (checksum: string) => {
  return api.get("/fullfinplan/GetAllMFLumpsumSipExists", {
    headers: { checksum },
  });
};

//Get MF Un-Mapped(Financial Plan)
export const getOtherAssetUnmapped = (checksum: string) => {
  return api.get("/fullfinplan/Running_Loan_And_Other_Asset", {
    headers: { checksum },
  });
};

//Get MF Un-Mapped(Financial Plan)
export const getLoanUnmapped = (checksum: string) => {
  return api.get("/fullfinplan/Running_Loan_And_Other_Asset", {
    headers: { checksum },
  });
};
export const otherAssetSearch = (checksum: string) => {
  return api.get("/fullfinplan/Master/TypeHeadSearch", {
    headers: { checksum },
  });
};

// Delete Net Asset (Financial Plan)
export const deleteMutualFund = (body: string) => {
  return api.post("/fullfinplan/DeleteNetAssetEntry", body);
}
// Get Asset Loan name Search
export const assetNameSearch = (checksum: string) => {
  return api.get("/fullfinplan/Master/TypeHeadSearch", {
    headers: { checksum },
  });
};

export const submitALLDetailsPlan = (checksum: string) => {
  return api.get("/fullfinplan/UpdatePlanFilledStatusByClient", {
    headers: { checksum },
  });
};

// UnMapped MF Saving List
export const UnmappedMFSaving = (body: string) => {
  return api.post("/fullfinplan/SaveCommittedSavingsList", body);
}

