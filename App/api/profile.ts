import api from "App/utils/api";

// profile member list
export const getProfileMembers = (checksum: string) => {
  return api.get("/KYC_CAF/profiles", {
    headers: { checksum },
  });
};

// kyc sync
export const callKycSync = (checksum: string) => {
  return api.get("/caf/karvykycstatusWithDBSync", {
    headers: { checksum },
  });
};

// generate otp for email and mobile
export const generateOTP_email_mobile = (body: string) => {
  return api.post("/KYC_CAF/generateOtp/emailMobile", body);
};

// validate otp for email and mobile
export const validateOTP_email_mobile = (body: string) => {
  return api.put("/KYC_CAF/validateOtp/emailMobile", body);
};

// update general details
export const updateGeneralDetails = (body: string) => {
  return api.post("/KYC_CAF/Basic_Details_Edit", body);
};

// update nominee details
export const updateNomineeDetails = (body: string) => {
  return api.post("/KYC_CAF/Nominee_Step", body);
};

// fetch bank details
export const fetchBankDetails = (ifsc: string) => {
  return api.get("/KYC_CAF/fetchBank", {
    headers: { checksum: ifsc },
  });
};

// change bank request
export const changeBankRequest = (body: string) => {
  return api.post("/KYC_CAF/Change_Bank_Request", body);
};

// update pan card
export const updatePAN = (pan: string) => {
  return api.post("/KYC_CAF/Update_PAN", pan);
};

// get address details based on pincode
export const fetchPincodeDetails = (pincode: string) => {
  return api.get("/data/pincodedetails", {
    headers: { checksum: pincode },
  });
};

// change address request
export const changeAddressRequest = (body: string) => {
  return api.post("/KYC_CAF/Change_Address_Request", body);
};

// update signature
export const updateSign = (sign: string) => {
  return api.post("/KYC_CAF/Update_Sign", sign);
};

// update FATCA
export const updateFatca = (body: string) => {
  return api.post("/KYC_CAF/Fatca_Step", body);
};

// change request list by type
export const changeRequestList = (body: string) => {
  return api.post("/KYC_CAF/Change_Request_List", body);
};

//View Profile by passing body
export const viewInvestmentProfile = (body: string) => {
  return api.post("/user/View_Profile", body);
};

//get Mandate by pass profile ID
export const mandateByProfileID = (profileId: string) => {
  return api.get("/user/ViewMandate_By_ProfileID", {
    headers: { checksum: profileId },
  });
};

//get Transaction by pass profile ID
export const mandateByTransactionID = (profileId: string) => {
  return api.get("/user/View_Transactions_By_ProfileID", {
    headers: { checksum: profileId },
  });
};

//add and update by pass body in investment
export const modifyInvestmentProfile = (body: string) => {
  return api.post("/user/Modify_Profile", body);
};

//Delete Investment profile
export const deleteInvestmentProfile = (body: string) => {
  return api.post("/user/Delete_Profile", body);
};

//Chnage profile pic
export const changeProfilePic = (body: string) => {
  return api.post("/document/saveprofilepic", body);
};

export const resetKYC = (basicId: string) => {
  return api.get("/KYC_CAF/Reset", {
    headers: { checksum: basicId },
  });
}