import api from "App/utils/api";

//Get SIP Calculator
export const getSIPCalculatorData = (body: string) => {
    return api.post("/sipcalc", body);
  };

  //Get Lumpsum Calculator
export const getLumpsumCalculatorData = (body: string) => {
    return api.post("/lumpsumcalc", body);
  };


   //Get Retirement Calculator
export const getRetirementCalculatorData = (body: string) => {
  return api.post("/retirementcalc", body);
};