import api from "App/utils/api";

//get Drop Down Data data
export const getInvestorDD = (checksum: string) => {
  return api.get("/invest/allMemberPurchaseDetails/", {
    headers: { checksum },
  });
};

//get Drop Down Data data
export const getInvestorsNonMF = (checksum: string) => {
  return api.get("/insurance/allmembers", {
    headers: { checksum },
  });
};

//get Drop Down Data data
export const getUpcomingTransactionsDD = (checksum: string) => {
  return api.get("/user/upcomingtxnmaster", {
    headers: { checksum },
  });
};

export const getUpcomingTransactionsData = () => {
  return api.get("/user/dashboardNotifications");
};

export const fetchSysTxnBunch = (body: any) => {
  return api.post("/user/systxnbunch", body);
};

///user/systxnserieswise
export const fetchSysTxnSeriesWise = (body: any) => {
  return api.post("/user/systxnserieswise", body);
};

export const deleteTxnOtp = (body: any) => {
  return api.post("/user/systxndelete/sendotp", body);
};

export const deleteTxnValidateotp = (body: any) => {
  return api.post("/user/systxndelete/validateotp", body);
};

export const deleteTxn = (body: any) => {
  return api.post("/user/systxndelete", body);
};