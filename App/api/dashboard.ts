import api from "App/utils/api";
import { NODE_API_URL } from "App/utils/constants";
import { checkSessionTimer } from "App/utils/helpers";
import axios from "axios";

//overall portfolio data
export const getOverallPortfolioData = (body: string) => {
  // return api.post("/portfolio/getOverallPortfolioData_UAT", body);
  return api.post("/portfolio/getOverallPortfolioData", body);
};

// get goal wise portfolio data
export const getGoalWisePortfolioData = () => {
  return api.get("/portfolio/getGoalwisePortfolioData");
};

export const getGoalWisePortfolioDetail = (body: string) => {
  return api.post("/portfolio/getGoalwisePortfolioDetail", body);
};

// get upcoming Transaction
export const upcomingTransaction = (checksum: string) => {
  return api.get("/user/Upcoming", {
    headers: { checksum },
  });
};

// get recent Transaction
export const recentTransaction = (checksum: string) => {
  return api.get("/user/Recent_Transactions", {
    headers: { checksum },
  });
};

// get AMC Investments Data
export const allAMCInvestments = (body: string) => {
  return api.post("/user/AllAmcInvestments", body);
}

// get Asset Allocations
export const assetAllocations = (checksum: string) => {
  return api.get("/user/AssetAllocation", {
    headers: { checksum },
  });
};

// get Asset Allocations All
export const assetAllocationsAll = (checksum: string) => {
  return api.get("/user/AssetAllocationAll", {
    headers: { checksum },
  });
};

// get Sub Category Asset Allocations
export const assetAllocationsSubCategory = (checksum: string) => {
  return api.get("/user/AssetAllocationSubCategory", {
    headers: { checksum },
  });
};

// growth Chart Data
export const growthChart = (body: string) => {
  return api.post("/user/GrowthChart", body);
}

// get Maturing Assets
export const maturingAssets = (checksum: string) => {
  return api.get("/user/MaturingAssets_v2", {
    headers: { checksum },
  });
};

// get Product wise Mandate 
export const productWiseMandate = (checksum: string) => {
  return api.get("/user/ViewProduct_wise_mandate", {
    headers: { checksum },
  });
};

// getPolicyDetails
export const fetchPolicyDetailsChart = (body: string) => {
  return api.post("/portfolio/getPolicyDetails", body);
}


// get All Member details
export const getAllMember = (checksum: string) => {
  return api.get("/insurance/allmembers", {
    headers: { checksum }
  });
}

//get PrePaid Details
export const getPrePaidDetails = (checksum: string) => {
  return api.get("/portfolio/getPrePaidDetails", {
    headers: { checksum },
  });
};

//get Fund Details
export const getFundDetails = (checksum: string) => {
  return api.get("/portfolio/getFundDetails", {
    headers: { checksum },
  });
};

//get Nominee Details
export const getNomineeDetails = (checksum: string) => {
  return api.get("/portfolio/getNomineeDetails", {
    headers: { checksum },
  });
};

//get PMS Performance Details
export const getMotilalPMSDetails = (body: string) => {
  return api.post("/Other_Asset/Moti_PMS/GetAccountPerformance", body);
};