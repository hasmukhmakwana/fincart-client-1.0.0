
export type signupBody = {
  Name: string;
  Userid: string;
  password: string;
  ClientStatus: string;
  Mobile: string;
  trackerCode: string;
};

export type loginBody = {
  Payload: string;
  grant_type: string;
};

export type fpBody = {
  email: string;
};