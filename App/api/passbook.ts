import api from "App/utils/api";

// Passbook Scheme's list
export const searchPassbookTransactions = (body: string) => {
    return api.post("/invest/passbook_v2", body);
}

//get Drop Down Data data
export const getInvestorDD = (checksum: string) => {
    return api.get("/insurance/allmembers", {
        headers: { checksum },
    });
};

//get Overall Report
export const generateToken = (checksum: string) => {
    return api.get("/portfolio/reportToken", {
        headers: { checksum },
    });
};