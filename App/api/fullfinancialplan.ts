import api from "App/utils/api";

//get planAssumption data
export const getPlanAssumption = (checksum: string) => {
  return api.get("/fullfinplan/GetPlanAssumption", {
    headers: { checksum },
  });
};

//get My Dream data
export const getMyDreamDetails = (checksum: string) => {
  return api.get("/fullfinplan/getFullViewPlanGenerate", {
    headers: { checksum },
  });
};

//get Summary data
export const getSummaryDetails = (checksum: string) => {
  return api.get("/fullfinplan/GetWayForwardPlanDetails", {
    headers: { checksum },
  });
};

//get Scheme Selection data
export const getSchemeDetails = (checksum: string) => {
  return api.get("/fullfinplan/GetSelectedSchemes", {
    headers: { checksum },
  });
};

// Passbook Scheme's list
export const retirementChartData = (body: string) => {
  return api.post("/retirementcalc", body);
}
