import api from "App/utils/api";

// Get ContactUs Subjects (Contact Us)
export const fetchGetContactUsSubjects = () => {
    return api.get("/user/GetContactUsSubjects" );
};

//Post Add ContactUs
export const SaveContactDetails = (body: string) => {
    return api.post("/user/SaveContactDetails", body);
}

//View Contact List
export const fetchViewContactList = (checksum: string) => {
    return api.get("/user/ViewContactList" , {
        headers: { checksum },
      });
};
