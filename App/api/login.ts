import api from "App/utils/api";
import { loginBody, fpBody } from ".";
import qs from "qs";
import axios from "axios";
import { NODE_API_URL } from "App/utils/constants";

//customer login
export const login = (body: loginBody) => {
  return axios({
    method: "post",
    url: NODE_API_URL + "/fintoken",
    data: qs.stringify(body),
    headers: {
      "content-type": "application/x-www-form-urlencoded",
    },
  });
};

// forgot password
export const forgotPassword = (checksum: string) => {
  return api.get("/user/forgotpassword", {
    headers: { NoAuth: "Y", checksum },
  });
};

// forgot password OTP Validate
export const validate_FP_OTP = (data: string) => {
  return api.put("/user/validateOTP/forgotPassword", data, {
    headers: {
      NoAuth: "Y",
    },
  });
};

// get user loggedIn details
export const getUserDetails = (access_token: string) => {
  // return axios({
  //   method: "get",
  //   url: NODE_API_URL + "/user/loggedinDetails",
  //   headers: {
  //     "content-type": "application/x-www-form-urlencoded",
  //     Authorization: `bearer ${access_token}`,
  //   },
  // });

  return api.get("/user/loggedinDetails", {
    headers: {
      "content-type": "application/x-www-form-urlencoded",
      NoAuth: "Y",
      Authorization: `bearer ${access_token}`,
    },
  });
};

export const getLoginDetails = (checksum: string) => {
  return api.get("/data/decrypt", {
    headers: { NoAuth: "Y", checksum },
  });
}