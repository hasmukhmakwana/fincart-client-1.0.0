import api from "App/utils/api";

export const fetchInvestUAT = () => {
    // return api.get("/invest/Master_uat");
    return api.get("/invest/Master_v2");
};

export const searchSchemes = (body: string) => {
    return api.post("/invest/searchSchemes", body);
};

export const fetchFundList = (checksum: string) => {
    return api.get("/invest/fundlist",
        {
            headers: { checksum }
        });
};

export const getSLInvestorDD = (checksum: string) => {
    return api.get("/invest/allMemberPurchaseDetails/", {
        headers: { checksum },
    });
};
