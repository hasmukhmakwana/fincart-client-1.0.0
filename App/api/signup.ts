import api from "App/utils/api";

//user sign-up
export const signup = (body: string) => {
  return api.post("/user/generateOtp", body, {
    headers: {
      NoAuth: "Y",
    },
  });
};

//user email and mobile verification with OTP
export const verifyOTP = (body: string) => {
  return api.put("/user/validateOtp", body, {
    headers: {
      NoAuth: "Y",
    },
  });
};
