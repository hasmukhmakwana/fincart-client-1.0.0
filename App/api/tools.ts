import api from "App/utils/api";

// get/fetch quickSips 
export const fetchQuickSips = (profileId: string) => {
  return api.get("/goal/fetch/quicksips", {
    headers: { checksum: profileId },
  });
};

// Get Fin Calculator
export const getFinCalculatorData = (body: string) => {
  return api.post("/fincalc", body);
};

// Create Goal  
export const createGoal = (body: string) => {
  return api.post("/goal/create", body);
};
// Get Tax Saving 
export const fetchTaxSavings = (profileId: string) => {
  return api.get("/goal/taxSavingGoal", {
    headers: { checksum: profileId },
  });
};

// POST Feedback 
export const userFeedback = (body: string) => {
  return api.post("/user/userFeedback", body);
};

// GET Feedback 
export const userFeedbackOptions = () => {
  return api.get("/user/feedbackLOV");
};

//Get All Unmapped Investments 
export const allUnmappedInvestments = (checksum: string) => {
  return api.get("/user/AllUnmappedInvestments", {
    headers: { checksum },
  });
};

//Post Map UnMapped Investment
export const mapUnMappedInvestment = (body: string) => {
  return api.post("/user/MapUnMappedInvestment", body);
};