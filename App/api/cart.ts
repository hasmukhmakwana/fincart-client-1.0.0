import api from "App/utils/api";

 // Add to Cart
 export const addingTocart = (body: string)=>{
    return api.post("/invest/addtocart", body);
}
 // Add to Systematic Cart
 export const addingToSystematicCart = (body: string)=>{
    return api.post("/invest/InsertToSystCart", body);
}

// Remove From Purchase cart
export const removingFromCart = (cartId: string) => {
    return api.put("/invest/removecart", {}, { headers: { checksum: cartId } });
  
};

// Remove From Transfer Cart
export const removingFromSysCart = (id: string) => {
  return api.get("/invest/DeleteFromSystCart", {
    headers: {
      checksum: id,
    },
  });
};
// Bank Payment Modes
export const getBankPaymentModes = (bankid: string) => {
    return api.get("/invest/bankPaymentModes", {
      headers: {
        checksum: bankid,
      },
    });
  };

   // Purchase Attempt
 export const purchaseAttempt = (body: string)=>{
    return api.post("/invest/purchaseAttempt", body);
}

// Cart Details
export const getCartDetails = (body: string) => {
    return api.get("/user/cartdetails", {
      headers: {
        checksum: body,
      },
    });
  };

// Cart Details
export const getSystematicCartDetails = (body: string) => {
    return api.get("/invest/ViewSystCart", {
      headers: {
        checksum: body,
      },
    });
  };

// Bill Desk Post String
export const getBillDeskPostString = (body: string) => {
    return api.get("/invest/BilldeskPostString", {
      headers: {
        checksum: body,
      },
    });
};

// Bill Desk Payment Status
export const getBillDeskPaymentStatus = (body: string) => {
    return api.get("/invest/billdskPaymentStatus", {
      headers: {
        checksum: body,
      },
    });
};

  // Send OTP
  export const sysSendOTP = (body: string)=>{
    return api.post("/user/OTP/send/systcart", body);
}

// Validate OTP
export const sysValidateOTP = (body: string) => {
  return api.get("/user/OTP/validate/systcart", {
    headers: {
      checksum: body,
    },
  });
};

