import api from "App/utils/api";
export const getNotificationData = () => {
    return api.get("/user/dashboardNotifications");
  };
  