import api from "App/utils/api";
import { NODE_API_URL } from "App/utils/constants";
import { checkSessionTimer } from "App/utils/helpers";
import axios from "axios";

// check kyc status
export const checkKYCStatus = (checksum: string) => {
  return api.get("/caf/Checkkycstatus", {
    headers: { checksum },
  });
};

//generate OTP for non-kyc PAN
export const generateOTP = (body: string) => {
  return api.post("/KYC_CAF/generateOtp", body);
};

//validate OTP for non-kyc PAN
export const validateOTP = (body: string) => {
  return api.put("/KYC_CAF/validateOtp", body);
};

//account creation
export const accountCreation = (body: string) => {
  return api.post("/KYC_CAF/Account_Creation", body);
};

// get the KYC_CAF_Master data
export const getKYC_CAF_Master = () => {
  return api.get("/KYC_CAF/KYC_CAF_Master");
};

// add personal info
export const addPersonalInfo = (body: string) => {
  return api.post("/KYC_CAF/Update_POI", body);
};

// update personal info - first API
export const updatePersonalInfo = (body: string) => {
  return api.post("/KYC_CAF/Update_KYC_Form", body);
};

// update personal info - second API - bcoz got an error in first API
export const updatePersonalInfo_bk = (data: string) => {
  // return axios({
  //   method: "post",
  //   url: NODE_API_URL + "/KYC_CAF/Update_KYC_Form",
  //   data,
  //   headers: {
  //     Authorization: `bearer ${checkSessionTimer()?.TOKEN?.access_token}`,
  //     "Content-Type": "application/json",
  //   },
  // });

  const requestOptions = {
    method: "POST",
    headers: { Authorization: `bearer ${checkSessionTimer()?.TOKEN?.access_token}`, "Content-Type": "application/json" },
    body: "CjEGwZg6KXVkTFc0mpbK5er0PVrBELg6f+OEBu2d76h6BgSEQeaCyxp3FZTADkT5ged4++pv5Z1t42CcelBQUeZoTZCYbM882MxFdwj0aCWSZ5CZE7mSLO1ExkspfUG0cu6HQA8fhP/MHe39NGtVKoPud+K4YAtd+Kx5sdEwkply89pc/Y1kIp4aD/vEGZ+eWwKSsCpWA9SWn8SR9+slTOxVywk6dIYO2KRlfTdXYtu9I57Qb3T3/OVTXwm85h7ScHmEuQRimfK6qhhqymO5AORV+RiOqpREzE1lFasq5Fms/ZXb/rjVF9nBwkGDdk51e3Rxvk0kxBhjH5SXDaSi7e/HP+NX0+3uyuVed73fyIecDww6SOWD8mmkmLgVcURLxTF2IR/bh3XyZ03p5zUq2dmVrYjcpd64L28iP1qqMnFfFoBGb2kcqH8dejlTpKLo68cHYYwkGHEY600O2ZDZ7cIXTBI8Vcw3Lo05WTQWMwE0edi7OWk2JMp+8VM0mRn8H9eEty1IHEfO73rr9bjBHQ==",
  };
  return fetch(NODE_API_URL + "/KYC_CAF/Update_KYC_Form", requestOptions)
};

// upload the cancelled cheque
export const uploadCancelledCheque = (cancelledCheque: any) => {
  return api.post("/KYC_CAF/Cancel_Cheque_Upload", cancelledCheque);
};

// penny transfer - posting banking details
export const pennyTransfer = (banking: string) => {
  return api.post("/KYC_CAF/Penny_Transfer", banking);
};

// varifing the banking details
export const verifyBanking = (banking: string) => {
  return api.post("/KYC_CAF/Verify_Bank_Account", banking);
};

// upload the proof of identity(pancard)
export const uploadPOI = (body: string) => {
  return api.post("/KYC_CAF/Upload_POI", body);
};

// update proof of identity(pancard) details
export const updatePOI = (POI: string) => {
  return api.post("/KYC_CAF/Update_POI", POI);
};

// upload the proof of address(aadharcard)
export const uploadPOA = (body: string) => {
  // return api.post("/KYC_CAF/Upload_POA_UAT", body);
  return api.post("/KYC_CAF/Upload_POA", body);
};

// update proof of address(aadharcard) details
export const updatePOA = (POA: string) => {
  // return api.post("/KYC_CAF/Update_POA_UAT", POA);
  return api.post("/KYC_CAF/Update_POA", POA);
};

// get the OTP for video recording
export const getVideoOTP = (checksum: string) => {
  return api.get("/KYC_CAF/Video_Start", { headers: { checksum } });
};

// upload the captured photo
export const uploadPhoto = (photo: string) => {
  return api.post("/KYC_CAF/Photo_Upload", photo);
};

// upload the recorded video
export const uploadVideo = (video: string) => {
  return api.post("/KYC_CAF/Upload_Video", video);
};

// upload the sign
export const uploadSign = (body: string) => {
  return api.post("/KYC_CAF/Upload_Sign", body);
};

// e-aadhaar-sign
export const createEAadhaarSign = (checksum: string) => {
  return api.get("/KYC_CAF/Create_PDF_EAadharSign", {
    headers: {
      checksum,
    },
  });
};

// reset all steps
export const resetAllSteps = (checksum: string) => {
  return api.get("/KYC_CAF/Reset", {
    headers: {
      checksum: "gfXhdKaRFXH/GxZock0kdQ==",
      // checksum,
    },
  });
};

// confirm KYC details for non-kyc user
export const confirmKYC = (checksum: string) => {
  return api.get("/KYC_CAF/Confirm_KYC", {
    headers: {
      // checksum: "gfXhdKaRFXH/GxZock0kdQ==",
      checksum,
    },
  });
};


// confirm KYC details for kyc user
export const confirmCAF = (checksum: string) => {
  return api.get("/KYC_CAF/confirm_caf", {
    headers: {
      // checksum: "gfXhdKaRFXH/GxZock0kdQ==",
      checksum,
    },
  });
};


// get summary data
export const getSummary = (checksum: string) => {
  return api.get("/KYC_CAF/Summary", {
    headers: {
      checksum,
    },
  });
};
//get the Bank Data based on IFSC Code
export const getKYC_CAF_Bank = (checksum: string) => {
  return api.get("/KYC_CAF/fetchBank", {
    headers: {
      checksum,
    },
  });
};

///Location Check
export const getCurrentLocation = () => {
  return fetch("https://ipapi.co/json", {
  }).then((response) => {
    return response?.text();
  });
}