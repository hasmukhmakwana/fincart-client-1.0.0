import api from "App/utils/api";

// Recommended schemes list
export const getFAQs = (checksum: string) => {
    return api.get("/user/FAQ",
        {
            headers: { checksum }
        });
}

export const getFAQMaster = () => {
    return api.get("/user/FAQCategories");
}