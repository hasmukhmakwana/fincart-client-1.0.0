import api from "App/utils/api";

//add new mandate - physical
export const addPhysicalMandate = (body: string) => {
  return api.post("/user/Create_Mandate", body);
};

//add new mandate - e-mandate
export const addEMandate = (body: string) => {
  return api.post("/emandate/saveandredirect", body);
};

//list
export const getMandateList = (checksum: string) => {
  return api.get("/user/ViewMandate_By_BasicID", {
    headers: { checksum },
  });
};

//bank list
export const getBankList = (checksum: string) => {
  return api.get("/emandate/getbank", {
    headers: { checksum },
  });
};

//mandate master api
export const getMandateMaster = () => {
  return api.get("/user/Mandate_Master");
};

//mandate master api
export const getAllMemberPurchaseDetails = (data: string) => {
  return api.get("/invest/allMemberPurchaseDetails", {
    headers: {
      checksum: data,
    },
  });
};

//transaction list for the single mandate
export const getTransactionList = (mandateId: string) => {
  return api.get("/user/View_Transactions_By_Mandate", {
    headers: {
      checksum: mandateId,
    },
  });
};

//update mandate signature
export const updateMandateSign = (data: string) => {
  return api.post("/KYC_CAF/Update_Sign", data);
};

//delete mandate
export const deleteMandate = (mandateId: string) => {
  return api.get("/user/Delete_Mandate", {
    headers: {
      checksum: mandateId,
    },
  });
};

//download mandate
export const downloadMandate = (basicid: string) => {
  return api.get("/user/Mandate_Download_Token", {
    headers: {
      checksum: basicid,
    },
  });
};

//check the status of mandate
export const checkMandateStatus = (mandateID: string) => {
  return api.put("/emandate/update", {}, { headers: { checksum: mandateID } });
};

//mandate Bank
export const getMandateBankList = () => {
  return api.get("/emandate/NPCI_Bank");
};

