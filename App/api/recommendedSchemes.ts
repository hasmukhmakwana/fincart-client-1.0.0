import api from "App/utils/api";

// Recommended schemes list
export const getRecommendedSchemes = (body: string) => {
    return api.post("/invest/recommendedSchemes", body);
}

export const getFullSummary = () => {
    return api.get("/user/fullSummary");
};

export const fetchAllInvestments = (body: string) => {
    return api.post("/user/AllInvestments", body);
}