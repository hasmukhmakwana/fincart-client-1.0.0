import api from "App/utils/api";

// get All Goal Investment
export const getAllGoalInvestment = (checksum: string) => {
  return api.get("/goal/AllGoalInvestments", {
    headers: { checksum },
  });
};

// create Goal
export const createGoal = (body: string) => {
  return api.post("/goal/create", body);
}

// Update Goal
export const updateGoal = (body: string) => {
  return api.put("/goal/update", body);
}
// Delete Goal
export const deleteGoal = (checksum: string) => {
  return api.delete("/goal/delete", {
    headers: { checksum },
  });
}
// fetch all goal data
export const fetchAllGoalData = () => {
  return api.get("/goal/fetch/all");
};

// get Goal Name Code List
export const getGoalNameCodeList = () => {
  return api.get("/goal/goalNameCodeList");
};

// fetch goal Data
export const fetchGoalData = (checksum: string) => {
  return api.get("/goal/fetch", {
    headers: { checksum },
  });
};

// Calculate Cost
export const calculateCost = (body: string) => {
  return api.post("/fincalc", body);
}