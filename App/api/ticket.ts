import api from "App/utils/api";

// GET Ticket Category
export const fetchTicketCategory = () => {
  return api.get("/ticket/Ticket_Category");
};

//Get Subject list
export const fetchSubjectlist = (checksum: string) => {
  return api.get("/ticket/subjectlist", {
    headers: { checksum },
  });
};

/// Post Created Ticket
export const saveCreateTicket = (body: string) => {
  return api.post("/ticket/CreateTicket", body);
};

/// get Ticket List
export const getTicketList = (body: string) => {
  return api.post("/ticket/GetTicketList", body);
};

//get Comment for ticket
export const fetchCommentForTicket = (checksum: string) => {
  return api.get("/ticket/CommentForTicket", {
    headers: { checksum },
  });
};

//get Ticket Attachments
export const fetchTicketAttachments = (checksum: string) => {
  return api.get("/ticket/GetTicketAttachments", {
    headers: { checksum },
  });
};

//get Subject Points
export const fetchSubjectSubPoints = (checksum: string) => {
  return api.get("/ticket/Subject_SubPoints", {
    headers: { checksum },
  });
};