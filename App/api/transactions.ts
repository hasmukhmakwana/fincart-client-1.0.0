import api from "App/utils/api";

// scheme Valid Dates
 export const schemeValidDates = (body: string)=>{
    return api.post("/invest/SchemeValidDates", body);
}

// send User Transaction OTP
export const sendOTP = (body: string)=>{
    return api.post("/user/OTP/send/usertransaction", body);
}

// validate User Transaction OTP
export const validateOTP = (checksum: string) => {
    return api.get("/user/OTP/validate/usertransaction", {
      headers: { checksum },
    });
  };

  // search Schemes
export const searchSchemes = (body: string)=>{
  return api.post("/invest/searchSchemes", body);
}

// Fund List
export const getFundList = (checksum: string) => {
  return api.get("/invest/fundlist", {
    headers: { checksum },
  });
};

 // Transactions
 export const investTransaction = (body: string)=>{
  return api.post("/invest/transaction", body);
}
