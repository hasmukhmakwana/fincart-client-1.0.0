import React, { useEffect, useState } from 'react'
import Box from "@ui/Box/Box";
import Card from "@ui/Card";
import styles from "../../Support.module.scss";
import { Button } from "@ui/Button/Button";
import Input from "@ui/Input";
import Search from 'App/icons/Search';
import { GroupBox } from "@ui/Group/Group.styles";
import Link from "next/link";
import parse from "html-react-parser";

import {
    Accordion,
    AccordionContent,
    AccordionItem,
    AccordionTrigger,
} from "@ui/Accordian/Accordian";
import Text from "@ui/Text/Text";
import DownArrow from "App/icons/DownArrow";
import Cart from "App/modules/CartModule/Cart";
import { encryptData, getUser } from 'App/utils/helpers';
import { FAQFOR } from '../FAQTypes';
import useFAQStore from '../store';
import { getFAQMaster, getFAQs } from 'App/api/faq';
import { getLS } from '@ui/DataGrid/utils';
import Loader from 'App/shared/components/Loader';

// const user: any = getUser();

let faqList = [
    {
        value: "1",
        title: "What is my current KYC status?",
        content: "Your KYC has been completed and verified. Your account is investment ready.",
    },
    {
        value: "2",
        title: "How to i change my name in KYC?",
        content: "Your KYC has been completed and verified. Your account is investment ready.",
    },
    {
        value: "3",
        title: "I want to change my bank account",
        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    },
    {
        value: "4",
        title: "Is my KYC data safe? Will it be shared with anyone?",
        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    },
    {
        value: "5",
        title: "Is it necessary to do KYC of every investment?",
        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    },
];

const FAQPage = () => {
    const user: any = getUser();
    const [show, setShow] = useState(false);
    const [loader, setLoader] = useState<boolean>(false);
    const [currentSection, setCurrentSection] = useState<FAQFOR>();

    const {
        faqCollection,
        setFAQCollection,
        faqMaster,
        setFAQMaster
    } = useFAQStore();

    const fetchFAQs = async (ID: number) => {
        try {
            setLoader(true);

            const encrypted_id: any = encryptData(ID, false);

            const result: any = await getFAQs(encrypted_id);

            if (result != undefined) {
                setFAQCollection(result?.data);
            }

            setLoader(false);
        }
        catch {
        }
    }

    const fetchFAQCategories = async () => {
        try {
            setLoader(true);

            const result: any = await getFAQMaster();

            if (result != undefined) {
                setFAQMaster(result?.data);

                console.log(faqMaster);

            }
            setLoader(false);
        }
        catch {
        }
    }

    useEffect(() => {
        fetchFAQCategories();
    }, []);

    return (
        <>
            {!show ? <>
                <Box>
                    <Box className="row text-center d-flex justify-content-start">
                        {faqMaster?.map((record: any) => {
                            return (<Box className="col-sm-12 col-md-4">
                                <Card className={`border-1 ${styles.cardBox} p-4`} css={{ cursor: "pointer" }}
                                    onClick={() => {
                                        setCurrentSection(record?.Heading);
                                        fetchFAQs(record?.ID);
                                        setShow(true);
                                    }}
                                >
                                    <Box>
                                        <img className='img-fluid'
                                            src={record?.ImagePath}
                                            alt={record?.Heading}
                                        />
                                    </Box>
                                    {record?.Heading}
                                </Card>
                            </Box>
                            )
                        })}
                    </Box>
                    <Box className="row text-end mt-4">
                        <Box className="col-sm-12 col-md-12 col-lg-12"><Text size="h5">Your issue is not listed here? <span style={{ textDecorationLine: "underline", textDecorationColor: "var(--colors-yellow)" }}> <Link href='/Support/RaiseTicket'>Please Raise a Ticket</Link></span></Text></Box>
                    </Box>
                </Box>
            </> : <>
                {loader ? <Loader></Loader> :
                    <Box>
                        <Card className="mt-0" css={{ p: 15 }}>
                            <Box className="row mb-2">
                                <Box className="col-12 col-sm-10 text-start">
                                    <Text
                                        size="h3"
                                        //@ts-ignore
                                        color="gray8"
                                    >
                                        {currentSection}
                                    </Text>
                                </Box>
                                <Box className="col-12 col-sm-2 text-end">
                                    <GroupBox className="justify-content-end">
                                        <Button
                                            className="p-2"
                                            color="yellowGroup"
                                            onClick={() => {
                                                setShow(!show);
                                            }}>
                                            <Text
                                                size="h6"
                                                //@ts-ignore
                                                color="gray8"
                                            >
                                                Back
                                            </Text>
                                        </Button>
                                    </GroupBox>
                                </Box>
                            </Box>
                            <Box className="row">
                                <Box className="col">
                                    <Accordion type="single" defaultValue="1" collapsible>
                                        {faqCollection?.map((item, index) => {
                                            console.log(item);
                                            return (
                                                <AccordionItem value={item?.Question}>
                                                    <AccordionTrigger css={{ borderRadius: "8px 8px 0px 0px" }} className={`${styles.accordienBordreHeader}`}>
                                                        <Box>
                                                            <Text size="h4">{item?.Question}</Text>
                                                        </Box>
                                                        <Box className="action">
                                                            <DownArrow />
                                                        </Box>
                                                    </AccordionTrigger>
                                                    <AccordionContent className={`${styles.accordienBordre} mb-1 p-3 pt-2`}>
                                                        <Box className="row">
                                                            <Box className="col-md-12 col-sm-12 col-lg-12">
                                                                <Text>{parse(item?.Answer)}</Text>
                                                            </Box>
                                                        </Box>
                                                    </AccordionContent>
                                                </AccordionItem>
                                            );
                                        })}
                                    </Accordion>
                                </Box>
                            </Box>
                        </Card>
                    </Box>
                }
            </>
            }
        </>
    )

}

export default FAQPage