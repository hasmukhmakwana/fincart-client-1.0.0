import create from "zustand";
import { FAQCategories, FAQList } from "./FAQTypes";

interface FAQStoreTypes {
    faqCollection: FAQList[];
    faqMaster: FAQCategories[];
    setFAQCollection: (payload: FAQList[]) => void;
    setFAQMaster: (payload: FAQCategories[]) => void;
}

const useFAQStore = create<FAQStoreTypes>((set) => ({
    faqCollection: [],
    faqMaster: [],

    setFAQCollection: (payload) =>
        set((state) => ({
            ...state,
            faqCollection: payload,
        })),

    setFAQMaster: (payload) =>
        set((state) => ({
            ...state,
            faqMaster: payload,
        })),
}));

export default useFAQStore;