export type FAQFOR = "TRANSACTIONS" | "ACCOUNT" | "REGISTRATION" | "MANDATE" | "OTHER";

export type FAQList = {
    QID: number;
    Question: string;
    Answer: string;
    Type: string;
}

export type FAQCategories = {
    ID: number;
    Heading: string;
    SubHeading: string;
    ImagePath: string;
}