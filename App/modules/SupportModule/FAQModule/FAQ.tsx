import Box from "@ui/Box/Box";
import Layout from "App/shared/components/Layout";
import Card from "@ui/Card";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import FAQPage from "./components/FAQPage";
import Cart from "App/modules/CartModule/Cart";

let faqList = [
  {
    value: "1",
    title: "What is my current KYC status?",
    content: "Your KYC has been completed and verified. Your account is investment ready.",
  },
  {
    value: "2",
    title: "How to i change my name in KYC?",
    content: "Your KYC has been completed and verified. Your account is investment ready.",
  },
  {
    value: "3",
    title: "I want to change my bank account",
    content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  },
  {
    value: "4",
    title: "Is my KYC data safe? Will it be shared with anyone?",
    content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  },
  {
    value: "5",
    title: "Is it necessary to do KYC of every investment?",
    content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  },
];

const FAQModule = () => {
  return (
    <>
      <Layout>
        <PageHeader title="Frequently Asked Questions" rightContent={<></>} />
        <FAQPage />
      </Layout>
    </>
  );
};

export default FAQModule;
