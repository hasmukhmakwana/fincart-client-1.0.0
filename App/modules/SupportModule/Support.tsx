import Layout from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
const SupportModule = () => {
  return (
    <Layout>
      <PageHeader title="Support" rightContent={<></>} />
    </Layout>
  );
};

export default SupportModule;
