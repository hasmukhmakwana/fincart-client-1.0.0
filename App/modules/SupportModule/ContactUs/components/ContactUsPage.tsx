import React, { useEffect, useState } from 'react'
import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
import DialogModal from '@ui/ModalDialog/ModalDialog';
import Input from "@ui/Input";
import Textarea from "@ui/Textarea";
import { Button } from "@ui/Button/Button";
import SelectMenu from "@ui/Select/Select";
import PhoneSquare from "App/icons/PhoneSquare";
import EmailIcon from "App/icons/EmailIcon";
import AddressIcon from "App/icons/AddressBook";
import { FontSizeIcon } from '@radix-ui/react-icons';
import styles from "../../Support.module.scss";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { fetchGetContactUsSubjects, SaveContactDetails, fetchViewContactList } from 'App/api/contactUs';
import { toastAlert } from 'App/shared/components/Layout';
import { encryptData, getUser } from "App/utils/helpers";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import useContactUsStore from '../store';
import Loader from 'App/shared/components/Loader';
import Pinterest from 'App/icons/Pinterest';
import Instagram from 'App/icons/Instagram';
import Facebook from 'App/icons/Facebook';
import Twitter from 'App/icons/Twitter';
import Youtube from 'App/icons/Youtube';
import Link from 'next/link';
import Linkedin from 'App/icons/Linkedin';

const ContactValidation = {
    SubjectId: Yup.string()
        .required("Subject is required"),

    Description: Yup.string()
        .required("Description is required")
        .trim(),
};

// export type ContactUsTypes = {
//     SubjectId: string;
//     Description: string;
// };

// interface StoreTypes {
//     ContactUsVal: ContactUsTypes;
// }
// const useRegistrationStore = create<StoreTypes>((set) => ({
//     //* initial state
//     ContactUsVal: {
//         SubjectId: "",
//         Description: "",
//     }
// }));

const ContactUsPage = () => {
    const [subjects, setSubjects] = useState<any>([]);
    const [contactList, setContactList] = useState<any>([]);
    const [loading, setLoader] = useState(false);
    const [open, setOpen] = useState(false);
    const { contactUsVal, setContactUsVal } = useContactUsStore();
    const user: any = getUser();
    const fetchSubject = async () => {
        try {
            setLoader(true)
            let result = await fetchGetContactUsSubjects();
            //console.log(result?.data);
            setSubjects(result?.data);
            setLoader(false)
        } catch (error) {
            console.log(error);
            setLoader(false)
            toastAlert("error", error);
        }
    }

    const submitReq = async (data: any) => {
        //console.log(data, "data")
        try {
            setLoader(true)
            let obj: any = {
                basicId: user?.basicid,
                Description: data?.Description,
                subjectId: data?.SubjectId
            }
            //console.log(obj);
            const enc: any = encryptData(obj);
            await SaveContactDetails(enc);
            setContactUsVal({
                SubjectId: "",
                Description: "",
            })
            toastAlert("success", "Successfully Submitted!!");
            setLoader(false)
        } catch (error) {
            console.log(error);
            setLoader(false)
            toastAlert("error", error);
        }
    }

    const ViewRequest = async () => {
        try {
            setLoader(true);
            const enc: any = encryptData(user?.basicid, true);
            let result = await fetchViewContactList(enc);
            // console.log(result?.data);
            setContactList(result?.data);
            setLoader(false);
        } catch (error) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error);
        }
    }

    useEffect(() => {
        fetchSubject();
        return () => { }
    }, [])


    return (
        <Box className="row">
            <Box className="col-md-12 col-sm-12 col-lg-8 mx-auto">
                <Formik
                    initialValues={contactUsVal}
                    validationSchema={Yup.object().shape(ContactValidation)}
                    enableReinitialize
                    onSubmit={(values, { resetForm }) => {
                        // console.log(values, "values");
                        submitReq(values);
                        resetForm();
                        setContactUsVal({
                            SubjectId: "",
                            Description: "",
                        })
                    }
                    }
                >
                    {({
                        values,
                        handleReset,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                        errors,
                        touched,
                        setFieldValue,
                        submitCount
                    }) => (

                        <>

                            <Card css={{ p: 25 }} >
                                <Box className="container">
                                    {/* My Ticket UI START */}
                                    <Box className="row">
                                        <Box className="col-md-12 col-sm-12 col-lg-12">
                                            <SelectMenu
                                                name='SubjectId'
                                                label="Subject"
                                                disabled={loading}
                                                items={subjects}
                                                value={values?.SubjectId}
                                                onChange={(e: any) => { setFieldValue("SubjectId", e?.Value); console.log(e) }}
                                                bindValue={"Value"}
                                                bindName={"Name"}
                                                error={submitCount ? errors.SubjectId : null}
                                            />
                                        </Box>
                                        {/* <Box className="col-md-12 col-sm-12 col-lg-6">
                        <Input label="Other" />
                    </Box> */}
                                    </Box>
                                    <Box className="row">
                                        <Textarea
                                            type="textarea"
                                            name="Description"
                                            label="Description"
                                            disabled={loading}
                                            value={values?.Description}
                                            placeholder="Type Description"
                                            rows={5}
                                            css={{ border: "1px solid #cccccc" }}
                                            onChange={(e: any) => {
                                                setFieldValue("Description", e?.target?.value)
                                            }}
                                            error={submitCount ? errors.Description : null}
                                        />
                                    </Box>
                                    <Box className="row justify-content-between">
                                        <Box className="col-md-6 col-sm-6 col-lg-6 text-start">
                                            <Box className="mt-1">
                                                <Button
                                                    className={`col-auto mb-2 py-1`}
                                                    color="yellowGroup"
                                                    disabled={loading}
                                                    size="md"
                                                    onClick={() => { ViewRequest(); setOpen(true) }}
                                                >
                                                    <Box>
                                                        <Text
                                                            //@ts-ignore
                                                            size="h6"
                                                            //@ts-ignore
                                                            color="yellowGroup"
                                                        >
                                                            {loading ? "loading.." : "View History"}
                                                        </Text>
                                                    </Box>
                                                </Button>
                                            </Box>
                                        </Box>
                                        <Box className="col-md-6 col-sm-6 col-lg-6 text-end">
                                            <Box className="mt-1">
                                                <Button
                                                    className={`col-auto mb-2 py-1`}
                                                    color="yellowGroup"
                                                    disabled={loading}
                                                    size="md"
                                                    onClick={handleSubmit}
                                                >
                                                    <Box>
                                                        <Text
                                                            //@ts-ignore
                                                            size="h6"
                                                            //@ts-ignore
                                                            color="yellowGroup"
                                                        >
                                                            {loading ? "Submitting.." : "Submit"}
                                                        </Text>
                                                    </Box>
                                                </Button>
                                            </Box>
                                        </Box>
                                    </Box>

                                </Box>
                                <Box className="row">
                                    <Card css={{ p: 25, backgroundColor: "#b3daff" }}>
                                        <Box className="row">
                                            <Box className="col-md-12 col-sm-12 col-lg-4">
                                                <Text size="h5" color="primary">
                                                    Address<span className={`${styles.managerDetails}`}> <AddressIcon /></span>
                                                </Text>
                                                <Text size="h6">
                                                    Second Floor, A-4 Sector-9,
                                                    NCPL Web Tower, Noida, Gautam Buddha Nagar,
                                                    Uttar Pradesh,
                                                    <br />Pincode: 201301
                                                </Text>
                                            </Box>
                                            <Box className="col-md-12 col-sm-12 col-lg-4 ps-5">
                                                <Text size="h5" color="primary">
                                                    Email Us<span className={`${styles.managerDetails}`}> <EmailIcon></EmailIcon></span>
                                                </Text>
                                                <Text size="h6" css={{ cursor: "pointer", color: "#fff" }}>
                                                    <a href='mailto:operations@fincart.com' style={{ color: "#000" }}>operations@fincart.com</a>
                                                </Text>
                                            </Box>
                                            <Box className="col-md-12 col-sm-12 col-lg-4 ps-5">
                                                <Text size="h5" color="primary">
                                                    Contact Us<span className={`${styles.managerDetails}`}><PhoneSquare></PhoneSquare></span>
                                                </Text>
                                                <Text size="h6" css={{ cursor: "pointer" }}>
                                                    <a href="tel:+917247075470" style={{ color: "#000" }}> + (91) 7247075470</a>
                                                </Text>
                                            </Box>
                                        </Box>
                                    </Card>
                                </Box>
                                <Box className="row">
                                    <Box className="col-12 text-end">
                                        <a href="https://www.facebook.com/fincartinvestmentplanners" target="_blank" className={`${styles.managerDetails}`}><Facebook /></a>
                                        <a href="https://www.instagram.com/fincart_financial_planner/" target="_blank" className={`${styles.managerDetails}`}><Instagram /></a>
                                        <a href="https://www.linkedin.com/authwall?trk=bf&trkInfo=AQGoKXiMkLbDYwAAAYYC-fiwiPY_H9yjOkAacuKsMEeoBdFaRwZMF3EVmEbX7y_pEa8rASSaGyJUDSRTJ_AK2BXMd0ruGwgstM51ATQteEe7XVIZnnMXulYPkHDG6kwI8AjbbDw=&original_referer=&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Ffincartfinancialplanners%2F" target="_blank" className={`${styles.managerDetails}`}><Linkedin /></a>
                                        <a href="https://twitter.com/fincart_advisor" target="_blank" className={`${styles.managerDetails}`}><Twitter /></a>
                                        <a href="https://www.youtube.com/channel/UCPgKy0xW3ysR4aAJkwh0oEg" target="_blank" className={`${styles.managerDetails}`}><Youtube /></a>
                                        <a href="https://in.pinterest.com/fincartinvestmentplanners/" target="_blank" className={`${styles.managerDetails}`}><Pinterest /></a>
                                    </Box>

                                    {/* <Box className="col-6"></Box>
                                    <Box className="col text-end border">
                                        <a href="https://www.facebook.com/fincartinvestmentplanners" target="_blank" className={`${styles.managerDetails}`}><Facebook /></a>
                                    </Box>
                                    <Box className="col text-end border">
                                        <a href="https://www.instagram.com/fincart_financial_planner/" target="_blank" className={`${styles.managerDetails}`}><Instagram /></a>
                                    </Box>
                                    <Box className="col text-end border">
                                        <a href="https://www.linkedin.com/authwall?trk=bf&trkInfo=AQGoKXiMkLbDYwAAAYYC-fiwiPY_H9yjOkAacuKsMEeoBdFaRwZMF3EVmEbX7y_pEa8rASSaGyJUDSRTJ_AK2BXMd0ruGwgstM51ATQteEe7XVIZnnMXulYPkHDG6kwI8AjbbDw=&original_referer=&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Ffincartfinancialplanners%2F" target="_blank" className={`${styles.managerDetails}`}><Linkedin /></a>
                                    </Box>
                                    <Box className="col-1 text-end">
                                        <a href="https://twitter.com/fincart_advisor" target="_blank" className={`${styles.managerDetails}`}><Twitter /></a>
                                    </Box>
                                    <Box className="col-1 text-end">
                                        <a href="https://www.youtube.com/channel/UCPgKy0xW3ysR4aAJkwh0oEg" target="_blank" className={`${styles.managerDetails}`}><Youtube /></a>
                                    </Box>
                                    <Box className="col-1 text-end">
                                        <a href="https://in.pinterest.com/fincartinvestmentplanners/" target="_blank" className={`${styles.managerDetails}`}><Pinterest /></a>
                                    </Box> */}







                                </Box>
                            </Card>
                        </>)}
                </Formik>
                <DialogModal
                    open={open}
                    setOpen={setOpen}
                    css={{
                        "@bp0": { width: "90%" },
                        "@bp1": { width: "50%" },
                    }}
                >
                    <Box>
                        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                            {/* @ts-ignore */}
                            <Text css={{ color: "var(--colors-blue1)" }}>View History</Text>
                        </Box>
                        <Box className={`modal-body p-3`} >

                            <Box className={`row ${styles.scrollit}`}>
                                <Box css={{ overflow: "auto" }}
                                    className="col-auto col-lg-12 col-md-12 col-sm-12" >
                                    <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                                        {/* @ts-ignore */}
                                        <Table className="text-capitalize table table-striped" >
                                            <ColumnParent className="text-capitalize ">
                                                <ColumnRow className=" sticky" css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "left" }}  >
                                                    <Column className="col-1 text-center"><Text size="h5">No.</Text></Column>
                                                    <Column className="col-4"><Text size="h5">Subject Title</Text></Column>
                                                    <Column className="col-7 "><Text size="h5">Description</Text></Column>
                                                </ColumnRow>
                                            </ColumnParent>
                                            {loading ? <> <DataParent css={{ textAlign: "left" }} >
                                                <DataRow>
                                                    <DataCell colSpan={3}><Loader /></DataCell>
                                                </DataRow>
                                            </DataParent></> : <>
                                                <DataParent css={{ textAlign: "left" }} >

                                                    {contactList.length === 0 ?
                                                        <DataRow className='text-center'>
                                                            <DataCell colSpan={3} className="text-center">No Record Available </DataCell>
                                                        </DataRow> : <>
                                                            {contactList.map((record: any, index: any) => {
                                                                return (
                                                                    <DataRow>
                                                                        <DataCell className="text-center">{index + 1} </DataCell>
                                                                        <DataCell>{record?.name1 || "- -"} </DataCell>
                                                                        <DataCell>{record?.description || "- -"}</DataCell>
                                                                    </DataRow>
                                                                );
                                                            })
                                                            }</>}

                                                </DataParent>
                                            </>}
                                        </Table>
                                    </Box>
                                </Box>
                            </Box>

                        </Box>
                    </Box>
                </DialogModal>
            </Box>
        </Box>


    )
}

export default ContactUsPage;