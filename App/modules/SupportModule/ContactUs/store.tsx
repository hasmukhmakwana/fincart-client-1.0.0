import create from "zustand";
export type ContactUsTypes = {
    SubjectId: string;
    Description: string;
};
interface StoreTypes {
    contactUsVal: ContactUsTypes;
    setContactUsVal : (payload: ContactUsTypes) => void;
}
const useContactUsStore = create<StoreTypes>((set) => ({
    //* initial state
    contactUsVal: {
        SubjectId: "",
        Description: "",
    },
    setContactUsVal: (payload) =>
    set((state) => ({
      ...state,
      contactUsVal: payload || "",
    })),
}));

export default useContactUsStore;