import Box from "@ui/Box/Box";
import Layout from "App/shared/components/Layout";
import Card from "@ui/Card";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import ContactUsPage from "./components/ContactUsPage";


const ContactUs = () => {
    return (
        <>
            <Layout>
                <PageHeader title="Contact Us" rightContent={<></>} />
                <ContactUsPage />
            </Layout>
        </>
    )
}

export default ContactUs;