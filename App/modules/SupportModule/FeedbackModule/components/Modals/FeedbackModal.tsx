import React, { useEffect, useState } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import StarFill from "App/icons/StarFill";
import Textarea from "@ui/Textarea";
import { Button } from "@ui/Button/Button";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import create from "zustand";
import Card from '@ui/Card';
import { userFeedback, userFeedbackOptions } from 'App/api/tools';
import * as Yup from "yup";
import { Formik } from "formik";
import Radio from "@ui/Radio/Radio";
import StarRatings from 'react-star-ratings';
//import next from 'next';
import { RadioGroup } from "@radix-ui/react-radio-group";
import Layout, { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import Modal from "@ui/ModalDialog/ModalDialog";


const FeedbackModal = ({ setOpen }: any) => {
    const user: any = getUser();
    const [openFeedback, setOpenFeedback] = useState(false);
    const [rateService, setRateService] = useState(false);
    const [platform, setPlatform] = useState("");
    const [commentsFincart, setCommentsFincart] = useState("");
    const [relationshipManager, setRelationshipManager] = useState("");
    const [rating, setRating] = useState(0);
    const [options, setOptions] = useState<any>([]);
    const [serviceErr, setServiceErr] = useState<any>(false);
    const [platformErr, setPlatformErr] = useState<any>(false);
    const [relationManagerErr, setRelationManagerErr] = useState<any>(false);

    const [feedbackSubmit, setFeedbackSubmit] = useState(false);

    const [openPurchase, setOpenPurchase] = useState(false);

    const getUserFeedbackMenuList = async () => {
        try {
            const result: any = await userFeedbackOptions();
            setOptions(result?.data);
            console.log(result?.data);
        } catch (error) {
            console.log(error);
        }
    }
    useEffect(() => {
        getUserFeedbackMenuList();
        return () => {
            setFeedbackSubmit(false);
        }
    }, [])
    const handleSubmit = async () => {

        if (rating === 0) {
            setServiceErr(true);
        }
        if (platform === "") {
            setPlatformErr(true);
        }
        if (relationshipManager === "") {
            setRelationManagerErr(true)
        }
        if (rating !== 0 && platform !== "" && relationshipManager !== "") {
            setRateService(true);
            try {
                let obj: any = {
                    basicid: user?.basicid,
                    Rating: rating,
                    Type: 'WEB',
                    Subject: 'Feedback of web dashboard and RM',
                    OtherSubject: '',
                    Comments: commentsFincart,
                    AboutFincart: '',
                    RMStatus: relationshipManager,
                    PlatformStatus: platform,
                    Attachment: ''
                }

                const enc: any = encryptData(obj);
                const result: any = await userFeedback(enc);
                if (result?.status === "Success") {
                    setRateService(false);
                    setOpenFeedback(true);
                    setFeedbackSubmit(true);
                    setRating(0);
                    setPlatform('');
                    setRelationshipManager('');
                }
                else {
                    toastAlert("warn", result?.msg);
                    setRateService(false);
                }
            }
            catch (error) {
                setRateService(false);
                console.log(error);
                toastAlert("error", error);
            }
        }

    }
    return (
        <>
            <Box className="modal-header p-2" css={{ borderRadius: "15px 15px 0 0" }}>
                <Box className="row">
                    <Box className="col-lg-12 px-0 mx-auto" css={{ borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }}>
                        {/* @ts-ignore */}
                        <Text size="h3" className="text-center py-2" css={{ color: "var(--colors-blue1)" }} >We value your feedback</Text>
                        {/* @ts-ignore */}
                        <Text size="h5" className="text-center" css={{ color: "var(--colors-blue1)" }} >Please complete the following form and help us improve<br /> our customer experience</Text>
                    </Box>
                </Box>
            </Box>
            <Box className="modal-body p-3">
                {!feedbackSubmit ? <>
                    <Box className="row mb-3">
                        <Box className="col-sm-12">
                            <Box className="card h-100" css={{ border: "1px solid #b3daff" }}>
                                {/* @ts-ignore */}
                                <Text size="h4" className="text-center py-2 pt-3" css={{ color: "var(--colors-blue1)" }} >How would you rate our service?</Text>
                                <Text className="text-center pt-2">
                                    <StarRatings
                                        rating={rating}
                                        starRatedColor="#faac05"
                                        starHoverColor="#faac05"
                                        starDimension="18px"
                                        changeRating={setRating}
                                        numberOfStars={10}
                                        name='rating'
                                    />
                                </Text>
                                <Box className="mt-1" css={{ height: "15px" }}>
                                    {serviceErr ? <Text size="h6" className="text-center" css={{ color: "#dc3545" }}>
                                        Please rate us between 1 to 10
                                    </Text> : null}
                                </Box>
                            </Box>

                        </Box>
                        <Box className="col-sm-12 mt-2">
                            <Box className="card h-100" css={{ border: "1px solid #b3daff" }}>
                                {/* @ts-ignore */}
                                <Text size="h4" className="text-center py-2" css={{ color: "var(--colors-blue1)" }}>Platform</Text>
                                <Box className="text-center pt-2">
                                    <Text size="h5">
                                        <RadioGroup
                                            defaultValue={platform}
                                            className="inlineRadio"
                                            id="platformId"
                                            onValueChange={(e: any) => {
                                                setPlatform(e);
                                            }}
                                        >
                                            {options?.map((ele: any) => {
                                                return (
                                                    <Radio value={ele?.Value} label={ele?.Name} id={"platformId"} />
                                                )
                                            })}
                                        </RadioGroup>
                                    </Text>
                                </Box>
                                <Box className="mt-1" css={{ height: "15px" }}>
                                    {platformErr ? <Text size="h6" className="text-center" css={{ color: "#dc3545" }}>
                                        Please share platform feedback
                                    </Text> : null}
                                </Box>
                            </Box>

                        </Box>
                        <Box className="col-sm-12 mt-2">
                            <Box className="card h-100" css={{ border: "1px solid #b3daff" }}>
                                {/* @ts-ignore */}
                                <Text size="h4" className="text-center py-2" css={{ color: "var(--colors-blue1)" }} >Relationship Manager</Text>
                                <Box className="text-center pt-2">
                                    <Text size="h5">
                                        <RadioGroup
                                            defaultValue={relationshipManager}
                                            className="inlineRadio"
                                            id="RM"
                                            onValueChange={(e: any) => {
                                                setRelationshipManager(e);
                                            }}
                                        >
                                            {options?.map((ele: any) => {
                                                return (
                                                    <Radio value={ele?.Value} label={ele?.Name} id={"RM"} />
                                                )
                                            })}
                                        </RadioGroup>
                                    </Text>
                                </Box>
                                <Box className="mt-1" css={{ height: "15px" }}>
                                    {relationManagerErr ? <Text size="h6" className="text-center" css={{ color: "#dc3545" }}>
                                        Please share relationship manager feedback!
                                    </Text> : null}
                                </Box>
                            </Box>

                        </Box>
                    </Box>


                    <Box className="row">
                        <Box className="col-lg-12">
                            {/* @ts-ignore */}
                            <Text size="h4" className="py-1" css={{ color: "var(--colors-blue1)" }} >Tell us how we can improve</Text>
                        </Box>
                    </Box>
                    <Box className="row">
                        <Box className="col-lg-12">
                            <Textarea
                                label=""
                                name=""
                                rows={3}
                                value={commentsFincart}
                                onChange={(e: any) => { setCommentsFincart(e.target.value) }}
                                //className="border border-2"
                                placeholder="Your word make Fincart better"
                                css={{ border: "1px solid #b3daff", backgroundColor: "#f5f5f5" }}
                            />
                        </Box>
                        {/* {item?.trxnSource !== "External" ? <> */}
                        <Box className="row justify-content-end mx-0">
                            {/* <Box className="col-auto"></Box> */}
                            <Button
                                className="mb-2 p-1 px-2"
                                color="yellowGroup"
                                size="md"
                                onClick={() => setOpen(false)}
                            >
                                <Text
                                    // @ts-ignore
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                >
                                    I will do this later
                                </Text>
                            </Button>
                            <Button
                                className="mb-2 p-1 px-2"
                                color="yellowGroup"
                                size="md"
                                onClick={() => { handleSubmit() }}
                                loading={rateService}
                            >
                                <Text
                                    // @ts-ignore
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                >
                                    Submit
                                </Text>
                            </Button>
                        </Box>
                    </Box>
                </> : <>
                    <Box className="row mx-1 my-1 pb-1 px-0">
                        <Box className="col-lg-12 px-0 mx-auto" css={{ borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }}>
                            {/* @ts-ignore */}
                            <Text size="h4" className="text-center py-2 pt-3" css={{ color: "var(--colors-blue1)" }} >Thank you for your valuable feedback</Text>
                        </Box>
                    </Box>
                </>}
            </Box>
        </>
    )
}

export default FeedbackModal