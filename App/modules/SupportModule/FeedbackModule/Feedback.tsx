import React from 'react'
import Layout from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import FeedbackPage from './components/FeedbackPage';

const Feedback = () => {
    return (
        <>
            <Layout>
                <PageHeader title="Feedback" rightContent={<></>} />
                <FeedbackPage />
            </Layout>
        </>
    )
}

export default Feedback