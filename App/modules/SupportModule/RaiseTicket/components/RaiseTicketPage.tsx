import React, { useState } from "react";
import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import { Button } from "@ui/Button/Button";
import styles from "../../Support.module.scss";
// import { ShowTicket, AddTickets } from "./subPage";
import ShowTicket  from "./subPage/ShowTicket";
import  AddTickets  from "./subPage/AddTickets";
import { encryptData,getUser } from "App/utils/helpers";
import { getTicketList } from "App/api/ticket";

const RaiseTicketPage = () => {
  const [show, setShow] = useState(false);

  return (
    <>
      <Box className="row">
        <Box className="col-12 text-end">
          <Text size="h5">
            <Button
              className={!show ? `` : ` ${styles.yellowBUtton}`}
              onClick={() => {
                setShow(true);
              }}
            >
              My Ticket
            </Button>{" "}
            <span style={{ color: "var(--colors-blue)" }}> |</span>{" "}
            <Button
              className={!show ? `${styles.yellowBUtton}` : ``}
              onClick={() => {
                setShow(false);
              }}
            >
              Add New Ticket
            </Button>
          </Text>
        </Box>
      </Box>
      {!show ? (
        <AddTickets setShow={setShow} />
      ) : (
        <>
          {/* My Ticket UI END */}
          {/* ADD NEW Ticket UI START */}
          <ShowTicket />
        </>
      )}
    </>
  );
}

export default RaiseTicketPage;
