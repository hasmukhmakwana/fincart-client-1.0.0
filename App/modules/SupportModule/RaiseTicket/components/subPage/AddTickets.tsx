import React, { useEffect, useState } from "react";
import Box from '@ui/Box/Box'
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
import styles from "../../../Support.module.scss";
import { fetchTicketCategory, fetchSubjectlist, saveCreateTicket, fetchSubjectSubPoints } from "App/api/ticket";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import { toastAlert } from "App/shared/components/Layout";
import { convertBase64, encryptData, getUser, plainBase64 } from "App/utils/helpers";
import SelectMenu from "@ui/Select/Select";
import Input from "@ui/Input";
import Textarea from "@ui/Textarea";
import { Button } from "@ui/Button/Button";
import parse from "html-react-parser";
const ObjectSchemaList: any = {
    mandatory: {
        TicketCategory: Yup.string()
            .required("Select Category"),

        TicketSubject: Yup.string()
            .required("Select Subject"),

        description: Yup.string()
            .required("Description is required")
            .trim(),
    },
    other: {
        otherSubject: Yup.string()
            .required("Other Subject is required")
            .trim(),
    }
}

export type TicketTypes = {
    TicketCategory: string;
    TicketSubject: string;
    description: string;
    otherSubject: string;
    uploadFile: any;
};

interface StoreTypes {
    TicketVal: TicketTypes;
    setTicketVal: (payload: TicketTypes) => void;
}
const useTicketStore = create<StoreTypes>((set) => ({
    //* initial state
    TicketVal: {
        TicketCategory: "",
        TicketSubject: "",
        description: "",
        otherSubject: "",
        uploadFile: "",
    },
    setTicketVal: (payload) =>
        set((state) => ({
            ...state,
            TicketVal: payload || "",
        })),
}));

const AddTickets = ({ setShow }: any) => {
    const user: any = getUser();

    const [validationSchema, setValidationSchema] = useState(
        ObjectSchemaList.mandatory
    );
    const { TicketVal, setTicketVal } = useTicketStore();
    const [submitTicket, setSubmitTicket] = useState(false);
    const [ticketCategory, setTicketCategory] = useState<any>([]);
    const [selectCategory, setSelectCategory] = useState<any>("");
    const [subjectlist, setSubjectlist] = useState<any>([]);
    const [subjectName, setSubjectName] = useState<any>("");
    const [notes, setNotes] = useState<any>("");
    const [loader, setLoader] = useState(false);
    const fetchCategory = async () => {
        try {
            setLoader(true);
            let result: any = await fetchTicketCategory();
            setTicketCategory(result?.data?.Value);
            // console.log(result?.data?.Value, "result");
            setLoader(false);
        } catch (error) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error)
        }
    }

    const fetchList = async (catId: any) => {
        try {
            setLoader(true);
            let enc: any = encryptData(catId, true);
            let result: any = await fetchSubjectlist(enc);
            setSubjectlist(result?.data);
            // console.log(result?.data, "resultlist");
            setLoader(false);
        } catch (error) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error)
        }
    }


    const fetchSubjectNote = async (val: any) => {
        try {
            setLoader(true);
            let enc: any = encryptData(val, true);
            let result: any = await fetchSubjectSubPoints(enc);
            // console.log(result?.data?.Value?.[0], "resultlist");
            setNotes(result?.data?.Value?.[0] || "");
            setLoader(false);
        }
        catch (error) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error)
        }
    }

    const saveTicket = async (val: any) => {
        // console.log(val, "val")
        try {

            setLoader(true);
            let DocArray: any = []
            if (val?.uploadFile?.length > 0) {
                val?.uploadFile?.map((i: any) => {
                    DocArray.push(plainBase64(i));
                })
            }
            // 
            let obj: any = {
                basicId: user?.basicid,
                Query: val?.description,
                Other_Subject: val?.otherSubject,
                SubjectId: val?.TicketSubject,
                Ticket_Category_Id: val?.TicketCategory,
                attachments: DocArray
            }
            console.log(obj, "obj");
            // setLoader(false);
            // return;
            let enc: any = encryptData(obj);
            let result: any = await saveCreateTicket(enc);
            // console.log(result);
            toastAlert("success", "Ticket Created Successfully !")
            setShow(true);
            setLoader(false);
            // setSubmitTicket(true);
        } catch (error) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error)
        }
    }

    useEffect(() => {
        let obj = ObjectSchemaList.mandatory;
        if (subjectName === "Other" || subjectName === "Others") {
            obj = {
                ...obj,
                ...ObjectSchemaList.other
            }
        }
        setValidationSchema(obj);
        // console.log("swt");
        // console.log(obj);
        return () => { }
    }, [subjectName])

    useEffect(() => {
        if (selectCategory !== "") {
            // console.log(selectCategory);
            (async () => {
                await fetchList(selectCategory);
            })();
        }
        return () => { }
    }, [selectCategory])


    useEffect(() => {
        (async () => {
            await fetchCategory();
        })();
        return () => { }
    }, [])
    return (
        <>
            <Card css={{ p: 25 }}>
                <Box className="container">
                    {/* My Ticket UI START */}
                    <Formik
                        initialValues={TicketVal}
                        validationSchema={Yup.object().shape(validationSchema)}
                        enableReinitialize
                        onSubmit={(values) => {
                            // console.log(values);
                            saveTicket(values);
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount,
                        }) => (
                            <>

                                <Box className="row">
                                    <Box className="col-md-12 col-sm-12 col-lg-6">
                                        <SelectMenu
                                            label="Category"
                                            name={"TicketCategory"}
                                            value={values?.TicketCategory}
                                            items={ticketCategory}
                                            disabled={loader}
                                            onChange={(e: any) => {
                                                setFieldValue("TicketCategory", e?.Value); setSelectCategory(e?.Value);
                                                setFieldValue("otherSubject", "");
                                                setFieldValue("description", "");
                                            }}
                                            bindValue={"Value"}
                                            bindName={"Text"}
                                            error={submitCount ? errors.TicketCategory : null}
                                        />
                                    </Box>
                                    <Box className="col-md-12 col-sm-12 col-lg-6">
                                        <SelectMenu
                                            label="Subject"
                                            name={"TicketSubject"}
                                            value={values?.TicketSubject}
                                            items={subjectlist}
                                            disabled={selectCategory === "" || loader}
                                            onChange={(e: any) => {
                                                setFieldValue("TicketSubject", e?.code);
                                                setSubjectName(e?.name);
                                                fetchSubjectNote(e?.code);

                                                setFieldValue("description", "");
                                                if (e?.name === "Others" || e?.name === "Other") {
                                                    setFieldValue("otherSubject", "");
                                                } else {
                                                    setFieldValue("otherSubject", e?.name);
                                                }
                                            }}
                                            bindValue={"code"}
                                            bindName={"name"}
                                            error={submitCount ? errors.TicketSubject : null}
                                        />
                                    </Box>
                                    {subjectName === "Others" || subjectName === "Other" ? <>
                                        <Box className="col-md-12 col-sm-12 col-lg-6">
                                            <Input
                                                label="Other"
                                                type="text"
                                                disabled={loader}
                                                name={"otherSubject"}
                                                value={values?.otherSubject}
                                                onChange={handleChange}
                                                css={{ border: "1px solid #cccccc" }}
                                                error={submitCount ? errors.otherSubject : null}
                                            />
                                        </Box>
                                    </> : <></>}
                                </Box>
                                <Box className="row">
                                    <Textarea
                                        type="textarea"
                                        name="description"
                                        label="Description"
                                        disabled={loader}
                                        value={values?.description}
                                        placeholder="Type Description"
                                        rows={4}
                                        css={{ border: "1px solid #cccccc" }}
                                        onChange={(e: any) => {
                                            setFieldValue("description", e?.target?.value)
                                        }}
                                        error={submitCount ? errors.description : null}
                                    />
                                </Box>
                                <Box className="row">
                                    <Box className="col-md-12 col-sm-12 col-lg-6">
                                        <Input
                                            type="file"
                                            multiple
                                            label="Upload File"
                                            name="uploadFile"
                                            disabled={loader}
                                            className={`px-0 border rounded-4 ${styles.uploadbtn}`}
                                            onChange={(
                                                e: React.ChangeEvent<HTMLInputElement>
                                            ) => {
                                                try {
                                                    let files: any = e?.target?.files;
                                                    let fileArray: any = [];
                                                    // console.log(e?.target?.files, "File")
                                                    Object.keys(files).forEach(async (key) => {
                                                        // console.log(files[key], "key");
                                                        const base64 = await convertBase64(files[key]);
                                                        fileArray.push(base64);
                                                    })
                                                    console.log(fileArray)
                                                    setFieldValue("uploadFile", fileArray);
                                                } catch (error: any) {
                                                    toastAlert("error", error);
                                                }
                                            }}
                                            note="(Optional)"
                                        />
                                    </Box>
                                    <Box className="col-md-12 col-sm-12 col-lg-6 text-end pt-4">
                                        <Box className="mt-1">
                                            <Button
                                                className={`col-auto mb-2 py-1`}
                                                color="yellowGroup"
                                                size="md"
                                                disabled={loader}
                                                onClick={handleSubmit}
                                            >
                                                <Box>
                                                    <Text
                                                        //@ts-ignore
                                                        size="h6"
                                                        //@ts-ignore
                                                        color="yellowGroup"
                                                    >

                                                        {loader ? "Loading.." : "Submit"}
                                                    </Text>
                                                </Box>
                                            </Button>
                                        </Box>
                                    </Box>

                                </Box>
                            </>
                        )}</Formik>

                    <Box className="row justify-content-center">
                        {notes.length === 0 ? <></> : <>
                            <Box className="col-11 pt-4 pb-2 border rounded-4" css={{ borderColor: "#b3daff !important" }}>
                                <Box>
                                    <Text css={{ color: "#005CB3" }}> Notes :</Text>
                                </Box>
                                <Box className="ms-3">
                                    <Text> {parse(notes)}</Text>
                                </Box>
                            </Box>
                        </>}

                    </Box>
                </Box>
            </Card>
        </>
    )
}

export default AddTickets