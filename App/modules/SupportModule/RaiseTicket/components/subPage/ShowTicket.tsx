import Box from '@ui/Box/Box'
import React, { useEffect, useState } from "react";
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
import DeleteIcon from "App/icons/DeleteIcon";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import ViewModal from "@ui/ModalDialog/ModalDialog";
import styles from "../../../Support.module.scss";
import TicketClose from "../modals/TicketClose";
import { getTicketList, fetchCommentForTicket, fetchTicketAttachments } from "App/api/ticket";
import { toastAlert } from 'App/shared/components/Layout';
import { encryptData, formatDate, getUser } from "App/utils/helpers";
import Group from "@ui/Group/Group";
import Pagination from "@ui/DataGrid/components/Pagination";
import Loader from 'App/shared/components/Loader';
import { Button } from '@ui/Button/Button';
import ViewIcon from 'App/icons/ViewIcon';
import Info from 'App/icons/Info';
import Download from 'App/icons/Download';
import Badge from '@ui/Badge/Badge';
import { now } from 'lodash';
import Timer from 'App/modules/Timer';
const ShowTicket = () => {
  const user: any = getUser();
  // const [closeTicket, setCloseTicket] = useState(false);
  const [loader, setLoader] = useState(false);
  const [loaderPop, setLoaderPop] = useState<any>(false);
  const [totalCount, setTotalCount] = useState(0);
  const [limit, setLimit] = useState(0);
  const [txnLen, setTxnLen] = useState(0);
  const [ticketList, setTicketList] = useState<any>([]);
  const [commentList, setCommentList] = useState<any>([]);
  const [commentPop, setCommentPop] = useState(false);
  const [documentList, setDocumentList] = useState<any>([]);
  const [documentPop, setDocumentPop] = useState(false);
  const PageLimit = 10;

  const fetchTicketList = async () => {
    try {
      setLoader(true);
      let obj: any = {
        OffsetValue: limit,
        PagingSize: PageLimit,
        basicId: user?.basicid
      }
      let enc: any = encryptData(obj);
      let result = await getTicketList(enc);
      setTicketList(result?.data);
      setTotalCount(result?.data?.[0]?.TotalRows)
      setTxnLen(result?.data?.length);

      setLoader(false);
    } catch (error) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error)
    }
  }

  const fetchCommentTicket = async (ticket: any) => {
    try {
      setLoaderPop(true);
      setCommentPop(true);
      let enc: any = encryptData(ticket, true);
      let result = await fetchCommentForTicket(enc);
      setCommentList(result?.data);
      setLoaderPop(false);
    } catch (error) {
      setLoaderPop(false);
      console.log(error);
      toastAlert("error", error)
    }
  }

  const fetchDocumentPdf = async (ticket: any) => {
    try {
      setLoaderPop(true);
      setDocumentPop(true);
      let enc: any = encryptData(ticket, true);
      let result = await fetchTicketAttachments(enc);
      setDocumentList(result?.data);
      setLoaderPop(false);
    } catch (error) {
      setLoaderPop(false);
      console.log(error);
      toastAlert("error", error)
    }
  }

  const isTATExpired = (tatDate: any) => {
    if (tatDate == undefined || tatDate == "") return false;

    var date = Date.parse(tatDate);

    if (date > 0) return true;
  }

  useEffect(() => {
    (async () => {
      await fetchTicketList();
    })();
    return () => {

    }
  }, [limit])

  const getFileName = (filePath: string) => {

    if (filePath == undefined || filePath.length == 0)
      return "";

    let fileName = filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length);

    return fileName;
  }

  const convertToPlain = (val: any) => {

    // Create a new div element
    var tempDivElement = document.createElement("div");

    // Set the HTML content with the given value
    tempDivElement.innerHTML = val;

    // Retrieve the text property of the element 
    return tempDivElement.textContent || tempDivElement.innerText || "";
  }

  return (
    <Box className="mt-2">
      {/* ADD NEW Ticket UI START */}
      {loader ? <>
        <Box className="container">
          <Box className="row mb-2 justify-content-center">
            <Box className="col-auto"><Loader />
            </Box>
          </Box>
        </Box>
      </>
        : <>
          {ticketList.length === 0 ? <>
            <Box className="container">
              <Box className="row mb-2 justify-content-center">
                <Box className="col-auto">No Tickets Found
                </Box>
              </Box>
            </Box>
          </> : <>
            <Box className="" css={{ overflow: "auto", maxHeight: "400px" }}>
              {ticketList?.map((record: any) => {
                return (
                  <>
                    <Card style={{ border: "3px solid rgb(179, 218, 255)" }}>
                      <Box className="container">
                        <Box className="row mb-2">
                          <Box className="col-md-12 col-sm-12 col-lg-8">
                            <Text size="h4" className="pt-2">
                              Ticket No {record?.["Ticket ID"]}
                            </Text>
                          </Box>
                          <Box className="col-md-12 col-sm-12 col-lg-2">
                            <Text size="h6" color="primary">
                              Ticket Created Date
                            </Text>
                            <Text size="h6">{formatDate(record?.["Created Date"], "d-m-y")}</Text>
                          </Box>
                          <Box className="col-md-12 col-sm-12 col-lg-2 text-center">
                            <Text size="h6" color="primary">
                              Status
                            </Text>
                            <span className={`${record?.Status === "Open" ? styles.statusActive : record?.Status === "Close" ? styles.statusClosed : styles.statusInprocess} ps-3 pe-3 text-center`} >
                              {record?.Status}
                            </span>
                          </Box>
                        </Box>
                        <Box className={styles.line}></Box>
                        <Box className="row mt-2">
                          <Box className="col-md-12 col-sm-12 col-lg-4">
                            <Text size="h6" color="primary">
                              {record?.SubjectName || "--"}
                            </Text>
                            <Box css={{ fontSize: "0.7rem" }}>
                              {convertToPlain(record?.Query)}
                            </Box>
                          </Box>
                          <Box className="col-md-12 col-sm-12 col-lg-4">
                            <Text size="h6" color="primary">
                              Support Comment
                              {record?.Comment === null || record?.Comment === "" ? <></> :
                                <Button
                                  className={`p-1 rounded-circle mx-5  ${styles.yellowBUtton}`}
                                  color="orange"
                                  onClick={() => { fetchCommentTicket(record?.["Ticket ID"]) }}
                                  title={"View Support Comments"}
                                >
                                  <Info color="#202020" height={14} width={14} className="ActionBtn" />
                                </Button>}
                            </Text>
                            <Text size="h6">
                              {convertToPlain(record?.Comment) || "--"}
                            </Text>
                          </Box>
                          <Box className="col-md-12 col-sm-12 col-lg-2">
                            <Text size="h6" color="primary">
                              Attachments
                              <Button
                                className={`p-0 rounded-circle mx-2`}
                                color="white"
                                onClick={() => { fetchDocumentPdf(record?.["Ticket ID"]) }}
                                title={"Attachments"}
                              >
                                <Badge
                                  type={"dot"}
                                  color={"orange1"}
                                  rounded
                                  className={"px-2"}
                                >
                                  {record?.AttachmentCount}
                                </Badge>
                              </Button>
                            </Text>
                          </Box>
                          {record?.Status != "Close" &&
                            <Box className="col-md-12 col-sm-12 col-lg-2 text-center">
                              <Text size="h6" color="primary">
                                Turn Around Time
                              </Text>
                              <Text size="h6">
                                {/* {formatDate(record?.TAT, "d-m-y") || ""} */}
                                {record?.Status == "Close" ? "" : record?.TAT == "" ? "PENDING" :
                                  (isTATExpired(formatDate(record?.TAT, "d-m-y") || "") ?
                                    <Timer CurrentDate={record?.TAT}></Timer>
                                    : <Text>
                                      TAT Expired
                                    </Text>)}
                              </Text>
                            </Box>
                          }
                        </Box>
                      </Box>
                    </Card>
                  </>
                );
              })}
            </Box>
          </>}
        </>}
      <Box className="row justify-content-end mt-2">
        <Box className="col-auto col-lg-auto col-sm-auto px-0 mb-0">
          <Group position="right">
            <Box>Total Record: {totalCount} <span className="mb-2" style={{ fontSize: "0.8rem" }}> [{limit} - {limit + txnLen}]</span></Box>
            <Pagination
              totalCount={totalCount}
              limit={PageLimit}
              onPageChange={(e: any) => {
                setLimit((e?.selected) * (PageLimit));
              }}
            />
          </Group>
        </Box>
      </Box>
      <ViewModal
        open={commentPop}
        setOpen={setCommentPop}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "50%" },
        }}>
        <Box className="modal-header" css={{ borderRadius: "15px 15px 0 0" }}>
          View RM comments for Ticket No.: <span> {commentList?.[0]?.TicketId || "--"}</span>
        </Box>
        <Box className="modal-body text-center px-5 py-2">
          <Box className={`${styles.scrollit}`}>
            {loaderPop ? <><Loader /></> : <>
              {commentList.length === 0 ? <>
                <Card className="my-5">No Comment available</Card>
              </> : <>
                {commentList?.map((item: any) => {
                  return (
                    <Card className="p-1 mb-1 row justify-content-between">
                      {/* <Box className="col-1">#</Box> */}
                      <Box className="col-12 text-start pb-1 border-bottom" css={{ borderColor: "orange !important" }}>{convertToPlain(item?.Comment)}</Box>

                      <Box className="col-12 text-end pt-2" >
                        <Box className="row justify-content-end">
                          <Box className="col-auto border-end" css={{ borderColor: "#000 !important" }}><Text color="primary" size={"h6"}>{item?.CommentBy}</Text></Box>
                          <Box className="col-auto"><Text color="primary" size={"h6"}>{formatDate(item?.CommentDate, "d-m-y")}</Text></Box>
                        </Box>
                      </Box>
                    </Card>
                  );
                })}
              </>}
            </>}
          </Box>
        </Box>
      </ViewModal>
      <ViewModal
        open={documentPop}
        setOpen={setDocumentPop}
        css={{
          "@bp0": { width: "80%" },
          "@bp1": { width: "30%" },
        }}>
        <Box className="modal-header" css={{ borderRadius: "15px 15px 0 0" }}>
          Attached Documents
        </Box>
        <Box className="modal-body text-center px-5 py-2">
          <Box className={`${styles.scrollitPop}`}>
            {loaderPop ? <><Loader /></> : <>
              {documentList.length === 0 ? <>
                <Card className="my-5">No Attachment available</Card>
              </> : <>
                {documentList?.map((item: any, i: any) => {
                  return (<Card className="p-1 mb-1 row justify-content-start">
                    <Box className="col-1 text-start"><a href={item?.ImageUrl} target="_blank"><Download color="orange" /></a></Box>
                    <Box className="mx-2 col text-start"><a style={{ fontSize: "0.8rem" }} href={item?.ImageUrl} target="_blank">{getFileName(item?.ImageUrl)}</a></Box>
                  </Card>);
                })}
              </>}
            </>}
          </Box>
        </Box>
      </ViewModal>
    </Box >
  )
}

export default ShowTicket