import React, { useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import styles from "../../../Support.module.scss";
import { Button } from "@ui/Button/Button";
import CheckCircleFill from "App/icons/CheckCircleFill";
import Textarea from "@ui/Textarea";


const TicketClose = () => {

    return (
        <>
            {/* <Box className={`${styles.modalBorder} ps-3 pe-3 modal-body`}> */}
            <Box className="modal-body text-center p-5">
                <Text size="h4" color="primary" className="pb-3">
                    Please share your reason
                </Text>
                <Box className="row">
                    <Textarea type="textarea" name="textValue" placeholder="Type here your reason" className="rounded" />
                </Box>
                <Box className="row">
                    <Box className="col-12 text-center mt-4">
                        <Button
                            className={`col-auto mb-2 py-1`}
                            color="yellowGroup"
                            size="md"
                        >
                            <Box>
                                <Text
                                    //@ts-ignore 
                                    size="h6"
                                    //@ts-ignore
                                    color="yellowGroup">
                                    Continue
                                </Text>
                            </Box>
                        </Button>
                    </Box>
                </Box>
            </Box>
        </>
    );
};
export default TicketClose;
