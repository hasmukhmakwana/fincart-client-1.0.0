import React, { useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import styles from "../../../Support.module.scss";
import { Button } from "@ui/Button/Button";
import CheckCircleFill from "App/icons/CheckCircleFill";

type NameEventTypes = {
    setShow: (value: boolean) => boolean;
    switchScreen:(value: boolean) => boolean;
  };

const TicketCreated = ({setShow, switchScreen}: NameEventTypes) => {

return (
    <>
    {/* <Box className={`${styles.modalBorder} ps-3 pe-3 modal-body`}> */}
    <Box className="modal-body text-center p-5">
    <CheckCircleFill color="orange" className="mt-1" size="30"/>
      {/* @ts-ignore */}
            <Text size="h3" color="primary" className="pt-3">
            Ticket Created #752384
            </Text>
            {/* @ts-ignore */}
            <Text size="h5" className="pt-2">
            Your ticket has been successfully created.
            </Text>
            <Box className="row">
                        <Box className="col-12 text-center mt-4">
                            <Button
                                className={`col-auto mb-2 py-1`}
                                color="yellowGroup"
                                size="md"
                                onClick={()=>{ switchScreen(false) ;setShow(false);}}
                            >
                                <Box>
                                    <Text
                                        //@ts-ignore 
                                        size="h6"
                                        //@ts-ignore
                                        color="yellowGroup">
                                        View Ticket
                                    </Text>
                                </Box>
                            </Button>
                        </Box>
                    </Box>
    </Box>
    </>
    );
};
export default TicketCreated;
    