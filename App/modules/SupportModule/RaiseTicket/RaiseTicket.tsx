import Box from "@ui/Box/Box";
import Layout from "App/shared/components/Layout";
import Card from "@ui/Card";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import styles from "../Support.module.scss";
import { Button } from "@ui/Button/Button";
import Input from "@ui/Input";
import Search from 'App/icons/Search';
import { GroupBox } from "@ui/Group/Group.styles";
import Link from "next/link";
import RaiseTicketPage from "./components/RaiseTicketPage";

const RaiseTicketModule = () => {
  return (
    <>
      <Layout>
        <PageHeader title="Raise A Ticket" rightContent={<></>} />
        <RaiseTicketPage />
      </Layout>
    </>
  )
}

export default RaiseTicketModule;