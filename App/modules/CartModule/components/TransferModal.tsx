import React, { useState } from 'react'
import Box from '@ui/Box/Box';
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import Input from "App/ui/Input";
import Layout, { toastAlert } from 'App/shared/components/Layout';
import { encryptData, getUser } from "App/utils/helpers";
import { sysValidateOTP } from "App/api/cart";
import { investTransaction } from "App/api/transactions";

const TransferModal = ({ sysCartTotal, proceedSysCartData, profileID, basicID, setOpenTransfer, fetchSystematicCartDetails, setOpenSuccess, setMessageArr }: any) => {
  const [otp, setOtp] = useState<any>('');
  const [loader,setLoader] = useState(false);
  const handleSubmit = async () => {
    if (otp === "") {
      toastAlert("warn", "OTP field cannot be blank");
    }
    else {
      try {
        setLoader(true);
        let msg: any = [];
        const dt = new Date();
        const enc: any = encryptData(otp, true);
        let result: any = await sysValidateOTP(enc);
        if (result?.status === "Success") {
          for (let i = 0; i < proceedSysCartData.length; i++) {
            try {
              let obj = {
                "id": proceedSysCartData[i]?.id,
                "basicID": basicID,
                "ProfileID": profileID,
                "purSchemeId": proceedSysCartData[i]?.FromSchemeID,
                "sellSchemeId": proceedSysCartData[i]?.ToSchemeID,
                "tranType": proceedSysCartData[i]?.Trxn_Type,
                "PurFolioNo": proceedSysCartData[i]?.FolioNo,
                "SellFolioNo": proceedSysCartData[i]?.SellFolioNo,
                "Amount": proceedSysCartData[i]?.Amount,
                "No_of_Installment": proceedSysCartData[i]?.No_of_Installment,
                "MDate": proceedSysCartData[i]?.MDate,
                "userGoalId": proceedSysCartData[i]?.userGoalId,
                "Units": proceedSysCartData[i]?.Units,
                "bankId": proceedSysCartData[i]?.Bank_ID,
                "mandateId": proceedSysCartData[i]?.Mandate_ID,
                "startDate": proceedSysCartData[i]?.Start_Date
              }
              let isCheckedRm: any = document.getElementById(proceedSysCartData[i]?.id) 
              const enc: any = encryptData(obj);
              let result1: any = await investTransaction(enc);
              if (result1?.status === "Success") {
                if (proceedSysCartData[i]?.Trxn_Type === "STP") {
                  msg.push({
                    type: 'success',
                    msg: `STP of ₹ ${proceedSysCartData[i]?.Amount} is registered from ${proceedSysCartData[i]?.toSchemeName
                      }. Amount will be switched to ${proceedSysCartData[i]?.Scheme_name
                      } from ${(dt.getMonth() + 1)} ${dt.getDate()} , ${dt.getFullYear()}  to next 1 Month.`,
                  })
                }
                else if (proceedSysCartData[i]?.Trxn_Type === "SWP") {
                  msg.push({
                    type: 'success',
                    msg: `SWP of ₹ ${proceedSysCartData[i]?.Amount} is registered from ${proceedSysCartData[i]?.toSchemeName}. Amount will be credited to (${proceedSysCartData[i]?.bank
                      }  - ${proceedSysCartData[i]?.Account_No} ) from ${(dt.getMonth() + 1)}  ${dt.getDate()} , ${dt.getFullYear()} to next 1 Month.`,
                  })
                }
                else if (proceedSysCartData[i]?.Trxn_Type === "R") {
                  msg.push({
                    type: 'success',
                    msg: `Redemption of ₹ ${proceedSysCartData[i]?.Amount} is registered from ${proceedSysCartData[i]?.toSchemeName}. Amount will be credited to (${proceedSysCartData[i]?.bank
                      }  - ${proceedSysCartData[i]?.Account_No} ) within 3 working days [T+3].`,
                  })
                }
                else if (proceedSysCartData[i]?.Trxn_Type === "SWITCH") {
                  msg.push({
                    type: 'success',
                    msg: `Switch of ₹ ${proceedSysCartData[i]?.Amount} is registered from ${proceedSysCartData[i]?.toSchemeName}. Amount will be switched to ${proceedSysCartData[i]?.Scheme_name
                      } within 3 working days.`,
                  })
                }
                else if (proceedSysCartData[i]?.Trxn_Type === "SIP") {
                  msg.push({
                    type: 'success',
                    msg: `SIP of ₹ ${proceedSysCartData[i]?.Amount} is registered from ${proceedSysCartData[i]?.toSchemeName}.`,
                  })
                }
              }
              else {
                msg.push({
                  type: 'error',
                  msg: `${proceedSysCartData[i]?.Trxn_Type} of ₹ ${proceedSysCartData[i]?.Amount || 0
                    } is registered from ${proceedSysCartData[i]?.toSchemeName}`,
                });
              }
              
            } catch (error: any) {
              console.log(error);
              toastAlert("error", error);
              msg.push({
                type: 'error',
                msg: `${proceedSysCartData[i]?.Trxn_Type} of ₹ ${proceedSysCartData[i]?.Amount || 0
                  } is registered from ${proceedSysCartData[i]?.toSchemeName}`,
              });
            }

          }
          setOpenTransfer(false);
          setMessageArr(msg);
          setOpenSuccess(true);
          setLoader(false);
          fetchSystematicCartDetails();
        }
        else {
          toastAlert("error", result?.msg);
          setLoader(false);
        }
      } catch (error: any) {
        console.log(error);
        setLoader(false);
        toastAlert("error", error);

      }
    }
  }
  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}>Enter OTP</Text>
        </Box>
        <Box className="modal-body p-3">
          <Box className="row border-bottom border-warning" >
            <Box className="col-12 mt-2">
              {/* @ts-ignore */}
              <Text className="text-capitalize" >Total No. of Transactions: {proceedSysCartData.length}</Text>
            </Box>
          </Box>
          <Box className="row border-bottom border-warning" >
            <Box className="col-12 mt-3">
              {/* @ts-ignore */}
              <Text className="text-capitalize" >Total Amount: {sysCartTotal}</Text>
            </Box>
          </Box>
          <Box className="row">
            <Box className="col-lg-4 mt-2">
              <Input
                label="Enter OTP"
                name="otp"
                className="border border-2"
                placeholder=""
                onChange={(e: any) => { setOtp(e.target.value) }}
              />
            </Box>
          </Box>
          <Box className="row justify-content-end mx-0">
            {/* <Box className="col-auto"></Box> */}
            <Button
              className="mt-2 p-2"
              color="yellowGroup"
              size="md"
              onClick={() => { handleSubmit() }}
              disabled={loader}
            >
              <Text
                // @ts-ignore
                //weight="bold"
                size="h4"
                //@ts-ignore
                color="white"
              //className={styles.button}
              >
              {loader ? "Loading.." : "Confirm"}  
              </Text>
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default TransferModal;