import React, { useEffect, useState } from 'react'
import Box from '@ui/Box/Box';
import Text from "@ui/Text/Text";
import { StyledTabs, TabsContent, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsTrigger } from "./TabTriggerCart.style";
import SelectMenu from '@ui/Select/Select';
import Input from "App/ui/Input";
import styles from "../Cart.module.scss";
import { Button } from "@ui/Button/Button";
import { encryptData, getUser, openTab } from "App/utils/helpers";
import { purchaseAttempt, getBillDeskPostString } from "App/api/cart";
import Layout, { toastAlert } from 'App/shared/components/Layout';
import { WEB_URL } from 'App/utils/constants';
import useCartStore from '../store';

type PurchaseEventTypes = {
  paymentData: (value: any) => any;
  setOpen: (value: boolean) => boolean;
  bankData: (value: any) => any;
};


const PurchaseModal = ({ paymentData, setOpen, bankData, profileID, basicID, proceedCartData }: any) => {
  const [bankName, setBankName] = useState<any>(bankData[0]?.accountNo);
  const [paymentTabs, setPaymentTabs] = useState<any>(paymentData)
  const [selectedPayMode, setSelectedPayMode] = useState<any>(paymentData[0]?.payModeId);
  const [btnState, setBtnState] = useState(false);
  const {
    setTransactionNo
  } = useCartStore();

  useEffect(() => {
    console.log("Pur ", bankData);
  }, [])
  const Submit = async () => {
    console.log(selectedPayMode);
    console.log(bankName);
    // if(selectedPayMode==="3"){
    console.log(profileID, " ", basicID);
    console.log(proceedCartData.length);
    setBtnState(true);

    let cartbunch = "";
    for (let i = 0; i < 5; i++) {
      if (proceedCartData[i]?.cartId === undefined && i === 4)
        cartbunch = cartbunch + "NA"
      else if (proceedCartData[i]?.cartId === undefined)
        cartbunch = cartbunch + "NA_"
      else if (proceedCartData[i]?.cartId !== undefined && i === 4) {
        cartbunch = cartbunch + proceedCartData[i]?.cartId
      }
      else {
        cartbunch = cartbunch + proceedCartData[i]?.cartId + "_"
      }
    }
    console.log(cartbunch);
    try {
      let obj = {
        basicID: basicID,
        ProfileID: profileID,
        trxntblId: 0,
        cartIdBunch: cartbunch,
        paymodeId: selectedPayMode,
        returnUrl: `${WEB_URL}/CartStatus`,
        // returnUrl: `https://uat-app-fincart.azurewebsites.net/PaymentStatus`,
        productId: 'DIRECT',
        Device:'WEB'
      }
      const enc: any = encryptData(obj);
      let result = await purchaseAttempt(enc);
      console.log("Purchase Data ", result?.data);
      localStorage.setItem("transactionNo", result?.data);
      try {
        const enc: any = encryptData(result?.data, true);
        let result1 = await getBillDeskPostString(enc);
        setBtnState(false);
        console.log("Get String ", result1?.data);
        window.open(result1?.data, '_self');
        // openTab(result?.data, true);
      } catch (error1: any) {
        console.log(error1);
        toastAlert("error", error1);
      }
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setBtnState(false);

    }
    // }
  }
  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}> Select Payment Option</Text>
        </Box>
        <Box className="modal-body p-3">
          <Box className="row border-bottom border-warning" >
            <Box className="col-12">
              {/* @ts-ignore */}
              <Text className="text-capitalize" >{bankData[0]?.bankName} ({bankData[0]?.accountNo})</Text>
            </Box>
          </Box>
          <Box className="row mt-2">
            <Box className="col-12">
              {/* @ts-ignore */}
              <Text size="h6" >
                Select Payment Option
              </Text>
            </Box>
            <Box className="col-12">
              <StyledTabs defaultValue="Net Banking">
                <TabsList
                  aria-label="Purchase Details"
                  className={`tabsCardStyle row`}
                  css={{ mb: 10 }}

                >
                  {paymentTabs.map((item: any, index: any) => {
                    return (
                      <Box className="col-auto">
                        <TabsTrigger
                          value={item?.payType}
                          className={`tabsCard`}
                          key={item?.payModeId}
                          onClick={() => { setSelectedPayMode(item?.payModeId) }}
                        >
                          <Box className="text-center">
                            {/* @ts-ignore */}
                            <Text size="h5">{item?.payType}</Text>
                          </Box>
                          <Box className="row justify-content-center" css={{ height: "1.2rem" }}>
                            <Box className="col-auto">
                              <img width="100px" src={item?.modeImg} alt={item?.payType} />
                            </Box>
                          </Box>
                        </TabsTrigger>
                      </Box>
                    );
                  })}
                </TabsList>
                <TabsContent value="NetBanking">
                  <Box className="row">
                    <Box className="col-lg-5 px-0 my-4">
                      <SelectMenu
                        label="Select Your Bank Name"
                        name="NetBankingOptions"
                        value={bankData[0]?.accountNo}
                        items={bankData}
                        bindValue={"accountNo"}
                        bindName={"bankName"}
                        onChange={(e) =>
                          setBankName(e?.accountNo || "")
                        }
                      />
                    </Box>
                  </Box>
                </TabsContent>
                <TabsContent value="UPI">
                  {/* <Box className="row">
                    <Text size="h5" className='px-0'>Ac no.: {bankData[0]?.accountNo}</Text>
                    <Box className="col-auto px-0 my-1">
                      <Input
                        label="Enter UPI ID"
                        name="upi"
                        className="border border-2"
                        placeholder="abc@okaxis"
                      />
                    </Box>
                  </Box> */}
                </TabsContent>
              </StyledTabs>
            </Box>
          </Box>
          <Box className="row justify-content-end mx-0">
            {/* <Box className="col-auto"></Box> */}
            <Button
              className="mt-2 p-2"
              color="yellowGroup"
              size="md"
              onClick={() => { Submit() }}
              disabled={btnState}
            >
              <Text
                // @ts-ignore
                //weight="bold"
                size="h6"
                //@ts-ignore
                color="white"
              //className={styles.button}
              >
                {btnState ? <>Loading... </> : <> Continue to Pay</>}

              </Text>
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default PurchaseModal