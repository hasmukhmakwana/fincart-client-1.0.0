//@ts-nocheck
import { mauve, violet } from "@radix-ui/colors";
import * as TabsPrimitive from "@radix-ui/react-tabs";
import { styled } from "App/theme/stitches.config";

const StyledTrigger = styled(TabsPrimitive.Trigger, {
  all: "unset",
  fontFamily: "inherit",
  backgroundColor: "white",
  padding: "0 20px",
  height: 150,
  flex: 1,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  fontSize: 15,
  lineHeight: 1,
  color: mauve.mauve11,
  userSelect: "none",
  '@bp0': {
    overflowX: "auto",
  },
  "&:first-child": { borderTopLeftRadius: 6 },
  "&:last-child": { borderTopRightRadius: 6 },
  "&:hover": { color: violet.violet11 },
  '&[data-state="active"]': {
    color: "orange",
    boxShadow: "inset 0 -1px 0 0 currentColor, 0 1px 0 0 currentColor",
  },
  // "&:focus": { position: "relative", boxShadow: `0 0 0 2px black` },
  "&.tabs": {
    border :6,
    flex: "none",
    '&[data-state="active"]': {
      backgroundColor: "orange",
      color: "$white",
      boxShadow: "none",
      "& p": {
        color: "#fff !important",
      }
    },
  },
  "&.tabsCenter": {
    border :6,
    borderBottom: "1px solid #000",
    flex: "none",
    '&[data-state="active"]': {
      backgroundColor: "orange",
      color: "$white",
      boxShadow: "none",
      "& p": {
        color: "#fff !important",
      }
    },
  },
  "&.tabsCard": {
    borderRadius: 6,
    marginLeft: 10,
    flex: "none",
    paddingBottom: "2rem !important",
    paddingTop: 2,
    overflow:"hidden",
    '@bp0': {
      flexDirection: "column",
      height: 100,
      marginLeft: 5,
      padding: "$5",
    },
    "& .iconTick": {
      fill: "orange !important",
      marginRight: 15,
      '@bp0': {
        margin: 'auto',
      },
    },
    '&[data-state="active"]': {
      backgroundColor: "orange",
      color: "$white",
      boxShadow: "none",
      "& .iconTick": {
        fill: "#fff !important",
      },
      "& p": {
        color: "#fff !important",
      }
    },
  },
  "&.tabsCards": {
    height: 150,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "$primary",
    borderRadius: 5,
    marginLeft: 5,
    flex: "none",
    paddingBottom: 0,
    paddingTop: 0,
    "& .iconTick": {
      fill: "orange !important",
      marginRight: 15,
    },
    '&[data-state="active"]': {
      backgroundColor: "orange",
      color: "$white",
      boxShadow: "none",
      "& .iconTick": {
        fill: "#fff !important",
      },
      "& p": {
        color: "#fff !important",
      }
    },
  },
});





// Exports
// export const StyledTabs = Tabs;
// export const TabsList = StyledList;
export const TabsTrigger = StyledTrigger;
// export const TabsContent = StyledContent;
// export const TabsContentNoValue = StyledContentNoValue;