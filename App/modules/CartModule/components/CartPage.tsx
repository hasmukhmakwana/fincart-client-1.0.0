import React, { useEffect, useState } from 'react'
import Box from "@ui/Box/Box";
import SelectMenu from "@ui/Select/Select";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import Layout, { toastAlert } from 'App/shared/components/Layout';
import Text from "@ui/Text/Text";
import StarFill from "App/icons/StarFill";
import DeleteIcon from "App/icons/DeleteIcon";
import Card from "App/ui/Card/Card";
import Checkbox from "@ui/Checkbox/Checkbox";
import { GroupBox } from "@ui/Group/Group.styles";
import styles from "../Cart.module.scss";
import Badge from "App/ui/Badge/Badge";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import PurchaseModal from './PurchaseModal';
import TransferModal from './TransferModal';
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import * as Yup from "yup";
import { Formik } from 'formik';
import useCartStore from '../store';
import CheckCircleFill from "App/icons/CheckCircleFill";
import CrossCircleFill from 'App/icons/CrossCircleFill';
import { encryptData, getUser } from "App/utils/helpers";
import { getCartDetails, getSystematicCartDetails, removingFromCart, getBankPaymentModes, removingFromSysCart, sysSendOTP } from "App/api/cart";
import Loader from "App/shared/components/Loader";
import useHeaderStore from 'App/shared/components/Header/store';

let CartTabs = [
    {
        title: "Purchase",
        value: "Purchase",
    },
    {
        title: "Transfer",
        value: "Transfer",
    },
];

const CartPage = () => {
    const user: any = getUser();
    const {
        investorACList,
        investorList,
        bankList

    } = useCartStore();
    const { cartCount, setCartCount } = useHeaderStore();
    const [investorAccountList, setInvestorAccountList] = useState<any>([]);
    const [openPurchase, setOpenPurchase] = useState(false);
    const [openTransfer, setOpenTransfer] = useState(false);
    const [openInvestor, setOpenInvestor] = useState<any>(investorList?.[0]?.basicid || "");
    const [openInvestorAcc, setOpenInvestorAcc] = useState<any>('');
    const [purchaseCartData, setPurchaseCartData] = useState<any>([]);
    const [systematicCartData, setSystematicCartData] = useState<any>([]);
    const [purchaseLoad, setPurchaseLoad] = useState(false);
    const [systematicLoad, setSystematicLoad] = useState(false);
    const [proceedCartData, setProceedCartData] = useState<any>([]);
    const [paymentData, setPaymentData] = useState<any>([]);
    const [bankData, setBankData] = useState<any>([]);
    const [proceedSysCartData, setProceedSysCartData] = useState<any>([]);
    const [purCartTotal, setPurCartTotal] = useState<Number>(0);
    const [sysCartTotal, setSysCartTotal] = useState<Number>(0);
    const [messageArr, setMessageArr] = useState<any>([]);
    const [openSuccess, setOpenSuccess] = useState<any>(false);
    const [openDelete, setOpenDelete] = useState<boolean>(false);
    const [deleteSysCartId, setDeleteSysCartId] = useState<string>('');
    const [deletePurCartId, setDeletePurCartId] = useState<string>('');
    const [continueLoading, setContinueLoading] = useState<boolean>(false);
    const [switchType, setSwitchType] = useState<any>("Purchase");
    let arr: any = [];
    useEffect(() => {
        if (openInvestor !== undefined && openInvestorAcc !== undefined && openInvestorAcc !== "") {
            setSysCartTotal(0);
            setPurCartTotal(0);
            setProceedCartData([]);
            setProceedSysCartData([]);
            fetchCartDetails();
            fetchSystematicCartDetails();
        }
    }, [openInvestorAcc, cartCount])

    useEffect(() => {
        let arr = investorACList.filter((item: any) => {
            return item?.basicid === openInvestor;
        });
        //console.log(arr);
        setInvestorAccountList(arr);
        setOpenInvestorAcc(arr[0]?.profileID);
    }, [openInvestor])

    const fetchCartDetails = async () => {
        setPurchaseLoad(true);
        try {
            const enc: any = encryptData({
                basicid: openInvestor,
                profileid: openInvestorAcc,
            });
            let result = await getCartDetails(enc);
            //console.log("Cart Data ", result?.data);
            //console.log(proceedCartData)
            if (proceedCartData.length === 0)
                setPurCartTotal(0)
            if (result?.data.length === undefined || result?.data === "") {
                //console.log("object")
                setPurchaseCartData([])
            }
            else {
                setPurchaseCartData(result?.data)
            }
            // console.log(mohMember);
            setPurchaseLoad(false);

        } catch (error: any) {
            console.log(error);
            setPurchaseLoad(false);
            toastAlert("error", error);
        }
    }
    const fetchSystematicCartDetails = async () => {
        setSystematicLoad(true);
        try {
            const enc: any = encryptData({
                basicId: openInvestor,
                profileid: openInvestorAcc,
            });
            let result = await getSystematicCartDetails(enc);
            //console.log("Cart Data ", result?.data);
            if (result?.data.length === undefined || result?.data === "") {
                //console.log("object")
                setSystematicCartData([])
            }
            else {
                setSystematicCartData(result?.data);
                //console.log(result?.data, "All Data");
            }
            //setPurchaseCartData(result?.data)
            // console.log(mohMember);
            setSystematicLoad(false);

        } catch (error: any) {
            console.log(error);
            setSystematicLoad(false);
            toastAlert("error", error);

        }
    }
    const addToProceed = (value: any) => {
        console.log(value)
        if (proceedCartData.length >= 5) {
            let check: any = document.getElementById(value?.cartId);
            check.checked = false;
            toastAlert("warn", "Can not Add more than 5 for purchasing schemes");
        } else {
            console.log(value)
            setProceedCartData([...proceedCartData, value]);
        }
    }

    const removeFromProceed = (id: any) => {
        console.log(proceedCartData);
        console.log(id);
        let arr = proceedCartData.filter((item: any) => {
            return item?.cartId !== id;
        })
        console.log(arr);
        setProceedCartData(arr);
    }

    const addToProceedSys = (value: any) => {
        //console.log(value)
        if (proceedSysCartData.length >= 5) {
            let check: any = document.getElementById(value?.id);
            check.checked = false;
            toastAlert("warn", "Can not Add more than 5 for purchasing schemes");
        } else {
            setProceedSysCartData([...proceedSysCartData, value]);
            //console.log(proceedSysCartData);
        }
    }

    const removeFromProceedSys = (id: any) => {
        // console.log(proceedCartData);
        // console.log(id);
        let arr = proceedSysCartData.filter((item: any) => {
            return item?.id !== id;
        })
        //console.log(arr);
        setProceedSysCartData(arr);
    }
    useEffect(() => {
        let total = 0;
        proceedCartData.map((el: any) => {
            total = total + Number(el.amount);
        })
        console.log("total ", total);
        setPurCartTotal(total);
    }, [proceedCartData])

    useEffect(() => {
        let total = 0;
        proceedSysCartData.map((el: any) => {
            total = total + Number(el.Amount);
        })
        //console.log("total ", total);
        setSysCartTotal(total);
    }, [proceedSysCartData])

    const deleteCartItem = async (id: any) => {
        try {
            const enc: any = encryptData(id, true);
            let result = await removingFromCart(enc);
            // console.log("Cart Data ", result?.data);
            let arr = proceedCartData.filter((item: any) => {
                return item?.cartId !== id;
            })
            console.log(arr);
            setProceedCartData(arr);
            setOpenDelete(false);
            setDeletePurCartId('');
            //setPurchaseCartData(result?.data)
            toastAlert("success", "Removed from Cart Successfully !");
            await fetchCartDetails();
            // console.log(mohMember);
            //setPurchaseLoad(false);

        } catch (error: any) {
            console.log(error);
            setOpenDelete(false);
            setDeletePurCartId('');
            //setPurchaseLoad(false);
            toastAlert("error", error);
        }
    }

    const deleteSysCartItem = async (id: any) => {
        try {
            const enc: any = encryptData(id, true);
            let result = await removingFromSysCart(enc);
            // console.log("Cart Data ", result?.data);
            let arr = proceedSysCartData.filter((item: any) => {
                return item?.id !== id;
            })
            //console.log(arr);
            setProceedSysCartData(arr);
            setOpenDelete(false);
            setDeleteSysCartId('');
            //setPurchaseCartData(result?.data)
            toastAlert("success", "Removed from Cart Successfully !");
            await fetchSystematicCartDetails();
            // console.log(mohMember);
            //setPurchaseLoad(false);
        } catch (error: any) {
            console.log(error);
            setOpenDelete(false);
            setDeleteSysCartId('');
            //setPurchaseLoad(false);
            toastAlert("error", error);
        }
    }

    const handlePayment = async () => {
        if (purCartTotal === 0) {
            toastAlert("warn", "Cart total must be greater than Zero to Proceed");
        }
        else {
            // console.log(proceedCartData);
            // console.log(user);
            setContinueLoading(true);
            setBankData(bankList.filter((item: any) => {
                return item?.basicid === openInvestor;
            }))
            arr = bankList.filter((item: any) => {
                return item?.basicid === openInvestor;
            });
            //console.log(arr);

            try {
                const enc: any = encryptData(arr[0]?.bankid, true);
                let result = await getBankPaymentModes(enc);
                // console.log("Payment Data ", result?.data);
                setPaymentData(result?.data)
                setOpenPurchase(true);
                // console.log(mohMember);
                //setPurchaseLoad(false);
                //     let banList:any=[];
                //     arr.map((ele)=>{
                //     let a={
                //         basicid: ele?.basicid,
                //         bankid: ele?.bankid,
                //         bankName: ele?.bankName,
                //         accountNo: ele?.accountNo
                //     }
                //     banList.push(a);
                //     })
                // console.log(banList);
                // setBankData([banList]);
                setContinueLoading(false);
            } catch (error: any) {
                console.log(error);
                //setPurchaseLoad(false);
                toastAlert("error", error);
                setContinueLoading(false);
            }
        }
    }

    const handleOTPSend = async () => {
        if (sysCartTotal === 0) {
            toastAlert("warn", "Cart total must be greater than Zero to Proceed");
        }
        else {
            setContinueLoading(true);
            try {
                let obj = {
                    userid: user?.userid,
                    mobile: user?.mobile,
                    name: user?.name,
                    txntype: 'multiple_transactions',
                    rmname: user?.RmName,
                    amt: sysCartTotal
                }
                // console.log(proceedSysCartData);
                console.log(obj);
                const enc: any = encryptData(obj);
                let result = await sysSendOTP(enc);
                // console.log("OTP Sent ", result?.data);
                toastAlert("success", "OTP Sent Successfully");
                setPaymentData(result?.data)
                setOpenTransfer(true);
                setContinueLoading(false);
            } catch (error: any) {
                console.log(error);
                toastAlert("error", error);
                setContinueLoading(false);
            }
        }
    }

    return (
        <>
            <Box className="container">
                <Box className="row p-1 mx-0 mb-2" css={{ backgroundColor: "#b3daff", borderRadius: "8px" }}>
                    <Box className="col-lg-6">
                        <SelectMenu
                            value={openInvestor}
                            items={investorList || []}
                            label={"Investor"}
                            bindValue={"basicid"}
                            bindName={"memberName"}
                            onChange={(e) =>
                                setOpenInvestor(e?.basicid || "")
                            } />
                    </Box>
                    <Box className="col-lg-6">
                        <SelectMenu value={openInvestorAcc}
                            items={investorAccountList}
                            label={"Investor Account"}
                            bindValue={"profileID"}
                            bindName={"InvestProfile"}
                            onChange={(e) =>
                                setOpenInvestorAcc(e?.profileID || "")
                            } />
                    </Box>
                </Box>
                <Box className="row justify-content-end" >
                    <Box className="col-auto">
                        <Box className="row justify-content-end" >
                            <Box className="col-auto">
                                <Box className="col-lg-auto px-0" css={{ borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }}>
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-end px-2 pt-1" css={{ backgroundColor: "#b3daff", color: "#0861b6" }} >Total Amount:<span></span></Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-end text-black px-2 pb-1" css={{ backgroundColor: "#b3daff" }} >&#x20B9; {switchType === "Purchase" ? purCartTotal.toLocaleString("en-IN") : sysCartTotal.toLocaleString("en-IN")}</Text>
                                </Box>
                            </Box>
                            <Box className="col-auto my-auto">
                                <Button
                                    className="p-2"
                                    color="yellowGroup"
                                    size="md"
                                    disabled={switchType === "Purchase" ? (purCartTotal === 0 || continueLoading) : (sysCartTotal === 0 || continueLoading)}
                                    onClick={() => { switchType === "Purchase" ? handlePayment() : handleOTPSend() }}
                                >
                                    <Text
                                        // @ts-ignore
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={styles.button}>
                                        {continueLoading ? "Loading..." : "Continue"}
                                    </Text>
                                </Button>
                            </Box>
                        </Box>
                    </Box>
                </Box>
                {purchaseLoad ?
                    <Loader />
                    :
                    <StyledTabs defaultValue="Purchase">
                        <TabsList
                            aria-label="Manage your account"
                            className="justify-content-left"
                            css={{
                                // borderBottom: "1px solid var(--colors-gray7)",
                                overflowX: "auto",
                            }}
                        >
                            {CartTabs.map((item, index) => {
                                return (
                                    <TabsTrigger
                                        value={item.value}
                                        className="tabs me-3"
                                        key={item.value}
                                        css={{ border: "solid 1px var(--colors-blue1)", height: "25px" }}
                                        onClick={(e: any) => {
                                            setSwitchType(item.value);
                                            setProceedCartData([]);
                                            setProceedSysCartData([]);
                                        }}
                                    >
                                        {/* @ts-ignore */}
                                        <Text size="h5">
                                            {item.title}{" "}
                                            ({item.title === "Purchase" ? (purchaseCartData?.length) : (systematicCartData?.length)})
                                        </Text>
                                    </TabsTrigger>
                                );
                            })}
                        </TabsList>
                        <TabsContent value="Purchase">
                            {purchaseCartData.map((ele: any) => {
                                // console.log(ele, "ele purchase")
                                return (
                                    <Card className="mt-0">
                                        <Box className="row mb-0 border-bottom border-warning mx-0">
                                            <Box className="col-lg-1">
                                                <Box>
                                                    {/* @ts-ignore */}
                                                    <img
                                                        width={58}
                                                        height={50}
                                                        src={ele?.goalImg}
                                                        alt="Goal"
                                                    />
                                                </Box>
                                            </Box>
                                            <Box className="col-lg-11">
                                                {/* @ts-ignore */}
                                                <Text size="h4" css={{ mt: 12 }}>
                                                    {ele?.goalName}
                                                </Text>
                                            </Box>
                                        </Box>
                                        <Box className="row">
                                            <Box className="col-lg-1"></Box>
                                            <Box className="col-lg-11">
                                                <Box className="row">
                                                    <Box className="col-lg-11 col-md-10">
                                                        <Box className="row">
                                                            <Box className="col-6 col-md-4 col-lg-4 m-2">
                                                                {/* @ts-ignore */}
                                                                <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.schemeName}</Text>
                                                                {/* @ts-ignore */}
                                                                <Text size="h6">Folio : {ele?.folioNo}</Text>
                                                                {/* @ts-ignore */}
                                                                <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                                                            </Box>
                                                            <Box className="col-6 col-md-4 col-lg-2 m-2">
                                                                {/* @ts-ignore */}
                                                                <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.tranType === "NOR" ? <>Purchase</> : ele?.tranType} Amount</Text>
                                                                {/* @ts-ignore */}
                                                                <Text size="h6">&#x20B9;{Number(ele?.amount).toLocaleString("en-IN")}</Text>
                                                            </Box>
                                                            <Box className="col-6 col-md-4 col-lg-2 m-2">
                                                                {/* @ts-ignore */}
                                                                <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.tranType === "ISIP" ? <>Start Date</> : <>Date</>}</Text>
                                                                {/* @ts-ignore */}
                                                                <Text size="h6">{ele?.sipStartDate}</Text>
                                                            </Box>
                                                            <Box className="col-6 col-md-4 col-lg-2 m-2">
                                                                {ele?.tranType === "ISIP" ? <>
                                                                    {/* @ts-ignore */}
                                                                    <Text size="h6" css={{ color: "var(--colors-blue1)" }}>End Date</Text>
                                                                    {/* @ts-ignore */}
                                                                    <Text size="h6">{ele?.sipEndDate === null ? '-' : ele?.sipEndDate}</Text>
                                                                </> : <></>}
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                    <Box className="col-lg-1 col-md-2">
                                                        <Box className="mt-3 mx-3 mx-md-0 px-0">
                                                            <GroupBox>
                                                                <Checkbox
                                                                    label=""
                                                                    id={ele?.cartId}
                                                                    name=""
                                                                    value={ele?.cartId}
                                                                    style={{ marginTop: "8px" }}
                                                                    onChange={(e: any) => { e.target.checked ? addToProceed(ele) : removeFromProceed(ele?.cartId) }}
                                                                />
                                                                <Button style={{ backgroundColor: "white", margin: "0px" }} onClick={() => { setDeletePurCartId(ele?.cartId); setOpenDelete(true); }}>
                                                                    <DeleteIcon color="orange" className="mt-1" />
                                                                </Button>
                                                            </GroupBox>
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </Box>
                                    </Card>
                                )
                            })}
                            <Box className="row mt-2 ">
                                <Box className="col-lg-12 col-md-12 col-sm-12 mb-3 ">
                                    <Text className="px-2 py-2 rounded"
                                        // @ts-ignore
                                        weight="normal"
                                        size="h6"
                                        css={{ color: "var(--colors-blue1)", border: "1px solid #4d7ea8 !important" }}
                                    >
                                        <span style={{ fontWeight: "bold" }}>Note: </span>
                                        If you invest before 2 pm, the transaction will be considered to have been initiated on the same day. If you invest after 2 pm, the transaction will be considered to have been initiated on the next working day. For liquid funds, the cut-off time is 1 pm.

                                        By clicking on the continue button, you confirm that you are aware of the details of thr scheme related document and each aspect of the investment.
                                    </Text>
                                </Box>
                            </Box>
                            <Box className="row mx-1 my-1 pb-2 px-0 justify-content-end">
                                <Box className="col-lg-auto px-0" css={{ borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }}>
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-end px-2 pt-1" css={{ backgroundColor: "#b3daff", color: "#0861b6" }} >Total Amount:</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-end text-black px-2 pb-1" css={{ backgroundColor: "#b3daff" }} >&#x20B9; {purCartTotal.toLocaleString("en-IN")}</Text>
                                </Box>
                            </Box>
                            <Box className="row justify-content-end mx-1">
                                {/* <Box className="col-auto"></Box> */}
                                <Button
                                    className="p-2"
                                    color="yellowGroup"
                                    size="md"
                                    disabled={(purCartTotal === 0 || continueLoading) ? true : false}
                                    onClick={() => { handlePayment(); }}
                                >
                                    <Text
                                        // @ts-ignore
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={styles.button}>
                                        {continueLoading ? "Loading..." : "Continue"}
                                    </Text>
                                </Button>
                            </Box>
                        </TabsContent>

                        <TabsContent value="Transfer">
                            {/* <Box className="row justify-content-end" >
                                <Box className="col-auto" css={{ position: "absolute" }}>
                                    <Button
                                        css={{ position: "relative", zIndex: "9999", top: "-35px" }}
                                        className="p-2"
                                        color="yellowGroup"
                                        size="md"
                                        disabled={(sysCartTotal === 0 || continueLoading) ? true : false}
                                        onClick={() => { handleOTPSend() }}
                                    >
                                        <Text
                                            // @ts-ignore
                                            size="h6"
                                            //@ts-ignore
                                            color="gray8"
                                            className={styles.button}
                                        >
                                            {continueLoading ? "Loading..." : "Continue"}
                                        </Text>
                                    </Button>
                                </Box>
                            </Box> */}
                            <Card className="mt-0">
                                <Box className="row mb-0 border-bottom border-warning mx-0">
                                    <Box className="col-lg-1">
                                        <Box>
                                            {/* @ts-ignore */}
                                            <img
                                                width={58}
                                                height={50}
                                                src={"/STP_img.png"}
                                                alt="STP"
                                            />
                                        </Box>
                                    </Box>
                                    <Box className="col-lg-11">
                                        {/* @ts-ignore */}
                                        <Text size="h4" css={{ mt: 12 }}>
                                            STP
                                        </Text>
                                    </Box>
                                </Box>
                                {systematicCartData.map((ele: any) => {
                                    if (ele?.Trxn_Type === "STP") {
                                        return (
                                            <>
                                                <Box className="row">
                                                    <Box className="col-lg-1"></Box>
                                                    <Box className="col-lg-11">
                                                        <Box className="row">
                                                            <Box className="col-lg-11 col-md-10">
                                                                <Box className="row">
                                                                    <Box className="col-6 col-md-4 col-lg-4 mt-2 ms-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.Scheme_name}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">Folio : {ele?.FolioNo}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.Trxn_Type} Amount</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">&#x20B9;{ele?.Amount}</Text>
                                                                    </Box>

                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Date</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.TrxnDt}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-auto mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>No. Of Months</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.No_of_Installment}</Text>
                                                                    </Box>
                                                                </Box>
                                                            </Box>
                                                            <Box className="col-lg-1 col-md-2">
                                                                <Box className="mt-3 mx-3 mx-md-0 px-0">
                                                                    <GroupBox>
                                                                        <Checkbox
                                                                            label=""
                                                                            id={ele?.id}
                                                                            name=""
                                                                            value={ele?.SchemeID}
                                                                            style={{ marginTop: "8px" }}
                                                                            onChange={(e: any) => { e.target.checked ? addToProceedSys(ele) : removeFromProceedSys(ele?.id) }}
                                                                        />
                                                                        <Button style={{ backgroundColor: "white", margin: "0px" }} onClick={() => { setDeleteSysCartId(ele?.id); setOpenDelete(true); }}>
                                                                            <DeleteIcon color="orange" className="mt-1" />
                                                                        </Button>
                                                                    </GroupBox>
                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </>
                                        )
                                    }
                                }
                                )}
                            </Card>
                            <Card className="mt-0">
                                <Box className="row mb-0 border-bottom border-warning mx-0">
                                    <Box className="col-lg-1">
                                        <Box>
                                            {/* @ts-ignore */}
                                            <img
                                                width={58}
                                                height={50}
                                                src={"/SIP_img.png"}
                                                alt="SIP"
                                            />
                                        </Box>
                                    </Box>
                                    <Box className="col-lg-11">
                                        {/* @ts-ignore */}
                                        <Text size="h4" css={{ mt: 12 }}>
                                            SIP
                                        </Text>
                                    </Box>
                                </Box>
                                {systematicCartData.map((ele: any) => {
                                    if (ele?.Trxn_Type === "SIP") {
                                        console.log(ele, "sip data");

                                        return (
                                            <>
                                                <Box className="row">
                                                    <Box className="col-lg-1"></Box>
                                                    <Box className="col-lg-11">
                                                        <Box className="row">
                                                            <Box className="col-lg-11 col-md-10">
                                                                <Box className="row">
                                                                    <Box className="col-6 col-md-4 col-lg-4 mt-2 ms-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.Scheme_name}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">Folio : {ele?.FolioNo}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.Trxn_Type} Amount</Text>
                                                                        {/* @ts-ignore */}
                                                                        {/* <Text size="h6">&#x20B9;{ele?.Amount}</Text> */}
                                                                        <Text size="h6"> &#x20B9;{ele.Amount}</Text>
                                                                    </Box>
                                                                    {/* <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore 
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Min Amount</Text>
                                                                        {/* @ts-ignore }
                                                                        <Text size="h6">&#x20B9;0</Text>
                                                                    </Box> */}
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Start Date</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.Start_Date}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>End Date</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.End_Date}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-auto mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>No. Of Months</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.No_of_Installment}</Text>
                                                                    </Box>
                                                                </Box>
                                                            </Box>
                                                            <Box className="col-lg-1 col-md-2">
                                                                <Box className="mt-3 mx-3 mx-md-0 px-0">
                                                                    <GroupBox>
                                                                        <Checkbox
                                                                            label=""
                                                                            id={ele?.id}
                                                                            name=""
                                                                            value={ele?.SchemeID}
                                                                            style={{ marginTop: "8px" }}
                                                                            onChange={(e: any) => { e.target.checked ? addToProceedSys(ele) : removeFromProceedSys(ele?.id) }}
                                                                        />
                                                                        <Button style={{ backgroundColor: "white", margin: "0px" }} onClick={() => { setDeleteSysCartId(ele?.id); setOpenDelete(true); }}>
                                                                            <DeleteIcon color="orange" className="mt-1" />
                                                                        </Button>
                                                                    </GroupBox>
                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </>
                                        )
                                    }
                                }
                                )}
                            </Card>
                            <Card className="mt-0">
                                <Box className="row mb-0 border-bottom border-warning mx-0">
                                    <Box className="col-lg-1">
                                        <Box>
                                            {/* @ts-ignore */}
                                            <img
                                                width={58}
                                                height={50}
                                                src={"/Switch_img.png"}
                                                alt="Switch"
                                            />
                                        </Box>
                                    </Box>
                                    <Box className="col-lg-11">
                                        {/* @ts-ignore */}
                                        <Text size="h4" css={{ mt: 12 }}>
                                            Switch
                                        </Text>
                                    </Box>
                                </Box>
                                {systematicCartData.map((ele: any) => {
                                    if (ele?.Trxn_Type === "SWITCH") {
                                        return (
                                            <>
                                                <Box className="row">
                                                    <Box className="col-lg-1"></Box>
                                                    <Box className="col-lg-11">
                                                        <Box className="row">
                                                            <Box className="col-lg-11 col-md-10">
                                                                <Box className="row">
                                                                    <Box className="col-6 col-md-4 col-lg-4 mt-2 ms-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.Scheme_name}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">Folio : {ele?.FolioNo}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.Trxn_Type} Amount</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6"> &#x20B9;{ele.Amount}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Unit</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.Units}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2"></Box>
                                                                    <Box className="col-6 col-md-4 col-lg-auto mt-2"></Box>
                                                                </Box>
                                                            </Box>
                                                            <Box className="col-lg-1 col-md-2">
                                                                <Box className="mt-3 mx-3 mx-md-0 px-0">
                                                                    <GroupBox>
                                                                        <Checkbox
                                                                            label=""
                                                                            id={ele?.id}
                                                                            name=""
                                                                            value={ele?.SchemeID}
                                                                            style={{ marginTop: "8px" }}
                                                                            onChange={(e: any) => { e.target.checked ? addToProceedSys(ele) : removeFromProceedSys(ele?.id) }}
                                                                        />
                                                                        <Button style={{ backgroundColor: "white", margin: "0px" }} onClick={() => { setDeleteSysCartId(ele?.id); setOpenDelete(true); }}>
                                                                            <DeleteIcon color="orange" className="mt-1" />
                                                                        </Button>
                                                                    </GroupBox>
                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </>
                                        )
                                    }
                                }
                                )}
                            </Card>
                            <Card className="mt-0">
                                <Box className="row mb-0 border-bottom border-warning mx-0">
                                    <Box className="col-lg-1">
                                        <Box>
                                            {/* @ts-ignore */}
                                            <img
                                                width={58}
                                                height={50}
                                                src={"/SWP_img.png"}
                                                alt="Switch"
                                            />
                                        </Box>
                                    </Box>
                                    <Box className="col-lg-11">
                                        {/* @ts-ignore */}
                                        <Text size="h4" css={{ mt: 12 }}>
                                            SWP
                                        </Text>
                                    </Box>
                                </Box>
                                {systematicCartData.map((ele: any) => {
                                    if (ele?.Trxn_Type === "SWP") {
                                        return (
                                            <>
                                                <Box className="row">
                                                    <Box className="col-lg-1"></Box>
                                                    <Box className="col-lg-11">
                                                        <Box className="row">
                                                            <Box className="col-lg-11 col-md-10">
                                                                <Box className="row">
                                                                    <Box className="col-6 col-md-4 col-lg-4 mt-2 ms-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.toSchemeName}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">Folio : {ele?.SellFolioNo}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.Trxn_Type} Amount</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">&#x20B9;{ele.Amount}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Unit</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.Units}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2"></Box>
                                                                    <Box className="col-6 col-md-4 col-lg-auto mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>No. Of Months</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.No_of_Installment}</Text>
                                                                    </Box>
                                                                </Box>
                                                            </Box>
                                                            <Box className="col-lg-1 col-md-2">
                                                                <Box className="mt-3 mx-3 mx-md-0 px-0">
                                                                    <GroupBox>
                                                                        <Checkbox
                                                                            label=""
                                                                            id={ele?.id}
                                                                            name=""
                                                                            value={ele?.SchemeID}
                                                                            style={{ marginTop: "8px" }}
                                                                            onChange={(e: any) => { e.target.checked ? addToProceedSys(ele) : removeFromProceedSys(ele?.id) }}
                                                                        />
                                                                        <Button style={{ backgroundColor: "white", margin: "0px" }} onClick={() => { setDeleteSysCartId(ele?.id); setOpenDelete(true); }}>
                                                                            <DeleteIcon color="orange" className="mt-1" />
                                                                        </Button>
                                                                    </GroupBox>
                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </>
                                        )
                                    }
                                }
                                )}
                            </Card>
                            <Card className="mt-0">
                                <Box className="row mb-0 border-bottom border-warning mx-0">
                                    <Box className="col-lg-1">
                                        <Box>
                                            {/* @ts-ignore */}
                                            <img
                                                width={58}
                                                height={50}
                                                src={"/Redemption_img.png"}
                                                alt="Redemption"
                                            />
                                        </Box>
                                    </Box>
                                    <Box className="col-lg-11">
                                        {/* @ts-ignore */}
                                        <Text size="h4" css={{ mt: 12 }}>
                                            Redemption
                                        </Text>
                                    </Box>
                                </Box>
                                {systematicCartData.map((ele: any) => {
                                    if (ele?.Trxn_Type === "R") {
                                        //console.log(ele, "redeem");
                                        return (
                                            <>
                                                <Box className="row">
                                                    <Box className="col-lg-1"></Box>
                                                    <Box className="col-lg-11">
                                                        <Box className="row">
                                                            <Box className="col-lg-11 col-md-10">
                                                                <Box className="row">
                                                                    <Box className="col-6 col-md-4 col-lg-4 mt-2 ms-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.toSchemeName}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">Folio : {ele?.SellFolioNo}</Text>
                                                                        {/* @ts-ignore */}
                                                                        <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>{ele?.Trxn_Type === 'R' ? 'Redeem' : ele?.Trxn_Type} Amount</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">&#x20B9;{ele.Amount}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2">
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Unit</Text>
                                                                        {/* @ts-ignore */}
                                                                        <Text size="h6">{ele?.Units}</Text>
                                                                    </Box>
                                                                    <Box className="col-6 col-md-4 col-lg-2 mt-2"></Box>
                                                                    <Box className="col-6 col-md-4 col-lg-auto mt-2"></Box>
                                                                </Box>
                                                            </Box>
                                                            <Box className="col-lg-1 col-md-2">
                                                                <Box className="mt-3 mx-3 mx-md-0 px-0">
                                                                    <GroupBox>
                                                                        <Checkbox
                                                                            label=""
                                                                            id={ele?.id}
                                                                            name=""
                                                                            value={ele?.SchemeID}
                                                                            style={{ marginTop: "8px" }}
                                                                            onChange={(e: any) => { e.target.checked ? addToProceedSys(ele) : removeFromProceedSys(ele?.id) }}
                                                                        />
                                                                        <Button style={{ backgroundColor: "white", margin: "0px" }} onClick={() => { setDeleteSysCartId(ele?.id); setOpenDelete(true); }}>
                                                                            <DeleteIcon color="orange" className="mt-1" />
                                                                        </Button>
                                                                    </GroupBox>
                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </>
                                        )
                                    }
                                }
                                )}
                            </Card>

                            <Box className="row mt-2 ">
                                <Box className="col-lg-12 col-md-12 col-sm-12 mb-3 ">
                                    <Text className="px-2 py-2 rounded"
                                        // @ts-ignore
                                        weight="normal"
                                        size="h6"
                                        css={{ color: "var(--colors-blue1)", border: "1px solid #4d7ea8 !important" }}
                                    >
                                        <span style={{ fontWeight: "bold" }}>Note: </span>
                                        If you invest before 2 pm, the transaction will be considered to have been initiated on the same day. If you invest after 2 pm, the transaction will be considered to have been initiated on the next working day. For liquid funds, the cut-off time is 1 pm.

                                        By clicking on the continue button, you confirm that you are aware of the details of thr scheme related document and each aspect of the investment.
                                    </Text>
                                </Box>
                            </Box>
                            <Box className="row mx-1 my-1 pb-1 px-0 justify-content-end">
                                <Box className="col-lg-auto px-0" css={{ borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }}>
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-end px-2 pt-1" css={{ backgroundColor: "#b3daff", color: "#0861b6" }} >Total Amount:</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-end text-black px-2 pb-1" css={{ backgroundColor: "#b3daff" }} >&#x20B9; {sysCartTotal}</Text>
                                </Box>
                            </Box>
                            <Box className="row justify-content-end mx-1">
                                {/* <Box className="col-auto"></Box> */}
                                <Button
                                    className="p-2"
                                    color="yellowGroup"
                                    size="md"
                                    disabled={(sysCartTotal === 0 || continueLoading) ? true : false}
                                    onClick={() => { handleOTPSend() }}
                                >
                                    <Text
                                        // @ts-ignore
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={styles.button}
                                    >
                                        {continueLoading ? "Loading..." : "Continue"}
                                    </Text>
                                </Button>
                            </Box>
                        </TabsContent>
                    </StyledTabs>}
            </Box>
            <DialogModal
                open={openDelete}
                setOpen={setOpenDelete}
                hideCloseBtn={true}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "30%" },
                }}
            >
                <Box className="col-md-12 col-sm-12 col-lg-12">
                    <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                        <Box className="row justify-content-start">
                            <Box className="col-auto">
                                <Text css={{ color: "var(--colors-blue1)" }}>Delete - Cart Item</Text></Box>
                        </Box>
                    </Box>
                    <Box className="modal-body p-3">
                        <Box className="my-1 mx-2">
                            <Box css={{ padding: "0.3rem", fontSize: "0.9rem" }}>
                                Are you sure?
                            </Box>
                            <Box css={{ pt: 10 }} className="text-end">
                                <Button color="yellow" onClick={() => { setDeletePurCartId(''); setDeleteSysCartId(''); setOpenDelete(false) }}>
                                    Cancel
                                </Button>
                                <Button color="yellow" onClick={() => {
                                    if (deletePurCartId !== "") {
                                        deleteCartItem(deletePurCartId)
                                        console.log(cartCount);
                                        let iCartCount = cartCount - 1;
                                        setCartCount(iCartCount);
                                    }
                                    else if (deleteSysCartId !== "") {
                                        deleteSysCartItem(deleteSysCartId);
                                        let iCartCount = cartCount - 1;
                                        setCartCount(iCartCount);
                                    }
                                }}>
                                    Confirm
                                </Button>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </DialogModal>
            <DialogModal
                open={openPurchase}
                setOpen={setOpenPurchase}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "40%" },
                }}
            >
                <PurchaseModal paymentData={paymentData} setOpen={setOpenPurchase} bankData={bankData} profileID={openInvestorAcc} basicID={openInvestor} proceedCartData={proceedCartData} />
            </DialogModal>
            <DialogModal
                open={openTransfer}
                setOpen={setOpenTransfer}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "40%" },
                }}
            >
                <TransferModal sysCartTotal={sysCartTotal} proceedSysCartData={proceedSysCartData} profileID={openInvestorAcc} basicID={openInvestor} setOpenTransfer={setOpenTransfer} fetchSystematicCartDetails={fetchSystematicCartDetails} setOpenSuccess={setOpenSuccess} setMessageArr={setMessageArr} />
            </DialogModal>
            <DialogModal
                open={openSuccess}
                setOpen={setOpenSuccess}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "35%" },
                }}
            >
                {/* @ts-ignore */}
                <Box className="col-md-12 col-sm-12 col-lg-12">
                    <Box className="modal-body modal-body-scroll">
                        <Box className="row justify-content-center">
                            <Box className="col-lg-12 text-center" css={{ py: 50 }}>
                                {messageArr.map((ele: any) => {
                                    return (
                                        <Box className="row justify-content-center m-2">
                                            <Box className='col-md-2 p-2'>
                                                {ele?.type === "success" ? <CheckCircleFill color="green" size="2rem" /> : <CrossCircleFill color="red" size="2rem" />}
                                            </Box>
                                            <Box className='col-md-2'>
                                                <Text size="h5" className="text-center mt-2 p-2" css={{ color: "var(--colors-blue1)" }}>{String(ele?.type).toUpperCase()}</Text>
                                            </Box>
                                            {/* @ts-ignore */}
                                            <Box className='col-md-8'>
                                                <Text size="h5" className="text-center mt-2" css={{ color: "var(--colors-blue1)" }}>{ele?.msg}</Text>
                                            </Box>
                                        </Box>
                                    )
                                })}
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </DialogModal>
        </>
    )
}

export default CartPage