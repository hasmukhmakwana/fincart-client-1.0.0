import create from "zustand";

import { investorType, investorACType, bankListType } from "./CartTypes"

interface StoreTypes {
  investorList: investorType[];
  investorACList: investorACType[];
  bankList: bankListType[];
  transactionNo:string;
  setInvestorList: (payload: investorType[]) => void;
  setInvestorACList: (payload: investorACType[]) => void;
  setBankList: (payload: bankListType[]) => void;
  setTransactionNo: (payload:string)=>void;
}

const useCartStore = create<StoreTypes>((set) => ({
  //* initial state
 
  investorList: [],
  investorACList: [],
  bankList:[],
  transactionNo:'',

 
  setInvestorList: (payload) =>
    set((state) => ({
      ...state,
      investorList: payload,
    })),

  setInvestorACList: (payload) =>
    set((state) => ({
      ...state,
      investorACList: payload,
    })),
    
  setBankList: (payload) =>
  set((state) => ({
    ...state,
  bankList: payload,
  })),
  
  setTransactionNo: (payload) =>
  set((state) => ({
    ...state,
  transactionNo: payload,
  })),
}));

export default useCartStore;