import React, { useEffect, useState } from 'react'
import CartPage from './components/CartPage';
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout, { toastAlert } from 'App/shared/components/Layout';
import {
  getAllMemberPurchaseDetails
} from "App/api/mandate"
import { encryptData, getUser } from "App/utils/helpers";
import useCartStore from "./store";
import Loader from "App/shared/components/Loader";



const Cart = () => {
  const user: any = getUser();
  const [loading, setLoading] = useState<boolean>(false);
  const {
    setInvestorACList,
    setInvestorList,
    setBankList
  } = useCartStore();

  const fetchMemberList = async () => {
    setLoading(true);
    try {
      const enc: any = encryptData({
        basicId: user?.basicid,
        fundid: "0",
      });
      let result = await getAllMemberPurchaseDetails(enc);
      setInvestorList(result?.data?.memberList);
      setInvestorACList(result?.data?.profileList);
      setBankList(result?.data?.bankList);
      setLoading(false);

    } catch (error: any) {
      console.log(error);
      setLoading(false);
      toastAlert("error", error);

    }
  };
  useEffect(() => {
    (async () => {
      await fetchMemberList();
    })();
  }, []);
  return (
    <Layout>
      <PageHeader title="Cart" rightContent={<></>} />
      {loading ?
        <Loader />
        :
        <CartPage />
      }
    </Layout>
  )
}

export default Cart