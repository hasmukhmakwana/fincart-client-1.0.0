
export type investorType = {
    memberName: string;
    basicid: string;
    isNominee: boolean;
}


export type investorACType = {
    profileID: string;
    InvestProfile: string;
    basicid: string;
}

export type bankListType={
    basicid: string;
    bankid: string;
    bankName: string;
    accountNo: string;
}