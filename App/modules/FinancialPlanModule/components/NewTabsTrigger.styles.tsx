//@ts-nocheck
import { blue, violet } from "@radix-ui/colors";
import * as TabsPrimitive from "@radix-ui/react-tabs";
import { styled } from "../../../theme/stitches.config";


const StyledTrigger = styled(TabsPrimitive.Trigger, {
  all: "unset",
  fontFamily: "inherit",
  backgroundColor: "white",
  padding: "0 20px",
  height: 45,
  flex: 1,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  fontSize: 15,
  lineHeight: 1,
  color: blue.blue11,
  userSelect: "none",
  '@bp0': {
    overflowX: "auto",
  },
  "&:first-child": { borderTopLeftRadius: 6 },
  "&:last-child": { borderTopRightRadius: 6 },
  "&:hover": { color: violet.violet11 },
  '&[data-state="active"]': {
    color: "orange",
    boxShadow: "inset 0 -1px 0 0 currentColor, 0 1px 0 0 currentColor",
  },
  // "&:focus": { position: "relative", boxShadow: `0 0 0 2px black` },
  "&.tabsCards": {
    height: 35,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "$primary",
    borderRadius: 10,
    marginLeft: 10,
    flex: "none",
    paddingBottom: 0,
    paddingTop: 0,
    "& .iconTick": {
      fill: "orange !important",
      marginRight: 15,
    },
    '&[data-state="active"]': {
      // backgroundColor: "orange",
      color: "orange",
      boxShadow: "none",
      borderBottom: "2px solid #4d7ea8 !important",
      "& .iconTick": {
        fill: "orange !important",
      },
      "& p": {
        color: "orange !important",
      }
    },
    '&[data-state="inactive"]': {
      // backgroundColor: "orange",
      borderBottom: "2px solid #b3daff !important",
    },
  },
});

// Exports
export const TabsTrigger = StyledTrigger;

