import Box from "App/ui/Box/Box";
import React, { useEffect, useState } from "react";
import Text from "App/ui/Text/Text";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import { BasicDetails, Expenses, Income, NetAssetEntry, Goals } from "./Forms";
import { toastAlert } from 'App/shared/components/Layout'
import style from "../FinancialPlan.module.scss"
import Goal from "App/icons/Goal";
import useFinancialPlanStore from "../store";
import { fetchIncomeDetails, fetchAllGoalSchemeUAT, getBasicDetails } from "App/api/financialPlan"
import { encryptData, getUser } from "App/utils/helpers";
import { fetchExpenseDetails } from "App/api/financialPlan"
import { fetchAssetsDetails } from "App/api/financialPlan";

let recentTabs = [
    {
        title: "General",
        value: "BasicDetails",
    },
    {
        title: "Goals",
        value: "Goals",
    },
    {
        title: "Income",
        value: "Income",
    },
    {
        title: "Asset",
        value: "NetAssetEntry",
    },
    {
        title: "Expenses",
        value: "Expenses",
    },
];
type SaveTypes = {
    fetchPlanId: () => void;
};
const FinancialPlanPage = ({ fetchPlanId }: SaveTypes) => {
   
    const {
        // loading,
        localUser,
        // relationList,
        // inflowList,
        // frequencyList,
        // outflowList,
        localPlanId,
        // inDropdownList,
        // insurancePolicyList,
        // lnDropdownList,
        // dgsDropdownList,
        // basicDetail,
        setAllGoals,
        setIncomeDetailsList,
        setExpenseDetailsList,
        setBasicDetail,
        // setOtDetailsList,
        // setUlipDetailsList,
        // setLnDetailsList,
        // setInDetailsList,
        // setMfDetailsList,
        // setINDropdownList,
        setRetireAge,
        setLoader
    } = useFinancialPlanStore();


    const fetchAllGoalScheme = async () => {
        try {
            setLoader(true);
            const enc: any = encryptData(localUser,true);
            let result = await fetchAllGoalSchemeUAT(enc);
            setAllGoals(result?.data?.allGoals);
            console.log(result?.data);
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
    }

    const fetchIncome = async () => {
        try {
            if(localPlanId!=="0"){
                if(localPlanId?.basicid===undefined)
                    return;
              }
            setLoader(true);
            const obj = {
                basicId: localUser,
                planid: localPlanId?.basicid===undefined?"0":localPlanId?.basicid
            }
            const enc: any = encryptData(obj);
            console.log(obj);
            console.log(localUser);

            let result = await fetchIncomeDetails(enc);
            console.log(enc);
            console.log(result);
            setIncomeDetailsList(result?.data);
            setLoader(false);
            console.log(result?.data);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    };

    const fetchExpense = async () => {
        try {
            if(localPlanId!=="0"){
                if(localPlanId?.basicid===undefined)
                    return;
              }
                
            setLoader(true);
            const obj = {
                basicId: localUser,
                planid: localPlanId?.basicid===undefined?"0":localPlanId?.basicid
            }
            const enc: any = encryptData(obj);
            console.log(obj);
            console.log(localUser);
            let result = await fetchExpenseDetails(enc);
            console.log(enc);
            console.log(result);
            setExpenseDetailsList(result?.data);
            setLoader(false);
            console.log(result?.data);
        } catch (error: any) {

            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    };

    const fetchBasicDetails = async () => {
        try {
          setLoader(true);
          if(localPlanId!=="0"){
            if(localPlanId?.basicid===undefined)
                return;
          }
            
          const obj: any = {
            basicId: localUser,
            PlanId: localPlanId?.basicid===undefined?"0":localPlanId?.basicid
          }
          // console.log(local.basicid);
          const enc: any = encryptData(obj);
          let result: any = await getBasicDetails(enc);
          console.log("basicDetails");
          console.log(result?.data);
         
          setBasicDetail(result?.data)
          setLoader(false);
    
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    }

    let options: any = [];
    const retireItem = () => {
        // console.log(options);
        let obj: any = {};
        for (let i = 60; i <= 90; i++) {
            options.push({ "id": "" + i + "", "name": "" + i + " Year" });
            obj = {
                id: i,
                name: "" + i + " Year"
            }
            // console.log(obj);
        }
        setRetireAge(obj);

        // console.log(options);
    }

    useEffect(() => {
        (async () => {
            console.log(localPlanId);
            if (localUser !== "" && (localPlanId?.basicid!==undefined || localPlanId==="0") ) {
                await fetchBasicDetails();
                // await fetchAllGoalScheme();
                await fetchIncome();
                await fetchExpense();
                await retireItem();
            }
        })();

        return () => {
        }
    }, [localUser, localPlanId])

    return (
        <>
            <StyledTabs defaultValue="BasicDetails">
                <TabsList
                    aria-label="Manage your Plan"
                    className="justify-content-center "
                    css={{
                        mb: 0,
                        borderBottom: "1px solid var(--colors-gray7)",
                        overflowX: "auto",
                    }}
                >
                    {recentTabs.map((item, index) => {
                        return (<>
                            <Box className="col-auto">
                                <TabsTrigger
                                    value={item.value}
                                    className="tabs px-1 px-sm-0 px-md-4 px-lg-5 me-2 border py-0"

                                    key={item.value}
                                    css={{ color: "solid 1px var(--colors-blue1)", height: "30px !important" }}
                                >
                                    {/* @ts-ignore */}
                                    <Text className="py-0" size="h4">
                                        {item.title}
                                    </Text>
                                </TabsTrigger>
                            </Box>
                        </>
                        );
                    })}
                </TabsList>
                <TabsContent value="BasicDetails" className="mt-2">
                    <BasicDetails />
                </TabsContent>
                <TabsContent value="Goals" className="mt-2">
                    <Goals />
                </TabsContent>
                <TabsContent value="Income" className="mt-2">
                    <Income fetchData={fetchIncome} />
                </TabsContent>
                <TabsContent value="NetAssetEntry" className="mt-2">
                    <NetAssetEntry />
                </TabsContent>
                <TabsContent value="Expenses" className="mt-2">
                    <Expenses fetchData={fetchExpense} fetchPlanId={fetchPlanId} />
                    {/* <Expenses clickOnSubmit={() => { }} /> */}
                </TabsContent>
            </StyledTabs>

        </>
    );
}

export default FinancialPlanPage