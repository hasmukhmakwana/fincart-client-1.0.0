import Box from "@ui/Box/Box";
import React, { useState } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import { Button } from "@ui/Button/Button";
import styles from "../../FinancialPlan.module.scss";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import SaveDataAlert from "./SaveDataAlert";
const PopMessagePage = () => {
  const [openDialog, setOpenDialog] = useState(false);
  return (
    <>


      <Box className="row">
        <Box className="col-md-12 col-sm-12 col-lg-12 mt-3">

          <Text weight="normal" size="h4" className="text-center mb-1">
            Submission Successfull
          </Text>
        </Box>

      </Box>
      <hr className="text-primary mx-0" />
      <Box className="row">
        <Box className="col-md-12 col-sm-12 col-lg-12 text-end">
          <Button
            className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
            color="yellowGroup"
            size="md"
          >

            <Text
              weight="normal"
              size="h6"
              //@ts-ignore
              color="gray8"
              className={styles.button}
            >
              Cancel
            </Text>
          </Button>&nbsp;
          <Button
            className={`col-auto mx-0 py-1 px-4 ${styles.button}`}
            color="yellowGroup"
            size="md"

          >
            <Text
              weight="normal"
              size="h6"
              //@ts-ignore
              color="gray8"
              className={styles.button}
              onClick={() => { setOpenDialog(true) }}
            >
              Sumbit All Details
            </Text>
          </Button>
        </Box>
      </Box>

      <DialogModal
        open={openDialog}
        setOpen={setOpenDialog}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },

        }}
      >
        <SaveDataAlert />
      </DialogModal>
    </>
  )
}
export default PopMessagePage;