import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input";
import SelectMenu from "@ui/Select/Select";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { Button } from "@ui/Button/Button";
import React, { useEffect, useState } from "react";
import styles from "../../FinancialPlan.module.scss";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import { values } from "lodash";
import useFinancialPlanStore from "../../store";
import DeleteIcon from "App/icons/DeleteIcon";
import PencilFill from "App/icons/PencilFill";
import { createExpenses } from "App/api/financialPlan"
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import { deleteExpense, submitALLDetailsPlan } from "App/api/financialPlan"
import Loader from "App/shared/components/Loader";

const validationSchemaEx = {
  RequiredIN: {
    Amount: Yup.string()
      .required("Amount is required")
      .matches(/^\d*$/gms, "Please enter only numeric amount")
      .trim(),

    ExpenseType: Yup.string()
      .required("Expense Type is required")
      .trim(),

    ExpenseName: Yup.string()
      .required("Expense is required")
      .trim(),
  },
  Other: {
    OtherName: Yup.string().required("Expense Name Required").trim()
  },
}

export type VerifyExpensesTypes = {
  Amount: string;
  ExpenseType: string;
  ExpenseName: string;
  OtherName: string;
};

type ExpensesEventTypes = {
  clickOnSubmit: (value: VerifyExpensesTypes) => any;
};
interface StoreTypes {
  verifyExpenses: VerifyExpensesTypes;
  setVerifyExpenses: (payload: VerifyExpensesTypes[]) => void;
}
const useRegistrationStore = create<StoreTypes>((set) => ({
  //* initial state
  verifyExpenses: {
    Amount: "",
    ExpenseType: "",
    ExpenseName: "",
    OtherName: "",
  },
  setVerifyExpenses: (payload: any) =>
    set((state) => ({
      ...state,
      verifyExpenses: payload,
    })),
}));
type SaveTypes = {
  fetchData: () => void;
  fetchPlanId: () => void;
};
const Expenses = ({ fetchData, fetchPlanId }: SaveTypes) => {
  const local: any = getUser();
  const { verifyExpenses, loading, setVerifyExpenses } = useRegistrationStore((state: any) => state);
  const [expenseFilter, setExpenseFilter] = useState();
  const [openDelete, setOpenDelete] = useState(false);
  const [expenseType, setExpenseType] = useState("SIP");
  const [loaderApi, setLoaderApi] = useState(false);
  const [goalId, setGoalId] = useState<any>({});
  const [buttonText, setButtonText] = useState("Add");
  const [validationSchema, setvalidationSchema] = useState(validationSchemaEx.RequiredIN);

  const handleExpenseType = (e: any) => {
    setExpenseType(e.name);
  }

  const [expenseWhtFilter, setexpenseWhtFilter] = useState();
  const [expenseWhtType, setExpenseWhtType] = useState();
  const [showSubmitButton, setShowSubmitButton] = useState(false);
  const [showExpenseName, setShowExpenseName] = useState(false);
  const handleExpenseWht = (e: any) => {
    setExpenseWhtType(e.name);
  }
  const {
    localUser,
    localPlanId,
    outflowList,
    expenseDetailsList,
    basicDetail, incomeDetailsList, loanList, insuranceList, mfList, otList
  } = useFinancialPlanStore();

  useEffect(() => {

    if (basicDetail.length > 0 && incomeDetailsList.length > 0 && expenseDetailsList.length > 0 && (loanList.length > 0 || insuranceList.length > 0 || mfList.length > 0 || otList.length > 0)) {
      setShowSubmitButton(true);
    }
  }, [])

  useEffect(() => {
    let obj = validationSchemaEx.RequiredIN;
    if (showExpenseName) {
      obj = { ...obj, ...validationSchemaEx.Other }

    }
    setvalidationSchema(obj);
    return () => { }
  }, [showExpenseName])

  const CreateNewExpense = async (data: any) => {
    data = data || {};
    try {
      setLoaderApi(true);
      const obj: any =
      {
        amount: parseInt(data?.Amount),
        assetTypeIdName: "",
        basicId: local?.basicid,
        createdBy: local?.rmCode,
        expenseTypeId: parseInt(data?.ExpenseType),
        expenseTypeName: "",
        outFlowTypeId: parseInt(data?.ExpenseName),
        outFlowTypeName: data?.OtherName,
        planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid
      };

      const enc: any = encryptData(obj);

      let result: any = await createExpenses(enc);
      if (result?.status === "Success") {
        fetchData();
        setShowExpenseName(false);
        toastAlert("success", "Expense Successfully Added");
        setLoaderApi(false);
        return true;
      }
      else {
        setLoaderApi(false);
        setButtonText("Add");
        toastAlert("warn", result?.msg);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }

  const UpdateExpense = async (data: any) => {
    data = data || {};
    try {
      setLoaderApi(true);
      const obj: any =
      {
        amount: parseInt(data?.Amount),
        assetTypeIdName: "",
        basicId: goalId?.basicId,
        createdBy: local?.rmCode,//FIN17092021234,
        expenseTypeId: parseInt(data?.ExpenseType),
        expenseTypeName: "",
        id: goalId?.id,
        outFlowTypeId: parseInt(data?.ExpenseName),
        outFlowTypeName: data?.OtherName,
        planId: goalId?.planId
      };
      const enc: any = encryptData(obj);
      let result: any = await createExpenses(enc);
      if (result?.status === "Success") {
        fetchData();
        setShowExpenseName(false);
        setButtonText("Add");
        toastAlert("success", "Expense Successfully Updated");
        setLoaderApi(false);
        return true;
      }
      else {
        setLoaderApi(false);
        setButtonText("Add");
        toastAlert("warn", result?.msg);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }
  const setExpense = async (record: any) => {

    if (buttonText === "Add") {
      const check = await CreateNewExpense(record);
      return check;
    }
    else if (buttonText === "Update") {
      const check = await UpdateExpense(record);
      return check;

    }
  }

  const ExpenseDelete = async (goalId: any) => {
    try {
      const enc: any = encryptData(goalId);
      const result: any = await deleteExpense(enc);
      fetchData();
      setOpenDelete(false);
      toastAlert("success", "Expense Deleted Successfully");
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }
  const submitALLDetails = async () => {
    try {
      let obj = {
        basicId: localUser,
        planId: localPlanId?.basicid,
        status: "F"
      }
      const enc: any = encryptData(obj);

      const result: any = await submitALLDetailsPlan(enc);
      fetchPlanId();
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
    }
  }
  return (
    <>
      <Box className="row justify-content-center">
        <Box className="col-10">
          <Formik
            initialValues={verifyExpenses}
            validationSchema={Yup.object().shape(validationSchema)}
            enableReinitialize
            onSubmit={async (values, { resetForm }) => {
              const check = await setExpense(values);
              if (check) {
                setVerifyExpenses({
                  Amount: "",
                  ExpenseType: "",
                  ExpenseName: "",
                  OtherName: ""
                })
                resetForm();
              }
            }}
          >
            {({
              values,
              handleReset,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (
              <>
                <Box className="row">
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu
                        items={[
                          { ID: "27", ExpenseType: "Fixed" },
                        ]}
                        bindValue={"ID"}
                        bindName={"ExpenseType"}
                        label={"Expense Type"}
                        name="ExpenseType"
                        value={values.ExpenseType}

                        onChange={(e: any) => {
                          setFieldValue("ExpenseType", e?.ID);
                        }}
                        required
                        error={submitCount ? errors.ExpenseType : undefined}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu
                        items={outflowList}
                        bindValue={"ID"}
                        bindName={"Type_name"}
                        label={"What Expenses Is it?"}
                        value={values.ExpenseName}
                        onChange={(e: any) => {
                          setFieldValue("ExpenseName", e?.ID);
                          if (e?.Type_name.trim() === "Other") {
                            setShowExpenseName(true);
                          }
                          else {
                            setFieldValue("OtherName", "");
                            setShowExpenseName(false);
                          }
                        }}
                        required
                        error={submitCount ? errors.ExpenseName : undefined}
                      />
                    </Box>
                  </Box>
                  {showExpenseName ?
                    <Box className="col-lg-4 col-md-6 col-sm-12">
                      <Box>
                        <Input label="Expense Name" name="OtherName" placeholder="Enter Expense Name"
                          error={submitCount ? errors.OtherName : null}
                          onChange={(e: any) => {
                            setFieldValue("OtherName", e?.target?.value)
                          }}
                          required
                          value={values?.OtherName} />
                      </Box>
                    </Box> : null}
                  <Box className="col-lg-4 col-md-6 col-sm-12">
                    <Box>
                      <Input label="Amount(Monthly)" name="Amount" placeholder="Enter Amount"
                        error={submitCount ? errors.Amount : null}
                        onChange={(e: any) => {
                          setFieldValue("Amount", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        required
                        value={values?.Amount} />
                    </Box>
                  </Box>
                </Box>

                <Box className="row justify-content-end">
                  <Box className="col-12 text-end">
                    <Button
                      className={`col-auto py-1 mx-0 px-4 ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      onClick={() => {
                        handleReset(); setVerifyExpenses({
                          Amount: "",
                          ExpenseType: "",
                          ExpenseName: "",
                          OtherName: ""
                        }); setButtonText("Add")
                      }}
                    >
                      <Text
                        //@ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={styles.button}
                      >
                        Reset
                      </Text>
                    </Button>&nbsp;
                    <Button
                      className={`col-auto  py-1 mx-0 px-4 ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      onClick={handleSubmit}
                      loading={loading}
                    >
                      <Text
                        // @ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={styles.button}
                      >
                        {buttonText}
                      </Text>
                    </Button>
                  </Box>
                </Box>
                {showSubmitButton ?
                  <Box className="row justify-content-center mt-5">
                    <Box className="col-12 text-center">
                      <Button
                        className={`col-auto  py-2 mx-0 px-4 ${styles.button}`}
                        color="yellowGroup"
                        size="md"
                        onClick={() => { submitALLDetails() }}
                      >
                        <Text
                          // @ts-ignore
                          weight="normal"
                          size="h6"
                          //@ts-ignore
                          color="gray8"
                          className={styles.button}
                        >
                          Submit ALL Details
                        </Text>
                      </Button>
                    </Box>
                  </Box>
                  : ""}
              </>
            )}
          </Formik>
        </Box>
      </Box>
      <hr className="text-primary mx-0 " />
      <Box>
        {/* @ts-ignore */}
        <Text className="text-center m-3" weight="normal" size="h4" css={{ color: "var(--colors-blue1)" }}>
          List Of Expenses
        </Text>
      </Box>

      <Box className="row">
        <Box
          className="col-auto col-lg-12 col-md-12 col-sm-12"
          css={{ overflowX: "auto" }}
        >
          <Box className="table-responsive" css={{ borderRadius: "8px" }}>
            <Table
              className="text-capitalize table table-striped"
              css={{ color: "var(--colors-blue1)" }}
            >
              <ColumnParent css={{ fontWeight: "normal" }} className="text-capitalize">
                <ColumnRow>
                  {/* <Column>
                    <Box className="row" css={{ width: "130px" }}>

                      <Box className="col-auto mt-0" css={{ fontWeight: "normal" }}>Select All </Box>
                      <Box className="col-auto mt-0">
                        <Input type="checkbox" />
                      </Box></Box>
                  </Column> */}

                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Expense Type</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Expense Name</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Amount</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Action</Text></Column>

                </ColumnRow>
              </ColumnParent>
              <DataParent>
                {expenseDetailsList.map((record, index) => {
                  return (
                    <DataRow>
                      {/* <DataCell><Box className="row">
                        <Box className="col-lg-9 mt-0"><Input type="checkbox" /></Box>
                      </Box></DataCell> */}
                      <DataCell>{record?.ExpenseTypeName}</DataCell>
                      <DataCell>{record?.OutFlowTypeName}</DataCell>
                      <DataCell>{record?.Amount}</DataCell>
                      <DataCell>
                        <Box className="row">
                          <Box className="col-auto">
                            {/* @ts-ignore */}
                            <Text onClick={() => {
                           
                              if (record?.OutFlowTypeId === "6") {
                                setShowExpenseName(true);
                              }
                              else {
                                setShowExpenseName(false);
                              }
                              setVerifyExpenses({ Amount: record?.Amount, ExpenseType: record?.ExpenseTypeId, ExpenseName: record?.OutFlowTypeId, OtherName: record?.OutFlowTypeName })
                              // setButtonText("Update");
                              if (buttonText === "Update") {
                                setButtonText("Add");
                              }
                              else if (buttonText === "Add") {
                                setButtonText("Update");
                              }
                              setGoalId({
                                planId: record?.PlanId,
                                basicId: record?.BasicId,
                                id: record?.Id
                              });
                            }}
                              css={{ cursor: "pointer" }}>
                              <PencilFill />
                            </Text>
                          </Box>
                          &nbsp;&nbsp;
                          <Box className="col-auto">
                            {/* @ts-ignore */}
                            <Text onClick={() => {
                              setOpenDelete(true);
                              setButtonText("Add");
                              setGoalId({
                                planId: record?.PlanId,
                                basicId: record?.BasicId,
                                id: record?.Id
                              });
                            }} css={{ cursor: "pointer" }}>
                              <DeleteIcon /></Text></Box>
                        </Box>
                      </DataCell>
                      {/* <DataCell colSpan={5} className="text-center">No Expenses Found </DataCell> */}
                    </DataRow>);
                })}

              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>
      <DialogModal
        open={openDelete}
        setOpen={setOpenDelete}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            <Box className="row">
              <Box className="col-auto"><Text css={{ color: "var(--colors-blue1)" }}>Delete Expense Warning</Text></Box>
              <Box className="col"></Box>
              <Box className="col-auto"></Box>
            </Box>
          </Box>
          <Box
            className="text-center modal-body p-3"
          >
            <Text css={{ mt: 20, pt: 10 }}>
              Are you sure to delete this Expense?

            </Text>
            <Box css={{ mt: 20, pt: 10 }} className="text-end">
              <Button
                type="submit"
                color="yellow"
                onClick={() => { setOpenDelete(false) }}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={() => { ExpenseDelete(goalId) }}
                loading={loading}
              >
                Delete Now
              </Button>
            </Box>
          </Box>
        </Box>
      </DialogModal>

    </>
  )
}

export default Expenses