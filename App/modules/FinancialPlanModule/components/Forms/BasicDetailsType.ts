import { type } from "os";

export type childType = {
  Full_Name_Child: string,
  DateOfBirth_Child: string,
  Age_Child: string,
  gender_child: string,
  InDepent: boolean,
  DepentId: string,
  basicId: string,
  Caf_Status: string,
  userRelId: string,
  // index: number,
}

export type depentType = {
  Full_Name_Dep: string,
  DateOfBirth_Dep: string,
  Age_Dep: string,
  relation: string,
  DepentId: string,
  status: boolean,
  basicId: string,
  Caf_Status: string,
  userRelId: string,
  // index: number,
}

export type verifyBasicDTypes = {
  Full_Name: string;
  DateOfBirth: string;
  companyName: string;
  maritalStatus: string;
  designation: string;
  gender: string;

  Full_Name_Spouse: string;
  DateOfBirth_Spouse: string;
  Age_Spouse: string;
  SpWorkStatus: string;
  companyName_Spouse: string;
  designation_Spouse: string;
  income_Spouse: string;

  children: any;
  dependant: any;
};