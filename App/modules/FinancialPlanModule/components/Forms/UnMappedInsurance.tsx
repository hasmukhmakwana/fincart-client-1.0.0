import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { Button } from "@ui/Button/Button";
import SelectMenu from "@ui/Select/Select";
import React, { useState, useEffect } from "react";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import Autocomplete from "react-autocomplete";
import { convertBase64, encryptData, getUser, plainBase64 } from "App/utils/helpers";
import { createInsurance, insurancePolicySearch } from "App/api/financialPlan";
import { toastAlert } from "App/shared/components/Layout";
import Checkbox from "@ui/Checkbox/Checkbox";


const UnMappedMeber = ({ setOpenUnmappedIN, fetchOtherAssetDetails, fetchInsuranceUnmapped }: any) => {
  const {
    unMappedInsurance,
    relationList,
    inDropdownList,
    localPlanId,
    basicDetail,
    frequencyList,
  } = useFinancialPlanStore();
  const local: any = getUser();
  const [policyList, setPolicyList] = useState([]);
  const [policyNameList, setPolicyNameList] = useState([]);
  const [insurancePolType, setInsurancePolType] = useState('');
  const [policyName, setPolicyName] = useState('');
  const [checkedList, setCheckedList] = useState<any>([]);
  const [loader, setLoader] = useState<boolean>(false);

  useEffect(() => {
    let arr: any = [];
    unMappedInsurance.map((ele, index) => {
      let obj = {
        BasicId: ele?.BasicId,
        FrequencyTypeName: ele?.FrequencyTypeName,
        PP_Term: ele?.PP_Term,
        PolicyIssueDate: ele?.PolicyIssueDate,
        PolicyOwnerName: ele?.PolicyOwnerName,
        assetName: ele?.assetName,
        assetTypeId: ele?.assetTypeId,
        policyId: ele?.policyId,
        policyNo: ele?.policyNo,
        policyPartnerId: ele?.policyPartnerId,
        policyPremium: ele?.policyPremium,
        policySumAssured: ele?.policySumAssured,
        policyTerm: ele?.policyTerm,
        trxnSource: ele?.trxnSource,
        i: index,
        isAdd: false
      }
      arr.push(obj)
    })
    setCheckedList(arr);
  }, [unMappedInsurance])

  const searchPolicy = async (part: any) => {
    try {
      const obj: any = {
        AssetType: insurancePolType || null,
        PolicyProvider: null,
        SearchText: part
      }
      const enc: any = encryptData(obj);
      const result: any = await insurancePolicySearch(enc);
      let policyNames: any = [];
      result?.data?.Value.map((record: any) => {
        policyNames.push(record.Text);
      });
      setPolicyList(result?.data?.Value);
      setPolicyNameList(policyNames);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }


  const addInCheckedList = (ele: any, isAdd: any) => {

    const newState = checkedList.map((obj: any) => {
      if (obj.i === ele?.i) {
        return { ...obj, isAdd: isAdd };
      }

      return obj;
    });

    setCheckedList(newState);

  }

  const saveUnMappedEntries = async () => {
    let arr = checkedList.filter((el: any) => el.isAdd === true)
    if (arr.length >= 1) {
      setLoader(true);

      let resultArr: any = [];
      arr.map((ele: any) => {
        let frequencyID = frequencyList.filter((el) => el?.Type_name.toLowerCase() === ele?.FrequencyTypeName)
        let basicId = basicDetail.filter((el) => el?.ClientName.trim() === ele?.PolicyOwnerName.trim())
        let obj = {
          assetName: ele?.assetName,
          assetTypeId: ele?.assetTypeId,
          basicId: ele?.BasicId,
          dependentid: "0",
          policySumAssured: Number(ele?.policySumAssured),
          premiumTerm: ele?.PP_Term,
          policyTerm: ele?.policyTerm,
          frequencyTypeId: frequencyID[0]?.ID,
          liquidityType: "",
          planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid, //localPlanId
          policyId: ele?.policyId,
          policyOwnerBasicId: basicId[0]?.BasicId,
          policyOwners: ele?.poli,
          policyPartnerId: Number(ele?.policyPartnerId),
          policyPremium: Number(ele?.policyPremium),
          rmCode: local?.rmCode,
          currentValue: 0,
          trxnSource: ele?.trxnSource
        }
        resultArr.push(obj);
      })
      try {
        const enc: any = encryptData(resultArr);
        const result: any = await createInsurance(enc);
        if (result?.status === "Success") {
          setOpenUnmappedIN(false);
          fetchInsuranceUnmapped();
          toastAlert("success", "Insurance Successfully Added");
          fetchOtherAssetDetails();
          setLoader(false);
          return true;
        }
        else {
          toastAlert("warn", result?.msg);
          setLoader(false);
          return false;
        }

      }
      catch (error: any) {
        console.log(error);
        setLoader(false);
        toastAlert("error", error);
      }
    }
    else {
      return
    }
  }

  return (
    //@ts-ignore
    <>
      <Box>
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text weight="bold" css={{ color: "var(--colors-blue1)" }}>Un-Mapped Insurance</Text>
        </Box>
        <Box className={`modal-body p-3`} >

          <Box className={`row ${styles.scrollit}`}>
            <Box css={{ overflow: "auto" }}
              className="col-auto col-lg-12 col-md-12 col-sm-12" >
              <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                <Table className="text-capitalize table table-striped mb-5 pb-5" >
                  <ColumnParent className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                    <ColumnRow className=" sticky" css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}  >
                      <Column><Text size="h5">Asset Name</Text></Column>
                      <Column><Text size="h5">Frequency</Text></Column>
                      <Column><Text size="h5">Policy name</Text></Column>
                      <Column><Text size="h5">Policy Term</Text></Column>
                      <Column><Text size="h5">Premium Paying Term</Text></Column>
                      <Column><Text size="h5">Source</Text></Column>
                      <Column><Text size="h5">Owner</Text></Column>
                      <Column><Text size="h5">Is Added</Text></Column>
                    </ColumnRow>
                  </ColumnParent>

                  <DataParent css={{ textAlign: "center" }} >
                    {checkedList.map((record: any, index: any) => {
                      return (
                        <DataRow>
                          <DataCell>
                            <SelectMenu
                              items={inDropdownList}
                              bindValue={"Asset_Id"}
                              disabled={true}
                              bindName={"AssetName"}
                              name="insuranceType"
                              value={record?.assetTypeId}
                              onChange={(e: any) => {
                                setInsurancePolType(e?.Asset_Id);
                              }} />
                          </DataCell>
                          <DataCell>{record?.FrequencyTypeName} </DataCell>
                          <DataCell>
                            {/* <Autocomplete
                      getItemValue={(item: any) => item}
                      items={policyNameList}
                      disabled={true}
                      menuStyle={{
                        borderRadius: '3px',
                        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                        background: 'rgba(255, 255, 255, 0.9)',
                        padding: '15px 0',
                        fontSize: '70%',
                        position: 'absolute',
                        overflow: 'auto',
                        zIndex: "1",
                        maxHeight: '25%', // TODO: don't cheat, let it flow to the bottom
                      }}
                      renderItem={(item: any, isHighlighted: any) =>
                        <div style={{ background: isHighlighted ? 'lightgray' : 'white', }}>
                          {item}
                        </div>
                      }
                      value={policyName===""?record?.assetName:policyName}
                      onChange={(e: any) => {  setPolicyName(e.target.value),searchPolicy(e.target.value) }}
                      onSelect={(val: any) => { setPolicyName(val); }}
                    /> */}

                            <Input type="text" value={record?.assetName} disabled={true} />
                          </DataCell>
                          <DataCell>
                            {record?.policyTerm}
                          </DataCell>
                          <DataCell>{record?.PP_Term}
                          </DataCell>
                          <DataCell>
                            {record?.trxnSource}
                          </DataCell>
                          <DataCell>
                            {record?.policyOwnerName}
                          </DataCell>
                          <DataCell>
                            <Checkbox
                              label=""
                              name="fp_added"
                              id="add"
                              checked={record?.isAdd}
                              onChange={(e: any) => { addInCheckedList(record, e.target.checked) }}
                            />
                          </DataCell>
                        </DataRow>
                      );
                    })}
                  </DataParent>
                </Table>
              </Box>
            </Box>
          </Box>
          <Box className="row justify-content-end mt-2">
            <Box className="col-auto">
              <Button
                className={`col-auto  py-1 px-4 mx-0 ${styles.button}`}
                color="yellowGroup"
                size="md"
                onClick={() => { saveUnMappedEntries() }}
              >
                <Text
                  //@ts-ignore
                  weight="normal"
                  size="h6"
                  //@ts-ignore
                  color="gray8"
                  className={styles.button}
                >
                  Save
                </Text>
              </Button>&nbsp;
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default UnMappedMeber