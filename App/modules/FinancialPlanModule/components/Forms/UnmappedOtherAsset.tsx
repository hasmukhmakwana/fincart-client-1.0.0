import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { Button } from "@ui/Button/Button";
import SelectMenu from "@ui/Select/Select";
import React, { useState, useEffect } from "react";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import { convertBase64, encryptData, getUser, plainBase64 } from "App/utils/helpers";
import Checkbox from "@ui/Checkbox/Checkbox";
import { createOtherAsset } from "App/api/financialPlan";
import { toastAlert } from "App/shared/components/Layout";

const UnMappedMeber = ({setOpenUnmappedOT, fetchOtherAssetDetails, fetchOtherAssetUnmapped}:any) => {
  const {
    unMappedOtherAsset,
    relationList,
    localPlanId,
    basicDetail
  } = useFinancialPlanStore();
  const [checkedList, setCheckedList] = useState<any>([]);
  const [loader, setLoader] = useState<boolean>(false);
  const local: any = getUser();

  // const padTo2Digits = (num: any) => {
  //   return num.toString().padStart(2, '0');
  // }
  // const formatDate = (date: any) => {
  //   return ((date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDate()).padStart(2, '0'))
  //   )
  // }

  useEffect(() => {
    let arr:any=[];
    unMappedOtherAsset.map((ele, index)=>{
       let obj={
        assetName: ele?.assetName,
        assetTypeId: ele?.assetTypeId,
        basicId: ele?.BasicId,
        dependenctid: "0",
        frequencyTypeId: Number(ele?.frequency),
        liquidityType: "",
        planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid,
        policyOwnerBasicId: ele?.BasicId,
        PolicyOwnerName: ele?.PolicyOwnerName,
        schemeName:ele?.schemeName,
        rmCode: local?.rmCode,
        trxnSource: "Fincart",
        schemeId: ele?.schemeId,
        loanEmiAmount: "",
        lockInDuration: "",
        lockInInterest: "",
        currentValue: "",
         i:index,
         isAdd:false
       }
       arr.push(obj)
    })
    setCheckedList(arr);
  }, [unMappedOtherAsset])

  const InterestAdding=(record:any, val:any, flag:any)=>{
    const newState = checkedList.map((obj:any) => {
      if (obj.i === record?.i) {
        switch(flag){
          case "interest":
            return {...obj, lockInInterest: val};
            break;
          case "duration":
            return {...obj, lockInDuration: val};
          case "currentValue":
            return {...obj, currentValue:val};
          case "monthlyContri":
            return {...obj, loanEmiAmount:val}
          case "liquidityType":
            return {...obj, liquidityType:val}
          case "checked":
            return {...obj, isAdd:val}
        }
        
      }

      return obj;
    });

    setCheckedList(newState);
  }

  const saveUnMappedEntries=async()=>{
    let arr=checkedList.filter((el:any)=> el.isAdd===true)
    if(arr.length>=1){
      setLoader(true);
    let resultArr:any=[];
    arr.map((ele:any)=>{
      let basicId= basicDetail.filter((el)=> el?.ClientName.trim()===ele?.PolicyOwnerName.trim())
        let obj={
          assetName: ele?.assetName,
          assetTypeId: ele?.assetTypeId,
          basicId: ele?.basicId,
          dependenctid: "0",
          liquidityType: ele?.liquidityType,
          planId: ele?.planId,
          policyOwnerBasicId: basicId[0]?.BasicId,
          rmCode: ele?.rmCode,
          trxnSource: ele?.trxnSource,
          schemeId: ele?.schemeId,
          loanEmiAmount: ele?.loanEmiAmount,
          lockInDuration: ele?.lockInDuration,
          lockInInterest: ele?.lockInInterest,
          currentValue: ele?.currentValue,
  
        }
        resultArr.push(obj);
      })

        try{
    
          const enc: any = encryptData(resultArr);
          const result: any = await createOtherAsset(enc);
          if(result.status==="Success"){
              toastAlert("success", "Unmapped Loan Successfully Added");
              setLoader(false);
              setOpenUnmappedOT(false);
              fetchOtherAssetDetails();
              fetchOtherAssetUnmapped();
          }
        }
        catch(error:any){
          console.log(error);
          setLoader(false);
          toastAlert("error", error);
        }
  
    
     
    }
    else{
      return
    }
  }


  return (
    //@ts-ignore
    <>
      <Box>
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}>Un-Mapped Other Assets</Text>
        </Box>
        <Box className={`modal-body p-3`} >

          <Box className={`row ${styles.scrollit}`}>
            <Box css={{ overflow: "auto" }}
              className="col-auto col-lg-12 col-md-12 col-sm-12" >
              <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                {/* @ts-ignore */}
                <Table className="text-capitalize table table-striped" >
                  <ColumnParent className="text-capitalize ">
                    <ColumnRow className=" sticky" css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}  >
                      <Column><Text size="h5">Asset type</Text></Column>
                      <Column><Text size="h5">Liq. Type</Text></Column>
                      <Column><Text size="h5">Asset name</Text></Column>
                      <Column><Text size="h5">Lock in(years)</Text></Column>
                      <Column><Text size="h5">Lock intr.(%)</Text></Column>
                      <Column><Text size="h5">Current value</Text></Column>
                      <Column><Text size="h5">Monthly Contr.</Text></Column>
                      <Column><Text size="h5">Owner</Text></Column>
                      <Column><Text size="h5">Is Add</Text></Column>
                    </ColumnRow>
                  </ColumnParent>

                  <DataParent css={{ textAlign: "center" }} >
                    {checkedList.map((record:any, index:any) => {
                      return (
                        <DataRow>
                          <DataCell>{record?.assetName} </DataCell>
                          <DataCell><SelectMenu
                        name="LiquidityType"
                        items={[
                          { id: "High", name: "High" },
                          { id: "Low", name: "Low" },
                        ]}
                        bindValue={"id"}
                        bindName={"name"}
                        value={record?.liquidityType}
                        onChange={(e: any) => {
                          InterestAdding(record,e?.id, "liquidityType")
                        }}
                        required
                        
                      /> </DataCell>
                          <DataCell>{record?.schemeName}</DataCell>
                          <DataCell>
                          <Input type="text" value={record?.lockInDuration} onChange={(e:any)=>{InterestAdding(record,e.target.value, "duration")}}/>
                          </DataCell>
                          <DataCell> 
                          <Input type="text" value={record?.lockInInterest} onChange={(e:any)=>{InterestAdding(record,e.target.value, "interest")}}/>
                          </DataCell>
                          <DataCell> 
                          <Input type="text" value={record?.currentValue} onChange={(e:any)=>{InterestAdding(record,e.target.value, "currentValue")}}/>
                          </DataCell>
                          <DataCell> 
                          <Input type="text" value={record?.loanEmiAmount} onChange={(e:any)=>{InterestAdding(record,e.target.value, "monthlyContri")}}/>
                          </DataCell>
                          <DataCell>{record?.PolicyOwnerName} </DataCell>
                          <DataCell>
                            <Checkbox
                              label=""
                              name="fp_added"
                              id="add"
                              checked={record?.isAdd}
                              onChange={(e:any)=>{InterestAdding(record,e.target.checked, "checked")}}
                            />
                          </DataCell>
                        </DataRow>
                      );
                    })}
                  </DataParent>
                </Table>
              </Box>
            </Box>
          </Box>
          <Box className="row justify-content-end">
            <Box className="col-auto">
              <Button
                className={`col-auto  py-1 px-4 mx-0 ${styles.button}`}
                color="yellowGroup"
                size="md"
                disabled={loader}
                onClick={()=>{saveUnMappedEntries()}}
              >
                <Text
                  //@ts-ignore
                  weight="normal"
                  size="h6"
                  //@ts-ignore
                  color="gray8"
                  className={styles.button}
                >
                   {loader?`Loading...`:`Save`}
                </Text>
              </Button>&nbsp;
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default UnMappedMeber