import React, { useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input/Input";
import SelectMenu from "@ui/Select/Select";
import 'react-autocomplete-input/dist/bundle.css';
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { Button } from "@ui/Button/Button";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import DeleteIcon from "App/icons/DeleteIcon";
import PencilFill from "App/icons/PencilFill";
import { createOtherAsset, otherAssetSearch } from "App/api/financialPlan";
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import { Label } from "@radix-ui/react-label";
import { clear } from "console";
import Autocomplete from "react-autocomplete";

export type verifyOtherAssetTypes = {
  AssetTypeName: string;
  AssetName: string;
  LockInDuration: string;
  LockInInterest: string;
  CurrentValueAsset: string;
  AdditionalContribution: string;
  policy: string;
  frequency: string;
}

const validationschemaOtherAsset = Yup.object().shape({
  AssetType: Yup.string().required("Asset Type is required").trim(),
  AssetName: Yup.string().required("Asset Name is required").trim(),
  LockInDuration: Yup.string(),
  LockInInterest: Yup.string(),
  CurrentValueAsset: Yup.string().required("Current Value is required").trim(),
  policy: Yup.string().required("Policy Name is required").trim(),
  AdditionalContribution: Yup.string(),

})

interface StoreTypes {
  verifyOtherAsset: verifyOtherAssetTypes;
}

const useRegistrationStore = create<StoreTypes>((set) => ({
  //* initial state
  verifyOtherAsset: {
    AssetTypeName: "",
    AssetName: "",
    LockInDuration: "",
    LockInInterest: "",
    CurrentValueAsset: "",
    AdditionalContribution: "",
    policy: "",
    frequency: ""
  },
  setVerifyOtherAsset: (payload: any) =>
    set((state) => ({
      ...state,
      verifyOtherAsset: payload,
    })),
}));



const OtherAssets = ({ otDetailsList, fetchOtherAssetDetails, setOpenDelete, setDeleteId, setOpenUnmappedOT }: any) => {
  const { basicDetail } = useFinancialPlanStore();
  //#region Declarations

  const local: any = getUser();
  const { verifyOtherAsset, setVerifyOtherAsset } = useRegistrationStore((state: any) => state);

  const [goalId, setGoalId] = useState<any>({});
  const [buttonText, setButtonText] = useState("Add");
  const [loaderApi, setLoaderApi] = useState(false);

  const [OtherAssetList, setOtherAssetList] = useState([]);
  const [OtherAssetNameList, setOtherAssetNameList] = useState([]);
  const {
    dgsDropdownList,
    frequencyList,
    localPlanId,
  } = useFinancialPlanStore();


  const searchAssetName = async (part: any) => {

    try {

      const obj: any = {
        Type: "OT",
        SearchText: part
      }
      const enc: any = encryptData(obj);
      const result: any = await otherAssetSearch(enc);
      let otherAssetName: any = [];
      result?.data?.Value.map((record: any) => {
        otherAssetName.push(record.Text);
      });
      setOtherAssetList(result?.data?.Value);
      setOtherAssetNameList(otherAssetName);
      setLoaderApi(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
    }
  }


  const setOtherAsset = async (record: any) => {

    if (buttonText === "Add") {
      const check = await CreateNewAsset(record);
      return check;
    }
    else if (buttonText === "Update") {
      const check = await UpdateOtherAsset(record);
      return check;
    }
  }
  //Update OtherAsset API
  const UpdateOtherAsset = async (data: any) => {
    data = data || {};
    setLoaderApi(true);
    let assetID: any = "";
    OtherAssetList.forEach(element => {
      // @ts-ignore
      if (element.Text.trim() === data?.AssetName.trim()) {
        // @ts-ignore
        assetID = element.Value;
      }
    });
    if(assetID===""){
      setLoaderApi(false);
      toastAlert("warn", "No Asset Found");
      return false; 
    }
    try {

      const obj: any =
      [{
        assetName: data?.AssetName,
        assetTypeId: data?.AssetType,
        basicId: local?.basicid,
        dependenctid: "0",
        frequencyTypeId: Number(data?.frequency),
        liquidityType: "",
        planId: goalId?.planId,
        id: goalId?.id,
        policyId: 0,
        policyOwnerBasicId: local?.basicid,
        rmCode: local?.rmCode,
        trxnSource: "Fincart",
        schemeId: assetID,
        loanEmiAmount: data?.AdditionalContribution === "" ? 0 : Number(data?.AdditionalContribution),
        lockInDuration: data?.LockInDuration === "" ? "0" : data?.LockInDuration,
        lockInInterest: data?.LockInInterest === "" ? "0" : data?.LockInInterest,
        currentValue: data?.CurrentValueAsset,
      }];
      const enc: any = encryptData(obj);

      let result: any = await createOtherAsset(enc);

      if (result?.status === "Success") {
        toastAlert("success", "Other Asset Successfully Updated");
        setLoaderApi(false);
        setButtonText("Add");
        fetchOtherAssetDetails();
        return true;
      }
      else {
        setLoaderApi(false);
        toastAlert("warn", result?.msg);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }
  const CreateNewAsset = async (data: any) => {
    data = data || {};
    try {
      setLoaderApi(true);
      let assetID: any = "";
      OtherAssetList.forEach(element => {
        // @ts-ignore
        if (element.Text.trim() === data?.AssetName.trim()) {
          // @ts-ignore
          assetID = element.Value;
        }
      });
      if(assetID===""){
        setLoaderApi(false);
        toastAlert("warn", "No Asset Found");
        return false; 
      }

      const obj: any =
      [{
        assetName: data?.AssetName,
        assetTypeId: data?.AssetType,
        basicId: local?.basicid,
        dependenctid: "0",
        frequencyTypeId: Number(data?.frequency),
        liquidityType: "",
        planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid,
        policyOwnerBasicId: local?.basicid,
        rmCode: local?.rmCode,
        trxnSource: "Fincart",
        schemeId: assetID,
        loanEmiAmount: data?.AdditionalContribution === "" ? 0 : Number(data?.AdditionalContribution),
        lockInDuration: data?.LockInDuration === "" ? "0" : data?.LockInDuration,
        lockInInterest: data?.LockInInterest === "" ? "0" : data?.LockInInterest,
        currentValue: data?.CurrentValueAsset,
      }]
      const enc: any = encryptData(obj);
      const result: any = await createOtherAsset(enc);
      if (result?.status === "Success") {
        toastAlert("success", "Other Asset Successfully Added");
        setLoaderApi(false);
        setButtonText("Add");
        fetchOtherAssetDetails();
        return true;
      }
      else {
        setLoaderApi(false);
        toastAlert("warn", result?.msg);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }

  return (
    <>
      <Box className="row justify-content-center">
        <Box className="col-10">
          <Formik
            initialValues={verifyOtherAsset}
            validationSchema={validationschemaOtherAsset}
            enableReinitialize
            onSubmit={async (values, { resetForm }) => {
              const check = await setOtherAsset(values);
              if (check) {
                resetForm();
                setVerifyOtherAsset({
                  AssetTypeName: "",
                  AssetName: "",
                  LockInDuration: "",
                  LockInInterest: "",
                  CurrentValueAsset: "",
                  AdditionalContribution: "",
                  policy: "",
                  frequency: ""
                })
              }
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleReset,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (
              <>

                <Box className="row">
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <SelectMenu
                        items={dgsDropdownList}
                        bindValue={"Asset_Id"}
                        bindName={"AssetName"}
                        label={"Asset Type"}
                        error={submitCount ? errors.AssetType : null}
                        name={"AssetType"}
                        value={values.AssetType}
                        required
                        onChange={(e: any) => {
                          setFieldValue("AssetType", e?.Asset_Id)

                        }}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>

                      <Label
                        style={{ marginBottom: 5, fontSize: '0.7rem', color: '#339cff', fontWeight: 600, letterSpacing: '0.1rem' }}>Asset Name <span style={{ color: "#e5484d" }}>&nbsp;*</span></Label><br></br>

                      <Autocomplete
                        getItemValue={(item: any) => item}
                        items={OtherAssetNameList}
                        style={{ color: "red !importent" }}
                        menuStyle={{
                          borderRadius: '3px',
                          borderColor: 'solid 1px #red',
                          boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                          background: 'rgba(255, 255, 255, 0.9)',
                          padding: '0px 0px',
                          fontSize: '75%',
                          position: 'absolute',
                          overflow: 'auto',
                          zIndex: "1",
                          maxHeight: '25%', // TODO: don't cheat, let it flow to the bottom
                        }}

                        renderItem={(item: any, isHighlighted: any) =>
                          <div style={{ background: isHighlighted ? 'lightgray' : 'white', }}>
                            {item}
                          </div>
                        }
                        value={values?.AssetName}
                        onChange={(e: any) => { setFieldValue("AssetName", e.target.value); searchAssetName(e.target.value) }}
                        onSelect={(val: any) => { setFieldValue("AssetName", val); }}

                      />
                      {/* @ts-ignore */}
                      {submitCount ? <Text size="h6" css={{ color: "#e5484d" }}>{errors.AssetName}</Text> : ""}
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Lock -In Duration(Years)"
                        name="LockInDuration"
                        placeholder="0"
                        onChange={(e: any) => {
                          setFieldValue("LockInDuration", e?.target?.value.length > 2 ? e?.target?.value.slice(0, 2) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.LockInDuration}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Lock-In Interest(%)"
                        name="LockInInterest"
                        placeholder="0"
                        onChange={(e: any) => {
                          setFieldValue("LockInInterest", e?.target?.value.length > 3 ? e?.target?.value.slice(0, 3) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.LockInInterest}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Current Value"
                        name="CurrentValueAsset"
                        placeholder="Enter Value"
                        error={submitCount ? errors.CurrentValueAsset : null}
                        required
                        onChange={(e: any) => {
                          setFieldValue("CurrentValueAsset", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.CurrentValueAsset}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Additional Contribution"
                        name="AdditionalContribution"
                        placeholder="Enter Value"
                        onChange={(e: any) => {
                          setFieldValue("AdditionalContribution", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.AdditionalContribution}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <SelectMenu
                        items={basicDetail}
                        bindValue={"BasicId"}
                        bindName={"ClientName"}
                        label={"Policy Owner"}
                        name={"policy"}
                        value={values.policy}
                        required
                        error={submitCount ? errors.policy : null}
                        onChange={(e: any) => {
                          setFieldValue("policy", e?.BasicId)
                        }}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <SelectMenu
                        items={frequencyList}
                        bindValue={"ID"}
                        bindName={"Type_name"}
                        label={"frequency"}
                        value={values.frequency}
                        onChange={(e: any) => {
                          setFieldValue("frequency", e?.ID)
                        }}

                        error={submitCount ? errors.frequency : undefined}

                      />
                    </Box>
                  </Box>
                  <Box className="row justify-content-between">
                    <Box className="col-auto py-1">
                      <Box
                        onClick={() => setOpenUnmappedOT(true)}
                        css={{
                          color: "var(--colors-blue1)",
                          cursor: "pointer",
                        }}//AR
                      >
                        {/* @ts-ignore */}
                        <Text size="h6"><u>Un-Mapped Entries</u></Text>
                      </Box>

                    </Box>
                    <Box className="col-auto">
                      <Button
                        className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                        color="yellowGroup"
                        size="md"
                        disabled={loaderApi}
                        onClick={() => {
                          handleReset(); setButtonText("Add"); setVerifyOtherAsset({
                            AssetTypeName: "",
                            AssetName: "",
                            LockInDuration: "",
                            LockInInterest: "",
                            CurrentValueAsset: "",
                            AdditionalContribution: "",
                            policy: "",
                            frequency: ""
                          })
                        }}
                      >

                        <Text
                          //@ts-ignore
                          weight="normal"
                          size="h6"
                          //@ts-ignore                             
                          color="gray8"
                          className={styles.button}
                        >
                          Reset
                        </Text>
                      </Button>&nbsp;
                      <Button
                        className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                        color="yellowGroup"
                        size="md"
                        onClick={() => {
                          handleSubmit();
                        }}
                        disabled={loaderApi}
                      >
                        <Text
                          //@ts-ignore
                          weight="normal"
                          size="h6"
                          //@ts-ignore
                          color="gray8"
                          className={styles.button}
                        >
                          {loaderApi ? 'Loading...' : `${buttonText}`}
                        </Text>
                      </Button>
                    </Box>
                  </Box >
                </Box >

              </>
            )}
          </Formik >
        </Box >
      </Box >
      <hr className="text-primary mx-0 my-1"></hr>
      <Box>
        <Text
          // @ts-ignore
          weight="normal"
          size="h4"
          css={{ color: "var(--colors-blue1)" }}
          className="text-center mx-3 my-3"
        >
          Other Assets
        </Text>
      </Box>
      <Box className="row ">
        <Box
          className="col-auto col-lg-12 col-md-12 col-sm-12"
          css={{ overflowX: "auto" }}
        >
          <Box className="table-responsive" css={{ borderRadius: "8px" }}>
            <Table
              className="text-capitalize table table-striped"
            // css={{ color: "var(--colors-blue1)" }}
            >
              <ColumnParent className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                <ColumnRow>
                  {/* <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>
                    <Box class="form-check form-check-inline">
                      <label className="form-check-label me-2" for="inlineCheckbox2">Select All</label>
                      <input className="form-check-input mt-1" type="checkbox" id="inlineCheckbox2" value="option2" />
                    </Box>
                  </Text>
                  </Column> */}
                  {/* <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Liquidity Type</Text></Column> */}
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Asset Name</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Asset Type</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Lock-In Duration (Year)</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Lock-In Interest(%)</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Current Value</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Additional Contribution</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Frequency</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Transaction Source</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Action</Text></Column>
                </ColumnRow>
              </ColumnParent>
              <DataParent>
                {otDetailsList.length === 0 ? <>
                  <DataRow>
                    <DataCell colSpan={11} className="text-center">
                      No Assets Found
                    </DataCell>
                  </DataRow>
                </> : <>
                  {
                    otDetailsList.map((record: any, index: any) => {
                      return (
                        <DataRow>
                          {/* <DataCell><Box className="row">
                            <Box className="col-lg-9 mt-0"><Input type="checkbox" /></Box>
                          </Box></DataCell> */}
                          {/* <DataCell>{record?.LiquidityType}</DataCell> */}
                          <DataCell>{record?.assetName}</DataCell>
                          <DataCell>{record?.assetTypeName}</DataCell>
                          <DataCell>{record?.lockInDuration}</DataCell>
                          <DataCell>{record?.lockInInterest}</DataCell>
                          <DataCell>{record?.CurrentValue}</DataCell>
                          <DataCell>{record?.loanEmiAmount}</DataCell>
                          <DataCell>{record?.FrequencyTypeName}</DataCell>
                          <DataCell>{record?.trxnSource}</DataCell>
                          <DataCell>
                            <Box className="row">
                              <Box className="col-auto px-2">
                                {/* @ts-ignore */}
                                <Text onClick={() => {

                                  setVerifyOtherAsset(
                                    {
                                      AssetType: record?.assetTypeId,
                                      AssetName: record?.assetName,
                                      LockInDuration: record?.lockInDuration,
                                      LockInInterest: record?.lockInInterest,
                                      CurrentValueAsset: record?.CurrentValue,
                                      AdditionalContribution: record?.loanEmiAmount,
                                      frequency: record?.FrequencyTypeId,
                                      policy: String(record?.policyOwnerBasicId)
                                    })
                                  setButtonText("Update");
                                  setGoalId({
                                    planId: record?.PlanId,
                                    basicId: record?.BasicId,
                                    id: record?.Id
                                  });
                                }}
                                  css={{ cursor: "pointer" }}>
                                  <PencilFill />
                                </Text>
                              </Box>

                              <Box className="col-auto px-2">
                                {/* @ts-ignore */}
                                <Text onClick={() => {
                                  setOpenDelete(true);
                                  setDeleteId({
                                    planId: record?.PlanId,
                                    basicId: record?.BasicId,
                                    id: record?.Id
                                  });
                                }} css={{ cursor: "pointer" }}>
                                  <DeleteIcon /></Text></Box>
                            </Box>
                          </DataCell>

                        </DataRow>)
                    })}
                </>}
              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>

    </>
  );
};

export default OtherAssets;
