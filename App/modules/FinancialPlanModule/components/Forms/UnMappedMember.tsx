import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { Button } from "@ui/Button/Button";
import { Formik, FieldArray } from "formik";
import SelectMenu from "@ui/Select/Select";
import React, { useState, useEffect } from "react";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import create from "zustand";
import Checkbox from "@ui/Checkbox/Checkbox";
import { getUser } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";

const UnMappedMember = ({ setOpen, values, setFieldValue}: any) => {
  const {
    recommendedMem,
    relationList,
  } = useFinancialPlanStore();
  const local: any = getUser();
  const [recommendedMemList, setRecommendedMemList] = useState<any>([]);
  const [relationCodeList, setRelationCodeList] = useState<any>([])
  const [checkedList, setCheckedList] = useState<any>([]);
  const [loader, setLoader] = useState<boolean>(false);

  const calcAge = (date: any) => {
    
    let age: any;
    var today = new Date();
    var birthDate = new Date(date);
    age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }


  const addUnMapped = (List: any) => {

    let check = List.reduce((a: any, e: any, i: any) => {
      if ([""].includes(e.relation)) { a.push(e); } return a;
    }, [])
   
    if (check?.length > 0) {
      toastAlert("warn", "Need select relations");
    } else {
      let childList = List.reduce((a: any, e: any, i: any) => {
        if (["07", "08"].includes(e.relation)) { a.push(e); } return a;
      }, [])

      let dependList = List.reduce((a: any, e: any, i: any) => {
        if (!["07", "08"].includes(e.relation)) { a.push(e); } return a;
      }, [])


      let Children: any = values?.children;
      let Dependant: any = values?.dependant;
      for (let i = 0; i < childList.length; i++) {
        Children.push({
          Full_Name_Child: childList[i]?.Full_Name_Dep,
          DateOfBirth_Child: childList[i]?.DateOfBirth_Dep,
          Age_Child: childList[i]?.Age_Dep  ,
          gender_child: childList[i]?.relation === "07" ? "M" : "F",
          InDepent: childList[i]?.status,
          DepentId: "",
          Caf_Status: childList[i]?.Caf_Status,
          basicId: childList[i]?.basicId,
          userRelId: childList[i]?.userRelId,
        }
        );

      }
      for (let i = 0; i < dependList.length; i++) {
        Dependant.push(dependList[i]);
      }

      setFieldValue("children", Children);
      setFieldValue("dependant", Dependant);

      setOpen(false);
    }
  }

  useEffect(() => {
    setRecommendedMemList(recommendedMem?.map((i: any, index: any) => ({ ...i, number: `${index + 1}`, relationId: "", IsDepend: true, IsAdd: false })));
    return () => {
      setRecommendedMemList([]);
    }
  }, [recommendedMem])

  const addRelationValue = (ele: any, val: any) => {

    const newState = recommendedMemList.map((obj: any) => {
      if (obj.number === ele?.number) {
        return { ...obj, relationId: val };
      }
      return obj;
    });
    setRecommendedMemList(newState);
  }

  // const addIsDependValue = (ele: any, val: any) => {
  //   const newState = recommendedMemList.map((obj: any) => {
  //     if (obj.number === ele?.number) {
  //       return { ...obj, IsDepend: val };
  //     }
  //     return obj;
  //   });
  //   setRecommendedMemList(newState);
  // }

  const addInCheckedList = (ele: any, val: any) => {
    const newState = recommendedMemList.map((obj: any) => {
      if (obj.number === ele?.number) {
        return { ...obj, IsAdd: val };
      }
      return obj;
    });
    setRecommendedMemList(newState);
  }

  useEffect(() => {
    setRelationCodeList(relationList.reduce((a: any, e: any, i: any) => {
      if (!["01", "02", "013"].includes(e.code)) { a.push(e); } return a;
    }, []))

    return () => { }
  }, [relationList])

  return (
    //@ts-ignore
    <>
      <Box>
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}> Un-Mapped Members</Text>
        </Box>
        <Box className={`modal-body p-3`} >
         
          <Box className={`row ${styles.scrollit}`}>
            <Box css={{ overflow: "auto" }}
              className="col-auto col-lg-12 col-md-12 col-sm-12" >
              <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                <Table className="text-capitalize table table-striped" >
                  <ColumnParent className="text-capitalize ">
                    <ColumnRow className=" sticky" css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}  >
                      <Column><Text size="h5">Name</Text></Column>
                      <Column><Text size="h5">CAF Status</Text></Column>
                      <Column><Text size="h5">Date Of Birth</Text></Column>
                      <Column><Text size="h5">Relation</Text></Column>
                      <Column><Text size="h5">Is Dependent</Text></Column>
                      <Column><Text size="h5">Add</Text></Column>
                    </ColumnRow>
                  </ColumnParent>

                  <DataParent css={{ textAlign: "center" }} >
                    {recommendedMemList.map((record: any, index: any) => {
                    
                      return (
                        <DataRow>
                          <DataCell>{record?.Name} </DataCell>
                          <DataCell>{record?.Caf_Status === "Y" ? <>Yes</> : <> No</>} </DataCell>
                          <DataCell>{record?.dob}</DataCell>
                          <DataCell>
                            <SelectMenu
                              isClearable
                              items={relationCodeList}
                              bindValue={`code`}
                              bindName={`Type_name`}
                              // name={`dependant.${index}.relationId`}
                              value={record.relationId}
                              onChange={(e: any) => {
                                addRelationValue(record, e?.code);

                              }}
                            />
                          </DataCell>
                          <DataCell>
                            <SelectMenu
                              items={[
                                { id: true, name: "Yes" },
                                { id: false, name: "No" },
                              ]}
                              bindValue={"id"}
                              bindName={"name"}
                              disabled={true}
                              // name={`dependant.${index}.status`}
                              value={record?.IsDepend}
                            // onChange={(e: any) => { addIsDependValue(record, e.id) }}
                            />
                          </DataCell>
                          <DataCell>
                            <Input
                              id={record?.number}
                              value={record?.IsAdd}
                              // name={`dependant.${index}.number`}
                              type="checkbox"
                              onClick={(e: any) => {
                             
                                addInCheckedList(record, (e.target.checked))
                                if (e.target.checked) {
                                
                                  let obj: any = {
                                    Full_Name_Dep: record?.Name,
                                    DateOfBirth_Dep: record?.dob,
                                    Age_Dep: String(calcAge(record?.dob)),
                                    Caf_Status: record?.Caf_Status,
                                    basicId: record?.basicId,
                                    relation: record?.relationId,
                                    DepentId: "",
                                    status: record?.IsDepend,
                                    userRelId: record?.userRelId,
                                    indexNo: record?.number
                                  }
                                  setCheckedList([...checkedList, obj])
                                } else {
                                  let arr = checkedList.filter((item: any) => {
                                    return item?.indexNo !== String(e.target.id);
                                  });
                                  setCheckedList(arr);
                                }
                              }}
                            />
                          </DataCell>
                        </DataRow>
                      );
                    })}
                  </DataParent>
                </Table>
              </Box>
            </Box>
          </Box>
          <Box className="row justify-content-end">
            <Box className="col-auto">
              <Button
                className={`col-auto  py-1 px-4 mx-0 ${styles.button}`}
                color="yellowGroup"
                size="md"
                onClick={() => { addUnMapped(checkedList) }}

              >
                <Text
                  //@ts-ignore
                  size="h6"
                  //@ts-ignore
                  color="gray8"
                  className={styles.button}
                >
                  Save
                </Text>
              </Button>&nbsp;
            </Box>
          </Box>

        </Box>
      </Box>
    </>
  )
}

export default UnMappedMember