
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input";
import SelectMenu from "@ui/Select/Select";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import DeleteIcon from "App/icons/DeleteIcon";
import PencilFill from "App/icons/PencilFill";
import { Button } from "@ui/Button/Button";
import React, { useState, useEffect, useRef } from "react";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import { createIncome } from "App/api/financialPlan"
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import { deleteIncome } from "App/api/financialPlan"


const validationSchemaIN = {
  RequiredIN: {
    Amount: Yup.string()
      .required("Amount  is required")
      .matches(/^\d*$/gms, "Please enter only numeric amount")
      .trim(),

    IncomeSource: Yup.string()
      .required("Income Source  is required")
      .trim(),

    Frequency: Yup.string()
      .required("Frequency  is required")
      .trim(),
  },
  Other: {
    OtherName: Yup.string().required("Income Name Required").trim()
  },
};

export type VerifyKYCTypes = {
  Amount: string;
  IncomeSource: string;
  Frequency: string,
  OtherName: string;
};

type KYCEventTypes = {
  clickOnSubmit: (value: VerifyKYCTypes) => any;
};
interface StoreTypes {
  verifyKYC: VerifyKYCTypes;
  setVerifyKYC: (payload: VerifyKYCTypes[]) => void;
}
const useRegistrationStore = create<StoreTypes>((set) => ({
  //* initial state
  verifyKYC: {
    Amount: "",
    IncomeSource: "",
    Frequency: "",
    OtherName: ""
  },
  setVerifyKYC: (payload: any) =>
    set((state) => ({
      ...state,
      verifyKYC: payload,
    })),
}));
type SaveTypes = {
  // setEditModalOpen: (values: boolean) => void;
  fetchData: () => void;
};

const Income = ({ fetchData }: SaveTypes) => {

  const local: any = getUser();
  const AmountInput = React.useRef();
  // const [tempData, setTempData] = useState<any>();
  const { verifyKYC, loading, setVerifyKYC } = useRegistrationStore((state: any) => state);
  const [loaderApi, setLoaderApi] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [goalId, setGoalId] = useState<any>({});
  const [buttonText, setButtonText] = useState("Add");
  const [validationSchema, setvalidationSchema] = useState(validationSchemaIN.RequiredIN);
  const [showIncomeName, setShowIncomeName] = useState(false);
  const { inflowList, frequencyList, incomeDetailsList,
    localPlanId
  } = useFinancialPlanStore();

  useEffect(() => {
    let obj = validationSchemaIN.RequiredIN;
    if (showIncomeName) {
      obj = { ...obj, ...validationSchemaIN.Other }

    }
    setvalidationSchema(obj);
    return () => { }
  }, [showIncomeName])


  //Update Income API
  const UpdateIncome = async (data: any) => {
    data = data || {};

    try {
      setLoaderApi(true);
      const obj: any =
      {
        amount: parseInt(data?.Amount),
        basicId: local?.basicid,
        employeeId: local?.rmCode,// FIN22062021193
        frequencyTypeId: parseInt(data?.Frequency),
        frequencyTypeName: "",
        id: goalId?.id,
        inflowTypeId: parseInt(data?.IncomeSource),
        inflowTypeName: data?.OtherName,
        planId: goalId?.planId
      };
      const enc: any = encryptData(obj);

      let result: any = await createIncome(enc);
      if (result?.status === "Success") {
        fetchData();
        setShowIncomeName(false);
        toastAlert("success", "Income Successfully Updated");
        setLoaderApi(false);
        setButtonText("Add");
        return true;
      }
      else {
        setLoaderApi(false);
        setButtonText("Add");
        toastAlert("warn", result?.msg);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }
  //Create Income API
  const CreateNewIncome = async (data: any) => {
    data = data || {};
    try {
      setLoaderApi(true);
      const obj: any =
      {
        amount: parseInt(data?.Amount),
        basicId: local?.basicid,
        employeeId: local?.rmCode,//FIN22062021193
        frequencyTypeId: parseInt(data?.Frequency),
        frequencyTypeName: "",
        inflowTypeId: parseInt(data?.IncomeSource),
        inflowTypeName: data?.OtherName,
        planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid
      };
      const enc: any = encryptData(obj);

      let result: any = await createIncome(enc);
      if (result?.status === "Success") {
        fetchData();
        setShowIncomeName(false);
        toastAlert("success", "Income Successfully Added");
        setLoaderApi(false);
        return true;
      }
      else {
        setLoaderApi(false);
        setButtonText("Add");
        toastAlert("warn", result?.msg);
        return false;
      }


    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }

  const setIncome = async (record: any) => {
    if (buttonText === "Add") {
      const check = await CreateNewIncome(record);
      return check;
    }
    else if (buttonText === "Update") {
      const check = await UpdateIncome(record);
      return check;
    }
  }

  const IncomeDelete = async (goalId: any) => {
    try {
      const enc: any = encryptData(goalId);
     
      const result: any = await deleteIncome(enc);
     
      fetchData();
      setOpenDelete(false);
      toastAlert("success", "Income Deleted Successfully");
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }
 

  return (
    <>
      <Box className="row justify-content-center">
        <Box className="col-10">
          <Formik
            initialValues={verifyKYC}
            validationSchema={Yup.object().shape(validationSchema)}
            enableReinitialize
            onSubmit={async (values, { resetForm }) => {
              //CreateNewIncome(values);
              // @ts-ignore

              const check = await setIncome(values);
              if (check) {
                setVerifyKYC({
                  Amount: "",
                  IncomeSource: "",
                  Frequency: "",
                  OtherName: ""
                })
                resetForm();
              }
            }}
          >
            {({
              values,
              handleReset,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (

              <>
                <Box className="row">
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu
                        items={inflowList}
                        bindValue={"ID"}
                        bindName={"Type_name"}
                        label={"Income Source"}
                        name="IncomeSource"
                        value={values?.IncomeSource}
                        onChange={(e: any) => {
                          setFieldValue("IncomeSource", e?.ID);
                          if (e?.Type_name.trim() === "Other") {
                            setShowIncomeName(true);
                          }
                          else {
                            setShowIncomeName(false);
                          }
                        }}
                        required
                        error={submitCount ? errors.IncomeSource : undefined}
                      />
                    </Box>
                  </Box>
                  {showIncomeName ?
                    <Box className="col-lg-4 col-md-5 col-sm-12">
                      <Box>
                        <Input label="Income Name" name="OtherName" placeholder="Enter Income Name"
                          required
                          error={submitCount ? errors.OtherName : null}
                          // onChange={handleChange}
                          onChange={(e: any) => {
                            setFieldValue("OtherName", e?.target?.value)
                          }}
                          value={values?.OtherName} />
                      </Box>
                    </Box> : null}
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input label="Amount" name="Amount" placeholder="Enter Amount"
                        required
                        ref={AmountInput}
                        error={submitCount ? errors.Amount : null}
                        // onChange={handleChange}
                        onChange={(e: any) => {
                          setFieldValue("Amount", (e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, '')).toString())
                        }}
                        value={values?.Amount} />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu
                        items={frequencyList}
                        bindValue={"ID"}
                        bindName={"Type_name"}
                        label={"Frequency"}
                        placeholder={"Select"}
                        // error={submitCount ? errors.Frequency : null}
                        // onChange={handleChange}
                        // value={values?.Frequency}

                        value={values?.Frequency}
                        onChange={(e: any) => {
                          // setfrquencyFilter(e?.id || "");
                          setFieldValue("Frequency", e.ID);//|| e.ID
                       
                        }}
                        required
                        error={submitCount ? errors.Frequency : undefined}
                      // value={frquencyfilter}
                      // onChange={(e: any) => { setfrquencyFilter(e?.ID || ""); handlefrquencyType(e) }} 
                      />
                    </Box>
                  </Box>
                </Box>

                <Box className="row justify-content-end">
                  <Box className="col-auto">
                    <Button
                      className={`col-auto  mb-2 mx-0 py-1 px-4 ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      // disabled={}
                      onClick={() => {
                        handleReset();
                        setVerifyKYC({
                          Amount: "",
                          IncomeSource: "",
                          Frequency: "",
                          OtherName: ""
                        })
                        setButtonText("Add");
                      }}
                    >
                      <Text
                        //@ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={styles.button}
                      >
                        Reset
                      </Text>
                    </Button>&nbsp;
                    <Button
                      className={`col-auto  mb-2 mx-0 py-1 px-4 ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      onClick={handleSubmit}
                      loading={loading}

                    >
                      <Text
                        //@ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={styles.button}
                      >
                        {buttonText}
                      </Text>
                    </Button>
                  </Box>
                </Box>

              </>)}</Formik>

        </Box>
      </Box>
      <hr className="text-primary mx-0" />

      <Box className="text-center m-3">
        {/* @ts-ignore */}
        <Text weight="normal" size="h4" css={{ color: "var(--colors-blue1)" }}>
          List Of Income Source
        </Text>
      </Box>

      <Box className="row ">
        <Box
          className="col-auto col-lg-12 col-md-12 col-sm-12"
          css={{ overflowX: "auto" }}
        >
          <Box className="table-responsive" css={{ borderRadius: "8px" }}>
            <Table
              className="text-capitalize table table-striped"
            //css={{ color: "var(--colors-blue1)" }}
            >
              <ColumnParent className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                <ColumnRow>
                  {/* <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>
                    <Box class="form-check form-check-inline">
                      <label className="form-check-label me-2" for="inlineCheckbox2">Select All</label>
                      <input className="form-check-input mt-1" type="checkbox" id="inlineCheckbox2" value="option2" />
                    </Box>
                  </Text>
                  </Column> */}
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Income From</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Amount</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Frequency</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Action</Text></Column>

                </ColumnRow>
              </ColumnParent>
              <DataParent>
                {loaderApi ? <>
                  <DataRow>

                    <DataCell colSpan={5} className="text-center"><Loader /> </DataCell>

                  </DataRow>
                </> :
                  <>
                    {incomeDetailsList.length === 0 ? <>
                      <DataRow>
                        <DataCell colSpan={5} className="text-center">No Income Found </DataCell>
                      </DataRow>
                    </> :
                      <>
                       
                        {incomeDetailsList.map((record, index) => {
                          return (
                            <DataRow className="text-center">
                              {/* <DataCell>
                                <Input type="checkbox" />
                              </DataCell> */}
                              <DataCell >{record?.InflowTypeName === "" ? "Other" : record?.InflowTypeName}</DataCell>
                              <DataCell>
                                {Number(record?.Amount).toLocaleString(
                                  "en-IN"
                                ) || "0.0"}
                              </DataCell>
                              <DataCell>{record?.FrequencyTypeName}</DataCell>
                              <DataCell>
                                <Box className="row justify-content-center">
                                  <Box className="col-auto">
                                    {/* @ts-ignore */}
                                    <Text onClick={() => {
                                    
                                      //setTempData(record);
                                      if (String(record?.InflowTypeId) === "23") {
                                        setShowIncomeName(true)
                                      }
                                      else {
                                        setShowIncomeName(false);
                                      }
                                      setVerifyKYC({ Amount: record?.Amount, Frequency: record?.FrequencyTypeId, IncomeSource: record?.InflowTypeId, OtherName: record?.InflowTypeName })
                                      setButtonText("Update");
                                      setGoalId({
                                        planId: record?.PlanId,
                                        basicId: record?.BasicId,
                                        id: record?.Id
                                      });
                                    }}
                                      css={{ cursor: "pointer" }}>
                                      <PencilFill />
                                    </Text>
                                  </Box>

                                  &nbsp;&nbsp;

                                  <Box className="col-auto">
                                    {/* @ts-ignore */}
                                    <Text onClick={() => {
                                      setOpenDelete(true);
                                      setGoalId({
                                        planId: record?.PlanId,
                                        basicId: record?.BasicId,
                                        id: record?.Id
                                      });
                                    }} css={{ cursor: "pointer" }}>
                                      <DeleteIcon />
                                    </Text>
                                  </Box>
                                </Box>
                              </DataCell>
                            </DataRow>
                          );
                        })}
                      </>
                    }
                  </>}
              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>
      <DialogModal
        open={openDelete}
        setOpen={setOpenDelete}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            <Box className="row">
              <Box className="col-auto"><Text css={{ color: "var(--colors-blue1)" }}>Delete Income Warning</Text></Box>
              <Box className="col"></Box>
              <Box className="col-auto"></Box>
            </Box>
          </Box>
          <Box
            className="text-center modal-body p-3"
          >
            <Text css={{ mt: 20, pt: 10 }}>
              Are you sure to delete this Income?

            </Text>
            <Box css={{ mt: 20, pt: 10 }} className="text-end">
              <Button
                type="submit"
                color="yellow"
                onClick={() => { setOpenDelete(false) }}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={() => { IncomeDelete(goalId) }}
                loading={loading}
              >
                Delete Now
              </Button>
            </Box>
          </Box>
        </Box>
      </DialogModal>
    </>
  )
}

export default Income