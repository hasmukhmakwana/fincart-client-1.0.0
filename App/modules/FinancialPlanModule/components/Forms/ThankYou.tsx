import React, { useEffect, useState } from 'react';
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import Box from '@ui/Box/Box'
import Layout, { toastAlert } from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'


const ThankYou = () => {
    return (
    <>

    <Box className="row">
    <Box className="col-12 text-center"></Box>
    <Box className="col-12 pt-4 pb-5 text-center">
    {/* @ts-ignore */}
    <Text size="h2" className="pb-1" css={{ color: "#005CB3"}}>Thank You!</Text>
    {/* @ts-ignore */}
    <Text size="h3" className="pb-1"> Your data is collected you will be notified<br/>
        When your relationship manager generates plan !!!</Text>
    </Box>
        <Box className="col-12 text-center"></Box>
    </Box>
    </>
    )
}

export default ThankYou