import React, { useEffect, useState } from "react";
import TextInput from "react-autocomplete-input";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input/Input";
import SelectMenu from "@ui/Select/Select";
// import styles from "../../../Tools.module.scss";
import 'react-autocomplete-input/dist/bundle.css';
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { StyledTabs, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsTrigger } from '../NewTabsTrigger.styles'
import { TabsContent } from "@radix-ui/react-tabs";
import { Button } from "@ui/Button/Button";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import UnMappedInsurance from "./UnMappedInsurance";
import UnMappedMutualFund from "./UnMappedMutualFund";
import UnmappedOtherAsset from "./UnmappedOtherAsset";
import UnmappedLoan from "./UnmappedLoan";
import DeleteIcon from "App/icons/DeleteIcon";
import PencilFill from "App/icons/PencilFill";
import { fetchAssetsDetails, createInsurance, insurancePolicySearch, mutualFundSchemeSearchFP, getInsuranceUnmapped, getInsuranceMutualFund, getOtherAssetUnmapped, getLoanUnmapped, uploadFilePDF, assetNameSearch, deleteMutualFund, createOtherAsset, deleteOtherAsset, otherAssetSearch } from "App/api/financialPlan";
import { toastAlert } from "App/shared/components/Layout";
import { convertBase64, encryptData, getUser, plainBase64 } from "App/utils/helpers";
import { Label } from "@radix-ui/react-label";
import { clear } from "console";
import Autocomplete from "react-autocomplete";
import MutualFund from "./MutualFund";
import Insurance from "./Insurance";
import OtherAssets from "./OtherAssets";
import Loan from "./Loan";

let recentTabsScreen3 = [
  {
    title: "Mutual Fund",
    value: "mutualFund",
  },
  {
    title: "Insurance",
    value: "insurance",
  },
  {
    title: "Other Assets",
    value: "otherAssets",
  },
  {
    title: "Loan",
    value: "loan",
  },
];


const NetAssetEntry = () => {
  const { basicDetail } = useFinancialPlanStore();

  const local: any = getUser();
  const [deleteId, setDeleteId] = useState<any>({});
  const [openDelete, setOpenDelete] = useState(false);
  const [openFileDialog, setOpenFileDialog] = useState(false);
  const [openUnmappedMF, setOpenUnmappedMF] = useState(false); //AR
  const [openUnmappedIN, setOpenUnmappedIN] = useState(false);
  const [openUnmappedOT, setOpenUnmappedOT] = useState(false);
  const [openUnmappedLN, setOpenUnmappedLN] = useState(false);
  const [casUploadResponse, setCASUploadResponse] = useState<any>();

  const {
    unMappedMF,
    setUnmappedMutualFund,
    setUnmappedInsurance,
    setUnmappedOtherAsset,
    setUnmappedLoan,
    localPlanId,
    localUser,
    setLoader,
    setLocalUser,
    setLoanList,
    setMfList,
    setInsuranceList,
    setOtList
  } = useFinancialPlanStore();
  const [mfDetailsList, setMfDetailsList] = useState<any>([])
  const [inDetailsList, setInDetailsList] = useState<any>([]);
  const [lnDetailsList, setLnDetailsList] = useState<any>([]);
  const [otDetailsList, setOtDetailsList] = useState<any>([]);
  const [ulipDetailsList, setUlipDetailsList] = useState<any>([]);
  const [delLoader, setDelLoader] = useState(false);

  const fetchOtherAssetDetails = async () => {
    try {
      if (localPlanId !== "0") {
        if (localPlanId?.basicid === undefined)
          return;
      }

      setLoader(true);

      const obj = {
        basicId: localUser,
        planid: localPlanId?.basicid === undefined ? "0" : localPlanId?.basicid,
      }
      const enc: any = encryptData(obj);
      let result = await fetchAssetsDetails(enc);
      let objMf: any = [];
      let objIn: any = [];
      let objLn: any = [];
      let objOt: any = [];
      let objUlip: any = [];
      for (let i = 0; i < result?.data.length; i++) {
        const element = result?.data[i];

        switch (result?.data[i].category) {
          case "MF":
            objMf.push(result?.data[i]);
            break;

          case "IN":
            objIn.push(result?.data[i]);
            break;

          case "LN":
            objLn.push(result?.data[i]);
            break;

          case "OT":
            objOt.push(result?.data[i]);
            break;

          case "ULIP":
            objUlip.push(result?.data[i]);
            break;
        }
      }
      setMfList(objMf);
      setInsuranceList(objIn);
      setOtList(objOt);
      setLoanList(objLn);
      setMfDetailsList(objMf);
      setInDetailsList(objIn);
      setLnDetailsList(objLn);
      setOtDetailsList(objOt);
      setUlipDetailsList(objUlip);
      setLoader(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }

  useEffect(() => {

    if (localUser !== "" && localPlanId !== "") {

      fetchOtherAssetDetails();
    }
  }, [])

  const fetchMutualFundUnmapped = async () => {
    try {
      if (localPlanId !== "0") {
        if (localPlanId?.basicid === undefined)
          return;
      }
      setLoader(true);
      const obj: any = {
        basicId: local?.basicid,
        planid: localPlanId?.basicid === undefined ? "0" : localPlanId?.basicid,
        fpAddeed: '0'
      }
      const enc: any = encryptData(obj);
      let result: any = await getInsuranceMutualFund(enc);
      setUnmappedMutualFund(result?.data?.Table)
      setLoader(false);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }

  }
  const fetchInsuranceUnmapped = async () => {
    try {
      setLoader(true);
      const enc: any = encryptData(local?.basicid, true);
      let result: any = await getInsuranceUnmapped(enc);
      setUnmappedInsurance(result?.data?.Insurance_List)
      setLoader(false);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  }

  const fetchOtherAssetUnmapped = async () => {
    try {
      setLoader(true);
      const obj: any = {
        basicId: local?.basicid,
        Type: 'OT'
      }
      const enc: any = encryptData(obj);
      let result: any = await getOtherAssetUnmapped(enc);
      setUnmappedOtherAsset(result?.data?.Other_Asset_List)
      setLoader(false);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }

  }

  const fetchLoanUnmapped = async () => {
    try {
      setLoader(true);
      const obj: any = {
        basicId: local?.basicid,
        Type: 'LN'
      }
      const enc: any = encryptData(obj);
      let result: any = await getLoanUnmapped(enc);
      setUnmappedLoan(result?.data?.Loan_Asset_List)
      setLoader(false);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }

  }

  const AssetDelete = async (deleteId: any) => {
    setDelLoader(true);
    try {
      const enc: any = encryptData(deleteId);
      
      const result: any = await deleteOtherAsset(enc);
    
      fetchOtherAssetDetails();
      setDelLoader(false);
      setOpenDelete(false);
      toastAlert("success", "Asset Deleted Successfully");
    } catch (error: any) {
      console.log(error);
      setDelLoader(false);
      toastAlert("error", error);
    }
  }

  useEffect(() => {
    setLocalUser(local?.basicid);
    fetchMutualFundUnmapped();
    fetchInsuranceUnmapped();
    fetchOtherAssetUnmapped();
    fetchLoanUnmapped();
    return () => { }
  }, [])
  //#endregion
  return (
    <>
      <StyledTabs defaultValue="mutualFund">
        <TabsList
          aria-label="Manage your Plan"
          className="tabsCardStyle justify-content-center row"
          css={{ mb: 20 }}
        >
          {recentTabsScreen3.map((item: any, index: any) => {
            return (
              <TabsTrigger
                value={item.value}
                className="tabsCards border-bottom border-0 rounded-0 col-auto"//tryby adding wordwrap none 
                key={item.value}
              >
                <Box className="text-center">
                  {/* @ts-ignore */}
                  <Text size="bolder">{item.title}</Text>
                </Box>
              </TabsTrigger>
            )
          })}
        </TabsList>
        <TabsContent value="mutualFund">
          <MutualFund mfDetailsList={mfDetailsList} fetchOtherAssetDetails={fetchOtherAssetDetails} setOpenFileDialog={setOpenFileDialog} setOpenDelete={setOpenDelete} setDeleteId={setDeleteId} setCASUploadResponse={setCASUploadResponse} setOpenUnmappedMF={setOpenUnmappedMF} fetchMutualFundUnmapped={fetchMutualFundUnmapped} />
        </TabsContent >

        < TabsContent value="insurance" >
          <Insurance inDetailsList={inDetailsList} fetchOtherAssetDetails={fetchOtherAssetDetails} setOpenDelete={setOpenDelete} setDeleteId={setDeleteId} setOpenUnmappedIN={setOpenUnmappedIN} />
        </TabsContent >
        < TabsContent value="otherAssets" >
          <OtherAssets otDetailsList={otDetailsList} fetchOtherAssetDetails={fetchOtherAssetDetails} setOpenDelete={setOpenDelete} setDeleteId={setDeleteId} setOpenUnmappedOT={setOpenUnmappedOT} />
        </TabsContent >
        < TabsContent value="loan" >
          <Loan lnDetailsList={lnDetailsList} fetchOtherAssetDetails={fetchOtherAssetDetails} setOpenDelete={setOpenDelete} setDeleteId={setDeleteId} setOpenUnmappedLN={setOpenUnmappedLN} />
        </TabsContent >
      </StyledTabs >

      <DialogModal
        open={openDelete}
        setOpen={setOpenDelete}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      ><Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3">
            <Box className="row">
              <Box className="col-auto text-light">Delete Asset Warning</Box>
              <Box className="col"></Box>
              <Box className="col-auto"></Box>
            </Box>
          </Box>
          <Box
            className="text-center modal-body modal-body-scroll"
            css={{ padding: "0.5rem" }}
          >
            <Text css={{ mt: 20, pt: 10 }}>
              Are you sure to delete this Asset?

            </Text>
            <Box css={{ mt: 20, pt: 10 }} className="text-end">
              <Button
                type="submit"
                color="yellow"
                disabled={delLoader}
                onClick={() => { setOpenDelete(false) }}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                disabled={delLoader}
                onClick={() => { AssetDelete(deleteId) }}
              >
                {delLoader ? 'Loading....' : ' Delete Now'}
              </Button>
            </Box>
          </Box>
        </Box>
      </DialogModal>


      <DialogModal
        open={openFileDialog}
        setOpen={setOpenFileDialog}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
          borderRadius: "15px"
        }}
      >
        <Card css={{ p: 25, pb: 5, mb: 15 }}>
          <Table className="text-capitalize table table-striped">
            {/* <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}> */}
            <ColumnParent className="text-capitalize p-3">
              <ColumnRow>
                <Column className="pb-2"><Text css={{ fontWeight: "normal" }}>Duplicate Record</Text></Column>
                <Column className="pb-2"><Text css={{ fontWeight: "normal" }}>Failed Record</Text></Column>
                <Column className="pb-2"><Text css={{ fontWeight: "normal" }}>Success Record</Text></Column>
                <Column className="pb-2"><Text css={{ fontWeight: "normal" }}>Total Record</Text></Column>
              </ColumnRow>
            </ColumnParent>
            <DataParent>
              <DataRow>
                <DataCell className="pb-0 text-center">{casUploadResponse?.duplicateRecords}</DataCell>
                <DataCell className="pb-0 text-center">{casUploadResponse?.failedRecords}</DataCell>
                <DataCell className="pb-0 text-center">{casUploadResponse?.successRecords}</DataCell>
                <DataCell className="pb-0 text-center">{casUploadResponse?.totalRecords}</DataCell>
              </DataRow>
            </DataParent>
          </Table>
          <Box css={{ mb: 15, mt: 10 }} className="text-center">
            <Button
              type="submit"
              color="yellow"
              onClick={() => { setOpenFileDialog(false) }}>
              OK
            </Button>
          </Box>
          <Box>
            <Text size="h6">Click on 'Un-Mapped Entries' to view imported folios</Text>
          </Box>
        </Card>
      </DialogModal>

      <DialogModal
        open={openUnmappedMF}
        setOpen={setOpenUnmappedMF}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "60%" },
          "z-index": 2
        }}
      >
        <UnMappedMutualFund setOpenUnmappedMF={setOpenUnmappedMF} fetchOtherAssetDetails={fetchOtherAssetDetails} fetchMutualFundUnmapped={fetchMutualFundUnmapped} />
      </DialogModal>

      <DialogModal
        open={openUnmappedLN}
        setOpen={setOpenUnmappedLN}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "60%" },
          "z-index": 2
        }}
      >
        <UnmappedLoan setOpenUnmappedLN={setOpenUnmappedLN} fetchOtherAssetDetails={fetchOtherAssetDetails} fetchLoanUnmapped={fetchLoanUnmapped} />
      </DialogModal>
      <DialogModal
        open={openUnmappedOT}
        setOpen={setOpenUnmappedOT}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "60%" },
          "z-index": 2
        }}
      >
        <UnmappedOtherAsset setOpenUnmappedOT={setOpenUnmappedOT} fetchOtherAssetDetails={fetchOtherAssetDetails} fetchOtherAssetUnmapped={fetchOtherAssetUnmapped} />
      </DialogModal>
      <DialogModal
        open={openUnmappedIN}
        setOpen={setOpenUnmappedIN}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "60%" },
          "z-index": 2
        }}
      >
        <UnMappedInsurance setOpenUnmappedIN={setOpenUnmappedIN} fetchOtherAssetDetails={fetchOtherAssetDetails} fetchInsuranceUnmapped={fetchInsuranceUnmapped} />
      </DialogModal>

    </>
  );
};

export default NetAssetEntry;
