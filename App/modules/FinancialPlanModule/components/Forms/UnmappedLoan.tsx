import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { Button } from "@ui/Button/Button";
import SelectMenu from "@ui/Select/Select";
import React, { useState, useEffect } from "react";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import { convertBase64, encryptData, getUser, plainBase64 } from "App/utils/helpers";
import { createOtherAsset } from "App/api/financialPlan";
import { toastAlert } from "App/shared/components/Layout";
import Checkbox from "@ui/Checkbox/Checkbox";


const UnMappedMeber = ({setOpenUnmappedLN,fetchOtherAssetDetails, fetchLoanUnmapped}:any) => {
  const {
    unMappedLoan,
    relationList,
    frequencyList,
    localPlanId
  } = useFinancialPlanStore();
  const [checkedList, setCheckedList] = useState<any>([]);
  const local: any = getUser();
  const [loader, setLoader] = useState<boolean>(false);
  useEffect(() => {
    let arr:any=[];
    unMappedLoan.map((ele, index)=>{
       let obj={
        LoanSchemeId: null,
        assetName: ele?.assetName,
        assetTypeId: ele?.assetTypeId,
        basicId: local?.basicid,
        frequencyTypeId: "",
        liquidityType: "",
        loanEmiAmount: "",
        loanEndDate: "",
        loanInterest: "",
        loanOutstandAmount: "",
        loanPrincipalAmount: ele?.loanPrincipalAmount,
        loanStartDate: "",
        planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid,
        rmCode: local?.rmCode,
        schemeId: ele?.schemeId,
        schemeName:ele?.schemeName,
        trxnSource: "",
        i:index,
        isAdd:false
       }
       arr.push(obj)
    })
    setCheckedList(arr);
 },[unMappedLoan])

  const InterestAdding=(record:any, val:any, flag:any)=>{
    const newState = checkedList.map((obj:any) => {
      if (obj.i === record?.i) {
        switch(flag){
          case "interest":
            return {...obj, loanInterest: val};
            break;
          case "emiAmount":
            return {...obj, loanEmiAmount: val};
          case "outStandAmt":
            return {...obj, loanOutstandAmount:val};
          case "trxnSource":
            return {...obj, trxnSource:val}
          case "frequency":
            return {...obj, frequencyTypeId:val}
        }
        
      }

      return obj;
    });

    setCheckedList(newState);
  }

  const saveUnMappedEntries=async()=>{
    let arr=checkedList.filter((el:any)=> el.isAdd===true)
    if(arr.length>=1){
      setLoader(true);
    let resultArr:any=[];
    arr.map((ele:any)=>{
        let obj={
          LoanSchemeId: null,
          assetName: ele?.assetName,
          assetTypeId: ele?.assetTypeId,
          basicId: local?.basicid,
          frequencyTypeId: ele?.frequencyTypeId,
          liquidityType: "",
          loanEmiAmount: ele?.loanEmiAmount,
          loanEndDate: "",
          loanInterest: ele?.loanInterest,
          loanOutstandAmount: ele?.loanOutstandAmount,
          loanPrincipalAmount: ele?.loanPrincipalAmount,
          loanStartDate: "",
          planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid,
          rmCode: local?.rmCode,
          schemeId: ele?.schemeId,
          schemeName:ele?.schemeName,
          trxnSource: ele?.trxnSource,
        }
        resultArr.push(obj);
      })

        try{
    
          const enc: any = encryptData(resultArr);
          const result: any = await createOtherAsset(enc);
          if(result.status==="Success"){
              toastAlert("success", "Unmapped Loan Successfully Added");
              setLoader(false);
              setOpenUnmappedLN(false);
              fetchOtherAssetDetails();
              fetchLoanUnmapped();
          }
        }
        catch(error:any){
          console.log(error);
          setLoader(false);
          toastAlert("error", error);
        }
    }
    else{
      return
    }
  }

  const addInCheckedList=(ele:any, isAdd:any)=>{
   
    const newState = checkedList.map((obj:any) => {
     if (obj.i === ele?.i) {
       return {...obj, isAdd: isAdd};
     }

     return obj;
   });

   setCheckedList(newState);

 }


  return (
    //@ts-ignore
    <>
      <Box>
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}>Un-Mapped Loan</Text>
        </Box>
        <Box className={`modal-body p-3`} >

          <Box className={`row ${styles.scrollit}`}>
            <Box css={{ overflow: "auto" }}
              className="col-auto col-lg-12 col-md-12 col-sm-12" >
              <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                <Table className="text-capitalize table table-striped" >
                  <ColumnParent className="text-capitalize ">
                    <ColumnRow className="sticky" css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}  >
                      <Column><Text size="h5">Asset Name</Text></Column>
                      <Column><Text size="h5">Scheme name</Text></Column>
                      <Column><Text size="h5">Principal Amount</Text></Column>
                      <Column><Text size="h5">Interest</Text></Column>
                      <Column><Text size="h5">EMI Amt.</Text></Column>
                      <Column><Text size="h5">Out. Amt.</Text></Column>
                      <Column><Text size="h5">Source</Text></Column>
                      <Column><Text size="h5">Frequency</Text></Column>
                      <Column><Text size="h5">Is Added</Text></Column>
                    </ColumnRow>
                  </ColumnParent>

                  <DataParent css={{ textAlign: "center" }} >
                    {checkedList.map((record:any, index:any) => {
                      return (
                        <DataRow>
                          <DataCell>{record?.assetName} </DataCell>
                          <DataCell>{record?.schemeName} </DataCell>
                          <DataCell>{Number(record?.loanPrincipalAmount).toLocaleString("en-In")}</DataCell>
                          <DataCell>
                          <Input type="text" value={record?.loanInterest} onChange={(e:any)=>{InterestAdding(record,e.target.value, "interest")}}/>
                          </DataCell>
                          <DataCell>
                          <Input type="text" value={record?.loanEmiAmount} onChange={(e:any)=>{InterestAdding(record,e.target.value,"emiAmount")}}/>
                          </DataCell>
                          <DataCell>
                          <Input type="text" value={record?.loanOutstandAmount} onChange={(e:any)=>{InterestAdding(record,e.target.value, "outStandAmt")}}/>
                          </DataCell>
                          <DataCell> 
                          <SelectMenu
                          value={record?.trxnSource}
                            items={[
                              { id: "Fincart", name: "From Us" },
                              { id: "External", name: "External" },
                            ]}
                            onChange={(e: any) => {
                             InterestAdding(record, e?.id, "trxnSource")
                            }}
                            bindValue={"id"}
                            bindName={"name"}
                          />
                          </DataCell>
                          <DataCell> 
                          <SelectMenu
                          value={record?.frequencyTypeId}
                            items={frequencyList}
                            onChange={(e: any) => {
                             InterestAdding(record, e?.ID, "frequency")
                            }}
                            bindValue={"ID"}
                            bindName={"Type_name"}
                            name="FrequencyMF"
                         
                          />
                          </DataCell>
                          <DataCell>
                          <Checkbox
                              label=""
                              name="fp_added"
                              id="add"
                              checked={record?.isAdd}
                              onChange={(e:any)=>{addInCheckedList(record,e.target.checked)}}
                            />
                          </DataCell>
                        </DataRow>
                      );
                    })}
                  </DataParent>
                </Table>
              </Box>
            </Box>
          </Box>
          <Box className="row justify-content-end">
            {unMappedLoan.length!==0?
            <Box className="col-auto">
              <Button
                className={`col-auto  py-1 px-4 mx-0 ${styles.button}`}
                color="yellowGroup"
                size="md"
                disabled={loader}
                onClick={()=>{saveUnMappedEntries()}}
              >
                <Text
                  //@ts-ignore
                  weight="normal"
                  size="h6"
                  //@ts-ignore
                  color="gray8"
                  className={styles.button}
                >
                  {loader?`Loading...`:`Save`}
                </Text>
              </Button>&nbsp;
            </Box>:null}
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default UnMappedMeber