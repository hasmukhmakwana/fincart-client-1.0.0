import React, { useEffect, useState } from "react";
import TextInput from "react-autocomplete-input";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input/Input";
import SelectMenu from "@ui/Select/Select";
// import styles from "../../../Tools.module.scss";
import 'react-autocomplete-input/dist/bundle.css';
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { Button } from "@ui/Button/Button";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import UnMappedMutualFund from "./UnMappedMutualFund";
import DeleteIcon from "App/icons/DeleteIcon";
import PencilFill from "App/icons/PencilFill";
import { createInsurance, mutualFundSchemeSearchFP, getInsuranceUnmapped, getInsuranceMutualFund, getOtherAssetUnmapped, getLoanUnmapped, uploadFilePDF, assetNameSearch, deleteMutualFund, createOtherAsset, deleteOtherAsset, otherAssetSearch } from "App/api/financialPlan";
import { toastAlert } from "App/shared/components/Layout";
import { convertBase64, encryptData, getUser, plainBase64 } from "App/utils/helpers";
import { Label } from "@radix-ui/react-label";
import { clear } from "console";
import Autocomplete from "react-autocomplete";

const validationMutualFundSchema = {
  mandatory: {
    InvestmentType: Yup.string().required("Investment Type is required").trim(),
    SchemeName: Yup.string().required("Scheme Name is required").trim(),
    // FolioNo: Yup.string().required("Folio No. is required").trim(),
    CurrentValue: Yup.string().required("Current Value is required").trim(),
    TransactionSource: Yup.string().required("Transaction Source is required").trim(),
    FrequencyMF: Yup.string().required("Frequency is required").trim(),
  },
  SIP: {
    SIPAmount: Yup.string().required("SIP Amount is required").trim(),
  }
}

const validateUploadFile = Yup.object().shape({
  FilePwdMF: Yup.string().required("Password is required").trim(),
  FileTypeMF: Yup.string().required("File Type is required").trim(),
})

export type VerifyMutualFundTypes = {
  InvestmentType: string;
  SchemeName: string;
  SIPAmount: string;
  FolioNo: string;
  CurrentValue: string;
  TransactionSource: string;
  FrequencyMF: string;
}

export type VerifyUploadFileTypes = {
  FilePwdMF: string;
  FileTypeMF: string;
}


interface StoreTypes {
  verifyMutualFund: VerifyMutualFundTypes;
  setVerifyMutualFund: (payload: VerifyMutualFundTypes[]) => void;
  verifyUploadFile: VerifyUploadFileTypes;
}

const useRegistrationStore = create<StoreTypes>((set) => ({
  //* initial state
  verifyMutualFund: {
    InvestmentType: "",
    SchemeName: "",
    SIPAmount: "",
    FolioNo: "",
    CurrentValue: "",
    TransactionSource: "",
    FrequencyMF: "",
  },

  setVerifyMutualFund: (payload: any) =>
    set((state) => ({
      ...state,
      verifyMutualFund: payload,
    })),

  verifyUploadFile: {
    FilePwdMF: "",
    FileTypeMF: "",
  },
  setVerifyUploadFile: (payload: any) =>
    set((state) => ({
      ...state,
      verifyUploadFile: payload,
    })),
}));



const MutualFund = ({ mfDetailsList, fetchOtherAssetDetails, setOpenFileDialog, setOpenDelete, setDeleteId, setCASUploadResponse, setOpenUnmappedMF, fetchMutualFundUnmapped }: any) => {

  const [validationSchemaMF, setValidationSchemaMF] = useState(validationMutualFundSchema.mandatory);
  const [investment, setInvestment] = useState(null);
  const [btnState, setBtnState] = useState(false);
  useEffect(() => {
    let obj = validationMutualFundSchema.mandatory;
    if (investment !== "LumpSum") {
      obj = {
        ...obj,
        ...validationMutualFundSchema.SIP
      }
    }
    setValidationSchemaMF(obj)
    return () => { }
  }, [investment])


  const local: any = getUser();
  const { loading, verifyMutualFund, setVerifyMutualFund, verifyUploadFile, setVerifyUploadFile } = useRegistrationStore((state: any) => state);
  const [schemeList, setSchemeList] = useState([]);
  const [buttonText, setButtonText] = useState("Add");
  const [investmentType, setInvestmentType] = useState("SIP");
  const [loaderApi, setLoaderApi] = useState(false);
  const [MFId, setMfId] = useState<any>({});

  const {
    frequencyList,
    localPlanId,
  } = useFinancialPlanStore();




  const searchScheme = async (part: any) => {

    try {
      const obj: any = {
        Type: "ALL",
        SearchText: part
      }

      const enc: any = encryptData(obj);
      const result: any = await mutualFundSchemeSearchFP(enc);
      let schemeNames: any = [];

      result?.data?.Value.map((record: any) => {
        schemeNames.push(record.Text);
      });
      setSchemeList(result?.data?.Value);

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);

    }
  }

  const CreateNewMutualFund = async (data: any) => {
    data = data || {};
    try {
      setLoaderApi(true);
      let Scheme: any = schemeList.filter((x: any) => {
        if (x.Text === data?.SchemeName) {
          return x.Value;
        }
      });
      if(Scheme.length===0){
        setLoaderApi(false);
        toastAlert("warn", "No Scheme Found");
        return false; 
      }
      const obj: any =
        [{
          assetName: Scheme?.[0]?.Text,
          assetTypeId: 7,//mutual fund
          basicId: local?.basicid,
          currentValue: data?.CurrentValue,
          folioNo: data?.FolioNo,
          frequencyTypeId: parseInt(data?.FrequencyMF),
          liquidityType: "",
          sipAmount: Number(data?.SIPAmount),
          mfInvestType: data?.InvestmentType,
          planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid,
          rmCode: local?.rmCode,
          schemeId: parseInt(Scheme?.[0]?.Value),
          trxnSource: data?.TransactionSource
        }]
      const enc: any = encryptData(obj);

      const result: any = await createInsurance(enc);
      if (result?.status === "Success") {
        fetchOtherAssetDetails();
        toastAlert("success", "Mutual Fund Successfully Added");
        setLoaderApi(false);
        setButtonText("Add");
        return true;
      }
      else {
        setLoaderApi(false);
        setButtonText("Add");
        toastAlert("warn", result?.msg);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }

  const UpdateMutualFund = async (data: any) => {
    data = data || {};
    try {
      setLoaderApi(true);
      let SchemeId: any = schemeList.filter((x: any) => {
        if (x.Text === data?.SchemeName) {
          return x.Value;
        }
      });
      if(SchemeId.length===0){
        setLoaderApi(false);
        toastAlert("warn", "No Scheme Found");
        return false; 
      }
      const obj: any =
        [{
          assetName: SchemeId?.[0]?.Text,
          assetTypeId: 7,
          basicId: local?.basicid,
          currentValue: data?.CurrentValue,
          folioNo: data?.FolioNo,
          frequencyTypeId: parseInt(data?.FrequencyMF),
          liquidityType: "",
          id: MFId?.id,
          sipAmount: Number(data?.SIPAmount),
          mfInvestType: data?.InvestmentType,
          planId: localPlanId?.basicid,
          rmCode: local?.rmCode,
          schemeId: SchemeId?.[0]?.Value,
          trxnSource: data?.TransactionSource
        }]
      const enc: any = encryptData(obj);

      const result: any = await createInsurance(enc);
      if (result?.status === "Success") {
        fetchOtherAssetDetails();
        toastAlert("success", "Mutual Fund Successfully Updated");
        setLoaderApi(false);
        setButtonText("Add");
        return true;
      }
      else {
        setLoaderApi(false);
        setButtonText("Add");
        toastAlert("warn", result?.msg);
        return false;
      }
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }

  const setMutualFund = async (record: any) => {
    if (buttonText === "Add") {
      const check = await CreateNewMutualFund(record);
      return check;
    }
    else if (buttonText === "Update") {
      const check = await UpdateMutualFund(record);
      return check;
    }
  }

  const UploadCASFile = async (data: any) => {
    data = data || {};

    setBtnState(true);
    try {
      setLoaderApi(true);
      const obj: any =
      {
        basicid: local?.basicid,
        password: data?.FilePwdMF,
        Pdf_File: plainBase64(data?.FileTypeMF)
      }
      const enc: any = encryptData(obj);
      const result: any = await uploadFilePDF(enc);
      if (result?.status === "Success") {
        setBtnState(false);
      
        setCASUploadResponse(result?.data);
        toastAlert("success", "File Successfully Uploaded");
        setOpenFileDialog(true);
        setLoaderApi(false);
        fetchOtherAssetDetails();
        fetchMutualFundUnmapped();

        return true;
      }
      else {
        setBtnState(false);
        toastAlert("warn", result?.msg);
        setLoaderApi(false);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      setBtnState(false);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }

  //#endregion
  return (
    <>
      <Box className="row justify-content-center">
        <Box className="col-10">
          <Formik
            initialValues={verifyMutualFund}
            validationSchema={Yup.object().shape(validationSchemaMF)}
            enableReinitialize
            onSubmit={async (values, { resetForm }) => {
              const check = await setMutualFund(values);
              if (check) {
                resetForm();
                setVerifyMutualFund({
                  InvestmentType: "",
                  SchemeName: "",
                  SIPAmount: "",
                  FolioNo: "",
                  CurrentValue: "",
                  TransactionSource: "",
                  FrequencyMF: "",
                })
              }
            }}
          >
            {({
              values,
              handleReset,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (
              <>
                <Box className="row">
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu
                        name="InvestmentType"
                        items={[
                          { id: "S", InvestmentType: "SIP" },
                          { id: "L", InvestmentType: "LumpSum" },
                        ]}
                        label={"Investment Type"}
                        bindValue={"id"}
                        bindName={"InvestmentType"}
                        value={values.InvestmentType}
                        required
                        onChange={(e: any) => {
                          handleChange;
                          setFieldValue("InvestmentType", e?.id);
                          setInvestment(e.InvestmentType)
                          setInvestmentType(e.InvestmentType);
                          if (e.InvestmentType === "LumpSum") {
                            setFieldValue("SIP", null);
                          }
                        }}
                        error={submitCount ? errors.InvestmentType : null}

                      />
                    </Box>
                  </Box>
                  {investmentType === "SIP" ||
                    investmentType === "LumpSum" ? (
                    <>
                      <Box className="col-lg-4 col-md-5 col-sm-12">
                        <Box>
                          <Label style={{
                            marginBottom: 5, fontSize: '0.7rem', color: '#339cff',
                            fontWeight: 600, letterSpacing: '0.1rem'
                          }}>
                            Scheme Name<span style={{ color: "#e5484d" }}>&nbsp;*</span>
                          </Label>
                          <br></br>
                          <Autocomplete
                            getItemValue={(item: any) => item.Text}
                            items={schemeList}
                            menuStyle={{
                              borderRadius: '3px',
                              boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                              background: 'rgba(255, 255, 255, 0.9)',
                              padding: '15px, 0',
                              fontSize: '75%',
                              position: 'absolute',
                              overflow: 'auto',
                              zIndex: "1",
                              maxHeight: '25%', // TODO: don't cheat, let it flow to the bottom
                            }}
                            renderItem={(item: any, isHighlighted: any) =>
                              <div style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
                                {item.Text}
                              </div>
                            }
                            value={values?.SchemeName}
                            onChange={(e: any) => { setFieldValue("SchemeName", e.target.value); searchScheme(e.target.value) }}
                            onSelect={(val: any) => { setFieldValue("SchemeName", val); }}
                          />

                          {/* @ts-ignore */}
                          {submitCount ? <Text size="h6" css={{ color: "#e5484d" }}>{errors.SchemeName}</Text> : ""}
                        </Box>
                      </Box>

                      {investmentType === "SIP" ? (
                        <>
                          <Box className="col-lg-4 col-md-5 col-sm-12">
                            <Box>
                              <Input
                                label="SIP Amount"
                                name="SIPAmount"
                                placeholder="Enter Amount"
                                error={submitCount ? errors.SIPAmount : null}
                                required
                                onChange={(e: any) => {
                                  setFieldValue("SIPAmount", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                                }}
                                value={values?.SIPAmount}
                              />
                            </Box>
                          </Box>
                        </>
                      ) : (
                        ""
                      )}
                      <Box className="col-lg-4 col-md-5 col-sm-12">
                        <Box>
                          <Input
                            label="Folio No."
                            name="FolioNo"
                            placeholder="Enter Folio No..."
                            onChange={(e: any) => {
                              setFieldValue("FolioNo", (e?.target?.value.length > 25 ? e?.target?.value.slice(0, 25) : e?.target?.value).toString())
                            }}
                            value={values?.FolioNo}
                          />
                        </Box>
                      </Box>
                      <Box className="col-lg-4 col-md-5 col-sm-12">
                        <Box>
                          <Input
                            label="Current Value"
                            name="CurrentValue"
                            placeholder="Enter Value..."
                            error={submitCount ? errors.CurrentValue : null}
                            required
                            //onChange={handleChange}
                            onChange={(e: any) => {
                              setFieldValue("CurrentValue", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                            }}
                            value={values?.CurrentValue}
                          />
                        </Box>
                      </Box>
                      <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                        <Box>
                          <SelectMenu
                            items={[
                              { id: "Fincart", TransactionSource: "From Us" },
                              { id: "External", TransactionSource: "External" },
                            ]}
                            bindValue={"id"}
                            bindName={"TransactionSource"}
                            label={"Transaction Source"}
                            name={"TransactionSource"}
                            value={values.TransactionSource}
                            onChange={(e: any) => {
                              setFieldValue("TransactionSource", e.id);
                            }}
                            required
                            error={submitCount ? errors.TransactionSource : undefined}
                          />
                        </Box>
                      </Box>
                      <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                        <Box>
                          <SelectMenu
                            items={frequencyList}
                            bindValue={"ID"}
                            bindName={"Type_name"}
                            name="FrequencyMF"
                            label={"Frequency"}
                            value={values.FrequencyMF}
                            onChange={(e: any) => {
                              setFieldValue("FrequencyMF", e.ID);
                            }}
                          />
                        </Box>
                      </Box>
                    </>
                  ) : (
                    ""
                  )}
                </Box>
                <Box className="row justify-content-between">
                  <Box className="col-auto py-1">
                    <Box
                      onClick={() => setOpenUnmappedMF(true)}
                      css={{
                        color: "var(--colors-blue1)",
                        cursor: "pointer",
                      }}//AR
                    >
                      {/* @ts-ignore */}
                      <Text size="h6"><u>Un-Mapped Entries</u></Text>
                    </Box>
                  </Box>
                  <Box className="col-auto">
                    <Button
                      className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      disabled={loaderApi}
                      onClick={() => {
                        handleReset(); setButtonText("Add"); setVerifyMutualFund({
                          InvestmentType: "",
                          SchemeName: "",
                          SIPAmount: "",
                          FolioNo: "",
                          CurrentValue: "",
                          TransactionSource: "",
                          FrequencyMF: "",
                        })
                      }}
                    >
                      <Text
                        // @ts-ignore
                        // @ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={styles.button}

                      >
                        Reset
                      </Text>
                    </Button>&nbsp;
                    <Button
                      className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      disabled={loaderApi}
                      onClick={handleSubmit}
                    >
                      <Text
                        // @ts-ignore
                        // @ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={styles.button}
                      >
                        {loaderApi ? "Loading...." : `${buttonText}`}
                      </Text>
                    </Button>
                  </Box>
                </Box >
                <hr className="text-primary mx-0"></hr>
              </>
            )}
          </Formik >

          <Box className="row justify-content-center mb-3 ">
            <Box className="col-auto">
              {/* @ts-ignore */}
              <Text weight="extrabold" size="h5" css={{ color: "var(--colors-blue1)" }}>
                OR
              </Text>
            </Box>
          </Box >
          <Formik
            initialValues={verifyUploadFile}
            validationSchema={validateUploadFile}
            enableReinitialize
            onSubmit={async (values, { resetForm }) => {

              const check = await UploadCASFile(values);
              if (check) {
                resetForm();
                setVerifyUploadFile({
                  FilePwdMF: "",
                  FileTypeMF: "",
                })
              }
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (
              <>
                <Box className="row">
                  <Box className="col-lg-4 col-md-6 col-sm-12">
                    <Box>
                      <Input
                        type="file"
                        label="Select CAS File (Pdf)"
                        name="FileTypeMF"
                        className={`px-0 border rounded-4 ${styles.uploadbtn}`}
                        error={submitCount ? errors.FileTypeMF : null}
                        accept=".pdf"
                        onChange={async (
                          e: React.ChangeEvent<HTMLInputElement>
                        ) => {
                          try {
                            const file: any =
                              e?.target?.files?.[0] || null;
                            const base64 = await convertBase64(file);
                            setFieldValue(
                              "FileTypeMF",
                              base64
                            );
                          } catch (error: any) {
                            toastAlert("error", error);
                          }
                        }}
                      //value={values?.FileTypeMF}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-6 col-sm-12">
                    <Box>
                      <Input
                        type="password"
                        label="File Password"
                        name="FilePwdMF"
                        placeholder="Enter Password"
                        autoComplete="false"
                        error={submitCount ? errors.FilePwdMF : null}
                        onChange={handleChange}
                        value={values?.FilePwdMF}
                      />
                    </Box>
                  </Box>

                  <Box className="col-lg-4 col-md-6 col-sm-12 mt-4">
                    <Box>
                      <Button
                        className={`col-auto  mb-2 mx-0 py-1 px-4 ${styles.button}`}
                        color="yellowGroup"
                        size="md"
                        onClick={handleSubmit}
                        disabled={btnState}

                      >
                        <Text
                          // @ts-ignore
                          weight="normal"
                          size="h6"
                          //@ts-ignore
                          color="gray8"
                          className={styles.button}
                        >

                          {btnState ? <>Loading... </> : <> Proceed </>}
                        </Text>
                      </Button>
                    </Box>
                  </Box>
                </Box>
              </>
            )}
          </Formik>
          <Box className="row mt-2 ">
            {/* @ts-ignore */}
            <Box className="col-lg-12 col-md-12 col-sm-12 mb-3 ">
              <Text className="px-2 py-2 rounded"
                // @ts-ignore
                weight="normal"
                size="h6"
                css={{ color: "var(--colors-blue1)", border: "1px solid #4d7ea8 !important" }}
              >
                Note:
                <br />
                1.Please Upload CAS Since Inception(Example:- From 1900 To
                Till Date)
                <br />
                2.Delete Below Records Before CAS Uploading Otherwise Data
                Will Not Update
              </Text>
            </Box>
          </Box>
        </Box ></Box >
      <hr className="text-primary mx-0"></hr>
      <Box>
        <Text
          //@ts-ignore
          weight="normal"
          size="h4"
          css={{ color: "var(--colors-blue1)" }}
          className="text-center mx-3 my-3"
        >
          Mutual Funds
        </Text>
      </Box>
      <Box className="row ">
        <Box
          className="col-auto col-lg-12 col-md-12 col-sm-12"
          css={{ overflowX: "auto" }}
        >
          <Box className="table-responsive" css={{ borderRadius: "8px" }}>
            <Table
              className="text-capitalize table table-striped"
            // css={{ color: "var(--colors-blue1)" }}
            >
              <ColumnParent className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                <ColumnRow>
                  {/* <Column>
                    <Box className="row" css={{ width: "130px" }}>
                      <Box className="col-auto mt-0" css={{ fontWeight: "normal" }}>Select All </Box>
                      <Box className="col-auto mt-0">
                        <Input type="checkbox" />
                      </Box></Box>
                  </Column> */}
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Invest Type</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Scheme Name</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>SIP Amount</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Current Value</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Action</Text></Column>
                </ColumnRow>
              </ColumnParent>
              <DataParent>

                {mfDetailsList.length === 0 ? <>
                  <DataRow>
                    <DataCell colSpan={11} className="text-center">
                      No Mutual Funds Found
                    </DataCell>
                  </DataRow>
                </> : <>
               
                
                  {mfDetailsList.map((record: any, index: any) => {
                    return (
                      <DataRow>
                        {/* <DataCell><Box className="row">
                          <Box className="col-lg-9 mt-0"><Input type="checkbox" /></Box>
                        </Box>
                        </DataCell> */}
                        <DataCell className="text-center">{record?.MFInvestType === "S" ? "SIP" : "LumpSum"}</DataCell>
                        <DataCell className="text-center">{record?.PolicyName}</DataCell>
                        <DataCell className="text-center">{record?.sipAmount}</DataCell>
                        <DataCell className="text-center">{record?.CurrentValue}</DataCell>
                        <DataCell className="text-center">
                          <Box className="row text-center">
                            <Box className="col-auto text-center">
                              {/* @ts-ignore */}
                              <Text onClick={() => {
                                setVerifyMutualFund({
                                  InvestmentType: record?.MFInvestType,
                                  SchemeName: record?.PolicyName,
                                  FolioNo: record?.folioNo,
                                  SIPAmount: record?.sipAmount,
                                  CurrentValue: record?.CurrentValue,
                                  TransactionSource: record?.trxnSource,
                                  FrequencyMF: record?.FrequencyTypeId
                                })
                                setButtonText("Update");
                                setMfId({
                                  planId: record?.PlanId,
                                  basicId: record?.BasicId,
                                  id: record?.Id
                                });
                              }}
                                css={{ cursor: "pointer" }}>
                                <PencilFill />
                              </Text>
                            </Box>
                            <Box className="col-auto">
                              {/* @ts-ignore */}
                              <Button

                                onClick={() => {
                                 setOpenDelete(true);
                                  setDeleteId({
                                    planId: record?.PlanId,
                                    basicId: record?.BasicId,
                                    id: record?.Id
                                  });
                                }}
                                css={{ background: "#fff" }}>
                                <DeleteIcon /></Button>
                            </Box>
                          </Box>
                        </DataCell>
                      </DataRow>
                    );
                  })}
                </>}
              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>
      {/* <DataCell colSpan={6} className="text-center">
                      No Mutual Funds Found{" "}
                    </DataCell> */}
    </>
  );
};

export default MutualFund;
