import React, { useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input/Input";
import SelectMenu from "@ui/Select/Select";
import { GroupBox } from "@ui/Group/Group.styles";
import Radio from "@ui/Radio/Radio";
import { RadioGroup } from "@ui/Radio";
import { Button } from "@ui/Button/Button";
import styles from "../../FinancialPlan.module.scss";
import * as Yup from "yup";
import create from "zustand";
import { Formik, FieldArray } from "formik";
import { AGE_REGEX } from "App/utils/constants";
import useFinancialPlanStore from "../../store";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import UnMappedMember from "./UnMappedMember";
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";
import { Label } from "@radix-ui/react-label";
import {
  getBasicDetails,
  saveBasicDetails,
  deleteMembers
} from "App/api/financialPlan"
import Divider from "@ui/Divider/Divider";
import Checkbox from "@ui/Checkbox/Checkbox";
import useBasicDetailsStore from "./subStore";

const localData: any = getUser();

const ObjectSchemaList: any = {
  mandatory: {
    Full_Name: Yup.string()
      .required("Full Name is required")
      .trim()
      .matches(/^\s*[\S]/gms, "Invalid Full Name"),

    DateOfBirth: Yup.string().required("Date Of Birth  is required").trim(),

    companyName: Yup.string()
      .required("Company Name is required")
      .trim()
      .matches(/^\s*[\S]/gms, "Invalid Company Name"),

    maritalStatus: Yup.string().required("Marital Status is required").trim(),
    gender: Yup.string()
      .required("Gender is required")
      .trim(),
    designation: Yup.string()
      .required("Designation is required")
      .trim()
      .matches(/^\s*[\S]/gms, "Invalid Designation"),
  },
  spouseBasic: {
    Full_Name_Spouse: Yup.string()
      .required("Full Name is required")
      .trim()
      .matches(/^\s*[\S]/gms, "Invalid Full Name"),

    DateOfBirth_Spouse: Yup.string()
      // .required("Date Of Birth  is required")
      .trim(),
    Age_Spouse: Yup.string()
      .required("Age is required")
      .trim()
      .matches(AGE_REGEX, "Invalid Age"),
    SpWorkStatus: Yup.string(),
  },
  // spouseWork: {
  //   companyName_Spouse: Yup.string()
  //     .required("Company Name is required")
  //     .trim()
  //     .matches(/^\s*[\S]/gms, "Invalid Company Name"),

  //   designation_Spouse: Yup.string()
  //     // .required("Designation is required")
  //     .trim(),
  //   // .matches(/^\s*[\S]/gms, "Invalid Designation"),

  //   income_Spouse: Yup.string()
  //     // .required("income is required")
  //     .trim()
  //   // .matches(/\d/gms, "Invalid Designation"),
  // },
  childList: {
    children: Yup.array().of(
      Yup.object().shape({
        Full_Name_Child: Yup.string()
          .required("Full Name is required")
          .trim()
          .matches(/^\s*[\S]/gms, "Invalid Full Name"),

        DateOfBirth_Child: Yup.string()
          // .required("Date Of Birth  is required")
          .trim(),

        Age_Child: Yup.string()
          .required("Age is required")
          .trim(),
        // .matches(AGE_REGEX, "Invalid Age"),
        gender_child: Yup.string()
          .required("Gender is required")
          .trim(),
        InDepent: Yup.boolean(),
      })),
  },
  dependlist: {
    dependant: Yup.array().of(
      Yup.object().shape({
        Full_Name_Dep: Yup.string()
          .required("Full Name is required")
          .trim()
          .matches(/^\s*[\S]/gms, "Invalid Full Name"),

        DateOfBirth_Dep: Yup.string()
          // .required("Date Of Birth  is required")
          .trim(),

        Age_Dep: Yup.string()
          .required("Age is required")
          .trim(),
        // .matches(AGE_REGEX, "Invalid Age"),

        relation: Yup.string()
          .required("Relation is required")
          .trim(),
      })),
  },

};

export type childType = {
  Full_Name_Child: string,
  DateOfBirth_Child: string,
  Age_Child: string,
  gender_child: string,
  InDepent: boolean,
  DepentId: string,
  basicId: string,
  Caf_Status: string,
  userRelId: string,
  // index: number,
}

export type depentType = {
  Full_Name_Dep: string,
  DateOfBirth_Dep: string,
  Age_Dep: string,
  relation: string,
  DepentId: string,
  status: boolean,
  basicId: string,
  Caf_Status: string,
  userRelId: string,
  // index: number,
}


const BasicDetails = () => {
  // const [InitialvalidationSchema, setInitialValidationSchema] = useState(
  //   ObjectSchemaList.mandatory
  // );
  const [validationSchema, setValidationSchema] = useState(
    ObjectSchemaList.mandatory
  );

  const {
    loading,
    relationList,
    depRelationList,
    basicDetail,
    localUser,
    localPlanId,
    setBasicDetail,
    setLoader
  } = useFinancialPlanStore();

  const [openUnmapped, setOpenUnmapped] = useState(false);
  // validationSchema;
  const [dependSt, setdependSt] = useState<any>("N");
  const [maritalFilter, setMaritalFilter] = useState<any>("N");
  const [relationfilter, setRelationfilter] = useState();
  const [spouseWorkSt, setSpouseWorkSt] = useState<any>("N");
  const [childrenSt, setChildrenSt] = useState<any>("N");
  // const [childrenValues, setChildrenValues] = useState<any>([ ]);
  // const [dependantValues, setDependantValues] = useState<any>([ ]);
  let self_relation = "self";
  let self_relCode = "013";
  const [investorDetail, setInvestorDetails] = useState<any>();
  const [spouseDetail, setSpouseDetails] = useState<any>({});
  const [childDetails, setChildDetails] = useState<any>();
  const [dependDetails, setDependDetails] = useState<any>();

  const [fullDep, setFullDep] = useState<any>([]);
  const [openDelModal, setOpenDelModal] = useState<any>(false);
  // let spouseDOB: Date;
  // const [childIsDep, setChildIsDep] = useState<any>({ DepId: "", status: true });


  let childListval: childType = {
    Full_Name_Child: "",
    DateOfBirth_Child: "",
    Age_Child: "",
    gender_child: "",
    InDepent: false,
    DepentId: "",
    basicId: "",
    Caf_Status: "",
    userRelId: "",
    // index: 0,
  };

  let dependListVal: depentType = {
    Full_Name_Dep: "",
    DateOfBirth_Dep: "",
    Age_Dep: "",
    relation: "",
    DepentId: "",
    status: true,
    basicId: "",
    Caf_Status: "",
    userRelId: "",
    // index: 0,
  };

  const { verifyBasicD, setVerifyBasicD } = useBasicDetailsStore();
  const fetchBasicDetails = async () => {
    try {
      setLoader(true);
      if (localPlanId !== "0") {
        if (localPlanId?.basicid === undefined)
          return;
      }

      const obj: any = {
        basicId: localUser,
        PlanId: localPlanId?.basicid === undefined ? "0" : localPlanId?.basicid
      }
     
      const enc: any = encryptData(obj);
      let result: any = await getBasicDetails(enc);

     
      setBasicDetail(result?.data)
     
      setLoader(false);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  }

  const SaveBasicDetails = async (data: any) => {
    try {
      setLoader(true);
      let obj: any = [
        {
          basicId: localData?.basicid,
          PlanId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid,
          age: "0",// from input
          caf_Status: localData?.cafstatus,
          clientName: localData?.name === undefined ? data?.Full_Name : localData?.name,
          companyName: data?.companyName,// from input
          dependent_Id: "0",// defaultset
          designation: data?.designation,// from input
          dob: data?.DateOfBirth,
          email: localData?.userid,
          gender: data?.gender, // from input
          isChildExist: childrenSt,// from input
          isDependent: false,// from input
          isDependentExist: dependSt,// from input 
          isMarried: maritalFilter,// from input
          isSpouseWork: spouseWorkSt,// from 
          mobile: localData?.mobile,
          monthIncome: investorDetail?.MonthIncome,// from input or default 
          panNumber: investorDetail?.PanNumber,//"TEEPT9430N",//need to know
          planId: null,// alwaysNull or remove
          relation: self_relation,//defaultset
          relationCode: self_relCode,// defaultset
          userRelId: "0",//auto
          empCode: localData?.rmCode,
        },
      ]
      if (maritalFilter === "Y") {
        obj.push(
          {
            basicId: "",//default
            clientName: data?.Full_Name_Spouse,
            gender: data?.gender === "F" ? "M" : "F",
            dob: data?.DateOfBirth_Spouse,
            age: data?.Age_Spouse,
            mobile: "",//
            email: "",//
            panNumber: "",//
            companyName: data?.companyName_Spouse === undefined ? "" : data?.companyName_Spouse,
            designation: data?.designation_Spouse === undefined ? "" : data?.designation_Spouse,
            monthIncome: data?.income_Spouse === undefined ? "" : data?.income_Spouse,
            relation: data?.gender === "M" ? "wife" : "husband",
            relationCode: data?.gender === "M" ? "01" : "02",
            isMarried: "",//
            isSpouseWork: "",//
            isChildExist: "",//
            isDependentExist: "",
            isDependent: data?.SpWorkStatus === "Y" ? false : true,
            caf_Status: spouseDetail?.Caf_Status,
            dependent_Id: spouseDetail?.Dependent_Id === "undefined" ? "0" : spouseDetail?.Dependent_Id,
            planId: null,
            userRelId: spouseDetail?.userRelId,
            PlanId: localPlanId?.basicid === undefined ? "0" : localPlanId?.basicid,
            empCode: localData?.rmCode,
          }
        )

        if (childrenSt === "Y") {
          data?.children?.map((i: any) => {
            let userRef: any = childDetails.filter((record: any) => record?.Dependent_Id === i?.DepentId)
        
            obj.push(
              {
                basicId: i?.basicId,//default
                clientName: i?.Full_Name_Child,
                gender: i?.gender_child,
                dob: i?.DateOfBirth_Child === undefined ? "" : i?.DateOfBirth_Child, //need to change
                age: i?.Age_Child === undefined ? "" : i?.Age_Child,  //need to change
                mobile: "",//
                email: "",//
                panNumber: "",//  
                companyName: "",
                designation: "",
                monthIncome: "",
                relation: i?.gender_child === "M" ? "son" : "daughter",
                relationCode: i?.gender_child === "M" ? "07" : "08",
                isMarried: "",//
                isSpouseWork: "",//
                isChildExist: "",//
                isDependentExist: "",
                isDependent: i?.InDepent === undefined ? false : i?.InDepent,
                caf_Status: userRef.length === 0 ? i?.Caf_Status.length !== 0 ? i?.Caf_Status : "" : userRef[0]?.Caf_Status,
                dependent_Id: userRef.length === 0 ? "0" : userRef[0]?.Dependent_Id,  //need to change
                planId: null,
                userRelId: userRef.length === 0 ? i?.userRelId.length !== 0 ? i?.userRelId : "" : userRef[0]?.userRelId,
                PlanId: localPlanId?.basicid === undefined ? "0" : localPlanId?.basicid,
                empCode: localData?.rmCode,
              }
            )
          })
        }
      }
      if (dependSt === "Y") {

        let depObj: any = data?.dependant?.reduce((a: any, e: any) => {
          if (!["01", "02", "07", "08", "013"].includes(e.relation)) { a.push(e); } return a;
        }, []);
      

        depObj.map((i: any) => {
          let userRef: any = dependDetails.filter((record: any) => record?.Dependent_Id === i?.DepentId)

          // if (i?.relation !== "01" || i?.relation !== "02" || i?.relation !== "07" || i?.relation !== "08") {
          obj.push(
            {
              basicId: i?.basicId,//default
              clientName: i?.Full_Name_Dep,
              gender: "",//
              dob: i?.DateOfBirth_Dep === undefined ? "" : i?.DateOfBirth_Dep, //need to change
              age: i?.Age_Dep === undefined ? "" : i?.Age_Dep,  //need to change
              mobile: "",//
              email: "",//
              panNumber: "",//
              companyName: "",
              designation: "",
              monthIncome: "",
              relation: "",// pass valid by relationcode
              relationCode: i?.relation,
              isMarried: "",//
              isSpouseWork: "",//
              isChildExist: "",//
              isDependentExist: "",
              isDependent: true,
              caf_Status: userRef.length === 0 ? i?.Caf_Status.length !== 0 ? i?.Caf_Status : "" : userRef[0]?.Caf_Status,
              dependent_Id: userRef.length === 0 ? "0" : userRef[0]?.Dependent_Id,  //need to change
              planId: null,
              userRelId: userRef.length === 0 ? i?.userRelId.length !== 0 ? i?.userRelId : "" : userRef[0]?.userRelId,
              PlanId: localPlanId?.basicid === undefined ? "0" : localPlanId?.basicid,
              empCode: localData?.rmCode,
            }
          )
          // }

        })
      }
      
      const enc: any = encryptData(obj);
  
      let result = await saveBasicDetails(enc);
      setLoader(false);
      await fetchBasicDetails();
   
      toastAlert("success", "Basic details successfully added");

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  }

  const removeMemDetail = async (id: any) => {
    try {
      setLoader(true);
      let obj: any = {
        planId: localPlanId?.basicid,
        basicId: localUser,
        id: id,
      }
     
      const enc: any = encryptData(obj);
   
      await deleteMembers(enc);
      setLoader(false);
      toastAlert("success", "Member successfully Deleted");
      await fetchBasicDetails();
      setOpenDelModal(false)
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setOpenDelModal(false)
      setLoader(false);
    }
  }

  const calcAge = (date: any) => {
   
    let age: any;
    var today = new Date();
    var birthDate = new Date(date);
    age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  useEffect(() => {
    (async () => {
      await fetchBasicDetails();
    })();
    return () => {
    }
  }, [])

  useEffect(() => {
    setInvestorDetails(basicDetail.filter((record) => record.Relation === self_relation)[0]);
    setSpouseDetails(basicDetail.filter((record) => record.RelationCode === "01" || record.RelationCode === "02")[0] || {});
    setDependDetails(basicDetail.filter((record) => record.RelationCode !== self_relCode || record.Relation !== self_relation));
    setChildDetails(basicDetail.filter((record) => record.RelationCode === "07" || record.RelationCode === "08"));

    // for Spouse
    if ((basicDetail.filter((record) => record.Relation === self_relation && record.IsSpouseWork === "Y")[0]) === undefined) {
      setSpouseWorkSt("N");
    }
    else {
      setSpouseWorkSt("Y");
    }

    //for children
    if ((basicDetail.filter((record) => record.Relation === self_relation && record.IsChildExist === "Y")[0]) === undefined) {
      setChildrenSt("N");
    }
    else {
      setChildrenSt("Y");
    }


    // for Dependent
    if ((basicDetail.filter((record) => record.Relation === self_relation && record.IsDependentExist === "Y")[0]) === undefined) {
      setdependSt("N");
    }
    else {
      setdependSt("Y");
    }

    //for Marital status
    if ((basicDetail.filter((record) => record.Relation === self_relation && record.IsMarried === "Y")[0]) === undefined) {
      setMaritalFilter("N");
    }
    else {
      setMaritalFilter("Y");
    }

    setVerifyBasicD({
      Full_Name: investorDetail?.ClientName,
      DateOfBirth: investorDetail?.Dob,
      companyName: investorDetail?.CompanyName,
      maritalStatus: investorDetail?.IsMarried,
      designation: investorDetail?.Designation,
      gender: investorDetail?.Gender,

      Full_Name_Spouse: spouseDetail?.ClientName,
      DateOfBirth_Spouse: spouseDetail?.Dob,
      Age_Spouse: spouseDetail?.Age,
      SpWorkStatus: investorDetail?.IsSpouseWork,
      companyName_Spouse: spouseDetail?.CompanyName,
      designation_Spouse: spouseDetail?.Designation,
      income_Spouse: spouseDetail?.MonthIncome,


      children: childDetails?.length ?
        childDetails.map((i: any, n: number) => ({
          Full_Name_Child: i?.ClientName,
          DateOfBirth_Child: i?.Dob,
          Age_Child: i?.Age,
          gender_child: i?.Gender,
          InDepent: i?.IsDependent,
          DepentId: i?.Dependent_Id,
          basicId: i?.BasicId,
          Caf_Status: i?.caf_Status,
          userRelId: i?.userRelId,
          // index: n
        }))
        : [childListval],
      dependant: dependDetails?.length ?
        dependDetails.map((i: any, n: number) => ({
          Full_Name_Dep: i?.ClientName,
          DateOfBirth_Dep: i?.Dob,
          Age_Dep: i?.Age,
          relation: i?.RelationCode,
          DepentId: i?.Dependent_Id,
          status: i?.IsDependent,
          basicId: i?.BasicId,
          Caf_Status: i?.caf_Status,
          userRelId: i?.userRelId,
          // index: n
        }))
        : [dependListVal],
    },
    )

    return () => { };
  }, [localPlanId, basicDetail]);


  useEffect(() => {
    let obj = ObjectSchemaList.mandatory;
    if (maritalFilter === "Y") {
      obj = {
        ...obj,
        ...ObjectSchemaList.spouseBasic
      }
      // if (spouseWorkSt === "Y") {
      //   obj = {
      //     ...obj,
      //     // ...ObjectSchemaList.spouseWork
      //   }
      // }
      if (childrenSt === "Y") {
        obj = {
          ...obj,
          ...ObjectSchemaList.childList
        }
      }
    }
    if (dependSt === "Y") {
      obj = {
        ...obj,
        ...ObjectSchemaList.dependlist
      }
    }
    setValidationSchema(obj);
   
    return () => { }
  }, [maritalFilter, spouseWorkSt, childrenSt, dependSt])


  return (
    <>
      <Box className="row justify-content-center">
        <Box className="col-10">

          <Formik
            initialValues={verifyBasicD}
            validationSchema={Yup.object().shape(validationSchema)}
            enableReinitialize
            onSubmit={(values) => {
             
              SaveBasicDetails(values)
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount,
            }) => (
              <>
                <Box className="row">
                  <Box className="col-lg-4 col-md-5 col-sm-6">
                    <Box>
                      <Input
                        label="Full Name(As Per PAN)"
                        // disabled
                        name="Full_Name"
                        // placeholder="RajatTest"
                        className={`${styles.inputBorder}`}
                        error={submitCount ? errors.Full_Name : null}
                        onChange={handleChange}
                        value={values?.Full_Name}
                        disabled
                        required
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-6">
                    <Box>
                      <Input
                        label="Date Of Birth"
                        type="date"
                        required
                        name="DateOfBirth"
                        // placeholder="11-11-0011"
                        className={`${styles.inputBorder}`}
                        error={submitCount ? errors.DateOfBirth : null}
                        onChange={handleChange}
                        value={values?.DateOfBirth}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-6 pt-1 ">
                    <Box>
                      <Text
                        //@ts-ignore
                        weight="bold"
                        size="h6"
                        css={{ color: "#61b2ff" }}
                      >
                        Gender<span style={{ color: "#e5484d" }}>&nbsp;*</span>
                      </Text>
                    </Box>
                    <Box className="py-1">
                      <GroupBox>
                        <RadioGroup
                          defaultValue={values.gender}
                          className="inlineRadio"
                          name="gender"
                          required
                          value={values.gender}
                          onValueChange={(e: any) => {
                            setFieldValue("gender", e);
                          }}
                        >
                          <Radio value="M" label="Male" id="MaleOwn" />
                          <Radio value="F" label="Female" id="FemaleOwn" />
                          <Text css={{ color: "red", fontSize: "0.60rem" }}>{submitCount ? errors?.gender : null}</Text>
                        </RadioGroup>
                      </GroupBox>
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-6">
                    <Box>
                      <SelectMenu
                        name="maritalStatus"
                        items={[
                          { id: "Y", name: "Married" },
                          { id: "N", name: "UnMarried" },
                        ]}
                        label={"Marital Status"}
                        bindValue={"id"}
                        bindName={"name"}
                        placeholder="Select"
                        className={`pt-1`}
                        value={values.maritalStatus}
                        onChange={(e: any) => {
                          setMaritalFilter(e?.id || "");
                          setFieldValue("maritalStatus", e.id);
                        }}
                        required
                        error={submitCount ? errors?.maritalStatus : ""}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-6">
                    <Box>
                      <Input
                        label="Company Name"
                        name="companyName"
                        // placeholder="ABC"
                        className={`${styles.inputBorder}`}
                        error={submitCount ? errors.companyName : null}
                        onChange={handleChange}
                        value={values?.companyName}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-6">
                    <Box>
                      <Input
                        label="Designation"
                        name="designation"
                        // placeholder="Designation"
                        className={`${styles.inputBorder}`}
                        error={submitCount ? errors.designation : null}
                        onChange={handleChange}
                        value={values?.designation}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box className="row justify-content-between">
                  <Box className="col-auto py-2 ">
                    <Box>
                      <Box
                        onClick={() => setOpenUnmapped(true)}
                        css={{
                          color: "var(--colors-blue1)",
                          cursor: "pointer",
                        }}
                      >
                        {/* @ts-ignore */}
                        <Text size="h6">
                          <u> Un-Mapped Entries</u>
                          {/* &#43;    */}
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                  <Box className="col-auto ps-0 pe-5">
                    {loading ? <><Box className=" px-0"><Loader /></Box></> : <></>}
                  </Box>
                  <Box className="col-auto"></Box>
                </Box>
                <DialogModal
                  open={openUnmapped}
                  setOpen={setOpenUnmapped}
                  css={{
                    "@bp0": { width: "90%" },
                    "@bp1": { width: "60%" },
                    "z-index": 2
                  }}
                >
                  <UnMappedMember setOpen={setOpenUnmapped} values={values} setFieldValue={setFieldValue}/>
                </DialogModal>
                {maritalFilter === "Y" ? (
                  <>
                    <Box>
                      <hr className="text-primary mx-0" />
                      <Box className="row">
                        <Box className="col-lg-12 col-md-6 col-sm-6 pb-2">
                          <Text
                            //@ts-ignore
                            weight="bolder"
                            size="h5"
                            css={{ color: "var(--colors-blue1)" }}
                          >
                            Spouse Details
                          </Text>
                        </Box>
                        <Box className="col-lg-4 col-md-5 col-sm-6">
                          <Box>
                            <Input
                              label="Full Name(As Per PAN)"
                              name="Full_Name_Spouse"
                              // placeholder="RajatTest"
                              className={`${styles.inputBorder}`}
                              error={submitCount ? errors.Full_Name_Spouse : null}
                              onChange={handleChange}
                              value={values?.Full_Name_Spouse}
                              required
                            />
                          </Box>
                        </Box>
                        <Box className="col-lg-4 col-md-5 col-sm-6">
                          <Box className="row justify-content-between">
                            <Box className="col-8 col-md-6">
                              <Input
                                label="Date Of Birth"
                                type="date"
                                name="DateOfBirth_Spouse"
                                // placeholder="11-11-0011"
                                className={`${styles.inputBorder}`}
                                error={
                                  submitCount ? errors.DateOfBirth_Spouse : null
                                }
                                onChange={(e: any) => {
                                  setFieldValue("DateOfBirth_Spouse", e?.target.value)
                                  setFieldValue("Age_Spouse", calcAge(e?.target.value).toString() !== "NaN" ? calcAge(e?.target.value).toString() : (verifyBasicD?.Age_Spouse || ""));
                                }}
                                value={values?.DateOfBirth_Spouse}
                              />
                            </Box>
                            <Box className="col-8 col-md-1 col-lg-1 text-center">
                              {/* @ts-ignore */}
                              <Text
                                className={`py-3`}
                                //@ts-ignore
                                size="h6"
                                css={{ color: "var(--colors-blue1)" }}
                              >
                                {" "}
                                OR{" "}
                              </Text>
                            </Box>
                            <Box className="col-8 col-md-3">
                              <Input
                                label="Age"
                                name="Age_Spouse"
                                className={`${styles.inputBorder}`}
                                error={submitCount ? errors.Age_Spouse : null}
                                minChar={2}
                                onChange={(e: any) => {
                                  // setFieldValue("Age_Spouse", (e?.target.value).toString())
                                  setFieldValue("Age_Spouse", (e?.target?.value.length > 2 ? e?.target?.value.slice(0, 2) : e?.target?.value.replace(/\D/g, '')).toString())
                                  setFieldValue("DateOfBirth_Spouse", "")
                                }}
                                value={values?.Age_Spouse}
                                required
                              />
                            </Box>
                          </Box>
                        </Box>
                        <Box className="col-lg-4 col-md-5 col-sm-6 pt-1 ">
                          <Box>
                            <Text
                              //@ts-ignore
                              weight="bold"
                              size="h6"
                              css={{ color: "#61b2ff" }}
                            >
                              Is Your Spouse Working ?<span style={{ color: "#e5484d" }}>&nbsp;*</span>
                            </Text>
                          </Box>
                          <Box className="py-1">
                            <GroupBox>
                              <RadioGroup
                                defaultValue={values.SpWorkStatus}
                                className="inlineRadio"
                                name="SpWorkStatus"
                                value={values.SpWorkStatus}
                                onValueChange={(e: any) => {
                                  setSpouseWorkSt(e);
                                  setFieldValue("SpWorkStatus", e)
                                }}
                              >
                                <Radio value="Y" label="Yes" id="SpWorkYes" />
                                <Radio value="N" label="No" id="SpWorkNo" />
                              </RadioGroup>
                            </GroupBox>
                          </Box>
                        </Box>
                        {spouseWorkSt === "Y" ? (
                          <>
                            <Box className="col-lg-4 col-md-5 col-sm-6">
                              <Box>
                                <Input
                                  label="Company Name"
                                  name="companyName_Spouse"
                                  // placeholder="ABC"
                                  className={`${styles.inputBorder}`}
                                  // error={submitCount ? errors.companyName_Spouse : null}
                                  onChange={handleChange}
                                  value={values?.companyName_Spouse}
                                />
                              </Box>
                            </Box>
                            <Box className="col-lg-4 col-md-5 col-sm-6">
                              <Box>
                                <Input
                                  label="Designation"
                                  name="designation_Spouse"
                                  // placeholder="Designation"
                                  className={`${styles.inputBorder}`}
                                  // error={submitCount ? errors.designation_Spouse : null}
                                  onChange={handleChange}
                                  value={values?.designation_Spouse}
                                />
                              </Box>
                            </Box>
                            <Box className="col-lg-4 col-md-5 col-sm-6">
                              <Box>
                                <Input
                                  label="Income"
                                  name="income_Spouse"
                                  className={`${styles.inputBorder}`}
                                  // error={submitCount ? errors.income_Spouse : null}
                                  onChange={handleChange}
                                  value={values?.income_Spouse}
                                />
                              </Box>
                            </Box>
                          </>
                        ) : (
                          <></>
                        )}
                      </Box>
                    </Box>
                    <hr className="text-primary mx-0" />
                    <Box className="row">
                      <Box className="col-lg-12 col-md-6 col-sm-6 pb-2">
                        <Text
                          //@ts-ignore
                          weight="bolder"
                          size="h5"
                          css={{ color: "var(--colors-blue1)" }}
                        >
                          Children Details
                        </Text>
                      </Box>
                      <Box className="col-lg-12 col-md-6 col-sm-6 pb-2">
                        <Box className="row">
                          <Box className="col-lg-4 col-md-5 col-sm-6 pt-1 ">
                            <Box>
                              <Text
                                //@ts-ignore
                                weight="bold"
                                size="h6"
                                css={{ color: "#61b2ff" }}
                              >
                                Are you Blessed with Children ?<span style={{ color: "#e5484d" }}>&nbsp;*</span>
                              </Text>
                            </Box>
                            <Box className="py-1">
                              <GroupBox>
                                <RadioGroup
                                  defaultValue={childrenSt}
                                  className="inlineRadio"
                                  name="childrenStatus"
                                  value={childrenSt}
                                  onValueChange={(e: any) => {
                                    setChildrenSt(e);
                                  }}
                                >
                                  <Radio
                                    value="Y"
                                    label="Yes"
                                    id="childStYes"
                                  />
                                  <Radio value="N" label="No" id="childStNo" />
                                </RadioGroup>
                              </GroupBox>
                            </Box>
                          </Box>
                        </Box>
                      </Box>
                      {childrenSt === "Y" ? (
                        <>
                          <FieldArray name="children">
                            {({ insert, remove, push }) => (
                              <>
                                {values?.children?.length > 0 &&
                                  values?.children?.map((item: any, index: number) => (
                                  
                                    <React.Fragment key={index}>
                                      {index !== 0 && <Divider />}
                                      <Card className="row justify-content-center">
                                        <Box className="col-lg-3 col-md-5 col-sm-6">
                                          <Box>
                                            <Input
                                              label="Full Name"
                                              name={`children.${index}.Full_Name_Child`}
                                              className={`${styles.inputBorder}`}
                                              // placeholder="RajatTest"
                                              // @ts-ignore
                                              error={submitCount ? errors?.children?.[index]?.Full_Name_Child : null}
                                              onChange={handleChange}
                                              value={item?.Full_Name_Child}
                                              required
                                            />
                                          </Box>
                                        </Box>
                                        <Box className="col-lg-4 col-md-5 col-sm-6">
                                          <Box className="row justify-content-between">
                                            <Box className="col-8 col-md-6">
                                              <Input
                                                label="Date Of Birth"
                                                type="date"
                                                name={`children.${index}.DateOfBirth_Child`}
                                                className={`${styles.inputBorder}`}
                                                // placeholder="11-11-0011"
                                                // @ts-ignore
                                                error={submitCount ? errors?.children?.[index]?.DateOfBirth_Child : null}
                                                onChange={(e: any) => {
                                                  let age: any = calcAge(e?.target.value);
                                                  setFieldValue(`children.${index}.DateOfBirth_Child`, e?.target.value);
                                                  setFieldValue(`children.${index}.Age_Child`, age.toString() !== "NaN" ?
                                                    // age < 9 ? ("0" + age).toString() :
                                                    age.toString() :
                                                    (verifyBasicD?.children?.[index]?.Age_Child || ""));

                                                }}
                                                value={item?.DateOfBirth_Child}
                                              />
                                            </Box>
                                            <Box className="col-8 col-md-1 col-lg-1 text-center">
                                              {/* @ts-ignore */}
                                              <Text
                                                className={`py-md-3 py-1`}
                                                // @ts-ignore
                                                size="h6"
                                                css={{ color: "var(--colors-blue1)" }}
                                              >
                                                {" "}
                                                OR{" "}
                                              </Text>
                                            </Box>
                                            <Box className="col-8 col-md-3">
                                              <Input
                                                label="Age"
                                                name={`children.${index}.Age_Child`}
                                                className={`${styles.inputBorder}`}
                                                // @ts-ignore
                                                error={submitCount ? errors?.children?.[index]?.Age_Child : null}

                                                onChange={(e: any) => {
                                                 
                                                  setFieldValue(`children.${index}.Age_Child`, (e?.target?.value.length > 2 ? e?.target?.value.slice(0, 2) : e?.target?.value.replace(/\D/g, '')).toString())
                                                  setFieldValue(`children.${index}.DateOfBirth_Child`, "");
                                                }}
                                                value={item?.Age_Child}
                                              />
                                            </Box>
                                          </Box>
                                        </Box>

                                        <Box className="col-lg-3 col-md-5 col-12 pt-1 ">
                                          <Box>
                                            <Text
                                              //@ts-ignore
                                              weight="bold"
                                              size="h6"
                                              css={{ color: "#61b2ff" }}
                                            >
                                              Gender <span style={{ color: "#e5484d" }}>&nbsp;*</span>
                                            </Text>
                                          </Box>
                                          <Box className="py-1">
                                            <GroupBox>
                                              <RadioGroup
                                                name={`children.${index}.gender_child`}
                                                defaultValue={item?.gender_child}
                                                className="inlineRadio"
                                                value={item?.gender_child}
                                                onValueChange={(e: any) => {
                                                  setFieldValue(`children.${index}.gender_child`, e);
                                                }}
                                              >
                                                <Radio
                                                  value="M"
                                                  label="Male"
                                                  id={`Male${index}`}
                                                />
                                                <Radio
                                                  value="F"
                                                  label="Female"
                                                  id={`Female${index}`}
                                                />
                                                {/* @ts-ignore */}
                                                <Text css={{ color: "red", fontSize: "0.60rem" }}>{submitCount ? errors?.children?.[index]?.gender_child : null}</Text>
                                              </RadioGroup>
                                            </GroupBox>
                                          </Box>
                                        </Box>
                                        <Box className="col-lg-auto col-md-5 col-auto ">
                                          <Box>
                                            <Label style={{
                                              marginBottom: 5, fontSize: '0.7rem', color: '#339cff',
                                              fontWeight: 600, letterSpacing: '0.1rem'
                                            }}>
                                              Is Dependent
                                            </Label>
                                            <Box className="text-center">
                                              <Checkbox
                                                label=""
                                                id={`children.${index}.InDepent`}
                                                name={`children.${index}.InDepent`}
                                                checked={item?.InDepent}
                                                onChange={(e: any) => {
                                                  if (item?.Full_Name_Child !== "" &&
                                                    item?.Age_Child !== "" &&
                                                    item?.gender_child !== "") {
                                                    setFieldValue(`children.${index}.InDepent`, e?.target?.checked);
                                                    setFieldValue(`children.${index}.status`, !(e?.target?.checked));
                                                    let dindex = values?.dependant.reduce((a: any, e: any, i: any) => {
                                                      if (e?.DepentId === item?.DepentId) { a.push(i); } return a;
                                                    }, []);
                                                   
                                                    setFieldValue(`dependant.${dindex[0]}.status`, e?.target?.checked)
                                                  } else {
                                                    toastAlert("warn", "Please, Fill all details of child to add in dependent");
                                                  }
                                                }}
                                              />
                                            </Box>
                                          </Box>
                                        </Box>
                                        {/* {index ? ( */}
                                        <Box className="col-lg-12 col-md-5 col-auto ">
                                          <Box className="row justify-content-end ">
                                            <Box className="col-lg-auto col-auto mx-5">
                                              <Box className="pt-4 pt-md-0">
                                                <Button
                                                  className={`col-auto mb-2  mx-0 py-1 px-4 ${styles.button}`}
                                                  color="yellowGroup"
                                                  size="md"
                                                  onClick={() => {
                                                    if (item?.DepentId === "") {
                                                      remove(index)
                                                    }
                                                    else {
                                                      setOpenDelModal(true);
                                                      setFullDep(item);
                                                    }
                                                  }}
                                                  disabled={loading}
                                                >
                                                  <Box>
                                                    <Text
                                                      //@ts-ignore
                                                      weight="normal"
                                                      size="h6"
                                                      //@ts-ignore
                                                      color="gray8"
                                                      className={styles.button}
                                                    >
                                                      Delete
                                                    </Text>
                                                  </Box>
                                                </Button>
                                              </Box>
                                            </Box>
                                          </Box>
                                        </Box>
                                      </Card>
                                    </React.Fragment>
                                  ))}
                                <Box className="row justify-content-end">
                                  <Box className="col-lg-3 col-md-5 col-sm-6">
                                    <Box className="pt-3">
                                      <Button
                                        className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                                        color="yellowGroup"
                                        size="md"
                                        onClick={() => push(childListval)}
                                        disabled={loading}
                                      >
                                        <Box>
                                          <Text
                                            //@ts-ignore
                                            weight="normal"
                                            size="h6"
                                            //@ts-ignore
                                            color="gray8"
                                            className={styles.button}
                                          >
                                            Add New Children
                                          </Text>
                                        </Box>
                                      </Button>
                                    </Box>
                                  </Box>
                                </Box>
                              </>
                            )}
                          </FieldArray>
                        </>
                      ) : (
                        <></>
                      )}
                    </Box>
                  </>
                ) : (
                  <></>
                )}
                <hr className="text-primary mx-0" />
                <Box className="row">
                  <Box className="col-lg-12 col-md-6 col-sm-6 pb-2">
                    <Text
                      //@ts-ignore
                      weight="bolder"
                      size="h5"
                      css={{ color: "var(--colors-blue1)" }}
                    >
                      Financial Dependants Details
                    </Text>
                  </Box>
                </Box>
                <Box className="row">
                  <Box className="col-lg-12 col-md-6 col-sm-6">
                    {/* @ts-ignore */}
                    <Text weight="normal" size="h6"
                      css={{ color: "var(--colors-blue1)" }}
                    >
                      Do You Have Any Dependant?
                    </Text>
                    <RadioGroup
                      defaultValue={dependSt}
                      className="inlineRadio"
                      name="dependantType"
                      value={dependSt}
                      onValueChange={(e: any) => {
                        setdependSt(e);
                      }}
                    >
                      <Radio label="Yes" id="depentYes" value="Y" />
                      <Radio label="No" id="depentNo" value="N" />
                    </RadioGroup>
                  </Box>
                </Box>
                {spouseWorkSt === "N" && maritalFilter === "Y" && (Object.keys(spouseDetail).length === 0) ?
                  <>
                    <Box className="row">
                      <Box className="col-lg-12 col-md-6 col-sm-6">
                        <Card className="row mx-0 justify-content-center">
                          <Box className="col-lg-3 col-md-5 col-sm-6">
                            <Box>
                              <Input
                                label="Full Name (As Per PAN)"
                                name="Full_Name_Spouse"
                                disabled
                                className={`${styles.inputBorder}`}
                                value={values?.Full_Name_Spouse}

                              />
                            </Box>
                          </Box>
                          <Box className="col-lg-4 col-md-5 col-12">
                            <Box className="row justify-content-between">
                              <Box className="col-12 col-md-6">
                                <Input
                                  label="Date Of Birth"
                                  type="date"

                                  className={`${styles.inputBorder}`}
                                  disabled
                                  value={values?.DateOfBirth_Spouse}
                                />
                              </Box>
                              <Box className="col-auto col-md-1">

                                <Text size="h6"
                                  className={`pt-4 py-md-3 `}
                                  css={{ color: "var(--colors-blue1)" }}
                                >
                                  {" "}
                                  OR{" "}
                                </Text>
                              </Box>
                              <Box className="col-auto col-md-3">
                                <Input
                                  label="Age"
                                  className={`${styles.inputBorder}`}
                                  disabled
                                  value={values?.Age_Spouse}
                                />
                              </Box>
                            </Box>
                          </Box>
                          <Box className="col-lg-3 col-md-5 col-sm-6 mt-1">
                            <Box>
                              <SelectMenu
                                items={relationList}
                                label={"Relation"}
                                bindValue={"code"}
                                bindName={"Type_name"}
                                value={values?.gender === "M" ? "wife" : "husband"}//relationCode
                                disabled
                                className={`text-capitalize`}

                              // @ts-ignore
                              />
                            </Box>
                          </Box>

                          <Box className="col-lg-2 col-md-5 col-auto mt-1">
                            <Box className="pt-4">
                            </Box>
                          </Box>
                        </Card>
                      </Box>
                    </Box>
                  </>
                  : <></>}
                {dependSt === "Y" ? <>
                  <FieldArray name="dependant">
                    {({ insert, remove, push }) => (

                      <>
                        {values?.dependant?.length > 0 &&
                          values?.dependant?.map((item: any, index: number) => (<>
                            {item?.relation === "01" || item?.relation === "02" || item?.relation === "07" || item?.relation === "08" ? <>
                              {spouseWorkSt === "Y" && (item?.relation === "01" || item?.relation === "02") ? <></> : <>
                                {((item?.relation === "01" || item?.relation === "02") && maritalFilter === "N") || !item?.status ? <></> : <>
                                  <React.Fragment key={index}>
                                    {index !== 0 && <Divider />}
                                    <Card className="row mx-0 justify-content-center">
                                      <Box className="col-lg-3 col-md-5 col-sm-6">
                                        <Box>
                                          <Input
                                            label="Full Name (As Per PAN)"
                                            name={`dependant.${index}.Full_Name_Dep`}
                                            disabled
                                            className={`${styles.inputBorder}`}
                                            value={item?.Full_Name_Dep}
                                          />
                                        </Box>
                                      </Box>
                                      <Box className="col-lg-4 col-md-5 col-12">
                                        <Box className="row justify-content-between">
                                          <Box className="col-12 col-md-6">
                                            <Input
                                              label="Date Of Birth"
                                              type="date"
                                              name={`dependant.${index}.DateOfBirth_Dep`}
                                              // placeholder="11-11-0011"
                                              className={`${styles.inputBorder}`}
                                              disabled
                                              value={item?.DateOfBirth_Dep}
                                            />
                                          </Box>
                                          <Box className="col-auto col-md-1">
                                            {/* @ts-ignore */}
                                            <Text size="h6"
                                              className={`pt-4 py-md-3 `}
                                              css={{ color: "var(--colors-blue1)" }}
                                            >
                                              {" "}
                                              OR{" "}
                                            </Text>
                                          </Box>
                                          <Box className="col-auto col-md-3">
                                            <Input
                                              label="Age"
                                              name={`dependant.${index}.Age_Dep`}
                                              className={`${styles.inputBorder}`}
                                              disabled
                                              value={item?.Age_Dep}
                                            />
                                          </Box>
                                        </Box>
                                      </Box>
                                      <Box className="col-lg-3 col-md-5 col-sm-6 mt-1">
                                        <Box>
                                          <SelectMenu
                                            name={`dependant.${index}.relation`}//relationCode
                                            items={relationList}
                                            label={"Relation"}
                                            bindValue={"code"}
                                            bindName={"Type_name"}
                                            value={item?.relation}//relationCode
                                            disabled
                                            className={`text-capitalize`}
                                          />
                                        </Box>
                                      </Box>

                                      <Box className="col-lg-2 col-md-5 col-auto mt-1">
                                        {item?.relation === "01" || item?.relation === "02" || item?.relation === "07" || item?.relation === "08" ? <>
                                        </> : <>
                                          <Box className="pt-4">
                                            <Button
                                              className={`col-auto mb-2  mx-0 py-1 px-4 ${styles.button}`}
                                              color="yellowGroup"
                                              size="md"
                                              onClick={() => {
                                                if (item?.DepentId === "") {
                                                  remove(index)
                                                }
                                                else {
                                                  setOpenDelModal(true);
                                                  setFullDep(item);
                                                  //removeMemDetail(item?.DepentId)
                                                }
                                              }}
                                              disabled={loading}
                                            >
                                              <Box>
                                                <Text
                                                  //@ts-ignore
                                                  weight="normal"
                                                  size="h6"
                                                  //@ts-ignore
                                                  color="gray8"
                                                  className={styles.button}
                                                >
                                                  Delete
                                                </Text>
                                              </Box>
                                            </Button>
                                          </Box>
                                        </>}
                                      </Box>

                                    </Card>
                                  </React.Fragment>
                                </>}
                              </>}

                            </> : <>
                              <React.Fragment key={index}>
                                {index !== 0 && <Divider />}
                                <Card className="row mx-0 justify-content-center">
                                  <Box className="col-lg-3 col-md-5 col-sm-6">
                                    <Box>
                                      <Input
                                        label="Full Name (As Per PAN)"
                                        name={`dependant.${index}.Full_Name_Dep`}
                                        // placeholder="Aryan Ashish Kamath"
                                        className={`${styles.inputBorder}`}
                                        // @ts-ignore
                                        error={submitCount ? errors?.dependant?.[index]?.Full_Name_Dep : null}
                                        onChange={handleChange}
                                        value={item?.Full_Name_Dep}
                                      />
                                    </Box>
                                  </Box>
                                  <Box className="col-lg-4 col-md-5 col-12">
                                    <Box className="row justify-content-between">
                                      <Box className="col-12 col-md-6">
                                        <Input
                                          label="Date Of Birth"
                                          type="date"
                                          name={`dependant.${index}.DateOfBirth_Dep`}
                                          // placeholder="11-11-0011"
                                          className={`${styles.inputBorder}`}
                                          // @ts-ignore
                                          error={submitCount ? errors?.dependant?.[index]?.DateOfBirth_Dep : null}
                                          onChange={(e: any) => {
                                            let age: any = calcAge(e?.target.value);
                                            setFieldValue(`dependant.${index}.DateOfBirth_Dep`, e?.target.value)
                                            setFieldValue(`dependant.${index}.Age_Dep`, age.toString() !== "NaN" ?
                                              // age < 9 ? ("0" + age).toString() :
                                              age.toString() :
                                              (verifyBasicD?.dependant?.[index]?.Age_Dep || ""));
                                          }}
                                          value={item?.DateOfBirth_Dep}
                                        />
                                      </Box>
                                      <Box className="col-auto col-md-1">
                                        {/* @ts-ignore */}
                                        <Text size="h6"
                                          className={`pt-4 py-md-3 `}
                                          css={{ color: "var(--colors-blue1)" }}
                                        >
                                          {" "}
                                          OR{" "}
                                        </Text>
                                      </Box>
                                      <Box className="col-auto col-md-3">
                                        <Input
                                          label="Age"
                                          name={`dependant.${index}.Age_Dep`}
                                          className={`${styles.inputBorder}`}
                                          // @ts-ignore
                                          error={submitCount ? errors?.dependant?.[index]?.Age_Dep : null}
                                          onChange={(e: any) => {
                                            setFieldValue(`dependant.${index}.Age_Dep`, (e?.target?.value.length > 3 ? e?.target?.value.slice(0, 3) : e?.target?.value.replace(/\D/g, '')).toString())
                                            setFieldValue(`dependant.${index}.DateOfBirth_Dep`, "");
                                          }}
                                          value={item?.Age_Dep}
                                        />
                                      </Box>
                                    </Box>
                                  </Box>
                                  <Box className="col-lg-3 col-md-5 col-sm-6 mt-1">
                                    <Box>
                                      <SelectMenu
                                        name={`dependant.${index}.relation`}//relationcode
                                        items={depRelationList}
                                        label={"Relation"}
                                        bindValue={"code"}
                                        bindName={"Type_name"}
                                        value={item?.relation}//relationcode
                                        onChange={(e: any) => {
                                          setRelationfilter(e?.ID || "");
                                         
                                          setFieldValue(`dependant.${index}.relation`, e?.code);
                                        }}
                                        className={`text-capitalize`}
                                        // @ts-ignore
                                        error={submitCount ? errors?.dependant?.[index]?.relation : null}
                                      />
                                    </Box>
                                  </Box>

                                  <Box className="col-lg-2 col-md-5 col-auto mt-1">
                                    {item?.relation === "01" || item?.relation === "02" || item?.relation === "07" || item?.relation === "08" ? <>
                                    </> : <>
                                      <Box className="pt-4">
                                        <Button
                                          className={`col-auto mb-2  mx-0 py-1 px-4 ${styles.button}`}
                                          color="yellowGroup"
                                          size="md"
                                          onClick={() => {
                                            if (item?.DepentId === "") {
                                              remove(index)
                                            }
                                            else {
                                              setOpenDelModal(true);
                                              setFullDep(item);
                                              //removeMemDetail(item?.DepentId)
                                            }
                                          }}
                                          disabled={loading}
                                        >
                                          <Box>
                                            <Text
                                              //@ts-ignore
                                              weight="normal"
                                              size="h6"
                                              //@ts-ignore
                                              color="gray8"
                                              className={styles.button}
                                            >
                                              Delete
                                            </Text>
                                          </Box>
                                        </Button>
                                      </Box>
                                    </>}
                                  </Box>

                                </Card>
                              </React.Fragment>
                            </>}

                          </>))}
                        <FieldArray name="children">
                          {({ insert, remove, push }) => (
                            <>
                              {values?.children?.length > 0 &&
                                values?.children?.map((item: any, index: number) => (

                                  <React.Fragment key={index}>
                                    {item?.DepentId === "" && item?.InDepent ? <>
                                      {index !== 0 && <Divider />}
                                      <Card className="row justify-content-center">
                                        <Box className="col-lg-3 col-md-5 col-sm-6">
                                          <Box>
                                            <Input
                                              label="Full Name (As Per PAN)"
                                              name={`children.${index}.Full_Name_Child`}
                                              className={`${styles.inputBorder}`}
                                              disabled
                                              value={item?.Full_Name_Child}
                                            />
                                          </Box>
                                        </Box>
                                        <Box className="col-lg-4 col-md-5 col-12">
                                          <Box className="row justify-content-between">
                                            <Box className="col-12 col-md-6">
                                              <Input
                                                label="Date Of Birth"
                                                type="date"
                                                name={`children.${index}.DateOfBirth_Child`}
                                                className={`${styles.inputBorder}`}
                                                disabled
                                                value={item?.DateOfBirth_Child}
                                              />
                                            </Box>
                                            <Box className="col-auto col-md-1">
                                              {/* @ts-ignore */}
                                              <Text
                                                className={`pt-4 py-md-3`}
                                                // @ts-ignore
                                                size="h6"
                                                css={{ color: "var(--colors-blue1)" }}
                                              >
                                                {" "}
                                                OR{" "}
                                              </Text>
                                            </Box>
                                            <Box className="col-auto col-md-3">
                                              <Input
                                                label="Age"
                                                name={`children.${index}.Age_Child`}
                                                className={`${styles.inputBorder}`}
                                                disabled

                                                value={item?.Age_Child}
                                              />
                                            </Box>
                                          </Box>
                                        </Box>

                                        <Box className="col-lg-3 col-md-5 col-sm-6 mt-1 ">
                                          <Box>
                                            <SelectMenu
                                              name={`children.${index}.gender_child`}//relationcode
                                              items={[{ id: "M", name: "Son" }, { id: "F", name: "Daughter" }]}
                                              label={"Relation"}
                                              bindValue={"id"}
                                              bindName={"name"}
                                              value={item?.gender_child}//relationcode
                                              disabled
                                              className={`text-capitalize`}
                                            // @ts-ignore

                                            />

                                          </Box>

                                        </Box>
                                        <Box className="col-lg-2 col-md-5 col-auto mt-1 ">
                                        </Box>
                                      </Card>
                                    </>
                                      : <></>}
                                  </React.Fragment>

                                ))}
                              <Box className="row justify-content-end">
                                <Box className="col-lg-3 col-md-5 col-sm-6">
                                  <Box className="pt-3">

                                  </Box>
                                </Box>
                              </Box>
                            </>
                          )}
                        </FieldArray>
                        <Box className="row justify-content-end">
                          <Box className="col-lg-3 col-md-5 col-sm-6">
                            <Box className="pt-3">
                              <Button
                                className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                                color="yellowGroup"
                                size="md"
                                onClick={() => push(dependListVal)}
                                disabled={loading}
                              >
                                <Box>
                                  <Text
                                    //@ts-ignore
                                    weight="normal"
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                    className={styles.button}
                                  >
                                    Add New Dependant
                                  </Text>
                                </Box>
                              </Button>
                            </Box>
                          </Box>
                        </Box>
                      </>
                    )}
                  </FieldArray>
                </> : <></>}

                <Box className="row my-2">
                  <Box className="col-auto">
                    <Button
                      className={`col-auto mb-2 mx-0 py-1 px-4  ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      onClick={handleSubmit}
                      disabled={loading}
                    >
                      <Box>
                        <Text
                          //@ts-ignore
                          weight="normal"
                          size="h6"
                          //@ts-ignore
                          color="gray8"
                          className={styles.button}
                          onClick={handleSubmit}

                        >
                          {loading ? "Loading..." : "Save and Next"}
                        </Text>
                      </Box>
                    </Button>
                    &nbsp;
                  </Box>
                </Box>
                <DialogModal
                  open={openDelModal}
                  setOpen={setOpenDelModal}
                  css={{
                    "@bp0": { width: "90%" },
                    "@bp1": { width: "50%" },
                  }}
                >
                  <Box className="col-md-12 col-sm-12 col-lg-12">
                    <Box className="modal-header p-3">
                      <Box className="row">
                        <Box className="col-auto text-light">Member Delete Warning</Box>
                        <Box className="col"></Box>
                        <Box className="col-auto"></Box>
                      </Box>
                    </Box>
                    <Box
                      className="text-center modal-body modal-body-scroll"
                      css={{ padding: "0.5rem" }}
                    >

                      <Text css={{ mt: 20, pt: 10 }}>
                        {/* @ts-ignore */}
                        {/* Name: {fullDep?.C} <br /> */}
                        Are you sure to delete this Member record?

                      </Text>
                      <Box css={{ mt: 20, pt: 10 }} className="text-end">
                        <Button
                          type="submit"
                          color="yellow"
                          onClick={() => { setOpenDelModal(false) }}>
                          Cancel
                        </Button>
                        <Button
                          type="submit"
                          color="yellow"
                          //@ts-ignore
                          onClick={() => { removeMemDetail(fullDep?.DepentId) }}
                          disabled={loading}
                        >
                          Delete Now
                        </Button>
                      </Box>
                    </Box>
                  </Box>

                </DialogModal>
              </>
            )}
          </Formik>
        </Box>
      </Box>
    </>
  );
};

export default BasicDetails;
