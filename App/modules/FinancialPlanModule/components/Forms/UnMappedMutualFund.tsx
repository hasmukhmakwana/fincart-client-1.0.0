import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { Button } from "@ui/Button/Button";
import SelectMenu from "@ui/Select/Select";
import React, { useState, useEffect } from "react";
import { convertBase64, encryptData, getUser, plainBase64 } from "App/utils/helpers";
import { UnmappedMFSaving } from "App/api/financialPlan";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import { colors } from "@react-spring/shared";
import { toastAlert } from "App/shared/components/Layout";
import Checkbox from "@ui/Checkbox/Checkbox";

const UnMappedMeber = ({ setOpenUnmappedMF, fetchOtherAssetDetails, fetchMutualFundUnmapped }: any) => {
  const {
    localPlanId,
    unMappedMF,
    localUser,
    relationList
  } = useFinancialPlanStore();
  const local: any = getUser();
  const [checkedList, setCheckedList] = useState<any>([]);
  const [loader, setLoader] = useState<boolean>(false);

  useEffect(() => {
    let arr: any = [];
    unMappedMF.map((ele, index) => {
      let obj = {
        SchemeName: ele?.SchemeName,
        foliono: ele?.foliono,
        schemeid: ele?.schemeid,
        currWorth: ele?.currWorth,
        trxnSource: ele?.trxnSource,
        TrxnType: ele?.TrxnType,
        sipAmount: ele?.sipAmount,
        IsAddedInFP: ele?.IsAddedInFP,
        id: ele?.id == null ? 0 : ele?.id,
        checkIsAddedInFP: "1",
        assetName: ele?.SchemeName,
        currentAmount: "",
        basicId: local?.basicid,
        planId: localPlanId?.basicid === undefined ? "0" : localPlanId?.basicid,
        rmCode: local?.rmCode,
        subBroker: "",
        i: index
      }
      arr.push(obj)
    })
    setCheckedList(arr);
  }, [unMappedMF])

  const saveUnMappedEntries = async () => {
    let arr = checkedList.filter((el: any) => el.IsAddedInFP === 1)
    if (arr.length >= 1) {
      setLoader(true);
      let resultArr: any = [];
      arr.map((ele: any) => {
        let obj = {
          SchemeName: ele?.SchemeName,
          foliono: ele?.foliono,
          schemeid: ele?.schemeid,
          currWorth: ele?.currWorth,
          trxnSource: ele?.trxnSource,
          TrxnType: ele?.TrxnType,
          sipAmount: ele?.sipAmount,
          IsAddedInFP: ele?.IsAddedInFP,
          id: ele?.id == null ? 0 : ele?.id,
          checkIsAddedInFP: "1",
          assetName: ele?.SchemeName,
          currentAmount: ele?.currWorth,
          basicId: local?.basicid,
          planId: localPlanId?.basicid === undefined ? "0" : localPlanId?.basicid,
          rmCode: local?.rmCode,
          subBroker: "",
        }
        resultArr.push(obj);
      })

      try {

        const enc: any = encryptData(resultArr);
        const result: any = await UnmappedMFSaving(enc);
        if (result.status === "Success") {

          toastAlert("success", "Mutual Fund Successfully Added");
          setLoader(false);
          setOpenUnmappedMF(false);
          fetchOtherAssetDetails();
          fetchMutualFundUnmapped();

        }
      }
      catch (error: any) {
        console.log(error);
        setLoader(false);
        toastAlert("error", error);
      }
    }
    else {
      return
    }
  }

  const addInCheckedList = (ele: any, isAdd: any) => {

    const newState = checkedList.map((obj: any) => {
      if (obj.i === ele?.i) {
        return { ...obj, IsAddedInFP: isAdd ? 1 : 0 };
      }

      return obj;
    });

    setCheckedList(newState);

  }

  const addInCheckedListALL = (isAdd: any) => {

    const newState = checkedList.map((obj: any) => {
      return { ...obj, IsAddedInFP: isAdd ? 1 : 0 };

    });

    setCheckedList(newState);

  }

  const SipAmountAdding = (ele: any, val: any) => {
    const newState = checkedList.map((obj: any) => {
      if (obj.i === ele?.i) {
        return { ...obj, sipAmount: val };
      }

      return obj;
    });

    setCheckedList(newState);
  }


  return (
    //@ts-ignore
    <>
      <Box>
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}>Please select your committed savings/SIP OR Lumpsum(Assets)</Text>
        </Box>
        <Box className={`modal-body p-3`} >

          <Box className={`row ${styles.scrollit}`}>
            <Box css={{ overflow: "auto" }}
              className="col-auto col-lg-12 col-md-12 col-sm-12" >
              {/* @ts-ignore */}
              <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Note: Below schemes are auto inserted by CAS or transactions done on our platform.</Text>
              <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                <Table className="text-capitalize table table-striped" >
                  <ColumnParent className="text-capitalize ">
                    <ColumnRow className=" sticky" css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}  >
                      <Column><Text size="h5">Folio no</Text></Column>
                      <Column><Text size="h5">Scheme name</Text></Column>
                      <Column><Text size="h5">Type</Text></Column>
                      <Column><Text size="h5">Source</Text></Column>
                      <Column><Text size="h5">Current worth</Text></Column>
                      <Column><Text size="h5">SIP Amount</Text></Column>
                      <Column><Text size="h5">Select  <Checkbox
                        label=""
                        name="all_add"
                        id="all"
                        onChange={(e: any) => { addInCheckedListALL(e.target.checked) }}
                      /></Text></Column>
                    </ColumnRow>
                  </ColumnParent>

                  <DataParent css={{ textAlign: "center" }} >
                    {checkedList.map((record: any, index: any) => {
                      return (
                        <DataRow>
                          <DataCell>{record?.foliono} </DataCell>
                          <DataCell>{record?.SchemeName} </DataCell>
                          <DataCell>{record?.TrxnType === 'S' ? 'SIP' : 'LUMPSUM'}</DataCell>
                          <DataCell>
                            {record?.trxnSource}
                          </DataCell>
                          <DataCell> {Number(record?.currWorth).toLocaleString("en-In")}
                          </DataCell>
                          <DataCell>
                            <Input type="text" css={{ border: "solid 1px #c6c6c6" }} value={record?.sipAmount} onChange={(e: any) => { SipAmountAdding(record, e.target.value) }} />
                          </DataCell>
                          <DataCell>
                            <Checkbox
                              label=""
                              name="fp_added"
                              id={record?.schemeid}
                              checked={record?.IsAddedInFP}
                              onChange={(e: any) => { addInCheckedList(record, e.target.checked) }}
                            />

                          </DataCell>
                        </DataRow>
                      );
                    })}
                  </DataParent>
                </Table>
              </Box>
            </Box>
          </Box>
          <Box className="row justify-content-end">
            {unMappedMF.length !== 0 ?
              <Box className="col-auto">
                <Button
                  className={`col-auto  py-1 px-4 mx-0 ${styles.button}`}
                  color="yellowGroup"
                  size="md"
                  disabled={loader}
                  onClick={() => { saveUnMappedEntries() }}
                >
                  <Text
                    //@ts-ignore
                    weight="normal"
                    size="h6"
                    //@ts-ignore
                    color="gray8"
                    className={styles.button}
                  >
                    {loader ? `Loading...` : `Save`}
                  </Text>
                </Button>&nbsp;
              </Box> : null}
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default UnMappedMeber