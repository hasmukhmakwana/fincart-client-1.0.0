import React, { useEffect, useState } from "react";
import TextInput from "react-autocomplete-input";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import Input from "@ui/Input/Input";
import SelectMenu from "@ui/Select/Select";
// import styles from "../../../Tools.module.scss";
import 'react-autocomplete-input/dist/bundle.css';
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { StyledTabs, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsTrigger } from '../NewTabsTrigger.styles'
import { TabsContent } from "@radix-ui/react-tabs";
import { Button } from "@ui/Button/Button";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import UnMappedInsurance from "./UnMappedInsurance";
import UnMappedMutualFund from "./UnMappedMutualFund";
import UnmappedOtherAsset from "./UnmappedOtherAsset";
import UnmappedLoan from "./UnmappedLoan";
import DeleteIcon from "App/icons/DeleteIcon";
import PencilFill from "App/icons/PencilFill";
import { fetchAssetsDetails, createInsurance, insurancePolicySearch, mutualFundSchemeSearchFP, getInsuranceUnmapped, getInsuranceMutualFund, getOtherAssetUnmapped, getLoanUnmapped, uploadFilePDF, assetNameSearch, deleteMutualFund, createOtherAsset, deleteOtherAsset, otherAssetSearch } from "App/api/financialPlan";
import { toastAlert } from "App/shared/components/Layout";
import { convertBase64, encryptData, getUser, plainBase64 } from "App/utils/helpers";
import { Label } from "@radix-ui/react-label";
import { clear } from "console";
import Autocomplete from "react-autocomplete";



const validationInsuranceSchema = {
  RequiredIN: {
    insuranceType: Yup.string().required("Insurance Type is required").trim(),
    PolicyProvider: Yup.string().required("Policy Provider is required").trim(),
    TransactionSource: Yup.string().required("Transaction Source is required").trim(),
    policyName: Yup.string().required("Policy Name  is required").trim(),
    PolicyNumber: Yup.string().trim(), //required("Policy Number is required")
    IssuedDate: Yup.string().trim(),  //.required("Issued Date is required")
    PremiumAmount: Yup.string().required("Premium Amount is required").trim(),
    PolicyTerm: Yup.string().trim(),   //.required("Policy Term is required")
    PremiumPayingTerm: Yup.string().required("Premium Paying Term is required").trim(),
    CoverSumAssured: Yup.string().trim(),   //.required("Cover/Sum Assured is required")
    PolicyOwner: Yup.string().required("Policy Owner is required").trim(),
    Frequency: Yup.string().required("Frequency is required").trim(),
    PolicyBenificiar: Yup.array(),    //.required("Policy Benificiar is required")
  },
  Available: {
    AvailableValue: Yup.string().required("Available Value is required").trim(),
  }
}



export type VerifyInsuranceTypes = {
  insuranceType: string;
  PolicyProvider: string;
  AvailableValue: string;
  TransactionSource: string;
  policyName: string;
  PolicyNumber: string;
  IssuedDate: string;
  PremiumAmount: string;
  PolicyTerm: string;
  PremiumPayingTerm: string;
  CoverSumAssured: string;
  PolicyOwner: string;
  Frequency: string;
  PolicyBenificiar: [];
}


interface StoreTypes {

  verifyInsurance: VerifyInsuranceTypes;

}

const useRegistrationStore = create<StoreTypes>((set) => ({
  verifyInsurance: {
    insuranceType: "",
    PolicyProvider: "",
    AvailableValue: "",
    TransactionSource: "",
    policyName: "",
    PolicyNumber: "",
    IssuedDate: "",
    PremiumAmount: "",
    PolicyTerm: "",
    PremiumPayingTerm: "",
    CoverSumAssured: "",
    PolicyOwner: "",
    Frequency: "",
    PolicyBenificiar: [],
  },
  setVerifyInsurance: (payload: any) =>
    set((state) => ({
      ...state,
      verifyInsurance: payload,
    })),


}));


const Insurance = ({ inDetailsList, fetchOtherAssetDetails, setOpenDelete, setDeleteId, setOpenUnmappedIN }: any) => {
  const { basicDetail } = useFinancialPlanStore();
  const [insurancePolType, setInsurancePolType] = useState('');
  const [validationSchemaIN, setvalidationSchemaIN] = useState(validationInsuranceSchema.RequiredIN);


  useEffect(() => {
    let obj = validationInsuranceSchema.RequiredIN;
    if (insurancePolType === "36" || insurancePolType === "37" || insurancePolType === "14") {
      obj = { ...obj, ...validationInsuranceSchema.Available }

    }
    setvalidationSchemaIN(obj);
    return () => { }
  }, [insurancePolType])
  const local: any = getUser();
  const { verifyInsurance, setVerifyInsurance, } = useRegistrationStore((state: any) => state);
  const [policyList, setPolicyList] = useState([]);
  const [policyNameList, setPolicyNameList] = useState([]);
  const [pPType, setPPType] = useState();
  const [insuranceType, setInsuranceType] = useState(); 
  const [loaderApi, setLoaderApi] = useState(false);
  const handleInsuranceType = (e: any) => {
    setInsuranceType(e.name);
    setinsurancePolicySelectedList([...insurancePolicyList.filter(id => id.AssetType_Id == e.Asset_Id)]);
  };

  const [buttonText, setButtonText] = useState("Add");
  const [policyProvider, setPolicyProvider] = useState();
  const handlePolicyProvider = (e: any) => { setPolicyProvider(e.name); };
  const {

    frequencyList,
    inDropdownList,
    insurancePolicyList,
    insurancePolicySelectedList,
    localPlanId,

    setinsurancePolicySelectedList,
  } = useFinancialPlanStore();
  const [InId, setInId] = useState<any>({});

  const searchPolicy = async (part: any) => {
    try {
      const obj: any = {
        AssetType: insurancePolType || null,
        PolicyProvider: pPType || null,
        SearchText: part
      }
      const enc: any = encryptData(obj);
      const result: any = await insurancePolicySearch(enc);
      let policyNames: any = [];
      result?.data?.Value.map((record: any) => {
        policyNames.push(record.Text);
      });
      setPolicyList(result?.data?.Value);
      setPolicyNameList(policyNames);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }


  const CreateAssetInsurance = async (data: any) => {
    data = data || {};

    try {
      setLoaderApi(true);
      let policyId: any = "";
      policyList.forEach(element => {
        // @ts-ignore
        if (element.Text.trim() === data?.policyName.trim()) {
          // @ts-ignore
          policyId = element.Value;
        }
      });
      if(policyId===""){
        setLoaderApi(false);
        toastAlert("warn", "No Ploicy Found");
        return false; 
      }
      let policyOwnerArr: any = [];
      data?.PolicyBenificiar.forEach((element: any) => {
        policyOwnerArr.push({
          clientName: element?.ClientName,
          dependentId: element?.Dependent_Id,
          basicId: element?.BasicId
        })
      });
      const obj: any =
      [{
        assetName: data?.policyName,
        assetTypeId: data?.insuranceType,
        basicId: local?.basicid,
        dependentid: "0",
        policySumAssured: Number(data?.CoverSumAssured),
        premiumTerm: data?.PremiumPayingTerm,
        policyTerm: data?.PolicyTerm,
        frequencyTypeId: data?.Frequency,
        liquidityType: "",
        planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid, //localPlanId
        policyId: policyId,
        policyOwnerBasicId: local?.basicid,
        policyOwners: policyOwnerArr,
        policyPartnerId: Number(data?.PolicyProvider),
        policyPremium: Number(data?.PremiumAmount),
        rmCode: local?.rmCode,
        currentValue: Number(data?.AvailableValue),
        trxnSource: data?.TransactionSource
      }]
      const enc: any = encryptData(obj);

      const result: any = await createInsurance(enc);
      if (result?.status === "Success") {
        toastAlert("success", "Insurance Successfully Added");
        fetchOtherAssetDetails();
        setLoaderApi(false);
        return true;
      }
      else {
        toastAlert("warn", result?.msg);
        setLoaderApi(false);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }

  const UpdateAssetInsurance = async (data: any) => {
    data = data || {};

    try {
      setLoaderApi(true);
      let policyId: any = '';
      policyList.forEach(element => {
        // @ts-ignore
        if (element.Text.trim() === data?.policyName.trim()) {
          // @ts-ignore
          policyId = element.Value;
        }
      });
      if(policyId===""){
        setLoaderApi(false);
        toastAlert("warn", "No Ploicy Found");
        return false; 
      }
      const obj: any =
      [{
        assetName: data?.policyName,
        assetTypeId: data?.insuranceType,
        basicId: local?.basicid,
        dependentid: "0",
        policySumAssured: Number(data?.CoverSumAssured),
        premiumTerm: data?.PremiumPayingTerm,
        policyTerm: data?.PolicyTerm,
        frequencyTypeId: data?.Frequency,
        liquidityType: "",
        planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid, //localPlanId
        policyId: policyId,
        policyOwnerBasicId: local?.basicid,
        policyPartnerId: Number(data?.PolicyProvider),
        policyPremium: Number(data?.PremiumAmount),
        rmCode: local?.rmCode,
        currentValue: Number(data?.AvailableValue),
        trxnSource: data?.TransactionSource,
        id: InId?.id
      }]
      const enc: any = encryptData(obj);
      const result: any = await createInsurance(enc);
      if (result?.status === "Success") {
        toastAlert("success", "Insurance Successfully Updated");
        setButtonText('Add');
        setLoaderApi(false);
        fetchOtherAssetDetails();
        return true;
      }
      else {
        toastAlert("warn", result?.msg);
        setLoaderApi(false);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }

  const setInsurances = async (record: any) => {
    if (buttonText === "Add") {
      const check = await CreateAssetInsurance(record);
      return check;
    }
    else if (buttonText === "Update") {
      const check = await UpdateAssetInsurance(record);
      return check;
    }
  }



  return (
    <>
      <Box className="row justify-content-center">
        <Box className="col-10">
          <Formik

            initialValues={verifyInsurance}
            validationSchema={Yup.object().shape(validationSchemaIN)}
            enableReinitialize
            onSubmit={async (values, { resetForm }) => {
              const check = await setInsurances(values);
              if (check) {
                resetForm();
                setVerifyInsurance({
                  insuranceType: "",
                  PolicyProvider: "",
                  AvailableValue: "",
                  TransactionSource: "",
                  policyName: "",
                  PolicyNumber: "",
                  IssuedDate: "",
                  PremiumAmount: "",
                  PolicyTerm: "",
                  PremiumPayingTerm: "",
                  CoverSumAssured: "",
                  PolicyOwner: "",
                  Frequency: "",
                  PolicyBenificiar: [],
                })
              }
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleReset,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (
              <>
                <Box className="row mt-1">
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <SelectMenu
                        items={inDropdownList}
                        bindValue={"Asset_Id"}
                        bindName={"AssetName"}
                        label={"Insurance Type"}
                        name="insuranceType"
                        value={values?.insuranceType}
                        onChange={(e: any) => {
                          setFieldValue("insuranceType", e?.Asset_Id);
                          handleInsuranceType(e);
                          setInsurancePolType(e?.Asset_Id)

                        }}
                        required
                        error={submitCount ? errors.insuranceType : undefined}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box className={"mt-1"} >
                      <SelectMenu
                        items={insurancePolicySelectedList}
                        bindValue={"PartnerId"}
                        bindName={"Partner_Name"}
                        label="Policy Provider"
                        name="PolicyProvider"
                        value={values.PolicyProvider}
                        onChange={(e: any) => {
                          setFieldValue("PolicyProvider", e?.PartnerId);
                          handlePolicyProvider(e);
                          setPPType(e?.PartnerId);
                        }}
                        required
                        error={submitCount ? errors.PolicyProvider : undefined}
                      />

                    </Box>
                  </Box>
                  {insurancePolType === "36" || insurancePolType === "37" || insurancePolType === "14" ? <>
                    <Box className="col-lg-4 col-md-5 col-sm-12">
                      <Box>
                        <Input
                          label="Available Value"
                          name="AvailableValue"
                          placeholder="Enter Current Value..."
                          required
                          error={submitCount ? errors.AvailableValue : null}
                          onChange={(e: any) => { setFieldValue("AvailableValue", e.target.value) }}

                          value={values?.AvailableValue}
                        />
                      </Box>
                    </Box>
                  </> : <></>}
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box className={"mt-1"}>
                      <SelectMenu
                        name="TransactionSource"
                        items={[{ id: "Fincart", name: "From Us" }, { id: "External", name: "External" }]}
                        bindValue={"id"}
                        bindName={"name"}
                        label={"Transaction Source"}
                        error={submitCount ? errors.TransactionSource : null}
                        value={values.TransactionSource}
                        required
                        onChange={(e: any) => {
                          setFieldValue("TransactionSource", e?.id);
                        }}
                      />
                    </Box>
                  </Box>

                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Policy Number"
                        name="PolicyNumber"
                        placeholder="Enter Policy No..."
                        onChange={(e: any) => {
                          setFieldValue("PolicyNumber", (e?.target?.value.length > 25 ? e?.target?.value.slice(0, 25) : e?.target?.value).toString())
                        }}
                        value={values?.PolicyNumber}
                      />
                    </Box>
                  </Box>

                  <Box className="col-lg-4 col-md-4 col-sm-12">

                    <Label
                      style={{ marginBottom: 5, fontSize: '0.7rem', color: '#339cff', fontWeight: 600, letterSpacing: '0.1rem' }}>Policy Name<span style={{ color: "#e5484d" }}>&nbsp;*</span></Label><br></br>

                    <Autocomplete
                      getItemValue={(item: any) => item}
                      items={policyNameList}
                      menuStyle={{
                        borderRadius: '3px',
                        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                        background: 'rgba(255, 255, 255, 0.9)',
                        padding: '0px 0px',
                        fontSize: '75%',
                        position: 'absolute',
                        overflow: 'auto',
                        zIndex: "1",
                        maxHeight: '25%', // TODO: don't cheat, let it flow to the bottom
                      }}
                      renderItem={(item: any, isHighlighted: any) =>
                        <div style={{ background: isHighlighted ? 'lightgray' : 'white', }}>
                          {item}
                        </div>
                      }
                      value={values?.policyName}
                      onChange={(e: any) => { setFieldValue("policyName", e.target.value); searchPolicy(e.target.value) }}
                      onSelect={(val: any) => { setFieldValue("policyName", val); }}
                    />
                    {submitCount ? <Text weight="normal"
                        size="h6"
                        //@ts-ignore
                        css={{ color: "#e5484d" }}>{errors.policyName}</Text> : null}
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        type="date"
                        label="Issue Date"
                        name="IssuedDate"
                        onChange={(e: any) => { setFieldValue("IssuedDate", e.target.value) }}
                        value={values?.IssuedDate}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Premium Amount"
                        name="PremiumAmount"
                        placeholder="Enter Amount"
                        error={submitCount ? errors.PremiumAmount : null}
                        required
                        onChange={(e: any) => {
                          setFieldValue("PremiumAmount", (e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, '')).toString())
                        }}

                        value={values?.PremiumAmount}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Policy Term"
                        name="PolicyTerm"
                        placeholder="Enter Policy Term.."
                        onChange={(e: any) => {
                          setFieldValue("PolicyTerm", (e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, '')).toString())
                        }}
                        value={values?.PolicyTerm}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Premium Paying Term"
                        name="PremiumPayingTerm"
                        placeholder="Enter PPT"
                        error={submitCount ? errors.PremiumPayingTerm : null}
                        onChange={(e: any) => {
                          setFieldValue("PremiumPayingTerm", (e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, '')).toString())
                        }}
                        required
                        value={values?.PremiumPayingTerm}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Cover/Sum Assured"
                        name="CoverSumAssured"
                        placeholder="Enter Sum Assured"
                        onChange={(e: any) => {
                          setFieldValue("CoverSumAssured", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.CoverSumAssured}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <SelectMenu
                        name="Frequency"
                        items={frequencyList}
                        bindValue={"ID"}
                        bindName={"Type_name"}
                        label={"Frequency"}
                        error={submitCount ? errors.Frequency : null}
                        value={values.Frequency}
                        required
                        onChange={(e: any) => {
                          setFieldValue("Frequency", e?.ID);
                        }}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <SelectMenu
                        items={basicDetail}
                        bindValue={"BasicId"}
                        bindName={"ClientName"}
                        name="PolicyOwner"
                        label={"Policy Owner"}
                        error={submitCount ? errors.PolicyOwner : null}
                        value={values.PolicyOwner}
                        required
                        onChange={(e: any) => {
                          setFieldValue("PolicyOwner", e?.BasicId);
                        }}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <SelectMenu
                        items={basicDetail}
                        bindValue={"Dependent_Id"}
                        bindName={"ClientName"}
                        label={"Policy Beneficiary"}
                        name="PolicyBenificiar"
                        value={values.PolicyBenificiar}
                        isMulti={true}
                        onChange={(e: any) => {
                      
                          setFieldValue("PolicyBenificiar", e);
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box className="row justify-content-between">
                  <Box className="col-auto py-1">
                    <Box
                      onClick={() => setOpenUnmappedIN(true)}
                      css={{
                        color: "var(--colors-blue1)",
                        cursor: "pointer",
                      }}//AR
                    >
                      {/* @ts-ignore */}
                      <Text size="h6"><u>Un-Mapped Entries</u></Text>
                    </Box>

                  </Box>
                  <Box className="col-auto">
                    <Button
                      className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      disabled={loaderApi}
                      onClick={() => {
                        handleReset(); setButtonText("Add"); setVerifyInsurance({
                          insuranceType: "",
                          PolicyProvider: "",
                          AvailableValue: "",
                          TransactionSource: "",
                          policyName: "",
                          PolicyNumber: "",
                          IssuedDate: "",
                          PremiumAmount: "",
                          PolicyTerm: "",
                          PremiumPayingTerm: "",
                          CoverSumAssured: "",
                          PolicyOwner: "",
                          Frequency: "",
                          PolicyBenificiar: [],
                        })
                      }}
                    >

                      <Text
                        //@ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={styles.button}
                      >
                        Reset
                      </Text>
                    </Button>&nbsp;
                    <Button
                      className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                      color="yellowGroup"
                      size="md"
                      onClick={handleSubmit}
                      disabled={loaderApi}

                    >
                      <Text
                        //@ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={styles.button}
                      >
                        {loaderApi ? 'Loading...' : `${buttonText}`}
                      </Text>
                    </Button>
                  </Box >
                </Box >
              </>
            )}
          </Formik >
        </Box >
      </Box >
      <hr className="text-primary mx-0 my-1" />
      <Box>
        <Text
          //@ts-ignore
          weight="normal"
          size="h4"
          css={{ color: "var(--colors-blue1)" }}
          className="text-center mx-3 my-3"
        >
          Insurance
        </Text>
      </Box>
      <Box className="row ">
        <Box
          className="col-auto col-lg-12 col-md-12 col-sm-12"
          css={{ overflowX: "auto" }}
        >
          <Box className="table-responsive" css={{ borderRadius: "8px" }}>
            <Table
              className="text-capitalize table table-striped"
              css={{ color: "var(--colors-blue1)" }}
            >
              <ColumnParent className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                <ColumnRow>
                  {/* <Column>
                    <Box className="row" css={{ width: "130px" }}>

                      <Box className="col-auto mt-0" css={{ fontWeight: "normal" }}>Select All </Box>
                      <Box className="col-auto mt-0">
                        <Input type="checkbox" />
                      </Box></Box>
                  </Column> */}
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Insurance</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Policy Details</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Current Value</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Premium</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Cover/Sum Assured</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Action</Text></Column>
                </ColumnRow>
              </ColumnParent>
              <DataParent>
                {inDetailsList.length === 0 ? <>
                  <DataRow>
                    <DataCell colSpan={11} className="text-center">
                      No Insurance Found{" "}
                    </DataCell>
                  </DataRow>
                </> : <>

                  {
                    inDetailsList.map((record: any, index: any) => {

                      return (

                        <DataRow>
                          {/* <DataCell><Box className="row">
                            <Box className="mt-0"><Input type="checkbox" /></Box>
                          </Box></DataCell> */}
                          <DataCell>
                            <span>Type:</span> {record?.assetTypeName}<br />
                            Source: {record?.trxnSource}</DataCell>
                          <DataCell>Provider: {record?.PolicyPartnerName}<br />
                            Name: {record?.assetName}<br />
                            Owner: {record?.PolicyOwnerName}<br />
                            Frequency: {record?.FrequencyTypeName}</DataCell>
                          <DataCell>{record?.CurrentValue}</DataCell>
                          <DataCell>Amount: {record?.policyPremium}<br />
                            Paying term: {record?.premiumTerm}<br />
                            Policy term: {record?.policyTerm}</DataCell>
                          <DataCell>{record?.policySumAssured}</DataCell>
                          <DataCell>
                            <Box className="row">
                              <Box className="col-auto">
                                {/* @ts-ignore */}
                                <Text onClick={async () => {
                                  handleInsuranceType({
                                    name: record?.policyName,
                                    Asset_Id: record?.assetTypeId
                                  });
                                  setInsurancePolType(String(record?.assetTypeId))
                                  await setVerifyInsurance({
                                    insuranceType: record?.assetTypeId,
                                    PolicyProvider: String(record?.policyPartnerId),
                                    PolicyNumber: record?.policyNo,
                                    policyName: record?.PolicyName,
                                    PremiumAmount: String(record.policyPremium),
                                    PolicyTerm: record?.policyTerm,
                                    PremiumPayingTerm: record?.premiumTerm,
                                    CoverSumAssured: record?.policySumAssured,
                                    Frequency: record?.FrequencyTypeId,
                                    TransactionSource: record.trxnSource,
                                    PolicyOwner: record?.policyOwnerBasicId,
                                    AvailableValue: record?.CurrentValue,
                                    IssuedDate: String(record?.PolicyIssueDate)
                                    // LoanType: record?.assetTypeId, AssetNameLoan: record?.assetName,
                                    // PrincipalAmount: record?.loanPrincipalAmount, RateOfInterest: record?.loanInterest, EMIAmount: record?.loanEmiAmount, LoanStartDate: record?.loanStartDate, LoanEndDate: record?.loanEndDate, OutstandingAmount: record?.loanOutstandAmount, Frequency: record?.FrequencyTypeId, TransactionSource: record.trxnSource === "Fincart" ? "1" : "2", LiquidityType: record?.LiquidityType

                                    //assetName: record?.policyName, policyPartnerId: record?.PolicyProvider, policyPremium: record?.PremiumAmount, trxnSource: record?.trxnSource

                                  })
                                  setButtonText("Update");

                                  setInId({
                                    planId: record?.PlanId,
                                    basicId: record?.BasicId,
                                    id: record?.Id
                                  });
                                }}
                                  css={{ cursor: "pointer" }}>
                                  <PencilFill />
                                </Text>
                              </Box>

                              &nbsp;&nbsp;

                              <Box className="col-auto">
                                {/* @ts-ignore */}
                                <Text onClick={() => {
                                  setOpenDelete(true);
                                  setDeleteId({
                                    planId: record?.PlanId,
                                    basicId: record?.BasicId,
                                    id: record?.Id
                                  });
                                }} css={{ cursor: "pointer" }}>
                                  <DeleteIcon /></Text></Box>
                            </Box>
                          </DataCell>
                        </DataRow>)
                    })}
                </>}
              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Insurance;
