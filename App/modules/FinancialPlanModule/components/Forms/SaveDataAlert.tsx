import Box from "@ui/Box/Box";
import React, { useState } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";
import { Button } from "@ui/Button/Button";
import styles from "../../FinancialPlan.module.scss";
const SaveDataAlert = () => {
    return (
        <>
            <Box className="row">
                <Box className="col-md-12 col-sm-12 col-lg-12 mt-3" css={{ color: "var(--colors-blue1)" }}>
                    {/* @ts-ignore */}
                    <Text weight="bold" size="h4" className="text-center mb-1">
                        Thank You
                    </Text>
                </Box>
            </Box>
            <hr className="text-primary mx-0" />
            <Box className="row">
                <Box className="col-md-12 col-sm-12 col-lg-12 text-end" css={{ color: "var(--colors-blue1)" }}>
                    <Text weight="normal" size="h4" className="text-center mb-1 rounded"  >
                        Your data is collected you will be notified <br />When your relationship manager genearte plan!!!
                    </Text>
                </Box>
            </Box>
        </>
    )
}
export default SaveDataAlert;