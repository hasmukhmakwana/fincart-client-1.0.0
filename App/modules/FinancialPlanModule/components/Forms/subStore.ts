import create from "zustand";

import { childType, depentType, verifyBasicDTypes } from "./BasicDetailsType";

interface StoreTypes {

    verifyBasicD: verifyBasicDTypes;
    setVerifyBasicD: (payload: verifyBasicDTypes) => void;
}


const useBasicDetailsStore = create<StoreTypes>((set) => ({
    //* initial state
    verifyBasicD: {
        Full_Name: "",
        DateOfBirth: "",
        companyName: "",
        maritalStatus: "",
        designation: "",
        gender: "",
        Full_Name_Spouse: "",
        DateOfBirth_Spouse: "",
        Age_Spouse: "",
        SpWorkStatus: "",
        companyName_Spouse: "",
        designation_Spouse: "",
        income_Spouse: "",
        children: [],
        dependant: [],
    },
    setVerifyBasicD: (payload) =>
        set((state) => ({
            ...state,
            verifyBasicD: payload,
        })),
}));

export default useBasicDetailsStore;