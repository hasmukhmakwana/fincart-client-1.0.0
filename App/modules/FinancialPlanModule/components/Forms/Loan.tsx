import React, { useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input/Input";
import SelectMenu from "@ui/Select/Select";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";

import { Button } from "@ui/Button/Button";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import styles from "../../FinancialPlan.module.scss";
import useFinancialPlanStore from "../../store";
import DeleteIcon from "App/icons/DeleteIcon";
import PencilFill from "App/icons/PencilFill";
import { createInsurance, assetNameSearch, } from "App/api/financialPlan";
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser, plainBase64 } from "App/utils/helpers";
import { Label } from "@radix-ui/react-label";
import Autocomplete from "react-autocomplete";

const validationLoanSchema = Yup.object().shape(
  {
    LoanType: Yup.string().required("Loan Type is required").trim(),
    AssetNameLoan: Yup.string().required("Asset Name is required").trim(),
    PrincipalAmount: Yup.string().required("Principal Amount is required").trim(),
    RateOfInterest: Yup.string().required("Rate Of Interest is required").trim(),
    LoanStartDate: Yup.string(),
    LoanEndDate: Yup.string().required("Loan End Date is required").trim(),
    EMIAmount: Yup.string().required("EMI Amount is required").trim(),
    OutstandingAmount: Yup.string().required("Outstanding Amount is required").trim(),
    TransactionSource: Yup.string().required("Transaction Source is required").trim(),
    Frequency: Yup.string().required("Frequency is required").trim(),
    LiquidityType: Yup.string().required("Liquidity Type is required").trim(),
  }
)


export type VerifyLoanTypes = {
  LoanType: string;
  AssetNameLoan: string;
  PrincipalAmount: string;
  RateOfInterest: string;
  LoanStartDate: string;
  LoanEndDate: string;
  EMIAmount: string;
  OutstandingAmount: string;
  TransactionSource: string;
  Frequency: string;
  LiquidityType: string;
}

interface StoreTypes {
  verifyLoan: VerifyLoanTypes;
  setVerifyLoan: (payload: VerifyLoanTypes[]) => void;
}

const useRegistrationStore = create<StoreTypes>((set) => ({
  //* initial state
  verifyLoan: {
    LoanType: "",
    AssetNameLoan: "",
    PrincipalAmount: "",
    RateOfInterest: "",
    LoanStartDate: "",
    LoanEndDate: "",
    EMIAmount: "",
    OutstandingAmount: "",
    TransactionSource: "",
    Frequency: "",
    LiquidityType: "",
  },

  setVerifyLoan: (payload: any) =>
    set((state) => ({
      ...state,
      verifyLoan: payload,
    })),

}));



const Loan = ({ lnDetailsList, fetchOtherAssetDetails, setOpenDelete, setDeleteId, setOpenUnmappedLN }: any) => {

  const local: any = getUser();
  const { verifyLoan, setVerifyLoan } = useRegistrationStore((state: any) => state);
  const [AssetLoan, setAssetLoan] = useState([]);
  const [AssetLoanList, setAssetLoanList] = useState([]);
  const [loanId, setLoanId] = useState<any>({});
  const [buttonText, setButtonText] = useState("Add");
  const [loaderApi, setLoaderApi] = useState(false);


  const {
    frequencyList,
    lnDropdownList,
    insurancePolicyList,
    localPlanId,
    setinsurancePolicySelectedList,
  } = useFinancialPlanStore();

  const searchAssetLoan = async (part: any) => {
    try {
      setLoaderApi(true);
      const obj: any = {
        SearchText: part,
        Type: "LN"
      }
      const enc: any = encryptData(obj);
      const result: any = await assetNameSearch(enc);
      let assetNameLoan: any = [];
      result?.data?.Value.map((record: any) => {
        assetNameLoan.push(record.Text);
      });
      setAssetLoan(result?.data?.Value);
      setAssetLoanList(assetNameLoan);
      setLoaderApi(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
    }
  }

  //Loan API
  const CreateNewLoan = async (data: any) => {
    data = data || {};
    try {
      setLoaderApi(true);
      let schemeId: any = "";
      AssetLoan.forEach(element => {
        // @ts-ignore
        if (element.Text.trim() === data.AssetNameLoan.trim()) {
          // @ts-ignore
          schemeId = element.Value;
        }
      });
      if(schemeId===""){
        setLoaderApi(false);
        toastAlert("warn", "No Asset Found");
        return false; 
      }
      const obj: any =
      [{
        LoanSchemeId: null,
        assetName: data?.AssetNameLoan,//"ICICI Bank_Manish Bangalore(Aggarwal Associates)",
        assetTypeId: Number(data?.LoanType),
        basicId: local?.basicid,
        frequencyTypeId: data?.Frequency,
        liquidityType: data?.LiquidityType,
        loanEmiAmount: data?.EMIAmount,
        loanEndDate: data?.LoanEndDate,
        loanInterest: data?.RateOfInterest,
        loanOutstandAmount: data?.OutstandingAmount,
        loanPrincipalAmount: data?.PrincipalAmount,
        loanStartDate: data?.LoanStartDate,
        planId: localPlanId?.basicid === undefined ? localPlanId === "0" ? localPlanId : undefined : localPlanId?.basicid,
        rmCode: local?.rmCode,
        schemeId: schemeId,
        trxnSource: data?.TransactionSource
      }]
      const enc: any = encryptData(obj);
      const result: any = await createInsurance(enc);
      if (result?.status === "Success") {
        toastAlert("success", "Loan Successfully Added");
        setLoaderApi(false);
        setButtonText("Add");
        fetchOtherAssetDetails();
        return true;
      }
      else {
        toastAlert("warn", result?.msg);
        setLoaderApi(false);
        return false;
      }


    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }
  const UpdateLoan = async (data: any) => {
    data = data || {};
    try {
      setLoaderApi(true);
      let schemeId: any = "";
      AssetLoan.forEach(element => {
        // @ts-ignore
        if (element.Text.trim() === data.AssetNameLoan.trim()) {
          // @ts-ignore
          schemeId = element.Value;
        }
      });
      if(schemeId===""){
        setLoaderApi(false);
        toastAlert("warn", "No Asset Found");
        return false; 
      }
      const obj: any =
      [{
        LoanSchemeId: null,
        assetName: data?.AssetNameLoan,
        assetTypeId: data?.LoanType,
        basicId: local?.basicid,
        frequencyTypeId: data?.Frequency,
        liquidityType: data?.LiquidityType,
        id: loanId?.id,
        loanEmiAmount: data?.EMIAmount,
        loanEndDate: data?.LoanEndDate,
        loanInterest: data?.RateOfInterest,
        loanOutstandAmount: data?.OutstandingAmount,
        loanPrincipalAmount: data?.PrincipalAmount,
        loanStartDate: data?.LoanStartDate,
        planId: localPlanId?.basicid,
        rmCode: local?.rmCode,
        schemeId: schemeId,
        trxnSource: data?.TransactionSource
      }]
      const enc: any = encryptData(obj);
      const result: any = await createInsurance(enc);
      if (result?.status === "Success") {
        toastAlert("success", "Loan Successfully Added");
        setLoaderApi(false);
        setButtonText("Add");
        fetchOtherAssetDetails();
        return true;
      } else {
        toastAlert("warn", result?.msg);
        setLoaderApi(false);
        return false;
      }

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoaderApi(false);
      return false;
    }
  }
  const setLoan = async (record: any) => {
    if (buttonText === "Add") {
      const check = await CreateNewLoan(record);
      return check;
    }
    else if (buttonText === "Update") {
      const check = await UpdateLoan(record);
      return check;
    }
  }

  return (
    <>
      <Box className="row justify-content-center">
        <Box className="col-10">
          <Formik
            initialValues={verifyLoan}
            validationSchema={validationLoanSchema}
            enableReinitialize
            onSubmit={async (values, { resetForm }) => {
              const check = await setLoan(values);
              if (check) {
                resetForm();
                setVerifyLoan({
                  LoanType: "",
                  AssetNameLoan: "",
                  PrincipalAmount: "",
                  RateOfInterest: "",
                  LoanStartDate: "",
                  LoanEndDate: "",
                  EMIAmount: "",
                  OutstandingAmount: "",
                  TransactionSource: "",
                  Frequency: "",
                  LiquidityType: "",
                })
              }
            }}
          >
            {({
              values,
              handleReset,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (
              <>

                <Box className="row">
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu

                        items={lnDropdownList}
                        bindValue={"Asset_Id"}
                        bindName={"AssetName"}
                        label={"Loan Type"}
                        name="LoanType"
                        value={values.LoanType}
                        onChange={(e: any) => {
                          setFieldValue("LoanType", e?.Asset_Id)
                        }}
                        required
                        error={submitCount ? errors.LoanType : undefined}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Label
                      style={{ marginBottom: 5, fontSize: '0.7rem', color: '#339cff', fontWeight: 600, letterSpacing: '0.1rem' }}>Asset Name <span style={{ color: "#e5484d" }}>&nbsp;*</span></Label><br></br>

                    <Autocomplete
                      getItemValue={(item: any) => item}
                      items={AssetLoanList}
                      menuStyle={{
                        borderRadius: '3px',
                        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                        background: 'rgba(255, 255, 255, 0.9)',
                        padding: '0px 0px',
                        fontSize: '75%',
                        position: 'absolute',
                        overflow: 'auto',
                        zIndex: "1",
                        maxHeight: '25%', // TODO: don't cheat, let it flow to the bottom
                      }}
                      renderItem={(item: any, isHighlighted: any) =>
                        <div style={{ background: isHighlighted ? 'lightgray' : 'white', }}>
                          {item}
                        </div>
                      }
                      value={values?.AssetNameLoan}
                      onChange={(e: any) => { setFieldValue("AssetNameLoan", e.target.value); searchAssetLoan(e.target.value) }}
                      onSelect={(val: any) => { setFieldValue("AssetNameLoan", val); }}
                    />
                    {/* @ts-ignore */}
                    {submitCount ? <Text size="h6" css={{ color: "#e5484d" }}>{errors.AssetNameLoan}</Text> : ""}

                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Principal Amount"
                        name="PrincipalAmount"
                        placeholder="Enter Amount..."
                        error={submitCount ? errors.PrincipalAmount : null}
                        required
                        onChange={(e: any) => {
                          setFieldValue("PrincipalAmount", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.PrincipalAmount}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Rate Of Interest(%)"
                        name="RateOfInterest"
                        placeholder="Enter Loan Interest..."
                        error={submitCount ? errors.RateOfInterest : null}
                        required
                        onChange={(e: any) => {
                          setFieldValue("RateOfInterest", e?.target?.value.length > 3 ? e?.target?.value.slice(0, 3) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.RateOfInterest}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        type="date"
                        label="Loan Start Date"
                        name="LoanStartDate"
                        onChange={handleChange}
                        value={values?.LoanStartDate}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        type="date"
                        label="Loan End Date"
                        name="LoanEndDate"
                        error={submitCount ? errors.LoanEndDate : null}
                        required
                        onChange={handleChange}
                        value={values?.LoanEndDate}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="EMI Amount"
                        name="EMIAmount"
                        placeholder="Enter EMI Amount"
                        error={submitCount ? errors.EMIAmount : null}
                        required
                        onChange={(e: any) => {
                          setFieldValue("EMIAmount", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.EMIAmount}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12">
                    <Box>
                      <Input
                        label="Outstanding Amount"
                        name="OutstandingAmount"
                        placeholder="Enter Amount.."
                        error={submitCount ? errors.OutstandingAmount : null}
                        required
                        onChange={(e: any) => {
                          setFieldValue("OutstandingAmount", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))
                        }}
                        value={values?.OutstandingAmount}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu
                        items={[
                          { id: "Fincart", TransactionSource: "From Us" },
                          { id: "External", TransactionSource: "External" },
                        ]}
                        bindValue={"id"}
                        bindName={"TransactionSource"}
                        label={"Transaction Source"}
                        name={"TransactionSource"}
                        value={values.TransactionSource}
                        onChange={(e: any) => {
                          setFieldValue("TransactionSource", e.id);
                        }}
                        required
                        error={submitCount ? errors.TransactionSource : undefined}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu
                        items={frequencyList}
                        bindValue={"ID"}
                        bindName={"Type_name"}
                        label={"Frequency"}
                        value={values.Frequency}
                        onChange={(e: any) => {
                          setFieldValue("Frequency", e?.ID)
                        }}
                        required
                        error={submitCount ? errors.Frequency : null}
                      />
                    </Box>
                  </Box>
                  <Box className="col-lg-4 col-md-5 col-sm-12 mt-1">
                    <Box>
                      <SelectMenu
                        name="LiquidityType"
                        items={[
                          { id: "High", name: "High" },
                          { id: "Low", name: "Low" },
                        ]}
                        bindValue={"id"}
                        bindName={"name"}
                        label={"Liquidity Type"}
                        value={values.LiquidityType}
                        onChange={(e: any) => {
                          setFieldValue("LiquidityType", e?.id)
                        }}
                        required
                        error={submitCount ? errors.LiquidityType : null}
                      />
                    </Box>
                  </Box>
                  <Box className="row justify-content-between">
                    <Box className="col-auto">
                      <Box
                        onClick={() => setOpenUnmappedLN(true)}
                        css={{
                          color: "var(--colors-blue1)",
                          cursor: "pointer",
                        }}//AR
                      >
                        {/* @ts-ignore */}
                        <Text size="h6"><u>Un-Mapped Entries</u></Text>
                      </Box>
                    </Box>
                    <Box className="col-auto">
                      <Button
                        className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                        color="yellowGroup"
                        size="md"
                        disabled={loaderApi}
                        onClick={() => {
                          handleReset(); setButtonText("Add"); setVerifyLoan({
                            LoanType: "",
                            AssetNameLoan: "",
                            PrincipalAmount: "",
                            RateOfInterest: "",
                            LoanStartDate: "",
                            LoanEndDate: "",
                            EMIAmount: "",
                            OutstandingAmount: "",
                            TransactionSource: "",
                            Frequency: "",
                            LiquidityType: "",
                          })
                        }}
                      >
                        <Text
                          // @ts-ignore
                          weight="normal"
                          size="h6"
                          //@ts-ignore
                          color="gray8"
                          className={styles.button}
                        >
                          Reset
                        </Text>
                      </Button>&nbsp;
                      <Button
                        className={`col-auto mb-2 mx-0 py-1 px-4 ${styles.button}`}
                        color="yellowGroup"
                        size="md"
                        onClick={handleSubmit}
                        disabled={loaderApi}
                      >
                        <Text
                          // @ts-ignore
                          weight="normal"
                          size="h6"
                          //@ts-ignore
                          color="gray8"
                          className={styles.button}
                        >
                          {loaderApi ? <>Loading...</> : <>{buttonText}</>}
                        </Text>
                      </Button>
                    </Box>
                  </Box >
                </Box >

              </>
            )}
          </Formik >
        </Box >
      </Box >
      <hr className="text-primary mx-0 my-1" />
      <Box>
        <Text
          // @ts-ignore
          weight="normal"
          size="h4"
          css={{ color: "var(--colors-blue1)" }}
          className="text-center m-3"
        >
          Loan
        </Text>
      </Box>
      <Box className="row ">
        <Box
          className="col-auto col-lg-12 col-md-12 col-sm-12"
          css={{ overflowX: "auto" }}
        >
          <Box className="table-responsive" css={{ borderRadius: "8px" }}>
            <Table
              className="text-capitalize table table-striped"
              css={{ color: "var(--colors-blue1)" }}
            >
              <ColumnParent className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                <ColumnRow>
                  {/* <Column>
                    <Box className="row" css={{ width: "130px" }}>
                      <Box className="col-auto mt-0" css={{ fontWeight: "normal" }}>Select All </Box>
                      <Box className="col-auto mt-0">
                        <Input type="checkbox" />
                      </Box></Box>
                  </Column> */}
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Loan Type</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Principal Amount</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Rate Of Interest(%)</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>EMI Amount</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Start Date</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>End Date</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Pending Loan Amount</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Frequency</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Transaction Source</Text></Column>
                  <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Action</Text></Column>
                </ColumnRow>
              </ColumnParent>
              <DataParent>
                {lnDetailsList.map((record: any, index: any) => {
                  return (<DataRow>
                    {/* <DataCell><Box className="row"><Box className="col-lg-9 mt-0"><Input type="checkbox" /></Box></Box></DataCell> */}
                    <DataCell>{record?.assetTypeName}</DataCell>
                    <DataCell>{record?.loanPrincipalAmount}</DataCell>
                    <DataCell>{record?.loanInterest}</DataCell>
                    <DataCell>{record?.loanEmiAmount}</DataCell>
                    <DataCell>{record?.loanStartDate}</DataCell>
                    <DataCell>{record?.loanEndDate}</DataCell>
                    <DataCell>{record?.loanOutstandAmount}</DataCell>
                    <DataCell>{record?.FrequencyTypeName}</DataCell>
                    <DataCell>{record?.trxnSource}</DataCell>
                    <DataCell>
                      <Box className="row">
                        <Box className="col-auto">
                          {/* @ts-ignore */}
                          <Text onClick={() => {
                            setVerifyLoan({
                              LoanType: record?.assetTypeId, AssetNameLoan: record?.assetName,
                              PrincipalAmount: record?.loanPrincipalAmount,
                              RateOfInterest: record?.loanInterest,
                              EMIAmount: record?.loanEmiAmount,
                              LoanStartDate: record?.loanStartDate,
                              LoanEndDate: record?.loanEndDate,
                              OutstandingAmount: record?.loanOutstandAmount,
                              Frequency: record?.FrequencyTypeId,
                              TransactionSource: record.trxnSource,
                              LiquidityType: record?.LiquidityType
                            })
                            setButtonText("Update");

                            setLoanId({
                              planId: record?.PlanId,
                              basicId: record?.BasicId,
                              id: record?.Id
                            });
                          }}
                            css={{ cursor: "pointer" }}>
                            <PencilFill />
                          </Text>
                        </Box>

                        &nbsp;&nbsp;

                        <Box className="col-auto">
                          {/* @ts-ignore */}
                          <Text onClick={() => {
                            setOpenDelete(true);
                            setDeleteId({
                              planId: record?.PlanId,
                              basicId: record?.BasicId,
                              id: record?.Id
                            });
                          }} css={{ cursor: "pointer" }}>
                            <DeleteIcon /></Text></Box>
                      </Box>
                    </DataCell>
                  </DataRow>)
                })}
              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Loan;
