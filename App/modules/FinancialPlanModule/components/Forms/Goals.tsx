import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import AlertDialog from "@ui/AlertDialog/ModalDialog";
import Input from "@ui/Input";
import SelectMenu from "@ui/Select/Select";
import PopMessagePage from "./PopMessagePage";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
// import { StyledTabs, TabsList, TabsContent, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { Button } from "@ui/Button/Button";
import DeleteIcon from "App/icons/DeleteIcon";
import PencilFill from "App/icons/PencilFill";
import React, { useState, useEffect } from "react";
import styles from "../../FinancialPlan.module.scss";
import * as Yup from "yup";
import create from "zustand";
import { Formik } from "formik";
import useFinancialPlanStore from "../../store";
import TextInput from "react-autocomplete-input";
import {
    fetchGoalDropDown,
    searchOtherGoal,
    fetchAllGoalSchemeUAT,
    fetchGoalInflationAndRor,
    SaveGoal,
    EditGoal,
    goalCalculate,
    fetchGoalDetails,
    deleteGoal,
} from "App/api/financialPlan"
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import Loader from "App/shared/components/Loader";
import { Label } from "@radix-ui/react-label";
import Autocomplete from "react-autocomplete";

const ObjectSchemaList = {
    main: {
        GoalsToFind: Yup.string()
            .required("Goals To Find is required")
            .trim(),
    },
    mandatory: {
        PresentValue: Yup.string()
            .required("Present Value is required")
            .trim(),

        FromDate: Yup.string()
            .required("From Date is required")
            .trim(),

        ToDate: Yup.string()
            .required("To Date  is required")
            .trim(),

        Priority: Yup.string()
            .required("Priority  is required")
            .trim(),
    },
    house: {
        TotalHouseCost: Yup.string()
            .required("House Cost is required")
            .trim(),

        downPaymentPer: Yup.number()
            .required("DownPayment(%) is required")
            .moreThan(0, 'DownPayment(%) should not be zero or less than zero')
            .lessThan(100, "DownPayment(%) should not be more than 2 digits"),
        downCost: Yup.string()
            .required("Down Payment is required")
            .trim(),
        FromDate: Yup.string()
            .required("From Date is required")
            .trim(),

        ToDate: Yup.string()
            .required("To Date  is required")
            .trim(),

        Priority: Yup.string()
            .required("Priority  is required")
            .trim(),
    },
    Retire: {
        retirementAge: Yup.string()
            .required("Retirement Age is required")
            .trim(),

        currentAge: Yup.string()
            .required("Current Age  is required")
            .trim(),
        lifeExpectancy: Yup.string()
            .required("Current Age  is required")
            .trim(),
        expenseMonth: Yup.string()
            .required("Expense Month is required")
            .trim(),
        Priority: Yup.string()
            .required("Priority  is required")
            .trim(),
    },
    child: {
        childAge: Yup.string()
            .required("Child Age is required")
            .trim(),

        childName: Yup.string()
            // .matches(/^\s*[\S]+(\s[\S]+)+\s*$/gms, "Invalid Name")
            .required("Child Name is required")
            .trim(),
        gender: Yup.string()
            .required("Child Gender is required")
            .trim(),
    },
    other: {
        GoalName: Yup.string()
            .required("Goal Name is required")
            .trim(),
    }
}
export type VerifyKYCTypes = {

    GoalsToFind: string;
    PresentValue: string;
    FromDate: string;
    ToDate: string;
    Priority: string;

    TotalHouseCost: string;
    downPaymentPer: string;
    downCost: string;

    retirementAge: string;
    currentAge: string;
    lifeExpectancy: string;
    expenseMonth: string;

    childAge: string;
    childName: string;
    gender: string;

    GoalName: string;
};

export type goalCalTypes = {
    Inflation: string;
    ROR_Lumpsum: string;
    ROR_Sip: string;
}
interface StoreTypes {
    verifyKYC: VerifyKYCTypes;
    setVerifyKYC: (payload: VerifyKYCTypes) => void;
    goalCal: goalCalTypes[];
    setGoalCal: (payload: goalCalTypes[]) => void;
}
const useRegistrationStore = create<StoreTypes>((set) => ({
    //* initial state
    goalCal: [],

    setGoalCal: (payload) =>
        set((state) => ({
            ...state,
            goalCal: payload || [],
        })),

    verifyKYC: {
        GoalsToFind: "",
        PresentValue: "",
        FromDate: "",
        ToDate: "",
        Priority: "",

        TotalHouseCost: "",
        downPaymentPer: "0",
        downCost: "0",

        retirementAge: "0",
        currentAge: "",
        lifeExpectancy: "0",
        expenseMonth: "",

        childAge: "",
        childName: "",
        gender: "",

        GoalName: "",
    },

    setVerifyKYC: (payload) =>
        set((state) => ({
            ...state,
            verifyKYC: payload,
        })),
}));

// type SaveTypes = {
//     fetchAllGoal: () => void;
// };

const Goals = () => {
    const {
        loading,
        localUser,
        localPlanId,
        childList,
        lnDropdownList,
        // allGoals,
        basicDetail,
        retireAge,
        inflationAndRor,
        setChildList,
        setInflationAndRor,
        setRetireAge,
        setLoader
    } = useFinancialPlanStore()
    ////getUser()
    const local: any = getUser();
    // const [local, setLocalU] = useState<any>(local);
    const [allGoals, setAllGoals] = useState<any>([]);
    const [goalDD, setGoalDD] = useState<any>([]);
    // const handleGoalsType = (e: any) => { setGoalsType(e.name); };
    const [retireAgen, setRetireAgen] = useState([]);
    // const handleLifeType = (e: any) => { setLifeType(e.name); };
    const [validationSchema, setValidationSchema] = useState(ObjectSchemaList.main);
    const [goalFind, setGoalFind] = useState("");
    const [goalList, setGoalList] = useState([]);
    const [goalNameList, setGoalNameList] = useState([]);
    let infObj: any = {};
    let editObj: any = {};
    const [durationVal, setDurationVal] = useState<any>();
    const [openModal, setOpenModal] = useState(false);
    const [openDelModal, setOpenDelModal] = useState(false);
    const [fullGoal, setFullGoal] = useState({});
    const [IandRcal, setIandRcal] = useState(false);
    const [createSt, setCreateSt] = useState(false);
    const [calcObj, setCalcObj] = useState<any>({});
    const [infObjst, setInfObj] = useState<any>({});
    const [goalValue, setgoalValue] = useState<any>();
    const [editGoal, setEditGoal] = useState<any>([]);
    const [toDate, setToDate] = useState("");
    const [loaderApi, setLoaderApi] = useState(false);
    const [btntext, setBtntext] = useState("Add");
    const [otherIandR, setOtherIandR] = useState<any>([]);
    const [totHousecost, setTotHousecost] = useState<any>("0");
    const {
        verifyKYC,
        load,
        setVerifyKYC,
    } = useRegistrationStore((state: any) => state);
    let options: any = [];

    const searchGoal = async (gltext: any) => {

        try {
            setLoaderApi(true);
            const enc: any = encryptData(gltext, true);


            const result: any = await searchOtherGoal(enc);
            let goalNames: any = [];
            result?.data?.Value.map((record: any) => {
                goalNames.push(record.Text);
            });

            setGoalList(result?.data);
            setGoalNameList(goalNames);
            setLoaderApi(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
            setLoaderApi(false);
        }
    }

    const fetchGoalDD = async () => {
        try {
            setLoader(true);
            const enc: any = encryptData(local?.basicid, true);
            let resultDD: any = await fetchGoalDropDown(enc);
            setGoalDD(resultDD?.data?.Value)

            setLoader(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    }

    const fetchAllGoalScheme = async () => {
        try {
            setLoader(true);
            const enc: any = encryptData(local?.basicid, true);

            let result = await fetchAllGoalSchemeUAT(enc);
            let SortData: any = result?.data?.allGoals;
            if (SortData !== undefined) {
                SortData = [...SortData].sort((a, b) => {
                    return parseInt(a.goalPriority) > parseInt(b.goalPriority) ? 1 : -1;
                })
                setAllGoals(SortData);
            }

            setLoader(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
    }

    const goalDetails = async (goalId: any) => {
        try {
            setIandRcal(true);
            const enc: any = encryptData(goalId);

            let result = await fetchGoalDetails(enc);

            setgoalValue(result?.data?.Goal_Code);

            editObj = result?.data;
            setEditGoal(result?.data)



            setOtherIandR({
                Inflation: result?.data?.Inflation_Rate,
                ROR_Sip: result?.data?.ROR,
                RRR: result?.data?.RRR,
                //@ts-ignore
                Trxn_Type: result?.data?.Trxn_Type,
            })
            setGoalFind(editObj?.Goal_Name);
            setVerifyKYC(
                {
                    GoalsToFind: editObj?.Goal_Name,
                    PresentValue: editObj?.Present_Value,  //condition for retirement/FG9
                    FromDate: editObj?.StartDate,
                    ToDate: editObj?.EndDate,
                    Priority: editObj?.goalPriority,

                    TotalHouseCost: editObj?.Goal_Code === "FG4" ? (parseInt(editObj?.Present_Value) / parseInt(editObj?.Downpayment_HouseFund_Only)) * 100 : "",
                    downPaymentPer: editObj?.Goal_Code === "FG4" ? parseInt(editObj?.Downpayment_HouseFund_Only) : 0,
                    downCost: editObj?.Goal_Code === "FG4" ? editObj?.Present_Value : 0,

                    retirementAge: editObj?.RetirementAge,
                    currentAge: editObj?.CurrentAge_RetdFund,
                    lifeExpectancy: editObj?.lifeExpectancy_OnlyRetd,
                    expenseMonth: editObj?.Goal_Code === "FG9" ? editObj?.Present_Value : "",

                    childAge: editObj?.ChildAge,
                    childName: editObj?.ChildName,
                    gender: editObj?.ChildGender,

                    GoalName: "",//
                }
            )

            setIandRcal(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setIandRcal(false);
        }
    }

    const reset = () => {
        fetchAllGoalScheme();

        fetchGoalDD();
        setGoalFind('');
        setBtntext("Add")
        setDurationVal("");
        setgoalValue("");
        setTotHousecost("0")
        setCalcObj({});
        editObj = {};
        setIandRcal(false);
        setCreateSt(false);
        setOtherIandR([]);
    }


    const createAndCalc = async (data: any) => { /// method to calculate and create/update goal 
        const Data = data || {};

        let minDate = new Date(Data?.FromDate);
        let maxDate = new Date(Data?.ToDate);

        setDurationVal(goalValue === "FG9" ? Number(Data?.retirementAge) - Number(Data?.currentAge) : (maxDate.getFullYear() - minDate.getFullYear()));
        try {
            if (!createSt) {
                setIandRcal(true);
                const obj = {
                    GoalCode: goalValue,
                    Duration: goalValue === "FG9" ? Number(Data?.retirementAge) - Number(Data?.currentAge) : (maxDate.getFullYear() - minDate.getFullYear())
                }
                const enc: any = encryptData(obj);
                const result: any = await fetchGoalInflationAndRor(enc);

                if (result?.data[0] === undefined) {
                    setInfObj(otherIandR);
                    infObj = otherIandR

                } else {
                    setInfObj(result?.data[0])
                    infObj = result?.data[0]
                }

                calculateGoal(Data)
                setIandRcal(false);
                return false;
            } else {
                if (btntext === "Add") {
                    setIandRcal(true);
                    const obj: any = {
                        uid: "",
                        goal_Name: Data?.GoalName,//
                        goalcode: Data?.GoalsToFind,//
                        getLumpsum: "",//calcObj?.getLumpsum
                        getSip: calcObj?.getSip,//
                        investLumpsum: "",//calcObj?.investLumpsum
                        investSip: calcObj?.investSip,//
                        errorMsg: "",
                        house_eligible_amount: "",
                        annual_Income: "",//
                        present_Value: goalValue !== "FG9" ? goalValue !== "FG4" ? Data?.PresentValue : Data?.downCost : Data?.expenseMonth,//
                        monthly_Value: "0",//
                        // @ts-ignore
                        pmt: calcObj?.PMT,//
                        time_Horizon: calcObj.Time_Horizon,
                        trxn_Type: "S",
                        userID: local?.userid,
                        risk: "M",
                        createdByEmail: local?.RmEmail,
                        relation: "",
                        childName: Data?.childName,//
                        childAge: Data?.childAge,//
                        childGender: Data?.gender,
                        type: "",
                        otherGoal: Data?.GoalName,
                        goal_Code_Other: "",
                        startDate: Data?.FromDate,//
                        endDate: Data?.ToDate,//
                        inflation_Rate: infObjst.Inflation,//
                        ror: infObjst?.ROR_Sip,//
                        rrr: 0,//
                        downpayment_HouseFund_Only: parseInt(Data?.downPaymentPer),//
                        retirementAge: parseInt(Data?.retirementAge),//
                        lifeExpectancy_OnlyRetd: parseInt(Data?.lifeExpectancy),//
                        currentAge_RetdFund: Data?.currentAge === "" ? 0 : Data?.currentAge,
                        goalID: 0,
                        targetValue: goalValue === "FG4" ? calcObj?.TargetValue : "",
                        // @ts-ignore
                        typeCode: calcObj?.TypeCode,
                        goalPriority: Data?.Priority,//
                        isChildGoal: Data?.GoalsToFind === "FG12" || Data?.GoalsToFind === "FG13" ? true : false,
                        basicId: localUser,
                        empCode: null,
                        planId: localPlanId?.basicId,
                        isRecurring: 0
                    }


                    const enc: any = encryptData(obj);
                    const result: any = await SaveGoal(enc);

                    setIandRcal(false);
                    setOpenModal(false);
                    setCreateSt(false);
                    toastAlert("success", "Goal Successfully Added");
                    reset();
                    fetchAllGoalScheme();

                    fetchGoalDD();

                    return true;
                }
                else {
                    setIandRcal(true);
                    const obj: any =
                    {
                        annual_Income: "",
                        basicId: editGoal?.BasicId,
                        childName: editGoal?.ChildName,
                        childAge: Data?.childAge || editGoal?.childAge,
                        childGender: Data?.gender || editGoal?.gender,
                        createdByEmail: local?.RmEmail,
                        currentAge_RetdFund: editGoal?.CurrentAge_RetdFund,
                        downpayment_HouseFund_Only: parseInt(Data?.downPaymentPer),
                        empCode: local?.rmCode,
                        endDate: Data?.ToDate,
                        errorMsg: "",
                        getLumpsum: "0",//
                        getSip: calcObj?.getSip,
                        goalID: editGoal?.GoalID,
                        goalPriority: Data?.Priority,
                        goal_Code_Other: "",
                        goal_Name: editGoal?.Goal_Name,
                        goalcode: editGoal?.Goal_Code,
                        house_eligible_amount: "",
                        inflation_Rate: infObjst?.Inflation,
                        investLumpsum: "0",//
                        investSip: calcObj?.investSip,
                        isChildGoal: editGoal?.GoalsToFind === "FG12" || editGoal?.GoalsToFind === "FG13" ? true : false,
                        isRecurring: 0,
                        lifeExpectancy_OnlyRetd: parseInt(Data?.lifeExpectancy),
                        monthly_Value: "0",
                        otherGoal: "",
                        planId: localPlanId?.basicId,
                        pmt: calcObj?.PMT,
                        present_Value: goalValue !== "FG9" ? goalValue !== "FG4" ? Data?.PresentValue : Data?.downCost : Data?.expenseMonth,
                        relation: "",
                        retirementAge: parseInt(Data?.retirementAge),
                        risk: "M",
                        ror: infObjst?.ROR_Sip,
                        rrr: infObjst?.RRR,
                        startDate: Data?.FromDate,
                        targetValue: goalValue === "FG4" ? calcObj?.TargetValue : "",
                        time_Horizon: calcObj?.Time_Horizon,
                        trxn_Type: editGoal?.Trxn_Type,
                        type: "",
                        typeCode: calcObj?.TypeCode,
                        uid: "",
                        userID: local?.userid,
                    }
                    const enc: any = encryptData(obj);
                    const result: any = await EditGoal(enc);

                    setBtntext("Add")
                    setIandRcal(false);
                    setOpenModal(false);

                    setCreateSt(false);
                    toastAlert("success", "Goal Successfully Updated");
                    fetchAllGoalScheme();
                    reset();

                    fetchGoalDD();

                    return true;
                }
            }
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            // setBtntext("Add")
            setIandRcal(false);
            return false;
        }
    }

    const calculateGoal = async (data: any) => {
        const Data = data || {};

        try {
            let date, MinDate, MaxDate;
            date = new Date(new Date().setFullYear(new Date().getFullYear()));
            MinDate = (date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDate()).padStart(2, '0'));
            MaxDate = (date.getFullYear() + 1) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDate()).padStart(2, '0'));
            setIandRcal(true);
            const retireObj: any = {
                currentAge_RetdFund: Data?.currentAge,
                retirementAge: Data?.retirementAge == null ? "0" : Data?.retirementAge,
            }

            const houseObj: any = {
                downpayment_HouseFund_Only: parseInt(Data?.downPaymentPer),
                house_cost_Value: parseInt(Data?.downCost),
            }

            const otherObj: any = {
                goal_Code_Other: "",
                otherGoal: Data?.GoalName,
            }

            const taxSObj: any = {
                currentAge_RetdFund: editGoal?.CurrentAge_RetdFund,
                retirementAge: editGoal?.RetirementAge,
                time_Horizon: editGoal?.Time_Horizon,
            }
            const obj1: any = {
                basicId: localUser,//
                endDate: goalValue !== "FG9" ? Data?.ToDate : MaxDate,//
                goalPriority: Data?.Priority,//
                goal_Code: goalValue,//
                inflation_Rate: infObj?.Inflation,
                present_Value: goalValue !== "FG9" ? goalValue !== "FG4" ? Data?.PresentValue : parseInt(Data?.TotalHouseCost) : Data?.expenseMonth,//
                ror: infObj?.ROR_Sip,
                rrr: infObj?.RRR,
                startDate: goalValue !== "FG9" ? Data?.FromDate : MinDate,//
                trxn_Type: infObjst?.Trxn_Type !== undefined ? infObjst?.Trxn_Type : "S",
                typeCode: "",
            }
            let obj: any = obj1

            switch (goalValue) {
                case "FG9":
                    obj = {
                        ...obj,
                        ...retireObj
                    }
                    break;

                case "FG4":
                    obj = {
                        ...obj,
                        ...houseObj
                    }
                    setTotHousecost(Data?.TotalHouseCost);
                    break;

                case "FG14":
                    obj = {
                        ...obj,
                        ...otherObj
                    }
                    break;

                case "FG104":
                    obj = {
                        ...obj,
                        ...taxSObj
                    }
                    break;
            }


            const enc: any = encryptData(obj);

            let result: any = await goalCalculate(enc)

            setOpenModal(true);

            setCalcObj(result?.data);
            setIandRcal(false);

        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setIandRcal(false);
        }
    }


    const deleteSetectGoal = async () => {
        try {
            setIandRcal(true);
            const obj: any = {
                basicid: localUser,
                // @ts-ignore
                UserGoalId: fullGoal.goalId,
            }

            const enc: any = encryptData(obj);

            const result: any = await deleteGoal(enc);
            toastAlert("success", "Goal Deleted");
            setOpenDelModal(false);
            fetchAllGoalScheme();
            fetchGoalDD();
            setIandRcal(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setIandRcal(false);
        }
    }


    useEffect(() => {
        let obj = ObjectSchemaList.main;
        switch (goalFind) {
            case "House": obj = {
                ...obj,
                ...ObjectSchemaList.house,
            }
                break;

            case "Retirement": obj = {
                ...obj,
                ...ObjectSchemaList.Retire,
            }
                break;
            case "Child Study": obj = {
                ...obj,
                ...ObjectSchemaList.child,
                ...ObjectSchemaList.mandatory,
            }
                break;
            case "Child Wedding": obj = {
                ...obj,
                ...ObjectSchemaList.child,
                ...ObjectSchemaList.mandatory,
            }

                break;
            case "Other": obj = {
                ...obj,
                ...ObjectSchemaList.other,
                ...ObjectSchemaList.mandatory,
            }

                break;

            default: obj = {
                ...obj,
                ...ObjectSchemaList.mandatory,
            }
                break;
        }

        setValidationSchema(obj)

        return () => { }
    }, [goalFind])

    useEffect(() => {
        setChildList(basicDetail.filter((record) => record.RelationCode === "07" || record.RelationCode === "08"));

        return () => {

        }
    }, [basicDetail])

    useEffect(() => {
        (async () => {
            await fetchAllGoalScheme();
            await fetchGoalDD();
        })();
        for (let i = 60; i <= 90; i++) {
            options.push({ "id": "" + i + "", "name": "" + i + " Year" });
        }
        setRetireAgen(options);

    }, [])
    return (
        <>
            <Box className="row justify-content-center">
                <Box className="col-10">
                    <Formik
                        initialValues={verifyKYC}
                        validationSchema={Yup.object().shape(validationSchema)}
                        enableReinitialize
                        onSubmit={async (values, { resetForm }) => {

                            const check = await createAndCalc(values);
                            if (check) {
                                resetForm();
                                setVerifyKYC({
                                    GoalsToFind: "",
                                    PresentValue: "",
                                    FromDate: "",
                                    ToDate: "",
                                    Priority: "",

                                    TotalHouseCost: "",
                                    downPaymentPer: "0",
                                    downCost: "0",

                                    retirementAge: "0",
                                    currentAge: "",
                                    lifeExpectancy: "0",
                                    expenseMonth: "",

                                    childAge: "",
                                    childName: "",
                                    gender: "",

                                    GoalName: "",
                                })
                                //setOpenModal(false);
                            }
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            handleReset,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount
                        }) => (
                            <>
                                <Box className="row">
                                    <Box className="col-lg-4 col-md-5 col-sm-6 mt-1">
                                        {btntext === "Update" ? <>
                                            <Box>
                                                <Input
                                                    label="Goals To Find"
                                                    name="GoalsToFind"
                                                    className={`${styles.inputBorder}`}

                                                    disabled={true}
                                                    value={values.GoalsToFind}

                                                />
                                            </Box>
                                        </> : <>
                                            <Box>
                                                <SelectMenu
                                                    label="Goals To Find"
                                                    items={goalDD}
                                                    bindValue={"Value"}
                                                    bindName={"Text"}
                                                    name={"GoalsToFind"}
                                                    // className={`pt-1`}
                                                    error={submitCount ? errors.GoalsToFind : null}
                                                    value={values.GoalsToFind}
                                                    onChange={(e: any) => {
                                                        if (e?.Text === "Retirement") {
                                                            setFieldValue("retirementAge", "")
                                                        } else {
                                                            setFieldValue("retirementAge", "0")
                                                        }
                                                        setGoalFind(e?.Text);
                                                        setFieldValue("GoalsToFind", e?.Value);

                                                        setgoalValue(e?.Value)
                                                        setFieldValue("PresentValue", "");
                                                        setFieldValue("FromDate", "")
                                                        setFieldValue("ToDate", "")
                                                        setFieldValue("Priority", "")
                                                        setFieldValue("TotalHouseCost", "")
                                                        setFieldValue("downPaymentPer", "0")
                                                        setFieldValue("downCost", "0")
                                                        setTotHousecost("0")
                                                        setFieldValue("currentAge", "")
                                                        setFieldValue("lifeExpectancy", "0")
                                                        setFieldValue("expenseMonth", "")
                                                        setFieldValue("childAge", "")
                                                        setFieldValue("childName", "")
                                                        setFieldValue("gender", "")
                                                        setFieldValue("GoalName", "")
                                                        setIandRcal(false);
                                                        setCreateSt(false);
                                                        setCalcObj({});
                                                        editObj = {};
                                                        setOtherIandR([]);
                                                    }}
                                                    required
                                                />
                                            </Box>
                                        </>}
                                    </Box>
                                    {goalFind === "House" ? (
                                        <>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        label="Total House Cost"
                                                        name="TotalHouseCost"
                                                        placeholder="Enter Value"
                                                        className={`${styles.inputBorder}`}
                                                        error={submitCount ? errors.TotalHouseCost : null}
                                                        onChange={(e: any) => {
                                                            setFieldValue("TotalHouseCost", e?.target?.value.replace(/\D/g, ''));
                                                            setFieldValue("downCost", ((e?.target?.value.replace(/\D/g, '') * values.downPaymentPer) / 100));
                                                        }}
                                                        value={values?.TotalHouseCost}
                                                    />
                                                </Box>
                                            </Box>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        label="Downpayment(%)"
                                                        name="downPaymentPer"
                                                        error={submitCount ? errors.downPaymentPer : null}
                                                        className={`${styles.inputBorder}`}
                                                        maxLength={3}
                                                        onChange={(e: any) => {
                                                            setFieldValue("downPaymentPer", e?.target?.value.replace(/\D/g, ''));
                                                            setFieldValue("downCost", ((values.TotalHouseCost * e?.target?.value.replace(/\D/g, '')) / 100))
                                                        }}
                                                        value={values?.downPaymentPer}
                                                    />
                                                </Box>
                                            </Box>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        label="Downpayment Cost"
                                                        name="downCost"
                                                        error={submitCount ? errors.downCost : null}
                                                        onChange={handleChange}
                                                        value={values.downCost}
                                                        disabled={true}
                                                        className={`${styles.inputBorder}`}
                                                    />
                                                </Box>
                                            </Box>
                                        </>
                                    ) : (
                                        ""
                                    )}
                                    {goalFind === "Retirement" ? (
                                        <>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        label="Retirement Age"
                                                        name="retirementAge"
                                                        error={submitCount ? errors.retirementAge : null}
                                                        onChange={(e: any) => {
                                                            setFieldValue("retirementAge", (e?.target?.value.length > 2 ? e?.target?.value.slice(0, 2) : e?.target?.value.replace(/\D/g, '')).toString())
                                                        }}
                                                        className={`${styles.inputBorder}`}
                                                        value={values?.retirementAge}
                                                        required
                                                    />
                                                </Box>
                                            </Box>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        label="Current Age"
                                                        name="currentAge"
                                                        error={submitCount ? errors.currentAge : null}
                                                        onChange={(e: any) => {
                                                            setFieldValue("currentAge", (e?.target?.value.length > 2 ? e?.target?.value.slice(0, 2) : e?.target?.value.replace(/\D/g, '')).toString())
                                                        }}
                                                        className={`${styles.inputBorder}`}
                                                        value={values?.currentAge}
                                                        required
                                                    />
                                                </Box>
                                            </Box>
                                            <Box className="col-lg-4 col-md-5 col-sm-6 mt-1">
                                                <Box>
                                                    <SelectMenu
                                                        name="lifeExpectancy"
                                                        items={retireAgen}
                                                        // items={retireAge.map((record,index)=>{
                                                        //     return(<>id:{record?.id}, name:{record?.name}</>);
                                                        // })}
                                                        bindValue={"id"}
                                                        bindName={"name"}
                                                        label={"Life Expectancy"}
                                                        value={values?.lifeExpectancy}
                                                        onChange={(e: any) => {
                                                            setFieldValue("lifeExpectancy", e?.id);

                                                        }}
                                                        error={submitCount ? errors.lifeExpectancy : null}
                                                    />
                                                </Box>
                                            </Box>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        label="Expense (Per Month)"
                                                        name="expenseMonth"
                                                        placeholder="Enter Value"
                                                        className={`${styles.inputBorder}`}
                                                        error={submitCount ? errors.expenseMonth : null}
                                                        onChange={(e: any) => {
                                                            setFieldValue("expenseMonth", (e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, '')).toString())
                                                        }}
                                                        value={values?.expenseMonth}
                                                        required
                                                    />
                                                </Box>
                                            </Box>
                                        </>
                                    ) : (
                                        ""
                                    )}
                                    {goalFind === "Child Study" ||
                                        goalFind === "Child Wedding" ? (
                                        <>
                                            <Box className="col-lg-4 col-md-5 col-sm-6 mt-1">
                                                {btntext === "Update" ? <>
                                                    <Box>
                                                        <Input
                                                            label="Child Name"
                                                            name="childName"
                                                            className={`${styles.inputBorder}`}
                                                            error={submitCount ? errors.childName : null}
                                                            onChange={handleChange}
                                                            disabled={true}
                                                            value={values.childName}
                                                        />
                                                    </Box>
                                                </> : <>
                                                    <Box>
                                                        <SelectMenu
                                                            items={childList}
                                                            bindValue={"ClientName"}
                                                            bindName={"ClientName"}
                                                            label={"Child Name"}
                                                            value={values?.childName}
                                                            name={"childName"}

                                                            // value={filter}
                                                            onChange={(e: any) => {
                                                                setFieldValue("childName", e?.ClientName);

                                                                setFieldValue("childAge", e?.Age)
                                                                setFieldValue("gender", e?.Gender === "M" ? "001" : e?.Gender === "F" ? "002" : "003")
                                                            }}
                                                            required
                                                            error={submitCount ? errors.childName : null}
                                                        />
                                                    </Box>
                                                </>}
                                            </Box>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        label="Child Age(Yrs)"
                                                        name="childAge"
                                                        placeholder="Enter Age"
                                                        className={`${styles.inputBorder}`}
                                                        error={submitCount ? errors.childAge : null}
                                                        onChange={(e: any) => {
                                                            setFieldValue("childAge", (e?.target?.value.length > 2 ? e?.target?.value.slice(0, 2) : e?.target?.value.replace(/\D/g, '')).toString())
                                                        }}
                                                        value={values?.childAge}
                                                        required
                                                    // disabled={true}
                                                    />
                                                </Box>
                                            </Box>
                                            <Box className="col-lg-4 col-md-5 col-sm-6 mt-1">
                                                <Box>
                                                    <SelectMenu
                                                        name={"gender"}
                                                        items={[{ id: "001", name: "Male" },
                                                        { id: "002", name: "Female" },
                                                        { id: "003", name: "Other" }]}
                                                        bindValue={"id"}
                                                        bindName={"name"}
                                                        label={"Gender"}
                                                        value={values?.gender}
                                                        onChange={(e: any) => {
                                                            setFieldValue("gender", e?.id);
                                                        }}
                                                        // disabled={true}
                                                        required
                                                        error={submitCount ? errors.gender : null}
                                                    />
                                                </Box>
                                            </Box>
                                        </>
                                    ) : (
                                        ""
                                    )}
                                    {goalFind === "Other" ? (

                                        <Box className="col-lg-4 col-md-4 col-sm-12">
                                            <Label className="pt-1"
                                                style={{ marginBottom: 5, fontSize: '0.7rem', color: '#339cff', fontWeight: 600, letterSpacing: '0.1rem' }}>Goal Name<span style={{ color: "#e5484d" }}>&nbsp;*</span></Label><br></br>

                                            <Autocomplete
                                                getItemValue={(item: any) => item}
                                                items={goalNameList}
                                                menuStyle={{
                                                    borderRadius: '3px',
                                                    boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                                                    background: 'rgba(255, 255, 255, 0.9)',
                                                    padding: '15px, 0',
                                                    fontSize: '75%',
                                                    position: 'absolute',
                                                    overflow: 'auto',
                                                    zIndex: "1",
                                                    maxHeight: '25%', // TODO: don't cheat, let it flow to the bottom
                                                }}
                                                renderItem={(item: any, isHighlighted: any) =>
                                                    <div style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
                                                        {item}
                                                    </div>
                                                }
                                                value={values?.GoalName}
                                                onChange={(e: any) => {
                                                    setFieldValue("GoalName", e.target.value)
                                                    searchGoal(e.target.value);
                                                }}
                                                onSelect={(val: any) => { setFieldValue("GoalName", val); }}
                                            />

                                            {/* @ts-ignore */}
                                            {submitCount ? <Text size="h6" css={{ color: "#e5484d" }}>{errors.GoalName}</Text> : ""}
                                        </Box>
                                    ) : (
                                        ""
                                    )}
                                    {goalFind === "House" || goalFind === "Retirement" ? (
                                        ""
                                    ) : (
                                        <Box className="col-lg-4 col-md-5 col-sm-6">
                                            <Box>
                                                <Input
                                                    label="Present Value"
                                                    name="PresentValue"
                                                    className={`${styles.inputBorder}`}
                                                    error={submitCount ? errors.PresentValue : null}
                                                    onChange={(e: any) => {
                                                        //setFieldValue("PresentValue", e?.target?.value.replace(/\D/g, ''))
                                                        setFieldValue("PresentValue", e?.target?.value.length > 10 ? e?.target?.value.slice(0, 10) : e?.target?.value.replace(/\D/g, ''))


                                                    }}
                                                    value={values?.PresentValue}
                                                    required
                                                />
                                            </Box>
                                        </Box>
                                    )}
                                    {goalFind === "Retirement" ? (
                                        ""
                                    ) : (
                                        <>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        type="date"
                                                        label="From Date"
                                                        name="FromDate"
                                                        className={`${styles.inputBorder}`}
                                                        error={submitCount ? errors.FromDate : null}
                                                        onChange={(e: any) => {
                                                            setFieldValue("FromDate", e.target.value);
                                                            if (btntext === "Add") {
                                                                let date = new Date(new Date(e.target.value).setFullYear(new Date().getFullYear() + 1));
                                                                let NewDate = (date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDate()).padStart(2, '0'));

                                                                setToDate(NewDate);
                                                                setFieldValue("ToDate", NewDate);
                                                            }
                                                        }}
                                                        value={values?.FromDate}
                                                        required
                                                    />
                                                </Box>
                                            </Box>
                                            <Box className="col-lg-4 col-md-5 col-sm-6">
                                                <Box>
                                                    <Input
                                                        type="date"
                                                        name="ToDate"
                                                        label="To Date"
                                                        min={toDate}
                                                        className={`${styles.inputBorder}`}
                                                        error={submitCount ? errors.ToDate : null}
                                                        onChange={(e: any) => {
                                                            setFieldValue("ToDate", e.target.value);
                                                        }}
                                                        value={values?.ToDate}
                                                        required
                                                    />
                                                </Box>
                                            </Box>
                                        </>
                                    )}
                                    <Box className="col-lg-4 col-md-5 col-sm-6">
                                        <Box>
                                            <Input
                                                label="Priority"
                                                name="Priority"
                                                className={`${styles.inputBorder}`}
                                                error={submitCount ? errors.Priority : null}
                                                onChange={(e: any) => {
                                                    setFieldValue("Priority", e?.target?.value.length > 2 ? e?.target?.value.slice(0, 2) : e?.target?.value.replace(/\D/g, ''))
                                                 }}
                                                value={values?.Priority}
                                                required
                                            />
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="row justify-content-end">
                                    <Box className="col-auto">
                                        <Button
                                            className={`col-auto  py-1 px-4 mx-0 ${styles.button}`}
                                            color="yellowGroup"
                                            size="md"
                                            onClick={() => {
                                                handleReset();
                                                reset();
                                                setVerifyKYC({
                                                    GoalsToFind: "",
                                                    PresentValue: "",
                                                    FromDate: "",
                                                    ToDate: "",
                                                    Priority: "",

                                                    TotalHouseCost: "",
                                                    downPaymentPer: "0",
                                                    downCost: "0",

                                                    retirementAge: "0",
                                                    currentAge: "",
                                                    lifeExpectancy: "0",
                                                    expenseMonth: "",

                                                    childAge: "",
                                                    childName: "",
                                                    gender: "",

                                                    GoalName: "",
                                                })
                                                setIandRcal(false);
                                                setCreateSt(false);
                                                setCalcObj({});
                                                editObj = {};
                                                setOtherIandR([]);
                                            }}
                                        >
                                            <Text
                                                //@ts-ignore
                                                weight="normal"
                                                size="h6"
                                                //@ts-ignore
                                                color="gray8"
                                                className={styles.button}
                                            >
                                                Reset
                                            </Text>
                                        </Button>&nbsp;
                                        <Button
                                            className={`col-auto  py-1 px-4 mx-0 ${styles.button}`}
                                            color="yellowGroup"
                                            size="md"
                                            onClick={() => { handleSubmit() }}
                                            loading={load}
                                        >
                                            <Text
                                                //@ts-ignore
                                                weight="normal"
                                                size="h6"
                                                //@ts-ignore
                                                color="gray8"
                                                className={styles.button}
                                            >
                                                {btntext}
                                            </Text>
                                        </Button>
                                    </Box>
                                </Box>
                                <DialogModal
                                    open={openModal}
                                    setOpen={setOpenModal}
                                    css={{
                                        "@bp0": { width: "90%" },
                                        "@bp1": { width: "50%" },
                                    }} >
                                    <>
                                        <Box>
                                            <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                                                {/* @ts-ignore */}
                                                <Text css={{ color: "var(--colors-blue1)" }}>Future Value Of Your Goal</Text>
                                            </Box>
                                            <Box className={`modal-body p-3`} >
                                                <Box className="row m-1 py-1 justify-content-between">
                                                    <Box className="col-auto"><Text size="h5">Goal:</Text></Box>
                                                    <Box className="col-auto"><Text size="h5">{IandRcal ? <><Loader /></> : goalFind}</Text></Box>
                                                </Box>
                                                {goalValue === "FG4" ? <>
                                                    <Box className="row m-1 py-1 justify-content-between">
                                                        <Box className="col-auto"><Text size="h5">Total House Cost:</Text></Box>
                                                        {/* @ts-ignore */}
                                                        <Box className="col-auto"><Text size="h5">{IandRcal ? <><Loader /></> : totHousecost}</Text></Box>
                                                    </Box>
                                                </> : <></>}
                                                <Box className="row m-1 py-1 justify-content-between">

                                                    <Box className="col-auto"><Text size="h5">Present Value:</Text></Box>
                                                    {/* @ts-ignore */}
                                                    <Box className="col-auto text-end"><Text size="h5">
                                                        {goalValue === "FG4" ? <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Downpayment: </Text> : <></>}
                                                        {IandRcal ? <><Loader /></> : calcObj.Present_Value}</Text></Box>
                                                </Box>
                                                <Box className="row m-1 py-1 justify-content-between">
                                                    <Box className="col-auto"><Text size="h5">PMT:</Text></Box>
                                                    {/* @ts-ignore */}
                                                    <Box className="col-auto"><Text size="h5">{IandRcal ? <><Loader /></> : calcObj.PMT}</Text></Box>
                                                </Box>
                                                {goalValue === "FG4" ? <>
                                                    <Box className="row m-1 py-1 justify-content-between">
                                                        <Box className="col-auto"><Text size="h5">Total House Cost After {durationVal} Year</Text></Box>
                                                        {/* @ts-ignore */}
                                                        <Box className="col-auto"><Text size="h5">{IandRcal ? <><Loader /></> : calcObj.getSip}</Text></Box>
                                                    </Box>
                                                </> : <></>}
                                                <Box className="row m-1 py-1 justify-content-between">
                                                    <Box className="col-auto"><Text size="h5">Target Value After {calcObj?.Time_Horizon} Year:</Text></Box>
                                                    {/* @ts-ignore */}
                                                    <Box className="col-auto text-end"><Text size="h5">
                                                        {goalValue === "FG4" ?
                                                            <>
                                                                <Text size="h6" css={{ color: "var(--colors-blue1)" }}>Downpayment: </Text>
                                                                {IandRcal ? <><Loader /></> : calcObj.TargetValue}
                                                            </>
                                                            : <>{IandRcal ? <><Loader /></> : calcObj.getSip}</>}</Text>

                                                    </Box>
                                                </Box>
                                                <Box className="row m-1 py-2 justify-content-between" css={{ background: "#b3daff", borderRadius: "10px" }}>
                                                    <Box className="col-6">
                                                        <Text size="h5">
                                                            Investment Amount With SIP
                                                            You Should Start:
                                                        </Text>
                                                    </Box>
                                                    {/* @ts-ignore */}
                                                    <Box className="col-auto"><Text size="h5">{IandRcal ? <><Loader /></> : calcObj.investSip}</Text></Box>
                                                </Box>
                                                <Box className="row m-1 py-1 justify-content-end gx-1">
                                                    <Box className="col-auto">
                                                        <Button
                                                            className={`col-auto  py-2 px-4 mx-0 ${styles.button}`}
                                                            color="yellowGroup"
                                                            size="md"
                                                            // onClick={handleSubmit}
                                                            // onClick={handleSubmit}//setOpenDialog(true) 
                                                            onClick={() => { setOpenModal(false) }}
                                                            loading={load}
                                                        >
                                                            <Text
                                                                //@ts-ignore
                                                                weight="normal"
                                                                size="h6"
                                                                //@ts-ignore
                                                                color="gray8"
                                                                className={styles.button}
                                                            >
                                                                Cancel
                                                            </Text>
                                                        </Button>
                                                    </Box>
                                                    <Box className="col-auto">
                                                        <Button
                                                            className={`col-auto  py-2 px-4 mx-0 ${styles.button}`}
                                                            color="yellowGroup"
                                                            size="md"
                                                            onClick={() => {
                                                                setCreateSt(true);
                                                                handleSubmit()
                                                            }}
                                                            // onClick={handleSubmit}//setOpenDialog(true) 
                                                            loading={load}
                                                        >
                                                            <Text
                                                                //@ts-ignore
                                                                size="h6"
                                                                //@ts-ignore
                                                                color="gray8"
                                                                className={styles.button}
                                                            >
                                                                Confirm and Save
                                                            </Text>
                                                        </Button>
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </Box>
                                    </>
                                    {/* <UnMappedMember /> */}
                                </DialogModal>
                            </>
                        )}
                    </Formik>
                </Box>
            </Box>
            <hr className="text-primary mx-0" />

            <Box className="text-center m-3">
                {/* @ts-ignore */}
                <Text size="h4" css={{ color: "var(--colors-blue1)" }}>
                    List Of Goals {allGoals?.length}
                </Text>
                {/* Un-Mapped Scheme */}
            </Box>
            <Box className="row ">
                <Box
                    className="col-auto col-lg-12 col-md-12 col-sm-12"
                    css={{ overflowX: "auto" }}
                >
                    <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                        <Table
                            className="text-capitalize table table-striped"
                        // css={{ color: "var(--colors-blue1)" }}
                        >
                            <ColumnParent className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                <ColumnRow>
                                    {/* <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>
                                        <Box class="form-check form-check-inline">
                                            
                                            <label className="form-check-label me-2" for="inlineCheckbox2">Select All</label>
                                            <input className="form-check-input mt-1" type="checkbox" id="inlineCheckbox2" value="option2" />
                                        </Box>
                                    </Text>
                                    </Column> */}
                                    <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Goal Name</Text></Column>
                                    <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Present Value</Text></Column>
                                    <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Inflation(%)</Text></Column>
                                    <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Tenure</Text></Column>
                                    <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Type</Text></Column>
                                    <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Future Value</Text></Column>
                                    <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Priority</Text></Column>
                                    <Column className="pb-4"><Text css={{ fontWeight: "normal" }}>Action</Text></Column>
                                </ColumnRow>
                            </ColumnParent>
                            <DataParent>
                                {allGoals?.length === 0 ? <DataRow>
                                    <DataCell colSpan={9} className="text-center">No Goals Found </DataCell>
                                </DataRow> : <>
                                    {loading ? <>
                                        <DataCell colSpan={9} className="text-center">
                                            <Loader />
                                        </DataCell>
                                    </> : <>
                                        {allGoals?.map((record: any, index: any) => {
                                            return (
                                                <DataRow className="text-center">
                                                    {/* <DataCell css={{ width: "130px" }}><Input type="checkbox" /></DataCell> */}
                                                    <DataCell>
                                                        {record?.goalCode === "FG4" ? <>
                                                            <Text size="h5">{record?.goalName}</Text>
                                                            <Text size="h6">(Total Cost)</Text>

                                                            <Text size="h5"> {Number((record?.currentCost / record?.downPaymentRate) * 100).toLocaleString(
                                                                "en-IN"
                                                            ) || "0.0"}</Text>
                                                        </>
                                                            : <Text size="h5">{record?.goalName}</Text>}
                                                    </DataCell>

                                                    <DataCell className="text-down">
                                                        {record?.goalCode === "FG4" ? <>
                                                            <Text>&nbsp;</Text>
                                                            <Text size="h6">(Downpayment)</Text>
                                                            {Number(record?.currentCost).toLocaleString(
                                                                "en-IN"
                                                            ) || "0.0"}
                                                        </>
                                                            : <> {Number(record?.currentCost).toLocaleString(
                                                                "en-IN"
                                                            ) || "0.0"}</>}
                                                    </DataCell>

                                                    <DataCell className="text-center">{record?.goalCode === "FG4" ? <><Text>&nbsp;</Text>{record?.inflationRate * 100}%</> : <>{record?.inflationRate * 100} %</>}</DataCell>
                                                    <DataCell className="text-center">{record?.goalCode === "FG4" ? <><Text>&nbsp;</Text>{record?.duration}</> : <>{record?.duration}</>}</DataCell>
                                                    <DataCell className="text-center">{record?.goalCode === "FG4" ? <><Text>&nbsp;</Text>{record?.investmentType === "S" ? "SIP" : "Lumpsum"}</> : <>{record?.investmentType === "S" ? "SIP" : "Lumpsum"}</>}</DataCell>
                                                    <DataCell>
                                                        {record?.goalCode === "FG4" ? <>
                                                            <Text>&nbsp;</Text>
                                                            <Text size="h6">(Downpayment)</Text>
                                                            {Number(record?.netDeficit).toLocaleString(
                                                                "en-IN"
                                                            ) || "0.0"}
                                                        </>
                                                            : <> {Number(record?.netDeficit).toLocaleString(
                                                                "en-IN"
                                                            ) || "0.0"}</>}


                                                        {/* {record?.netDeficit} */}
                                                    </DataCell>
                                                    <DataCell>{record?.goalCode === "FG4" ? <><Text>&nbsp;</Text>{record?.goalPriority}</> : <>{record?.goalPriority}</>}</DataCell>
                                                    <DataCell>
                                                        <Box className="row justify-content-center">

                                                            <Box className="col-auto" css={{ cursor: "pointer" }} onClick={() => { setBtntext("Update"); goalDetails(record?.goalId) }}>
                                                                <PencilFill />
                                                            </Box>
                                                            &nbsp;&nbsp;
                                                            <Box className="col-auto" css={{ cursor: "pointer" }} onClick={() => { setOpenDelModal(true); setFullGoal(record) }}>
                                                                <DeleteIcon />
                                                            </Box>
                                                        </Box>
                                                    </DataCell>
                                                </DataRow>
                                            );
                                        })}
                                    </>}
                                </>}
                            </DataParent>
                        </Table>
                    </Box>
                </Box>
            </Box>

            {/* <AlertDialog
                open={openDialog}
                setOpen={setOpenDialog}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "40%" },
                }}
            >
                <PopMessagePage />
            </AlertDialog> */}

            <DialogModal
                open={openDelModal}
                setOpen={setOpenDelModal}
                css={{
                    "@bp0": { width: "90%" },
                    "@bp1": { width: "50%" },
                }}
            >
                <Box className="col-md-12 col-sm-12 col-lg-12">
                    <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                        <Box className="row">
                            <Box className="col-auto"><Text css={{ color: "var(--colors-blue1)" }}>Delete Goal Warning</Text></Box>
                            <Box className="col"></Box>
                            <Box className="col-auto"></Box>
                        </Box>
                    </Box>
                    <Box
                        className="text-center modal-body p-3"
                    >

                        <Text css={{ mt: 20, pt: 10 }}>
                            {/* @ts-ignore */}
                            Goal Name: <b>{fullGoal.goalName}</b><br />
                            Are you sure to delete this Goal?

                        </Text>
                        <Box css={{ mt: 20, pt: 10 }} className="text-end">
                            <Button
                                type="submit"
                                color="yellow"
                                onClick={() => { setOpenDelModal(false) }}>
                                Cancel
                            </Button>
                            <Button
                                type="submit"
                                color="yellow"
                                //@ts-ignore
                                onClick={() => { deleteSetectGoal(); reset() }}
                                loading={loading}
                            >
                                Delete Now
                            </Button>
                        </Box>
                    </Box>
                </Box>

            </DialogModal>
        </>

    )
}

export default Goals