import { calculateAge } from "App/utils/helpers";
import * as Yup from "yup";

const checkAge = (age: string) => {
    return calculateAge(age)?.years < 18;
};



export const SCHEMAS: any = {
    children: {
        childrenList: Yup.array().of(
            Yup.object().shape({
                Full_Name_Child: Yup.string()
                    .required("Full Name is required")
                    .trim()
                    .matches(/^\s*[\S]/gms, "Invalid Full Name"),

                DateOfBirth_Child: Yup.string()
                    .trim(),

                Age_Child: Yup.string()
                    .required("Age is required")
                    .trim(),
                gender_child: Yup.string(),
                isDependChild: Yup.string(),
            })
        )
    },
    dependant: {
        dependantList: Yup.array().of(
            Yup.object().shape({
                Full_Name_Dep: Yup.string()
                    .required("Full Name is required")
                    .trim()
                    .matches(/^\s*[\S]/gms, "Invalid Full Name"),

                DateOfBirth_Dep: Yup.string()
                    .trim(),

                Age_Dep: Yup.string()
                    .required("Age is required")
                    .trim(),
                relation: Yup.string(),
            })
        )
    },
}