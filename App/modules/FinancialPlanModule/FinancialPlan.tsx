import React, { useEffect, useState } from 'react'
import Box from '@ui/Box/Box'
import Layout, { toastAlert } from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import FinancialPlanPage from './components/FinancialPlanPage'
import useFinancialPlanStore from './store'
import { encryptData, getUser } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";
import FullFinancialPlanPage from '../FullFinancialPlanModule/components/FullFinancialPlanPage';
import useHeaderStore from "App/shared/components/Header/store";
import { Button } from "@ui/Button/Button";
import ThankYou from './components/Forms/ThankYou'
import DialogModal from "@ui/ModalDialog/ModalDialog";
import Input from "@ui/Input";
import Text from "@ui/Text/Text";
import {
  getDropdownDataAll,
  getDropDownInsurance,
  getDropdownDataLN,
  getDropdownDataOT,
  getRecommendedMemberList,
  getInsuranceUnmapped,
  getFPPlanId,
  createNewPlan
} from "App/api/financialPlan"

// const local: any = getUser();

const FinancialPlan = () => {
  const {
    localUser,
    localPlanId,
    setLoader,
    setLocalUser,
    setLocalPlanId,
    setLocalRmcode,

    setRelationList,
    setDepRelationList,
    setInflowList,
    setFrequencyList,
    setOutflowList,

    setINDropdownList,
    setInsurancePolicyList,
    setinsurancePolicySelectedList,
    setLNDropdownList,
    setDGSDropdownList,

    setRecommendedMem,

    setBasicDetail,
  } = useFinancialPlanStore();
  const {
    userInfo
  } = useHeaderStore();
  const [local, setLocal] = useState<any>(userInfo || getUser());
  const [planShow, setPlanShow] = useState<boolean>(true);
  const [planId, setPlanId] = useState<any>("0");
  const [showCreatePlan, setShowCreatePlan] = useState<any>(false);
  const [planName, setPlanName] = useState<any>('')
  const [loading, setLoading] = useState<boolean>(false);
  const [errorPlan, setErrorPlan] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [errorMsg, setErrorMsg] = useState<String>('Plan id seen');

  const fetchPlanId = async () => {
    try {
      // setPlanShow(true);
      setLoader(true);
      const enc: any = encryptData(local?.basicid, true);
      let result: any = await getFPPlanId(enc);
      // console.log("planid");
      // console.log(result?.data);
      if (result?.data === "0") {
        setShowCreatePlan(true);
      }
      setPlanId(result?.data);
      setLocalPlanId(result?.data);
      setLoader(false);
      setPlanShow(false);
    } catch (error) {
      console.log(error);

      toastAlert("error", error);
      setLoader(false);
    }
  }

  const fetchDropDownall = async () => {
    try {
      setLoader(true);
      let result: any = await getDropdownDataAll();
      // console.log(result?.data);
      setLoader(false);

      const mainList: any[] = result?.data || []; // 4 items
      let dpData: any = {};
      if (mainList.length) {
        dpData = {
          [mainList[0][0].Type]: mainList[0],
          [mainList[1][0].Type]: mainList[1],
          [mainList[2][0].Type]: mainList[2],
          [mainList[3][0].Type]: mainList[3],
        };
      }
      setRelationList(dpData.Relation);
      // console.log(dpData.Relation, "dpData.Relation");

      dpData.Relation = dpData.Relation.reduce((a: any, e: any, i: any) => {
        if (!["01", "02", "07", "08", "013"].includes(e.code)) { a.push(e); } return a;
      }, []);

      // console.log(dpData.Relation, "dpData.Relation new")
      setDepRelationList(dpData.Relation)

      setInflowList(dpData.Inflow);
      setFrequencyList(dpData.Frequency);
      setOutflowList(dpData.Outflow);
      // console.log(dpData);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  }

  const fetchDropDown = async () => {
    try {
      setLoader(true);
      let result: any = await getDropDownInsurance();

      setINDropdownList(result?.data?.asset_list);
      setInsurancePolicyList(result?.data?.policy_list);

      let resultLN: any = await getDropdownDataLN();
      // console.log(resultLN?.data);
      setLNDropdownList(resultLN?.data);

      let resultDGS: any = await getDropdownDataOT();
      // console.log(resultDGS?.data);
      setDGSDropdownList(resultDGS?.data);

      setLoader(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  }

  const fetchRecommendedList = async () => {
    try {
      setLoader(true);
      const obj: any = {
        basicId: local?.basicid,
        Req_Type: "1"
      }
      // console.log(local.basicid);
      const enc: any = encryptData(obj);
      let result: any = await getRecommendedMemberList(enc);
      // console.log(result?.data, "recommended list");
      setRecommendedMem(result?.data?.recommended_Member)
      setLoader(false);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  }

  useEffect(() => {
    (async () => {
      // console.log(local);
      setLocalUser(local?.basicid);
      setLocalRmcode(local?.rmCode);
      await fetchPlanId();
      await fetchDropDownall();
      await fetchDropDown();
      await fetchRecommendedList();
      // console.log(local);
    })();

    return () => { }
  }, [local?.basicid])


  const CreatePlan = async () => {
    console.log(local)
    if (planName === "" || planName === " ") {
      setErrorPlan(true);
      return;
    }
    else {
      try {
        setLoading(true);
        setErrorPlan(false);
        const obj: any =
        {
          "basicId": local?.basicid,
          "planName": planName,
          "empId": local?.rmCode,
          "createdBy": local?.rmCode
        };
        const enc: any = encryptData(obj);

        let result: any = await createNewPlan(enc);
        console.log(result);
        if (result.status === "Success") {
          setLoading(false);
          setShowCreatePlan(false);
          setErrorPlan(false);
          setError(false);
          toastAlert("success", "Plan Created Successfully");
          fetchPlanId();
        }
        else {
          setLoading(false);
          setError(true);
          setErrorMsg(result?.msg);
        }
      } catch (error: any) {
        console.log(error);
        toastAlert("error", error);
        setLoading(false);
      }
    }

  }
  return (
    <>
      {planShow ?
        <Loader />
        :
        planId === "0" ?
          <Layout>
            <PageHeader title="Financial Plan" rightContent={<></>} />
            <Box className="row justify-content-center">
              <Box className="col-12 col-sm-12 col-md-11 col-lg-10 pt-2" css={{ background: "#fff" }} >
              </Box>
            </Box>
          </Layout>

          : planId?.isFilledByClient === "" || planId?.isFilledByClient === "N" ?
            <Layout>
              <PageHeader title="Financial Plan" rightContent={<></>} />
              <Box className="row justify-content-center">
                <Box className="col-12 col-sm-12 col-md-11 col-lg-10 pt-2" css={{ background: "#fff" }} >
                  <FinancialPlanPage fetchPlanId={fetchPlanId} />
                </Box>
              </Box>
            </Layout>
            :
            planId?.isFilledByClient === "Y" ?
              <Layout>
                <PageHeader title="Full Financial Plan"
                  rightContent={
                    <>
                      {/* <Button
                        color="yellow">
                        Execute My Plan
                      </Button> */}
                    </>
                  } />
                <FullFinancialPlanPage />
              </Layout>

              : planId?.isFilledByClient === "F" ?
                <Layout>
                  <PageHeader title="Financial Plan" rightContent={<></>} />
                  <Box className="row justify-content-center">
                    <Box className="col-12 col-sm-12 col-md-11 col-lg-10 pt-2" css={{ background: "#fff" }} >
                      <ThankYou />
                    </Box>
                  </Box>
                </Layout>
                : ""

      }
      <DialogModal
        open={showCreatePlan}
        setOpen={setShowCreatePlan}
        css={{
          "@bp0": { width: "40%" },
          "@bp1": { width: "30%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            <Box className="row">
              <Box className="col-auto"><Text css={{ color: "var(--colors-blue1)" }}>Create New Financial Plan</Text></Box>
              <Box className="col"></Box>
              <Box className="col-auto"></Box>
            </Box>
          </Box>
          <Box
            className="text-center modal-body p-3 modal-body-scroll"
            css={{ padding: "0.5rem" }}
          >
            <Box className="row" css={{ ml: 10 }}>
              <Box className="col-lg-10 col-md-10 col-sm-12">
                {error ?
                  <Box>
                    <Text size="h6" css={{ color: "red" }}>{errorMsg}</Text>
                  </Box>
                  : null}
                <Box>
                  <Input label="Plan Name" name="plan" placeholder="Enter Plan Name"
                    required
                    error={errorPlan ? "Please Enter Plan Name to proceed" : null}
                    onChange={(e: any) => { setPlanName(e.target.value) }}
                    value={planName} />
                </Box>
              </Box>
            </Box>
            <Box css={{ mt: 20, pt: 10, mb: 20, mr: 20 }} className="text-end">
              <Button
                type="submit"
                color="yellow"
                onClick={() => { setError(false); setErrorPlan(false); setShowCreatePlan(false) }}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={() => { CreatePlan() }}
                loading={loading}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </Box>
      </DialogModal>
    </>

  )
}

export default FinancialPlan