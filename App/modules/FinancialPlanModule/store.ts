import { Payload } from "echarts";
import create from "zustand";

import {
    MasterDropDownAll, MasterDropDown, PolicyList, IncomeDetails, RecommendedMemType,
    basicDetailList, ExpenseDetails, NetAssetsTypes, GoalSchemeUATType, InflationAndRor, goalDropDown, retire,
    PolicyNameList
} from "./FinancialPlanTypes"


interface StoreTypes {
    loading: boolean;
    localUser: string;
    localPlanId: any;
    localRmcode: string;
    retireAge: retire[];
    childList: any[]

    relationList: MasterDropDownAll[];
    depRelationList: MasterDropDownAll[];
    inflowList: MasterDropDownAll[];
    frequencyList: MasterDropDownAll[];
    outflowList: MasterDropDownAll[];

    lnDropdownList: MasterDropDown[];
    dgsDropdownList: MasterDropDown[];
    inDropdownList: MasterDropDown[];

    insurancePolicyList: PolicyList[];
    insurancePolicySelectedList: PolicyList[];
    policyNameList: PolicyNameList[];

    recommendedMem: any[];
    unMappedMF: any[];
    unMappedOtherAsset: any[];
    unMappedLoan: any[];
    unMappedInsurance: any[];
    basicDetail: basicDetailList[];
    incomeDetailsList: IncomeDetails[];
    expenseDetailsList: ExpenseDetails[];
    // NetAssetsTypes

    ulipDetailsList: NetAssetsTypes[];
    loanList: NetAssetsTypes[];
    insuranceList: NetAssetsTypes[];
    mfList: NetAssetsTypes[];
    otList: NetAssetsTypes[];

    allGoals: GoalSchemeUATType[];
    inflationAndRor: InflationAndRor[];

    goalDD: goalDropDown[];

    setLoader: (payload: boolean) => void;
    setLocalUser: (payload: string) => void;
    setLocalPlanId: (payload: any) => void;
    setLocalRmcode: (payload: string) => void;
    setRetireAge: (payload: retire[]) => void;

    setChildList: (payload: any[]) => void;

    setRelationList: (payload: MasterDropDownAll[]) => void;
    setDepRelationList: (payload: MasterDropDownAll[]) => void;
    setInflowList: (payload: MasterDropDownAll[]) => void;
    setFrequencyList: (payload: MasterDropDownAll[]) => void;
    setOutflowList: (payload: MasterDropDownAll[]) => void;


    setLNDropdownList: (payload: MasterDropDown[]) => void;
    setDGSDropdownList: (payload: MasterDropDown[]) => void;
    setINDropdownList: (payload: MasterDropDown[]) => void;

    setInsurancePolicyList: (payload: PolicyList[]) => void;
    setinsurancePolicySelectedList: (payload: PolicyList[]) => void;
    setPolicyNameList: (payload: PolicyNameList[]) => void;

    setRecommendedMem: (payload: any[]) => void;
    setBasicDetail: (payload: basicDetailList[]) => void;

    setUnmappedMutualFund: (payload: any[]) => void;
    setUnmappedInsurance: (payload: any[]) => void;
    setUnmappedOtherAsset: (payload: any[]) => void;
    setUnmappedLoan: (payload: any[]) => void;


    setIncomeDetailsList: (payload: IncomeDetails[]) => void;

    setAllGoals: (payload: GoalSchemeUATType[]) => void;
    setInflationAndRor: (payload: InflationAndRor[]) => void;

    setGoalDD: (payload: goalDropDown[]) => void;
    setExpenseDetailsList: (payload: ExpenseDetails[]) => void;

    //setOtherAssetsDetailsList: (payload: NetAssetsTypes[]) => void;
    setUlipDetailsList: (payload: NetAssetsTypes[]) => void;
    setLoanList: (payload: NetAssetsTypes[]) => void;
    setInsuranceList: (payload: NetAssetsTypes[]) => void;
    setMfList: (payload: NetAssetsTypes[]) => void;
    setOtList: (payload: NetAssetsTypes[]) => void;
}

const useFinancialPlanStore = create<StoreTypes>((set) => ({
    loading: false,
    localUser: "",
    localPlanId: {},
    localRmcode: "",
    retireAge: [],

    childList: [],

    relationList: [],
    depRelationList: [],
    inflowList: [],
    frequencyList: [],
    outflowList: [],

    lnDropdownList: [],
    dgsDropdownList: [],
    inDropdownList: [],

    insurancePolicyList: [],
    insurancePolicySelectedList: [],
    policyNameList: [],

    recommendedMem: [],
    unMappedMF: [],
    unMappedOtherAsset: [],
    unMappedLoan: [],
    unMappedInsurance: [],
    basicDetail: [],
    incomeDetailsList: [],
    expenseDetailsList: [],

    allGoals: [],
    inflationAndRor: [],
    goalDD: [],

    ulipDetailsList: [],
    loanList: [],
    insuranceList: [],
    mfList: [],
    otList: [],

    setLoader: (payload) =>
        set((state) => ({
            ...state,
            loading: payload,
        })),

    setLocalUser: (payload) =>
        set((state) => ({
            ...state,
            localUser: payload,
        })),

    setLocalPlanId: (payload) =>
        set((state) => ({
            ...state,
            localPlanId: payload,
        })),

    setLocalRmcode: (payload) =>
        set((state) => ({
            ...state,
            localRmcode: payload,
        })),

    setRetireAge: (payload) =>
        set((state) => ({
            ...state,
            retireAge: payload,
        })),

    setChildList: (payload) =>
        set((state) => ({
            ...state,
            childList: payload,
        })),

    setRelationList: (payload) =>
        set((state) => ({
            ...state,
            relationList: payload || [],
        })),
        
    setDepRelationList: (payload) =>
        set((state) => ({
            ...state,
            depRelationList: payload || [],
        })),

    setInflowList: (payload) =>
        set((state) => ({
            ...state,
            inflowList: payload || [],
        })),

    setFrequencyList: (payload) =>
        set((state) => ({
            ...state,
            frequencyList: payload || [],
        })),

    setOutflowList: (payload) =>
        set((state) => ({
            ...state,
            outflowList: payload || [],
        })),

    setLNDropdownList: (payload) =>
        set((state) => ({
            ...state,
            lnDropdownList: payload || [],
        })),

    setDGSDropdownList: (payload) =>
        set((state) => ({
            ...state,
            dgsDropdownList: payload || [],
        })),

    setINDropdownList: (payload) =>
        set((state) => ({
            ...state,
            inDropdownList: payload,
        })),

    setInsurancePolicyList: (payload) =>
        set((state) => ({
            ...state,
            insurancePolicyList: payload || [],
        })),

    setinsurancePolicySelectedList: (payload) =>
        set((state) => ({
            ...state,
            insurancePolicySelectedList: payload || [],
        })),

    setPolicyNameList: (payload) =>
        set((state) => ({
            ...state,
            policyNameList: payload || [],
        })),

    setRecommendedMem: (payload) =>
        set((state) => ({
            ...state,
            recommendedMem: payload || [],
        })),

    setUnmappedInsurance: (payload) =>
        set((state) => ({
            ...state,
            unMappedInsurance: payload || [],
        })),

    setUnmappedMutualFund: (payload) =>
        set((state) => ({
            ...state,
            unMappedMF: payload || [],
        })),

    setUnmappedOtherAsset: (payload) =>
        set((state) => ({
            ...state,
            unMappedOtherAsset: payload || [],
        })),

    setUnmappedLoan: (payload) =>
        set((state) => ({
            ...state,
            unMappedLoan: payload || [],
        })),

    setBasicDetail: (payload) =>
        set((state) => ({
            ...state,
            basicDetail: payload || [],
        })),

    setIncomeDetailsList: (payload) =>
        set((state) => ({
            ...state,
            incomeDetailsList: payload,
        })),

    setAllGoals: (payload) =>
        set((state) => ({
            ...state,
            allGoals: payload,
        })),

    setInflationAndRor: (payload) =>
        set((state) => ({
            ...state,
            inflationAndRor: payload,
        })),

    setGoalDD: (payload) =>
        set((state) => ({
            ...state,
            goalDD: payload,
        })),
    setExpenseDetailsList: (payload) =>
        set((state) => ({
            ...state,
            expenseDetailsList: payload,
        })),

    setUlipDetailsList: (payload) =>
        set((state) => ({
            ...state,
            ulipDetailsList: payload,
        })),

    setLoanList: (payload) =>
        set((state) => ({
            ...state,
            loanList: payload,
        })),

    setInsuranceList: (payload) =>
        set((state) => ({
            ...state,
            insuranceList: payload,
        })),

    setMfList: (payload) =>
        set((state) => ({
            ...state,
            mfList: payload,
        })),

    setOtList: (payload) =>
        set((state) => ({
            ...state,
            otList: payload,
        })),
}))

export default useFinancialPlanStore;