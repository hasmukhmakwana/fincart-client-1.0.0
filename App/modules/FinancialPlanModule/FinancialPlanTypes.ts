import { type } from "os";

export type MasterDropDownAll = {
    ID: string;
    Type: string;
    Type_name: string;
    code: string;
    subType: string;
}

export type MasterDropDown = {
    AssetName: string;
    Asset_Id: string;
    Asset_ShortCode: string;
    Category: string;
}

export type PolicyList = {
    PartnerId: string;
    Partner_Name: string;
    AssetType_Id: string;
}

export type PolicyNameList = {
    Name: string;
    Value: string;
    Category: string;
}

export type RecommendedMemType = {
    Name: string;
    dob: string;
    basicId: string;
    Caf_Status: string;
    userRelId: string;
}

export type basicDetailList = {
    BasicId: string;
    ClientName: string;
    Gender: string;
    Dob: string;
    Age: string;
    Mobile: string;
    Email: string;
    PanNumber: string;
    CompanyName: string;
    Designation: string;
    MonthIncome: string;
    Relation: string;
    RelationCode: string;
    IsMarried: string;
    IsSpouseWork: string;
    IsChildExist: string;
    IsDependentExist: string;
    IsDependent: boolean;
    Caf_Status: string;
    PlanId: string;
    empCode: string;
    userRelId: string;
    Dependent_Id: string;
}

export type IncomeDetails = {
    Id: string,
    BasicId: string,
    Amount: string,
    InflowTypeId: string,
    InflowTypeName: string,
    FrequencyTypeId: string,
    FrequencyTypeName: string,
    EmployeeId: string,
    PlanId: string,
}

export type GoalSchemeUATType = {
    goalId: string,
    goalName: string,
    goalCode: string,
    getAmount: string,
    investAmount: string,
    duration: string,
    startDate: string,
    endDate: string,
    currentCost: string,
    retirementAge: string,
    inflationRate: string,
    ror: string,
    pmt: string,
    investmentType: string,
    goalPriority: string,
    downPaymentRate: string,
    netDeficit: string,
    isChildGoal: boolean,
    folioSchemes: []
}

export type InflationAndRor = {
    Inflation: string,
    ROR_Lumpsum: string,
    ROR_Sip: string,
}

export type goalDropDown = {
    Text: string,
    Value: string,
    Category: string,
}

export type ExpenseDetails = {
    Id: string,
    BasicId: string,
    Amount: string,
    FinalAmount: string,
    ExpenseTypeId: string,
    OutFlowTypeId: string,
    AssetId: string,
    AssetTypeId: string,
    AssetTypeIdName: string,
    ExpenseTypeName: string,
    OutFlowTypeName: string,
    entryDate: string,
    IsContinue: string,
    createdBy: string,
    PlanId: string
}

export type NetAssetsTypes = {
    Id: string,
    PlanId: string,
    RmCode: string,
    BasicId: string,
    assetTypeId: string,
    fundId: string,
    schemeId: string,
    folioNo: string,
    sipAmount: string,
    policyId: string,
    policyPartnerId: string,
    policyNo: string,
    PolicyIssueDate: string,
    policyPremium: string,
    policySumAssured: string,
    policyTerm: string,
    policyOwnerBasicId: string,
    policyOwners: [],
    IsActiveInsur: boolean,
    policyStatus: string,
    lockInDuration: string,
    lockInInterest: string,
    
    loanInterest: string,
    loanEmiAmount: string,
    loanPrincipalAmount: string,
    loanStartDate: string,
    loanEndDate: string,
    loanOutstandAmount: string,

    assetName: string,
    LiquidityType: string,
    CurrentValue: string,
    advisorNote: string,
    createdDate: string,
    UpdatedDate: string,
    subbroker: string,
    status: boolean,
    MFInvestType: string,
    trxnSource: string,
    CurrHLV: string,
    FinalHLV: string,
    category: string,
    schemeName: string,
    assetTypeName: string,
    PolicyPartnerName: string,
    PolicyName: string,
    PolicyOwnerName: string,
    FrequencyTypeId: string,
    FrequencyTypeName: string,
    premiumTerm: string,
    dependenctid: string,
    AttachmentPath: string
}

export type retire = {
    id: number;
    name: string;
}