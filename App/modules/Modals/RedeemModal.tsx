import React, { useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import * as Select from "@radix-ui/react-select";
import { GroupBox } from "@ui/Group/Group.styles";
import Radio from "@ui/Radio/Radio";
import { RadioGroup } from "@ui/Radio/";
import StarFill from "App/icons/StarFill";
import Input from "App/ui/Input";
import CartFill from "App/icons/CartFill";
import style from "App/modules/ProductModule/Product.module.scss";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { encryptData, getUser } from "App/utils/helpers";
import { sendOTP, validateOTP, investTransaction } from "App/api/transactions";
import { addingToSystematicCart } from "App/api/cart";
import Layout, { toastAlert } from "App/shared/components/Layout";

export type VerifyRedeemTypes = {
  Amount: string;
  otp: string;
};

interface StoreTypes {
  verifyName: VerifyRedeemTypes;
}

const useRegistrationStore = create<StoreTypes>((set) => ({
  //* initial state
  verifyName: {
    Amount: "",
    otp: "",
  },
}));

type NameEventTypes = {
  selectedData: (value: any) => any;
  setOpenRedeem: (value: boolean) => boolean;
  setOpenSuccess: (value: boolean) => boolean;
  setMessageTxt: (value: string) => string;
};

const RedeemModal = ({ selectedData, setOpenRedeem, setOpenSuccess, setMessageTxt }: NameEventTypes) => {
  const user: any = getUser();
  const [showOtpPart, setShowOtpPart] = useState(false);
  const [amountType, setAmountType] = useState("partial");
  const [investStatus, setInvestStatus] = useState(false);
  const [purchaseData, setPurchaseData] = useState<any>(selectedData);
  const { verifyName, loading } = useRegistrationStore((state: any) => state);  
  const redeemModalObj = {
    Amount: Yup.number()
      .required("Amount is required")
      .positive()
      .integer()
      .min(1, `Min is 1`)
      .max(purchaseData?.currValue, `Max is ${Number(purchaseData?.currValue).toLocaleString("en-IN")}`),
  }
  const [validationSchema, setValidationSchema] = useState(
    redeemModalObj
  );
  const addToCart = async (val: any) => {
    console.log(val);
    if (val?.Amount === "") {
      toastAlert("error", "Amount should not be empty while adding to Cart");
    }
    else {
      try {
        console.log(user)
        let dt = new Date();
        let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId
        let obj = {
          basicid: user?.basicid,
          CartData: [
            {
              "PurFolioNo": "",
              "fromSchemeId": "",
              "sellFolioNo": purchaseData?.folioNo,
              "toSchemeID": purchaseData?.schemeId,
              "amount": val?.Amount,
              "startDate": "",
              "endDate": "",
              "No_of_Installment": "0",
              "MDate": "",
              "Units": "0",
              "bankId": purchaseData?.bankId,
              "accountNo": purchaseData?.bankAccNo,
              "goalCode": "FG13",
              "trxnTypeId": "6",
              "userGoalid": goalid,
              "mandateID": "",
              "trxnDate": `${dt.getFullYear()}-${dt.getMonth() < 9 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1)}-${dt.getDate() < 10 ? '0' + (dt.getDate()) : (dt.getDate())}`,
              "profileId": purchaseData?.ProfileID,
              "remark": "",
              "bankName": purchaseData?.bankName,
              "empCode": "",
              "profileName": user?.name
            }
          ]
        }
        // console.log(obj)
        const enc: any = encryptData(obj);
        const result: any = await addingToSystematicCart(enc);
        setOpenRedeem(false);
        setMessageTxt("Redemption of ₹" + val?.Amount + " is added to Cart Successfully")
        setOpenSuccess(true);
        setInvestStatus(false);
      }
      catch (error) {
        console.log(error);
        setInvestStatus(false);
        toastAlert("error", error);
      }
    }

  }
  const RedeemFun = async (e: any) => {
    // console.log(e);
    // console.log(showOtpPart);
    if (showOtpPart === false || e?.otp === undefined || e?.otp === "") {
      let obj = {
        'userid': user?.userid,
        'mobile': user?.mobile,
        'name': user?.name,
        'txntype': "SELL",
        'purscheme': '',
        'sellscheme': purchaseData?.schemeName,
        'rmname': user?.RmName,
        'amt': e?.Amount
      }
      // console.log(obj);
      try {
        const enc: any = encryptData(obj);
        // console.log(enc);
        const result: any = await sendOTP(enc);
        setShowOtpPart(true);
        toastAlert("success", "OTP Sent Successfully");
      }
      catch (error) {
        console.log(error);
        toastAlert("error", error);
      }
    }
    else if (showOtpPart === true) {
      try {
        let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId
        const enc: any = encryptData(e?.otp, true);
        // console.log(enc);
        const result: any = await validateOTP(enc);
        console.log(result);
        if (result?.status === "Success") {
          let obj1 = {
            "basicID": user?.basicid,
            "ProfileID": purchaseData?.ProfileID,
            "purSchemeId": "",
            "sellSchemeId": purchaseData?.schemeId,
            "tranType": "R",
            "PurFolioNo": "",
            "SellFolioNo": purchaseData?.folioNo,
            "Amount": e?.Amount,
            "No_of_Installment": "0",
            "MDate": "",
            "userGoalId": goalid,
            "Units": "0",
            "bankId": purchaseData?.bankId,
            "mandateId": purchaseData?.mandateId,
            "startDate": ""
          }
          const enc1: any = encryptData(obj1);
          const result2: any = await investTransaction(enc1);
          setOpenRedeem(false);
          setMessageTxt("Redemption of ₹" + e?.Amount + " is registered from '" + purchaseData?.schemeName + "'. Amount will be credited to (" + purchaseData?.bankName + "-" + purchaseData?.bankAccNo + ") within 3 working days [T+3].")
          setOpenSuccess(true);

        }

      }
      catch (error) {
        console.log(error);
        toastAlert("error", error);
      }
    }

  };
  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}>
            Redeem Details
          </Text>
        </Box>
        <Box className="modal-body p-3 mx-2">
          <Formik
            initialValues={verifyName}
            validationSchema={Yup.object().shape(validationSchema)}
            enableReinitialize
            onSubmit={(values, { resetForm }) => {
              // RedeemFun(values);
              console.log(values);
              if (investStatus) {
                //console.log(values, "RedeemFun");
                RedeemFun(values);
              } else {
                // console.log(values, "addToCart");
                addToCart(values)
              }

            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount

            }) => (
              // console.log(status),
              <>
                {/* {status==="Success" ? setShowOtpPart(true): setShowOtpPart(false)} */}
                <Box className="row border-bottom border-warning mx-2">
                  <Box className="col-12">
                    <Box className="row">
                      <Box className="col-auto">
                        {/* @ts-ignore */}
                        <Text className="text-capitalize">
                          {purchaseData?.schemeName}{" "}
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                  <Box className="col-12 py-2">
                    <Box className="row">
                      <Box className="col-auto">
                        {/* @ts-ignore */}
                        <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{purchaseData?.obj}</Text>
                        {/* @ts-ignore */}
                        <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{purchaseData?.subObj}</Text>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box className="row mt-2 mx-1">
                  <Box className="col-12">
                    <Box className="row">
                      <Box className="col-auto">
                        <Text>
                          Folio : <span>{purchaseData?.folioNo}</span>
                        </Text>
                      </Box>
                      <Box className="col-auto">
                        <Text>
                          Current Value : <span>{Number(purchaseData?.currValue).toLocaleString("en-IN")}</span>
                        </Text>
                      </Box>
                      <Box className="col-auto">
                        <Text>
                          Unit : <span>{purchaseData?.Units}</span>
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                  {!showOtpPart ? (
                    <Box>
                      <Box className="col-12">
                        <Box className="row">
                          <Box>
                            <GroupBox>
                              <RadioGroup
                                defaultValue={amountType}
                                className="inlineRadio"
                                name="amountType"
                                onValueChange={(e: any) => {
                                  if (e === "full")
                                    setFieldValue("Amount", purchaseData?.AvlBalAmt);
                                  setAmountType(e);
                                  // console.log(e);
                                }}
                              >
                                <Radio
                                  name="amountType"
                                  value="partial"
                                  label="Partial"
                                  id="partial"
                                />
                                <Radio
                                  name="amountType"
                                  value="full"
                                  label="Full"
                                  id="full"
                                />
                              </RadioGroup>
                            </GroupBox>
                          </Box>
                          <Box className="col-7">
                            <Input
                              label={`Enter Amount (Max.:${Number(purchaseData?.currValue).toLocaleString("en-IN")})`}
                              name="Amount"
                              className="border border-2"
                              disabled={!(amountType === "partial")}
                              placeholder="Enter Amount"
                              // @ts-ignore
                              error={submitCount ? errors.Amount : null}
                              onChange={(e: any) => { setFieldValue("Amount", e?.target?.value.length === 1 && e?.target?.value === "0" ? "" : e?.target?.value.replace(/\D/g, '')) }}
                              value={amountType === "partial" ? values.Amount : purchaseData?.AvlBalAmt}
                            />
                          </Box>
                          {/* <Text label={errors.Amount} ></Text> */}
                        </Box>
                      </Box>
                    </Box>
                  ) : (
                    <Box className="col-12">
                      <Box className="row">
                        <Box className="col-7">
                          <Input
                            label="Enter Amount"
                            name="Amount"
                            className="border border-2"
                            value={amountType === "partial" ? values.Amount : purchaseData?.AvlBalAmt}
                          />
                        </Box>
                        <Box className="col-7">
                          <Input
                            label="Enter OTP"
                            name="otp"
                            className="border border-2"
                            value={values.otp}
                            onChange={handleChange}
                          />
                        </Box>
                      </Box>
                    </Box>
                  )}
                </Box>

                <Box className="row justify-content-end">
                  <Box className="col-auto px-0">

                    {showOtpPart ? (<>
                      <Button
                        type="submit"
                        css={{ backgroundColor: "orange" }}
                        onClick={() => {
                          setShowOtpPart(false);
                          setInvestStatus(false);
                        }}>
                        <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                          Back
                        </Text>
                      </Button>
                    </>

                    ) : (<>
                      <Button
                        type="button"
                        css={{ backgroundColor: "orange" }}
                        onClick={() => {
                          setInvestStatus(false);
                          handleSubmit()
                        }}
                      >
                        <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                          <CartFill className="me-1" />
                          Add to Cart</Text>
                      </Button>
                    </>)}

                  </Box>
                  <Box className="col-auto">
                    {showOtpPart ? (
                      <Button
                        type="submit"
                        css={{ backgroundColor: "orange" }}
                        onClick={() => { setInvestStatus(true); handleSubmit() }}
                      >
                        <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                          Confirm
                        </Text>
                      </Button>
                    ) : (
                      <Button
                        type="submit"
                        css={{ backgroundColor: "orange" }}
                        onClick={() => { setInvestStatus(true); handleSubmit() }}
                        // onClick={() => {}}
                        loading={loading}
                      >
                        <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                          {" "}
                          <CartFill className="me-2" color="white" /> Redeem
                        </Text>
                      </Button>
                    )}
                  </Box>
                </Box>
              </>
            )}
          </Formik>
        </Box>
      </Box>
    </>
  );
};
export default RedeemModal;
