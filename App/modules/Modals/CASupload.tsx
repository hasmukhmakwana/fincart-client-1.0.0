import React, { useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import * as Select from '@radix-ui/react-select'
import { GroupBox } from "@ui/Group/Group.styles";
import Radio from "@ui/Radio/Radio";
import { RadioGroup } from "@ui/Radio/";
import StarFill from "App/icons/StarFill";
import Input from "App/ui/Input";
import CartFill from "App/icons/CartFill";
import style from "App/modules/ProductModule/Product.module.scss";
import Upload from "App/icons/Upload";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";

const CASModalObj = {

    OTPcas: Yup.string()
        .required("Amount is required")
        .trim(),
    // .matches(/{6}/g, "Only Numbers are allowed"),
    Filecas: Yup.string()
        .required("File is required"),

}
let validationSchema = Yup.object().shape(CASModalObj);


export type VerifyCASTypes = {
    // Name: string;
    OTPcas: string;
    Filecas: any;
    // InvestDate: string;
};

interface StoreTypes {
    verifyPay: VerifyCASTypes;
}
const useRegistrationStore = create<StoreTypes>((set) => ({
    //* initial state
    verifyPay: {
        // Name: "",
        OTPcas: "",
        Filecas: "",
        // InvestDate: "",
    }
}))

// type NameEventTypes = {
//     clickOnSubmit: (value: VerifyCASTypes) => any;
// };

const CASupload = () => {
    const [fieldValue, setFieldValue] = useState();
    const { verifyPay, loading } = useRegistrationStore((state: any) => state);
    const PurchaseFun = (e: any) => {
        // clickOnSubmit(e);
        console.log(e);
    };
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header">
                    {/* @ts-ignore */}
                    <Text weight="bold" css={{ color: "var(--colors-blue1)" }}> Upload CAS</Text>
                </Box>
                <Box className="modal-body">
                    <Box className="row justify-content-center">
                        <Box className="col-auto">
                            {/* @ts-ignore */}
                            <Text weight="bold" color="blue1" className="text-capitalize" >Upload Your Consolidate Account Statement </Text>
                        </Box>
                    </Box>
                    <Formik
                        initialValues={verifyPay}
                        validationSchema={validationSchema}
                        enableReinitialize
                        onSubmit={(values, { resetForm }) => {

                            PurchaseFun(values);
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount
                        }) => (
                            <>
                                <Box className="row mt-2">
                                    <Box className="col-auto">
                                        <Input
                                            label="Upload File"
                                            type="file"
                                            name="Filecas"
                                            // style={{ color: "orange"}}
                                            className={`px-0 border rounded-4 ${style.uploadBtn}`}
                                            error={submitCount ? errors.Filecas : null}
                                            onChange={handleChange}
                                            value={values?.Filecas}
                                            autocomplete="off"
                                        />
                                    </Box>
                                </Box>
                                <Box className="row mt-2">
                                    <Box className="col-auto">
                                        <Input
                                            label="Enter OTP"
                                            name="OTPcas"
                                            className="border border-2"
                                            error={submitCount ? errors.OTPcas : null}
                                            onChange={handleChange}
                                            value={values?.OTPcas}
                                            autocomplete="off"
                                        />
                                    </Box>
                                </Box>
                                <Box className="row justify-content-end" >
                                    <Box className="col-auto">
                                        <Button
                                            type="submit"
                                            css={{ backgroundColor: "orange" }}
                                            onClick={() => { handleSubmit() }}
                                        >
                                            <Text className="p-2" css={{ color: "white" }}> <Upload className="me-2" color="white" /> Upload</Text>
                                        </Button>
                                    </Box>
                                </Box>
                            </>)}</Formik>
                </Box>
            </Box>
        </>
    );
}
export default (CASupload);