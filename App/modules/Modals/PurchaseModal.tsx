import React, { useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import style from "App/modules/ProductModule/Product.module.scss";
import "App/modules/ProductModule/Product.module.scss";
import { StyledTabs, TabsContent, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsTrigger } from "./TabTrigger.style";
import SelectMenu from "@ui/Select/Select";
import StarFill from "App/icons/StarFill";
import Input from "App/ui/Input";
import Basket2fill from "App/icons/Basket2fill";
import CartFill from "App/icons/CartFill";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { addingTocart, getBankPaymentModes } from "App/api/cart";
import { encryptData, getUser } from "App/utils/helpers";
import Layout, { toastAlert } from "App/shared/components/Layout";
import {
    getAllMemberPurchaseDetails
} from "App/api/mandate"


export type VerifyPurchaseTypes = {
    // Name: string;
    Amount: string;
    // InvestDate: string;
};

interface StoreTypes {
    verifyPay: VerifyPurchaseTypes;
}

const useRegistrationStore = create<StoreTypes>((set) => ({
    //* initial state
    verifyPay: {
        // Name: "",
        Amount: "",
        // InvestDate: "",
    }
}))

type NameEventTypes = {
    selectedData: (value: any) => any;
    setOpenPurchase: (value: boolean) => boolean;
    setOpenSuccess: (value: boolean) => boolean;
    setMessageTxt: (value: string) => string;
};

let paymentTabs = [
    {
        title: "Net Banking",
        value: "Net Banking",
        icon: <img height="150%" src={"/BankIconSmall.png"}
            alt="BankIcon" />
    },
    {
        title: "UPI",
        value: "UPI",
        icon: <img height="95%" src={"/UPIIconSmall.png"}
            alt="UPIICon" />
    },
];
const PurchaseModal = ({ selectedData, setOpenPurchase, setOpenSuccess, setMessageTxt }: NameEventTypes) => {
    const user: any = getUser();
    const [filter, setFilters] = useState("1");
    const [showOtp, setshowOtp] = useState(false);
    const { verifyPay, loading } = useRegistrationStore((state: any) => state);
    const [purchaseData, setPurchaseData] = useState<any>(selectedData);
    const [bankData, setBankData] = useState<any>([]);
    const [paymentData, setPaymentData] = useState<any>([]);
    const [selectedPayMode, setSelectedPayMode] = useState<any>('');

    const [investStatus, setInvestStatus] = useState(false);

    const purchaseModalObj = {
        Amount: Yup.number()
            .required("Amount is required")
            .positive()
            .integer()
            .min(purchaseData?.addminAmt, `Min is ${purchaseData?.addminAmt}`)
            .max(purchaseData?.addMaxAmt, `Max is ${purchaseData?.addMaxAmt}`),
    }

    const [validationSchema, setValidationSchema] = useState(
        purchaseModalObj
    );

    const addToCart = async (val: any) => {
        console.log(val);
        if (val?.Amount === "") {
            toastAlert("error", "Amount should not be empty while adding to Cart");
        }
        else {
            try {
                let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId;
                let obj =
                {
                    'basicID': user?.basicid,
                    'ProfileID': purchaseData?.ProfileID,
                    'purSchemeId': purchaseData?.schemeId,
                    'tranType': 'NOR',
                    'Amount': Number(val?.Amount),
                    'No_of_Installment': null,
                    'MDate': null,
                    'PurFolioNo': purchaseData?.folioNo || "New",
                    'bankId': purchaseData?.bankId,
                    'userGoalId': goalid,
                    'startDate': null
                }
                console.log(obj)
                const enc: any = encryptData(obj);
                const result: any = await addingTocart(enc);
                setOpenPurchase(false);
                setMessageTxt("Purchase of ₹" + val?.Amount + " is added to Cart Successfully")
                setOpenSuccess(true);

            }
            catch (error) {
                console.log(error);
                toastAlert("error", error);
            }

        }

    }

    const PurchaseFun = async (e: any) => {

        let bankList: any = [];
        try {
            const enc: any = encryptData({
                basicId: user?.basicid,
                fundid: "0",
            });
            let result = await getAllMemberPurchaseDetails(enc);
            bankList = result?.data?.bankList;
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
        setBankData(bankList.filter((item: any) => {
            return item?.basicid === user?.basicid;
        }))
        let arr = bankList.filter((item: any) => {
            return item?.basicid === user?.basicid;
        });
        console.log(arr);
        setFilters(arr[0]?.accountNo)
        try {
            const enc: any = encryptData(arr[0]?.bankid, true);
            let result = await getBankPaymentModes(enc);
            console.log("Payment Data ", result?.data);
            setPaymentData(result?.data)
            setSelectedPayMode(result?.data[0]?.payModeId)
            setshowOtp(true);
        } catch (error: any) {
            console.log(error);
            //setPurchaseLoad(false);
            toastAlert("error", error);
        }
    };

    return (
        <>
        {console.log(selectedData,"selectedData")}
            <Box className="col-md-12 col-sm-12 col-lg-12">
            <Box className="modal-header p-3" css={{borderRadius: "15px 15px 0 0"}}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}> Purchase Details</Text>
                </Box>
                <Box className="modal-body p-3">
                    <Formik
                        initialValues={verifyPay}
                        validationSchema={Yup.object().shape(validationSchema)}
                        enableReinitialize
                        onSubmit={(values, { resetForm }) => {
                            if (investStatus) {
                                // PurchaseFun(values);
                            } else {
                                // console.log(values)
                                addToCart(values);
                            }
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount
                        }) => (
                            <>
                                <Box className="row border-bottom border-warning mx-2">
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text className="text-capitalize" >{purchaseData?.schemeName} </Text>
                                            </Box>
                                            <Box className="col-auto pt-0" >
                                                <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" />
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="col-12 py-2">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{purchaseData?.obj}</Text>
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{purchaseData?.subObj}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="row mt-2 mx-1">
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                <Text>Folio : <span>{purchaseData?.folioNo}</span></Text>
                                            </Box>
                                            <Box className="col-auto">
                                                <Text>Current Value : <span>{Number(purchaseData?.currValue).toLocaleString("en-IN")}</span></Text>
                                            </Box>
                                            <Box className="col-auto">
                                                <Text>Unit : <span>{purchaseData?.Units}</span></Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="col-auto">
                                        <Input
                                            label="Enter Amount"
                                            name="Amount"
                                            placeholder="Enter Amount"
                                            className="border border-2"
                                            //@ts-ignore
                                            error={submitCount ? errors.Amount : null}
                                            onChange={(e: any) => { setFieldValue("Amount", e?.target?.value.length === 1 && e?.target?.value === "0" ? "" :e?.target?.value.replace(/\D/g, '')) }}
                                            value={values?.Amount}
                                            autocomplete="off"
                                        />
                                    </Box>
                                </Box>
                                {showOtp ?
                                    <>
                                        <Box className="row  border-top border-secondary">
                                            <Box className="col-12 mt-2">
                                                {/* @ts-ignore */}
                                                <Text size="h4" >
                                                    Select Payment Option
                                                </Text>
                                            </Box>
                                            <Box className="col-12 mt-3">
                                                <StyledTabs defaultValue="NetBanking" css={{ height: "12rem" }}>
                                                    <TabsList
                                                        aria-label="Purchase Details"
                                                        className={`tabsCardStyle row py-1 ${style.paymentScroll}`}
                                                        css={{ mb: 10 }}

                                                        // onChange={(e)=}
                                                        onChange={(e: any) => {
                                                            // setFieldValue("occupation", e.Value);
                                                            console.log(e);
                                                        }}
                                                    >
                                                        {paymentData.map((item: any, index: any) => {
                                                            return (
                                                                <Box className="col-auto" >
                                                                    <TabsTrigger
                                                                        value={item?.payType}
                                                                        className={`tabsCard ${style.paymentTabs}`}
                                                                        key={item?.payModeId}
                                                                        onClick={() => { setSelectedPayMode(item?.payModeId) }}
                                                                    >
                                                                        <Box className="text-center">
                                                                            {/* @ts-ignore */}
                                                                            <Text size="h5">{item?.payType}</Text>
                                                                        </Box>
                                                                        <Box className="row justify-content-center" css={{ height: "1.2rem" }}>
                                                                            <Box className="col-auto">
                                                                                <img width="100px" src={item?.modeImg} alt={item?.payType} />
                                                                            </Box>
                                                                        </Box>
                                                                    </TabsTrigger>
                                                                </Box>
                                                            );
                                                        })}
                                                    </TabsList>
                                                    <TabsContent value="NetBanking">
                                                        <Box className="row pt-1">
                                                            {/* @ts-ignore */}
                                                            <Text size="h5">&nbsp;</Text>
                                                            <Box className="col-12 col-md-auto col-lg-auto">
                                                                <SelectMenu
                                                                    label="Select Your Bank Name"
                                                                    name="NetBankingOptions"
                                                                    value={filter}
                                                                    items={bankData}
                                                                    bindValue={"accountNo"}
                                                                    bindName={"bankName"}
                                                                    onChange={(e) =>
                                                                        setFilters(e?.accountNo || "")
                                                                    }
                                                                />
                                                            </Box>
                                                        </Box>
                                                    </TabsContent>
                                                    <TabsContent value="UPI">
                                                        <Box className="row pt-1">
                                                            {/* @ts-ignore */}
                                                            <Text size="h5">Ac no.: {bankData[0]?.accountNo}</Text>
                                                            <Box className="col-12 col-md-auto col-lg-auto">
                                                                <Input
                                                                    label="Enter UPIID"
                                                                    name="UpiId"
                                                                    className="border border-2"
                                                                    placeholder="abc@okbank"
                                                                />
                                                            </Box>
                                                        </Box>
                                                    </TabsContent>

                                                </StyledTabs>
                                            </Box>
                                        </Box>
                                    </> : ""}

                                <Box className="row justify-content-end mx-2">
                                    <Box className="col-auto px-0">

                                        {showOtp ? (<>
                                            <Button
                                                type="submit"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={() => setshowOtp(false)}>
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                    Back
                                                </Text>
                                            </Button>

                                        </>

                                        ) : (<>
                                            <Button
                                                type="button"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={handleSubmit}
                                            >
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                    <CartFill className="me-1" />
                                                    Add to Cart</Text>
                                            </Button>
                                        </>)}

                                    </Box>

                                    {/* <Box className="col-auto">
                                        {showOtp ?
                                            (<>
                                                <Button
                                                    type="submit"
                                                    css={{ backgroundColor: "orange" }}
                                                    onClick={() => Submit()}
                                                >
                                                    <Text className="p-1" css={{ color: "white" }}>
                                                        Confirm
                                                    </Text>
                                                </Button>
                                            </>)
                                            :
                                            (<>
                                                <Button
                                                    type="submit"
                                                    css={{ backgroundColor: "orange" }}
                                                    onClick={() => { handleSubmit() }}
                                                    loading={loading}
                                                >
                                                    <Text className="p-2" css={{ color: "white" }}>  <Basket2fill className="me-1" color="white" />
                                                        Purchase</Text>
                                                </Button>
                                            </>)
                                        }
                                    </Box> */}
                                </Box>
                            </>
                        )}</Formik>
                </Box>
            </Box>
        </>
    );
}
export default (PurchaseModal);