import React, { useState, useEffect } from "react";
import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import { Button } from "@ui/Button/Button";
import style from "./Profile.module.scss";
import Box from "./../../ui/Box/Box";
import { encryptData, getUser, plainBase64 } from "App/utils/helpers";
import { callKycSync, getProfileMembers, resetKYC, changeProfilePic } from "App/api/profile";
import { useRouter } from "next/router";
import Loader from "App/shared/components/Loader";
import NoData from "@ui/SimpleGrid/components/NoData";
import FullProfile from "./components/FullProfile/FullProfilePage";
import ArrowLeftShort from "App/icons/ArrowLeftShort";
import MemberList from "./components/MemberList";
import useProfileStore from "./store";
import ProfileEdit from "./components/ProfileEdit";
import { getKYC_CAF_Master } from "App/api/registration";
import useRegistrationStore from "../RegistrationModule/store";

const Module = () => {
  const router = useRouter();
  const { setSelectedBasicID, KYC_CAF_MASTER, setKYC_CAF_MASTER } =
    useProfileStore();
  const { setBasicID, setRegiType } = useRegistrationStore();

  const user: any = getUser();
  const [loader, setloader] = useState<boolean>(false);
  const [profile, setprofile] = useState<any[]>([]);
  const [profileLoader, setProfileLoader] = useState<boolean>(false);
  const [currentId, setCurrentId] = useState<any>();

  const [formType, setformType] = useState<"list" | "summary" | "editList">(
    "list"
  );

  useEffect(() => {
    if (router.pathname === "/Profile") {
      setformType("list");
    }
    return () => { };
  }, [router]);

  const profileMembers = async () => {
    try {
      setloader(true);
      const enc: any = encryptData(user?.basicid, true);
      const result: any = await getProfileMembers(enc);
      if (result?.data?.profilePercent < 100) {
        setRegiType("MAIN");
        setBasicID(result?.data?.basicId);
        router.push("/Registration");
        return;
      }
      const list: any[] = result?.data?.memberProfiles || [];
      delete result?.data?.memberProfiles;
      list.unshift(result?.data || {});
      setloader(false);
      console.log(list);
      setprofile(list);
    } catch (error) {
      console.log(error);
      setloader(false);
      toastAlert("error", error);
    }
  }

  useEffect(() => {
    //profile list
    (async () => {
      await profileMembers();
    })();
    //caf master list

    if (!KYC_CAF_MASTER.length) {
      (async () => {
        try {
          const result = await getKYC_CAF_Master();
          setKYC_CAF_MASTER(result?.data);
        } catch (error) {
          console.log(error);
          return toastAlert("error", error);
        }
      })();
    }

    return () => { };
  }, []);

  //*functions

  const kycSync = async (member: any) => {
    try {
      if (member === undefined) return null;
      setProfileLoader(true);
      setCurrentId(member?.basicId);
      let obj = {
        "basicid": member?.basicId,
        "pannumber": member?.panNumber
      }

      const enc: any = encryptData(obj);

      const result = await callKycSync(enc);

      //const result = { data: "N" };
      if (result?.data != "N") {
        const list: any[] = profile.map((i) =>
          i.basicId === member.basicId ? { ...i, kycStatus: "success" } : i
        );

        setprofile(list);
      }

      setProfileLoader(false);
    } catch (error) {
      setProfileLoader(false);
      console.log(error);
      return toastAlert("error", error);
    }
  };

  const resetKYCDetails = async (member: any) => {
    try {
      if (member === undefined) return null;

      setloader(true);
      const enc: any = encryptData(member?.basicId, true);
      let result = await resetKYC(enc);

      if (result?.status != undefined && result?.status.toString() === "Success") {
        toastAlert("success", "Profile reset done successfully");
      }
      profileMembers();
      setloader(false);
    } catch (error) {
      setloader(false);
      console.log(error);
      return toastAlert("error", error);
    }
  };

  const handleProfileEdit = (member: any) => {
    if (member?.profilePercent >= 100) {
      setSelectedBasicID(member?.basicId);
      setformType("summary");
    } else {
      setRegiType("PENDING");
      setBasicID(member?.basicId);
      router.push("/Registration");
    }
  };

  const handleProfilePicEdit = async (member: any, profilePicPath: any) => {
    // console.log(member);
    // console.log(profilePicPath);
    try {
      setProfileLoader(true);
      let obj = {
        "file": plainBase64(profilePicPath),
        "basicid": member?.basicId
      }
      //console.log(obj);
      const enc: any = encryptData(obj);
      const result: any = await changeProfilePic(enc);
      //console.log(result);
      if (result?.status != undefined && result?.status.toString() === "Success") {
        toastAlert("success", "Profile Picture Uploaded successfully");
        setProfileLoader(false);
        profileMembers();
      }
    } catch (error) {
      setProfileLoader(false);
      console.log(error);
      setloader(false);
      toastAlert("error", error);
    }

  }
  const getCurrentUI = () => {
    // console.log(formType);

    const compoList: any = {
      list: (
        <MemberList
          profile={profile}
          handleProfileEdit={handleProfileEdit}
          handleProfilePicEdit={handleProfilePicEdit}
          profileLoader={profileLoader}
          basicId={currentId}
          kycSync={kycSync}
          resetKYC={resetKYCDetails}
        />
      ),
      summary: <FullProfile />,
      editList: <ProfileEdit />,
    };
    const compo: any = compoList[formType];
    return compo;
  };

  //* Main Return
  return (
    <Layout>
      <>
        <PageHeader
          title="Profile"
          rightContent={
            <Button
              color="yellow"
              size="md"
              className={style.mt10}
              onClick={() => {
                setBasicID(0);
                setRegiType("MEMBER");
                router.push("/Registration");
              }}
              loading={loader}
            >
              Add Member
            </Button>
          }
        />
        {loader ? (
          <Loader />
        ) : profile.length ? (
          <Box className="row">
            <Box className="col-md-12 col-sm-12 col-lg-12">
              {getCurrentUI()}
              <Box className="row">
                <Box className="col"></Box>
                <Box
                  className="col col-auto col-sm-auto text-end"
                  css={{ mt: 20 }}
                >
                  <Button
                    type="submit"
                    color="yellow"
                    onClick={() => setformType("list")}
                    hide={formType === "list"}
                  >
                    <ArrowLeftShort size="20" />
                    Back
                  </Button>
                </Box>
              </Box>
            </Box>
          </Box>
        ) : (
          <Box className="row text-center">
            <Box className="col-12">
              <NoData />
            </Box>
          </Box>
        )}
      </>
    </Layout>
  );
};

export default Module;
