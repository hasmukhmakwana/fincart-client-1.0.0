import Text from "@ui/Text/Text";
import style from ".././Profile.module.scss";
import Box from "./../../../ui/Box/Box";
import Card from "@ui/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import CheckCircleFill from "App/icons/CheckCircleFill";
import { WEB_URL } from "App/utils/constants";
import ReloadArrows from "App/icons/ReloadArrows";
import Info from "App/icons/Info";
import PencilFill from "App/icons/PencilFill";
import { useRouter } from "next/router";
import { SingleAction } from "@ui/SimpleGrid/DataGrid.styles";
import Recycle from "App/icons/Recycle";
import ModalDialog from "@ui/AlertDialog/ModalDialog";
import { useState } from "react";
import { Button } from "@ui/Button/Button";
import Input from "@ui/Input";
import { convertBase64 } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import Loader from "@ui/SimpleGrid/components/Loader";

const StatusList: any = {
  pending: "Pending",
  inprocess: "In Process",
  success: "Done",
  failed: "Failed",
};

const StatusColor: any = {
  pending: "rgb(179, 218, 255)",
  inprocess: "rgb(179, 218, 255)",
  success: "rgb(0, 92, 179)",
  failed: "rgb(179, 218, 255)",
};

const StatusClass: any = {
  success: "apiStatusClasses_success",
  pending: "apiStatusClasses_inprocess",
  inprocess: "apiStatusClasses_inprocess",
  failed: "apiStatusClasses_inprocess",
};

interface MemberCardTypes {
  index: number;
  member: any;
  handleProfileEdit: (obj: any) => void;
  handleProfilePicEdit: (obj: any, obj2: any) => void;
  profileLoader: boolean;
  kycSync: (obj: any) => void;
  resetKYC: (obj: any) => void;
  basicId: any
}

const MemberCard = ({
  index,
  member,
  handleProfileEdit,
  handleProfilePicEdit,
  profileLoader,
  kycSync,
  resetKYC,
  basicId
}: MemberCardTypes) => {
  const router = useRouter();
  const [open, setOpen] = useState<boolean>(false);
  const [openPrfile, setOpenProfile] = useState<boolean>(false);
  const [profilePicPath, setProfilePicpath] = useState<string>('');
  const [error, setError] = useState<boolean>(false);
  //* Main Return

  const sendProfilePic = async (member: any) => {
    console.log(member);
    console.log(profilePicPath);
    if (profilePicPath !== "") {
      handleProfilePicEdit(member, profilePicPath);
    }
    else {
      setError(true);
    }
  }
  return (
    <Card className="p-4" css={{ height: "100%" }} key={index}>
      <Box className={style.basketBox}>
        {(profileLoader && basicId == member?.basicId) ? <><Loader></Loader></> : <>
          <Box className="row">
            <Box className="col col-auto rounded-2">
              <img
                src={member?.profilePic || WEB_URL + "/no_image.png"}
                alt="profilePic"
                className={style.img}
              />
              <SingleAction
                css={{ borderRadius: 7, width: 25 }}
                onClick={() => { setOpenProfile(true) }}
                title="Profile Pic"
              >
                <PencilFill color="black" height={14} width={14} />
              </SingleAction>
            </Box>
            <Box className="col">
              <Box className={style.mt10}>
                <Text
                  weight="bold"
                  size="bt"
                  transform="uppercase"
                  className="text-end">
                  <span className="badge bg-primary">
                    {member?.isGroupLeader === "N" ? "Member" : "Owner"}
                  </span>
                </Text>
                <Text
                  weight="bold"
                  size="bt"
                  transform="uppercase"
                  css={{ color: "var(--colors-blue2)" }}>
                  {member?.panNumber}
                </Text>
                <Text weight="bold" color="default" size="h4" className="my-1">
                  {member?.clientName}
                </Text>
                <Text weight="normal" color="default" size="bt">
                  {member?.accountTypeText}
                </Text>
              </Box>
              <GroupBox className="mt-4 mb-2">
                <Box>
                  <CheckCircleFill
                    color={StatusColor[member?.kycStatus]}
                  ></CheckCircleFill>
                </Box>
                <Box className={`align-self-center ${style.statusName}`}>
                  <Text size="h6">KYC</Text>
                </Box>
                <Box className={`${style.statusFrame}`}>
                  <Box className={`badge ${StatusClass[member?.kycStatus]}`}>
                    {StatusList[member?.kycStatus]}
                  </Box>
                </Box>
                {member?.kycStatus != "success" && (
                  <Box className="pointer">
                    <SingleAction
                      css={{ borderRadius: 7 }}
                      onClick={() => kycSync(member)}
                      title="Sync KYC"
                    >
                      <ReloadArrows color="black" height={14} width={14} />
                    </SingleAction>
                  </Box>
                )}
              </GroupBox>
              <GroupBox className="my-2">
                <Box>
                  <CheckCircleFill
                    color={
                      StatusColor[
                      member?.profilePercent >= 100 ? "success" : "inprocess"
                      ]
                    }
                  ></CheckCircleFill>
                </Box>
                <Box className={`align-self-center ${style.statusName}`}>
                  <Text size="h6">Profile</Text>
                </Box>
                <Box className={`${style.statusFrame}`}>
                  <Box
                    className={`badge ${StatusClass[
                      member?.profilePercent >= 100 ? "success" : "inprocess"
                    ]
                      }`}
                  >
                    {member?.profilePercent || 0} %
                  </Box>
                </Box>
                <Box className="pointer">
                  <SingleAction
                    css={{ borderRadius: 7 }}
                    onClick={() => handleProfileEdit(member)}
                    title="Profile"
                  >
                    <Info color="black" height={14} width={14} />
                  </SingleAction>
                </Box>
                {member?.profilePercent < 100 ?
                  <>
                    <Box className="pointer">
                      <SingleAction
                        css={{ borderRadius: 7 }}
                        onClick={() => { setOpen(true) }}
                        title="Reset"
                      >
                        <Recycle color="black" height={14} width={14} />

                      </SingleAction>
                    </Box>
                    {/*@ts-ignore*/}
                    <Text className="my-auto" css={{ fontSize: "0.745rem" }}>Reset Profile</Text>
                  </>
                  : <></>}
              </GroupBox>
              <GroupBox className="my-2">
                <Box>
                  <CheckCircleFill
                    color={StatusColor[member?.mandateStatus]}
                  ></CheckCircleFill>
                </Box>
                <Box className={`align-self-center ${style.statusName}`}>
                  <Text size="h6">SIP Mandate</Text>
                </Box>
                <Box className={`${style.statusFrame}`}>
                  <Box className={`badge ${StatusClass[member?.mandateStatus]}`}>
                    {StatusList[member?.mandateStatus]}
                  </Box>
                </Box>
                <Box className="pointer">
                  <SingleAction
                    css={{ borderRadius: 7 }}
                    onClick={() => router.push("/Mandate/MandateList")}
                    title="SIP Mandate"
                  >
                    <Info color="black" height={14} width={14} />
                  </SingleAction>
                </Box>
              </GroupBox>
              <GroupBox className="my-2">
                <Box>
                  <CheckCircleFill
                    color={
                      StatusColor[
                      member?.totalInvstAccounts > 0 ? "success" : "inprocess"
                      ]
                    }
                  ></CheckCircleFill>
                </Box>
                <Box className={`align-self-center ${style.statusName}`}>
                  <Text size="h6">Investment A/C</Text>
                </Box>
                <Box className={`${style.statusFrame}`}>
                  <Box
                    className={`badge ${StatusClass[
                      member?.totalInvstAccounts > 0 ? "success" : "inprocess"
                    ]
                      }`}
                  >
                    {member?.totalInvstAccounts || 0}
                  </Box>
                </Box>
                <Box className="pointer">
                  <SingleAction
                    css={{ borderRadius: 7 }}
                    onClick={() => router.push("/Investment")}
                    title="Investment Account"
                  >
                    <Info color="black" height={14} width={14} />
                  </SingleAction>
                </Box>
              </GroupBox>
            </Box>
          </Box>
        </>}
      </Box >
      <ModalDialog
        open={open}
        setOpen={setOpen}
        hideCloseBtn={true}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "30%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            <Box className="row justify-content-start">
              <Box className="col-auto">
                <Text css={{ color: "var(--colors-blue1)" }}>Reset Profile Warning</Text></Box>
            </Box>
          </Box>
          <Box className="modal-body p-3">
            <Box className="my-1 mx-2">
              <Box css={{ padding: "0.3rem", fontSize: "0.9rem" }}>
                Are you sure to Reset this Profile?
              </Box>
              <Box css={{ pt: 10 }} className="text-end">
                <Button color="yellow" onClick={() => { setOpen(false) }}>
                  Cancel
                </Button>
                <Button color="yellow" onClick={() => resetKYC(member)}>
                  Reset
                </Button>
              </Box>
            </Box>
          </Box>
        </Box>
      </ModalDialog>
      <ModalDialog
        open={openPrfile}
        setOpen={setOpenProfile}
        hideCloseBtn={true}
        css={{
          "@bp0": { width: "70%" },
          "@bp1": { width: "30%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            <Box className="row justify-content-start">
              <Box className="col-auto">
                <Text css={{ color: "var(--colors-blue1)" }}>Change Profile Picture</Text></Box>
            </Box>
          </Box>
          <Box className="modal-body p-3">
            <Box className="my-1 mx-2">
              <Box css={{ padding: "0.3rem", fontSize: "0.9rem" }}>
                <Box className="col-10 col-md-10 col-sm-10 col-lg-10">
                  <Text
                    color="mediumBlue"
                    size="h6"
                    space="letterspace"
                  >
                    Upload Profile Picture
                  </Text>
                  <Input
                    parentCSS={{ mb: 0 }}
                    type="file"
                    className="form-control"
                    name="cancelledCheque"
                    // value={values?.cancelledCheque||""}
                    onChange={async (
                      e: React.ChangeEvent<HTMLInputElement>
                    ) => {
                      try {
                        const file: any = e?.target?.files?.[0] || null;
                        if (file !== null || file !== "") {
                          setError(false);
                        }
                        const base64: any = await convertBase64(file);
                        setProfilePicpath(base64);
                      } catch (error: any) {
                        toastAlert("error", error);
                      }
                    }}
                    required
                    error={
                      error ? "Select Pic to proceed" : null
                    }
                  />
                </Box>

                <Box css={{ pt: 10 }} className="text-end">
                  <Button color="yellow" onClick={() => { setOpenProfile(false) }} disabled={profileLoader}>
                    Cancel
                  </Button>
                  <Button color="yellow" onClick={() => { sendProfilePic(member); }} disabled={profileLoader}>
                    {profileLoader
                      ? "Loading..."
                      : "Set"}
                  </Button>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </ModalDialog>
    </Card >
  );
};

export default MemberCard;
