import style from ".././Profile.module.scss";
import Box from "./../../../ui/Box/Box";
import Card from "@ui/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import CheckCircleFill from "App/icons/CheckCircleFill";
import { useRouter } from "next/router";
import EditIcon from "App/icons/EditIcon";
import CrossCircleFill from "App/icons/CrossCircleFill";
import Divider from "@ui/Divider/Divider";

const List = [
  {
    label: "Pan Verification",
    isDone: true,
  },
  {
    label: "Personal Details",
    isDone: true,
  },
  {
    label: "Bank Account",
    isDone: true,
  },
  {
    label: "Proof Of Identity",
    isDone: true,
  },
  {
    label: "Proof Of Address",
    isDone: true,
  },

  {
    label: "In-Person Verification",
    isDone: true,
  },
  {
    label: "Signature",
    isDone: true,
  },
  {
    label: "Confirm KYC & Account Creation Details",
    isDone: false,
  },
];
interface ProfileEditTypes { }

const ProfileEdit = () => {
  const router = useRouter();
  //* Main Return
  return (
    <div className="row">
      <div className="col col-md-3"></div>
      <div className="col col-md-6">
        <Card className="p-4 mt-4">
          <Box>
            <Box className="row">
              <Box className="col text-center">
                You are few step away to start your investment
              </Box>
            </Box>
            {List.map((item: any, index: number) => (
              <>
                <Box>
                  <Divider />
                </Box>
                <Box className="row" key={index}>
                  <Box className="col col-8">
                    <GroupBox>
                      <Box>
                        {item.isDone ? (
                          <CheckCircleFill color="green"></CheckCircleFill>
                        ) : (
                          <CrossCircleFill color="red"></CrossCircleFill>
                        )}
                      </Box>
                      <Box>{item.label}</Box>
                    </GroupBox>
                  </Box>
                  <Box className="col col-4 text-end pointer">
                    <EditIcon />
                  </Box>
                </Box>
              </>
            ))}
          </Box>
        </Card>
      </div>
    </div>
  );
};

export default ProfileEdit;
