import React, { useEffect, useRef, useState } from "react";
import Card from "@ui/Card";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import { getSummary } from "App/api/registration";
import { toastAlert } from "App/shared/components/Layout";
import PoiPoa from "./PoiPoa";
import BankDetails from "./BankDetails";
import PersonalInfo from "./PersonalInfo";
import MediaData from "./MediaData";
import styles from "../../Profile.module.scss";
import {
  calculateAge,
  encryptData,
  formatDate,
  getUser,
  plainBase64,
} from "App/utils/helpers";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@ui/Accordian/Accordian";
import DownArrow from "App/icons/DownArrow";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { UiTypePypes } from "../../ProfileTypes";
import {
  changeAddressRequest,
  changeBankRequest,
  changeRequestList,
  generateOTP_email_mobile,
  updateFatca,
  updateGeneralDetails,
  updateNomineeDetails,
  updatePAN,
  updateSign,
  validateOTP_email_mobile,
} from "App/api/profile";
import OTP from "../UpdateModal/OTP";
import General from "../UpdateModal/General";
import Mobile from "../UpdateModal/Mobile";
import Email from "../UpdateModal/Email";
import Nominee from "../UpdateModal/Nominee";
import Bank from "../UpdateModal/Bank";
import POI from "../UpdateModal/POI";
import POA from "../UpdateModal/POA";
import useProfileStore from "../../store";
import Sign from "../UpdateModal/Sign";
import Fatca from "../UpdateModal/Fatca";
import FatcaInfo from "./FatcaInfo";
import { modalWidth, SCHEMAS } from "../../profileHelper";
import RequestList from "../UpdateModal/RequestList";
import { NRI_PROFILE_TYPE_VALUE } from "App/utils/constants";

// const initDynamicObj: any = {
const initDynamicObj: any = {
  mobile: {
    initialValues: {
      mobile: "",
    },
    schema: SCHEMAS.mobile,
  },

  email: {
    initialValues: {},
    schema: SCHEMAS.email,
  },

  OTP: {
    initialValues: {
      OTP: "",
    },
    schema: SCHEMAS.otp,
  },

  general: {
    initialValues: {},
    schema: SCHEMAS.general,
  },

  nominee: {
    initialValues: {},
    schema: SCHEMAS.nominee,
  },

  bank: {
    initialValues: {},
    schema: SCHEMAS.bank,
  },

  POI: {
    initialValues: {},
    schema: SCHEMAS.poi,
  },

  POA: {
    initialValues: {},
    schema: {},
  },

  sign: {
    initialValues: {},
    schema: SCHEMAS.sign,
  },

  fatca: {
    initialValues: {},
    schema: {},
  },

  bank_Requests: {
    initialValues: {
      list: [],
    },
    schema: {},
  },

  POA_Requests: {
    initialValues: {
      list: [],
    },
    schema: {},
  },
};

interface FullProfilePageTypes { }

//*main
const FullProfilePage = () => {
  const user: any = getUser();
  const { selectedBasicID } = useProfileStore();

  const [summary, setSummary] = useState<any>(null);
  const [open, setOpen] = useState<boolean>(false);
  const [UiType, setUiType] = useState<UiTypePypes>("mobile");
  const [dynamicObj, setdynamicObj] = useState<any>(initDynamicObj);
  const [isSameLocal, setisSameLocal] = useState<boolean>(true);
  const [isNRI, setIsNRI] = useState<boolean>(false);
  const [isDoc2, setisDoc2] = useState<boolean>(false);
  const [isOtherLocal, setisOtherLocal] = useState<boolean>(true);

  const prevState = useRef({
    email_mobile: "",
    type: "",
  });

  //*useEffects
  useEffect(() => {
    getSummaryInfo();
    return () => { };
  }, []);

  useEffect(() => {
    console.log("is NRI", isNRI);

    //POA
    let poa_schema: any = SCHEMAS.poa;

    if (!isSameLocal) {
      poa_schema = { ...poa_schema, ...SCHEMAS.poa_same };
    }

    if (isDoc2) {
      poa_schema = { ...poa_schema, ...SCHEMAS.poa_doc2 };
    }

    if (isNRI) {
      poa_schema = { ...poa_schema, ...SCHEMAS.poa_nri };
    }

    //fatca
    let fatca_schema: any = SCHEMAS.fatca;
    if (isOtherLocal) {
      fatca_schema = { ...fatca_schema, ...SCHEMAS.fatca_other };
    }

    setdynamicObj({
      ...dynamicObj,
      POA: { ...dynamicObj.POA, schema: poa_schema },
      fatca: { ...dynamicObj.fatca, schema: fatca_schema },
    });
    return () => { };
  }, [isSameLocal, isDoc2, isOtherLocal, isNRI]);

  const getSummaryInfo = async () => {
    try {
      const enc: any = encryptData(selectedBasicID, true);
      const result = await getSummary(enc);
      const summaryData: any = result?.data || {};
      setSummary({
        ...summaryData,
        Nominee_Details: summaryData.Nominee_Details.map((i: any) => ({
          ...i,
          isMinor: calculateAge(i.Nominee_DOB)?.years < 18,
          Nominee_DOB: formatDate(i.Nominee_DOB, "d-m-y"),
        })),
      });
      console.log(summaryData, "summaryData");
      setIsNRI((summaryData?.Kyc_Details?.ProfileType_Value == NRI_PROFILE_TYPE_VALUE));
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  //*functions

  const clickOnSubmit = async (values: any) => {
    //console.log("submited..........", values);
    //return;

    try {
      switch (UiType) {
        case "mobile":
          {
            const obj: any = {
              BasicID: selectedBasicID,
              email_mobile: values?.mobile || "",
              type: "mobile",
              name: summary?.Account_Creation_Details?.ClientName || "",
              otp: "",
            };
            // console.log("obj..........", obj);
            const enc: any = encryptData(obj);
            await generateOTP_email_mobile(enc);
            prevState.current = {
              email_mobile: values?.mobile || "",
              type: "mobile",
            };
            setUiType("OTP");
            toastAlert("success", "OTP Sent!");
          }

          break;

        case "email":
          {
            const obj: any = {
              BasicID: selectedBasicID,
              email_mobile: values?.email || "",
              type: "email",
              name: summary?.Account_Creation_Details?.ClientName || "",
              otp: "",
            };
            const enc: any = encryptData(obj);
            await generateOTP_email_mobile(enc);
            prevState.current = {
              email_mobile: values?.email || "",
              type: "email",
            };
            setUiType("OTP");
            toastAlert("success", "OTP Sent!");
          }
          break;

        case "OTP":
          {
            const obj: any = {
              BasicID: selectedBasicID,
              email_mobile: prevState.current.email_mobile,
              type: prevState.current.type,
              name: summary?.Account_Creation_Details?.ClientName || "",
              otp: values?.OTP || "",
            };
            // console.log("obj..........", obj);
            const enc: any = encryptData(obj);
            await validateOTP_email_mobile(enc);
            toastAlert("success", "Updated");
            setOpen(false);
            getSummaryInfo();
          }
          break;

        case "general":
          {
            const obj: any = {
              BasicId: selectedBasicID,
              AadharNo: values?.aadharNo || "",
              Gender: values?.gender || "",
              Annual_Income: values?.AnnualIncome || "",
              Occupation: values?.Occupation || "",
              Mother_name: values?.motherName || "",
              Marital_status: values?.maritalStatus || "",
              Investor_Type: "01",
              GuardDOB: "",
            };
            const enc: any = encryptData(obj);
            await updateGeneralDetails(enc);
            toastAlert("success", "Updated");
            setOpen(false);
            getSummaryInfo();
          }
          break;

        case "nominee":
          {
            let per_total: number = 0;

            const Nominee_List = values.nomineeList.map(
              (i: any, index: number) => {
                per_total += parseInt(i.percent || "0");
                return {
                  BasicId: selectedBasicID,
                  Nom_Id: i.Nom_Id || 0,
                  Nominee_Name: i.nomineeName,
                  Relation: i.relation,
                  Nominee_DOB: i.nomineeDOB,
                  Nominee_Percent: i.percent,
                  Guardian_Name: i.gaurdianName,
                  Guardian_Pan: i.gaurdianPan,
                  Nominee_Pan: i?.Nominee_Pan || "",
                  Nominee_DOB_Proof_Type: i?.Nominee_DOB_Proof_Type || "",
                  Guardian_Rel_Type: i?.Guardian_Rel_Type || "",
                  Nominee_DOB_Proof_Path: i?.Nominee_DOB_Proof_Path || "",
                  [`NOM${index + 1}_File`]: plainBase64(i?.document) || "",
                };
              }
            );

            // console.log(per_total);
            if (per_total != 100) {
              return toastAlert("warn", "Nominee_Percent sum must be 100");
            }
            console.log({ Nominee_List });

            const obj: any = {
              BasicId: selectedBasicID,
              Nominee_List,
            };
            const enc: any = encryptData(obj);
            await updateNomineeDetails(enc);
            toastAlert("success", "Updated");
            setOpen(false);
            getSummaryInfo();
          }
          break;

        case "bank":
          {
            const obj: any = {
              BasicID: selectedBasicID,
              BankName: values?.bankName || "",
              NameAsPerBank: values?.nameAsPerBank || "",
              BankAddress: values?.bankAddress || "",
              BankAccNo: values?.accountNumber || "",
              BankBranch: values?.branch || "",
              BankCity: values?.city || "",
              AccountType: values?.accountType || "",
              MICR: values?.MICR || "",
              IFSC: values?.IFSC || "",
              FilePath1: plainBase64(values?.document),
              DocCode1: values?.bankProofType || "",
              Reason: values?.reason || "",
              Platform_Type: "WEB",
              RmCode: user?.rmCode || "",
            };

            // console.log(obj);

            const enc: any = encryptData(obj);
            await changeBankRequest(enc);
            toastAlert("success", "Your request has been sent..!!");
            setOpen(false);
            // getSummaryInfo();
          }
          break;

        /***
         * @identity_PAN
         */

        case "POI":
          {
            const obj: any = {
              BasicID: selectedBasicID,
              PAN_File: plainBase64(values?.document),
              Platform_Type: "WEB",
            };

            // console.log(obj);

            const enc: any = encryptData(obj);
            await updatePAN(enc);
            toastAlert("success", "Updated");
            setOpen(false);
            getSummaryInfo();
          }
          break;

        /***
         * @address_aadhar_card
         */
        case "POA":
          {
            const obj: any = {
              BasicID: selectedBasicID,
              Address: values?.address || "",
              Pin: values?.pincode || "",
              State: values?.state || "",
              City: values?.city || "",
              Country: values?.country || "",
              DocCode1: values?.proofType1 || "",
              FilePath1: plainBase64(values?.document1),
              Corsp_Address_Same_As_Perma_Address: values?.profile_type == NRI_PROFILE_TYPE_VALUE ? "N" : values?.isSame ? "Y" : "N",
              CorspAddress: values?.corspAddress || "",
              CorspPin: values?.corspPincode || "",
              CorspState: values?.corspState || "",
              CorspCity: values?.corspCity || "",
              CorspCountry: values?.corspCountry || "",
              //DocCode2: values?.proofType1 === "03" ? "12" : "", // only aadhar case, 12 for aadhar back
              DocCode2: values?.profile_type == NRI_PROFILE_TYPE_VALUE ? values?.proofType2 : values?.proofType1 === "03" ? "12" : "",
              FilePath2: values.document2 ? plainBase64(values.document2) : "",
              Reason: values?.reason || "",
              Platform_Type: "WEB",
              RmCode: user?.rmCode || "",
              Address_type: values?.proofType1 || "",
            };

            //console.log(obj);

            const enc: any = encryptData(obj);
            await changeAddressRequest(enc);
            toastAlert("success", "Your request has been sent..!!");
            setOpen(false);
            // getSummaryInfo();
          }
          break;

        case "sign":
          {
            const obj: any = {
              BasicID: selectedBasicID,
              Sign_File: plainBase64(values?.document),
            };
            const enc: any = encryptData(obj);
            await updateSign(enc);
            toastAlert("success", "Updated");
            setOpen(false);
            getSummaryInfo();
          }
          break;

        case "fatca":
          {
            let items: any[] = [];
            if (values?.Is_Tax_Country_Other_Than_India) {
              items = values?.Fatca_Other_Country_Details.map((i: any) => ({
                Tax_Residency_Country: i?.Country_TAX_RES || "",
                Tax_Identification_Type: i?.Iden_Type || "",
                Tax_Identification_No: i?.Country_Payer_Id || "",
              }));
            }
            const obj: any = {
              BasicId: selectedBasicID,
              Place_Of_Birth: values?.Place_Of_Birth || "",
              Country_Of_Birth: values?.Country_Of_Birth || "",
              Source_Of_Wealth: values?.Source_Of_Wealth || "",
              Is_politically_Exposed: values?.Is_politically_Exposed || "N",
              Is_Tax_Country_Other_Than_India:
                values?.Is_Tax_Country_Other_Than_India ? "Y" : "N",
              Fatca_Other_Country_Details: items,
            };

            // console.log(obj);
            const enc: any = encryptData(obj);
            await updateFatca(enc);
            toastAlert("success", "Updated");
            setOpen(false);
            getSummaryInfo();
          }
          break;
        /**
         * @DEFAULT
         */
        default:
          toastAlert("warn", "Issue in Types");
          break;
      }
    } catch (error) {
      console.log(error);
      return toastAlert("error", error);
    }
  };

  const handleOpenModalAction = (ui: UiTypePypes) => {
    let initialValues: any = {};

    switch (ui) {
      case "mobile":
        initialValues = {
          mobile: "",
        };
        break;

      case "email":
        initialValues = {
          email: "",
        };
        break;

      case "OTP":
        break;

      case "general":
        initialValues = {
          gender: summary?.Kyc_Details?.Gender_Value || "M",
          maritalStatus: summary?.Kyc_Details?.MaritalStatus_Value || "MARRIED",
          aadharNo: summary?.POA_Details?.Uid || "",
          // fatherName: summary?.Account_Init_Details?.FatherName || "",
          motherName: summary?.Kyc_Details?.MotherName || "",
          // DateofBirth: summary?.Account_Init_Details?.DOB || "",
          // AddressType: summary?.Kyc_Details?.Address_Type_Value || "",
          Occupation: summary?.Kyc_Details?.Occupation_Value || "",
          AnnualIncome: summary?.Kyc_Details?.AnnualIncome_Value || "",
          // SourceOfIncome: summary?.Kyc_Details?.SourceOfIncome_Value || "",
        };
        break;
      case "nominee":
        const list: any[] = summary?.Nominee_Details || [];

        if (!list.length) list.push({});
        initialValues = {
          nomineeList: list.map((i) => ({
            Nom_Id: i.Nom_Id || 0,
            nomineeName: i.Nominee_Name,
            relation: i.Relation,
            nomineeDOB: formatDate(i.Nominee_DOB),
            percent: i.Nominee_Percent,
            gaurdianName: i.Guardian_Name,
            gaurdianPan: i.Guardian_Pan,
            //new fields
            Nominee_Pan: i.Nominee_Pan,
            Nominee_DOB_Proof_Type: i.Nominee_DOB_Proof_Type,
            Guardian_Rel_Type: i.Guardian_Rel_Type,
            Nominee_DOB_Proof_Path: i?.Nominee_DOB_Proof_Path || "",
            document: i?.Nominee_DOB_Proof_Path || "",
          })),
        };
        break;

      case "bank":
        initialValues = {
          IFSC: summary?.Bank_Details?.IFSC || "",
          bankCode: summary?.Bank_Details?.Bank_Code || null,
          bankName: summary?.Bank_Details?.Bank_Name || "",
          accountNumber: summary?.Bank_Details?.Account_No || "",
          bankAddress: summary?.Bank_Details?.Bank_Address || "",
          branch: summary?.Bank_Details?.Branch_Name || "",
          city: summary?.Bank_Details?.Bank_City || "",
          MICR: summary?.Bank_Details?.MICR || "",
          nameAsPerBank: summary?.Bank_Details?.Name_As_Per_Bank || "",
          accountType: summary?.Bank_Details?.AccountType_Code || null,
        };
        break;

      case "POI":
        break;

      case "POA":
        initialValues = {
          address: summary?.POA_Details?.Address || "",
          pincode: summary?.POA_Details?.PinCode || "",
          state: summary?.POA_Details?.State || "",
          city: summary?.POA_Details?.City || "",
          country: summary?.POA_Details?.Country || "",
          isSame: true,
          corspAddress: summary?.POA_Details?.CorspAddress || "",
          corspPincode: summary?.POA_Details?.CorspPinCode || "",
          corspState: summary?.POA_Details?.CorspState || "",
          corspCity: summary?.POA_Details?.CorspCity || "",
          corspCountry: summary?.POA_Details?.CorspCountry || "",
          profile_type: summary?.Kyc_Details?.ProfileType_Value || "01"
        };
        break;

      case "sign":
        break;

      case "fatca":
        const items: any[] = [];

        //item first
        //show atleast one item
        items.push({
          Country_TAX_RES: summary?.Kyc_Details?.Country_TAX_RES1_Value || null,
          Iden_Type: summary?.Kyc_Details?.Iden_Type1_Value || null,
          Country_Payer_Id: summary?.Kyc_Details?.Country_Payer_Id1 || "",
        });

        //item second
        if (summary?.Kyc_Details?.Country_Payer_Id2) {
          items.push({
            Country_TAX_RES:
              summary?.Kyc_Details?.Country_TAX_RES2_Value || null,
            Iden_Type: summary?.Kyc_Details?.Iden_Type2_Value || null,
            Country_Payer_Id: summary?.Kyc_Details?.Country_Payer_Id2 || "",
          });
        }
        //item third
        if (summary?.Kyc_Details?.Country_Payer_Id3) {
          items.push({
            Country_TAX_RES:
              summary?.Kyc_Details?.Country_TAX_RES3_Value || null,
            Iden_Type: summary?.Kyc_Details?.Iden_Type3_Value || null,
            Country_Payer_Id: summary?.Kyc_Details?.Country_Payer_Id3 || "",
          });
        }

        initialValues = {
          Place_Of_Birth: summary?.Kyc_Details?.Place_Of_Birth || "",
          Country_Of_Birth: summary?.Kyc_Details?.Country_Of_Birth || null,
          Source_Of_Wealth: summary?.Kyc_Details?.SourceOfIncome_Value || null,
          Is_politically_Exposed:
            summary?.Kyc_Details?.Politically_Exposed_Value || null,
          Is_Tax_Country_Other_Than_India:
            summary?.Kyc_Details?.Tax_Country_Other_Than_India === "Y"
              ? true
              : false,

          Fatca_Other_Country_Details: items,
        };
        break;

      default:
        return toastAlert("warn", "Issue in Types");
        break;
    }

    setdynamicObj({
      ...dynamicObj,
      [ui]: {
        ...dynamicObj[ui],
        initialValues,
      },
    });
    setUiType(ui);
    setOpen(true);
  };

  const handleRequestModal = async (ui: UiTypePypes) => {
    let initialValues: any = {};
    try {
      switch (ui) {
        case "bank_Requests":
          const enc: any = encryptData({
            BasicID: selectedBasicID,
            Type: "BANK",
          });

          const result: any = await changeRequestList(enc);

          initialValues = {
            list: result?.data || [],
          };

          break;

        case "POA_Requests":
          const enc_poa: any = encryptData({
            BasicID: selectedBasicID,
            Type: "ADDRESS",
          });
          const result_poa: any = await changeRequestList(enc_poa);

          initialValues = {
            list: result_poa?.data || [],
          };
          break;

        default:
          return toastAlert("warn", "Issue in Types");
          break;
      }

    } catch (error) {
      console.log(error);
      return toastAlert("error", error);
    }

    setdynamicObj({
      ...dynamicObj,
      [ui]: {
        ...dynamicObj[ui],
        initialValues,
      },
    });
    setUiType(ui);
    setOpen(true);
  };

  /**render the ui based on type */
  const renderModalUI = (formikObj: any) => {
    const compList: any = {
      mobile: <Mobile setOpen={setOpen} formikObj={formikObj} />,
      email: <Email setOpen={setOpen} formikObj={formikObj} />,
      OTP: <OTP setOpen={setOpen} formikObj={formikObj} />,
      general: <General setOpen={setOpen} formikObj={formikObj} />,
      nominee: <Nominee setOpen={setOpen} formikObj={formikObj} />,
      bank: <Bank setOpen={setOpen} formikObj={formikObj} />,
      POI: <POI setOpen={setOpen} formikObj={formikObj} />,
      POA: (
        <POA
          setOpen={setOpen}
          formikObj={formikObj}
          setisSameLocal={setisSameLocal}
          isNRI={isNRI}
          setisDoc2={setisDoc2}
        />
      ),
      sign: <Sign setOpen={setOpen} formikObj={formikObj} />,
      fatca: (
        <Fatca
          setOpen={setOpen}
          formikObj={formikObj}
          setisOtherLocal={setisOtherLocal}
        />
      ),
      bank_Requests: (
        <RequestList
          setOpen={setOpen}
          formikObj={formikObj}
          title="All Bank Requests"
        />
      ),
      POA_Requests: (
        <RequestList
          setOpen={setOpen}
          formikObj={formikObj}
          title="All Address Requests"
        />
      ),
    };

    return compList[UiType];
  };

  //*main return
  return (
    <Box>
      <Card noshadow>
        <Text size="h4" border="default" css={{ mb: 10 }}>
          Edit Profile
          {summary?.Account_Creation_Details?.ClientName && (
            <span className="ms-1">
              -
              <span className="ms-1 text-primary">
                {summary?.Account_Creation_Details?.ClientName}
              </span>
            </span>
          )}
        </Text>
        <Box css={{ p: 20 }}>
          <Accordion type="single" defaultValue="1" collapsible>
            <AccordionItem value="1">
              <AccordionTrigger css={{ borderRadius: "8px 8px 0px 0px" }} className={`${styles.accordienBordreHeader}`}>
                <Box>
                  <Text size="h5" css={{ textTransform: "uppercase" }}>
                    Personal Information
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent className={`${styles.accordienBordre} mb-1 p-3 pt-2`}>
                <PersonalInfo
                  summary={summary}
                  handleOpenModalAction={handleOpenModalAction}
                />
              </AccordionContent>
            </AccordionItem>
            <AccordionItem value="2" className="mt-2">
              <AccordionTrigger css={{ borderRadius: "8px 8px 0px 0px" }} className={`${styles.accordienBordreHeader}`}>
                <Box>
                  <Text size="h5" css={{ textTransform: "uppercase" }}>
                    Bank Account Details
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent className={`${styles.accordienBordre} mb-1 p-3 pt-2`}>
                <BankDetails
                  summary={summary}
                  handleOpenModalAction={handleOpenModalAction}
                  handleRequestModal={handleRequestModal}
                />
              </AccordionContent>
            </AccordionItem>

            <AccordionItem value="3" className="mt-2">
              <AccordionTrigger css={{ borderRadius: "8px 8px 0px 0px" }} className={`${styles.accordienBordreHeader}`}>
                <Box>
                  <Text size="h5" css={{ textTransform: "uppercase" }}>
                    Proof of Identity
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent className={`${styles.accordienBordre} mb-1 p-3 pt-2`}>
                <PoiPoa
                  summary={summary}
                  handleOpenModalAction={handleOpenModalAction}
                  handleRequestModal={handleRequestModal}
                />
              </AccordionContent>
            </AccordionItem>

            <AccordionItem value="4" className="mt-2">
              <AccordionTrigger css={{ borderRadius: "8px 8px 0px 0px" }} className={`${styles.accordienBordreHeader}`}>
                <Box>
                  <Text size="h5" css={{ textTransform: "uppercase" }}>
                    Signature
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent className={`${styles.accordienBordre} mb-1 p-3 pt-2`}>
                <MediaData
                  summary={summary}
                  handleOpenModalAction={handleOpenModalAction}
                />
              </AccordionContent>
            </AccordionItem>

            <AccordionItem value="5" className="mt-2">
              <AccordionTrigger css={{ borderRadius: "8px 8px 0px 0px" }} className={`${styles.accordienBordreHeader}`}>
                <Box>
                  <Text size="h5" css={{ textTransform: "uppercase" }}>
                    Fatca
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent className={`${styles.accordienBordre} mb-1 p-3 pt-2`}>
                <FatcaInfo
                  summary={summary}
                  handleOpenModalAction={handleOpenModalAction}
                />
              </AccordionContent>
            </AccordionItem>
          </Accordion>
        </Box>
      </Card>

      <DialogModal
        open={open}
        setOpen={setOpen}
        css={{
          "@bp0": { width: "100%" },
          "@bp1": { width: modalWidth[UiType] },
          "z-index": 2
        }}
      >
        <Formik
          initialValues={dynamicObj[UiType]?.initialValues || {}}
          validationSchema={Yup.object().shape(
            dynamicObj[UiType]?.schema || {}
          )}
          enableReinitialize
          onSubmit={async (values, { setSubmitting }) => {
            setSubmitting(true);
            await clickOnSubmit(values);
            setSubmitting(false);
          }}
        >
          {(formikObj) => <Form>{renderModalUI(formikObj)}</Form>}
        </Formik>
      </DialogModal>
    </Box>
  );
};
export default FullProfilePage;
