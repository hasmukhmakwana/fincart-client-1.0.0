import Box from "../../../../ui/Box/Box";
import Input from "@ui/Input";
import Heading from "./Heading";
import { UiTypePypes } from "../../ProfileTypes";
interface FatcaInfoTypes {
  summary: any;
  handleOpenModalAction: (ui: UiTypePypes) => void;
}
const FatcaInfo = ({ summary, handleOpenModalAction }: FatcaInfoTypes) => {
  //*main return
  return (
    <>
      <Box className="row">
        <Heading
          name="Fatca Details"
          tooltip="Update Fatca Information"
          handleOpenModalAction={() => handleOpenModalAction("fatca")}
        />
        <Box className="col-md-2 col-sm-2 col-lg-2">
          <Input
            label="Birth Place"
            name="Place_Of_Birth"
            value={summary?.Kyc_Details?.Place_Of_Birth || ""}
            disabled
          />
        </Box>
        <Box className="col-md-2 col-sm-2 col-lg-2">
          <Input
            label="Birth Country"
            name="Country_Of_Birth"
            value={summary?.Kyc_Details?.Country_Of_Birth || ""}
            disabled
          />
        </Box>
        <Box className="col-md-2 col-sm-2 col-lg-2">
          <Input
            label="Source of Wealth"
            name="SourceOfIncome_Text"
            value={summary?.Kyc_Details?.SourceOfIncome_Text || ""}
            disabled
          />
        </Box>
        <Box className="col-md-2 col-sm-2 col-lg-2">
          <Input
            label="Political Exposed"
            name="Politically_Exposed_Text"
            value={summary?.Kyc_Details?.Politically_Exposed_Text || ""}
            disabled
          />
        </Box>
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Tax country Other than India"
            name="Tax_Country_Other_Than_India"
            value={summary?.Kyc_Details?.Tax_Country_Other_Than_India || ""}
            disabled
          />
        </Box>
        {summary?.Kyc_Details?.Tax_Country_Other_Than_India === "Y" && (
          <>
            {summary?.Kyc_Details?.Country_Payer_Id1 && (
              <>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax Country"
                    name="Country_TAX_RES1_Text"
                    value={summary?.Kyc_Details?.Country_TAX_RES1_Text || ""}
                    disabled
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax ID Type"
                    name="Iden_Type1_Text"
                    value={summary?.Kyc_Details?.Iden_Type1_Text || ""}
                    disabled
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax ID Number"
                    name="Country_Payer_Id1"
                    value={summary?.Kyc_Details?.Country_Payer_Id1 || ""}
                    disabled
                  />
                </Box>
              </>
            )}
            {summary?.Kyc_Details?.Country_Payer_Id2 && (
              <>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax Country"
                    name="Country_TAX_RES2_Text"
                    value={summary?.Kyc_Details?.Country_TAX_RES2_Text || ""}
                    disabled
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax ID Type"
                    name="Iden_Type2_Text"
                    value={summary?.Kyc_Details?.Iden_Type2_Text || ""}
                    disabled
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax ID Number"
                    name="Country_Payer_Id2"
                    value={summary?.Kyc_Details?.Country_Payer_Id2 || ""}
                    disabled
                  />
                </Box>
              </>
            )}
            {summary?.Kyc_Details?.Country_Payer_Id3 && (
              <>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax Country"
                    name="Country_TAX_RES3_Text"
                    value={summary?.Kyc_Details?.Country_TAX_RES3_Text || ""}
                    disabled
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax ID Type"
                    name="Iden_Type3_Text"
                    value={summary?.Kyc_Details?.Iden_Type3_Text || ""}
                    disabled
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="First Tax ID Number"
                    name="Country_Payer_Id3"
                    value={summary?.Kyc_Details?.Country_Payer_Id3 || ""}
                    disabled
                  />
                </Box>
              </>
            )}
          </>
        )}
      </Box>
    </>
  );
};

export default FatcaInfo;
