import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import { SingleAction } from "@ui/SimpleGrid/DataGrid.styles";
import Text from "@ui/Text/Text";
import EditIcon from "App/icons/EditIcon";
import React from "react";
import { UiTypePypes } from "../../ProfileTypes";

interface HeadingType {
  name: string;
  tooltip: string;
  handleOpenModalAction: () => void;
  handleRequestModal?: () => void;
}
function Heading({
  name,
  tooltip,
  handleOpenModalAction,
  handleRequestModal,
}: any) {
  return (
    <Box>
      <Box className="col col-12 mt-3 d-flex justify-content-between">
        <Box className="">
          <Text weight="bold" size="h5" css={{ mb: 10 }}>
            {name}
          </Text>
        </Box>
        <Box className="d-flex justify-content-between">
          {!!handleRequestModal && (
            <Box css={{ mr: 10 }}>
              <span className="btn-link pointer" onClick={handleRequestModal}>
                View All Requests
              </span>
            </Box>
          )}
          <Box className="pointer">
            <SingleAction
              css={{ borderRadius: 7 }}
              onClick={handleOpenModalAction}
              title={tooltip}
            >
              <EditIcon color="black" height={14} width={14} />
            </SingleAction>
          </Box>
        </Box>
      </Box>
      <Box>
        {/* @ts-ignore */}
        <Text weight="bold" size="h5" border="default2" css={{ mb: 10 }}></Text>
      </Box>
    </Box>
  );
}

export default Heading;
