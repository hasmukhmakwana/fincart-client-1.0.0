import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import Textarea from "@ui/Textarea";
import Heading from "./Heading";
import { UiTypePypes } from "../../ProfileTypes";
import { useMemo } from "react";
import { NRI_PROFILE_TYPE_VALUE } from "App/utils/constants";

interface PoiPoaTypes {
  summary: any;
  handleOpenModalAction: (ui: UiTypePypes) => void;
  handleRequestModal: (ui: UiTypePypes) => void;
}

const PoiPoa = ({
  summary,
  handleOpenModalAction,
  handleRequestModal,
}: PoiPoaTypes) => {
  //*functions
  const getText = useMemo(
    () => (value: string) => {
      if (summary?.POA_Details?.POA_Type_Value === "aadhaar") {
        return value === "front" ? "Aadhaar Front Side" : "Aadhaar Back Side";
      }
    },
    []
  );
  //*main return
  return (
    <>
      <Box>
        <Heading
          name="Proof of Identity"
          tooltip="Update Identity Proof"
          handleOpenModalAction={() => handleOpenModalAction("POI")}
        />
        <Box className="row">
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <img
              src={summary?.POI_Details?.POI_Path_Fincart || "../no_image.png"}
              width={"100%"}
            ></img>
          </Box>
          <Box className="col-md-8 col-sm-8 col-lg-8">
            <Box className="row">
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="PAN Number"
                  name="PAN Number"
                  value={summary?.Account_Init_Details?.PanNumber || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Name"
                  name="name"
                  value={summary?.Account_Init_Details?.ClientName || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Father's Name"
                  name="motherName"
                  value={summary?.Account_Init_Details?.FatherName || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Date of Birth"
                  name="DateofBirth"
                  value={summary?.Account_Init_Details?.DOB || ""}
                  disabled
                />
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      <Box css={{ pt: 20 }}>
        <Heading
          name="Proof of Address"
          tooltip="Update Proof of Address"
          handleOpenModalAction={() => handleOpenModalAction("POA")}
          handleRequestModal={() => handleRequestModal("POA_Requests")}
        />
        <Box className="row">
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Text color="blue1" size="h6" space="letterspace">
              {getText("front")}
            </Text>
            <Box>
              <img
                src={
                  summary?.POA_Details?.POA_FrontPath_Fincart ||
                  "../no_image.png"
                }
                width={"80%"}
              ></img>
            </Box>
            {/* </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4"> */}
            <Text
              color="blue1"
              size="h6"
              space="letterspace"
              css={{ marginTop: "10px" }}
            >
              {getText("back")}
            </Text>
            <Box>
              <img
                src={
                  summary?.Kyc_Details?.ProfileType_Value == NRI_PROFILE_TYPE_VALUE ?
                    summary?.POA_Details?.OverseasPoaPath || "../no_image.png" :
                    summary?.POA_Details?.POA_BackPath_Fincart || "../no_image.png"
                }
                width={"80%"}
              ></img>
            </Box>
          </Box>
          <Box className="col-md-8 col-sm-8 col-lg-8">
            <Box className="row">
              <Box className="col-12">
                {/* @ts-ignore */}
                <Text color="primary">
                  {summary?.Kyc_Details?.ProfileType_Value == NRI_PROFILE_TYPE_VALUE ? "Indian Address" : "Permanent Address Details"}
                </Text>
              </Box>
            </Box>
            <Box className="row mt-3">
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Aadhaar Number"
                  name="aadhaarNumber"
                  value={summary?.POA_Details?.Uid || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Textarea
                  label="Address"
                  name="address"
                  value={summary?.POA_Details?.Address || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Country"
                  name="country"
                  value={summary?.POA_Details?.Country || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="State"
                  name="state"
                  value={summary?.POA_Details?.State || ""}
                  disabled
                />
              </Box>
              {/* <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="District"
                  name="district"
                  value={summary?.POA_Details?.District || ""}
                  disabled
                />
              </Box> */}
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="City"
                  name="city"
                  value={summary?.POA_Details?.City}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Pin Code"
                  name="pinCode"
                  value={summary?.POA_Details?.PinCode}
                  disabled
                />
              </Box>
            </Box>

            <Box className="row mt-3">
              <Box className="col-12">
                {/* @ts-ignore */}
                <Text color="primary">
                  {summary?.Kyc_Details?.ProfileType_Value == NRI_PROFILE_TYPE_VALUE ? "Overseas Address" : "Correspondence Address Details"}
                </Text>
              </Box>
            </Box>
            <Box className="row mt-3">
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Textarea
                  label="Address"
                  name="address"
                  value={summary?.POA_Details?.CorspAddress || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Country"
                  name="country"
                  value={summary?.POA_Details?.CorspCountry || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="State"
                  name="state"
                  value={summary?.POA_Details?.CorspState || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="City"
                  name="city"
                  value={summary?.POA_Details?.CorspCity}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Pin Code"
                  name="pinCode"
                  value={summary?.POA_Details?.CorspPinCode}
                  disabled
                />
              </Box>
            </Box>

            {/* {summary?.Kyc_Details?.ProfileType_Value == NRI_PROFILE_TYPE_VALUE && <>
              </>} */}
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default PoiPoa;
