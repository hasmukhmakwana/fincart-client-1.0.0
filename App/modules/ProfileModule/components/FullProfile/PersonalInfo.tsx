import Box from "../../../../ui/Box/Box";
import Input from "@ui/Input";
import Divider from "@ui/Divider/Divider";
import EditIcon from "App/icons/EditIcon";
import { SingleAction } from "@ui/SimpleGrid/DataGrid.styles";
import Heading from "./Heading";
import { UiTypePypes } from "../../ProfileTypes";
import React from "react";

interface PersonalInfoTypes {
  summary: any;
  handleOpenModalAction: (ui: UiTypePypes) => void;
}
const PersonalInfo = ({
  summary,
  handleOpenModalAction,
}: PersonalInfoTypes) => {
  //*main return
  return (
    <>
      <Box className="row">
        <Box className="col-md-6 col-sm-6 col-lg-6">
          <Box className="row align-items-end">
            <Box className="col">
              <Input
                label="Mobile"
                name="mobile"
                value={summary?.Account_Creation_Details?.ClientMobile || ""}
                disabled
              />
            </Box>
            <Box className="col col-auto pb-3">
              <Box
                className={`badge ${
                  summary?.Account_Creation_Details?.IsMobile_Verified === "Y"
                    ? "bg-success"
                    : "bg-danger"
                }`}
              >
                {summary?.Account_Creation_Details?.IsMobile_Verified === "Y"
                  ? "Verified"
                  : "Not Verified"}
              </Box>
            </Box>
            <Box className="col col-auto pb-3">
              <SingleAction
                css={{ borderRadius: 7 }}
                onClick={() => handleOpenModalAction("mobile")}
                title="Update Mobile"
              >
                <EditIcon color="black" height={14} width={14} />
              </SingleAction>
            </Box>
          </Box>
        </Box>

        <Box className="col-md-6 col-sm-6 col-lg-6">
          <Box className="row align-items-end">
            <Box className="col">
              <Input
                label="Email"
                name="email"
                value={summary?.Account_Creation_Details?.ClientEmail || ""}
                disabled
              />
            </Box>
            <Box className="col col-auto pb-3">
              <Box
                className={`badge ${
                  summary?.Account_Creation_Details?.IsEmail_Verified === "Y"
                    ? "bg-success"
                    : "bg-danger"
                }`}
              >
                {summary?.Account_Creation_Details?.IsEmail_Verified === "Y"
                  ? "Verified"
                  : "Not Verified"}
              </Box>
            </Box>
            <Box className="col col-auto pb-3">
              <SingleAction
                css={{ borderRadius: 7 }}
                onClick={() => handleOpenModalAction("email")}
                title="Update Email"
              >
                <EditIcon color="black" height={14} width={14} />
              </SingleAction>
            </Box>
          </Box>
        </Box>

        <Heading
          name="General"
          tooltip="Update General Information"
          handleOpenModalAction={() => handleOpenModalAction("general")}
        />
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Gender"
            name="gender"
            value={summary?.Kyc_Details?.Gender_Text || ""}
            disabled
          />
        </Box>
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Marital Status"
            name="maritalStatus"
            value={summary?.Kyc_Details?.MaritalStatus_Text || ""}
            disabled
          />
        </Box>
        {/* <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Father's Name"
            name="motherName"
            value={summary?.Account_Init_Details?.FatherName || ""}
            disabled
          />
        </Box> */}
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Mother's Name"
            name="motherName"
            value={summary?.Kyc_Details?.MotherName || ""}
            disabled
          />
        </Box>
        {/* <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Aadhar Number"
            name="aadharNo"
            value={summary?.POA_Details?.Uid || ""}
            disabled
          />
        </Box> */}
        {/* <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Date of Birth"
            name="DateofBirth"
            value={summary?.Account_Init_Details?.DOB || ""}
            disabled
          />
        </Box> */}
        {/* <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Address Type"
            name="AddressType"
            value={summary?.Kyc_Details?.Address_Type_Text || ""}
            disabled
          />
        </Box> */}
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Occupation"
            name="Occupation"
            value={summary?.Kyc_Details?.Occupation_Text || ""}
            disabled
          />
        </Box>
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Annual Income"
            name="AnnualIncome"
            value={summary?.Kyc_Details?.AnnualIncome_Text || ""}
            disabled
          />
        </Box>
        {/* <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Source Of Income"
            name="SourceOfIncome"
            value={summary?.Kyc_Details?.SourceOfIncome_Text || ""}
            disabled
          />
        </Box> */}
      </Box>
      <Box>
        <Heading
          name="Nominee(s)"
          tooltip="Update Nominee(s)"
          handleOpenModalAction={() => handleOpenModalAction("nominee")}
        />
        {summary?.Nominee_Details?.map((item: any, index: number) => (
          <React.Fragment key={index}>
            {index !== 0 && <Divider />}
            <Box className="row">
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Nominee Name"
                  name="nomineeName"
                  value={item?.Nominee_Name || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Relation"
                  name="relation"
                  value={item?.Relation || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Nominee Percent"
                  name="percent"
                  value={item?.Nominee_Percent || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Date of Birth"
                  name="DOB"
                  value={item?.Nominee_DOB || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Nominee PAN"
                  name="Nominee_Pan"
                  value={item?.Nominee_Pan || ""}
                  disabled
                />
              </Box>
              {item.isMinor && (
                <Box className="col-md-4 col-sm-4 col-lg-4 mt-4">
                  Proof:
                  <a href={item?.Nominee_DOB_Proof_Path}>
                    {item?.Nominee_DOB_Proof}
                  </a>
                </Box>
              )}
              {item.isMinor && (
                <>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="Guardian Name"
                      name="gaurdianName"
                      value={item?.Guardian_Name}
                      disabled
                    />
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="Guardian PAN"
                      name="gaurdianPan"
                      value={item?.Guardian_Pan || ""}
                      disabled
                    />
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="Guardian Relation"
                      name="Guard_Nom_Relation"
                      value={item?.Guard_Nom_Relation || ""}
                      disabled
                    />
                  </Box>
                </>
              )}
            </Box>
          </React.Fragment>
        ))}
      </Box>
    </>
  );
};

export default PersonalInfo;
