import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import Heading from "./Heading";
import { UiTypePypes } from "../../ProfileTypes";
import { WEB_URL } from "App/utils/constants";

const orientation = {
  width: 400,
  height: 300,
};
interface MediaDataTypes {
  summary: any;
  handleOpenModalAction: (ui: UiTypePypes) => void;
}

const MediaData = ({ summary, handleOpenModalAction }: MediaDataTypes) => {
  //*main return
  return (
    <Box className="row">
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Heading
          name="Signature"
          tooltip="Update Signature"
          handleOpenModalAction={() => handleOpenModalAction("sign")}
        />
        <img
          src={
            summary?.Signature_Details?.SignaturePath_Fincart_WithoutBG ||
            WEB_URL + "/no_image.png"
          }
          {...orientation}
        ></img>
      </Box>
    </Box>
  );
};

export default MediaData;
