import { Button } from "@ui/Button/Button";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/DataGrid/DataGrid.styles";

import Box from "../../../../ui/Box/Box";

interface RequestListTypes {
  setOpen: (value: boolean) => void;
  formikObj: any;
  title: string;
}

//*main
const RequestList = ({ setOpen, formikObj, title }: RequestListTypes) => {
  const {
    values,
    setFieldValue,
    handleChange,
    handleSubmit,
    isSubmitting,
    errors,
    submitCount,
  } = formikObj;

  //*main return
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light">{title}</Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box className="modal-body p-3 modal-body-scroll">
        <Box className="container">
          <Box className="row mb-2 mt-2">
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Table className="table table-striped forthcoming">
                <ColumnParent>
                  <ColumnRow>
                    <Column style={{ fontSize: '0.9rem' }}>Request No.</Column>
                    <Column style={{ fontSize: '0.9rem' }}>Status</Column>
                    <Column className="text-center" style={{ fontSize: '0.9rem' }}>Documents</Column>
                  </ColumnRow>
                </ColumnParent>
                <DataParent className="modal-body-scroll">
                  {values?.list?.map((item: any, index: number) => (
                    <DataRow key={index}>
                      <DataCell className="nowrap">{item?.Request_ID}</DataCell>
                      <DataCell className="nowrap">{item?.Status}</DataCell>
                      <DataCell className="nowrap">
                        {item?.Documents?.map((i: any, idx: number) => (
                          <Box className="d-flex justify-content-between">
                            <Box>{i?.Doc_Name}</Box>
                            <Box>
                              <a href={i?.Doc_Path}>View</a>
                            </Box>
                          </Box>
                        ))}
                      </DataCell>
                    </DataRow>
                  ))}
                </DataParent>
              </Table>
            </Box>
            <Box css={{ pt: 10 }} className="text-end">
              <Button color="yellow" onClick={() => setOpen(false)}>
                OK
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default RequestList;
