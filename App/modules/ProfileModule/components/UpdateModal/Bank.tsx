import { Button } from "@ui/Button/Button";
import Input from "@ui/Input";
import SelectMenu from "@ui/Select/Select";
import Text from "@ui/Text/Text";
import Textarea from "@ui/Textarea";
import { fetchBankDetails } from "App/api/profile";
import { toastAlert } from "App/shared/components/Layout";
import { convertBase64, encryptData, getArrayFromKey } from "App/utils/helpers";

import Box from "../../../../ui/Box/Box";
import useProfileStore from "../../store";
interface BankTypes {
  setOpen: (value: boolean) => void;
  formikObj: any;
}

//*main
const Bank = ({ setOpen, formikObj }: BankTypes) => {
  const {
    values,
    setFieldValue,
    handleChange,
    handleSubmit,
    isSubmitting,
    errors,
    submitCount,
  } = formikObj;

  const { KYC_CAF_MASTER } = useProfileStore();

  //* functions
  const fetchBank = async (ifsc: string) => {
    try {
      if (ifsc.length === 11) {
        const enc: any = encryptData(ifsc, true);
        const result: any = await fetchBankDetails(enc);
        console.log({ result });
        setFieldValue("bankName", result?.data?.BankName || "");
        setFieldValue(
          "bankCode",
          getArrayFromKey(KYC_CAF_MASTER, "Bank_list")?.filter(
            (i: any) => i.Text === result?.data?.BankName
          )?.[0]?.Value || null
        );
        setFieldValue(
          "bankAddress",
          result?.data?.BankAddress || ""
        );
        setFieldValue("branch", result?.data?.Branch || "");
        setFieldValue("city", result?.data?.City || "");
        setFieldValue("MICR", result?.data?.MICR || "");
      }
    } catch (error) {
      console.log(error);
      return toastAlert("error", error);
    }
  };

  //*main return
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light">Change Bank Request</Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box className="modal-body p-3 modal-body-scroll">
        <Box className="container">
          <Box className="row mb-2 mt-2">
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <Input
                label="IFSC"
                name="IFSC"
                value={values?.IFSC || ""}
                onChange={(e: any) => {
                  handleChange(e);
                  if (!errors?.IFSC) {
                    fetchBank(e.target.value);
                  }
                }}
                required
                error={errors.IFSC || null}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <SelectMenu
                items={getArrayFromKey(KYC_CAF_MASTER, "Bank_list")}
                bindValue={"Value"}
                bindName={"Text"}
                label={"Bank"}
                placeholder={"Select Bank"}
                name="bankCode"
                value={values.bankCode}
                onChange={(e: any) => {
                  setFieldValue("bankCode", e?.Value || null);
                }}
                required
                error={submitCount ? errors.bankCode : null}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <Input
                label="Account Number"
                name="accountNumber"
                value={values?.accountNumber || ""}
                onChange={handleChange}
                required
                error={submitCount ? errors.accountNumber : null}
              />
            </Box>
            <Box className="col-md-8 col-sm-8 col-lg-8">
              <Textarea
                label="Bank Address"
                placeholder="Bank Address"
                name="bankAddress"
                value={values?.bankAddress}
                onChange={handleChange}
                required
                error={submitCount ? errors.bankAddress : null}
              ></Textarea>
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <Input
                label="Branch"
                name="branch"
                value={values?.branch || ""}
                onChange={handleChange}
                required
                error={submitCount ? errors.branch : null}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <Input
                label="City"
                name="city"
                value={values?.city || ""}
                onChange={handleChange}
                required
                error={submitCount ? errors.city : null}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <Input
                label="MICR"
                name="MICR"
                value={values?.MICR || ""}
                onChange={handleChange}
                required
                error={submitCount ? errors.MICR : null}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <Input
                label="Name as per bank"
                name="nameAsPerBank"
                value={values?.nameAsPerBank || ""}
                onChange={handleChange}
                required
                error={submitCount ? errors.nameAsPerBank : null}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <SelectMenu
                items={getArrayFromKey(KYC_CAF_MASTER, "AccountType_list")}
                bindValue={"Value"}
                bindName={"Text"}
                label={"Account Type"}
                placeholder={"Select Account Type"}
                name="accountType"
                value={values.accountType}
                onChange={(e: any) => {
                  setFieldValue("accountType", e?.Value || null);
                }}
                required
                error={submitCount ? errors.accountType : null}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <SelectMenu
                items={getArrayFromKey(KYC_CAF_MASTER, "KYCDocs_list")?.filter(
                  (i: any) => i.Category === "01_BankChange"
                )}
                bindValue={"Value"}
                bindName={"Text"}
                label={"Bank Proof Type"}
                placeholder={"Select Bank Proof Type"}
                name="bankProofType"
                value={values.bankProofType}
                onChange={(e: any) => {
                  setFieldValue("bankProofType", e.Value);
                }}
                required
                error={submitCount ? errors.bankProofType : null}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <Text color="mediumBlue" size="h6" space="letterspace">
                Upload Bank Proof Document
                <span className="text-danger">*</span>
              </Text>
              <Input
                type="file"
                className="form-control"
                name="document"
                accept=".jpg,.png,.pdf"
                onChange={async (e: React.ChangeEvent<HTMLInputElement>) => {
                  try {
                    const file: any = e?.target?.files?.[0] || null;
                    const base64 = await convertBase64(file);
                    setFieldValue("document", base64);
                  } catch (error: any) {
                    toastAlert("error", error);
                  }
                }}
                required
                error={submitCount ? errors.document : null}
              />
            </Box>
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Textarea
                label="Reason for bank change"
                placeholder="Reason for bank change"
                name="reason"
                value={values?.reason}
                onChange={handleChange}
                required
                error={submitCount ? errors.reason : null}
              ></Textarea>
            </Box>
            <Box css={{ pt: 10 }} className="text-end">
              <Button color="yellow" onClick={() => setOpen(false)}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={handleSubmit}
                loading={isSubmitting}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default Bank;
