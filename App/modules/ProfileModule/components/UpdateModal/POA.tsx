import { Button } from "@ui/Button/Button";
import Checkbox from "@ui/Checkbox/Checkbox";
import Input from "@ui/Input";
import SelectMenu from "@ui/Select/Select";
import Text from "@ui/Text/Text";
import Textarea from "@ui/Textarea";
import { fetchPincodeDetails } from "App/api/profile";
import { toastAlert } from "App/shared/components/Layout";
//import { NRI_PROFILE_TYPE_VALUE } from "App/utils/constants";
import { convertBase64, encryptData, getArrayFromKey } from "App/utils/helpers";
import Box from "../../../../ui/Box/Box";
import useProfileStore from "../../store";
interface POATypes {
  setOpen: (value: boolean) => void;
  formikObj: any;
  setisSameLocal: (value: boolean) => void;
  setisDoc2: (value: boolean) => void;
  isNRI: boolean;
}

//*main
const POA = ({ setOpen, formikObj, setisSameLocal, setisDoc2, isNRI }: POATypes) => {
  const {
    values,
    setFieldValue,
    handleChange,
    handleSubmit,
    isSubmitting,
    errors,
    submitCount,
  } = formikObj;

  const { KYC_CAF_MASTER } = useProfileStore();
  //*main return
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light">Change Address Request</Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box className="modal-body p-3 modal-body-scroll">
        <Box className="container">
          <Box className="row mb-2 mt-2">
            <Box className="col-md-12 col-sm-12 col-lg-12">
              {/* @ts-ignore */}
              <Text color="primary">
                {isNRI ? "Indian Address" : "Permanent Address Details"}
              </Text>
            </Box>
          </Box>
          <Box className="row">
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Textarea
                label="Address"
                placeholder="Address"
                name="address"
                value={values?.address}
                onChange={handleChange}
                required
                error={submitCount ? errors.address : null}
              ></Textarea>
            </Box>
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <Input
                type="number"
                label="Pincode"
                name="pincode"
                value={values?.pincode || ""}
                maxLength={6}
                onChange={async (e: any) => {
                  handleChange(e);
                  const value: string = e?.target?.value || "";
                  if (value.length === 6) {
                    try {
                      const enc: any = encryptData(value, true);
                      const result: any = await fetchPincodeDetails(enc);
                      setFieldValue("city", result?.data?.city || "Other");
                      setFieldValue("state", result?.data?.state || "Other");
                      setFieldValue("country", result?.data?.country || "India");
                    } catch (error) {
                      console.log(error);
                      return toastAlert("error", error);
                    }
                  }
                }}
                required
                error={errors.pincode}
              />
            </Box>
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <Input
                label="State"
                name="state"
                value={values?.state || ""}
                onChange={handleChange}
              />
            </Box>
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <Input
                label="City"
                name="city"
                value={values?.city || ""}
                onChange={handleChange}
              />
            </Box>
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <Input
                label="Country"
                name="country"
                value={values?.country || ""}
                onChange={handleChange}
              />
            </Box>
            <Box className="col-md-4 col-sm-4 col-lg-4">
              <SelectMenu
                // items={(values?.profile_type == NRI_PROFILE_TYPE_VALUE) ? getArrayFromKey(KYC_CAF_MASTER, "KYCDocs_list")?.filter(
                //   (i: any) => i.Category === "24_AddressChangeIndian") : getArrayFromKey(KYC_CAF_MASTER, "KYCDocs_list")?.filter(
                //     (i: any) => i.Category === "01_AddressChange")}
                items={getArrayFromKey(KYC_CAF_MASTER, "KYCDocs_list")?.filter(
                  (i: any) => i.Category === (isNRI ? "24_AddressChangeIndian" : "01_AddressChange"))}
                bindValue={"Value"}
                bindName={"Text"}
                label={"Address Proof Type"}
                placeholder={"Address Proof Type"}
                name="proofType1"
                value={values.proofType1}
                onChange={(e: any) => {
                  setFieldValue("proofType1", e?.Value || null);
                  setisDoc2(e.Value === "03" ? true : false);
                }}
                required
                error={submitCount ? errors.proofType1 : null}
              />
            </Box>

            <Box className="col-md-4 col-sm-4 col-lg-4">
              <Text color="mediumBlue" size="h6" space="letterspace">
                Upload Address Proof (Front)
                <span className="text-danger">*</span>
              </Text>
              <Input
                type="file"
                className="form-control"
                name="document1"
                accept=".jpg,.png"
                onChange={async (e: React.ChangeEvent<HTMLInputElement>) => {
                  try {
                    const file: any = e?.target?.files?.[0] || null;
                    const base64 = await convertBase64(file);
                    setFieldValue("document1", base64);
                  } catch (error: any) {
                    toastAlert("error", error);
                  }
                }}
                required
                error={submitCount ? errors.document1 : null}
              />
            </Box>
            {values?.proofType1 === "03" && (
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Text color="mediumBlue" size="h6" space="letterspace">
                  Upload Address Proof (Back)
                  <span className="text-danger">*</span>
                </Text>
                <Input
                  type="file"
                  className="form-control"
                  name="document2"
                  onChange={async (e: React.ChangeEvent<HTMLInputElement>) => {
                    try {
                      const file: any = e?.target?.files?.[0] || null;
                      const base64 = await convertBase64(file);
                      setFieldValue("document2", base64);
                    } catch (error: any) {
                      toastAlert("error", error);
                    }
                  }}
                  required
                  error={submitCount ? errors.document2 : null}
                />
              </Box>
            )}
          </Box>
          {!isNRI &&
            <Box className="row">
              <Box className="col-md-12 col-sm-12 col-lg-12 mb-3">
                <Checkbox
                  label="Is Correspondence Address Same As Permanent Address?"
                  id="isSame"
                  name="isSame"
                  checked={values?.isSame}
                  onChange={(e: any) => {
                    setFieldValue("isSame", e?.target?.checked);
                    setisSameLocal(e?.target?.checked);
                  }}
                />
              </Box>
            </Box>
          }
          {(!values?.isSame || isNRI) && (
            <>
              <Box className="row my-2">
                <Box className="col-md-12 col-sm-12 col-lg-12">
                  {/* @ts-ignore */}
                  <Text color="primary">
                    {isNRI ? "Overseas Address" : "Correspondence Address Details"}
                  </Text>
                </Box>
              </Box>
              <Box className="row">
                <Box className="col-md-12 col-sm-12 col-lg-12">
                  <Textarea
                    label="Address"
                    placeholder="Address"
                    name="corspAddress"
                    value={values?.corspAddress}
                    onChange={handleChange}
                    required
                    error={submitCount ? errors.corspAddress : null}
                  ></Textarea>
                </Box>
                <Box className="col-md-3 col-sm-3 col-lg-3">
                  <Input
                    type="number"
                    label="pincode"
                    name="corspPincode"
                    value={values?.corspPincode || ""}
                    maxLength={6}
                    onChange={async (e: any) => {
                      handleChange(e);
                      const value: string = e?.target?.value || "";
                      if (value.length === 6) {
                        try {
                          const enc: any = encryptData(value, true);
                          const result: any = await fetchPincodeDetails(enc);
                          setFieldValue(
                            "corspCity",
                            result?.data?.city || "Other"
                          );
                          setFieldValue(
                            "corspState",
                            result?.data?.state || "Other"
                          );
                          setFieldValue(
                            "corspCountry",
                            result?.data?.country || "India"
                          );
                        } catch (error) {
                          console.log(error);
                          return toastAlert("error", error);
                        }
                      }
                    }}
                    required
                    error={errors.corspPincode}
                  />
                </Box>
                <Box className="col-md-3 col-sm-3 col-lg-3">
                  <Input
                    label="State"
                    name="corspState"
                    value={values?.corspState || ""}
                    onChange={handleChange}
                  />
                </Box>
                <Box className="col-md-3 col-sm-3 col-lg-3">
                  <Input
                    label="City"
                    name="corspCity"
                    value={values?.corspCity || ""}
                    onChange={handleChange}
                  />
                </Box>
                <Box className="col-md-3 col-sm-3 col-lg-3">
                  <Input
                    label="Country"
                    name="corspCountry"
                    value={values?.corspCountry || ""}
                    onChange={handleChange}
                  />
                </Box>
                {isNRI && <>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <SelectMenu
                      items={getArrayFromKey(KYC_CAF_MASTER, "KYCDocs_list")?.filter(
                        (i: any) => i.Category === "24_AddressChangeOverseas")}
                      bindValue={"Value"}
                      bindName={"Text"}
                      label={"Address Proof Type"}
                      placeholder={"Address Proof Type"}
                      name="proofType2"
                      value={values.proofType2}
                      onChange={(e: any) => {
                        setFieldValue("proofType2", e?.Value || null);
                      }}
                      required
                      error={submitCount ? errors.proofType2 : null}
                    />
                  </Box>

                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Text color="mediumBlue" size="h6" space="letterspace">
                      Upload Address Proof
                      <span className="text-danger">*</span>
                    </Text>
                    <Input
                      type="file"
                      className="form-control"
                      name="document2"
                      accept=".jpg,.png"
                      onChange={async (e: React.ChangeEvent<HTMLInputElement>) => {
                        try {
                          const file: any = e?.target?.files?.[0] || null;
                          const base64 = await convertBase64(file);
                          setFieldValue("document2", base64);
                        } catch (error: any) {
                          toastAlert("error", error);
                        }
                      }}
                      required
                      error={submitCount ? errors.document2 : null}
                    />
                  </Box>
                </>}
              </Box>
            </>
          )}

          <Box className="row">
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Textarea
                label="Reason for address change"
                placeholder="Reason for address change"
                name="reason"
                value={values?.reason}
                onChange={handleChange}
                required
                error={submitCount ? errors.reason : null}
              ></Textarea>
            </Box>
          </Box>

          <Box className="row mb-2">
            <Box css={{ pt: 10 }} className="text-end">
              <Button color="yellow" onClick={() => setOpen(false)}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={handleSubmit}
                loading={isSubmitting}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box >
  );
};
export default POA;
