import { Button } from "@ui/Button/Button";
import Checkbox from "@ui/Checkbox/Checkbox";
import Input from "@ui/Input";
import SelectMenu from "@ui/Select/Select";
import Text from "@ui/Text/Text";
import { getArrayFromKey } from "App/utils/helpers";
import { FieldArray } from "formik";
import React from "react";

import Box from "../../../../ui/Box/Box";
import useProfileStore from "../../store";

//*CONSTANTS
const TEXT_COUNT = ["First", "Second", "Third"];

interface POATypes {
  setOpen: (value: boolean) => void;
  formikObj: any;
  setisOtherLocal: (value: boolean) => void;
}

//*main
const POA = ({ setOpen, formikObj, setisOtherLocal }: POATypes) => {
  const {
    values,
    setFieldValue,
    handleChange,
    handleSubmit,
    isSubmitting,
    errors,
    submitCount,
  } = formikObj;

  const { KYC_CAF_MASTER } = useProfileStore();

  //*main return
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light">Update Fatca</Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box className="modal-body p-3">
        <Box className="container">
          <Box className="row mt-2">
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <Input
                label="Birth Place"
                name="Place_Of_Birth"
                value={values?.Place_Of_Birth || ""}
                onChange={handleChange}
                required
                error={submitCount ? errors.Place_Of_Birth : null}
              />
            </Box>
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <SelectMenu
                items={getArrayFromKey(KYC_CAF_MASTER, "Country_list")}
                bindValue={"Text"}
                bindName={"Text"}
                label="Country"
                placeholder="Country"
                name="Country_Of_Birth"
                value={values?.Country_Of_Birth}
                onChange={(e: any) => {
                  setFieldValue("Country_Of_Birth", e?.Text || null);
                }}
                required
                error={submitCount ? errors.Country_Of_Birth : null}
              />
            </Box>
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <SelectMenu
                items={getArrayFromKey(KYC_CAF_MASTER, "SourceOfIncome_list")}
                bindValue={"Value"}
                bindName={"Text"}
                label="Source of Wealth"
                placeholder="Source of Wealth"
                name="Source_Of_Wealth"
                value={values.Source_Of_Wealth}
                onChange={(e: any) => {
                  setFieldValue("Source_Of_Wealth", e?.Value || null);
                }}
                required
                error={submitCount ? errors.Source_Of_Wealth : null}
              />
            </Box>

            <Box className="col-md-3 col-sm-3 col-lg-3">
              <SelectMenu
                items={getArrayFromKey(KYC_CAF_MASTER, "PEP_list")}
                bindValue={"Value"}
                bindName={"Text"}
                label="Political Exposed"
                placeholder="Political Exposed"
                name="Is_politically_Exposed"
                value={values.Is_politically_Exposed}
                onChange={(e: any) => {
                  setFieldValue("Is_politically_Exposed", e?.Value || null);
                }}
                required
                error={submitCount ? errors.Is_politically_Exposed : null}
              />
            </Box>
          </Box>
          <Box className="row">
            <Box className="col-md-12 col-sm-12 col-lg-12 mb-3">
              <Checkbox
                label="Tax country other than India?"
                id="Tax_Country_Other_Than_India"
                name="Is_Tax_Country_Other_Than_India"
                checked={values?.Is_Tax_Country_Other_Than_India}
                onChange={(e: any) => {
                  setFieldValue(
                    "Is_Tax_Country_Other_Than_India",
                    e?.target?.checked
                  );
                  setisOtherLocal(e?.target?.checked);
                }}
              />
            </Box>
          </Box>

          {values?.Is_Tax_Country_Other_Than_India && (
            // @ts-ignore
            <FieldArray name="Fatca_Other_Country_Details">
              {({ insert, remove, push }) => (
                <React.Fragment>
                  {values?.Fatca_Other_Country_Details?.length > 0 &&
                    values?.Fatca_Other_Country_Details?.map(
                      (item: any, index: number) => (
                        <>
                          <Box className="row my-2">
                            <Box className="col-md-12 col-sm-12 col-lg-12 d-flex justify-content-between">
                              {/* @ts-ignore */}
                              <Text color="primary">
                                {TEXT_COUNT[index]} Tax Residency
                              </Text>
                              {index != 0 && (
                                <Button
                                  color="yellow"
                                  onClick={() => remove(index)}
                                >
                                  Remove
                                </Button>
                              )}
                            </Box>
                          </Box>
                          <Box className="row">
                            <Box className="col-md-4 col-sm-4 col-lg-4">
                              <SelectMenu
                                items={getArrayFromKey(
                                  KYC_CAF_MASTER,
                                  "Country_list"
                                )}
                                bindValue={"Value"}
                                bindName={"Text"}
                                label="Tax Residency Country"
                                placeholder="Tax Residency Country"
                                name={`Fatca_Other_Country_Details.${index}.Country_TAX_RES`}
                                value={item?.Country_TAX_RES || ""}
                                onChange={(e: any) => {
                                  setFieldValue(
                                    `Fatca_Other_Country_Details.${index}.Country_TAX_RES`,
                                    e?.Value || null
                                  );
                                }}
                                required
                                error={
                                  submitCount
                                    ? errors?.Fatca_Other_Country_Details?.[index]
                                      ?.Country_TAX_RES
                                    : null
                                }
                              />
                            </Box>
                            <Box className="col-md-4 col-sm-4 col-lg-4">
                              <SelectMenu
                                items={getArrayFromKey(
                                  KYC_CAF_MASTER,
                                  "TaxIdType_list"
                                )}
                                bindValue={"Value"}
                                bindName={"Text"}
                                label="Identification Type"
                                placeholder="Identification Type"
                                name={`Fatca_Other_Country_Details.${index}.Iden_Type`}
                                value={item?.Iden_Type || ""}
                                onChange={(e: any) => {
                                  setFieldValue(
                                    `Fatca_Other_Country_Details.${index}.Iden_Type`,
                                    e?.Value || null
                                  );
                                }}
                                required
                                error={
                                  submitCount
                                    ? errors?.Fatca_Other_Country_Details?.[index]
                                      ?.Iden_Type
                                    : null
                                }
                              />
                            </Box>
                            <Box className="col-md-4 col-sm-4 col-lg-4">
                              <Input
                                label="Identification Number"
                                placeholder="Identification Number"
                                name={`Fatca_Other_Country_Details.${index}.Country_Payer_Id`}
                                value={item?.Country_Payer_Id || ""}
                                onChange={handleChange}
                                required
                                error={
                                  submitCount
                                    ? errors?.Fatca_Other_Country_Details?.[index]
                                      ?.Country_Payer_Id
                                    : null
                                }
                              />
                            </Box>
                          </Box>
                        </>
                      )
                    )}

                  <Box className="row">
                    <Box css={{ pt: 10 }}>
                      <Button
                        color="yellow"
                        onClick={() => push({})}
                        disabled={values?.Fatca_Other_Country_Details?.length > 2}
                      >
                        Add More Tax Residency
                      </Button>
                    </Box>
                  </Box>
                </React.Fragment>
              )}
            </FieldArray>
          )}
          <Box className="row mb-2">
            <Box css={{ pt: 10 }} className="text-end">
              <Button color="yellow" onClick={() => setOpen(false)}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={handleSubmit}
                loading={isSubmitting}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default POA;
