import { Button } from "@ui/Button/Button";
import Divider from "@ui/Divider/Divider";
import Input from "@ui/Input";
import SelectMenu from "@ui/Select/Select";
import Text from "@ui/Text/Text";
import { toastAlert } from "App/shared/components/Layout";
import {
  calculateAge,
  convertBase64,
  getArrayFromKey,
} from "App/utils/helpers";
import { FieldArray } from "formik";
import React, { useEffect } from "react";
import Box from "../../../../ui/Box/Box";
import useProfileStore from "../../store";
interface NomineeTypes {
  setOpen: (value: boolean) => void;
  formikObj: any;
}

//*main
const Nominee = ({ setOpen, formikObj }: NomineeTypes) => {
  const { KYC_CAF_MASTER } = useProfileStore();

  const {
    values,
    setFieldValue,
    handleChange,
    handleSubmit,
    isSubmitting,
    errors,
    submitCount,
  } = formikObj;

  console.log("NomValues", values);

  const nomineeObj: any = {
    nomineeName: "",
    relation: "",
    nomineeDOB: "",
    percent: "",
    gaurdianName: "",
    gaurdianPan: "",
  };

  //*useEffects

  //*main return
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light">Update Nominee(s)</Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box className="modal-body p-3 modal-body-scroll">
        <Box className="container">
          {/* @ts-ignore */}
          <FieldArray name="nomineeList">
            {({ insert, remove, push }) => (
              <>
                {values?.nomineeList?.length > 0 &&
                  values?.nomineeList?.map((item: any, index: number) => (
                    <React.Fragment key={index}>
                      {index !== 0 && <Divider />}
                      <Box className="row">
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Nominee Name"
                            placeholder="Nominee Name"
                            name={`nomineeList.${index}.nomineeName`}
                            value={item?.nomineeName || ""}
                            onChange={handleChange}
                            required
                            error={
                              submitCount
                                ? errors?.nomineeList?.[index]?.nomineeName
                                : null
                            }
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Relation"
                            placeholder="Relation"
                            name={`nomineeList.${index}.relation`}
                            value={item?.relation || ""}
                            onChange={handleChange}
                            required
                            error={
                              submitCount
                                ? errors?.nomineeList?.[index]?.relation
                                : null
                            }
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            type="number"
                            label="Nominee Percent (%)"
                            placeholder="Nominee Percent"
                            name={`nomineeList.${index}.percent`}
                            value={item?.percent || ""}
                            onChange={handleChange}
                            required
                            error={
                              submitCount
                                ? errors?.nomineeList?.[index]?.percent
                                : null
                            }
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            type="date"
                            label="Date Of Birth"
                            placeholder="Date Of Birth"
                            name={`nomineeList.${index}.nomineeDOB`}
                            value={item?.nomineeDOB || ""}
                            onChange={handleChange}
                            required
                            error={
                              submitCount
                                ? errors?.nomineeList?.[index]?.nomineeDOB
                                : null
                            }
                          />
                        </Box>

                        {calculateAge(item?.nomineeDOB)?.years < 18 ? (
                          <>
                            <Box className="col-md-4 col-sm-4 col-lg-4">
                              <SelectMenu
                                items={getArrayFromKey(
                                  KYC_CAF_MASTER,
                                  "docSebiNominee"
                                )}
                                bindValue={"Value"}
                                bindName={"Text"}
                                label={"Nominee DOB Proof Type"}
                                placeholder={"Select Nominee DOB Proof Type"}
                                name={`nomineeList.${index}.Nominee_DOB_Proof_Type`}
                                value={item?.Nominee_DOB_Proof_Type}
                                onChange={(e: any) => {
                                  setFieldValue(
                                    `nomineeList.${index}.Nominee_DOB_Proof_Type`,
                                    e.Value
                                  );
                                }}
                                required
                                error={
                                  submitCount
                                    ? errors?.nomineeList?.[index]
                                      ?.Nominee_DOB_Proof_Type
                                    : null
                                }
                              />
                            </Box>

                            <Box className="col-md-4 col-sm-4 col-lg-4">
                              <Text
                                //@ts-ignore
                                color="mediumBlue"
                                size="h6"
                                space="letterspace"
                              >
                                Upload DOB Proof
                                <span className="text-danger">*</span>
                              </Text>
                              <Input
                                type="file"
                                className="form-control"
                                name={`nomineeList.${index}.document`}
                                accept=".jpg,.png"
                                onChange={async (
                                  e: React.ChangeEvent<HTMLInputElement>
                                ) => {
                                  try {
                                    const file: any =
                                      e?.target?.files?.[0] || null;
                                    const base64 = await convertBase64(file);
                                    setFieldValue(
                                      `nomineeList.${index}.document`,
                                      base64
                                    );
                                  } catch (error: any) {
                                    toastAlert("error", error);
                                  }
                                }}
                                required
                                error={
                                  submitCount
                                    ? errors?.nomineeList?.[index]?.document
                                    : null
                                }
                              />
                            </Box>
                            <Box className="col-md-4 col-sm-4 col-lg-4">
                              <Input
                                label="Guardian Name"
                                placeholder="Guardian Name"
                                name={`nomineeList.${index}.gaurdianName`}
                                value={item?.gaurdianName || ""}
                                onChange={handleChange}
                                required
                                error={
                                  submitCount
                                    ? errors?.nomineeList?.[index]?.gaurdianName
                                    : null
                                }
                              />
                            </Box>
                            <Box className="col-md-4 col-sm-4 col-lg-4">
                              <Input
                                label="Guardian PAN"
                                placeholder="Guardian PAN"
                                name={`nomineeList.${index}.gaurdianPan`}
                                value={item?.gaurdianPan || ""}
                                onChange={handleChange}
                                required
                                error={
                                  submitCount
                                    ? errors?.nomineeList?.[index]?.gaurdianPan
                                    : null
                                }
                              />
                            </Box>
                            <Box className="col-md-4 col-sm-4 col-lg-4">
                              <SelectMenu
                                items={getArrayFromKey(
                                  KYC_CAF_MASTER,
                                  "relationlistSebiNominee"
                                )}
                                bindValue={"Value"}
                                bindName={"Text"}
                                label={"Gua. Rel. With Nominee"}
                                placeholder={"Select Gua. Rel. With Nominee"}
                                name={`nomineeList.${index}.Guardian_Rel_Type`}
                                value={item?.Guardian_Rel_Type}
                                onChange={(e: any) => {
                                  setFieldValue(
                                    `nomineeList.${index}.Guardian_Rel_Type`,
                                    e.Value
                                  );
                                }}
                              />
                            </Box>
                          </>
                        ) : (
                          <Box className="col-md-4 col-sm-4 col-lg-4">
                            <Input
                              label="Nominee PAN"
                              placeholder="Nominee PAN"
                              name={`nomineeList.${index}.Nominee_Pan`}
                              value={item?.Nominee_Pan || ""}
                              onChange={handleChange}
                              required
                              error={
                                submitCount
                                  ? errors?.nomineeList?.[index]?.Nominee_Pan
                                  : null
                              }
                            />
                          </Box>
                        )}
                      </Box>
                      {values?.nomineeList?.length > 1 && (
                        <Box key={index}>
                          <Button color="yellow" onClick={() => remove(index)}>
                            Remove
                          </Button>
                        </Box>
                      )}
                    </React.Fragment>
                  ))}

                <Box css={{ pt: 10, mb: 10 }} className="text-end">
                  <Button
                    color="yellow"
                    onClick={() => push(nomineeObj)}
                    disabled={values?.nomineeList?.length === 3}
                  >
                    Add Nominee
                  </Button>
                  <Button color="yellow" onClick={() => setOpen(false)}>
                    Cancel
                  </Button>
                  <Button
                    type="submit"
                    color="yellow"
                    onClick={handleSubmit}
                    loading={isSubmitting}
                  >
                    Submit
                  </Button>
                </Box>
              </>
            )}
          </FieldArray>
        </Box>
      </Box>
    </Box>
  );
};
export default Nominee;
