import { Button } from "@ui/Button/Button";
import Input from "@ui/Input";
import Text from "@ui/Text/Text";
import { toastAlert } from "App/shared/components/Layout";
import { convertBase64 } from "App/utils/helpers";
import React, { useState } from "react";
import Box from "../../../../ui/Box/Box";
interface SignTypes {
  setOpen: (value: boolean) => void;
  formikObj: any;
}

//*main
const Sign = ({ setOpen, formikObj }: SignTypes) => {
  const {
    values,
    setFieldValue,
    handleChange,
    handleSubmit,
    isSubmitting,
    errors,
    submitCount,
  } = formikObj;


  //*main return
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light">Update Signature</Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box className="modal-body p-3 modal-body-scroll">
        <Box className="container">
          <Box className="row mb-2 mt-2">
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Text color="mediumBlue" size="h6" space="letterspace">
                Upload Signature
                <span className="text-danger">*</span>
              </Text>
              <Input
                type="file"
                className="form-control"
                name="document"
                accept=".jpg,.png"
                onChange={async (e: React.ChangeEvent<HTMLInputElement>) => {
                  try {
                    const file: any = e?.target?.files?.[0] || null;
                    const base64 = await convertBase64(file);
                    setFieldValue("document", base64);
                  } catch (error: any) {
                    toastAlert("error", error);
                  }
                }}
                required
                error={submitCount ? errors.document : null}
              />
            </Box>
            <Box css={{ pt: 10 }} className="text-end">
              <Button color="yellow" onClick={() => setOpen(false)}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={handleSubmit}
                loading={isSubmitting}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default Sign;
