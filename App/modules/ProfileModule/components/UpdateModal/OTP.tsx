import { Button } from "@ui/Button/Button";
import Input from "@ui/Input";
import Box from "../../../../ui/Box/Box";
interface OTPTypes {
  setOpen: (value: boolean) => void;
  formikObj: any;
}

//*main
const OTP = ({ setOpen, formikObj }: OTPTypes) => {
  const {
    values,
    handleChange,
    handleSubmit,
    isSubmitting,
    errors,
    submitCount,
  } = formikObj;

  //*main return
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light">OTP Verification</Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box
        className="modal-body p-3 modal-body-scroll"
        css={{ fontSize: "0.8rem" }}
      >
        <Box className="container">
          <Box className="row mb-2">
            <Box className="col col-12 col-md-6">
              <Input
                label="OTP"
                name="OTP"
                value={values?.OTP || ""}
                onChange={handleChange}
                placeholder="Enter OTP"
                required
                error={submitCount ? errors.OTP : null}
              />
            </Box>
            <Box css={{ pt: 10 }} className="text-end">
              <Button color="yellow" onClick={() => setOpen(false)}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={handleSubmit}
                loading={isSubmitting}
              >
                Verify OTP
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default OTP;
