import { Button } from "@ui/Button/Button";
import { GroupBox } from "@ui/Group/Group.styles";
import Input from "@ui/Input";
import Label from "@ui/Label/Label";
import { RadioGroup } from "@ui/Radio";
import Radio from "@ui/Radio/Radio";
import SelectMenu from "@ui/Select/Select";
import { getArrayFromKey } from "App/utils/helpers";
import React from "react";
import Box from "../../../../ui/Box/Box";
import useProfileStore from "../../store";
import Text from "@ui/Text/Text";
import style from "../../Profile.module.scss";
interface GeneralTypes {
  setOpen: (value: boolean) => void;
  formikObj: any;
}

//*main
const General = ({ setOpen, formikObj }: GeneralTypes) => {
  const {
    values,
    setFieldValue,
    handleChange,
    handleSubmit,
    isSubmitting,
    errors,
    submitCount,
  } = formikObj;

  const { KYC_CAF_MASTER } = useProfileStore();

  //*main return
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Text css={{ color: "var(--colors-blue1)" }}>Update General Information</Text>
      </Box>
      <Box className="modal-body p-3">
        <Box className="container">
          <Box className="row mb-2 mt-2">
            <Box className="col-md-6 col-sm-6 col-lg-6">
              <Label css={{ mb: 5 }} htmlFor="gender" label="Gender" />
              <GroupBox css={{ mb: 10 }}>
                <RadioGroup
                  defaultValue={values.gender}
                  className="inlineRadio"
                  name="gender"
                  onValueChange={(e: any) => {
                    setFieldValue("gender", e);
                  }}
                >
                  {getArrayFromKey(KYC_CAF_MASTER, "Gender_list")?.map(
                    (i: any, index: number) => (
                      <React.Fragment key={index}>
                        <Radio value={i.Value} label={i.Text} id={i.Value} />
                      </React.Fragment>
                    )
                  )}
                </RadioGroup>
              </GroupBox>
            </Box>
            <Box className="col-md-6 col-sm-6 col-lg-6">
              <Label
                css={{ mb: 5 }}
                htmlFor="maritalStatus"
                label="Marital Status"
              />
              <GroupBox css={{ mb: 10 }}>
                <RadioGroup
                  defaultValue={values.maritalStatus}
                  className="inlineRadio"
                  name="maritalStatus"
                  onValueChange={(e: any) => {
                    setFieldValue("maritalStatus", e);
                  }}
                >
                  {getArrayFromKey(KYC_CAF_MASTER, "MaritalStatus_list")?.map(
                    (i: any, index: number) => (
                      <React.Fragment key={index}>
                        <Radio value={i.Value} label={i.Text} id={i.Value} />
                      </React.Fragment>
                    )
                  )}
                </RadioGroup>
              </GroupBox>
            </Box>
            {/* <Box className="col-md-3 col-sm-3 col-lg-3">
            <Input
              label="Father's Name"
              name="fatherName"
              value={values?.fatherName || ""}
              onChange={handleChange}
              required
              error={submitCount ? errors.fatherName : null}
            />
          </Box> */}
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <Input
                label="Mother's Name"
                name="motherName"
                value={values?.motherName || ""}
                onChange={handleChange}
                required
                error={submitCount ? errors.motherName : null}
              />
            </Box>

            <Box className="col-md-3 col-sm-3 col-lg-3">
              <Input
                // type="number"
                label="Addhar Number"
                name="aadharNo"
                value={values?.aadharNo || ""}
                onChange={handleChange}
                required
                error={submitCount ? errors.aadharNo : null}
              />
            </Box>
            {/* <Box className="col-md-3 col-sm-3 col-lg-3">
            <Input
              type="date"
              label="Date of Birth"
              name="DateofBirth"
              value={values?.DateofBirth || ""}
              onChange={handleChange}
              required
              error={submitCount ? errors.DateofBirth : null}
            />
          </Box> */}
            {/* <Box className="col-md-3 col-sm-3 col-lg-3">
            <SelectMenu
              items={getArrayFromKey(KYC_CAF_MASTER, "AddressType_list")}
              bindValue={"Value"}
              bindName={"Text"}
              label={"AddressType"}
              placeholder={"Select AddressType"}
              name="AddressType"
              value={values.AddressType}
              onChange={(e: any) => {
                setFieldValue("AddressType", e.Value);
              }}
              required
              error={submitCount ? errors.AddressType : null}
            />
          </Box> */}
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <SelectMenu
                items={getArrayFromKey(KYC_CAF_MASTER, "Occupation_list")}
                bindValue={"Value"}
                bindName={"Text"}
                label={"Occupation"}
                placeholder={"Select Occupation"}
                name="Occupation"
                value={values.Occupation}
                onChange={(e: any) => {
                  setFieldValue("Occupation", e.Value);
                }}
                required
                error={submitCount ? errors.Occupation : null}
              />
            </Box>
            <Box className="col-md-3 col-sm-3 col-lg-3">
              <SelectMenu
                items={getArrayFromKey(KYC_CAF_MASTER, "AnnualIncome_list")}
                bindValue={"Value"}
                bindName={"Text"}
                label={"Annual Income"}
                placeholder={"Select Annual Income"}
                name="AnnualIncome"
                className={style.zindex}
                value={values.AnnualIncome}
                onChange={(e: any) => {
                  setFieldValue("AnnualIncome", e.Value);
                }}
                required
                error={submitCount ? errors.AnnualIncome : null}
              />
            </Box>
            {/* <Box className="col-md-3 col-sm-3 col-lg-3">
            <SelectMenu
              items={getArrayFromKey(KYC_CAF_MASTER, "SourceOfIncome_list")}
              bindValue={"Value"}
              bindName={"Text"}
              label={"Source Of Income"}
              placeholder={"Select Source Of Income"}
              name="SourceOfIncome"
              value={values.SourceOfIncome}
              onChange={(e: any) => {
                setFieldValue("SourceOfIncome", e.Value);
              }}
              required
              error={submitCount ? errors.SourceOfIncome : null}
            />
          </Box> */}

            <Box css={{ pt: 10 }} className="text-end">
              <Button color="yellow" onClick={() => setOpen(false)}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="yellow"
                onClick={handleSubmit}
                loading={isSubmitting}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default General;
