import React from "react";
import Box from "./../../../ui/Box/Box";
import MemberCard from "./MemberCard";
import OwnerMemberCard from "./OwnerMemberCard";

interface MemberListTypes {
  profile: any;
  handleProfileEdit: (obj: any) => void;
  handleProfilePicEdit: (obj: any, obj2: any) => void;
  profileLoader: boolean;
  basicId: any;
  kycSync: (obj: any) => void;
  resetKYC: (obj: any) => void;
}

const MemberList = ({
  profile,
  handleProfileEdit,
  handleProfilePicEdit,
  profileLoader,
  kycSync,
  resetKYC,
  basicId
}: MemberListTypes) => {
  //* Main Return
  return (
    <Box className="row">
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="row">
          {profile?.map((member: any, index: number) => (
            (member?.isGroupLeader === "N") ?
              <Box className="col-md-6 mb-4" key={index}>
                <MemberCard
                  index={index}
                  member={member}
                  handleProfileEdit={handleProfileEdit}
                  handleProfilePicEdit={handleProfilePicEdit}
                  profileLoader={profileLoader}
                  kycSync={kycSync}
                  basicId={basicId}
                  resetKYC={resetKYC}
                />
              </Box>
              :
              <Box className="col-md-6 mb-4" key={index}>
                <OwnerMemberCard
                  index={index}
                  member={member}
                  handleProfileEdit={handleProfileEdit}
                  handleProfilePicEdit={handleProfilePicEdit}
                  profileLoader={profileLoader}
                  kycSync={kycSync}
                  basicId={basicId}
                  resetKYC={resetKYC}
                />
              </Box>
          ))}
        </Box>
      </Box>
    </Box>
  );
};

export default MemberList;
