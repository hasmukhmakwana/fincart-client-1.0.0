import create from "zustand";
interface StoreTypes {
  selectedBasicID: number;
  KYC_CAF_MASTER: any[];
  setSelectedBasicID: (payload: number) => void;
  setKYC_CAF_MASTER: (payload: any[]) => void;
}

const useProfileStore = create<StoreTypes>((set) => ({
  //* initial state
  selectedBasicID: 0,
  KYC_CAF_MASTER: [],

  //* methods for manipulating state
  setSelectedBasicID: (payload) =>
    set((state) => ({
      ...state,
      selectedBasicID: payload,
    })),

  setKYC_CAF_MASTER: (payload) =>
    set((state) => ({
      ...state,
      KYC_CAF_MASTER: payload || [],
    })),
}));

export default useProfileStore;
