import { calculateAge } from "App/utils/helpers";
import * as Yup from "yup";

const checkAge = (age: string) => {
  return calculateAge(age)?.years < 18;
};

export const modalWidth: any = {
  mobile: "30%",
  email: "30%",
  OTP: "30%",
  general: "50%",
  nominee: "50%",
  bank: "50%",
  POI: "30%",
  POA: "50%",
  sign: "30%",
  fatca: "50%",
  bank_Requests: "30%",
  POA_Requests: "30%",
};

export const SCHEMAS: any = {
  mobile: {
    mobile: Yup.number()
      .required("Mobile is required")
      .min(6000000000, "invalid mobile number")
      .max(9999999999, "invalid mobile number"),
  },

  email: {
    email: Yup.string().required("Email is required").email("Invalid Email"),
  },

  otp: {
    OTP: Yup.string().required("OTP is required"),
  },

  general: {
    gender: Yup.string().required("Gender is required"),
    maritalStatus: Yup.string().required("Marital Status is required"),
    aadharNo: Yup.string()
      .required("Aadhar Number is required")
      .trim()
      .matches(/^[0-9]{12}$/, "Invalid Aadhar Number"),
    // fatherName: Yup.string().required("Father Name is required"),
    motherName: Yup.string().required("Mother Name is required"),
    // DateofBirth: Yup.string().required("DOB is required"),
    // AddressType: Yup.string().required("Address Type is required"),
    Occupation: Yup.string().required("Occupation is required"),
    AnnualIncome: Yup.string().required("Annual Income is required"),
    // SourceOfIncome: Yup.string().required("Source Of Income is required"),
  },
  nominee: {
    // nomineeList: Yup.array().of(
    //   Yup.object().shape({
    //     nomineeName: Yup.string().required("Nominee Name is required"),
    //     relation: Yup.string().required("Relation is required"),
    //     nomineeDOB: Yup.string().required("DOB is required"),
    //     percent: Yup.string().required("Nominee Percent(%) is required"),
    //   })
    // ),

    nomineeList: Yup.array().of(
      Yup.object().shape({
        nomineeName: Yup.string().required("Nominee Name is required"),
        relation: Yup.string().required("Relation is required"),
        nomineeDOB: Yup.string().required("DOB is required"),
        percent: Yup.string().required("Nominee Percent(%) is required"),
        document: Yup.string().when("nomineeDOB", {
          is: (value: string) => checkAge(value),
          then: Yup.string().required("DOB Proof is required").nullable(),
        }),
        Nominee_DOB_Proof_Type: Yup.string().when("nomineeDOB", {
          is: (value: string) => checkAge(value),
          then: Yup.string().required("DOB Proof Type is required"),
        }),
        gaurdianName: Yup.string().when("nomineeDOB", {
          is: (value: string) => checkAge(value),
          then: Yup.string().required("Gaurdian Name is required").nullable(),
        }),
        gaurdianPan: Yup.string().when("nomineeDOB", {
          is: (value: string) => checkAge(value),
          then: Yup.string().required("Gaurdian PAN is required").nullable(),
        }),
        Nominee_Pan: Yup.string().when("nomineeDOB", {
          is: (value: string) => !checkAge(value),
          then: Yup.string().required("Nominee Pan is required"),
        }),
      })
    ),
  },
  bank: {
    IFSC: Yup.string()
      .required("IFSC is required")
      .trim()
      .matches(/^[A-Za-z]{4}[0-9]{6,7}$/, "Invalid IFSC"),
    bankCode: Yup.string().required("Bank is required").nullable(),
    accountNumber: Yup.string().required("Account Number is required"),
    bankAddress: Yup.string().required("Bank Address is required"),
    branch: Yup.string().required("Branch is required"),
    city: Yup.string().required("City is required"),
    MICR: Yup.string().required("MICR is required"),
    nameAsPerBank: Yup.string().required("Name as per bank is required"),
    accountType: Yup.string().required("Account Type is required").nullable(),
    bankProofType: Yup.string()
      .required("Bank Proof Type is required")
      .nullable(),
    document: Yup.string().required("Document is required").nullable(),
    reason: Yup.string().required("Reason is required"),
  },

  poi: {
    document: Yup.string().required("PAN Card is required").nullable(),
  },

  poa: {
    document1: Yup.string().required("Document is required").nullable(),
    proofType1: Yup.string().required("Proof Type is required"),
    address: Yup.string().required("Address is required"),
    pincode: Yup.string()
      .required("Pincode is required")
      .trim()
      .matches(/^[1-9][0-9]{5}$/, "Invalid Pincode"),
    reason: Yup.string().required("Reason is required"),
  },
  poa_same: {
    corspAddress: Yup.string().required("Address is required"),
    corspPincode: Yup.string()
      .required("Pincode is required")
      .trim()
      .matches(/^[1-9][0-9]{5}$/, "Invalid Pincode"),
  },
  poa_doc2: {
    document2: Yup.string().required("Document is required").nullable(),
  },

  poa_nri: {
    corspAddress: Yup.string().required("Address is required"),
    corspPincode: Yup.string()
      .required("Pincode is required")
      .trim()
      .matches(/^[1-9][0-9]{5}$/, "Invalid Pincode"),
    proofType2: Yup.string().required("Proof Type is required"),
    document2: Yup.string().required("Document is required").nullable(),
  },

  sign: {
    document: Yup.string().required("Signature is required"),
  },

  fatca: {
    Place_Of_Birth: Yup.string().required("Birth Place is required"),
    Country_Of_Birth: Yup.string()
      .required("Birth Country is required")
      .nullable(),
    Source_Of_Wealth: Yup.string()
      .required("Source of Wealth is required")
      .nullable(),
    Is_politically_Exposed: Yup.string()
      .required("Politically Exposed is required")
      .nullable(),
  },
  fatca_other: {
    Fatca_Other_Country_Details: Yup.array().of(
      Yup.object().shape({
        Country_TAX_RES: Yup.string()
          .required("Tax Residency Country is required")
          .nullable(),
        Iden_Type: Yup.string()
          .required("Identification Type is required")
          .nullable(),
        Country_Payer_Id: Yup.string().required(
          "Identification Number is required"
        ),
      })
    ),
  },
};
