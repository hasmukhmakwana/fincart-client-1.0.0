export type UiTypePypes =
  | "mobile"
  | "email"
  | "OTP"
  | "general"
  | "nominee"
  | "bank"
  | "POI"
  | "POA"
  | "sign"
  | "fatca"
  | "bank_Requests"
  | "POA_Requests";
