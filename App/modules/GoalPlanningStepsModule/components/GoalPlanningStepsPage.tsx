import Badge from "@ui/Badge/Badge";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import EditIcon from "App/icons/EditIcon";
import HomeIcon from "App/icons/HomeIcon";
import Image from "next/image";
import React from "react";
import style from "../GoalPlanningSteps.module.scss";
import Box from "../../../ui/Box/Box";
import { GroupBox } from "../../../ui/Group/Group.styles";
import CartFill from "App/icons/CartFill";
import HourglassSplit from "App/icons/HourglassSplit";
import SelectMenu from "@ui/Select/Select";
import Piggybankfill from "App/icons/Piggybankfill";
import Input from "@ui/Input";
import { StyledTabs, TabsContent, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsTrigger } from "@radix-ui/react-tabs";
import PlusIcon from "App/icons/PlusIcon";
import StarFill from "App/icons/StarFill";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/DataGrid/DataGrid.styles";
import WheelChair from "App/icons/WheelChair";
const GoalPlaningStepsPage = () => {
  return (
    <Box background="">
      <Box className="">
        <Box className="row">
          <Box className="col-md-3 col-sm-3 col-lg-3 mb-2">
            <Card color="bluedark">
              <Box>
                <GroupBox
                  position="center"
                  className="text-center"
                  color="white"
                  horizontal
                >
                  <Box css={{ m: 10 }}>
                    <Text weight="bold" color="white" size="h2">
                      Select Goal For
                    </Text>
                  </Box>
                  <Box>
                    <WheelChair size="75" color="white" />
                  </Box>
                  <Box css={{ m: 10 }}>
                    <Text weight="bold" color="white" size="h4">
                      Vacation
                    </Text>
                  </Box>
                </GroupBox>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-9 col-sm-9 col-lg-9">
            <Card>
              <Text weight="bold" size="h4" border="default">
                Tenure
              </Text>
              <Box className="row" css={{ p: 22 }}>
                <Box className="col-auto">
                  <Card>
                    <Box css={{ p: 15 }} className="svgblue">
                      <HourglassSplit size="50" color="blue" />
                    </Box>
                  </Card>
                </Box>
                <Box className="col-4">
                  <Box>
                    <Text>What is your tenure?</Text>
                  </Box>
                  <Box>
                    <SelectMenu
                      items={[
                        { id: "1", name: "Baroda" },
                        { id: "2", name: "Surat" },
                      ]}
                      bindValue={"id"}
                      bindName={"name"}
                      label={""}
                      placeholder={"1 Years"}
                      onChange={()=>{}}
                    />
                  </Box>
                </Box>
                <Box className="col-4">
                  <Box css={{ mt: 21 }}>
                    <SelectMenu
                      items={[
                        { id: "1", name: "Baroda" },
                        { id: "2", name: "Surat" },
                      ]}
                      bindValue={"id"}
                      bindName={"name"}
                      label={""}
                      placeholder={"0 Months"}
                      onChange={()=>{}}
                    />
                  </Box>
                </Box>
                <Box className="col-1">
                  <Box css={{ mt: 20 }}>
                    <Button color="yellow">Continue</Button>
                  </Box>
                </Box>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-12 col-sm-12 col-lg-12 mb-2">
            <Card>
              <Text weight="bold" size="h4" border="default">
                Invetment Amount
              </Text>
              <Box className="row" css={{ p: 22 }}>
                <Box className="col-auto">
                  <Card>
                    <Box css={{ p: 15 }} className="svgblue">
                      <Piggybankfill size="50" color="blue" />
                    </Box>
                  </Card>
                </Box>
                <Box className="col">
                  <Box className="row">
                    <Box className="col-4">
                      <Box>
                        <Text>What is the expected Amount ?</Text>
                      </Box>
                    </Box>
                    <Box className="col-3">
                      <Input label="" name="amount" placeholder="1,00,0000" />
                    </Box>
                    <Box className="col-4"></Box>
                    <Box className="col-4">
                      <Box>
                        <Text>
                          <Text>What is the already Invest Amount?</Text>
                        </Text>
                      </Box>
                    </Box>
                    <Box className="col-3">
                      <Input label="" name="amount" placeholder="1,00,0000" />
                    </Box>
                    <Box className="col-4"></Box>
                  </Box>
                  <Box className="row">
                    <Box className="col-auto">
                      <Text>
                        What is the percentage of the growth expected for the
                        invested Amount?
                      </Text>
                    </Box>
                    <Box className="col-auto">
                      <Input label="" name="amount" placeholder="1,00,0000" />
                    </Box>
                  </Box>
                  <Box className="row">
                    <Box className="col-auto">
                      <Text>Adjust for inflation</Text>
                    </Box>
                    <Box className="col" css={{ mr: 25 }}>
                      <Input label="" name="amount" placeholder="1,00,0000" />
                    </Box>
                    <Box className="col-auto">
                      <Input label="" name="amount" placeholder="1,00,0000" />
                    </Box>
                    <Box className="col">
                      <Box>
                        <Button color="yellow">Continue</Button>
                      </Box>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-3 col-sm-3 col-lg-3 mb-2">
            <Card color="bluedark">
              <Box className={style.target}>
                <GroupBox
                  position="center"
                  className="text-center"
                  color="white"
                  horizontal
                >
                  <Box css={{ m: 10 }}>
                    <Text weight="bold" color="white" size="h2">
                      Goal Amount
                    </Text>
                    <Text
                      weight="bold"
                      color="white"
                      size="h1"
                      css={{ mb: 77 }}
                    >
                      10,00,000
                    </Text>
                  </Box>
                </GroupBox>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-9 col-sm-9 col-lg-9">
            <Card>
              <Text weight="bold" size="h4" border="default">
                Projected Plan
              </Text>
              <Box className="row" css={{ p: 22 }}>
                <Box className="col-4">
                  <Box>
                    <Text weight="extrabold" size="h6">
                      Monthly SIP
                    </Text>
                  </Box>
                  <Box>
                    <Input label="" name="amount" placeholder="1,00,0000" />
                  </Box>
                </Box>
                <Box className="col-4">
                  <Box>
                    <Text weight="extrabold" size="h6">
                      Lumpsum
                    </Text>
                  </Box>
                  <Box>
                    <Input label="" name="amount" placeholder="1,00,0000" />
                  </Box>
                </Box>
                <Box className="col-4">
                  <Box>
                    <Text weight="extrabold" size="h6">
                      Tenure
                    </Text>
                  </Box>
                  <Box>
                    <Input label="" name="amount" placeholder="" />
                  </Box>
                </Box>
                <Box className="col text-end">
                  <Box css={{ mt: 10 }}>
                    <Button color="yellow">Plan this goal</Button>
                  </Box>
                </Box>
              </Box>
            </Card>
          </Box>
          <Box className="col">
            <Card>
              <StyledTabs defaultValue="recommendation">
                <TabsList aria-label="Manage your account">
                  <TabsTrigger value="recommendation" className={style.activetab}>
                   Recommendation
                  </TabsTrigger>
                  <TabsTrigger value="projections"  className={style.inactivetab}>
                    Projections
                  </TabsTrigger>
                </TabsList>
                <TabsContent value="recommendation" className="tabsContent">
                  <Table bordered>
                    <ColumnParent>
                      <ColumnRow>
                        <Column
                          position={"center"}
                          css={{ width: "25px", minWidth: "50px" }}
                        ></Column>
                        <Column className={style.columnRow}>Scheme </Column>
                        <Column className={style.columnRow}>Category</Column>
                        <Column className={style.columnRow}>Rating</Column>
                        <Column className={style.columnRow}>Return</Column>
                        <Column className={style.columnRow}>SIP</Column>
                        <Column className={style.columnRow}>Lumpsum</Column>
                      </ColumnRow>
                    </ColumnParent>
                    <DataParent>
                      <DataRow>
                        <DataCell position={"center"}>
                          {" "}
                          <Box className={style.plusYellow}>
                            <PlusIcon color="white" />
                          </Box>
                        </DataCell>
                        <DataCell className={style.dataCell}>AXIS MID CAP FUND-DIRECT GROWTH</DataCell>
                        <DataCell className={style.dataCell}> Equity-Mid-Cap</DataCell>
                        <DataCell className={style.dataCell}>
                          5 <StarFill color="orange" />
                        </DataCell>
                        <DataCell className={style.dataCell}>54.19</DataCell>
                        <DataCell className={style.dataCell}>1000</DataCell>
                        <DataCell className={style.dataCell}>10,000</DataCell>
                      </DataRow>
                      <DataRow>
                        <DataCell position={"center"}>
                          <Box className={style.plusYellow}>
                            <PlusIcon color="white" />
                          </Box>
                        </DataCell>
                        <DataCell className={style.dataCell}>AXIS MID CAP FUND-DIRECT GROWTH</DataCell>
                        <DataCell className={style.dataCell}> Equity-Mid-Cap</DataCell>
                        <DataCell className={style.dataCell}>
                          5 <StarFill color="orange" />
                        </DataCell>
                        <DataCell className={style.dataCell}>54.19</DataCell>
                        <DataCell className={style.dataCell}>1000</DataCell>
                        <DataCell className={style.dataCell}>10,000</DataCell>
                      </DataRow>
                    </DataParent>
                  </Table>
                </TabsContent>
                <TabsContent value="projections">Previous</TabsContent>
              </StyledTabs>
            </Card>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default GoalPlaningStepsPage;
