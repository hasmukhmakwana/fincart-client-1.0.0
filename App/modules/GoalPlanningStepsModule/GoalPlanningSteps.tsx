import Holder from 'App/shared/components/Holder/Holder'
import Layout from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import React from 'react'
import GoalPlaningStepsPage from './components/GoalPlanningStepsPage'

const GoalPlanningStepsModule = () => {
  return (
    <Layout>
        <PageHeader title='Goal Planning Initiate Goal' rightContent={<Holder/>} />
        <GoalPlaningStepsPage/>
    </Layout>
  )
}

export default GoalPlanningStepsModule