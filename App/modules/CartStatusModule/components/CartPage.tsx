import React, { useEffect, useState } from 'react'
import Box from "@ui/Box/Box";
import SelectMenu from "@ui/Select/Select";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import Text from "@ui/Text/Text";
import CheckCircleFill from "App/icons/CheckCircleFill";
import CrossCircleFill from 'App/icons/CrossCircleFill';
import { encryptData, getUser } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";

import { useRouter } from "next/router";
import { Button } from '@ui/Button/Button';
import Cancel from 'App/icons/Cancel';


const CartPage = ({ transactionStatus, transactionDetails, transactionId, totalAmount, reason }: any) => {
    const user: any = getUser();
    const router = useRouter();
    return (
        <>
            <Box className="row mx-1 my-1 pb-1 px-0">
                <Box className="col-lg-12 px-0 mx-auto" css={{ borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }}>
                    <Box className="row">
                        <Box className="col-12 pt-3 text-center" css={{ backgroundColor: "#b3daff" }}>
                            {transactionStatus == "success" ? <>
                                <Text size="h1" className="text-center text-success">Success&nbsp;<CheckCircleFill color="rgb(25, 135, 84)" size="1.5rem"></CheckCircleFill></Text>
                            </> : transactionStatus == "failed" ? <>
                                <Text size="h1" className="text-center text-danger">Failed&nbsp;<CrossCircleFill color="rgb(220, 53, 69)" size="1.5rem"></CrossCircleFill></Text></> : <></>}
                        </Box>
                    </Box>
                    {/* @ts-ignore */}
                    <Text size="h5" className="text-center py-2 pb-2"
                        css={{ backgroundColor: "#b3daff", color: "var(--colors-blue1)" }} >Transaction Reference Number : {transactionId}</Text>
                    {/* @ts-ignore */}
                    <Text size="h5" className="text-center py-2 pb-2"
                        css={{ backgroundColor: "#b3daff", color: "var(--colors-blue1)" }} >Your Transaction of ₹ {Number(totalAmount).toLocaleString('en-In')} is {transactionStatus}.</Text>
                    {transactionStatus != "success" && <>
                        {/* @ts-ignore */}
                        <Text className="text-center py-2 pb-2"
                            css={{ backgroundColor: "#b3daff", fontSize: "0.85rem", color: "var(--colors-red10)" }}>Reason: {reason}</Text>
                        <Text className="text-center py-2 pb-2"
                            css={{ backgroundColor: "#b3daff", fontSize: "0.75rem", cursor: "pointer", textDecoration: "underline", color: "var(--colors-blue1)" }}
                            /* @ts-ignore */
                            onClick={() => { router.push('/Cart') }}>Click here to try again</Text></>}
                </Box>
            </Box>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-body modal-body-scroll p-3">
                    <Box className="row justify-content-center">
                        <Box className="col-lg-12 text-center">
                            <Box className={`row`}>
                                <Box className="d-flex justify-content-center">
                                    <Box css={{ overflow: "auto" }}
                                        className="col-auto col-lg-8 col-md-8 col-sm-8" >
                                        <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                                            <Table className="text-capitalize table table-striped" >
                                                <ColumnParent className="text-capitalize ">
                                                    <ColumnRow className="sticky" css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}  >

                                                        <Column><Text size="h5">Scheme name</Text></Column>
                                                        <Column><Text size="h5">Amount</Text></Column>
                                                        <Column><Text size="h5">Folio No</Text></Column>
                                                    </ColumnRow>
                                                </ColumnParent>

                                                <DataParent css={{ textAlign: "center" }} >
                                                    <DataRow>
                                                    </DataRow>
                                                    {transactionDetails?.map((record: any, index: any) => {
                                                        return (
                                                            <DataRow>
                                                                <DataCell>{record?.schemeName}</DataCell>
                                                                <DataCell>{Number(record?.amount).toLocaleString("en-In")}</DataCell>
                                                                <DataCell>{record?.folioNo}</DataCell>
                                                            </DataRow>
                                                        );
                                                    })}
                                                </DataParent>
                                            </Table>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>

                        </Box>
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default CartPage