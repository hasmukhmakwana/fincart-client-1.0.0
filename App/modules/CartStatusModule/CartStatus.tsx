import React, { useEffect, useState } from 'react'
import CartPage from './components/CartPage';
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout, { toastAlert } from 'App/shared/components/Layout';
import { getBillDeskPaymentStatus } from "App/api/cart";
import { encryptData, getUser } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";
import useCartStore from '../CartModule/store';


const Cart = () => {
  const user: any = getUser();
  const [loading, setLoading] = useState<boolean>(false);
  const [loadAPI, setLoadAPI] = useState<boolean>(true);
  const [paymentStatus, setPaymentStatus] = useState<string>('');
  const [amount, setAmount] = useState<string>('');
  const [reason, setReason] = useState<string>('');
  const [tranxId, setTranxId] = useState<string>('');
  const [tranxStatus, setTranxStatus] = useState<string>('');
  const [logDetails, setLogDetails] = useState<any>([]);
  const {
    transactionNo
  } = useCartStore();
  let timer: any;

  useEffect(() => {
    console.log(localStorage.getItem("transactionNo"));
    fetchPaymentDetails();
  }, [])

  const fetchPaymentDetails = async () => {
    setLoading(true);
    try {
      const enc: any = encryptData({
        trxnno: localStorage.getItem("transactionNo"),
        platform: 'web'
      });

      let result: any = await getBillDeskPaymentStatus(enc);
      setTranxStatus(result?.data?.txnStatus);
      setTranxId(result?.data?.txnId);
      setAmount(result?.data?.txnTotalAmount);
      setReason(result?.data?.txnFailReason);
      setLogDetails(result?.data?.txnLogDetails);
      setLoading(false);
    } catch (error: any) {
      console.log(error);
      setLoading(false);
      toastAlert("error", error);
    }
  }


  return (
    <Layout>
      <PageHeader title="Cart Status" rightContent={<></>} />
      {loading ?
        <Loader />
        :
        <CartPage transactionStatus={tranxStatus} transactionDetails={logDetails} transactionId={tranxId} totalAmount={amount} reason={reason} />
      }
    </Layout>
  )
}

export default Cart