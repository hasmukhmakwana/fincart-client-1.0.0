import React from "react";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import style from "../Member.module.scss";
import Box from "./../../../ui/Box/Box";
import { GroupBox } from "@ui/Group/Group.styles";
import CheckCircleFill from "App/icons/CheckCircleFill";
function MemberPage() {
  return (
    // <Box>
    //   <Box className="container-fluid">
        <Box className="row">
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Box>
              <Box className="row">
                <Box className="col-md-6">
                  <Box>
                    <Box className={style.basketBox}>
                      <Box className="row">
                        <Box className="col col-auto">
                          <Box className={style.img}>
                          </Box>
                        </Box>
                        <Box className="col col-auto">
                          <Box className={style.mt10}>
                          <Text
                            weight="bold"
                            color="default"
                            size="bt"
                            transform="uppercase"
                          >
                            afgaiod
                          </Text>
                          <Text weight="bold" color="default" size="h2">
                            Abhay Shah
                          </Text>
                          <Text weight="normal" color="default" size="bt">
                            InBoxisual
                          </Text>
                          <Button color="yellow" size="xl" className={style.mt10}>
                            Edit Profile
                          </Button>
                          </Box>
                        </Box>
                        <Box className="col-md-12">
                          <GroupBox className="my-2">
                              <Box>
                              <CheckCircleFill color="green"></CheckCircleFill>
                              </Box>

                            <Box>KYC</Box>
                          </GroupBox>
                          <GroupBox className="my-2">
                              <Box>
                              <CheckCircleFill color="green"></CheckCircleFill>
                              </Box>

                            <Box>Profile</Box>
                          </GroupBox>
                          <GroupBox className="my-2">
                              <Box>
                              <CheckCircleFill color="green"></CheckCircleFill>
                              </Box>

                            <Box>SIP Mandate</Box>
                          </GroupBox>
                          <GroupBox className="my-2">
                              <Box>
                              <CheckCircleFill color="green"></CheckCircleFill>
                              </Box>

                            <Box>Investment A/C</Box>
                          </GroupBox>
                        </Box>
                      </Box>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
    //   </Box>
    // </Box>
  );
}
export default MemberPage;
