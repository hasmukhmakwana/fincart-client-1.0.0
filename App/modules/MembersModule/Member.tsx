import { Button } from '@ui/Button/Button'
import Layout from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import React from 'react'
import MemberPage from './components/MemberPage'

function MemberModule() {
  return (
    <Layout>
    <PageHeader title='Members' rightContent={<Button color="yellow" size="xl" >Add Member</Button>} />
      <MemberPage/>
    </Layout>
  )
}

export default MemberModule