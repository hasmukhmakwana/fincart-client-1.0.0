import Box from "App/ui/Box/Box";
import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Holder from "App/shared/components/Holder/Holder";
import Loader from "App/shared/components/Loader";
import dynamic from "next/dynamic";

import {
  getOverallPortfolioData,
  getGoalWisePortfolioData,
  upcomingTransaction,
  recentTransaction,
  maturingAssets,
  productWiseMandate,
  assetAllocations,
  assetAllocationsAll,
  growthChart,
  getAllMember
} from "App/api/dashboard";

import usePortfolioStore from "./store";
import { Capitalize, encryptData, formatDate, getUser } from "App/utils/helpers";
import { useEffect, useState } from "react";
// import ProgressDemo from "./components/PlanButNoInvestment/Progress"; 

const InvestedFunds: any = dynamic(
  () => import("App/modules/DashboardModule/components/InvestedFunds"),
  {
    loading: () => <Loader />,
  }
);
const Analytics: any = dynamic(
  () => import("App/modules/DashboardModule/components/Analytics"),
  {
    loading: () => <Loader />,
  }
);
const RecentTransactions: any = dynamic(
  () => import("App/modules/DashboardModule/components/RecentTransactions"),
  {
    loading: () => <Loader />,
  }
);

const ActiveList: any = dynamic(
  () => import("App/modules/DashboardModule/components/ActiveList"),
  {
    loading: () => <Loader />,
  }
);

const NoInvestment = dynamic(
  () => import("App/modules/DashboardModule/components/noInvestment"),
  {
    loading: () => <Loader />,
  }
);
const KycStatus = dynamic(
  () => import("App/modules/DashboardModule/components/kycStatusbar"),
  {
    loading: () => <Loader />,
  }
);

const NoPlan = dynamic(
  () => import("App/modules/DashboardModule/components/noPlan"),
  {
    ssr: false,
    loading: () => <Loader />
  }
)

const Goals = dynamic(
  () => import("App/modules/DashboardModule/components/MyGoals/Goals"),
  {
    loading: () => <Loader />,
  }
);

const PlanButNoInvestment = dynamic(
  () => import("App/modules/DashboardModule/components/PlanButNoInvestment/PlanButNoInvestment"),
  {
    loading: () => <Loader />,
  }
);


const MyGoals: any = dynamic(
  () => import("App/modules/DashboardModule/components/MyGoals/MyGoals"),
  {
    loading: () => <Loader />,
  }
);

const Portfolio: any = dynamic(
  () => import("App/modules/DashboardModule/components/Portfolio"),
  {
    loading: () => <Loader />,
  }
);

//*css
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import useHeaderStore from "App/shared/components/Header/store";
import useSwitchHoldingStore from "App/shared/components/Holder/store";
import { use } from "echarts";
import FeedbackModal from "../SupportModule/FeedbackModule/components/Modals/FeedbackModal";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import router, { useRouter } from "next/router";
import { getLoginDetails, getUserDetails, login } from "App/api/login";
import { LoginFormData } from "../LoginModule/loginTypes";
import { CLIENT_ID, CLIENT_SECRET_KEY, TOKEN_PREFIX, USER_DETAILS } from "App/utils/constants";
import { setLS } from "@ui/DataGrid/utils";
import useLoginStore from "../LoginModule/store";
import { values } from "lodash";
//*main
const Dashboard = () => {
  const { selectMember, setMOHmember, setInvestor, setSwitchLoader } = useSwitchHoldingStore();

  const { setUserInfo } = useHeaderStore();

  const [subLoader, setSubLoader] = useState(false);
  const {
    maturingDataArray,
    setPortfolioScreen,
    setLoader,
    setGoalLoader,
    setOverallPortfolioData,
    setPortfolioData,
    setGain_percentage,
    setLastUpdatedDate,
    setMutualFundPortfolioData,
    setDigitalGoldPortfolioData,
    setEpfPortfolioData,
    setInsurancePortfolioData,
    setLiquiloanPortfolioData,
    setPmsPortfolioData,
    setProtectionPortfolioData,
    setUlipPortfolioData,
    setUpcomingTransactions,
    setMaturingTransactions,
    setMaturingDataArray,
    setGoalWisePortfolioData,
    setButtonTextList,
    setMandateTransactions,
    setMfChartData,
    setOverallChartData,
    setLegendNamesOA,
    setLegendNamesMF,
    setRecentTransactions,
    setGrowthChartData,
    setMonthGrowthChart,
    setBasicId
  } = usePortfolioStore();
  const {
    userInfo,
    showFeedback,
    setShowFeedback
  } = useHeaderStore();

  const [viewPart, setViewPart] = useState<any>();

  const fetchOverallData = async () => {
    try {
      if (!selectMember?.basicid) return;
      setLoader(true);
      setBasicId(selectMember?.basicid);
      const obj: any = {
        basicID: selectMember?.basicid,
        assetType: "",
        isActive: "true",
      };

      const enc: any = encryptData(obj);

      const result: any = await getOverallPortfolioData(enc);

      setLoader(false);

      setOverallPortfolioData(result?.data?.overall_Portfolio_object);

      setPortfolioData(result?.data?.asset_wise_portfolio);
      setButtonTextList(result?.data?.Button_text_list);
      setLastUpdatedDate(result?.data?.last_Updated_date);

      for (let i = 0; i < result?.data?.productWisePortfolio.length; i++) {

        switch (result?.data?.productWisePortfolio[i].investment_type) {
          case "MUTUAL FUND":
            result?.data?.productWisePortfolio[i].data.sort((a: any, b: any) =>
              a.partner_name > b.partner_name ? 1 : -1
            );
            setMutualFundPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
          case "ULIP":
            setUlipPortfolioData(result?.data?.productWisePortfolio[i].data);
            break;
          case "PMS":
            setPmsPortfolioData(result?.data?.productWisePortfolio[i].data);
            break;
          case "LIQUILOAN":
            setLiquiloanPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
          case "OTHER INSURANCE":
            setInsurancePortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
          case "PROTECTION":
            setProtectionPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
          case "EPF":
            setEpfPortfolioData(result?.data?.productWisePortfolio[i].data);
            break;
          case "DIGITAL GOLD":
            setDigitalGoldPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
        }
      }
      setGain_percentage(result?.data?.Gain_percentage);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  const fetchUpcomingData = async () => {
    try {
      if (!selectMember?.basicid) return;

      setLoader(true);

      const enc: any = encryptData(selectMember?.basicid, true);

      const result: any = await upcomingTransaction(enc);
      setLoader(false);

      setUpcomingTransactions(result?.data);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  const fetchMaturingData = async () => {
    try {
      let currDate = new Date();
      if (!selectMember?.basicid) return;

      setSubLoader(true);
      let obj: any = {

        id: String(selectMember?.basicid),
        Date: String(formatDate(currDate, "y-m-d"))

      }
      const enc: any = encryptData(obj);

      const result: any = await maturingAssets(enc);

      setSubLoader(false);
      if (Object.keys(result?.data).length > 0) {
        setMaturingTransactions(result?.data?.MaturingAssets);
        setMaturingDataArray(result?.data?.DateArr);
      } else {
        setMaturingTransactions([]);
        setMaturingDataArray({});
      }

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setSubLoader(false);
    }
  };

  const fetchMandateData = async () => {
    try {
      if (!selectMember?.basicid) return;

      setLoader(true);
      const enc: any = encryptData(selectMember?.basicid, true);

      const result: any = await productWiseMandate(enc);
      setLoader(false);

      setMandateTransactions(result?.data?.MandateList);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  const fetchGoalWisePortfolioData = async () => {
    try {
      if (!selectMember?.basicid) return;

      setGoalLoader(true);

      const result: any = await getGoalWisePortfolioData();
      console.log(result, "result");
      setGoalLoader(false);

      setGoalWisePortfolioData(result?.data);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setGoalLoader(false);
    }
  };

  const fetchAssetAllocationData = async () => {
    try {
      if (!selectMember?.basicid) return;
      setLoader(true);
      const enc: any = encryptData(selectMember?.basicid, true);

      const result: any = await assetAllocations(enc);
      setLoader(false);

      let arr = [];
      let legendNamesMF = [];

      let mfchartData: any = Object.entries(result?.data[0]);

      if (mfchartData != undefined && mfchartData != null && mfchartData.length != 0) {
        for (let i = 0; i < mfchartData.length; i++) {
          arr.push({
            value: parseFloat(mfchartData[i][1]),
            name: Capitalize(mfchartData[i][0].replace("_per", ""))
          });

          legendNamesMF.push(Capitalize(mfchartData[i][0].replace("_per", "")))
        }
      }

      setMfChartData(arr);
      setLegendNamesMF(legendNamesMF);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  const fetchGrowthChartData = async () => {
    try {
      if (!selectMember?.basicid) return;
      setLoader(true);
      let obj = {
        'basicID': selectMember?.basicid,
        'product_type': 'MF',
        'PMS': ''
      }
      const enc: any = encryptData(obj);

      const result: any = await growthChart(enc);
      setLoader(false);

      let month: any = [];
      let principal_val: any = [];
      let current_val: any = [];
      result?.data.map((ele: any) => {
        month.push((ele?.month).toUpperCase());
        principal_val.push(ele?.principal_val)
        current_val.push(ele?.current_val);
      })

      setMonthGrowthChart(month);
      let series: any = [];
      series.push({
        name: "Principal",
        type: "line",
        smooth: "true",
        symbol: "circle",
        symbolSize: 6,
        data: principal_val,
        lineStyle: {
          width: 3,
          color: "#b3daff",
        },
      })
      series.push({
        name: "Current Value",
        type: "line",
        smooth: "true",
        symbol: "circle",
        symbolSize: 6,
        data: current_val,
        lineStyle: {
          width: 3,
          color: "#005CB3",
        },
      })

      setGrowthChartData(series);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  const fetchOverallAssetAllocationData = async () => {
    try {
      if (!selectMember?.basicid) return;
      setLoader(true);
      const enc: any = encryptData(selectMember?.basicid, true);

      const result: any = await assetAllocationsAll(enc);
      setLoader(false);

      let arr1 = [];
      let legendNamesOA = [];

      let mfchartData: any = Object.entries(result?.data[0]);

      if (mfchartData != undefined && mfchartData != null && mfchartData.length != 0) {
        for (let i = 0; i < mfchartData.length; i++) {
          arr1.push({
            value: parseFloat(mfchartData[i][1]),
            name: mfchartData[i][0].toUpperCase() == "MF" ? "MUTUAL FUND" : mfchartData[i][0].toUpperCase()
          });

          legendNamesOA.push(mfchartData[i][0].toUpperCase() == "MF" ? "MUTUAL FUND" : mfchartData[i][0].toUpperCase())
        }
      }

      setOverallChartData(arr1);
      setLegendNamesOA(legendNamesOA);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };


  useEffect(() => {
    setPortfolioScreen(true);

    if (selectMember?.basicid !== "" && selectMember?.basicid !== null) {
      (async () => {
        await fetchOverallData();
        await fetchGrowthChartData();
        await fetchAssetAllocationData();
        await fetchOverallAssetAllocationData();
        await fetchUpcomingData();
        await fetchMaturingData();
        await fetchMandateData();
        await fetchGoalWisePortfolioData();
      })();
    }
    return () => {
      setGoalWisePortfolioData([]);
    }
  }, [selectMember]);

  useEffect(() => {
    if (userInfo === null || userInfo === undefined) {
      // const userDetails = getUser();
      setViewPart(getUser());
    } else {
      setViewPart(userInfo);
    }
    return () => { }
  }, [userInfo]);

  const fetchMemberList = async (basicid: any) => {
    try {
      if (!basicid) return;
      setSwitchLoader(true);
      const enc: any = encryptData(basicid, true);

      let result: any = await getAllMember(enc);

      let obj: any = {
        "memberName": "All",
        "basicid": "0",
        "isNominee": false
      }

      let allObj: any;

      if (result?.data?.memberList !== null && result?.data?.memberList?.length !== 0 && result?.data?.memberList !== undefined) {
        allObj = result?.data?.memberList;
        allObj.splice(0, 0, obj);
      } else {
        allObj = [obj];
      }
      setMOHmember(allObj);
      setSwitchLoader(false);
    } catch (error: any) {
      console.log(error);
      setSwitchLoader(false);
      toastAlert("error", error);
    }
  };


  useEffect(() => {
    if (viewPart !== undefined && viewPart !== null) {
      fetchMemberList(viewPart?.basicid);
      setInvestor(viewPart?.basicid)
    }
  }, [viewPart])

  //*main return
  return (
    <>
      <Layout>
        <PageHeader title="Dashboard" rightContent={<Holder />} />
        <Box className="row">
          <Box className="col-md-12">
            {(viewPart?.isClient === "N" &&
              (viewPart?.IsFinancialPlan === "" || viewPart?.IsFinancialPlan === "N") && !viewPart?.isGoal) ? <><NoInvestment /></> : <><InvestedFunds /></>}
            {/* <NoInvestment />  */}
          </Box>

          <Box className="col mb-3">
            {viewPart?.isClient === "Y" ? <><Analytics /></> : <></>}
          </Box>

          <Box className="col-lg-12 col-sm-12 col-md-12 ">
            <Box className="row">
              {viewPart?.isClient === "Y" ? <>
                <Box className="col-12 col-sm-5 mb-3">
                  <RecentTransactions />
                </Box>
                <Box className="col-12 col-sm-7">
                  <ActiveList subLoader={subLoader} setSubLoader={setSubLoader} basicId={selectMember?.basicid} />
                </Box>
              </> : <></>}

              {viewPart?.isClient === "N" && (viewPart?.IsFinancialPlan === "Y" || viewPart?.isGoal) ? <>
                <Box className="col-sm-12 mb-3">
                  <PlanButNoInvestment />
                </Box>
              </> : viewPart?.isGoal && viewPart?.isClient === "Y" ? <>
                <Box className="col-sm-12 mb-3">
                  <MyGoals reload={fetchGoalWisePortfolioData} />
                </Box>
              </> : (viewPart?.isGoal === false && viewPart?.isClient === "Y") ?
                <NoPlan /> :
                <></>}
              {viewPart?.isGoal === false && viewPart?.isClient === "N" &&
                (viewPart?.IsFinancialPlan === "" || viewPart?.IsFinancialPlan === "N") ? <>
                <Box className="col-sm-12 mb-5">
                  {(viewPart?.cafstatus != "success") && <KycStatus view={viewPart} />}
                  <NoPlan />
                </Box>
              </> : <></>}
            </Box>
          </Box>
          <Box className="col-lg-12">
            <Portfolio id={selectMember?.basicid} />
          </Box>
        </Box>
        {/* {viewFeedback && } */}
        <DialogModal
          open={showFeedback}
          setOpen={setShowFeedback}
          css={{
            "@bp0": { width: "80%" },
            "@bp1": { width: "40%" },
          }}
        // className=""
        >
          <FeedbackModal setOpen={setShowFeedback}></FeedbackModal>
        </DialogModal>
      </Layout>
    </>
  );
};

export default Dashboard;
