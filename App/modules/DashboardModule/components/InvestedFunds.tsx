import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import { GroupBox } from "@ui/Group/Group.styles";
import UpArrowLong from "App/icons/UpArrowLong";
import DownArrowLong from "App/icons/DownArrowLong";
import { Button } from "@ui/Button/Button";
import usePortfolioStore from "../store";
import Loader from "App/shared/components/Loader";
import { isNumber, isUndefined, values } from "lodash";
import { useRouter } from "next/router";
import useContactUsStore from "App/modules/SupportModule/ContactUs/store";
const InvestedFunds = () => {
  const {
    loading,
    overallPortfolioData,
    portfolioData,
    Gain_percentage,
    buttonTextList,
    last_Updated_date
  } = usePortfolioStore();
  const { setContactUsVal } = useContactUsStore();
  const router = useRouter();

  const contactUSval = (Type: any) => {
    // console.log(Type,"Type");
    let typeVal: any;
    if (Type != undefined) {
      switch (Type) {
        case "PMS":
          typeVal = "1";
          break;
        case "LIQUILOAN":
          typeVal = "2";
          break;
        case "ULIP":
          typeVal = "4";
          break;
        case "Protection":
          typeVal = "5";
          break;
        case "OTHER":
          typeVal = "6";
          break;
        // default:
        //   break;
      }
      setContactUsVal({
        SubjectId: String(typeVal),
        Description: ""
      }
      );
      router.push('/Support/ContactUs');
    }
  }

  return (
    <Box className="graphContainer py-3 py-md-4 px-4 mb-4 align-content-center">
      <Box className="row">
        {loading ? <Loader></Loader> :
          <>
            <Box className="col-12 col-sm-3 col-md-4 p-0 d-flex flex-column justify-content-top">
              <Box className="row">
                <Box className="col">
                  <Box>
                    {/* @ts-ignore */}
                    <Text size="h4" color="white">
                      Current Value
                    </Text>
                    {/* @ts-ignore */}
                    <Text size="h3" color="lightBlue">
                      {overallPortfolioData?.current_value === "0" ? <>- -</> : <> &#x20B9;{" "}{Number(overallPortfolioData?.current_value).toLocaleString(
                        "en-IN"
                      ) || "0.0"}</>}
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="row mt-4">
                <Box className="col-6 col-md-auto col-lg-auto mb-2">
                  {/* @ts-ignore */}
                  <Text size="bt2" color="graylight">
                    Invested Amount
                  </Text>
                  {/* @ts-ignore */}
                  <Text size="h5" color="lightBlue">
                    {overallPortfolioData?.net_investment === "0" ? <>-</> :
                      <> &#x20B9;{" "}
                        {Number(overallPortfolioData?.net_investment).toLocaleString(
                          "en-IN"
                        ) || "0.0"}</>}

                  </Text>
                </Box>
                <Box className="col-6 col-md-auto col-lg-auto mb-2">
                  {/* @ts-ignore */}
                  <Text size="bt2" color="graylight">
                    XIRR
                  </Text>
                  {/* @ts-ignore */}
                  <Text size="h5" color="lightBlue">
                    {overallPortfolioData?.xirr === "0" ? <>-</> :
                      <>{overallPortfolioData?.xirr || "0.0"}%</>}
                  </Text>
                </Box>
              </Box>
              <Box className="row mt-4">
                <Box className="col-12 col-md-auto col-lg-auto">
                  {/* @ts-ignore */}
                  <Text size="bt2" color="graylight">
                    Gains
                  </Text>
                  {overallPortfolioData?.netgain === "0" ? <> <Text size="h5" color="lightBlue">-</Text></> :
                    <>
                      <GroupBox align="end">
                        <Box>
                          {/* @ts-ignore */}
                          <Text color="lightBlue" size="h5">
                            ₹{" "}
                            {Number(overallPortfolioData?.netgain).toLocaleString(
                              "en-IN"
                            ) || "0.0"}
                          </Text>
                        </Box>
                        <Box
                          className={
                            Number(overallPortfolioData?.netgain) > 0 ? "greenArrowup" : "redArrowDown"
                          }
                        >
                          {Number(overallPortfolioData?.netgain) > 0 ? (
                            <UpArrowLong color="yellow10" size="15"></UpArrowLong>
                          ) : (
                            <DownArrowLong color="yellow10" size="15"></DownArrowLong>
                          )}
                        </Box>
                        {/* <Box>
                          <Text color="lightBlue" size="h5">
                            {Gain_percentage}%
                          </Text>
                        </Box> */}
                      </GroupBox>
                    </>}
                </Box>
              </Box>
              <Box className="row mt-auto">
                {/* @ts-ignore */}
                <Text size="bt1" color="graylight">
                  Last updated on : <span style={{ color: "#B3DAFF" }}>{last_Updated_date}</span>
                </Text>
              </Box>
            </Box>
            <Box className="col-12 col-sm-9 col-md-8 p-0 mt-4 mt-md-0 d-flex flex-column justify-content-top">
              <Box className="row row-cols-1 row-cols-md-4 investinfo">
                {portfolioData.map((record) => {
                  // console.log(record, "types");
                  return (
                    <Box className="col mb-3">
                      {/* @ts-ignore */}
                      <Text size="h4" color="white" className="text-uppercase mb-1">
                        {record?.investment_type_name || ""}
                      </Text>
                      {
                        ((record?.current_value == "0") ? (
                          <Box className="row">
                            <Box className="col">
                              {
                                //@ts-ignore
                                Object.hasOwn(
                                  buttonTextList,
                                  record?.investment_type_value
                                ) ? (
                                  <Button className="py-1 px-2 mt-1" color="lightBlue"
                                    onClick={() => {
                                      if (record?.investment_type_value !== "MF") {
                                        contactUSval(record?.investment_type_value);
                                      }
                                    }}
                                  >
                                    <Text
                                      //@ts-ignore
                                      color="gray8"
                                    >
                                      {buttonTextList[record?.investment_type_value]}
                                    </Text>
                                  </Button>
                                ) : record?.investment_type_value === "OTHER" ? (
                                  <Button className="py-1 px-2 mt-1" color="lightBlue"
                                    onClick={() => {
                                      contactUSval(record?.investment_type_value);
                                    }}
                                  >
                                    <Text
                                      //@ts-ignore
                                      color="gray8"
                                    >
                                      {buttonTextList["INSURANCE"]}
                                    </Text>
                                  </Button>
                                ) : (
                                  ""
                                )
                              }
                            </Box>
                          </Box>
                        ) : (<>
                          <Box className="row">
                            <Box className="col-6">
                              {/* @ts-ignore */}
                              <Text size="bt" className="nowrap">
                                Current Value :
                              </Text>
                            </Box>
                            <Box className="col-6 text-end">
                              {/* @ts-ignore */}
                              <Text size="bt">
                                &#x20B9;
                                {Number(record?.current_value).toLocaleString(
                                  "en-IN"
                                ) || "0.0"}
                              </Text>
                            </Box>
                            {(record?.Sum_assured === undefined) ?
                              <>
                                <Box className="col-6">
                                  {/* @ts-ignore */}
                                  <Text size="bt" className="nowrap">
                                    Gains :
                                  </Text>
                                </Box>
                                <Box className="col-6 text-end">
                                  {/* @ts-ignore */}
                                  <Text size="bt">
                                    &#x20B9;{(record?.netgain) ? Number((record?.netgain)).toLocaleString("en-IN") :
                                      "0"}{" "}
                                  </Text>
                                </Box></> :
                              <>
                                <Box className="col-6">
                                  {/* @ts-ignore */}
                                  <Text size="bt" className="nowrap">
                                    Sum Assured :
                                  </Text>
                                </Box>
                                <Box className="col-6 text-end">
                                  {/* @ts-ignore */}
                                  <Text size="bt">
                                    &#x20B9;{(record?.Sum_assured) ? Number((record?.Sum_assured)).toLocaleString("en-IN") :
                                      "0"}{" "}
                                  </Text>
                                </Box>
                              </>}
                            {(record?.Sum_assured === undefined) ? <>
                              <Box className="col-6">
                                {/* @ts-ignore */}
                                <Text size="bt" className="nowrap">
                                  Invested :
                                </Text>
                              </Box>
                              <Box className="col-6 text-end">
                                {/* @ts-ignore */}
                                <Text size="bt">
                                  &#x20B9;{isNaN(Number(record?.invested_value)) ? "0"
                                    : Number(record?.invested_value).toLocaleString(
                                      "en-IN"
                                    )}</Text>
                              </Box>
                              <Box className="col-6">
                                {/* @ts-ignore */}
                                <Text size="bt" className="nowrap">
                                  Annualized<br></br>Returns :
                                </Text>
                              </Box>
                              <Box className="col-6 text-end">
                                {/* @ts-ignore */}
                                <Text size="bt">{record?.Xirr}%</Text>
                              </Box>
                            </> : <></>}
                          </Box>
                        </>))
                      }
                    </Box>
                  );
                })}
              </Box>
            </Box></>
        }
      </Box>
    </Box >
  );
};

export default InvestedFunds;
