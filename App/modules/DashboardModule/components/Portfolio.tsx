import React from "react";
import Box from "../../../ui/Box/Box";
import Layout, { toastAlert } from "App/shared/components/Layout";
import style from "../Dashboard.module.scss";
import Text from "@ui/Text/Text";
import { GroupBox } from "./../../../ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import EditIcon from "App/icons/EditIcon";
import Card from "@ui/Card";

import { TabsContent } from "@radix-ui/react-tabs";
import Radio from "@ui/Radio/Radio";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@ui/Accordian/Accordian";
import DownArrow from "App/icons/DownArrow";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/DataGrid/DataGrid.styles";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import usePortfolioStore from "../store";
import EyeIcon from "App/icons/EyeIcon";
import { useEffect, useState } from "react";

// import MutualFundPage from "App/modules/ProductModule/MutualFundModule/components/MutualFundPage";
import {
  MutualFundPage,
  ULIPPage,
  InsurancePage,
  LiquiloanPage,
  PMSPage,
  EPFPage,
  ProtectionPage
} from "App/modules/ProductModule"

let recentTabs = [
  {
    title: "Mutual Fund",
    value: "Mutual Fund",
  },
  {
    title: "ULIP",
    value: "ULIP",
  },
  {
    title: "PMS",
    value: "PMS",
  },
  {
    title: "LiquiLoans",
    value: "LiquiLoans",
  },
  {
    title: "Other Insurance",
    value: "Other Insurance",
  },
  {
    title: "Protection",
    value: "Protection",
  },
  // {
  //   title: "NPS",
  //   value: "NPS",
  // },
  // {
  //   title: "EPF",
  //   value: "EPF",
  // },
  // {
  //   title: "Digital Gold",
  //   value: "Digital Gold",
  // },
];

const Portfolio = ({ id }: any) => {

  const {
    loading,
    portfolioData,
    ulipPortfolioData,
    pmsPortfolioData,
    insurancePortfolioData,
    liquiloanPortfolioData,
  } = usePortfolioStore();

  const [dataType, setDataType] = useState<any>("active");

  return (
    <StyledTabs defaultValue="Mutual Fund">
      <TabsList
        aria-label="Manage your account"
        className="tabsCardStyle"
        css={{ mb: 10 }}
      >
        {recentTabs.map((item, index) => {
          return (
            <TabsTrigger
              value={item.value}
              className="tabsCards"
              key={item.value}
            >
              <Box className="text-center">
                {/* @ts-ignore */}
                <Text size="bt2">{item.title}</Text>
              </Box>
            </TabsTrigger>
          );
        })}
      </TabsList>
      <TabsContent value="Mutual Fund">
        <MutualFundPage basicId={id} userGoalId={0} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={false} />
      </TabsContent>
      <TabsContent value="ULIP">
        <ULIPPage id={id} userGoalId={0} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={false} />
      </TabsContent>
      <TabsContent value="PMS">
        <PMSPage id={id} userGoalId={0} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={false} />
      </TabsContent>
      <TabsContent value="LiquiLoans">
        <LiquiloanPage id={id} userGoalId={0} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={false} />
      </TabsContent>
      <TabsContent value="Other Insurance">
        <InsurancePage id={id} userGoalId={0} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={false} />
      </TabsContent>
      <TabsContent value="Protection">
        <ProtectionPage id={id} userGoalId={0} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={false} />
      </TabsContent>
      {/* <TabsContent value="NPS">
        <Card css={{ p: 15, mb: 20 }}>
          <Box>
            <Box className="container pt-5 ps-3 pb-4">
              <Box className={`${style.sectionMainHeading} pb-3 text-center text-sm-start`}>What is NPS?
              </Box>
              <Box className="row d-flex justify-content-center text-center">
                <Box className="col-2">&nbsp;</Box>
                <Box className={`col-8 ${style.lightTextBlue} ${style.yellowBorderBottom} text-center`}>The National
                  Pension System (NPS) is being administered and regulated by Pension Fund
                  Regulatory and
                  Development Authority (PFRDA) set up under PFRDA Act, 2013. NPS, a
                  unique Permanent Retirement Account
                  Number (PRAN) is generated and maintained by the Central Recordkeeping
                  Agency (CRA) for individual subscriber.</Box>
                <Box className="col-2">&nbsp;</Box>
              </Box>
              <Box className={`${style.subHeading} text-center pt-3`}>Benefits Of NPS</Box>
              <Box className="row mt-5 align-items-end align-sm-items-center">
                <Box className="d-none d-md-block col-md-1">&nbsp;</Box>
                <Box className="col-md-6 col-sm-12 d-flex align-self-center">
                  <img src="/NPS.png" alt="ULIP" className="img-fluid mx-auto" />
                </Box>
                <Box className="col-md-4 col-sm-12">
                  <Box className="row">
                    <Box
                      className="col-4 col-md-4 col-sm-4 pt-lg-0 pt-md-0 pt-sm-4 text-end">
                      <img src="/Liquidity.png" alt="Contributions" />
                    </Box>
                    <Box
                      className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                      Liquidity</Box>
                  </Box>
                  <Box className="row">
                    <Box
                      className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-end ps-0 my-auto`}>
                      Low Cost Structure
                    </Box>
                    <Box
                      className="col-4 col-md-4 col-sm-4 pt-lg-0 pt-md-0 pt-sm-4 text-left">
                      <img src="/LowCost.png" alt="Emergency" />
                    </Box>
                  </Box>
                  <Box className="row mt-4">
                    <Box className="col-4 col-md-4 col-sm-4 text-end">
                      <img src="/addMoney.png" alt="Premature Withdrawals" />
                    </Box>
                    <Box
                      className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                      Partial Withdrawal</Box>

                  </Box>
                  <Box className="row mt-4">
                    <Box
                      className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-end my-auto`}>
                      Flexibility Of Investment
                    </Box>
                    <Box className="col-4 col-md-4 col-sm-4 text-left">
                      <img src="/Flexibility.png" alt="Nomination" />
                    </Box>
                  </Box>
                  <Box className="row mt-4">
                    <Box className="col-4 col-md-4 col-sm-4 text-end">
                      <img src="/Tax.png" alt="Tax Benefits" />
                    </Box>
                    <Box
                      className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                      Tax Benefits</Box>

                  </Box>
                </Box>
                <Box className="d-none d-md-block col-md-1">&nbsp;</Box>
              </Box>
              <Box className="mt-4 text-end">
                <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow">
                  <GroupBox>
                    /*@ts-ignore *
                    <Text size="h5">Invest Now</Text>
                  </GroupBox>
                </Button>
              </Box>
            </Box>
          </Box>
        </Card>
      </TabsContent>
      <TabsContent value="EPF">
        <EPFPage />
      </TabsContent>
      <TabsContent value="Digital Gold">
        <Card css={{ p: 15, mb: 20 }}>
          <Box>
            <Box className="container pt-5 ps-3 pb-4">
              <Box className={`${style.sectionMainHeading} pb-3 text-center text-sm-start`}>What is Digital
                Gold?</Box>
              <Box className="row d-flex justify-content-center text-center">
                <Box className="col-2">&nbsp;</Box>
                <Box className={`col-8 ${style.lightTextBlue} ${style.yellowBorderBottom} text-center`}>Digital gold
                  is a new age investment instrument that allows you to invest in 24
                  Karat, 999.9 purest gold,
                  which is then stored in MMTC-PAMP’s secure vaults under your ownership.
                  If you wish to take possession
                  of the same, you can redeem digital gold for 24 Karat, 999.9 purest gold
                  coins and gold bars.
                </Box>
                <Box className="col-2">&nbsp;</Box>
              </Box>


              <Box className="row justify-content-md-center ">

                <Box className="col-md-auto mt-5">
                  <Box className="row justify-content-center">
                    <Box className="col-7 col-md-3 col-lg-6">
                      <img className={`img-fluid ${style.imagegold}`} src="/DigitalGold.png" />
                    </Box>
                    <Box
                      className="col-12 col-md-7 col-lg-6 justify-content-center text-center">
                      <Box className="justify-content-center">
                        <Box className="row m-3 mt-3">
                          <Box className="col-3 col-md-3 col-lg-3  p-0">
                            <img className={`img-fluid ${style.investImg}`}
                              src="/Secure.png" />
                          </Box>
                          <Box
                            className={`col-6 col-md-7 col-lg-7 ${style.pointerHeading} text-start mt-3`}>
                            100% Secure
                          </Box>
                        </Box>

                        <Box className="row m-3">
                          <Box className="col-1 col-lg-2">&nbsp;</Box>
                          <Box
                            className={`col-7 col-md-6 col-lg-7 ${style.pointerHeading} text-end`}>
                            Sell whenever you want
                          </Box>

                          <Box className="col-3 col-md-4 col-lg-3 p-0">
                            <img className={`img-fluid ${style.investImg}`}
                              src="/Sell_whenever.png" />
                          </Box>
                        </Box>

                        <Box className="row m-3">
                          <Box className="col-3 col-md-3 col-lg-3 p-0">
                            <img className={`img-fluid ${style.investImg}`}
                              src="/Purity.png" />
                          </Box>
                          <Box
                            className={`col-6 col-md-7 col-lg-7 ${style.pointerHeading} text-start mt-3`}>
                            Highest Purity
                          </Box>
                        </Box>

                        <Box className="row m-3">
                          <Box
                            className={`col-8 col-md-7 col-lg-9 ${style.pointerHeading} text-end`}>
                            Take physical delivery of the gold
                          </Box>
                          <Box className="col-3 col-md-3 col-lg-3 p-0">
                            <img className={`img-fluid ${style.investImg}`}
                              src="/deliveryOfTheGold.png" />
                          </Box>
                        </Box>
                      </Box>
                    </Box>
                  </Box>
                </Box>
              </Box>
              <Box className="row">
                <Box className="col-sm-5 col-md-7 col-lg-8">&nbsp;</Box>
                <Box className="mt-4 text-end">
                  <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow">
                    <GroupBox>
                      {/*@ts-ignore *
                      <Text size="h5">Invest Now</Text>
                    </GroupBox>
                  </Button>
                </Box>
                <Box className="col-sm-11 col-md-1 ">&nbsp;</Box>
              </Box>
            </Box>
          </Box>
        </Card>
      </TabsContent> */}
    </StyledTabs>
  );
};
export default Portfolio;
