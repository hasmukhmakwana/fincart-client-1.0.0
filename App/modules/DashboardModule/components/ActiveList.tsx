import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import CheckCircleFill from "App/icons/CheckCircleFill";
import Check from "App/icons/Check";
import Cross from "App/icons/Cross";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/DataGrid/DataGrid.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import Badge from "@ui/Badge/Badge";
import { Button } from "@ui/Button/Button";
import usePortfolioStore from "../store";
import React, { useEffect, useState } from "react";
import style from "../Dashboard.module.scss";
import CrossCircleFill from "App/icons/CrossCircleFill";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@ui/Accordian/Accordian";
import DownArrow from "App/icons/DownArrow";
import Loader from "App/shared/components/Loader";
import { maturingAssets } from "App/api/dashboard";
import { encryptData, formatDate } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import { useRouter } from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';
let recentTabs = [
  {
    title: "Upcoming",
    value: "Upcoming",
  },
  {
    title: "Maturing",
    value: "Maturing",
  },
  {
    title: "Mandates",
    value: "Mandates",
  },
];

const ActiveList = ({ subLoader, setSubLoader, basicId }: any) => {
  const router = useRouter();
  const { setContactUsVal } = useContactUsStore();
  const {
    upcomingTransactions,
    maturingTransactions,
    maturingDataArray,
    mandateTransactions,
    setMaturingTransactions,
  } = usePortfolioStore();
  const [totalUpcoming, setTotalUpcoming] = useState<any>(0);
  const [totalMature, setTotalMature] = useState<any>(0);
  const [totalMandate, setTotalMandate] = useState<any>(0);

  const fetchMaturingData = async (dt: any) => {
    try {
      if (!basicId) return;

      setSubLoader(true);
      let obj: any = {

        id: String(basicId),
        Date: String(dt)

      }
      const enc: any = encryptData(obj);

      const result: any = await maturingAssets(enc);

      setSubLoader(false);
      if (Object.keys(result?.data).length > 0) {
        setMaturingTransactions(result?.data?.MaturingAssets);
      } else {
        setMaturingTransactions([]);
      }

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setSubLoader(false);
    }
  };

  useEffect(() => {
    let total: number = 0;
    upcomingTransactions.map((i: any) => {
      total = total + Number(i?.Amount)
    })
    setTotalUpcoming(total);
    total = 0;
    maturingTransactions.map((i: any) => {
      total = total + Number(i?.amount)
    })
    setTotalMature(total);
    total = 0;
    mandateTransactions.map((i: any) => {
      total = total + Number(i?.amount)
    })
    setTotalMandate(total)
    return () => { }
  }, [mandateTransactions])

  return (
    <>
      {/* @ts-ignore */}
      <Text size="h3" color="goalBlue">
        &nbsp;
      </Text>
      <Box className="mb-3">
        {/* @ts-ignore */}
        <Text size="h4" color="goalBlue">
          Forthcoming
        </Text>
      </Box>
      <StyledTabs defaultValue="Upcoming">
        <TabsList
          aria-label="Manage your account"
          className="tabsCardStyle"
          css={{ mb: 20 }}
        >
          {recentTabs.map((item, index) => {
            return (
              <TabsTrigger value={item.value} className="tabsCard" key={item.value}>
                <CheckCircleFill className="iconTick" size="20"></CheckCircleFill>
                <Box className="text-center">
                  {/* @ts-ignore */}
                  <Text size="h5">{item.title}</Text>
                  {/* @ts-ignore */}
                  <Text color="blue" size="h9" className="mt-2">
                    {item.title === "Upcoming" ? upcomingTransactions.length : item.title === "Maturing" ? maturingTransactions.length : item.title === "Mandates" ? mandateTransactions.length : ""}
                  </Text>
                </Box>
              </TabsTrigger>
            );
          })}
        </TabsList>
        <TabsContent value="Upcoming">
          <Card css={{ p: 15, mb: 20, fs: "0.875rem" }}>
            <Box className={`table-responsive ${style.scrollit}`} css={{ overflowX: "auto" }}>
              {/* @ts-ignore */}
              <Table className="table table-striped forthcoming">
                <ColumnParent>
                  <ColumnRow>
                    <Column className="px-0 text-start">Type</Column>
                    <Column className="ps-1 text-start">Scheme</Column>
                    <Column className="px-0 text-start">Date</Column>
                    <Column className="ps-1 text-start">Frequency</Column>
                    <Column className="px-0 text-end">Amount (in Rs.)</Column>
                  </ColumnRow>
                </ColumnParent>
                <DataParent className="modal-body-scroll">
                  {upcomingTransactions.map(record => {
                    let dt = new Date(record?.trxnDate)
                    let lastdt = dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear();
                    return (
                      <DataRow>
                        <DataCell className="nowrap px-0">
                          {record?.Trans_Type == "ULIP" ?
                            <Badge size="sm" color="bluelight1" className="d-inline-block">
                              {record?.Trans_Type}
                            </Badge> :
                            <Badge size="sm" className="d-inline-block">
                              {record?.Trans_Type?.toUpperCase() == "NOR" ? "Lumpsum" : record?.Trans_Type?.toUpperCase() == "R" ? "Redeem" : record?.Trans_Type}
                            </Badge>
                          }
                        </DataCell>
                        <DataCell className="nowrap ps-1">{record?.SchemeName}</DataCell>
                        <DataCell className="nowrap px-0">{lastdt}</DataCell>
                        <DataCell className="nowrap ps-1">{record?.frequency}</DataCell>
                        <DataCell className="text-end pe-3">&#x20B9; {parseFloat(record?.Amount).toLocaleString("en-IN")}</DataCell>
                      </DataRow>
                    )
                  })}
                </DataParent>
              </Table>
            </Box>
            <Box className="row justify-content-end pt-2" css={{ color: "var(--colors-blue1)" }}>
              {totalUpcoming > 0 ? <>
                <Text className="col-auto mx-4" size="h5">Total:</Text>
                <Text className="col-auto me-2" size="h5">&#x20B9; {Number(totalUpcoming).toLocaleString("en-IN")}</Text>
              </> : <><Text children={undefined}></Text></>}
            </Box>
          </Card>

        </TabsContent>
        <TabsContent value="Maturing">
          <Card css={{ p: 15, mb: 20, fs: "0.875rem" }}>
            <Accordion type="single" defaultValue={Object.keys(maturingDataArray)[0]} collapsible onValueChange={(e) => {
              if (e !== "") {
                fetchMaturingData(maturingDataArray?.[e]);
              }
            }} >
              {Object.keys(maturingDataArray).map((ele: any, i: any) => {
                return (
                  <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                    <AccordionTrigger>
                      <Box>
                        <Text
                          /* @ts-ignore */
                          size="h5">{ele}</Text>
                      </Box>
                      <Box className="action">
                        <DownArrow />
                      </Box>
                    </AccordionTrigger>
                    <AccordionContent>
                      {subLoader ? (
                        <Loader />
                      ) : (
                        <>
                          {maturingTransactions.length === 0 ?
                            <GroupBox
                              /* @ts-ignore */
                              position="apart"
                              css={{ mb: 20, alignItems: "flex-start" }}
                            >
                              <Box>
                                <Text
                                  /* @ts-ignore */
                                  color="default">No Data Found </Text>
                              </Box>
                            </GroupBox>
                            : <>
                              <Box className={`table-responsive ${style.scrollitMature}`} css={{ overflowX: "auto" }}>
                                <Table className="table table-striped forthcoming">
                                  <ColumnParent>
                                    <ColumnRow>
                                      <Column>Scheme/Policy</Column>
                                      <Column>Expire On</Column>
                                      <Column>Amount (in Rs.)</Column>
                                      <Column>Take Action</Column>
                                    </ColumnRow>
                                  </ColumnParent>
                                  <DataParent>
                                    {maturingTransactions.map((record: any) => {
                                      let dt = new Date(record?.trxnDate)
                                      let lastdt = dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear();
                                      return (<>
                                        <DataRow>
                                          <DataCell className="nowrap">{record?.SchemeName}
                                          </DataCell>
                                          <DataCell className="nowrap">{lastdt}</DataCell>
                                          <DataCell>&#x20B9; {Number(record?.amount).toLocaleString("en-IN")}</DataCell>
                                          <DataCell><Button className="takeactionBtn" color="yellow" onClick={() => {
                                            setContactUsVal({
                                              SubjectId: "9",
                                              Description: ""
                                            }
                                            );
                                            router.push('/Support/ContactUs')
                                          }}>Request a callback</Button></DataCell>
                                        </DataRow>
                                      </>
                                      )
                                    })}
                                  </DataParent>
                                </Table>
                              </Box>
                              {/* </>
                              );
                            })} */}
                            </>}
                        </>
                      )}
                    </AccordionContent>
                  </AccordionItem>
                )
              })}
            </Accordion>
          </Card>
        </TabsContent>
        <TabsContent value="Mandates">
          <Card css={{ p: 15, mb: 20, fs: "0.875rem" }}>
            {/* @ts-ignore */}
            {/* <Box className="table-responsive" css={{ maxHeight: "calc(100vh - 500px)",overflowY: "auto"}}> */}
            <Box className={`table-responsive ${style.scrollit}`} css={{ overflowX: "auto" }}>
              <Table className="table-striped forthcoming">
                <ColumnParent>
                  <ColumnRow>
                    <Column></Column>
                    <Column>Client Name</Column>
                    <Column>Bank</Column>
                    <Column>Amount (in Rs.)</Column>
                    <Column>Valid Till</Column>
                    <Column>Active</Column>
                  </ColumnRow>
                </ColumnParent>
                <DataParent >
                  {mandateTransactions.map(record => {
                    return (
                      <DataRow>
                        <DataCell>
                          {record?.assetType == "PROTECTION" ?
                            <Badge size="sm" color="bluelight2" className="d-inline-block">
                              {record?.assetType}
                            </Badge> :
                            record?.assetType == "ULIP" ?
                              <Badge size="sm" color="bluelight1" className="d-inline-block">
                                {record?.assetType}
                              </Badge> :
                              <Badge size="sm" className="d-inline-block">
                                {record?.assetType}
                              </Badge>
                          }
                        </DataCell>
                        <DataCell>{record?.ClientName}</DataCell>
                        <DataCell>{record?.BankName}</DataCell>
                        <DataCell>&#x20B9; {Number(record?.amount).toLocaleString("en-IN")}</DataCell>
                        <DataCell className="nowrap">{record?.valid_till}</DataCell>
                        <DataCell className="bluedarkText text-center">{record?.status}
                        </DataCell>
                      </DataRow>
                    )
                  })}
                </DataParent>
              </Table>

            </Box>

            <Box className="row justify-content-end pt-2" css={{ color: "var(--colors-blue1)" }}>
              {totalMandate > 0 ? <>
                <Text className="col-auto mx-4" size="h5">Total:</Text>
                <Text className="col-auto ms-2" size="h5">&#x20B9; {Number(totalMandate).toLocaleString("en-IN")}</Text>
              </> : <><Text children={undefined}></Text></>}
            </Box>

          </Card>
        </TabsContent>
      </StyledTabs>
    </>
  );
};

export default ActiveList;
