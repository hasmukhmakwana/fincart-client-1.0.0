import Box from "@ui/Box/Box";
import React, { useState } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card/Card";

const SaveDataAlert = () => {
    return (
        <>
            <Box className="row">
                <Box className="col-md-12 col-sm-12 col-lg-12 my-5" css={{ color: "var(--colors-blue1)" }}>
                    {/* @ts-ignore */}
                    <Text weight="bold" size="h4" className="text-center mb-1">
                        Thank you for your interest,<br />
                        Our Experts will reach out to you
                    </Text>
                </Box>
            </Box>

        </>
    )
}
export default SaveDataAlert;