import React, { useEffect, useState } from "react";
import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
// import { Carousel } from "react-responsive-carousel";
import { Button } from "@ui/Button/Button";
import style from "../Dashboard.module.scss";
import router, { Router } from "next/router";

const noInvestment = () => {
    return (
        <>
            <Box css={{ background: "#005cb3", p: 15, borderRadius: "10px" }}>
                {/* @ts-ignore */}
                <Text weight="normal" size="h4" color="white">
                    Suggested next steps:
                </Text>
                {/* <Carousel
                    infiniteLoop={false}
                    dynamicHeight={false}
                    showThumbs={false}
                    showStatus={false}
                    renderIndicator={(onClickHandler, isSelected, index, label) => {
                        const defStyle = {
                            marginLeft: 5,
                            color: "white",
                            cursor: "pointer",
                            backgroundColor: "#F7B500",
                            width: "10px",
                            height: "8px",
                            display: " inline-block",
                            borderRadius: "10px"
                        };
                        const style = isSelected
                            ? { ...defStyle, color: "red" }
                            : { ...defStyle };
                        return (
                            <span
                                style={style}
                                onClick={onClickHandler}
                                onKeyDown={onClickHandler}
                                key={index}
                                role="button"
                            ></span>
                        );
                    }}
                >
                </Carousel> */}
                <Box>
                    <Box css={{ p: 15, mb: 10, pb: 50 }}>
                        <Box className="row row-cols-3 mt-3 justify-content-center">
                            <Box className="col-10 col-sm-10 col-md-4 py-1" >
                                <Box className="card text-center h-100" css={{ background: "#b3daff" }} >
                                    <Box className="card-body">
                                        <Box>
                                            <img src="/GoalFund.png" className="img-fluid"
                                                style={{ width: 80, padding: 10 }} />
                                        </Box>
                                        <Box className={`${style.subHeading}`}>Create your first goal</Box>
                                        <Button className={`btn ${style.OtherBtn} text-center larger py-1 my-2 align-items-end`} onClick={() => { router.push("/Goals/MyGoals"); }}>Initiate</Button>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="col-10 col-sm-10 col-md-4 py-1"  >
                                <Box className="card text-center h-100" css={{ background: "#b3daff" }}>
                                    <Box className="card-body">
                                        <Box>
                                            <img src="/QuickSIP.png" className="img-fluid"
                                                style={{ width: 90, padding: 10 }} />
                                        </Box>
                                        <Box className={`${style.subHeading}`}>Quick SIP</Box>
                                        <Button
                                            className={`btn ${style.OtherBtn} text-center larger py-1 my-2 align-items-end`} onClick={() => { router.push("/Tools/QuickSIP"); }}>Start SIP</Button>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="col-10 col-sm-10 col-md-4 py-1" >
                                <Box className="card text-center h-100" css={{ background: "#b3daff" }}  >
                                    <Box className="card-body">
                                        <Box>
                                            <img src="/TaxSaving.png" className="img-fluid"
                                                style={{ width: 80, padding: 10 }} />
                                        </Box>
                                        <Box className={`${style.subHeading}`}>Start your tax saving</Box>
                                        <Button
                                            className={`btn ${style.OtherBtn} text-center larger py-1 my-2 align-items-end`} onClick={() => { router.push("/Tools/TaxSaving"); }}>Save Tax</Button>
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default noInvestment
