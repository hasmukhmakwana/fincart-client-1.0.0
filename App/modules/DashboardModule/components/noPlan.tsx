import Box from "@ui/Box/Box";
import { Carousel } from '3d-react-carousal';
import style from "../Dashboard.module.scss";
import router from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';
const noPlan = () => {
    const { setContactUsVal } = useContactUsStore();
    let slides = [
        <div className={`container ${style.darkBlueColorBg} my-auto`}>
            <div className="row">
                <div className={`col-12 col-md-12 col-lg-12 ${style.regularTextWhite}`}>
                    <div className="row justify-content-center mt-4">
                        <div className="col-9 col-md-7 col-lg-7">
                            <p className="mx-auto text-center mb-3 text-white">
                                <b><u>80%</u></b> of Investors between age group
                            </p>
                            <p className="mx-auto text-center mb-3 text-white">
                                <b><u>30-40</u></b> who are <b><u>Married</u></b> and have
                            </p>
                            <p className="mx-auto text-center mb-3 text-white">
                                <b><u>1</u></b> child opt for following goals.
                            </p>
                            <p className="mx-auto text-center mb-3 text-white">
                                Feel free to explore
                            </p>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className={`col-11 col-md-10 col-lg-10 rounded-4 ${style.lightBg} ${style.subNavName} mt-4`}>
                            <div className="row m-1">
                                <div className="col-4 col-md-4 col-lg-4 text-center">
                                    <label className="text-center ms-1" htmlFor="chkWealth">Wealth&nbsp;Creation</label>
                                    <img className={`img-fluid ${style.wealthimg} mx-auto mt-1`}
                                        src='/Family3.png' alt='Family' />
                                </div>
                                <div className="col-4 col-md-4 col-lg-4 text-center">
                                    <label className="text-center ms-1" htmlFor="chkCar">Car&nbsp;Purchase</label>
                                    <img className={`img-fluid ${style.carImg} mx-auto mt-1`}
                                        src='/car2.png' alt='car' />
                                </div>
                                <div className="col-4 col-md-4 col-lg-4 text-center">
                                    <label className="text-center ms-1" htmlFor="chkRetirement">Secure&nbsp;Retirement</label>
                                    <img className={`img-fluid ${style.retirementImg} mx-auto mt-1`}
                                        src='/retirement1.png' alt='Retirement' />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="text-end mt-3">
                        <button className={`btn ${style.OtherBtn} text-center larger py-1 my-2`}
                            style={{ marginRight: "25px" }} onClick={() => router.push("/Goals/MyGoals")}>Let's Begin</button>
                    </div>
                </div>
            </div>
        </div>,
        <div className={`container ${style.darkBlueColorBg} my-auto`} style={{ paddingBottom: "1rem" }}>
            <div className="row">
                <div className="col-12 col-md-12 col-lg-12 mt-4">
                    <p className={`${style.regularTextWhite} mb-3`}><b>Do you prefer a
                        personalised plan created by our experts?</b>
                    </p>
                    <p className={`${style.regularTextWhite} text-start ${style.txtwidth} mb-3`}>We
                        can create a comprehensive financial plan that covers all of yours goals and
                        requirements, no matter how complex your financial status is.<br></br>
                        The plan can be tailormade for your individual needs and
                        priorities.
                    </p>

                    <div className="row justify-content-center mt-5">
                        <div className="col-12 col-md-12 col-lg-12">
                            <div className={`rounded-4 ${style.lightBg} ${style.subNavName} position-relative`}>
                                <div className="position-absolute bottom-50 end-0">
                                    <img className={`img-fluid ${style.imgalign}`} src='/Adviser.png'></img>
                                </div>
                                <div className="row">
                                    <div className="col-12 mt-3text-start">
                                        <b style={{ marginLeft: "15px" }}>Book your appointment with our
                                            expert.</b>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="text-left col-12 mt-2">
                                        <p style={{ marginLeft: "15px" }}>We will get in touch with you and
                                            begin the journey to create
                                            your personalised plan</p>
                                    </div>
                                </div>
                                <div className="text-end mt-1">
                                    <button className={`btn ${style.OtherBtn} text-center larger py-1 my-2`}
                                        style={{ marginRight: "10px" }}
                                        onClick={() => {
                                            setContactUsVal({
                                                SubjectId: "8",
                                                Description: ""
                                            }
                                            );
                                            router.push("/Support/ContactUs")
                                        }}
                                    >Connect</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>,
        <div className={`container ${style.darkBlueColorBg} my-auto`}>
            <div className="row">
                <div className={`col-12 ${style.regularTextWhite} text-start`}>
                    <div className="row">
                        <div className="col-1">&nbsp;</div>
                        <div className="col-10">
                            <div className="row mt-2">
                                <div className="col-7 col-md-7 col-lg-7 text-start">
                                    Shailesh, is on track for his retirement
                                </div>
                                <div className="col-2 col-md-2 col-lg-2 text-center">
                                    <img className="img-fluid" src='/22Y.png' style={{ height: "40px" }} />
                                    <div className={`${style.yellowText} text-center`}>22&nbsp;Years</div>
                                </div>
                                <div className="col-3 col-md-3 col-lg-3 text-end">
                                    <img className="img-fluid" src='/retirement.png' style={{ height: "40px" }} />
                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-7 col-md-7 col-lg-7 text-start">
                                    Bhavika's daughter Snehal is all set for her Masters in USA
                                </div>
                                <div className="col-2 col-md-2 col-lg-2 text-center">
                                    <img className="img-fluid" src='/18Y.png' style={{ height: "40px" }} />
                                    <div className={`${style.yellowText} text-center`}>18&nbsp;Years</div>
                                </div>
                                <div className="col-3 col-md-3 col-lg-3 text-end">
                                    <img className="img-fluid" src='/Education.png' style={{ height: "40px" }} />
                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-7 col-md-7 col-lg-7text-start">
                                    Rajesh is all set to launch his startup
                                </div>
                                <div className="col-2 col-md-2 col-lg-2 text-center">
                                    <img className="img-fluid" src='/6Y.png' style={{ height: "40px" }} />
                                    <div className={`${style.yellowText} text-center`}>6&nbsp;Years</div>
                                </div>
                                <div className="col-3 col-md-3 col-lg-3 text-end">
                                    <img className="img-fluid" src='/startup.png' style={{ height: "40px" }} />
                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-7 col-md-7 col-lg-7text-start">
                                    Mohit is secure about his dependents future
                                </div>
                                <div className="col-2 col-md-2 col-lg-2 text-center">
                                    <img className="img-fluid" src='/InfinitePeace.png' style={{ height: "40px" }} />
                                    <div className={`${style.yellowText} text-center`}>Infinite&nbsp;Peace</div>
                                </div>
                                <div className="col-3 col-md-3 col-lg-3 text-end">
                                    <img className="img-fluid" src='/Infinite.png' style={{ height: "40px" }} />
                                </div>
                            </div>
                        </div>
                        <div className="col-1">&nbsp;</div>
                    </div>
                </div>
                <div className="col-12">
                    <div className="row">
                        <div className="col-1">&nbsp;</div>
                        <div className={`col-10 rounded-2 ${style.lightBg}`}>
                            <div className="row p-2 ps-3 pe-3">
                                <img className="img-fluid" src='/CarousalScreen2Img.png' />
                            </div>
                            <div className="row p-1 ps-3 pe-3">
                                <div className={`${style.subNavName} col-6`}>
                                    <div className="row">
                                        <div className="col-12 ps-3">
                                            They all took the first step<br />to financial freedom by clicking
                                        </div>
                                    </div>
                                </div>
                                <div className="col-6 text-end">
                                    <button className={`btn ${style.OtherBtn} text-center larger py-1 my-2`} onClick={() => router.push("/FinancialPlan")}>Start My Plan</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-1">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>];

    // const callback = function (index: any) {
    //     console.log("callback", index);
    // }

    return (
        <>
            <Box className="d-block" css={{ background: "#FFF", p: 15, borderRadius: "10px", minHeight: "550px" }}>
                <Carousel slides={slides} autoplay={false} interval={1000} /*onSlideChange={callback}*/ arrows={true} />
            </Box>
        </>
    )
}

export default noPlan