import React,{useState, useEffect} from 'react';
import { styled } from '@stitches/react';
import * as ProgressPrimitive from "@radix-ui/react-progress";


// Your app...
const ProgressBar = ({prvalue}:any) => {
  const [progress, setProgress] = React.useState(1);
//@ts-ignore
  const StyledProgress = styled(ProgressPrimitive.Root, {
    position: 'relative',
    overflow: 'hidden',
    background: '#b3daff',
     borderRadius: '999999px',
    width: "100%",
    height: 20,
    // padding:'3px',
    // border:'1px solid ',
    
    transform: 'translateZ(0)',
  });
  //@ts-ignore
  const StyledText = styled(ProgressPrimitive.Root, {
    position: 'relative',
    overflow: 'hidden',
    color: '#005CB3',
    fontSize: 'smaller',
    width: "100%",
    height: 15,
    
    transform: 'translateZ(0)',
  });
//@ts-ignore
  const StyledIndicator = styled(ProgressPrimitive.Indicator, {
    backgroundColor: '#005CB3',
    color: "#fff",
    // borderRadius: '5px',
    height: '100%',
    transition: 'width 660ms cubic-bezier(0.65, 0, 0.35, 1)',
  });
  
  // Exports
 const Progress = StyledProgress;
 const ProgressIndicator = StyledIndicator;
 const ProgressText = StyledText;
  React.useEffect(() => {
   setProgress(prvalue)
    return () => {}
  }, []);

  return (
    <>
    <Progress>
      <ProgressIndicator style={{  width: `${progress}%` }}>
        <label className="ms-2" style={{position:"absolute",fontSize:'smaller'}}>{progress}%</label>
        <label className="me-2" style={{position:"absolute",fontSize:'smaller', right:"0"}}>{100 - progress}%</label>
        </ProgressIndicator>
    </Progress>
    <ProgressText>
       
       <label className="ms-2" style={{ position: "absolute",left: "0" }}>Insure</label>
       <label className="me-2" style={{ position: "absolute", right: "0" }}>UnderInsured</label>
     
     </ProgressText>
    </>
  );
};


export default ProgressBar;


//@ts-ignore

// // const StyledProgress = styled(ProgressPrimitive.Root, {
// //   // position: 'absolute',
// //   overflow: 'hidden',
// //   background: blackA.blackA9,
// //   borderRadius: '99999px',
// //   width: 800,
// //   height: 25,

// //   // Fix overflow clipping in Safari
// //   // https://gist.github.com/domske/b66047671c780a238b51c51ffde8d3a0
// //   transform: 'translateZ(0)',
// // });

// // //@ts-ignore
// // const StyledIndicator = styled(ProgressPrimitive.Indicator, {
// //   backgroundColor: 'white',
// //   width: '100%',
// //   height: '100%',
// //   transition: 'transform 660ms cubic-bezier(0.65, 0, 0.35, 1)',
// // });

// // // Exports
// // export const Progress = StyledProgress;
// // export const ProgressIndicator = StyledIndicator;

// // // type SaveTypes = {
// // //   prvalue: (values: number)=>
// // // };
// // // Your app...
// // const ProgressDemo = ({prvalue,progressTextstr}:any) => {
// //   const [progress, setProgress] = useState(0);


// //   useEffect(() => {
// //      setProgress(prvalue);
// //   }, [prvalue]);


// //   return (
// //     <>
// //     <Progress  style={{background:"lightBlue"}} > 
// //       <ProgressIndicator  style={{ background:"blue", transform: `translateX(-${100 - progress}%)` }}  />
// //     </Progress>
// //     </>
// //   );
// // };
// // export default ProgressDemo;
// //============================sample code to display number with progress 
// // const Progress_bar = ({bgcolor,progress,height}:any) => {
     
// //   const Parentdiv = {
// //       height: height,
// //       width: '100%',
// //       backgroundColor: 'whitesmoke',
// //       borderRadius: 40,
// //       margin: 50
// //     }
    
// //     const Childdiv = {
// //       height: '100%',
// //       width: `${progress}%`,
// //       backgroundColor: bgcolor,
// //      borderRadius:40,
// //       textAlign: 'right'
// //     }
    
// //     const progresstext = {
// //       padding: 10,
// //       color: 'black',
// //       fontWeight: 900
// //     }
      
// //   return (
// //   <div style={Parentdiv}>
// //     <div style={Childdiv}>
// //       <span style={progresstext}>{`${progress}%`}</span>
// //     </div>
// //   </div>
// //   )
// // }

// // export default Progress_bar;

// //progress bar with text
// const ProgressBar = (props:any) => {
//   const { completed,remain } = props;

//   const containerStyles = {
//     height: 20,
//     width: '100%',
//     backgroundColor: "#e0e0de",
//     borderRadius: 50,
//     marginTop: 10,
//     marginBottom: 10
//   }

//   const fillerStyles = {
//     height: '100%',
//     width: `${completed}%`,
//     backgroundColor: '#005CB3',
//     borderRadius: 'inherit',
//     textAlign: 'right'
//   }

//   const labelStyles = {
//     padding: 5,
//     color: 'white',
//     fontWeight: 'bold'
//   }

//   return (
//     // <div style={containerStyles}>
//     //   <div style={fillerStyles}>
//     //     <span style={labelStyles}>{`${completed}%`}</span>
//     //   </div>
//     // </div>
   
//       <div style={containerStyles}>
//         <div style={fillerStyles}>
//           <span  role="progressbar"
//               aria-valuenow={completed}
//               aria-valuemin="0"
//               aria-valuemax="100"
//               style={labelStyles}>{`${completed}%`}
//         </span>
//         </div>
//       </div>
//   );
// };