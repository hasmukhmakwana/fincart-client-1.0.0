import React from 'react';
import { styled } from '@stitches/react';
import * as ProgressPrimitive from '@radix-ui/react-progress';

// Your app...
const ProgressDemo = ({value}:any) => {
  const [progress, setProgress] = React.useState(1);

  const StyledProgress = styled(ProgressPrimitive.Root, {
    position: 'relative',
    overflow: 'hidden',
    backgroundColor: 'transparent',
     borderRadius: '5px',
    width: 180,
    height: 25,
    padding:'3px',
    border:'1px solid orange',
    
    transform: 'translateZ(0)',
  });
  
  const StyledText = styled(ProgressPrimitive.Root, {
    position: 'relative',
    overflow: 'hidden',
    backgroundColor: 'transparent',
     borderRadius: '5px',
    width: 180,
    height: 25,
    padding:'3px',
    border:'1px solid orange',
    
    transform: 'translateZ(0)',
  });
  

  const StyledIndicator = styled(ProgressPrimitive.Indicator, {
    backgroundColor: 'orange',
    borderRadius: '5px',
    height: '100%',
    transition: 'width 660ms cubic-bezier(0.65, 0, 0.35, 1)',
  });
  
  // Exports
 const Progress = StyledProgress;
 const ProgressIndicator = StyledIndicator;

  React.useEffect(() => {
   setProgress(value)
    return () => {}
  }, []);

  return (
    <Progress>
      <ProgressIndicator style={{  width: `${progress}%` }} />
    </Progress>
  );
};

export default ProgressDemo;