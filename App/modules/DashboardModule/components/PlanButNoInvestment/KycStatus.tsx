import React from "react";
import Box from "@ui/Box/Box";
const KycStatus = () => {
  return (
    <>
      <Box className="container bgWhite rounded-4 pb-2 mb-4">
        <Box className="sectionMainHeading mb-2 pt-3 text-center">
          You're KYC Verified !<br />
          <span className="lightTextBlue">
            Add your bank account to start investing
          </span>
        </Box>
        <Box className="row mt-2">
          <Box className="col-1"></Box>
          <Box className="col-10 sectionMainHeading text-center position-relative mt-3 mt-md-0">
            <Box className="progress rounded-4" style="height: 20px;">
              <Box
                className="progress-bar me-1 darkBlueColorBg text-start ps-2"
                role="progressbar"
                style="width: 25%"
                aria-valuenow="25"
                aria-valuemin="0"
                aria-valuemax="100"
              >
                <small
                  className="d-flex position-absolute progressBarText start-0 ms-0 ms-lg-5"
                  style={{ marginTop: "40px" }}
                >
                  KYC Verification
                </small>
              </Box>
              <Box
                className="progress-bar me-1 lightBlueColorBg text-end pe-2"
                role="progressbar"
                style={{ width: "25%" }}
                aria-valuenow="25"
                aria-valuemin="0"
                aria-valuemax="100"
              >
                <small className="d-flex position-absolute progressBarText2 start-25 ms-3 ms-lg-5 ps-0 ps-lg-5">
                  Personal
                </small>
              </Box>
              <Box
                className="progress-bar me-1 greyBg text-end pe-2"
                role="progressbar"
                style={{ width: "25%" }}
                aria-valuenow="25"
                aria-valuemin="0"
                aria-valuemax="100"
              >
                <small
                  className="d-flex position-absolute progressBarText end-25 ms-2 ms-lg-5 ps-3 ps-lg-5"
                  style={{ marginTop: "40px" }}
                >
                  Bank
                </small>
              </Box>
              <Box
                className="progress-bar greyBg text-end pe-2"
                role="progressbar"
                style={{ width: "25%" }}
                aria-valuenow="25"
                aria-valuemin="0"
                aria-valuemax="100"
              >
                <small className="d-flex position-absolute progressBarText2 end-25 ms-2 ms-lg-5 ps-0 ps-lg-5">
                  Documents
                </small>
              </Box>
            </Box>

            <Box className="mt-4 text-end">
              <button className="btn OtherBtn text-center larger py-1 my-2">
                Complete Registration
              </button>
            </Box>
          </Box>
          <Box className="col-1"></Box>
        </Box>
      </Box>
    </>
  );
};

export default KycStatus;
