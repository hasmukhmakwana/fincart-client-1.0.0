import Box from "@ui/Box/Box";
// import { Carousel } from "react-responsive-carousel";
import Card from "App/ui/Card/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import React, { useState } from 'react'
import Text from "App/ui/Text/Text";
import Input from "@ui/Input/Input";
import CarFill from "App/icons/CarFill";
import MortorboardFill from "App/icons/MortorboardFill";
import Carousel from 'react-elastic-carousel';
import { Button } from "@ui/Button/Button";
import { color } from "echarts";
import styles from "../../../DashboardModule/Dashboard.module.scss";
import ProgressDemo from "./Progress";
import HomeFill from "App/icons/HomeFill";
import { WEB_URL } from "App/utils/constants";
import AlertDialog from "@ui/AlertDialog/ModalDialog";
import PopMessagePage from "../ThankYouModel";
import ProgressOrange from "./ProgressOrange";
import usePortfolioStore from "../../store";
import style from "./Carousel.module.scss"
import Loader from "App/shared/components/Loader";
const PlanButNoInvestment = () => {
    const [progressValue, setprogressValue] = useState(28);
    const [openDialog, setOpenDialog] = useState(false);
    const { goalWisePortfolioData, loading } =
        usePortfolioStore();
    const [infoData, setinfoData] = useState<any>([
        {
            goalCode: "FG1",
            icon: 'motorcycle.png'
        },
        {
            goalCode: "FG2",
            icon: 'FontAwsome (chart-line).png'
        },
        {
            goalCode: "FG3",
            icon: 'FontAwsome (car).png'
        },
        {
            goalCode: "FG4",
            icon: 'FontAwsome (home).png'
        },
        {
            goalCode: "FG5",
            icon: 'FontAwsome (book-reader).png'
        },
        {
            goalCode: "FG6",
            icon: 'FontAwsome (umbrella-beach).png'
        },
        {
            goalCode: "FG7",
            icon: 'NPS.png'
        },
        {
            goalCode: "FG8",
            icon: 'FontAwsome (coins).png'
        },
        {
            goalCode: "FG9",
            icon: 'FontAwsome (wheelchair).png'
        },
        {
            goalCode: "FG10",
            icon: 'FontAwsome (umbrella-beach).png'
        },
        {
            goalCode: "FG11",
            icon: 'ChildSecurity.png'
        },
        {
            goalCode: "FG12",
            icon: 'FontAwsome (book-reader).png'
        },
        {
            goalCode: "FG13",
            icon: 'NPS.png'
        },
        {
            goalCode: "FG14",
            icon: 'Other.png'
        },
        {
            goalCode: "FG17",
            icon: 'EmergencyFund.png'
        },
        {
            goalCode: "FG23",
            icon: 'BankMandates.png'
        },
        {
            goalCode: "FG24",
            icon: 'insurance.png'
        },
    ]);

    const breakPoints = [
        { width: 1, itemsToShow: 2, Readonly: false },
        { width: 550, itemsToShow: 3, Readonly: false },
        { width: 768, itemsToShow: 4, Readonly: false },
        { width: 1200, itemsToShow: 5, Readonly: false },
    ];
    return (
        <>
            {/* @ts-ignore */}
            <Box className="container card">
                <Box className="row text-center pt-4">
                    {/* @ts-ignore */}
                    <Text size="h3" color="goalBlue">Your goals are waiting for you,<br />
                        We recommend that you make a beginning</Text>
                    <Box className="col-12 my-2">
                        <Box className="row row-cols-4 justify-content-center my-1 border-bottom pb-3" css={{ borderColor: "#faac05 !important" }}>
                            {/* For box1 */}

                            {loading ? <><Loader /></> : <>
                                <Carousel isRTL={false} breakPoints={breakPoints}>

                                    {goalWisePortfolioData.map(record => {
                                        let arr = (infoData.filter((ele: any) => ele?.goalCode == record?.goalCode))
                                        // console.log(record, "record");

                                        return (<>

                                            <Box className="col-auto col-lg-auto col-md-auto col-sm-auto border rounded m-2 p-2 ">
                                                <Box className="row justify-content-center ">
                                                    <Box className="col-lg-12 pt-1 pb-1 text-center"   >
                                                        <Box className="row justify-content-center">
                                                            <Box className="col-auto">
                                                                <Text className="align-self-center" css={{ background: "#b3daff", width: 50, borderRadius: "5px" }} >
                                                                    {/* <HomeFill color="#005cb3" width={45}
                                                                height={40} className="rounded p-2" /> */}
                                                                    <img
                                                                        width={45}
                                                                        height={40}
                                                                        src={`${WEB_URL}/${arr[0]?.icon}`}
                                                                        alt={`${arr[0]?.icon}`}
                                                                        className="rounded p-2"
                                                                    />
                                                                </Text>
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                    <Box className=" col-auto mb-2">
                                                        <Text size="h4" color="goalBlue">
                                                            {record?.GName}
                                                        </Text>
                                                    </Box>
                                                </Box>
                                                <Box className="row justify-content-between py-2">
                                                    <Box className="col-auto ">
                                                        <Box>
                                                            <ProgressOrange value={record?.goalAchived} />
                                                            {/* // value={Math.round(parseFloat(record?.investAmount) / parseFloat(record?.Goal_MaturityAmt) * 100)} /> */}
                                                        </Box>
                                                    </Box>
                                                    <Box className="col-auto ">
                                                        <Text size="h6" color="goalBlue">
                                                            {record?.goalAchived}%
                                                        </Text>
                                                    </Box>
                                                </Box>
                                                <Box className="row justify-content-between">
                                                    <Box className="col-auto">
                                                        <Box>
                                                            <Text size="h6" color="goalBlue">
                                                                Goal Target
                                                            </Text>
                                                        </Box>
                                                    </Box>
                                                    <Box className="col-auto">
                                                        <Text size="h6" color="goalBlue">
                                                            {Number(record?.Goal_MaturityAmt).toLocaleString("en-IN")}
                                                        </Text>
                                                    </Box>
                                                </Box>
                                                <Box className="row justify-content-between">
                                                    <Box className="col-auto">
                                                        <Box>
                                                            <Text size="h6" color="goalBlue">
                                                                SIP Amount
                                                            </Text>
                                                        </Box>
                                                    </Box>
                                                    <Box className="col-auto">
                                                        <Text size="h6" color="goalBlue">
                                                            {Number(Math.round(parseInt(record?.investAmount))).toLocaleString("en-IN")}
                                                        </Text>
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </>);
                                    })}

                                </Carousel>
                            </>}

                        </Box>
                        {/* for progress Bar */}
                        <Box className="row justify-content-center mx-auto mt-4">
                            <Box className="col-lg-10 ">
                                <Box className="row text-center">
                                    <Text size="h4" color="goalBlue">
                                        Security Is a Click Away
                                    </Text>
                                </Box>
                            </Box>
                            <Box className="col-lg-9 col-md-9 col-sm-8 my-1">
                                <ProgressDemo prvalue={progressValue} />
                            </Box>
                        </Box>
                        <Box className="row text-end justify-content-center ">
                            <Box className="col-11 col-lg-9 col-md-9 col-sm-11 px-md-4  ms-md-1 ">
                                <Button
                                    className={`py-1 px-4  ${styles.button}`}
                                    color="yellowGroup"
                                    size="md"
                                // css={{mr:55}}
                                >
                                    <Text
                                        weight="normal"
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={styles.button}
                                    >
                                        Execute Plans
                                    </Text>
                                </Button>
                            </Box>
                        </Box>
                        {/* for buttons */}
                        <Box className="row justify-content-center m-2">
                            <Box className=" col-lg-6 col-md-11 col-sm-11">
                                <Box className="row border rounded justify-content-center mx-2 p-2">
                                    {/* <Box className="col-" */}
                                    <Box className="text-center">
                                        <Text size="h4" color="goalBlue">
                                            Would you like us to
                                        </Text>
                                    </Box>
                                    <Box className="row mt-3 justify-content-center">
                                        <Box className="col-lg-6 col-md-8 col-sm-8 px-0">
                                            <Button
                                                className={`w-100 py-1 px-4 ${styles.button}`}
                                                color="yellowGroup"
                                                size="md"
                                                onClick={() => { setOpenDialog(true) }}
                                            >
                                                <Text
                                                    weight="normal"
                                                    size="h6"
                                                    //@ts-ignore
                                                    color="gray8"
                                                    className={styles.button}
                                                >
                                                    Review and Contact You
                                                </Text>
                                            </Button></Box>

                                    </Box>
                                    <Box className="row  mt-3 justify-content-center">
                                        <Box className="col-lg-6 col-md-8 col-sm-8 px-0"> <Button
                                            className={`w-100 py-1 px-4 ${styles.button}`}
                                            color="yellowGroup"
                                            size="md"

                                        >
                                            <Text
                                                weight="normal"
                                                size="h6"
                                                //@ts-ignore
                                                color="gray8"
                                                className={styles.button}
                                            >
                                                Recommend Curated Portfolios
                                            </Text>
                                        </Button></Box>
                                    </Box>
                                    <Box className="row mt-3 justify-content-center">
                                        <Box className="col-lg-6 col-md-8 col-sm-8 px-0">
                                            <Button
                                                className={`w-100 mx-auto py-1 px-4 ${styles.button}`}
                                                color="yellowGroup"
                                                size="md"
                                            >
                                                <Text
                                                    weight="normal"
                                                    size="h6"
                                                    //@ts-ignore
                                                    color="gray8"
                                                    className={styles.button}
                                                >
                                                    Help Revisit Financial Plan
                                                </Text>
                                            </Button>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Box>
            <AlertDialog
                open={openDialog}
                setOpen={setOpenDialog}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "40%" },
                }}
            >
                <PopMessagePage />
            </AlertDialog>
        </>
    )
}


export default PlanButNoInvestment