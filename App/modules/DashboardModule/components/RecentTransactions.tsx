import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import Layout, { toastAlert } from "App/shared/components/Layout";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@ui/Accordian/Accordian";
import { recentTransaction } from "App/api/dashboard";
import { encryptData, getUser } from "App/utils/helpers";
import DownArrow from "App/icons/DownArrow";
import Badge from "@ui/Badge/Badge";
import { Button } from "@ui/Button/Button";
import { WEB_URL } from "App/utils/constants";
import Loader from "App/shared/components/Loader";
import usePortfolioStore from "../store";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import useHeaderStore from "App/shared/components/Header/store";
import { useEffect, useState } from "react";
import { loadingIndicatorCSS } from "react-select/dist/declarations/src/components/indicators";
let recentTabs = [
  {
    title: "Mutual Fund",
    value: "MF",
  },
  {
    title: "Insurance",
    value: "INSURANCE",
  },
  {
    title: "Alternate",
    value: "ALTERNATE",
  },
];

const RecentTransactions = () => {
  const [changeActive, setChangeActive] = useState("");
  const user: any = getUser();
  const [tabNavMf, setTabNavMf] = useState<Number>(1);
  const [tabNavIn, setTabNavIn] = useState<Number>(1);
  const [tabNavAlt, setTabNavAlt] = useState<Number>(1);
  const [selectmonth, setselectmonth] = useState<any>({});
  const [filterData1, setFilterData1] = useState<any>([]);
  const [filterData2, setFilterData2] = useState<any>([]);
  const [filterData3, setFilterData3] = useState<any>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const { recentTransactions, setRecentTransactions, setLoader, basicId } =
    usePortfolioStore();

  const fetchRecentData = async (obj: any) => {
    try {
      setLoading(true);
      const enc: any = encryptData(obj);

      const result: any = await recentTransaction(enc);
      setLoading(false);

      setRecentTransactions(result?.data);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoading(false);
    }
  };

  useEffect(() => {
    var monthName = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec");
    var months1: any = [];
    var months2: any = [];
    var months3: any = [];
    var d = new Date();
    d.setDate(1);
    for (let i = 0; i <= 11; i++) {
      let ab = {
        id: i + 1,
        name: monthName[d.getMonth()] + ' ' + d.getFullYear(),
        month: monthName[d.getMonth()],
        year: d.getFullYear()
      }
      if (i < 4)
        months1.push(ab);
      if (i >= 4 && i < 8)
        months2.push(ab);
      if (i >= 8 && i < 12)
        months3.push(ab);
      d.setMonth(d.getMonth() - 1);
    }
    setselectmonth(months1[0])
    setFilterData1(months1)
    setFilterData2(months2)
    setFilterData3(months3)
  }, [])


  function checkLeapYear(year: any) {

    //three conditions to find out the leap year
    if ((0 == year % 4) && (0 != year % 100) || (0 == year % 400)) {
      return true;
    } else {
      return false;
    }
  }


  const handleMonthWiseTransactions = (e: any, type: any) => {
    if (basicId === "") return;
    if (e === "")
      return;
    var monthName = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec");
    var d = new Date();
    setLoading(true);
    let obj = {}
    let month = monthName.indexOf(e?.month)
    if (e.month === monthName[d.getMonth()]) {
      obj = {
        id: basicId,
        asset_type: type,
        from_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/01`,
        to_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/${d.getDate() < 10 ? '0' + d.getDate() : d.getDate()}`,
      };
    }
    else {
      if (e?.month === "Jan" || e?.month === "Mar" || e?.month === "May" || e.month === "Jul" || e?.month === "Aug" || e?.month === "Oct" || e?.month == "Dec") {
        obj = {
          id: basicId,
          asset_type: type,
          from_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/01`,
          to_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/31`,
        };
      }
      else if (e?.month === "Apr" || e?.month === "Jun" || e?.month === "Sept" || e?.month === "Nov") {
        obj = {
          id: basicId,
          asset_type: type,
          from_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/01`,
          to_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/30`,
        };
      }
      else {
        if (checkLeapYear(e?.year)) {
          obj = {
            id: basicId,
            asset_type: type,
            from_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/01`,
            to_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/29`,
          };
        }
        else {
          obj = {
            id: basicId,
            asset_type: type,
            from_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/01`,
            to_date: `${e.year}/${month < 9 ? '0' + (month + 1) : (month + 1)}/28`,
          };
        }
      }
    }
    fetchRecentData(obj);
  }
  return (
    <>
      <Text
        /* @ts-ignore */
        size="h3" color="goalBlue">
        Transactions
      </Text>
      <Card css={{ p: 25 }}>
        <GroupBox
          /* @ts-ignore */
          position="apart" css={{ mb: 0 }}>
          <Box className="mb-2">
            <Text
              /* @ts-ignore */
              size="h4" color="goalBlue">
              Recent
            </Text>
          </Box>
        </GroupBox>
        <StyledTabs
          defaultValue="MF"
          onValueChange={(e) => {
            setChangeActive(e);
          }}
        >
          <TabsList
            aria-label="Manage your account"
            className="tabsStyle"
            css={{ mb: 20 }}
          >
            {recentTabs.map((item, index) => {
              return (
                <TabsTrigger
                  value={item.value}
                  className="tabs"
                  key={item.value}
                >
                  <Text
                    /* @ts-ignore */
                    size="h6">{item.title}</Text>
                </TabsTrigger>
              );
            })}
          </TabsList>
          <TabsContent value="MF" className="dashboard-list-scroll">
            <Box className="row mb-3">
              <Box className="col">
                <Button className="takeactionBtn" color="yellow" disabled={tabNavMf === 1 ? true : false} onClick={() => { setTabNavMf(Number(tabNavMf) - 1) }}>
                  Prev
                </Button>
              </Box>
              <Box className="col text-end">
                <Button className="takeactionBtn" color="yellow" disabled={tabNavMf === 3 ? true : false} onClick={() => { setTabNavMf(Number(tabNavMf) + 1) }}>
                  Next
                </Button>
              </Box>
            </Box>
            {tabNavMf === 1 ?
              <>
                <Accordion type="single" defaultValue="0" collapsible onValueChange={(e) => {
                  handleMonthWiseTransactions(e, "MF");
                }} >
                  {filterData1.map((ele: any, i: any) => {
                    return (
                      <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                        <AccordionTrigger>
                          <Box>
                            <Text
                              /* @ts-ignore */
                              size="h5">{ele?.name}</Text>
                          </Box>
                          <Box className="action">
                            <DownArrow />
                          </Box>
                        </AccordionTrigger>
                        <AccordionContent>
                          {loading ? (
                            <Loader />
                          ) : (
                            <>
                              {recentTransactions.length === 0 ?
                                <GroupBox
                                  /* @ts-ignore */
                                  position="apart"
                                  css={{ mb: 20, alignItems: "flex-start" }}
                                >
                                  <Box>
                                    <Text
                                      /* @ts-ignore */
                                      color="default">No Data Found </Text>
                                  </Box>
                                </GroupBox>
                                : <>
                                  {recentTransactions.map((record) => {
                                    let dt = new Date(record?.trxnDate);
                                    let lastdt =
                                      dt.getDate() +
                                      "/" +
                                      (dt.getMonth() + 1) +
                                      "/" +
                                      dt.getFullYear();
                                    return (
                                      <>
                                        {record?.assettype === "MF" ? (
                                          <GroupBox
                                            /* @ts-ignore */
                                            position="apart"
                                            css={{ mb: 20, alignItems: "flex-start" }}
                                          >
                                            <Box>
                                              <GroupBox
                                                /* @ts-ignore */
                                                position="apart">
                                                <Box>
                                                  <img
                                                    width={50}
                                                    height={50}
                                                    src={record?.fund_img}
                                                    alt="logo"
                                                  />
                                                </Box>
                                                <Box>
                                                  <Text
                                                    /* @ts-ignore */
                                                    color="default" size="bt2">
                                                    {record?.SchemeName}{" "}
                                                  </Text>
                                                  <Text
                                                    /* @ts-ignore */
                                                    color="goalBlue" size="bt2">
                                                    &#x20B9;{" "}
                                                    {Number(record?.Amount).toLocaleString(
                                                      "en-IN"
                                                    )}{" "}
                                                  </Text>
                                                </Box>
                                              </GroupBox>
                                            </Box>
                                            <Box className="text-end">
                                              <Badge size="sm" className="d-inline-block">
                                                {/* {record?.Trans_Type} */}
                                                {record?.Trans_Type === "R" ? "Redeem"
                                                  : record?.Trans_Type === "NOR" ? "Lumpsum"
                                                    : record?.Trans_Type || ""
                                                }
                                              </Badge>
                                              <Text
                                                /* @ts-ignore */
                                                color="goalBlue" size="bt2">{lastdt}</Text>
                                            </Box>
                                          </GroupBox>
                                        ) : ""}
                                      </>
                                    );
                                  })}
                                </>}
                            </>
                          )}
                        </AccordionContent>
                      </AccordionItem>
                    )
                  })}
                </Accordion>
              </>
              : tabNavMf === 2 ?
                <>
                  <Accordion type="single" collapsible onValueChange={(e) => {
                    handleMonthWiseTransactions(e, "MF");
                  }}>
                    {filterData2.map((ele: any, i: any) => {
                      return (
                        <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                          <AccordionTrigger>
                            <Box>
                              {/* @ts-ignore */}
                              <Text size="h5">{ele?.name}</Text>
                            </Box>
                            <Box className="action">
                              <DownArrow />
                            </Box>
                          </AccordionTrigger>
                          <AccordionContent>
                            {loading ? (
                              <Loader />
                            ) : (
                              <>
                                {recentTransactions.length === 0 ?
                                  <GroupBox
                                    /* @ts-ignore */
                                    position="apart"
                                    css={{ mb: 20, alignItems: "flex-start" }}
                                  >
                                    <Box>
                                      {/* @ts-ignore */}
                                      <Text color="default">No Data Found </Text>
                                    </Box>
                                  </GroupBox>
                                  : <>
                                    {recentTransactions.map((record) => {
                                      let dt = new Date(record?.trxnDate);
                                      let lastdt =
                                        dt.getDate() +
                                        "/" +
                                        (dt.getMonth() + 1) +
                                        "/" +
                                        dt.getFullYear();
                                      return (
                                        <>
                                          {record?.assettype === "MF" ? (
                                            <GroupBox
                                              // @ts-ignore
                                              position="apart" css={{ mb: 20, alignItems: "flex-start" }}>
                                              <Box>
                                                {/* @ts-ignore */}
                                                <GroupBox position="apart">
                                                  <Box>
                                                    <img
                                                      width={50}
                                                      height={50}
                                                      src={record?.fund_img}
                                                      alt="logo"
                                                    />
                                                  </Box>
                                                  <Box>
                                                    {/* @ts-ignore */}
                                                    <Text color="default" size="bt2">
                                                      {record?.SchemeName}{" "}
                                                    </Text>
                                                    {/* @ts-ignore */}
                                                    <Text color="goalBlue" size="bt2">
                                                      &#x20B9;{" "}
                                                      {Number(record?.Amount).toLocaleString(
                                                        "en-IN"
                                                      )}{" "}
                                                    </Text>
                                                  </Box>
                                                </GroupBox>
                                              </Box>
                                              <Box className="text-end">
                                                <Badge size="sm" className="d-inline-block">
                                                  {record?.Trans_Type}
                                                </Badge>
                                                {/* @ts-ignore */}
                                                <Text color="goalBlue" size="bt2">{lastdt}</Text>
                                              </Box>
                                            </GroupBox>
                                          ) : ""}
                                        </>
                                      );
                                    })}
                                  </>}
                              </>
                            )}
                          </AccordionContent>
                        </AccordionItem>
                      )
                    })}
                  </Accordion>
                </>
                : tabNavMf === 3 ?
                  <>
                    <Accordion type="single" collapsible onValueChange={(e) => {
                      handleMonthWiseTransactions(e, "MF");
                    }}>
                      {filterData3.map((ele: any, i: any) => {
                        return (
                          <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                            <AccordionTrigger>
                              <Box>
                                {/* @ts-ignore */}
                                <Text size="h5">{ele?.name}</Text>
                              </Box>
                              <Box className="action">
                                <DownArrow />
                              </Box>
                            </AccordionTrigger>

                            <AccordionContent>
                              {loading ? (
                                <Loader />
                              ) : (
                                <>
                                  {recentTransactions.length === 0 ?
                                    <GroupBox
                                      //  @ts-ignore
                                      position="apart"
                                      css={{ mb: 20, alignItems: "flex-start" }}
                                    >
                                      <Box>
                                        {/* @ts-ignore */}
                                        <Text color="default">No Data Found </Text>
                                      </Box>
                                    </GroupBox>
                                    : <>
                                      {recentTransactions.map((record) => {
                                        let dt = new Date(record?.trxnDate);
                                        let lastdt =
                                          dt.getDate() +
                                          "/" +
                                          (dt.getMonth() + 1) +
                                          "/" +
                                          dt.getFullYear();
                                        return (
                                          <>
                                            {record?.assettype === "MF" ? (
                                              <GroupBox
                                                // @ts-ignore
                                                position="apart"
                                                css={{ mb: 20, alignItems: "flex-start" }}
                                              >
                                                <Box>
                                                  {/* @ts-ignore */}
                                                  <GroupBox position="apart">
                                                    <Box>
                                                      <img
                                                        width={50}
                                                        height={50}
                                                        src={record?.fund_img}
                                                        alt="logo"
                                                      />
                                                    </Box>
                                                    <Box>
                                                      {/* @ts-ignore */}
                                                      <Text color="default" size="bt2">
                                                        {record?.SchemeName}{" "}
                                                      </Text>
                                                      {/* @ts-ignore */}
                                                      <Text color="goalBlue" size="bt2">
                                                        &#x20B9;{" "}
                                                        {Number(record?.Amount).toLocaleString(
                                                          "en-IN"
                                                        )}{" "}
                                                      </Text>
                                                    </Box>
                                                  </GroupBox>
                                                </Box>
                                                <Box className="text-end">
                                                  <Badge size="sm" className="d-inline-block">
                                                    {record?.Trans_Type}
                                                  </Badge>
                                                  {/* @ts-ignore */}
                                                  <Text color="goalBlue" size="bt2">{lastdt}</Text>
                                                </Box>
                                              </GroupBox>
                                            ) : ""}
                                          </>
                                        );
                                      })}
                                    </>}
                                </>
                              )}
                            </AccordionContent>

                          </AccordionItem>
                        )
                      })}
                    </Accordion>
                  </>
                  : ""}

          </TabsContent>
          <TabsContent value="INSURANCE" className="dashboard-list-scroll">
            <Box className="row mb-3">
              <Box className="col">
                <Button className="takeactionBtn" color="yellow" disabled={tabNavIn === 1 ? true : false} onClick={() => { setTabNavIn(Number(tabNavIn) - 1) }}>
                  Prev
                </Button>
              </Box>
              <Box className="col text-end">
                <Button className="takeactionBtn" color="yellow" disabled={tabNavIn === 3 ? true : false} onClick={() => { setTabNavIn(Number(tabNavIn) + 1) }}>
                  Next
                </Button>
              </Box>
            </Box>

            {tabNavIn === 1 ?
              <>
                <Accordion type="single" defaultValue="0" collapsible onValueChange={(e) => {
                  handleMonthWiseTransactions(e, "INSURANCE");
                }} >
                  {filterData1.map((ele: any, i: any) => {
                    return (
                      <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                        <AccordionTrigger>
                          <Box>
                            {/* @ts-ignore */}
                            <Text size="h5">{ele?.name}</Text>
                          </Box>
                          <Box className="action">
                            <DownArrow />
                          </Box>
                        </AccordionTrigger>
                        <AccordionContent>
                          {loading ? (
                            <Loader />
                          ) : (
                            <>
                              {recentTransactions.length === 0 ?
                                <GroupBox
                                  //  @ts-ignore
                                  position="apart"
                                  css={{ mb: 20, alignItems: "flex-start" }}
                                >
                                  <Box>
                                    {/* @ts-ignore */}
                                    <Text color="default">No Data Found </Text>
                                  </Box>
                                </GroupBox>
                                : <>
                                  {recentTransactions.map((record) => {
                                    let dt = new Date(record?.trxnDate);
                                    let lastdt =
                                      dt.getDate() +
                                      "/" +
                                      (dt.getMonth() + 1) +
                                      "/" +
                                      dt.getFullYear();
                                    return (
                                      <>
                                        {record?.assettype === "INSURANCE" ? (
                                          <GroupBox
                                            // @ts-ignore
                                            position="apart"
                                            css={{ mb: 20, alignItems: "flex-start" }}
                                          >
                                            <Box>
                                              {/* @ts-ignore */}
                                              <GroupBox position="apart">
                                                <Box>
                                                  <img
                                                    width={50}
                                                    height={50}
                                                    src={record?.fund_img}
                                                    alt="logo"
                                                  />
                                                </Box>
                                                <Box>
                                                  {/* @ts-ignore */}
                                                  <Text color="default" size="bt2">
                                                    {record?.SchemeName}{" "}
                                                  </Text>
                                                  {/* @ts-ignore */}
                                                  <Text color="goalBlue" size="bt2">
                                                    &#x20B9;{" "}
                                                    {Number(record?.Amount).toLocaleString(
                                                      "en-IN"
                                                    )}{" "}
                                                  </Text>
                                                </Box>
                                              </GroupBox>
                                            </Box>
                                            <Box className="text-end">
                                              <Badge size="sm" className="d-inline-block">
                                                {record?.Trans_Type}
                                              </Badge>
                                              {/* @ts-ignore */}
                                              <Text color="goalBlue" size="bt2">{lastdt}</Text>
                                            </Box>
                                          </GroupBox>
                                        ) : ""}
                                      </>
                                    );
                                  })}
                                </>}
                            </>
                          )}
                        </AccordionContent>
                      </AccordionItem>
                    )
                  })}
                </Accordion>
              </>
              : tabNavIn === 2 ?
                <>
                  <Accordion type="single" collapsible onValueChange={(e) => {
                    handleMonthWiseTransactions(e, "INSURANCE");
                  }}>
                    {filterData2.map((ele: any, i: any) => {
                      return (
                        <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                          <AccordionTrigger>
                            <Box>
                              {/* @ts-ignore */}
                              <Text size="h5">{ele?.name}</Text>
                            </Box>
                            <Box className="action">
                              <DownArrow />
                            </Box>
                          </AccordionTrigger>
                          <AccordionContent>
                            {loading ? (
                              <Loader />
                            ) : (
                              <>
                                {recentTransactions.length === 0 ?
                                  <GroupBox
                                    // @ts-ignore
                                    position="apart"
                                    css={{ mb: 20, alignItems: "flex-start" }}
                                  >
                                    <Box>
                                      {/* @ts-ignore */}
                                      <Text color="default">No Data Found </Text>
                                    </Box>
                                  </GroupBox>
                                  : <>
                                    {recentTransactions.map((record) => {
                                      let dt = new Date(record?.trxnDate);
                                      let lastdt =
                                        dt.getDate() +
                                        "/" +
                                        (dt.getMonth() + 1) +
                                        "/" +
                                        dt.getFullYear();
                                      return (
                                        <>
                                          {record?.assettype === "INSURANCE" ? (
                                            <GroupBox
                                              //@ts-ignore
                                              position="apart"
                                              css={{ mb: 20, alignItems: "flex-start" }}
                                            >
                                              <Box>
                                                {/* @ts-ignore */}
                                                <GroupBox position="apart">
                                                  <Box>
                                                    <img
                                                      width={50}
                                                      height={50}
                                                      src={record?.fund_img}
                                                      alt="logo"
                                                    />
                                                  </Box>
                                                  <Box>
                                                    {/* @ts-ignore */}
                                                    <Text color="default" size="bt2">
                                                      {record?.SchemeName}{" "}
                                                    </Text>
                                                    {/* @ts-ignore */}
                                                    <Text color="goalBlue" size="bt2">
                                                      &#x20B9;{" "}
                                                      {Number(record?.Amount).toLocaleString(
                                                        "en-IN"
                                                      )}{" "}
                                                    </Text>
                                                  </Box>
                                                </GroupBox>
                                              </Box>
                                              <Box className="text-end">
                                                <Badge size="sm" className="d-inline-block">
                                                  {record?.Trans_Type}
                                                </Badge>
                                                {/* @ts-ignore */}
                                                <Text color="goalBlue" size="bt2">{lastdt}</Text>
                                              </Box>
                                            </GroupBox>
                                          ) : ""}
                                        </>
                                      );
                                    })}
                                  </>}
                              </>
                            )}
                          </AccordionContent>
                        </AccordionItem>
                      )
                    })}
                  </Accordion>
                </>
                : tabNavIn === 3 ?
                  <>
                    <Accordion type="single" collapsible onValueChange={(e) => {
                      handleMonthWiseTransactions(e, "INSURANCE");
                    }}>
                      {filterData3.map((ele: any, i: any) => {
                        return (
                          <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                            <AccordionTrigger>
                              <Box>
                                {/* @ts-ignore */}
                                <Text size="h5">{ele?.name}</Text>
                              </Box>
                              <Box className="action">
                                <DownArrow />
                              </Box>
                            </AccordionTrigger>

                            <AccordionContent>
                              {loading ? (
                                <Loader />
                              ) : (
                                <>
                                  {recentTransactions.length === 0 ?
                                    <GroupBox
                                      //@ts-ignore
                                      position="apart"
                                      css={{ mb: 20, alignItems: "flex-start" }}
                                    >
                                      <Box>
                                        {/* @ts-ignore */}
                                        <Text color="default">No Data Found </Text>
                                      </Box>
                                    </GroupBox>
                                    : <>
                                      {recentTransactions.map((record) => {
                                        let dt = new Date(record?.trxnDate);
                                        let lastdt =
                                          dt.getDate() +
                                          "/" +
                                          (dt.getMonth() + 1) +
                                          "/" +
                                          dt.getFullYear();
                                        return (
                                          <>
                                            {record?.assettype === "INSURANCE" ? (
                                              <GroupBox
                                                //@ts-ignore
                                                position="apart"
                                                css={{ mb: 20, alignItems: "flex-start" }}
                                              >
                                                <Box>
                                                  {/* @ts-ignore */}
                                                  <GroupBox position="apart">
                                                    <Box>
                                                      <img
                                                        width={50}
                                                        height={50}
                                                        src={record?.fund_img}
                                                        alt="logo"
                                                      />
                                                    </Box>
                                                    <Box>
                                                      {/* @ts-ignore */}
                                                      <Text color="default" size="bt2">
                                                        {record?.SchemeName}{" "}
                                                      </Text>
                                                      {/* @ts-ignore */}
                                                      <Text color="goalBlue" size="bt2">
                                                        &#x20B9;{" "}
                                                        {Number(record?.Amount).toLocaleString(
                                                          "en-IN"
                                                        )}{" "}
                                                      </Text>
                                                    </Box>
                                                  </GroupBox>
                                                </Box>
                                                <Box className="text-end">
                                                  <Badge size="sm" className="d-inline-block">
                                                    {record?.Trans_Type}
                                                  </Badge>
                                                  {/* @ts-ignore */}
                                                  <Text color="goalBlue" size="bt2">{lastdt}</Text>
                                                </Box>
                                              </GroupBox>
                                            ) : ""}
                                          </>
                                        );
                                      })}
                                    </>}
                                </>
                              )}
                            </AccordionContent>

                          </AccordionItem>
                        )
                      })}
                    </Accordion>
                  </>
                  : ""}
          </TabsContent>
          <TabsContent value="ALTERNATE" className="dashboard-list-scroll">
            <Box className="row mb-3">
              <Box className="col">
                <Button className="takeactionBtn" color="yellow" disabled={tabNavAlt === 1 ? true : false} onClick={() => { setTabNavAlt(Number(tabNavAlt) - 1) }}>
                  Prev
                </Button>
              </Box>
              <Box className="col text-end">
                <Button className="takeactionBtn" color="yellow" disabled={tabNavAlt === 3 ? true : false} onClick={() => { setTabNavAlt(Number(tabNavAlt) + 1) }}>
                  Next
                </Button>
              </Box>
            </Box>

            {tabNavAlt === 1 ?
              <>
                <Accordion type="single" defaultValue="0" collapsible onValueChange={(e) => {
                  handleMonthWiseTransactions(e, "ALTERNATE");
                }} >
                  {filterData1.map((ele: any, i: any) => {
                    return (
                      <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                        <AccordionTrigger>
                          <Box>
                            {/* @ts-ignore */}
                            <Text size="h5">{ele?.name}</Text>
                          </Box>
                          <Box className="action">
                            <DownArrow />
                          </Box>
                        </AccordionTrigger>
                        <AccordionContent>
                          {loading ? (
                            <Loader />
                          ) : (
                            <>
                              {recentTransactions.length === 0 ?
                                <GroupBox
                                  //@ts-ignore
                                  position="apart"
                                  css={{ mb: 20, alignItems: "flex-start" }}
                                >
                                  <Box>
                                    {/* @ts-ignore */}
                                    <Text color="default">No Data Found </Text>
                                  </Box>
                                </GroupBox>
                                : <>
                                  {recentTransactions.map((record) => {
                                    let dt = new Date(record?.trxnDate);
                                    let lastdt =
                                      dt.getDate() +
                                      "/" +
                                      (dt.getMonth() + 1) +
                                      "/" +
                                      dt.getFullYear();
                                    return (
                                      <>
                                        {record?.assettype === "ALTERNATE" ? (
                                          <GroupBox
                                            //@ts-ignore
                                            position="apart"
                                            css={{ mb: 20, alignItems: "flex-start" }}
                                          >
                                            <Box>
                                              {/* @ts-ignore */}
                                              <GroupBox position="apart">
                                                <Box>
                                                  <img
                                                    width={50}
                                                    height={50}
                                                    src={record?.fund_img}
                                                    alt="logo"
                                                  />
                                                </Box>
                                                <Box>
                                                  {/* @ts-ignore */}
                                                  <Text color="default" size="bt2">
                                                    {record?.SchemeName}{" "}
                                                  </Text>
                                                  {/* @ts-ignore */}
                                                  <Text color="goalBlue" size="bt2">
                                                    &#x20B9;{" "}
                                                    {Number(record?.Amount).toLocaleString(
                                                      "en-IN"
                                                    )}{" "}
                                                  </Text>
                                                </Box>
                                              </GroupBox>
                                            </Box>
                                            <Box className="text-end">
                                              <Badge size="sm" className="d-inline-block">
                                                {record?.Trans_Type}
                                              </Badge>
                                              {/* @ts-ignore */}
                                              <Text color="goalBlue" size="bt2">{lastdt}</Text>
                                            </Box>
                                          </GroupBox>
                                        ) : ""}
                                      </>
                                    );
                                  })}
                                </>}
                            </>
                          )}
                        </AccordionContent>
                      </AccordionItem>
                    )
                  })}
                </Accordion>
              </>
              : tabNavAlt === 2 ?
                <>
                  <Accordion type="single" collapsible onValueChange={(e) => {
                    handleMonthWiseTransactions(e, "ALTERNATE");
                  }}>
                    {filterData2.map((ele: any, i: any) => {
                      return (
                        <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                          <AccordionTrigger>
                            <Box>
                              {/* @ts-ignore */}
                              <Text size="h5">{ele?.name}</Text>
                            </Box>
                            <Box className="action">
                              <DownArrow />
                            </Box>
                          </AccordionTrigger>
                          <AccordionContent>
                            {loading ? (
                              <Loader />
                            ) : (
                              <>
                                {recentTransactions.length === 0 ?
                                  <GroupBox
                                    //@ts-ignore
                                    position="apart"
                                    css={{ mb: 20, alignItems: "flex-start" }}
                                  >
                                    <Box>
                                      {/* @ts-ignore */}
                                      <Text color="default">No Data Found </Text>
                                    </Box>
                                  </GroupBox>
                                  : <>
                                    {recentTransactions.map((record) => {
                                      let dt = new Date(record?.trxnDate);
                                      let lastdt =
                                        dt.getDate() +
                                        "/" +
                                        (dt.getMonth() + 1) +
                                        "/" +
                                        dt.getFullYear();
                                      return (
                                        <>
                                          {record?.assettype === "ALTERNATE" ? (
                                            <GroupBox
                                              //@ts-ignore
                                              position="apart"
                                              css={{ mb: 20, alignItems: "flex-start" }}
                                            >
                                              <Box>
                                                {/* @ts-ignore */}
                                                <GroupBox position="apart">
                                                  <Box>
                                                    <img
                                                      width={50}
                                                      height={50}
                                                      src={record?.fund_img}
                                                      alt="logo"
                                                    />
                                                  </Box>
                                                  <Box>
                                                    {/* @ts-ignore */}
                                                    <Text color="default" size="bt2">
                                                      {record?.SchemeName}{" "}
                                                    </Text>
                                                    {/* @ts-ignore */}
                                                    <Text color="goalBlue" size="bt2">
                                                      &#x20B9;{" "}
                                                      {Number(record?.Amount).toLocaleString(
                                                        "en-IN"
                                                      )}{" "}
                                                    </Text>
                                                  </Box>
                                                </GroupBox>
                                              </Box>
                                              <Box className="text-end">
                                                <Badge size="sm" className="d-inline-block">
                                                  {record?.Trans_Type}
                                                </Badge>
                                                {/* @ts-ignore */}
                                                <Text color="goalBlue" size="bt2">{lastdt}</Text>
                                              </Box>
                                            </GroupBox>
                                          ) : ""}
                                        </>
                                      );
                                    })}
                                  </>}
                              </>
                            )}
                          </AccordionContent>
                        </AccordionItem>
                      )
                    })}
                  </Accordion>
                </>
                : tabNavAlt === 3 ?
                  <>
                    <Accordion type="single" collapsible onValueChange={(e) => {
                      handleMonthWiseTransactions(e, "ALTERNATE");
                    }}>
                      {filterData3.map((ele: any, i: any) => {
                        return (
                          <AccordionItem value={ele} css={{ borderRadius: "8px" }}>
                            <AccordionTrigger>
                              <Box>
                                {/* @ts-ignore */}
                                <Text size="h5">{ele?.name}</Text>
                              </Box>
                              <Box className="action">
                                <DownArrow />
                              </Box>
                            </AccordionTrigger>

                            <AccordionContent>
                              {loading ? (
                                <Loader />
                              ) : (
                                <>
                                  {recentTransactions.length === 0 ?
                                    <GroupBox
                                      //@ts-ignore
                                      position="apart"
                                      css={{ mb: 20, alignItems: "flex-start" }}
                                    >
                                      <Box>
                                        {/* @ts-ignore */}
                                        <Text color="default">No Data Found </Text>
                                      </Box>
                                    </GroupBox>
                                    : <>
                                      {recentTransactions.map((record) => {
                                        let dt = new Date(record?.trxnDate);
                                        let lastdt =
                                          dt.getDate() +
                                          "/" +
                                          (dt.getMonth() + 1) +
                                          "/" +
                                          dt.getFullYear();
                                        return (
                                          <>
                                            {record?.assettype === "ALTERNATE" ? (
                                              <GroupBox
                                                //@ts-ignore
                                                position="apart"
                                                css={{ mb: 20, alignItems: "flex-start" }}
                                              >
                                                <Box>
                                                  {/* @ts-ignore */}
                                                  <GroupBox position="apart">
                                                    <Box>
                                                      <img
                                                        width={50}
                                                        height={50}
                                                        src={record?.fund_img}
                                                        alt="logo"
                                                      />
                                                    </Box>
                                                    <Box>
                                                      {/* @ts-ignore */}
                                                      <Text color="default" size="bt2">
                                                        {record?.SchemeName}{" "}
                                                      </Text>
                                                      {/* @ts-ignore */}
                                                      <Text color="goalBlue" size="bt2">
                                                        &#x20B9;{" "}
                                                        {Number(record?.Amount).toLocaleString(
                                                          "en-IN"
                                                        )}{" "}
                                                      </Text>
                                                    </Box>
                                                  </GroupBox>
                                                </Box>
                                                <Box className="text-end">
                                                  <Badge size="sm" className="d-inline-block">
                                                    {record?.Trans_Type}
                                                  </Badge>
                                                  {/* @ts-ignore */}
                                                  <Text color="goalBlue" size="bt2">{lastdt}</Text>
                                                </Box>
                                              </GroupBox>
                                            ) : ""}
                                          </>
                                        );
                                      })}
                                    </>}
                                </>
                              )}
                            </AccordionContent>

                          </AccordionItem>
                        )
                      })}
                    </Accordion>
                  </>
                  : ""}
          </TabsContent>
        </StyledTabs>
      </Card>
    </>
  );
};

export default RecentTransactions;
