import React from 'react'
import Box from "App/ui/Box/Box";
// import Text from "App/ui/Text/Text";
import style from "../Dashboard.module.scss"
import { Button } from '@ui/Button/Button';
import router from 'next/router';
import useRegistrationStore from 'App/modules/RegistrationModule/store';
// import Progress from '@ui/Progress/Progress';
// import ProgressOrange from '@ui/Progress/ProgressOrange';
const kycStatusbar = ({ view }: any) => {
    const { setBasicID, setRegiType } = useRegistrationStore();
    return (
        <Box>
            <Box className="row mb-4">

                <Box className={`container ${style.bgWhite} rounded-4 pb-2 mb-4`}>
                    <Box className={`${style.sectionMainHeading} mb-2 pt-3 text-center`}>You're KYC Verified !<br />
                        <span className={`${style.lightTextBlue}`}>Add your bank account to start investing</span>
                    </Box>
                    <Box className="row mt-2 justify-content-center">
                        {/* <Box className="col-1"></Box> */}
                        <Box className={`col-11 col-sm-11 col-md-11 col-lg-10 ${style.sectionMainHeading} text-center mt-3 mt-md-0`}>

                            <Box className="row" css={{ height: "25px", width: "100%" }}>
                                <Box
                                    className={`col-3 border ${view?.isAccountDone ? style.darkBlueColorBg : style.lightBlueColorBg}`}
                                    css={{ borderColor: "#fff", borderRadius: "30px 0px 0px 30px" }}>
                                </Box>
                                <Box className={`col-3 border ${view?.isBasicDone ? style.darkBlueColorBg : view?.isAccountDone ? style.lightBlueColorBg : style.greyBg}`}
                                    css={{ borderColor: "#fff" }}>
                                </Box>
                                <Box className={`col-3 border ${view?.isBankDone ? style.darkBlueColorBg : view?.isBasicDone ? style.lightBlueColorBg : style.greyBg}`}
                                    css={{ borderColor: "#fff" }}>
                                </Box>
                                <Box className={`col-3 border ${view?.cafstatus === "success" ? view?.isBankDone ? style.darkBlueColorBg : style.lightBlueColorBg : style.greyBg}`}
                                    css={{ borderColor: "#fff", borderRadius: " 0px 30px 30px 0px " }}>
                                </Box>
                            </Box>
                            <Box className={`row ${style.kycStatusText}`} >
                                <Box className={`col-3`} >KYC Verification</Box>
                                <Box className={`col-3`} >Personal</Box>
                                <Box className={`col-3`} >Bank</Box>
                                <Box className={`col-3`} >Documents</Box>
                            </Box>
                            <Box className="row mt-4 text-end justify-content-end mx-1">
                                <Box className="col-auto">
                                    <Button
                                        className={`btn ${style.OtherBtn} text-center larger py-1 my-2`}
                                        onClick={() => {
                                            setBasicID(view?.basicid)
                                            setRegiType(view?.cafstatus)
                                            router.push("/Profile")
                                        }}
                                        disabled={view?.cafstatus === "success"}
                                    >{view?.cafstatus === "success" ? "Registration Complete" : "Complete Registration"}</Button>
                                </Box>
                            </Box>
                        </Box>
                        {/* <Box className="col-1"></Box> */}
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

export default kycStatusbar
