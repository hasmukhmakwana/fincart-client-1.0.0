import React, { useEffect, useState } from "react";
import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import ReactECharts from "echarts-for-react";
import Card from "App/ui/Card/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import Badge from "@ui/Badge/Badge";
import { Carousel } from "react-responsive-carousel";
import { TabsContent } from "@radix-ui/react-tabs";
import Layout, { toastAlert } from "App/shared/components/Layout";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import usePortfolioStore from "../store";
import Loader from "App/shared/components/Loader";
import {
  assetAllocationsSubCategory, growthChart
} from "App/api/dashboard";
import { encryptData, getUser } from "App/utils/helpers";
import { RadioGroup } from "@radix-ui/react-radio-group";
import Radio from "@ui/Radio/Radio";
import useSwitchHoldingStore from "App/shared/components/Holder/store";

let assetAllocationTabs = [
  {
    title: "Mutual Fund",
    value: "MutualFund",
  },
  {
    title: "Overall",
    value: "Overall",
  },
];
let InvestmentGrowthTabs = [
  {
    title: "Mutual Fund",
    value: "MF",
  },
  {
    title: "ULIP",
    value: "ULIP",
  },
  {
    title: "PMS",
    value: "PMS",
  },
];

const Analytics = () => {
  const {
    selectMember
  } = useSwitchHoldingStore()
  const [showSubCategory, setShowSubCategory] = useState<string>("");
  const [investmentActive, setInvestmentActive] = useState<string>("");
  const [pmsActive, setPMSActive] = useState<string>("whiteoak");
  const [user, setUser] = useState(selectMember);
  const { loading, setLoader, mfChartData, overallChartData, growthChartData, monthGrowthChart, legendNamesOA, legendNamesMF, setGrowthChartData,
    setMonthGrowthChart } = usePortfolioStore();
  const [subCategoryData, setSubCategoryData] = useState<any>([]);
  const [subCategoryName, setSubCategoryName] = useState<any>([]);


  const InvestmentGrowth = {
    title: {

      x: "left",
    },
    tooltip: {
      trigger: "item",

      backgroundColor: "rgba(0,0,0,0.7)",
      borderColor: "rgba(0,0,0,0)",

      textStyle: {
        color: "rgba(255, 255, 255, 1)",
      },
    },
    color: ["#b3daff", "#005CB3"],
    legend: {
      type: "plain",
      orient: "horizontal",

      top: 40,
      itemWidth: 14,
      icon: "roundRect",
      data: ["Principal", "Current Value"],
    },
    grid: {

      bottom: "10%",

    },
    xAxis: {
      data: monthGrowthChart,
      axisLine: {
        show: true,
      },
      splitLine: {
        show: true,
      },
    },
    yAxis: {
      axisLine: {
        show: true,
      },
      splitLine: {
        show: true,
      },
    },
    series: growthChartData,
  };

  const AssetAllocationMF = {
    title: {
      // text: 'Sales last week by Kits',
      // subtext: 'Sub Text',
      x: "left",
    },
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/>{b} : {c} ({d}%)",
    },
    legend: {
      orient: "horizontal",
      // left: 'left',
      top: 0,
      // bottom: "1%",
      //data: ["EQUITY", "DEBT", "HYBRID", "OTHERS"],
      data: legendNamesMF
    },
    color: ["#015cb3", "#fcd681", "#329cff", "#b3daff"],
    series: [
      {
        name: "Asset Allocation",
        type: "pie",
        radius: "55%",
        center: ["50%", "50%"],
        data: mfChartData,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)",
          },
        },
        label: {
          formatter: "{c}%",
          position: "inside",
        },
      },
    ],
  };
  const AssetAllocationOverall = {
    title: {
      // text: 'Sales last week by Kits',
      // subtext: 'Sub Text',
      x: "left",
    },
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/>{b} : {c} ({d}%)",
    },
    legend: {
      orient: "horizontal",
      top: 0,
      //data: ["MUTUAL FUND", "ULIP", "OTHER", "PROTECTION", "PMS", "LIQUILOAN"],
      data: legendNamesOA
    },
    color: ["#005CB3", "#fcd681", "#339CFF", "#b3daff", "#fbc040", "#D2D9EB"],
    series: [
      {
        name: "Asset Allocation",
        type: "pie",
        radius: "55%",
        center: ["50%", "50%"],
        data: overallChartData,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)",
          },
        },
        label: {
          formatter: "{c}%",
          position: "inside",
        },
      },
    ],
  };
  const MutualFund = {
    title: {
      // text: 'Sales last week by Kits',
      // subtext: 'Sub Text',
      x: "left",
    },
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/>{b} : {c} ({d}%)",
    },
    legend: {
      orient: "horizontal",
      // left: 'left',
      top: 0,
      // bottom: "1%",
      data: subCategoryName,
    },
    color: ["#015cb3", "#fcd681", "#329cff", "#b3daff"],
    series: [
      {
        name: "Asset Allocation",
        type: "pie",
        // radius: ["40%", "70%"],
        radius: ["40%", "70%"],
        center: ["50%", "50%"],
        data: subCategoryData,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)",
          },
        },
        label: {
          formatter: "{c}%",
          position: "inside",
        },
      },
    ],
  };

  const onChartClick = async (params: any) => {
    setShowSubCategory(params.data.name);

    try {

      if (user.basicid === "null" || user.basicid === undefined) {
        return;
      }
      const obj: any = {
        id: user?.basicid,
        type: params.data.name
      };

      const enc: any = encryptData(obj);

      const result: any = await assetAllocationsSubCategory(enc);

      let ar = Object.keys(result?.data)
      let ar1 = Object.entries(result?.data);

      const upper = ar.map(element => {
        return element.toUpperCase();
      });

      const valArray = ar1.map(ele => {
        return ({ "value": ele[1], "name": ele[0].toUpperCase() });
      })
      setSubCategoryData(valArray);
      setSubCategoryName(upper);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);

    }

  };

  const onEvents = {
    click: onChartClick,
  };
  const fetchGrowthChartData = async () => {
    try {
      if (!user?.basicid) return;
      setLoader(true);
      let obj = {
        'basicID': user?.basicid,
        'product_type': investmentActive,
        'PMS': pmsActive
      }
      const enc: any = encryptData(obj);

      const result: any = await growthChart(enc);
      setLoader(false);

      let month: any = [];
      let principal_val: any = [];
      let current_val: any = [];
      result?.data.map((ele: any) => {
        month.push((ele?.month).toUpperCase());
        principal_val.push(ele?.principal_val)
        current_val.push(ele?.current_val);
      })

      setMonthGrowthChart(month);
      let series: any = [];
      series.push({
        name: "Principal",
        type: "line",
        smooth: "true",
        symbol: "circle",
        symbolSize: 6,
        data: principal_val,
        lineStyle: {
          width: 3,
          color: "#b3daff",
        },
      })
      series.push({
        name: "Current Value",
        type: "line",
        smooth: "true",
        symbol: "circle",
        symbolSize: 6,
        data: current_val,
        lineStyle: {
          width: 3,
          color: "#005CB3",
        },
      })
      setGrowthChartData(series);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };
  useEffect(() => {
    if (investmentActive === "") return;
    fetchGrowthChartData();
    return () => {
      setGrowthChartData([]);
    }
  }, [investmentActive, pmsActive])

  return (
    <>
      <Text weight="normal" size="h3" color="goalBlue">
        Analytics
      </Text>
      {/* <Carousel
        infiniteLoop={true}
        dynamicHeight={true}
        showThumbs={false}
        showStatus={false}
        renderIndicator={(onClickHandler, isSelected, index, label) => {
          const defStyle = {
            marginLeft: 5,
            color: "white",
            cursor: "pointer",
            backgroundColor: "#F7B500",
            width: "100px",
            height: "5px",
            display: " inline-block",
          };
          const style = isSelected
            ? { ...defStyle, color: "red" }
            : { ...defStyle };
          return (
            <span
              style={style}
              onClick={onClickHandler}
              onKeyDown={onClickHandler}
              key={index}
              role="button"
            ></span>
          );
        }}
      > */}
      {/* Do Not Remove this Commented Code */}
      {/* <div>
          <Card css={{ p: 15, mb: 20, pb: 50 }}>
            <GroupBox position="apart" css={{ mb: 10 }}>
              <Box>
                <Text weight="extrabold" size="h4">
                  Investment Growth
                </Text>
              </Box>
            </GroupBox>
            <Box
              className="row row-cols-1 row-cols-md-2
             justify-content-center"
            >
              <Box className="col">
                <StyledTabs defaultValue="MF" onValueChange={(e) => {
                
                  setInvestmentActive(e);
                }}>
                  <TabsList
                    aria-label="Manage your account"
                    className="tabsCenterStyle"
                    css={{ mb: 5 }}
                  >
                    {InvestmentGrowthTabs.map((item, index) => {
                      return (
                        <TabsTrigger
                          value={item.value}
                          className="tabsCenter"
                          key={item.value}
                        >
                          <Text size="h6">{item.title}</Text>
                        </TabsTrigger>
                      );
                    })}
                  </TabsList>
                  <TabsContent value="MF">
                    {loading ?
                      <Loader />
                      :
                      <Box>
                        <ReactECharts
                          option={InvestmentGrowth}
                          style={{ height: 300 }}
                          opts={{ renderer: "svg" }}
                        />
                      </Box>}
                  </TabsContent>
                  <TabsContent value="ULIP">
                    {loading ?
                      <Loader />
                      :
                      <Box>
                        <ReactECharts
                          option={InvestmentGrowth}
                          style={{ height: 300 }}
                          opts={{ renderer: "svg" }}
                        />
                      </Box>
                    }
                  </TabsContent>
                  <TabsContent value="PMS">

                    <Box>
                      <RadioGroup
                        defaultValue={pmsActive}
                        className="inlineRadio"
                        onValueChange={(e: any) => {
                          setPMSActive(e);
                        }}
                      >
                        <Radio value="whiteoak" label="WhiteOak PMS" id="white" />
                        <Radio
                          value="motilal"
                          label="Motilal Oswal PMS"
                          id="motilal"
                        />
                        <Radio
                          value="kotak"
                          label="Kotak PMS"
                          id="kotak"
                        />
                      </RadioGroup>
                    </Box>
                    {loading ?
                      <Loader />
                      :
                      <Box>
                        <ReactECharts
                          option={InvestmentGrowth}
                          style={{ height: 300 }}
                          opts={{ renderer: "svg" }}
                        />
                      </Box>
                    }
                  </TabsContent>
                </StyledTabs>
              </Box>
            </Box>
          </Card>
        </div> */}
      {/* </Carousel> */}
      <div className="d-block">
        <Card css={{ p: 15, mb: 20, pb: 50 }}>
          <GroupBox position="apart" css={{ mb: 10 }}>
            <Box>
              <Text weight="extrabold" size="h4">
                Asset Allocation
              </Text>
            </Box>
          </GroupBox>
          {showSubCategory === "" ? (
            <StyledTabs defaultValue="MutualFund">
              <TabsList
                aria-label="Manage your account"
                className="tabsCenterStyle"
                css={{ mb: 20 }}
              >
                {assetAllocationTabs.map((item, index) => {
                  return (
                    <TabsTrigger
                      value={item.value}
                      className="tabsCenter"
                      key={item.value}
                    >
                      <Text size="h6">{item.title}</Text>
                    </TabsTrigger>
                  );
                })}
              </TabsList>
              <TabsContent value="MutualFund">
                {/* <Box>
                    <Text size="h6">click on pie to view sub-category</Text>
                  </Box> */}
                <Box>
                  <ReactECharts
                    option={AssetAllocationMF}
                    style={{ height: 300 }}
                    opts={{ renderer: "svg" }}
                  // onEvents={onEvents}
                  />
                  {mfChartData?.filter((i: any) => i.value === 0).length === mfChartData?.length ? <>
                    <Box className="row justify-content-center" >
                      <Text className="col-auto" size="h5" css={{ color: "var(--colors-blue1)" }}>No Details</Text>
                    </Box></> : <></>}
                </Box>
              </TabsContent>
              <TabsContent value="Overall">
                <Box>
                  <ReactECharts
                    option={AssetAllocationOverall}
                    style={{ height: 300 }}
                    opts={{ renderer: "svg" }}
                  />
                  {overallChartData?.filter((i: any) => i.value === 0).length === overallChartData?.length ? <>
                    <Box className="row justify-content-center" >
                      <Text className="col-auto" size="h5" css={{ color: "var(--colors-blue1)" }}>No Details</Text>
                    </Box></> : <></>}
                </Box>
              </TabsContent>
            </StyledTabs>
          ) : (
            <>
              <Text
                css={{ p: 5, mb: 10, pb: 10, cursor: "pointer" }}
                weight="bold"
                size="h7"
                color="yellow"
                decoration="underline"
                //@ts-ignore
                onClick={() => {
                  setShowSubCategory("");
                }}
              >
                Back to Category
              </Text>
              <Text
                css={{ p: 5, mb: 10, pb: 10 }}
                weight="extrabold"
                size="h4"
                color="goalBlue"
              >
                Mutual Fund - {showSubCategory}
              </Text>
              <Box>
                <ReactECharts
                  option={MutualFund}
                  style={{ height: 300 }}
                  opts={{ renderer: "svg" }}
                />
              </Box>
            </>
          )}
        </Card>
      </div>
    </>
  );
};

export default Analytics;
