import ReactEChartsCore from "echarts-for-react/lib/core";
import * as echarts from "echarts";

/***
 *
 * @Example https://echarts.apache.org/examples/en/editor.html?c=gauge-ring&lang=js&random=sqwbiszii3
 */

interface GaugeRingTypes {
  value: number,
  valueColor: string
}

const GaugeRing = ({ value, valueColor }: GaugeRingTypes) => {

  const option = {
    series: [
      {
        type: "gauge",
        data: [
          {
            value,
            detail: {
              valueAnimation: true,
              offsetCenter: ["0%", "0%"],
            },
          },
        ],
        startAngle: 90,
        endAngle: -270,
        pointer: {
          show: false,
        },
        progress: {
          show: true,
          overlap: false,
          roundCap: true,
          clip: false,
          itemStyle: value >= 0 && value <= 50 ? {
            color: '#dc3545',
          } : value > 50 && value <= 99 ? {
            color: '#fbc040',
          } : value >= 100 ? {
            color: '#32cd32',
          } : {
            color: "#000",
          }
        },
        axisLine: {
          lineStyle: {
            width: 4,
          },
        },
        splitLine: {
          show: false,
          distance: 0,
          length: 10,
        },
        axisTick: {
          show: false,
        },
        axisLabel: {
          show: false,
          distance: 50,
        },
        detail: {
          fontSize: 10,
          color: valueColor,
          formatter: "{value}%",
        },
      },
    ],
  };
  return (
    <ReactEChartsCore
      echarts={echarts}
      option={option}
      lazyUpdate={true}
      style={{
        height: 85,
        margin: -10,
      }}
    />
  );
};

export default GaugeRing;
