import React from 'react'
import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
//  import { Carousel } from '3d-react-carousal';
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import Badge from "@ui/Badge/Badge";
import PencilFill from "App/icons/PencilFill";
import MortorboardFill from "App/icons/MortorboardFill";
import GaugeRing from "App/modules/DashboardModule/components/MyGoals/GaugeRing";
import usePortfolioStore from "../../store";

const Goals = () => {
    let slides = [
        <img src="https://picsum.photos/800/300/?random" alt="1" />,
        <img src="https://picsum.photos/800/301/?random" alt="2" />,
        <img src="https://picsum.photos/800/302/?random" alt="3" />,
        <img src="https://picsum.photos/800/303/?random" alt="4" />,
        <img src="https://picsum.photos/800/304/?random" alt="5" />];
    return (
        <Card css={{ p: 25 }}>
            {/* <GroupBox position="apart" css={{ mb: 20 }}>
                <Box>
                    <Text color="primary" size="h4">
                        My Goals
                    </Text>
                </Box>
                <Box>

                    <Text weight="bold" size="h6" color="primary">
                        <a href="#"> View More </a>
                    </Text>
                </Box>
            </GroupBox> */}
            
            <Box className="gallery">
                <Box className="gallery-container">
                    {/* <Box className="gallery-item gallery-item-1 darkBlueColorBg" > */}

                    {/* <Carousel slides={slides} autoplay={false} interval={1000}/> */}
                        {/* <car2  slides={slides}/> */}
                        {/* <Box className="row">
                            <Box className="col-12 col-md-12 col-lg-12 mt-4">
                                <Text className="regularTextWhite" style="margin-left: 28px;"><b>Do you prefer a
                                    personalised plan created by our experts?</b></Text>

                                <Text className="regularTextWhite text-start txtwidth" style="margin-left: 28px;">We
                                    can create a comprehensive financial plan that covers all of yours goals and
                                    requirements, no matter how complex your financial status is.
                                    <br /><br />The plan can be tailormade for your individual needs and
                                    priorities.
                                </Text>

                                <Box className="row justify-content-center mt-5">
                                    <Box className="col-11 col-md-11 col-lg-11">
                                        <Box className="rounded-4 lightBg subNavName position-relative">
                                            <Box className=" position-absolute  bottom-50 end-0">
                                                 <img className="img-fluid imgalign" src="./Images/Adviser.png">
                                            </Box>
                                            <Box className="row">
                                                <Box className="col-11 col-md-8 col-lg-8 mt-3">
                                                    <Text style="margin-left: 15px;">Book your appointment with our
                                                        expert.</Text>
                                                </Box>
                                            </Box>
                                            <Box className="text-start col-11 col-md-11 col-lg-11 mt-2">
                                                <Text style="margin-left: 15px;">We will get in touch with you and
                                                    begin the journey to create
                                                    your personalised plan</Text>
                                            </Box>
                                            <Box className="text-end mt-1">
                                                <Button className="btn OtherBtn text-center larger py-1 my-2"
                                                    css="margin-right: 10px;">Connect</Button>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                        </Box> */}
                    {/* </Box> */}
                </Box>

                <Box className="gallery-controls"></Box>
            </Box>

        </Card>
    )
}

export default Goals