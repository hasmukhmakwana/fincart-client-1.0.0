import React, { useState } from 'react';
import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import GaugeRing from "App/modules/DashboardModule/components/MyGoals/GaugeRing";
import usePortfolioStore from "../../store";
import { WEB_URL } from "App/utils/constants";
import { useRouter, NextRouter } from "next/router";
import useGoalStore from "../../../GoalsModule/store";
import EditGoal from "../../../GoalsModule/components/EditGoal";
import DeleteGoal from "../../../GoalsModule/components/DeleteGoal";
import {
  fetchAllGoalData,
  getGoalNameCodeList
} from "App/api/goalPlan";
import DeleteIcon from 'App/icons/DeleteIcon';
import Coins from 'App/icons/Coins';
import EditIcon from 'App/icons/EditIcon';
import DialogModal from '@ui/ModalDialog/ModalDialog';
import { toastAlert } from 'App/shared/components/Layout';
import Loader from 'App/shared/components/Loader';
import GoalPortfolioSingle from 'pages/GoalPortfolioSingle';
import { setLS } from '@ui/DataGrid/utils';

const MyGoals = ({ reload }: any) => {
  const router: NextRouter = useRouter();
  const {
    open, setOpen,
    openEdit, setOpenEdit,
    setGoalNameCodeList,
    openDelete, setOpenDelete
  } = useGoalStore();

  const [loader, setLoader] = useState<boolean>(false);
  const [allGoalData, setAllGoalData] = useState<any>([]);
  const [selectedGoal, setSelectedGoal] = useState<any>([]);
  const [goalId, setGoalId] = useState<string>("");
  const { goalLoader, goalWisePortfolioData, setSelectedGoalWisePortfolioData, basicId } =
    usePortfolioStore();

  const [infoData, setinfoData] = useState<any>([
    {
      goalCode: "FG1",
      icon: 'motorcycle.png'
    },
    {
      goalCode: "FG2",
      icon: 'FontAwsome (chart-line).png'
    },
    {
      goalCode: "FG3",
      icon: 'FontAwsome (car).png'
    },
    {
      goalCode: "FG4",
      icon: 'FontAwsome (home).png'
    },
    {
      goalCode: "FG5",
      icon: 'FontAwsome (book-reader).png'
    },
    {
      goalCode: "FG6",
      icon: 'FontAwsome (umbrella-beach).png'
    },
    {
      goalCode: "FG7",
      icon: 'NPS.png'
    },
    {
      goalCode: "FG8",
      icon: 'FontAwsome (coins).png'
    },
    {
      goalCode: "FG9",
      icon: 'FontAwsome (wheelchair).png'
    },
    {
      goalCode: "FG10",
      icon: 'FontAwsome (umbrella-beach).png'
    },
    {
      goalCode: "FG11",
      icon: 'ChildSecurity.png'
    },
    {
      goalCode: "FG12",
      icon: 'FontAwsome (book-reader).png'
    },
    {
      goalCode: "FG13",
      icon: 'NPS.png'
    },
    {
      goalCode: "FG14",
      icon: 'Other.png'
    },
    {
      goalCode: "FG17",
      icon: 'EmergencyFund.png'
    },
    {
      goalCode: "FG23",
      icon: 'BankMandates.png'
    },
    {
      goalCode: "FG24",
      icon: 'insurance.png'
    },
  ]);

  const fetchGoalData = async () => {
    try {


      setLoader(true);


      const result: any = await fetchAllGoalData();
      setLoader(false);

      setAllGoalData(result?.data);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };
  const fetchGoalNameCodeList = async () => {
    try {
      setLoader(true);
      const result: any = await getGoalNameCodeList();
      setLoader(false);

      setGoalNameCodeList(result?.data);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };


  return (
    <Card css={{ p: 25 }}>
      {/* @ts-ignore */}
      <GroupBox position="apart" css={{ mb: 20 }}>
        <Box>
          {/* @ts-ignore */}
          <Text color="primary" size="h4">
            My Goals
          </Text>
        </Box>
        <Box>
          {/* @ts-ignore */}
          <Text weight="bold" size="h6" color="primary">
            {/* <a href={router.push("/Goals/MyGoals")}> View More </a> */}
            <Button className="rounded-0 p-0" style={{ backgroundColor: "#FFFFFF", border: "none", color: "#005cb3", fontSize: "0.8rem", borderBottom: "solid 1px #fbc040", pb: "0" }} onClick={() => { router.push("/Goals/MyGoals") }}> View More </Button>
          </Text>
        </Box>
      </GroupBox>
      {goalLoader ? <><Box className="row justify-content-center"><Box className="col-auto"><Loader /></Box></Box></> : <>
        {goalWisePortfolioData.map(record => {
          let arr = (infoData.filter((ele: any) => ele?.goalCode == record?.goalCode))
          return (
            <Box className="row goalBox">
              <Box className="col-auto">
                <img
                  width={20}
                  height={15}
                  src={`${WEB_URL}/${arr[0]?.icon}`}
                  alt={`${arr[0]?.icon}`}
                />
              </Box>
              <Box className="col">
                <Box className="row">
                  <Box className="col col-12 mb-3 mb-md-0">
                    <Box>
                      {/* @ts-ignore */}
                      <Text color="primary">{record?.GName}</Text>
                      <Box></Box>
                    </Box>
                  </Box>
                  <Box className="col mb-3 mb-md-0">
                    <Box>
                      {/* @ts-ignore */}
                      <Text color="primary" size="h6">
                        Investment Amount :
                      </Text>
                      {/* @ts-ignore */}
                      <Text weight="semibold">
                        &#x20B9; {Number(record?.InvestedAmount).toLocaleString("en-IN")}
                      </Text>
                      <Box></Box>
                    </Box>
                  </Box>
                  <Box className="col col-6 col-md-4 text-md-center mb-3 mb-md-0">
                    {/* @ts-ignore */}
                    <Text color="primary" size="h6">Current Value:</Text>
                    <Text>&#x20B9; {Number(record?.currWorth).toLocaleString("en-IN")}</Text>
                  </Box>
                  <Box className="col col-6 col-md-4 text-md-center mb-3 mb-md-0">
                    <Box className="text-center" display="inlineBlock">
                      <GroupBox css={{ position: "relative" }}>
                        {/* @ts-ignore */}
                        <Text size="h6" color="primary">Target Amount</Text>
                        {/* <a>
                        <Badge
                          css={{
                            right: -5,
                            top: 10,
                            pt: 4,
                            pr: 4,
                            pl: 4,
                            pb: 4,
                          }}
                          position={"topRight"}
                          type={"dot"}
                          color={"primary"}
                          rounded
                        >
                          <PencilFill color="white"></PencilFill>
                        </Badge>
                      </a> */}
                      </GroupBox>
                      <Text>&#x20B9; {Number(record?.Goal_MaturityAmt).toLocaleString("en-IN")}</Text>
                    </Box>
                  </Box>
                </Box>
              </Box>
              <Box className="col col-12 col-md-1 text-center align-self-center mb-3 mb-md-0">
                <GaugeRing value={Math.round(parseFloat(record?.currWorth) / parseFloat(record?.Goal_MaturityAmt) * 100)} valueColor="#379bfd" />
              </Box>
              <Box className="col col-12 col-md-2 text-center mt-3 my-md-auto">
                <Box className="row justify-content-center mt-3 mb-1">

                  {record?.InvestedAmount > "0" ?
                    <Button
                      color="yellow"
                      size="md"
                      className="col-auto d-inline-flex p-2 mb-2 mx-1"
                      onClick={() => {
                        //add items to localStorage
                        //set goalCode
                        setSelectedGoalWisePortfolioData(record);

                        router.push("/GoalPortfolioSingle")
                      }}
                    >
                      <Coins size="15" className="me-2" />
                      {/* @ts-ignore */}
                      <Text size="h6"
                        //@ts-ignore
                        color="gray8"
                      // className={style.button}
                      //setGoalId(record.userGoalId);
                      // setOpenTaxSavingInvest(true);
                      >
                        Invest More
                      </Text>
                    </Button> :
                    <>
                      <Button
                        className="col-auto mb-2 mx-1"
                        size="md"
                        color="yellow"
                        title="Invest"
                        css={{
                          //backgroundColor: "transparent",
                          textAlign: "center",
                          cursor: "pointer",
                        }}
                        // disabled={currentScheme?.isSIPAllow === "Y" ? false : true}
                        onClick={() => {
                          //add items to localStorage
                          //set goalCode
                          localStorage.setItem("userGoalId", record?.UserGoalId)
                          //set section
                          localStorage.setItem("section", "GOAL_PLANNING")
                          //set amount
                          localStorage.setItem("amount", record?.investAmount)
                          //set duration
                          localStorage.setItem("duration", record?.duration)
                          router.push("/RecommendedSchemes")
                        }}
                      >
                        <Coins />
                      </Button>
                      <Button
                        className="col-auto mb-2 mx-1"
                        size="md"
                        color="yellow"
                        title="Edit"
                        css={{
                          // backgroundColor: "transparent",
                          textAlign: "center",
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          setSelectedGoal(record);
                          setOpenEdit(true);
                        }}
                      >
                        <EditIcon />
                      </Button>
                      <Button
                        className="col-auto mb-2 mx-1"
                        size="md"
                        color="yellow"
                        title="Delete"
                        css={{
                          //backgroundColor: "transparent",
                          textAlign: "center",
                          cursor: "pointer",
                        }}
                        // disabled={currentScheme?.isSIPAllow === "Y" ? false : true}
                        onClick={() => {
                          setGoalId(record?.UserGoalId);
                          setOpenDelete(true);
                        }}
                      >
                        <DeleteIcon />
                      </Button>
                    </>
                  }
                </Box>
              </Box>
            </Box>
          )
        })}
      </>}
      <DialogModal
        open={openEdit}
        setOpen={setOpenEdit}
        css={{
          padding: 0,
          "@bp0": { width: "100%" },
          "@bp1": { width: "40%" },
        }}
      >
        <EditGoal selectedGoal={selectedGoal} fetchGoal={fetchGoalData} fetchGoalNameCodeList={fetchGoalNameCodeList} reload={reload} />
      </DialogModal>
      <DialogModal
        open={openDelete}
        setOpen={setOpenDelete}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        {/* @ts-ignore */}
        <DeleteGoal IdPass={goalId} fetchGoalData={fetchGoalData} fetchGoalNameCodeList={fetchGoalNameCodeList} reload={reload} />

      </DialogModal>
    </Card>
  );
};

export default MyGoals;
