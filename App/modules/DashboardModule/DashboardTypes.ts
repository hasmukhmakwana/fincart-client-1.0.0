import { NumberLocale } from "yup/lib/locale";

export type OverallPortfolioTypes = {
  net_investment: string;
  current_value: string;
  netgain: string;
  xirr: string
};

export type PortfolioTypes = {
  current_value: string;
  netgain: string;
  investment_type_name: string;
  investment_type_value: string;
  invested_value: string;
  Xirr: string;
  Sum_assured: string;
};

export type ProductPortfolioTypes =
  {
    id: string;
    investment: string;
    current_value: string;
    netgain: string;
    xirr: string;
    switch_in: string;
    switch_out: string;
    divPaid_interest: string;
    investment_type_name: string;
    partner_name: string;
    curr_goal_invested_amt: string;
    curr_goal_invested_per: string;
    curr_goal_current_amt: string;
    curr_goal_gain: string;
    partner_id: string;
    isShow: boolean;
    details: string;
    divGain: string;
    mfGain: string;
    current_nav: string;
    units: string;
  };

export type UpcomingTransactionType = {
  SchemeName: string;
  trxnDate: Date;
  Amount: string;
  Trans_Type: string;
  frequency: string;
  status: string;
};
export type RecentTransactionType = {
  SchemeName: string;
  trxnDate: Date;
  Amount: string;
  Trans_Type: string;
  assettype: string;
  fund_img: string;
};
export type MaturingTransactionType = {
  SchemeName: string;
  trxnDate: Date;
  amount: string;
  TakeAction: string;
};

export type MandateTransactionType = {
  MandateID: string;
  amount: string;
  valid_till: string;
  status: string;
  assetType: string;
  productType: string;
  ClientName: string;
  BankName: string;
};

export type GoalWisePortfolioType = {
  UserId: string;
  goalCode: string;
  investAmount: string;
  typeCode: string;
  duration: string;
  UserGoalId: string;
  GName: string;
  InvestedAmount: string;
  currWorth: string;
  gain: string;
  goalPriority: number;
  goalAchived: number;
  GoalEnd_Date: number;
  Goal_MaturityAmt: string;
  last_Updated_date: Date;
};

export type FormTypes = "List" | "Info" | "Download" | "Edit" | "Delete";
