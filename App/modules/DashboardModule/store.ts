import { MANDATE_TYPES } from "App/utils/constants";
import { List } from "echarts";
import create from "zustand";
import {
  OverallPortfolioTypes, PortfolioTypes, ProductPortfolioTypes, UpcomingTransactionType, MaturingTransactionType,
  GoalWisePortfolioType, MandateTransactionType, RecentTransactionType
} from "./DashboardTypes";

interface StoreTypes {
  loading: boolean;
  isPortfolioScreen: boolean;
  goalLoader: boolean;
  isFilter: boolean;
  Gain_percentage: number;
  last_Updated_date: string;
  overallPortfolioData: OverallPortfolioTypes;
  portfolioData: PortfolioTypes[];
  mutualFundPortfolioData: ProductPortfolioTypes[];
  insurancePortfolioData: ProductPortfolioTypes[];
  epfPortfolioData: ProductPortfolioTypes[];
  pmsPortfolioData: ProductPortfolioTypes[];
  liquiloanPortfolioData: ProductPortfolioTypes[];
  ulipPortfolioData: ProductPortfolioTypes[];
  digitalGoldPortfolioData: ProductPortfolioTypes[];
  protectionPortfolioData: ProductPortfolioTypes[];
  upcomingTransactions: UpcomingTransactionType[];
  recentTransactions: RecentTransactionType[];
  maturingTransactions: any;
  maturingDataArray: any;
  mandateTransactions: MandateTransactionType[];
  goalWisePortfolioData: GoalWisePortfolioType[];
  selectedGoalWisePortfolioData: GoalWisePortfolioType;
  buttonTextList: any;
  mfChartData: any;
  overallChartData: any;
  growthChartData: any;
  monthGrowthChart: any;
  basicId: string;
  legendNamesOA: string[];
  legendNamesMF: string[];
  setOverallPortfolioData: (payload: OverallPortfolioTypes) => void;
  setPortfolioData: (payload: PortfolioTypes[]) => void;
  setLoader: (payload: boolean) => void;
  setPortfolioScreen: (payload: boolean) => void;
  setIsFilter: (payload: boolean) => void;
  setGoalLoader: (payload: boolean) => void;
  setMutualFundPortfolioData: (payload: ProductPortfolioTypes[]) => void;
  setInsurancePortfolioData: (payload: ProductPortfolioTypes[]) => void;
  setEpfPortfolioData: (payload: ProductPortfolioTypes[]) => void;
  setPmsPortfolioData: (payload: ProductPortfolioTypes[]) => void;
  setLiquiloanPortfolioData: (payload: ProductPortfolioTypes[]) => void;
  setUlipPortfolioData: (payload: ProductPortfolioTypes[]) => void;
  setDigitalGoldPortfolioData: (payload: ProductPortfolioTypes[]) => void;
  setProtectionPortfolioData: (payload: ProductPortfolioTypes[]) => void;
  setUpcomingTransactions: (payload: UpcomingTransactionType[]) => void;
  setRecentTransactions: (payload: RecentTransactionType[]) => void;
  setMaturingTransactions: (payload: any) => void;
  setMaturingDataArray: (payload: any) => void;
  setMandateTransactions: (payload: MandateTransactionType[]) => void;
  setGoalWisePortfolioData: (payload: GoalWisePortfolioType[]) => void;
  setSelectedGoalWisePortfolioData: (payload: GoalWisePortfolioType) => void;
  setButtonTextList: (payload: any) => void;
  setMfChartData: (payload: any) => void;
  setOverallChartData: (payload: any) => void;
  setGrowthChartData: (payload: any) => void;
  setMonthGrowthChart: (payload: any) => void;
  setGain_percentage: (payload: number) => void;
  setBasicId: (payload: string) => void;
  setLastUpdatedDate: (payload: string) => void;
  setLegendNamesOA: (payload: any) => void;
  setLegendNamesMF: (payload: any) => void;
}

const today = new Date();

const usePortfolioStore = create<StoreTypes>((set) => ({
  //* initial state
  overallPortfolioData: {
    net_investment: "",
    current_value: "",
    netgain: "",
    xirr: ""
  },
  last_Updated_date: "",
  Gain_percentage: 0,
  portfolioData: [],

  mutualFundPortfolioData: [],
  insurancePortfolioData: [],
  epfPortfolioData: [],
  pmsPortfolioData: [],
  liquiloanPortfolioData: [],
  ulipPortfolioData: [],
  digitalGoldPortfolioData: [],
  protectionPortfolioData: [],
  upcomingTransactions: [],
  recentTransactions: [],
  maturingTransactions: [],
  maturingDataArray: [],
  mandateTransactions: [],
  goalWisePortfolioData: [],
  buttonTextList: {},
  mfChartData: [],
  growthChartData: [],
  monthGrowthChart: [],
  overallChartData: [],
  legendNamesMF: [],
  legendNamesOA: [],
  loading: false,
  goalLoader: false,
  isFilter: false,
  basicId: "",
  isPortfolioScreen: true,
  selectedGoalWisePortfolioData: {
    UserId: "",
    goalCode: "",
    investAmount: "",
    typeCode: "",
    duration: "",
    UserGoalId: "",
    GName: "",
    InvestedAmount: "",
    currWorth: "",
    gain: "",
    goalPriority: 0,
    goalAchived: 0,
    GoalEnd_Date: 0,
    Goal_MaturityAmt: "",
    last_Updated_date: new Date(),
  },
  //* methods for manipulating state

  setPortfolioScreen: (payload: any) =>
    set((state) => ({
      ...state,
      isPortfolioScreen: payload,
    })),

  setOverallPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      overallPortfolioData: payload,
    })),

  setGain_percentage: (payload: any) =>
    set((state) => ({
      ...state,
      Gain_percentage: payload,
    })),

  setLastUpdatedDate: (payload: any) =>
    set((state) => ({
      ...state,
      last_Updated_date: payload
    })),

  setPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      portfolioData: payload || [],
    })),

  setLoader: (payload: any) =>
    set((state) => ({
      ...state,
      loading: payload,
    })),

  setIsFilter: (payload: any) =>
    set((state) => ({
      ...state,
      isFilter: payload,
    })),

  setGoalLoader: (payload: any) =>
    set((state) => ({
      ...state,
      goalLoader: payload,
    })),

  setMutualFundPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      mutualFundPortfolioData: payload || [],
    })),
  setInsurancePortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      insurancePortfolioData: payload || [],
    })),
  setEpfPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      epfPortfolioData: payload || [],
    })),
  setPmsPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      pmsPortfolioData: payload || [],
    })),
  setLiquiloanPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      liquiloanPortfolioData: payload || [],
    })),
  setUlipPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      ulipPortfolioData: payload || [],
    })),
  setDigitalGoldPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      digitalGoldPortfolioData: payload || [],
    })),
  setProtectionPortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      protectionPortfolioData: payload || [],
    })),
  setUpcomingTransactions: (payload: any) =>
    set((state) => ({
      ...state,
      upcomingTransactions: payload || [],
    })),
  setRecentTransactions: (payload: any) =>
    set((state) => ({
      ...state,
      recentTransactions: payload || [],
    })),
  setMaturingTransactions: (payload: any) =>
    set((state) => ({
      ...state,
      maturingTransactions: payload || [],
    })),
  setMaturingDataArray: (payload: any) =>
    set((state) => ({
      ...state,
      maturingDataArray: payload || [],
    })),
  setMandateTransactions: (payload: any) =>
    set((state) => ({
      ...state,
      mandateTransactions: payload || [],
    })),
  setGoalWisePortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      goalWisePortfolioData: payload || [],
    })),
  setSelectedGoalWisePortfolioData: (payload: any) =>
    set((state) => ({
      ...state,
      selectedGoalWisePortfolioData: payload,
    })),
  setButtonTextList: (payload: any) =>
    set((state) => ({
      ...state,
      buttonTextList: payload,
    })),
  setMfChartData: (payload: any) =>
    set((state) => ({
      ...state,
      mfChartData: payload || [],
    })),
  setOverallChartData: (payload: any) =>
    set((state) => ({
      ...state,
      overallChartData: payload || [],
    })),
  setGrowthChartData: (payload: any) =>
    set((state) => ({
      ...state,
      growthChartData: payload || [],
    })),
  setMonthGrowthChart: (payload: any) =>
    set((state) => ({
      ...state,
      monthGrowthChart: payload || [],
    })),
  setBasicId: (payload: any) =>
    set((state) => ({
      ...state,
      basicId: payload || "",
    })),
  setLegendNamesMF: (payload: any) =>
    set((state) => ({
      ...state,
      legendNamesMF: payload
    })),
  setLegendNamesOA: (payload: any) =>
    set((state) => ({
      ...state,
      legendNamesOA: payload
    })),
}));


export default usePortfolioStore;
