import { RadioGroup } from "@radix-ui/react-radio-group";
import { TabsContent, TabsList, TabsTrigger } from "@radix-ui/react-tabs";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import Checkbox from "@ui/Checkbox/Checkbox";
import { GroupBox } from "@ui/Group/Group.styles";
import Input from "@ui/Input";
import Radio from "@ui/Radio/Radio";
import SelectMenu from "@ui/Select/Select";
import { StyledTabs } from "@ui/Tabs/Tabs.styles";
import style from "../SwitchTransaction.module.scss";
import Text from "@ui/Text/Text";
import X from "App/icons/X";
import React from "react";
import ArrowDownUp from "App/icons/ArrowDownUp";
const SwitchTransactionPage = () => {
  return (
    <Box background="gray">
      <Box className="container-fluid">
        <Box className="row">
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Card>
            <Box>
              <Box className="row">
                  <GroupBox borderBottom css={{pt:20, pb:20, pr:20, pl:35}}>
                <Box className="col-auto">
                  <Text weight="extrabold" size="h3">
                    Category
                  </Text>
                </Box>
                <Box className="col-2">
                  <Box>
                    <SelectMenu
                      items={[
                        { id: "1", name: "thiruvananthapuram" },
                        { id: "2", name: "Surat" },
                      ]}
                      bindValue={"id"}
                      bindName={"name"}
                      placeholder={"All"}
                      onChange={()=>{}}
                    />
                  </Box>
                </Box>
                </GroupBox>
              </Box>
              </Box>

              <Box css={{ p: 20 }}>
                <Box>
                  <Text weight="bold">
                    ADITYA BIRLA SUN LIFE LIQUID FUND-DIRECT PLAN
                  </Text>
                </Box>
                <Box>
                  <Box className="row" position="middle">
                    <Box className="col-auto" alignSelt="center">
                      <Checkbox label="" id="Debt Funds" />
                    </Box>
                    <Box className="col-auto">
                      {/* <Text color="gray" size="h6">
                      Folio
                    </Text>
                    <Text>1010001010</Text> */}
                      <Input
                        label="Folio"
                        name="firstName"
                        placeholder="1010001010"
                        disabled
                        color="none"
                      />
                    </Box>
                    <Box className="col-auto">
                      <Input
                        label="Amount"
                        name="firstName"
                        placeholder="Amount"
                      />
                    </Box>
                    <Box className="col-auto">
                      <Input
                        label="Units"
                        name="firstName"
                        placeholder="Units"
                      />
                    </Box>
                    <Box className="col-auto">
                      <SelectMenu
                        items={[
                          { id: "1", name: "Baroda" },
                          { id: "2", name: "Surat" },
                        ]}
                        bindValue={"id"}
                        bindName={"Goal"}
                        label={"Goal"}
                        placeholder={"No Goal"}
                        onChange={()=>{}}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box>
                  {/* <Box className={style.test}><div className={style.switchBox}>THIS IS A TEST</div></Box> */}
                  <Box className="switchDivider dividerLine orangeLine">
                    <Box className={style.switchBox} position="middle">
                      <GroupBox align="center">
                        <ArrowDownUp />
                        <Text weight="extrabold">Switch to</Text>
                      </GroupBox>
                    </Box>
                  </Box>
                </Box>
                <Box>
                  <Text weight="bold">
                    ADITYA BIRLA SUN LIFE LIQUID FUND-GROWTH-DIRECT PLAN
                  </Text>
                </Box>
                <Box>
                  <Box className="row" position="middle">
                    <Box className="col-2">
                      <SelectMenu
                        items={[
                          { id: "1", name: "Baroda" },
                          { id: "2", name: "Surat" },
                        ]}
                        bindValue={"id"}
                        bindName={"name"}
                        label={"Switch By"}
                        placeholder={"All Unit"}
                        onChange={()=>{}}
                      />
                    </Box>

                    <Box className="col-2">
                      <Input
                        label="Amount/Units"
                        name="firstName"
                        placeholder="0"
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Card>
            <RadioGroup defaultValue="1M" className="inlineRadio">
              <Radio
                value="1M"
                label="I/We hereby confirm that this is transaction done purely at my/our sole discretion"
                id="1M"
              />
            </RadioGroup>
            <Box className="text-end">
              <Button color="yellow">Confirm</Button>
            </Box>
          </Box>
          <Box className="col"></Box>
        </Box>
      </Box>
    </Box>
  );
};
export default SwitchTransactionPage;
