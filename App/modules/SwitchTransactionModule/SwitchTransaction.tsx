import Holder from 'App/shared/components/Holder/Holder'
import Layout from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import React from 'react'
import SwitchTransactionPage from './components/SwitchTransactionPage'

const SwitchTransactionModule = () => {
  return (
    <Layout>
    <PageHeader title='Switch' rightContent={<Holder/>} />
  <SwitchTransactionPage/>
  </Layout>
  )
}

export default SwitchTransactionModule