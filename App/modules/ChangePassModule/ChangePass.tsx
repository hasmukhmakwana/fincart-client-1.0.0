import React from "react";
import Layout from 'App/shared/components/Layout'
import ChangePassPage from "./components/ChangePassPage";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import { Button } from "@ui/Button/Button";
import style from "./ChangePass.module.scss";
const ChangePass = () => {
    return (
      <Layout>
        <PageHeader title="Change Password" rightContent={<></>} />
        <ChangePassPage/>
      </Layout>
    )
  }
  
  export default ChangePass;