export type SendOTPTypes = {
  Email: string;
  Password: string;
};

export type OTPFormTypes = {
  OTP: string;
};
