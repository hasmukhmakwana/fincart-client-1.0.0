import React, { useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import Basic from "./Basic";
import OTP from "./OTP";
import {
  forgotPassword,
  validate_FP_OTP,
} from "App/api/login";

import CheckCircleFill from "App/icons/CheckCircleFill";
import { SendOTPTypes, OTPFormTypes } from "../changePassTypes"
import { encryptData, getUser } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";


const ChangePassPage = () => {
  const user: any = getUser();
  const [showOTP, setShowOTP] = useState(false);
  const [showThankyou, setShowThankyou] = useState(false);
  const [cpassType, setCPassType] = useState("password");
  const [npassType, setNPassType] = useState("password");
  const [ncpassType, setCNPassType] = useState("password");
  const [basicDetails, setbasicDetails] = useState<SendOTPTypes>({
    Email: user?.userid,
    Password: ""
  });
  let val: number;
  const togglePassword = (val: number) => {
    switch (val) {
      case 1:
        if (cpassType === "password") {
          setCPassType("text")
          return;
        }
        setCPassType("password")
        break;
      case 2:
        if (npassType === "password") {
          setNPassType("text")
          return;
        }
        setNPassType("password")
        break;
      case 3:
        if (ncpassType === "password") {
          setCNPassType("text")
          return;
        }
        setCNPassType("password")
        break;
    }
  }

  /** @handleSendOTP investor account creation */
  const handleSendOTP = async (formValues: SendOTPTypes) => {
    try {
      console.log("handleSendOTP: ", formValues);
      setbasicDetails(formValues);
      const encryptedData: any = encryptData(formValues?.Email, true);
      let result: any = await forgotPassword(encryptedData);
      console.log("api response --- ", result);
      toastAlert("success", "OTP has been sent");
      setShowOTP(true);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  /** @handleVerifyOTP email and mobile OTP */
  const handleVerifyOTP = async (formValues: OTPFormTypes) => {
    try {
      console.log("handleVerifyOTP: ", formValues);
      const obj: any = {
        otp: formValues?.OTP,
        userid: basicDetails?.Email,
        password: basicDetails?.Password,
      };

      const encryptedData: any = encryptData(obj);
      let result: any = await validate_FP_OTP(encryptedData);
      console.log("result --- ", result);
      setShowThankyou(true);
      /**
       * @ACCOUNT_CREATION
       *
       */
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  return (
    <>
      <Box background="gray">
        <Box className="container-fluid">
          <Box className="row justify-content-center">
            <Box className="col-12 col-md-6 col-sm-12 col-lg-4">
              <Card>
                {showOTP === false ?
                  <Basic
                    handleSendOTP={handleSendOTP}
                    basicDetails={basicDetails}
                  />
                  :
                  showOTP === true && showThankyou === false ?
                    <OTP handleVerifyOTP={handleVerifyOTP} setShowOTP={setShowOTP} />
                    :
                    <Box align="center" className="row justify-content-center" style={{ border: "1px solid black", borderRadius: "15px", margin: "1rem" }}>
                      <Box className="row justify-content-center" style={{ padding: "1rem", paddingTop: "2rem" }}>
                        <CheckCircleFill className="iconTick" size="30" color={"#FAAC05"}></CheckCircleFill>
                      </Box>
                      <Box className="row justify-content-center" style={{ padding: "0.5rem" }}>
                        {/* @ts-ignore */}
                        <Text color="primary" size="h4" css={{ textAlign: "center" }}>
                          Password Updated !
                        </Text>
                      </Box>
                      <Box className="row justify-content-center" style={{ padding: "0.5rem", paddingBottom: "2rem" }}>
                        {/* @ts-ignore */}
                        <Text color="default" css={{ textAlign: "center" }}>
                          Your Password has been changed successfully.
                        </Text>
                      </Box>
                    </Box>
                }
              </Card>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ChangePassPage;