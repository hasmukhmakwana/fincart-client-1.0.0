import React, { useState } from "react";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import style from "../ChangePass.module.scss";
import Input from "@ui/Input";
import AlertDialog from "@ui/AlertDialog/ModalDialog";
import PasswordUpdatedModel from "./PasswordUpdatedModel";
const PassOTPPage = () => {
    const [openDialog, setOpenDialog] = useState(false);
    return (
        <>
            <Box className="row justify-content-center">
                <Box className="col-12 col-md-6 col-sm-12 col-lg-4">

                    <Box>
                        <>
                            <Box className="col" style={{ position: "relative" }}>
                                <Input

                                    label="Enter OTP"
                                    required
                                    //onChange={handleChange}
                                    // value={values.password}
                                    name="OTP"
                                    placeholder="12345"
                                    className="border "
                                    css={{ fontWeight: "normal" }}

                                />
                            </Box>
                        </>
                        <Box className="row justify-content-end">
                            <Box className="col-auto" >
                                <Button
                                    className={`col-auto  mx-0  px-2 ${style.button}`}
                                    type="submit"
                                    color="yellowGroup"
                                //color="gray8"
                                // onClick={handleSubmit}
                                // loading={loading}
                                >
                                    Cancel
                                </Button>&nbsp;
                                <Button
                                    className={`col-auto  mx-0  px-2 ${style.button}`}
                                    type="submit"
                                    color="yellowGroup"
                                    onclick={setOpenDialog(true)}
                                // onClick={handleSubmit}
                                // loading={loading}
                                >
                                    Confirm
                                </Button>
                            </Box>
                        </Box>

                    </Box>

                </Box>
            </Box>
            <AlertDialog
                open={openDialog}
                setOpen={setOpenDialog}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "40%" },
                }}
            >
                <PasswordUpdatedModel />
            </AlertDialog>
        </>
    );
};

export default PassOTPPage;
