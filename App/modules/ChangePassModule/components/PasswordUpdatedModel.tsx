import React from 'react'
import Box from "@ui/Box/Box";
import Card from '@ui/Card';
import CheckCircleFill from 'App/icons/CheckCircleFill';
import Input from '@ui/Input';
const PasswordUpdatedModel = () => {
    return (
        <Box className="row justify-content-center">
            <Box className="col-12 col-md-6 col-sm-12 col-lg-4">
                <Card>
                    <Box className="row  justify-content-center">
                        <Box className="col-auto">
                            <CheckCircleFill />
                        </Box>
                    </Box>
                    <Box className="row  justify-content-center">
                        <Box className="col-auto">
                            <Input label="Password Updated !" ></Input>
                        </Box>
                    </Box>
                    <Box className="row  justify-content-center">
                        <Box className="col-auto">
                            <Input label="Your password has been changed scucessfully" ></Input>
                        </Box>
                    </Box>
                </Card>
            </Box>
        </Box>

    )
}

export default PasswordUpdatedModel