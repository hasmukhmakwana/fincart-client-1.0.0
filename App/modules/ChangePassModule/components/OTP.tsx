import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { OTPFormTypes } from "../changePassTypes";
import { Formik } from "formik";
import * as Yup from "yup";
import { useEffect, useMemo, useState } from "react";

const validationSchema = Yup.object().shape({
  OTP: Yup.string().required("OTP is required"),
});

type OTPTypes = {
  handleVerifyOTP: (value: OTPFormTypes) => void;
  setShowOTP: (value: boolean) => void;
};

// main
const OTP = ({ handleVerifyOTP, setShowOTP }: OTPTypes) => {
  const [formValues, setformValues] = useState<OTPFormTypes>({
    OTP: ""
  });



  return (
    <>
      {/* @ts-ignore */}
      <Formik
        initialValues={formValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleVerifyOTP(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount
        }) => (
          <Box>
            <>
              <Box className="col" style={{ position: "relative" }}>
                <Input
                  type="number"
                  label="Enter OTP"
                  value={values?.OTP}
                  placeholder="OTP"
                  onChange={handleChange}
                  required
                  error={submitCount ? errors.OTP : null}
                  name="OTP"
                  className="border"
                  css={{ fontSize: "0.75rem" }}
                />
              </Box>
            </>
            <Box className="row justify-content-end">
              <Box className="col-auto" >
                <Button
                  type="submit"
                  color="yellow"
                  onClick={() => { setShowOTP(false) }}
                // loading={loading}
                >
                  Cancel
                </Button>
              </Box>
              <Box className="col-auto">
                <Button
                  type="submit"
                  color="yellow"
                  onClick={handleSubmit}
                >
                  Confirm
                </Button>
              </Box>
            </Box>
          </Box>
        )}
      </Formik>
    </>
  );
};
export default OTP;
