import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { SendOTPTypes } from "../changePassTypes";
import { Formik } from "formik";
import * as Yup from "yup";
import { useEffect, useMemo, useState } from "react";
import EyeIcon from "App/icons/EyeIcon";
import EyeSlash from "App/icons/EyeSlash";

const validationSchema = Yup.object().shape({
  Password: Yup.string().required("Password is required").min(6, "Password is too short - should be 6 chars minimum.")
    .max(15, "Password is too long - should be 15 chars maximum."),
  Email: Yup.string().required("Email is required").email("Invalid Email"),
});

type BasicTypes = {
  handleSendOTP: (values: SendOTPTypes) => void;
  basicDetails: SendOTPTypes;
};

// main
const Basic = ({ handleSendOTP, basicDetails }: BasicTypes) => {

  const [npassType, setNPassType] = useState("password");
  const [formValues, setformValues] = useState<SendOTPTypes>(basicDetails);
  const [btnState, setBtnState] = useState(false);
  // const [timer, setTimer] = useState();
  const togglePassword = (val: number) => {
    switch (val) {
      case 2:
        if (npassType === "password") {
          setNPassType("text")
          return;
        }
        setNPassType("password")
        break;

    }
  }

  useEffect(() => {
    const interval = setInterval(() => { setBtnState(false) }, 5000);
    return () => {
      clearInterval(interval);
    };
  }, [btnState])

  return (
    <>
      <Formik
        initialValues={formValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleSendOTP(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount
        }) => (
          <Box className="p-3">
            <>
              <Box className="col" style={{ position: "relative" }}>
                <Input
                  type="email"
                  label="Email Address"
                  // required
                  value={values?.Email}
                  name="Email"
                  className="border"
                  css={{ fontSize: "0.75rem" }}
                  autoComplete="off"
                  readonly="readonly"
                  onChange={handleChange}
                  required
                  error={submitCount ? errors.Email : null}
                />
              </Box>
            </>
            <>
              <Box className="col" style={{ position: "relative" }}>
                <Input
                  type={npassType}
                  label="New Password"
                  required
                  onChange={handleChange}
                  value={values.Password}
                  name="Password"
                  className="border"
                  css={{ fontSize: "0.75rem" }}
                  error={submitCount ? errors.Password : null}
                  maxLength={15}
                />
                <Button
                  //onClick={togglePasswordType}
                  className="input-group-text"
                  color="clear"
                  style={{
                    position: "absolute",
                    right: "0px",
                    top: "31px",
                  }}
                >
                  {npassType === "password" ? <EyeIcon style={{ fill: "blue" }} onClick={(val: number) => { togglePassword(val = 2) }} /> : <EyeSlash style={{ fill: "blue" }} onClick={(val: number) => { togglePassword(val = 2) }} />}
                </Button>
              </Box>
            </>

            <Box className="row justify-content-end">
              {/* <Box className="col-auto" >
                <Button
                  type="submit"
                  disabled={btnState}
                  color="yellow"
                >
                  Cancel
                </Button>
              </Box> */}
              <Box className="col-auto">
                <Button
                  type="submit"
                  color="yellow"
                  disabled={btnState}
                  onClick={() => {
                    setBtnState(true);
                    handleSubmit();
                  }}
                //   onClick={()=>{setShowOTP(true)}}
                // loading={loading}
                >
                  {btnState ? <>Loading </> : <>Send OTP </>}
                </Button>
              </Box>
            </Box>
          </Box>
          //   <Box css={{ p: 20 }}>
          //     <Box className="row">
          //       <Box className="col-md-6 col-sm-12 col-lg-6">
          //         <Input
          //           label="PAN Number"
          //           name="PAN_No"
          //           value={values?.PAN_No.toUpperCase()}
          //           placeholder="PAN Number"
          //           onChange={handleChange}
          //           required
          //           error={errors.PAN_No}
          //           disabled
          //         />
          //       </Box>
          //       <Box className="col-md-6 col-sm-12 col-lg-6">
          //         <Input
          //           label="Name As Per Bank"
          //           name="Name"
          //           value={values?.Name}
          //           placeholder="Name As Per Bank"
          //           onChange={handleChange}
          //           required
          //           error={errors.Name}
          //         />
          //       </Box>
          //     </Box>
          //     <Box className="row">
          //       <Box className="col-md-6 col-sm-12 col-lg-6">
          //         <Input
          //           type="email"
          //           label="Investor Email"
          //           name="Email"
          //           value={values?.Email}
          //           placeholder="Investor Email"
          //           onChange={handleChange}
          //           required
          //           error={errors.Email}
          //         />
          //       </Box>
          //       <Box className="col-md-6 col-sm-12 col-lg-6">
          //         <Input
          //           type="number"
          //           label="Investor Mobile"
          //           name="Mobile"
          //           value={values?.Mobile}
          //           placeholder="Investor Mobile"
          //           onChange={handleChange}
          //           required
          //           error={errors.Mobile}
          //         />
          //       </Box>
          //     </Box>
          //     <Box className="row">
          //       <Box className="col"></Box>
          //       <Box className="col col-sm-auto">
          //         <Button
          //           color="yellow"
          //           onClick={() => setKYCStatus(null)}
          //         >
          //           Back
          //         </Button>
          //         <Button
          //           type="submit"
          //           color="yellow"
          //           onClick={handleSubmit}
          //           loading={loading}
          //         >
          //           Send OTP
          //         </Button>
          //       </Box>
          //     </Box>
          //   </Box>
        )}
      </Formik>
    </>
  );
};
export default Basic;
