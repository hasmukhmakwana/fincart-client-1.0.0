import Holder from 'App/shared/components/Holder/Holder'
import Layout from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import React from 'react'
import SystematicTransferPlanPage from './components/SystematicTransferPlanPage'

const SystematicTransferPlanModule = () => {
  return (
    <Layout>
    <PageHeader title='Systematic Transfer Plan' rightContent={<Holder/>} />
   <SystematicTransferPlanPage/>
   </Layout>
  )
}

export default SystematicTransferPlanModule