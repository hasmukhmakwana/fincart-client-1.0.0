import { RadioGroup } from "@radix-ui/react-radio-group";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import Checkbox from "@ui/Checkbox/Checkbox";
import { GroupBox } from "@ui/Group/Group.styles";
import Input from "@ui/Input";
import Radio from "@ui/Radio/Radio";
import SelectMenu from "@ui/Select/Select";
import Text from "@ui/Text/Text";
import React from "react";
import style from "../SystematicTransferPlan.module.scss";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/DataGrid/DataGrid.styles";
const SystematicTransferPlanPage = () => {
  return (
    <Box background="gray">
      <Box className="container-fluid">
        <Box className="row">
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Card>
              <GroupBox
                position="apart"
                align="center"
                css={{ p: 20 }}
                borderBottom
              >
                <Box>
                  <Text weight="extrabold" size="h3">
                    Invest From
                  </Text>
                </Box>
              </GroupBox>
              <Box css={{ p: 20 }}>
                <Table bordered>
                  <ColumnParent>
                    <ColumnRow>
                      <Column className={style.columnRow}>Scheme </Column>
                      <Column className={style.columnRow}>Folio Number</Column>
                      <Column className={style.columnRow}>Unit</Column>
                      <Column className={style.columnRow}>Current Value</Column>
                    </ColumnRow>
                  </ColumnParent>
                  <DataParent>
                    <DataRow>
                      <DataCell className={style.dataCell}>
                        AXIS MID CAP FUND-DIRECT GROWTH
                      </DataCell>
                      <DataCell className={style.dataCell}>1854672</DataCell>
                      <DataCell className={style.dataCell}>420.05</DataCell>
                      <DataCell className={style.dataCell}>0</DataCell>
                    </DataRow>
                  </DataParent>
                </Table>
              </Box>
            </Card>
            <Card>
              <GroupBox
                position="apart"
                align="center"
                css={{ p: 20 }}
                borderBottom
              >
                <Box>
                  <Box>
                    <Text weight="extrabold" size="h3">
                      Invest In
                    </Text>
                  </Box>
                </Box>
                {/* <Box>
                <Text weight="bold">Total Amount : 0</Text>
              </Box> */}
              </GroupBox>
              <Box css={{ p: 20 }}>
                <Box>
                  <Text weight="bold">
                    ADITYA BIRLA SUN LIFE LIQUID FUND-GROWTH-DIRECT GROWTH
                  </Text>
                </Box>
                <Box>
                  <Box className="row" position="middle">
                    <Box className="col-3">
                      {/* <Text color="gray" size="h6">
                    Folio
                  </Text>
                  <Text>1010001010</Text> */}
                      <Input
                        label="Initial Investment"
                        name="firstName"
                        placeholder="1010001010"
                      />
                    </Box>
                    <Box className="col-3">
                      <Input
                        label="Amount"
                        name="firstName"
                        placeholder="Amount"
                      />
                    </Box>
                    <Box className="col-3">
                      <Input
                        label="Number of Installment"
                        name="firstName"
                        placeholder="Units"
                      />
                    </Box>
                    <Box className="col-3">
                    <Input
                        label="Goal"
                        name="firstName"
                        placeholder="No Goal"
                      />
                    </Box>
                    <Box className="col-3">
                    <SelectMenu
                        items={[
                          { id: "1", name: "Baroda" },
                          { id: "2", name: "Surat" },
                        ]}
                        bindValue={"id"}
                        bindName={"name"}
                        label={"Frequency"}
                        placeholder={""}
                        onChange={()=>{}}
                      />
                    </Box>
                    <Box className="col-3">
                    <SelectMenu
                        items={[
                          { id: "1", name: "Baroda" },
                          { id: "2", name: "Surat" },
                        ]}
                        bindValue={"id"}
                        bindName={"name"}
                        label={"STP Day"}
                        placeholder={""}
                        onChange={()=>{}}
                      />
                    </Box>
                    <Box className="col-3">
                    <SelectMenu
                        items={[
                          { id: "1", name: "Baroda" },
                          { id: "2", name: "Surat" },
                        ]}
                        bindValue={"id"}
                        bindName={"name"}
                        label={"Start Date"}
                        placeholder={""}
                        onChange={()=>{}}
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Card>
            <RadioGroup defaultValue="1M" className="inlineRadio">
              <Radio
                value="1M"
                label="I/We hereby confirm that this is transaction done purely at my/our sole discretion"
                id="1M"
              />
            </RadioGroup>
            <Box className="text-end">
              <Button color="yellow">Cancle </Button>
              <Button color="yellow">Confirm</Button>
            </Box>
          </Box>
          <Box className="col"></Box>
        </Box>
      </Box>
    </Box>
  );
};
export default SystematicTransferPlanPage;
