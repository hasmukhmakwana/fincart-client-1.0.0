import Box from "@ui/Box/Box";
import { setLS } from "@ui/DataGrid/utils";
import Loader from "@ui/SimpleGrid/components/Loader";
import { getLoginDetails, getUserDetails, login } from "App/api/login";
import useHeaderStore from "App/shared/components/Header/store";
import { toastAlert } from "App/shared/components/Layout";
import { CLIENT_ID, CLIENT_SECRET_KEY, TOKEN_PREFIX, USER_DETAILS, WEB_URL } from "App/utils/constants";
import { encryptData } from "App/utils/helpers";
import router, { useRouter } from "next/router";
import style from '../login.module.scss';

const AutoLogin = () => {
    const {
        userInfo,
        showFeedback,
        setShowFeedback,
        setUserInfo
    } = useHeaderStore();

    const validateLogin = () => {
        var router = useRouter();
        var id = router.query["id"];

        if (id != undefined) {
            const encrypted_id: any = encryptData(id, true);
            assignLoggedInStatus(encrypted_id);
        }
    }

    const assignLoggedInStatus = async (encrypted_id: string) => {
        const result = await getLoginDetails(encrypted_id);

        if (result != undefined) {
            const logInDetails = result?.data;

            if (logInDetails != undefined) {
                const logInArray: string[] = logInDetails?.split("|");

                const loginObj: any = {
                    username: (logInArray[0]).toString(),
                    password: (logInArray[1]).toString(),
                    client_id: CLIENT_ID,
                    client_secret: CLIENT_SECRET_KEY
                }

                const encrypted: any = encryptData(loginObj);

                let loginResult = await login({
                    Payload: encrypted,
                    grant_type: "password",
                });

                if (loginResult?.data?.access_token) {
                    getLoggedInDetails(loginResult.data.access_token);
                    setLS(TOKEN_PREFIX, {
                        ...loginResult.data,
                        local_iat: new Date().getTime() + loginResult.data.expires_in * 1000,
                    });

                    router.push("/Dashboard");
                }
            }
        }
    }

    const getLoggedInDetails = async (access_token: string) => {
        try {
            let result = await getUserDetails(access_token);

            const userInfo: any = result?.data || "";

            setUserInfo(userInfo);

            setLS(USER_DETAILS, userInfo);

            setShowFeedback(false);
        } catch (error: any) {
            if (error?.msg === "Database Error") {
                return toastAlert("error", error);
            }
            console.log(error);
            toastAlert("error", error);
        }
    };

    return (
        <>
            {validateLogin()}
            <Box className="container">
                <div className="row">
                    <div className="col-12">
                        <img
                            src={`${WEB_URL}/logo.png`}
                            alt="logo"
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        &nbsp;
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 text-center">
                        <h3 className={`${style.titleHeader}`}>Processing ...</h3><br></br><br></br>
                        <Loader></Loader>
                    </div>
                </div>
            </Box>
        </>
    )
}

export default AutoLogin;