import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { ActiveTypes, LoginFormData } from "../loginTypes";
import Box from "App/ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Input from "App/ui/Input";
import dynamic from "next/dynamic";
import EyeIcon from "App/icons/EyeIcon";
import { encryptData } from "App/utils/helpers";
import { forgotPassword } from "App/api/login";
const EyeSlash:any = dynamic(() => import("App/icons/EyeSlash"));
const LoginSchema = Yup.object().shape({
  username: Yup.string()
    .required("Username is required")
    .email("Invalid Username"),
  password: Yup.string().required("Password is required"),
});
const ForgotSchema = Yup.object().shape({
  username: Yup.string().required("Email is required").email("Invalid Email"),
});

const VerifyOTP = Yup.object().shape({
  OTP: Yup.string().required("OTP is required"),
  password: Yup.string()
    .required("Password is required")
    .min(6, "Password is too short - should be 6 chars minimum.")
    .max(15, "Password is too long - should be 15 chars maximum."),
});

const schemaList: any = {
  login: LoginSchema,
  fp: ForgotSchema,
  otp: VerifyOTP,
};

const helperText: any = {
  login: "Forgot your password ?",
  fp: "Back To Login.",
  otp: "Back To Login",
};

const buttonText: any = {
  login: "login",
  fp: "send otp",
  otp: "verify otp",
};

type LoginEventTypes = {
  initValues: LoginFormData;
  setLoginForm: (values: LoginFormData) => void;
  handleData: (value: LoginFormData) => void;
  togglePasswordType: () => void;
  passwordType: "password" | "text";
  active: ActiveTypes;
  changeActive: () => void;
  loading: boolean;
  setMessage: (msg: string) => void;
};
function LoginForm({
  initValues,
  setLoginForm,
  handleData,
  togglePasswordType,
  passwordType,
  active,
  changeActive,
  loading,
  setMessage,
}: LoginEventTypes) {
  const router = useRouter();
  const [counter, setcounter] = useState<number>(30);

  //*useEffects
  useEffect(() => {
    if (!counter || active !== "otp") {
      return;
    }
    const interval = setTimeout(() => setcounter((prev) => prev - 1), 1000);
    return () => {
      clearInterval(interval);
    };
  }, [counter, active]);

  //*functions

  const resendOTP = async () => {
    try {
      setMessage("");
      setcounter(30);
      console.log(initValues?.username);
      const enc: any = encryptData(initValues?.username, true);
      await forgotPassword(enc);
      setMessage("OTP has been resent!!");
    } catch (error: any) {
      console.log(error);
      setcounter(0);
      setMessage(error?.[0]?.msg || error?.msg);
    }
  };

  return (
    <>
      <Formik
        initialValues={initValues}
        validationSchema={schemaList[active]}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleData(values);
          setLoginForm(values);
          resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount,
        }) => (
          <Form>
            {active != "otp" ? (
              <Box className="col">
                <Input
                  label="USERNAME"
                  required
                  onChange={handleChange}
                  value={values.username}
                  name="username"
                  error={submitCount ? errors.username : null}
                />
              </Box>
            ) : (
              <Box className="col">
                <Input
                  label="OTP"
                  required
                  onChange={handleChange}
                  value={values.OTP}
                  name="OTP"
                  error={submitCount ? errors.OTP : null}
                />
              </Box>
            )}
            {active != "fp" ? (
              <>
                <Box className="col" style={{ position: "relative" }}>
                  <Input
                    type={passwordType}
                    label="PASSWORD"
                    required
                    onChange={handleChange}
                    value={values.password}
                    name="password"
                    error={submitCount ? errors.password : null}
                  />
                  <Button
                    onClick={togglePasswordType}
                    className="input-group-text"
                    color="clear"
                    style={{
                      position: "absolute",
                      right: "0px",
                      top: "31px",
                    }}
                  >
                    {passwordType == "password" ? (
                      <EyeIcon style={{ fill: "blue" }} />
                    ) : (
                      <EyeSlash style={{ fill: "blue" }} />
                    )}
                  </Button>
                </Box>
              </>
            ) : null}
            <Box>
              <Box className="">
                <Box className="align-item-center"></Box>
                <Button
                  type="submit"
                  full
                  color="shadowbtn"
                  onClick={handleSubmit}
                  loading={loading}
                  css={{
                    textTransform: "uppercase",
                    wordSpacing: "3px",
                    letterSpacing: "1px",
                  }}
                >
                  {buttonText[active]}
                </Button>
                <Box className="align-item-center"></Box>
                {active === "otp" && (
                  <Button
                    className="mt-3"
                    full
                    color="shadowbtn"
                    onClick={resendOTP}
                    // loading={resendLoading}
                    css={{
                      textTransform: "uppercase",
                      wordSpacing: "3px",
                      letterSpacing: "1px",
                    }}
                    disabled={!!counter}
                  >
                    {counter ? `resend otp (${counter})` : `resend otp`}
                  </Button>
                )}
              </Box>
            </Box>
            <Box>
              <Box>
                <Box className="align-item-center my-3">
                  <a className="small" color="shadowbtn" onClick={changeActive}>
                    {helperText[active]}
                  </a>
                </Box>
                <Button
                  full
                  color="default"
                  css={{
                    textTransform: "uppercase",
                    wordSpacing: "3px",
                    letterSpacing: "1px",
                  }}
                  onClick={() => router.push("/SignUp")}
                >
                  CREATE AN ACCOUNT
                </Button>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </>
  );
}
export default LoginForm;
