export type LoginFormData = {
  username: string;
  password: string;
  client_id: string;
  client_secret: string;
  OTP: string;
};

export type LoginFormTypes = {
  alertMessage?: string;
  loader: boolean;
  loginData: {
    username: string;
    password: string;
    client_id: string;
    client_secret: string;
    OTP: string;
  };
};

export type ActiveTypes = "login" | "fp" | "otp";

export enum LOGIN_ACTION_TYPES {
  DO_LOGIN = "DO_LOGIN",
  ERROR = "ERROR",
  RESET_FORM = "RESET_FORM",
  GET_FNYEAR = "GET_FNYEAR",
}
