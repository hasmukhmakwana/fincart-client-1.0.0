import React, { FC, useEffect, useReducer, useState } from "react";
import style from "./login.module.scss";
import { TOKEN_PREFIX, USER_DETAILS, WEB_URL } from "App/utils/constants";
import {
  forgotPassword,
  login,
  getUserDetails,
  validate_FP_OTP,
} from "App/api/login";
import { useRouter } from "next/router";
import Text from "@ui/Text/Text";
import LoginForm from "./components/loginForm";
import { initialState, LoginReducer } from "./loginReducer";
import { ActiveTypes, LoginFormData, LOGIN_ACTION_TYPES } from "./loginTypes";
import { setLS } from "@ui/DataGrid/utils";
import { encryptData } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import useHeaderStore from "App/shared/components/Header/store";
import ModalDialog from "@ui/AlertDialog/ModalDialog";
import SessionEnd from "./SessionEnd";
import useLoginStore from "./store";
// var localStorage: Storage;
const headerText: any = {
  login: "Please login using your account",
  fp: "Forgot Password?",
  otp: "Verify OTP",
};


const LoginModule: FC = () => {
  const [state, dispatch] = useReducer(LoginReducer, initialState);
  const { loginStatus, setLoginStatus } = useLoginStore();
  const { setUserInfo, setShowFeedback } = useHeaderStore();
  const setLoginForm = (formValues: LoginFormData) => {
    dispatch({ type: LOGIN_ACTION_TYPES.DO_LOGIN, payload: formValues });
  };

  const setMessage = (payload: string) => {
    dispatch({
      type: LOGIN_ACTION_TYPES.ERROR,
      payload,
    });
  };

  const router = useRouter();
  const [active, setactive] = useState<ActiveTypes>("login");
  const [passwordType, setpasswordType] = useState<"password" | "text">(
    "password"
  );
  const [loading, setloading] = useState<boolean>(false);

  useEffect(() => {
    router.prefetch("/");
    return () => { };
  }, []);

  const changeActive = () => {
    setMessage("");
    setactive((prev) => (prev == "login" ? "fp" : "login"));
  };

  const togglePasswordType = () => {
    setpasswordType((prev) => (prev == "password" ? "text" : "password"));
  };

  const handleData = async (formValues: LoginFormData) => {
    if (active === "login") {
      try {
        setloading(true);
        const encrypted: any = encryptData(formValues);
        // console.log(encrypted);
        let result = await login({
          Payload: encrypted,
          grant_type: "password",
        });
        setloading(false);

        console.log(result, "----result");
        if (result?.data?.access_token) {
          getLoggedInDetails(result.data.access_token);
          setLS(TOKEN_PREFIX, {
            ...result.data,
            local_iat: new Date().getTime() + result.data.expires_in * 1000,
          });
          // authDispatch({ type: "SIGN_IN" });
          router.push("/Dashboard");
        } else {
          setMessage("Invalid credentials, Please try again.");
        }
      } catch (error: any) {
        console.log(error);
        setloading(false);

        setMessage(
          error?.msg ||
          error?.[0]?.msg ||
          error?.response?.data?.error_description ||
          "Something went wrong!"
        );
      }
    } else if (active === "fp") {
      try {
        setloading(true);
        const enc: any = encryptData(formValues?.username, true);
        await forgotPassword(enc);
        setloading(false);
        setactive("otp");
        setMessage("OTP has been sent!!");
      } catch (error: any) {
        console.log(error);
        setloading(false);
        setMessage(error?.[0]?.msg || error?.msg);
      }
    } else {
      try {
        setloading(true);
        const obj: any = {
          otp: formValues?.OTP,
          userid: formValues?.username,
          password: formValues?.password,
        };
        const enc: any = encryptData(obj);
        await validate_FP_OTP(enc);
        setloading(false);
        setactive("login");
        setMessage("OTP Verified!!");
      } catch (error: any) {
        console.log(error);
        setloading(false);
        setMessage(error?.[0]?.msg || error?.msg);
      }
    }
  };

  const getLoggedInDetails = async (access_token: string) => {
    try {
      let result = await getUserDetails(access_token);
      const userInfo: any = result?.data || "";
      setUserInfo(userInfo);
      setLS(USER_DETAILS, userInfo);

      if (userInfo?.isFeedbackReq == "Y") {
        setShowFeedback(true);
      }
      else {
        setShowFeedback(false);
      }
    } catch (error: any) {
      if (error?.msg === "Database Error") {
        return toastAlert("error", error);
      }
      console.log(error);
      toastAlert("error", error);
    }
  };

  return (
    <div className={style.loginbg}>
      <header className={style.header}>
        <div className={style.headerContainer}>
          <div
            style={{
              paddingBottom: "0.5rem",
              paddingTop: "0.5rem",
            }}
          >
            <div className="container">
              <div className="row row-cols-2 align-items-center justify-content-between">
                <div className="col d-flex align-items-center">
                  <div className={`${style.logoSection}`}>
                    <img
                      // width={100}
                      // height={30}
                      src={`${WEB_URL}/logo.png`}
                      alt="logo"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div className="container">
        <div className="row">
          <div className="col-12 col-sm-8 mt-3 my-sm-auto">
            <div className="ms-auto">
              <div className="row">
                <div className="col-12">
                  <h2 className="text-white text-center">
                    MEET YOUR FUTURE<br></br>
                    GOALS WITH FINCART
                  </h2>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  &nbsp;
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="text-center" style={{ color: '#B3DAFF' }}>
                    At Fincart, our endeavor is not to sell you yet another financial product but help you
                    with a completely customized solution and guide you in your financial journey.
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  &nbsp;
                </div>
              </div>
              <div
                style={{ borderBottom: "solid 1px #FAAC05" }}
              ></div>
              <div className="row">
                <div className="col-12">
                  &nbsp;
                </div>
              </div>
              {/* <div className="row">
                <div className="col-12">
                  <hr className="w-75 m-auto" style={{ borderColor: '#FAAC05', borderWidth: '0.13rem' }}></hr>
                  &nbsp;
                </div>
              </div> */}
              <div className="row">
                <div className="col-12">
                  <div className="text-center">
                    <img src={`${WEB_URL}/investment_icon.png`}
                      alt="logo" className="img-fluid"></img>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-4">
            <div className="mt-3 mt-sm-auto align-items-start align-items-center">
              <div className={style.loginContainer}>
                <div className={style.loginBox}>
                  <div className="text-center pt-4">
                    {/* @ts-ignore */}
                    <Text className={style.pleaselogin} color="default" size="h6">
                      {headerText[active]}
                    </Text>
                    {state?.alertMessage ? (
                      <Text css={{ fontSize: "0.7rem" }} className="text-danger">
                        {state?.alertMessage}
                      </Text>
                    ) : null}
                  </div>
                  <LoginForm
                    setMessage={setMessage}
                    initValues={state.loginData}
                    setLoginForm={setLoginForm}
                    handleData={handleData}
                    togglePasswordType={togglePasswordType}
                    passwordType={passwordType}
                    active={active}
                    changeActive={changeActive}
                    loading={loading}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <ModalDialog
          open={loginStatus}
          setOpen={setLoginStatus}
          hideCloseBtn={true}
          css={{
            "@bp0": { width: "50%" },
            "@bp1": { width: "20%" },
          }}
        >
          <SessionEnd setOpen={setLoginStatus} />
        </ModalDialog>
      </div>
    </div>
  );
};

export default LoginModule;
