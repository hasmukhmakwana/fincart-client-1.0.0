import create from "zustand";

interface StoreTypes {
    loginStatus: boolean;
    setLoginStatus: (payload: boolean) => void;
}
const useLoginStore = create<StoreTypes>((set) => ({
    loginStatus: false,
    setLoginStatus: (payload) =>
        set((state) => ({
            ...state,
            loginStatus: payload,
        })),
}))

export default useLoginStore;