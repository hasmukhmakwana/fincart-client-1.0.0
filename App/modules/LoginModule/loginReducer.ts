import { CLIENT_ID, CLIENT_SECRET_KEY } from "App/utils/constants";
import { LoginFormTypes, LOGIN_ACTION_TYPES } from "./loginTypes";

export const initialState: LoginFormTypes = {
  loginData: {
    username: "",
    password: "",
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET_KEY,
    OTP: "",
  },
  alertMessage: "",
  loader: true,
};

export function LoginReducer(
  state: LoginFormTypes = initialState,
  action: { payload: any; type: LOGIN_ACTION_TYPES }
) {
  let { type, payload } = action;
  switch (type) {
    case LOGIN_ACTION_TYPES.DO_LOGIN:
      return { ...state, loginData: { ...payload } };

    case LOGIN_ACTION_TYPES.GET_FNYEAR:
      return {
        ...state,
        loginData: [...payload.rows],
      };

    //ERROR
    case LOGIN_ACTION_TYPES.ERROR:
      return { ...state, alertMessage: payload };

    default:
      return { ...state };
      break;
  }
}
