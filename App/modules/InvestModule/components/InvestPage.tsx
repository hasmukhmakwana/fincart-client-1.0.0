import React from "react";
import Image from "next/image";
import style from "../Invest.module.scss";
import Box from "./../../../ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import { GroupBox } from "./../../../ui/Group/Group.styles";
import Card from "@ui/Card";
import UserIcon from "App/icons/UserIcon";
import RightArrowShort from "App/icons/RightArrowShort";
import Personfill from "App/icons/Personfill";

const InvestPage = () => {
  return (
    <Box background="gray">
      <Box className="container-fluid">
        <Box className="row">
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Box className={style.container}>
              <Text weight="bold" color="default" size="h2">
                Invest Lumpsum
              </Text>
            </Box>
            <Card>
              <Text weight="bold" size="h4" border="default">
                Select Holding Pattern
              </Text>
              <Box className={style.investContainer}>
                <GroupBox>
                  <Box className={style.imgbg} background="default">
                    <Personfill size="20" />
                  </Box>
                  <Box>
                    <Text weight="bold" size="h6">
                      First/Primary Holder
                    </Text>
                    <Text size="h6">Primary Holder Name</Text>
                  </Box>
                </GroupBox>
                <GroupBox>
                  <Box className={style.imgbg} background="default">
                    <Personfill size="20"  />
                    {/* <img
                            width={54}
                            height={54}
                            src={`${WEB_URL}/FontAwsome (book-reader).png`}
                            alt="logo"
                        /> */}
                  </Box>
                  <Box>
                    <Text weight="bold" size="h6">
                      Second Holder
                    </Text>
                    <Text size="h6">Second Holder Name</Text>
                  </Box>
                </GroupBox>
                <GroupBox>
                  <Box className={style.imgbg} background="default">
                    <Personfill size="20" />
                  </Box>
                  <Box>
                    <Text weight="bold" size="h6">
                      Third Holder
                    </Text>
                    <Text size="h6">Third Holder Name</Text>
                  </Box>
                </GroupBox>
                <GroupBox>
                  <Box>
                    <Button color="yellow">
                      Select <RightArrowShort size="20" />

                    </Button>
                  </Box>

                </GroupBox>
              </Box>
            </Card>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default InvestPage;
