import React  from "react";
import Layout from "App/shared/components/Layout";

import Loader from "App/shared/components/Loader";
import dynamic from "next/dynamic";
const InvestPage: any = dynamic(() => import("./components/InvestPage"), {
  loading: () => <Loader />,
});

const InvestModule = () => {
  return (
    <Layout>
      <InvestPage />
    </Layout>
  );
};

export default InvestModule;
