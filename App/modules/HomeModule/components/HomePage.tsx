import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import ArrowRightFill from "App/icons/ArrowRightFill";
import { WEB_URL } from "App/utils/constants";
import Image from "next/image";
import React from "react";
import style from "../Home.module.scss";
import Box from "./../../../ui/Box/Box";
const HomePage = () => {
  return (
    <Box className="container-fluid">
      <Box className="row">
        <Box className="col-md-12 col-sm-12 col-lg-12">

          <Box>
            <Box className={style.homeContainer}>
              <Text weight="bold" color="default" size="h5">
                Equity Basket
              </Text>
              <Box>
                <Button color="yellow">
                  View More
                  <ArrowRightFill size="10"></ArrowRightFill>
                </Button>
              </Box>
            </Box>
            <Box className="row">
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                      <img
                        width={105}
                        height={73}
                        src={`${WEB_URL}/icon1.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Large Cap
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                     <img
                        width={105}
                        height={73}
                        src={`${WEB_URL}/icon1.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Multi Cap
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                     <img
                        width={105}
                        height={73}
                        src={`${WEB_URL}/icon1.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Mid Cap
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                     <img
                        width={105}
                        height={73}
                        src={`${WEB_URL}/icon1.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Small Cap
                    </Text>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
          <Box>
            <Box className={style.homeContainer}>
              <Text weight="bold" color="default" size="h5">
                Debt Basket
              </Text>
              <Box>
                <Button color="yellow">
                  View More
                  <ArrowRightFill size="10"></ArrowRightFill>
                </Button>
              </Box>
            </Box>
            <Box className="row">
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                    <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Liquid
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                     <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Short Term
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Money Market
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Long Term
                    </Text>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
          <Box>
            <Box className={style.homeContainer}>
              <Text weight="bold" color="default" size="h5">
                Themed Basket
              </Text>
              <Box>
                <Button color="yellow">
                  View More
                  <ArrowRightFill size="10"></ArrowRightFill>
                </Button>
              </Box>
            </Box>
            <Box className="row">
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Tax Saving
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Sectoral
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Passive Funds
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Children Funds
                    </Text>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
          <Box>
            <Box className={style.homeContainer}>
              <Text weight="bold" color="default" size="h5">
                Fincart Curated Basket
              </Text>
              <Box>
                <Button color="yellow">
                  View More
                  <ArrowRightFill size="10"></ArrowRightFill>
                </Button>
              </Box>
            </Box>
            <Box className="row">
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Tax Saving
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Sectoral
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Passive Funds
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box className="col-md-3">
                <Box className={style.basketContainer}>
                  <Box className={style.basketBox}>
                    <Box className="text-center">
                       <img
                        width={78}
                        height={78}
                        src={`${WEB_URL}/icon2.png`}
                        alt="hand"
                      />
                    </Box>
                    <Text
                      weight="bold"
                      color="default"
                      size="bt"
                      className="text-center"
                    >
                      Children Funds
                    </Text>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default HomePage;
