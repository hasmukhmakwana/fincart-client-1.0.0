
import Holder from 'App/shared/components/Holder/Holder'
import Layout from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import React from 'react'
import HomePage from './components/HomePage'

const HomeModule = () => {
  return (
    <Layout>
    <PageHeader title='Home' rightContent={<Holder/>} />
   <HomePage/>
   </Layout>
  )
}

export default HomeModule