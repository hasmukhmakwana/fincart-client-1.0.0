export type AMC = {
    fundid: String,
    fundName: String,
    fundImg: String
}

export type SchemeCategory = {
    objName: String
}

export type SchemeSubCategory = {
    objName: String,
    subObjName: String
}


export type schemeList = {
    vendorName: String,
    rating: String,
    _1M: String,
    _3M: String,
    _6M: String,
    _1Y: String,
    _3Y: String,
    _5Y: String,
    minAmt: String,
    maxAmt: String,
    addminAmt: String,
    addMaxAmt: String,
    sipminAmt: String,
    sipmaxAmt: String,
    fundName: String,
    fundImg: String,
    fundId: String,
    goalName: String,
    goalCode: String,
    goalImg: String,
    typeCode: number,
    isPurAllow: String,
    isSwitchAllow: String,
    isRedAllow: String,
    isSIPAllow: String,
    isSTPAllow: String,
    isSWPAllow: String,
    multiples: number,
    schemeId: String,
    schemeName: String,
    orgSchemeName: String,
    obj: String,
    subObj: String,
    status: String,
    aum: String,
}

//Transaction

export type InvestmentProfileTypes = {
    Id: string;
    Profileid: string;
    FirstPan: string;
    SecondPan: string;
    ThirdPan: string;
    Moh: string;
    FirstApplicant: string;
    SecondApplicant: string;
    ThirdApplicant: string;
    FirstApplicantBasicId: string;
    SecondApplicantBasicId: string;
    ThirdApplicantBasicId: string;
    GroupLeaderBasicId: string;
    TotalActiveMandate: string;
    TotalActiveTransaction: string;
    isDelete: string;
    isUpdate: string;
};


export type MOHTypes = {
    Text: string;
    Value: string;
    Category: string;
}

export type MOHmember = {
    memberName: string;
    basicid: string;
    isNominee: boolean;
    CAF_Status: string;
}

export type MandateListTypes = {

    Id: string;
    Username: string;
    Recordid: string;
    Bid: string;
    GroupLeader: string;
    ProfileID: string;
    Bank: string;
    Branch: string;
    AccountNo: string;
    IFSC: string;
    MICR: string;
    FromMonth: string;
    FromYear: string;
    ToMonth: string;
    ToYear: string;
    PerDayLimit: string;
    Active: string;
    MandateID: string;
    Reason: string;
    UMRN_NO: string;
    Until_cancel: string;
    Basic_id: string;
    DeleteStatus: string;
    Failure_reason: string;
    Upload_status: string;
    Frequency: string;
    mandatetype: string;
    profilename: string;
    moh: string;
}

export type TransactionListTypes = {
    FolioNo: string;
    SchemeID: string;
    SchemeName: string;
    Amount: string;
    Trxn_typeid: string;
    Trxn_Type: string;
}

export type profileListType = {
    profileID: string;
    InvestProfile: string;
    basicid: string;
}

export type bankListType = {
    accountNo: string;
    bankName: string;
    bankid: string;
    basicid: string;
}