import React, { useState } from "react";
import Box from "../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import { GroupBox } from "../../../ui/Group/Group.styles";
import Card from "App/ui/Card/Card";
import style from "../SchemeList.module.scss";
import { WEB_URL } from "App/utils/constants";
import useSchemeStore from "../store";
import { formatDate } from "App/utils/helpers";

const SalesLastWeekProducts = {
  title: {
    x: "left",
  },
  tooltip: {
    trigger: "item",
    formatter: (params: any) => {
      console.log(params, "params");
      return `
      <div style="text-align: center;">
      <img style="margin-top: -30px" width="40" src="https://cdn.shopclues.com/images/thumbnails/72640/320/320/178916041DF0DZ14UL1495806482.jpg"  />
      <h6>${params.seriesName}</h6>
      <h5>${params.value}</h5>
      </div>
      `;
    },
  },
  legend: {
    type: "scroll",
    orient: "vertical",
    right: "0",
    top: 20,
    data: [""],
  },
  xAxis: {
    data: [],
  },
  yAxis: {},
  series: [
    {
      name: "Door/Window Sensor",
      type: "line",
      smooth: "true",
      data: [0, 10, 20, 30].map((i) => i * 2.5),
    },
  ],
};
const AssetAllocation = {
  title: {
    x: "left",
  },
  tooltip: {
    trigger: "item",
  },
  legend: {
    orient: "horizontal",
    left: "center",
  },
  xAxis: {
    data: [],
  },
  yAxis: {},
  series: [
    {
      name: "Access From",
      type: "pie",
      radius: "50%",
      data: [
        { value: 1048, name: "Equity" },
        { value: 735, name: "Debt" },
        { value: 580, name: "Liquid" },
      ],
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: "rgba(0, 0, 0, 0.5)",
        },
      },
    },
  ],
};
const ReturnComparison = {
  title: {
    x: "left",
    subtext: "Investment Amount :5000",
  },
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow",
    },
  },
  legend: {},
  xAxis: [
    {
      type: "category",
      data: ["Saving Acocunt", "Fixed Deposite", "This Fund"],
    },
  ],
  yAxis: [
    {
      type: "value",
    },
  ],
  series: [
    {
      name: "Saving Acocunt",
      type: "bar",
      stack: "Ad",
      emphasis: {
        focus: "series",
      },
      data: [220, 182, 191, 234, 290, 330, 310],
    },
    {
      name: "Fixed Deposite",
      type: "bar",
      stack: "Ad",
      emphasis: {
        focus: "series",
      },
      data: [150, 232, 201, 154, 190, 330, 410],
    },
  ],
};
const SectoralAllocation = {
  grid: {
    top: "5%",
    bottom: "55%",
    height: "10%",
    width: "100%",
    right: "0%",
    containLabel: true,
  },
  yAxis: {
    type: "category",
    nameLocation: "start",
    data: [
      "Basic Material",
      "Consumer Cyclical",
      "Financial Services",
      "Real Estate",
      "Communication Services",
      "Energy",
      "Industrial",
      "Technology",
    ].reverse(),
    axisLine: {
      show: false,
    },
    axisTick: {
      show: false,
    },
    show: true,
    axisLabel: {
      margin: 10,
      fontWeight: "normal",
      textStyle: { color: "#969eaa" }
    },
  },
  xAxis: {
    type: "value",
    show: true,
    splitLine: {
      show: true,
    },
    axisTick: {
      show: true,
    },
  },
  series: [
    {
      data: [
        {
          value: 100,
          itemStyle: { color: "#545e98" },
        },
        {
          value: 275,
          itemStyle: { color: "#3022d7" },
        },
        {
          value: 150,
          itemStyle: { color: "#3fc397" },
        },
        {
          value: 140,
          itemStyle: { color: "#258457" },
        },
        {
          value: 196,
          itemStyle: { color: "#ff3869" },
        },
        {
          value: 170,
          itemStyle: { color: "#98817B" },
        },
        {
          value: 149,
          itemStyle: { color: "#ff3869" },
        },
        {
          value: 297,
          itemStyle: { color: "#F5DF00" },
        },
      ],
      label: {
        normal: {
          position: "right",
          show: true,
        },
      },
      type: "bar",
      barCategoryGap: "50%",
      showBackground: true,
      backgroundStyle: {
        color: "#f5f5f5",
        barBorderRadius: [50, 50],
        borderColor: "red",
      },
      itemStyle: {
        barBorderRadius: [50, 50],
      },
      animation: true,
    },
  ],
};

interface SchemeModalTypes {
  getStars: (value: number) => void;
}

const SchemeModal = ({ getStars }: SchemeModalTypes) => {
  const [open, setOpen] = useState<boolean>(false);

  const {
    schemeDetails
  } = useSchemeStore();

  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header" css={{ borderRadius: "15px 15px 0 0" }}>
          <Box>
            <Box className="row">
              <Box className="col-auto">
                <Box>
                  <img
                    width={55}
                    height={55}
                    src={schemeDetails?.goalImg}
                    alt="logo"
                  />
                </Box>
              </Box>
              <Box className="col">
                {/* @ts-ignore */}
                <GroupBox align="center" space="sm" css={{ marginBottom: '10px' }}>
                  {/* @ts-ignore */}
                  <Text weight="bold" size="h4" className="text-light">
                    {schemeDetails?.schemeName}
                  </Text>
                  {getStars(Number(schemeDetails?.rating))}
                </GroupBox>
                <GroupBox>
                  <Box>
                    {/* @ts-ignore */}
                    <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{schemeDetails?.obj}</Text>
                    {/* @ts-ignore */}
                    <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{schemeDetails?.subObj}</Text>
                  </Box>
                </GroupBox>
              </Box>
            </Box>
          </Box>
        </Box>
        <Box className="modal-body p-3 m-1">
          <Box css={{ overflow: "auto", height: "450px" }} >
            <Card css={{ p: 10, pt: 0 }}>
              {/* @ts-ignore */}
              <GroupBox position="apart" className="pt-0" css={{ mb: 10 }}>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h4" weight="bold" css={{ color: "var(--colors-blue1)" }}>
                    Overview
                  </Text>
                </Box>
                {/* <Box>
                </Box> */}
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" className={`${style.borderBottomGrey}`}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box css={{ width: 25 }}>
                      <img
                        width={20}
                        height={15}
                        src={`${WEB_URL}/FontAwsome (chart-bar).png`}
                        alt="logo"
                      />
                    </Box>
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">Expense Ratio</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{schemeDetails?.ExpenseRatio}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" className={`${style.borderBottomGrey}`}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box css={{ width: 25 }}>
                      <img
                        width={15}
                        height={15}
                        src={`${WEB_URL}/FontAwsome (chart-pie).png`}
                        alt="logo"
                      />
                    </Box>
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">AUM</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">&#x20b9; {schemeDetails?.aum}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" className={`${style.borderBottomGrey}`}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box css={{ width: 25 }}>
                      <img
                        width={15}
                        height={15}
                        src={`${WEB_URL}/FontAwsome (calendar-day).png`}
                        alt="logo"
                      />
                    </Box>
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">Inception Date</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{formatDate(schemeDetails.InceptionDate, "d-m-y") || ""}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" className={`${style.borderBottomGrey}`}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box css={{ width: 25 }}>
                      <img
                        width={15}
                        height={20}
                        src={`${WEB_URL}/FontAwsome (rupee-sign-new).png`}
                        alt="logo"
                      />
                    </Box>
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">Min Lumpsum/SIP</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">&#x20b9;{schemeDetails?.minAmt}/&#x20b9;{schemeDetails?.sipMinAmt}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" className={`${style.borderBottomGrey}`}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box css={{ width: 25 }}>
                      <img
                        width={15}
                        height={15}
                        src={`${WEB_URL}/FontAwsome (lock).png`}
                        alt="logo"
                      />
                    </Box>
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">Lock in</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{schemeDetails?.LockIN}</Text>
                </Box>
              </GroupBox>
              <GroupBox position="apart" className={`${style.borderBottomGrey}`}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box css={{ width: 25 }}>
                      <img
                        width={25}
                        height={15}
                        src={`${WEB_URL}/FontAwsome (sign-out-alt).png`}
                        alt="logo"
                      />
                    </Box>
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">Exit Load</Text>
                      <Text size="h5">{schemeDetails?.ExitLoad}</Text>
                    </Box>
                  </GroupBox>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}

            </Card>
            <Card css={{ p: 10 }}>
              {/* @ts-ignore */}
              <GroupBox position="apart" css={{ mb: 10 }}>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h4" weight="bold" css={{ color: "var(--colors-blue1)" }}>
                    Performance
                  </Text>
                </Box>
                <Box>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" css={{ pr: 5, pl: 5 }}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">1 Month</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{schemeDetails?._1M}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" bg>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">3 Month</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{schemeDetails?._3M}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" css={{ pr: 5, pl: 5 }}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">6 Month</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{schemeDetails?._6M}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" bg>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">1 Year</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{schemeDetails?._1Y}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" css={{ pr: 5, pl: 5 }}>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">3 Year</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{schemeDetails?._3Y}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
              <GroupBox position="apart" bg>
                <Box>
                  {/* @ts-ignore */}
                  <GroupBox position="apart">
                    <Box>
                      {/* @ts-ignore */}
                      <Text size="h5">5 Year</Text>
                    </Box>
                  </GroupBox>
                </Box>
                <Box>
                  {/* @ts-ignore */}
                  <Text size="h5">{schemeDetails?._5Y}</Text>
                </Box>
              </GroupBox>
              {/* @ts-ignore */}
            </Card>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default SchemeModal;
