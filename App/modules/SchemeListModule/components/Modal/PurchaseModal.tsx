import React, { useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import style from "../../SchemeList.module.scss";
import SelectMenu from "@ui/Select/Select";
import StarFill from "App/icons/StarFill";
import Input from "App/ui/Input";
import CartFill from "App/icons/CartFill";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { addingTocart, getBankPaymentModes } from "App/api/cart";
import { encryptData, getUser } from "App/utils/helpers";
import Layout, { toastAlert } from "App/shared/components/Layout";
import useSchemeStore from "../../store";
import { fetchAllInvestments } from "App/api/recommendedSchemes";
import Loader from "App/shared/components/Loader";
import useHeaderStore from "App/shared/components/Header/store";
import { getSLInvestorDD } from 'App/api/schemeList';

export type VerifyPurchaseTypes = {
    // Name: string;
    investor: string;
    investorAccount: string;
    folioNo: string;
    Amount: string;
    // InvestDate: string;
};

interface StoreTypes {
    verifyPay: VerifyPurchaseTypes;
}

type NameEventTypes = {
    selectedData: (value: any) => void;
    setOpenPurchase: (value: boolean) => void;
    setOpenSuccess: (value: boolean) => void;
    setMessageTxt: (value: string) => void;
};

const PurchaseModal = ({ selectedData, setOpenPurchase, setOpenSuccess, setMessageTxt }: NameEventTypes) => {
    const user: any = getUser();

    const {
        mohMember,
        profileList,
        allMemberList
    } = useSchemeStore();

    const { cartCount, setCartCount } = useHeaderStore();//@SN-Cart Count Changes

    const useRegistrationStore = create<StoreTypes>((set) => ({
        //* initial state
        verifyPay: {
            // Name: "",
            investor: mohMember[0]?.basicid || "",
            investorAccount: "",
            folioNo: "",
            Amount: "",
            // InvestDate: "",
        }
    }))

    // const [filter, setFilters] = useState("1");
    const [load, setLoad] = useState<any>(false);
    const [investorAccountList, setInvestorAccountList] = useState<any>([]);
    const { verifyPay, loading } = useRegistrationStore((state: any) => state);
    const [purchaseData, setPurchaseData] = useState<any>(selectedData);
    const [amountType, setAmountType] = useState<any>("new");
    const [investorId, setInvestorId] = useState<any>(mohMember[0]?.basicid || "");
    const [accountId, setAccountId] = useState<any>("");
    const [loadingCart, setLoadingCart] = useState(false);
    const [loadFol, setLoadFol] = useState(false);
    const [foliosList, setFoliosList] = useState<any>([]);

    const purchaseModalObj = {
        mandatory: {
            investor: Yup.string()
                .required("Investor is required")
                .trim(),

            investorAccount: Yup.string()
                .required("Investor Account is required")
                .trim(),

            folioNo: Yup.string()
                .required("folio No is required")
                .trim(),

        },
        initialAmount: {
            Amount: Yup.number()
                .required("Amount is required")
                .positive()
                .integer()
                .min(purchaseData?.minAmt, `Min is ${purchaseData?.minAmt}`)
                .max(purchaseData?.maxAmt, `Max is ${purchaseData?.maxAmt}`),
        },
        addedAmount: {
            Amount: Yup.number()
                .required("Amount is required")
                .positive()
                .integer()
                .min(purchaseData?.addminAmt, `Min is ${purchaseData?.addminAmt}`)
                .max(purchaseData?.addMaxAmt, `Max is ${purchaseData?.addMaxAmt}`),
        }
    }

    const [validationSchema, setValidationSchema] = useState(purchaseModalObj.mandatory);

    const fetchAllInvest = async (bid: any, pid: any) => {
        try {
            setLoad(true);
            let obj: any = {
                basicid: bid,
                profileid: pid,
                filtertype: 'ALL'
            }
            let enc: any = encryptData(obj);
            let result: any = await fetchAllInvestments(enc);
            console.log(result?.data);
            let val = result?.data?.filter((i: any) => i.schemeId === purchaseData?.schemeId).length
            if (val === 0) {
                setAmountType("new")
            } else {
                setAmountType("add");
            }
            setLoad(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoad(false);
        }
    }

    const fetchInvestorDD = async (Id: any) => {
        try {
            setLoadFol(true);
            let obj: any = {
                basicId: Id,
                fundid: purchaseData?.fundId
            }
            const enc: any = encryptData(obj);
            const result: any = await getSLInvestorDD(enc);
            setFoliosList(result?.data?.folioList?.filter((i: any) => (i?.basicid === Id)));
            setLoadFol(false);
        } catch (error) {
            setLoadFol(false);
            console.log(error);
            toastAlert("error", error);
        }
    }

    const addToCart = async (val: any) => {
        console.log(val);
        let bankDetails: any = allMemberList?.bankList.filter((record: any) => record?.basicid === val.investor);
        // console.log(bankDetails[0].bankid, "bankDetails")
        if (val?.Amount === "") {
            toastAlert("error", "Amount should not be empty while adding to Cart");
        }
        else {
            try {
                setLoadingCart(true);
                let userGoalId = localStorage.getItem("userGoalId");
                // let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId;
                let obj =
                {
                    'basicID': val?.investor,
                    'ProfileID': val?.investorAccount,
                    'purSchemeId': purchaseData?.schemeId,
                    'tranType': 'NOR',
                    'Amount': Number(val?.Amount),
                    'No_of_Installment': null,
                    'MDate': null,
                    'PurFolioNo': val?.folioNo,
                    'bankId': bankDetails[0]?.bankid,
                    'userGoalId': userGoalId,
                    'startDate': null
                }
                //console.log(obj)
                const enc: any = encryptData(obj);
                const result: any = await addingTocart(enc);
                setOpenPurchase(false);
                setLoadingCart(false);
                setMessageTxt("Purchase of ₹" + val?.Amount + " is added to Cart Successfully")
                setOpenSuccess(true);
                let iCartCount = cartCount + 1;//@SN-Cart Count Changes
                setCartCount(1);
            }
            catch (error) {
                setLoadingCart(false);
                console.log(error);
                toastAlert("error", error);
            }
        }
    }

    useEffect(() => {
        let obj = purchaseModalObj.mandatory;
        if (amountType === "add") {
            obj = {
                ...obj,
                ...purchaseModalObj.addedAmount,
            }
        } else {
            obj = {
                ...obj,
                ...purchaseModalObj.initialAmount,
            }
        }
        setValidationSchema(obj)
        return () => { }
    }, [amountType])

    useEffect(() => {
        let arr = profileList.filter((item: any) => {
            return item?.basicid === investorId;
        });
        fetchInvestorDD(investorId);
        setInvestorAccountList(arr);
    }, [investorId])

    useEffect(() => {
        (async () => {
            if (investorId !== "" && accountId !== "") {
                await fetchAllInvest(investorId, accountId);
            }
        })();
        return () => { }
    }, [investorId, accountId])

    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}>Purchase Details</Text>
                </Box>
                <Box className="modal-body p-3">
                    <Formik
                        initialValues={verifyPay}
                        validationSchema={Yup.object().shape(validationSchema)}
                        enableReinitialize
                        onSubmit={(values, { resetForm }) => {
                            addToCart(values);
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount
                        }) => (
                            <>
                                <Box className="row mx-1 border-bottom border-warning" >
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text className="text-capitalize" >{purchaseData?.schemeName} </Text>
                                            </Box>
                                            <Box className="col-auto pt-0" >
                                                {
                                                    <>
                                                        {
                                                            (() => {
                                                                const arr = [];
                                                                for (let i = 0; i < purchaseData?.rating; i++) {
                                                                    arr.push(
                                                                        <StarFill color="orange" className="me-1" />
                                                                    );
                                                                }
                                                                return arr;
                                                            })()
                                                        }
                                                    </>
                                                }
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="col-12 py-2 px-3">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{purchaseData?.obj}</Text>
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{purchaseData?.subObj}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="row mt-2 mx-1 ">
                                    <Box className="col-6">
                                        <SelectMenu
                                            name='investor'
                                            value={values.investor}
                                            items={mohMember}
                                            bindValue={"basicid"}
                                            bindName={"memberName"}
                                            label={"Investor"}
                                            placeholder={"Select"}
                                            onChange={(e: any) => {
                                                setFieldValue("investor", e?.basicid);
                                                setInvestorId(e?.basicid)
                                                setFieldValue("folioNo", "");
                                            }}
                                            required
                                            // @ts-ignore
                                            error={submitCount ? errors.investor : null}
                                        />
                                    </Box>
                                    <Box className="col-6">

                                        <SelectMenu
                                            name='investorAccount'
                                            value={values.investorAccount}
                                            items={investorAccountList}
                                            bindValue={"profileID"}
                                            bindName={"InvestProfile"}
                                            label={"Investor Account"}
                                            placeholder={"Select"}
                                            onChange={(e: any) => {
                                                setFieldValue("investorAccount", e?.profileID);
                                                setAccountId(e?.profileID)
                                            }}
                                            required
                                            // @ts-ignore
                                            error={submitCount ? errors.investorAccount : null}
                                        />
                                    </Box>
                                    <Box className="col-6">
                                        <SelectMenu
                                            name='folioNo'
                                            value={values.folioNo}
                                            items={foliosList}
                                            bindValue={"folioNo"}
                                            bindName={"folioNo"}
                                            disabled={loadFol}
                                            label={`Folio ${loadFol ? ": loading.." : ""}`}
                                            placeholder={"Select"}
                                            onChange={(e: any) => {
                                                setFieldValue("folioNo", e?.folioNo);
                                            }}
                                            required
                                            // @ts-ignore
                                            error={submitCount ? errors.folioNo : null}
                                        />
                                    </Box>

                                    <Box className="col-6">
                                        <Input
                                            label="Enter Amount"
                                            name="Amount"
                                            placeholder="Enter Amount"
                                            className="border border-2"
                                            //@ts-ignore
                                            error={submitCount ? errors.Amount : null}
                                            onChange={(e: any) => { setFieldValue("Amount", e?.target?.value.replace(/\D/g, '')) }}
                                            value={values?.Amount}
                                            autocomplete="off"
                                        />
                                    </Box>
                                    <Box className="col-auto">
                                        {load ? <><Loader /></> : <></>}
                                    </Box>
                                </Box>

                                <Box className="row justify-content-end ms-1 pb-1">
                                    <Box className="col-auto ">
                                        <Button
                                            type="button"
                                            css={{ backgroundColor: "orange" }}
                                            disabled={loadingCart || loadFol}
                                            onClick={handleSubmit}
                                        >
                                            <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                {loadingCart ? <>Loading... </> : <><CartFill className="me-1" /> Add to Cart </>}
                                            </Text>
                                        </Button>
                                    </Box>
                                </Box>
                            </>
                        )}</Formik>
                </Box>
            </Box>
        </>
    );
}
export default (PurchaseModal);