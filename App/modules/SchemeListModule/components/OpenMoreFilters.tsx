import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Checkbox from "@ui/Checkbox/Checkbox";
import { GroupBox } from "@ui/Group/Group.styles";
import Popover from "@ui/Popover";
import { StyledClose } from "@ui/Popover/Popover.styles";
import ChevronDoubleRight from "App/icons/ChevronDoubleRight";
import Cross from "App/icons/Cross";
import Dash from "App/icons/Dash";
import PlusIcon from "App/icons/PlusIcon";
import style from "../SchemeList.module.scss";

const OpenMoreFilters = ({ children, ...props }: any) => {
  return (
    <Popover>
      {/* @ts-ignore */}
      <Popover.Trigger asChild>
        <span>
          <Button color="white" size="sm">
            <ChevronDoubleRight color="gray"></ChevronDoubleRight>
          </Button>
        </span>
        {/* @ts-ignore */}
      </Popover.Trigger>
      {/* @ts-ignore */}
      <Popover.Content sideOffset={0} side="bottom">
        <Box
          css={{
            width: "200px",
            overflowX: "auto",
            maxHeight: "300px",
            fontSize: "0.8rem",
          }}
        >
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Equity Funds" id="Debt_Funds_1" />
              </Box>
              <Box>
                <Dash color="blue"></Dash>
              </Box>
            </GroupBox>
            <Box className={style.ulContainer}>
              <Box className={style.liliContainer}>
                <Checkbox label="Large Cap Funds" id="Debt_Funds_2" />
              </Box>
              <Box className={style.liliContainer}>
                <Checkbox label="Mid Cap Funds" id="Debt_Funds_3" />
              </Box>
              <Box className={style.liliContainer}>
                <Checkbox label="Large Cap Funds" id="Debt_Funds_4" />
              </Box>
              <Box className={style.liliContainer}>
                <Checkbox label="Mid Cap Funds" id="Debt_Funds_5" />
              </Box>
              <Box className={style.liliContainer}>
                <Checkbox label="Large Cap Funds" id="Debt_Funds_6" />
              </Box>
              <Box className={style.liliContainer}>
                <Checkbox label="Mid Cap Funds" id="Debt_Funds_7" />
              </Box>
              <Box className={style.liliContainer}>
                <Checkbox label="Large Cap Funds" id="Debt_Funds_8" />
              </Box>
              <Box className={style.liliContainer}>
                <Checkbox label="Mid Cap Funds" id="Debt_Funds_9" />
              </Box>
            </Box>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Hybrid Funds" id="Debt_Funds_10" />
              </Box>
              <Box>
                <PlusIcon color="blue"></PlusIcon>
              </Box>
            </GroupBox>
          </Box>
        </Box>
        {/* @ts-ignore */}
        <Popover.Close asChild>
          <StyledClose aria-label="Close">
            <Cross />
          </StyledClose>
          {/* @ts-ignore */}
        </Popover.Close>

        {/* @ts-ignore */}
      </Popover.Content>
    </Popover>
  );
};

export default OpenMoreFilters;
