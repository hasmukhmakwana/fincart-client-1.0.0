import React, { ReactNode, useState, useEffect } from "react";
import Box from "../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import { GroupBox } from "./../../../ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import StarFill from "App/icons/StarFill";
import SelectMenu from "@ui/Select/Select";
import SchemeDetails from "./SchemeDetails";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import ViewModal from "@ui/ModalDialog/ModalDialog";
import style from "../SchemeList.module.scss";
import useSchemeStore from "../store";
import router from "next/router";
import { GoalsSections } from "App/utils/constants";
import { fetchFundList, searchSchemes } from "App/api/schemeList";
import { toastAlert } from "App/shared/components/Layout";
import { encryptData } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";
import {
  PurchaseModal,
  SIPModal,
} from "./Modal";
import CheckCircleFill from "App/icons/CheckCircleFill";
//*constants
const TERM_YEARS = ["1M", "3M", "6M", "1Y", "3Y", "5Y"];

//*main
const SchemeListPage = () => {
  //*states
  const {
    loading,
    filters,
    funds,
    mohMember,
    subObjective,
    tempObjectiveList,
    schemeSrchList,
    schemeDetails,
    setLoader,
    setSchemeDetails,
    setSchemeSrchList,
    setTempObjectiveList,
    setSubObjective,
    setFunds,
    setFilters
  } = useSchemeStore();
  // 
  const [open, setOpen] = useState(false);
  const [openInvestor, setOpenInvestor] = useState<any>();
  const [openObjective, setOpenObjective] = useState<any>();
  const [openSubObjective, setOpenSubObjective] = useState<any>("G");
  const [openSuccess, setOpenSuccess] = useState(false);
  const [load, setLoad] = useState(false);
  const [schemeload, setSchemeLoad] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [openPurchase, setOpenPurchase] = useState(false);
  const [openSIP, setOpenSIP] = useState(false);
  const [messageTxt, setMessageTxt] = useState<any>("");
  const [selectedData, setSelectedData] = useState<any>([]);
  const [currentSection, setCurrentSection] = useState<GoalsSections>();

  //* functions
  const getStars = (count: number) => {
    count = count > 5 ? 5 : count;
    const start: ReactNode[] = [];
    for (let i = 1; i <= count; i++) {
      start.push(<StarFill height={10} width={10} color="orange" />);
    }
    return start;
  };

  const fetchDropDown = async () => {
    try {
      setLoad(true);
      let userGoalId = localStorage.getItem("userGoalId");
      let encrypedString: any = encryptData(userGoalId, true);
      let result: any = await fetchFundList(encrypedString);
      setFunds(result?.data?.funds);
      setTempObjectiveList(result?.data?.obj);
      setSubObjective(result?.data?.subObj);
      setLoad(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoad(false);
    }
  }

  const searchSchemeList = async () => {
    try {
      if (openInvestor === undefined || openInvestor === "" || openObjective === "" ||
        openSubObjective === "" || openObjective === undefined ||
        openSubObjective === undefined) {

        toastAlert("warn", "Please Select All Fields");
        return;
      }
      setSchemeLoad(true);
      let userGoalId = localStorage.getItem("userGoalId");
      let obj: any = {
        fundid: openInvestor !== undefined ? openInvestor : "",
        obj: openObjective !== undefined ? openObjective : "",
        subObj: "",
        schemeName: "",
        userGoalId: userGoalId,
        divOpt: openSubObjective !== undefined ? openSubObjective : "",
        clMnrStar: 'Y'
      }

      const enc: any = encryptData(obj);
      const result: any = await searchSchemes(enc);

      setSchemeSrchList(result?.data);
      setSchemeLoad(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setSchemeLoad(false);
    }
  }

  const assignSchemeDetails = (schemeId: String) => {
    setSchemeDetails(schemeSrchList.filter(e => e.schemeId === schemeId)[0])
  }

  useEffect(() => {
    (async () => {
      await fetchDropDown();
      let section: any = localStorage?.getItem("section");
      setCurrentSection(section);
    })();
    return () => { }
  }, [])

  //*main return
  return (
    <>
      {load ? <>
        <Box className="py-3"><Loader /></Box>
      </> :
        <>
          <Box className="row m-auto rounded" css={{ backgroundColor: "#b3daff" }}>
            <Box className="col-lg-4">
              <SelectMenu
                name="AMC"
                value={openInvestor}
                items={funds}
                label={"Select AMC"}
                bindValue={"fundid"}
                bindName={"fundName"}
                disabled={schemeload}
                onChange={(e: any) =>
                  setOpenInvestor(e?.fundid || "")
                }
              />
            </Box>
            <Box className="col-lg-4">
              <Box>
                <SelectMenu
                  value={openObjective}
                  name={"SchemeCategory"}
                  items={tempObjectiveList}
                  label={"Select Scheme Category"}
                  bindValue={"objName"}
                  bindName={"objName"}
                  disabled={schemeload}
                  onChange={(e: any) => {
                    setOpenObjective(e?.objName || "")
                  }
                  }
                />
              </Box>
            </Box>
            <Box className="col-lg-3">
              <Box>
                <SelectMenu
                  label={"Select Dividend Option"}
                  name={"DividendOptions"}
                  value={openSubObjective}
                  items={[
                    { id: "N", divName: "All" },
                    { id: "G", divName: "Growth" },
                    { id: "P", divName: "Dividend Payout" },
                    { id: "R", divName: "Dividend Re-invest" },
                  ]}
                  bindValue={"id"}
                  bindName={"divName"}
                  disabled={schemeload}
                  onChange={(e: any) => {
                    setOpenSubObjective(e?.id || "")
                  }
                  }
                  isClearable
                />
              </Box>
            </Box>
            <Box className="col-lg-1 text-end  mt-4">
              <Box>
                <Button
                  className={`col-auto mb-1 py-1`}
                  color="yellowGroup"
                  size="md"
                  onClick={() => { searchSchemeList() }}
                >
                  <Box>
                    <Text
                      //@ts-ignore 
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                      className={`${style.button}`}
                    >
                      Search
                    </Text>
                  </Box>
                </Button>
              </Box>
            </Box>
          </Box>
          <Box className="row">
            {schemeload ? <><Box className="py-3"><Loader /></Box> </> :
              schemeSrchList.length === 0 ? <></> : <>
                {schemeSrchList.map((item: any, index: number) => {
                  return (
                    <Box>
                      <Card css={{ p: 20 }}>
                        <Box className="row">
                          <Box className="col-auto">
                            <Box
                              onClick={(e: any) => { assignSchemeDetails(item?.schemeId), setOpen(true) }}
                              css={{ cursor: "pointer" }}>
                              <img width={60} height={60} src={item?.goalImg} alt="logo" />
                            </Box>
                          </Box>
                          <Box className="col">
                            <GroupBox align="center" space="xs">
                              {/* @ts-ignore */}
                              <Text weight="normal" size="h5" color="blue" css={{ cursor: "pointer" }} onClick={(e: any) => { assignSchemeDetails(item?.schemeId), setOpen(true) }} >
                                {item?.schemeName}
                              </Text>
                              {getStars(item?.rating)}
                            </GroupBox>
                            <GroupBox>
                              <Box>
                                {/* @ts-ignore */}
                                <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{item?.obj}</Text>
                                {/* @ts-ignore */}
                                <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{item?.subObj}</Text>
                              </Box>
                            </GroupBox>
                            <GroupBox>
                              <Box>
                                {/* @ts-ignore */}
                                <Text size="h6" color="black">
                                  Annual Returns : 1M : {item?._1M} | 3M : {item?._3M} | 1Y : {item?._1Y} | 3Y : {item?._3Y} | 5Y : {item?._5Y}
                                </Text>
                              </Box>
                              <Box>
                                <Text size="h6" weight="bold">
                                  Scheme Size : {item?.aum}
                                </Text>
                              </Box>
                            </GroupBox>

                          </Box>
                          <Box className="col-auto mt-5">

                            <GroupBox>
                              {currentSection === "TAX_SAVING" ?
                                <>
                                  <Box>

                                    <Button
                                      className={`col-auto mb-1 py-1`}
                                      color="yellowGroup"
                                      size="md"
                                      onClick={(e: any) => {
                                        if (mohMember[0]?.isNominee) {
                                          setOpenPurchase(true);
                                          setSelectedData(item);
                                        } else {
                                          setOpenModal(true);
                                        }
                                      }}
                                    >
                                      <Box>
                                        <Text
                                          //@ts-ignore 
                                          size="h6"
                                          //@ts-ignore
                                          color="gray8"
                                          className={`${style.button}`}
                                        >
                                          Purchase</Text>
                                      </Box>
                                    </Button>
                                  </Box>
                                </> : currentSection === "QUICK_SIP" ? <>
                                  <Box alignSelf="bottom">
                                    <Button
                                      className={`col-auto mb-1 py-1`}
                                      color="yellowGroup"
                                      size="md"
                                      onClick={(e: any) => {
                                        if (mohMember[0]?.isNominee) {
                                          setOpenSIP(true);
                                          setSelectedData(item);
                                        } else {
                                          setOpenModal(true);
                                        }
                                      }}
                                    >
                                      <Box>
                                        <Text
                                          //@ts-ignore 
                                          size="h6"
                                          //@ts-ignore
                                          color="gray8"
                                          className={`${style.button}`}
                                        >
                                          SIP</Text>
                                      </Box>
                                    </Button>
                                  </Box>
                                </> : <>
                                  <Box>
                                    <Button
                                      className={`col-auto mb-1 py-1`}
                                      color="yellowGroup"
                                      size="md"
                                      onClick={(e: any) => {
                                        if (mohMember[0]?.isNominee) {
                                          setOpenPurchase(true);
                                          setSelectedData(item);
                                        } else {
                                          setOpenModal(true);
                                        }
                                      }}
                                    >
                                      <Box>
                                        <Text
                                          //@ts-ignore 
                                          size="h6"
                                          //@ts-ignore
                                          color="gray8"
                                          className={`${style.button}`}
                                        >
                                          Purchase</Text>
                                      </Box>
                                    </Button>
                                  </Box>
                                  <Box alignSelf="bottom">
                                    <Button
                                      className={`col-auto mb-1 py-1`}
                                      color="yellowGroup"
                                      size="md"
                                      onClick={(e: any) => {
                                        if (mohMember[0]?.isNominee) {
                                          setOpenSIP(true);
                                          setSelectedData(item);
                                        } else {
                                          setOpenModal(true);
                                        }
                                      }}
                                    >
                                      <Box>
                                        <Text
                                          //@ts-ignore 
                                          size="h6"
                                          //@ts-ignore
                                          color="gray8"
                                          className={`${style.button}`}
                                        >
                                          SIP</Text>
                                      </Box>
                                    </Button>
                                  </Box>
                                </>}
                            </GroupBox>
                          </Box>

                        </Box>
                      </Card>

                    </Box>
                  );
                })}
              </>
            }

          </Box>

          <DialogModal
            open={openModal}
            setOpen={setOpenModal}
            css={{
              "@bp0": { width: "70%" },
              "@bp1": { width: "40%" },
            }}
          >
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                {/* @ts-ignore */}
                <Text css={{ color: "var(--colors-blue1)" }}> Nominee Warning</Text>
              </Box>
              <Box className="modal-body p-3">
                <Box className="row justify-content-center mx-1 py-1 " >
                  <Text>Nominee Details are Mandatory. Please click on Continue, to proceed</Text>
                </Box>
                <Box className="row justify-content-end mx-1 pb-1" >
                  <Box className="col-auto px-0 pt-3">
                    <Button
                      type="button"
                      css={{ backgroundColor: "orange" }}
                      onClick={() => router.push("/Profile")}

                    >
                      <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                        {/* <CartFill className="me-1" /> */}
                        Continue
                      </Text>
                    </Button>
                  </Box>
                </Box>
              </Box>
            </Box>
          </DialogModal>

          {/* MODAL */}
          <ViewModal
            open={open}
            setOpen={setOpen}
            css={{
              "@bp0": { width: "100%" },
              "@bp1": { width: "45%" },
            }}
          >
            <SchemeDetails getStars={getStars} />
          </ViewModal>
          <DialogModal
            open={openPurchase}
            setOpen={setOpenPurchase}
            css={{
              "@bp0": { width: "90%" },
              "@bp1": { width: "50%" },
            }}
          >
            <PurchaseModal selectedData={selectedData} setOpenPurchase={setOpenPurchase} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
          </DialogModal>
          <DialogModal
            open={openSIP}
            setOpen={setOpenSIP}
            css={{
              "@bp0": { width: "90%" },
              "@bp1": { width: "50%" },
            }}
          >
            <SIPModal selectedData={selectedData} setOpenSIP={setOpenSIP} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
          </DialogModal>
          <DialogModal
            open={openSuccess}
            setOpen={setOpenSuccess}
            css={{
              "@bp0": { width: "60%" },
              "@bp1": { width: "35%" },
            }}
          >
            {/* @ts-ignore */}
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Box className="modal-body modal-body-scroll">
                <Box className="row justify-content-center">
                  <Box className="col-lg-12 text-center" css={{ py: 50 }}>
                    <CheckCircleFill color="orange" size="2rem" />
                    {/* @ts-ignore */}
                    <Text size="h5" className="text-center mt-2" css={{ color: "var(--colors-blue1)" }}>{messageTxt}</Text>
                  </Box>
                </Box>
              </Box>
            </Box>
          </DialogModal>
        </>
      }
    </>
  );
};
export default SchemeListPage;
