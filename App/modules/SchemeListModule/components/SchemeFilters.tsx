import React, { useState } from "react";
import Box from "../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import { GroupBox } from "../../../ui/Group/Group.styles";
import Card from "@ui/Card";
import Checkbox from "@ui/Checkbox/Checkbox";
import PlusIcon from "App/icons/PlusIcon";
import Dash from "App/icons/Dash";
import style from "../SchemeList.module.scss";
import Loader from "App/shared/components/Loader";
import dynamic from "next/dynamic";
const OpenMoreFilters:any = dynamic(() => import("./OpenMoreFilters"), {
  loading: () => <Loader />,
});

const SchemeFilters = () => {
  return (
    <Card className="mt-0">
      <Box>
        <GroupBox className={style.categoryContainer}>
          <Text
            weight="bold"
            size="bt"
            color="gray"
            space="letterspace"
            css={{ ml: 10 }}
          >
            Category
          </Text>
          <OpenMoreFilters />
        </GroupBox>
        <Box className={style.ulContainer}>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Debt Funds" id="Debt Funds" />
              </Box>
              <Box>
                <PlusIcon color="blue"></PlusIcon>
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Equity Funds" id="Debt Funds" />
              </Box>
              <Box>
                <Dash color="blue"></Dash>
              </Box>
            </GroupBox>
            <Box className={style.ulContainer}>
              <Box className={style.liliContainer}>
                <Checkbox label="Large Cap Funds" id="Debt Funds" />
              </Box>
              <Box className={style.liliContainer}>
                <Checkbox label="Mid Cap Funds" id="Debt Funds" />
              </Box>
            </Box>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Hybrid Funds" id="Debt Funds" />
              </Box>
              <Box>
                <PlusIcon color="blue"></PlusIcon>
              </Box>
            </GroupBox>
          </Box>
        </Box>
      </Box>
      <Box>
        <GroupBox className={style.categoryContainer} css={{ mt: 20 }}>
          <Text
            weight="bold"
            size="bt"
            color="gray"
            space="letterspace"
            css={{ ml: 10 }}
          >
            Risk
          </Text>

          <OpenMoreFilters />
        </GroupBox>
        <Box className={style.ulContainer}>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Very Hight Risk" id="Very Hight Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox
                  label="Low to Moderate Risk"
                  id="Low to Moderate Risk"
                />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Moderate Risk" id="Moderate Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Low Risk" id="Low Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Debt Funds" id="Debt Funds" />
              </Box>
              <Box>
                <PlusIcon color="blue"></PlusIcon>
              </Box>
            </GroupBox>
          </Box>
        </Box>
      </Box>
      <Box>
        <GroupBox className={style.categoryContainer} css={{ mt: 20 }}>
          <Text
            weight="bold"
            size="bt"
            color="gray"
            space="letterspace"
            css={{ ml: 10 }}
          >
            AMC
          </Text>

          <OpenMoreFilters />
        </GroupBox>
        <Box className={style.ulContainer}>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Very Hight Risk" id="Very Hight Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox
                  label="Low to Moderate Risk"
                  id="Low to Moderate Risk"
                />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Moderate Risk" id="Moderate Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Low Risk" id="Low Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Debt Funds" id="Debt Funds" />
              </Box>
              <Box>
                <PlusIcon color="blue"></PlusIcon>
              </Box>
            </GroupBox>
          </Box>
        </Box>
      </Box>
      <Box>
        <GroupBox className={style.categoryContainer} css={{ mt: 20 }}>
          <Text
            weight="bold"
            size="bt"
            color="gray"
            space="letterspace"
            css={{ ml: 10 }}
          >
            Rating
          </Text>

          <OpenMoreFilters />
        </GroupBox>
        <Box className={style.ulContainer}>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Very Hight Risk" id="Very Hight Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox
                  label="Low to Moderate Risk"
                  id="Low to Moderate Risk"
                />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Moderate Risk" id="Moderate Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Low Risk" id="Low Risk" />
              </Box>
            </GroupBox>
          </Box>
          <Box className={style.liContainer}>
            <GroupBox position="apart" space="none">
              <Box>
                <Checkbox label="Debt Funds" id="Debt Funds" />
              </Box>
              <Box>
                <PlusIcon color="blue"></PlusIcon>
              </Box>
            </GroupBox>
          </Box>
        </Box>
      </Box>
    </Card>
  );
};
export default SchemeFilters;
