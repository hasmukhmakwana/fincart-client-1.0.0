import { WEB_URL } from "App/utils/constants";
import create from "zustand";
import { AMC, SchemeCategory, SchemeSubCategory, schemeList, InvestmentProfileTypes, MOHTypes, MOHmember, MandateListTypes, TransactionListTypes, profileListType, bankListType } from "./SchemeListTypes"
interface StoreTypes {
  schemeList: any[];
  filters: any;
  funds: AMC[];
  tempObjectiveList: SchemeCategory[];
  subObjective: SchemeSubCategory[];
  schemeSrchList: schemeList[];
  loading: boolean;
  investor: string;

  schemeDetails: any;

  mohMember: MOHmember[];
  mandateList: MandateListTypes[];
  transactionList: TransactionListTypes[];
  profileList: profileListType[];
  bankList: bankListType[];

  allMemberList: any;
  setAllMemberList: (payload: any) => void;
  setSchemeDetails: (payload: schemeList) => void;

  setBankList: (payload: bankListType[]) => void;
  setProfileList: (payload: profileListType[]) => void;
  setLoader: (payload: boolean) => void;
  setInvestor: (payload: string) => void;

  setMOHmember: (payload: MOHmember[]) => void;
  setMandateList: (payload: MandateListTypes[]) => void;
  setTransactionList: (payload: TransactionListTypes[]) => void;
  setSchemeSrchList: (payload: schemeList[]) => void;
  setFunds: (payload: AMC[]) => void;
  setTempObjectiveList: (payload: SchemeCategory[]) => void;
  setSubObjective: (payload: SchemeSubCategory[]) => void;
  setFilters: (payload: any) => void;
}
const useSchemeStore = create<StoreTypes>((set) => ({
  //* initial state
  schemeList: [
    {
      name: "Axis Mid Cap Fund Growth",
      img: `${WEB_URL}/axis-bank.png`,
      stars: 5,
    },
    {
      name: "ICICI Mid Cap Fund Growth",
      img: `${WEB_URL}/icici.png`,
      stars: 4,
    },

    {
      name: "Birla Mid Cap Fund Growth",
      img: `${WEB_URL}/birlalogo.jpg`,
      stars: 3,
    },

    {
      name: "DSP Mid Cap Fund Growth",
      img: `${WEB_URL}/dsplogo.png`,
      stars: 2,
    },
  ],

  filters: {
    termYear: "3Y",
    riskReturnOptions: "Risk",
  },
  schemeDetails: {},
  allMemberList: [],

  funds: [],
  tempObjectiveList: [],
  subObjective: [],
  schemeSrchList: [],
  profileList: [],
  bankList: [],

  loading: false,
  investor: "",
  mohMember: [],
  mandateList: [],
  transactionList: [],

  setSchemeDetails: (payload) =>
    set((state) => ({
      ...state,
      schemeDetails: payload
    })),

  setAllMemberList: (payload) =>
    set((state) => ({
      ...state,
      allMemberList: payload,
    })),

  setFunds: (payload) =>
    set((state) => ({
      ...state,
      funds: payload,
    })),
  setBankList: (payload) =>
    set((state) => ({
      ...state,
      bankList: payload,
    })),

  setTempObjectiveList: (payload) =>
    set((state) => ({
      ...state,
      tempObjectiveList: payload,
    })),

  setSubObjective: (payload) =>
    set((state) => ({
      ...state,
      subObjective: payload,
    })),

  setSchemeSrchList: (payload) =>
    set((state) => ({
      ...state,
      schemeSrchList: payload,
    })),

  setProfileList: (payload) =>
    set((state) => ({
      ...state,
      profileList: payload,
    })),

  //* methods for manipulating state
  setFilters: (payload) =>
    set((state) => ({
      ...state,
      filters: {
        ...state.filters,
        [payload.key]: payload.value,
      },
    })),


  setInvestor: (payload) =>
    set((state) => ({
      ...state,
      investor: payload,
    })),


  setMOHmember: (payload) =>
    set((state) => ({
      ...state,
      mohMember: payload,
    })),

  setLoader: (payload) =>
    set((state) => ({
      ...state,
      loading: payload,
    })),

  setMandateList: (payload) =>
    set((state) => ({
      ...state,
      mandateList: payload,
    })),

  setTransactionList: (payload) =>
    set((state) => ({
      ...state,
      transactionList: payload,
    })),
}));

export default useSchemeStore;
