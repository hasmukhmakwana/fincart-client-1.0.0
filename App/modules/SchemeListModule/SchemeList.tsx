import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import React, { useState, useEffect } from "react";
import SchemeListPage from "./components/SchemeListPage";
import Holder from "App/shared/components/Holder/Holder";
import Box from "@ui/Box/Box";
import SchemeFilters from "./components/SchemeFilters";
import useSchemeStore from "./store";
import { encryptData, getUser } from "App/utils/helpers";
import { keys } from "lodash";
import {
  viewInvestmentProfile,
} from "App/api/profile"

import {
  getMandateMaster,
  getAllMemberPurchaseDetails
} from "App/api/mandate"
import useHeaderStore from "App/shared/components/Header/store";

const SchemeListModule = () => {
  const user: any = getUser();
  const {
    investor,

    setInvestor,

    setMOHmember,
    setProfileList,
    setAllMemberList,
  } = useSchemeStore();

  const { cartCount } = useHeaderStore();

  const fetchMemberList = async () => {
    try {
      const enc: any = encryptData({
        basicId: user?.basicid,
        fundid: "0",
      });
      let result = await getAllMemberPurchaseDetails(enc);
      // console.log(result?.data);
      setAllMemberList(result?.data);
      setMOHmember(result?.data?.memberList);
      setProfileList(result?.data?.profileList);

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  useEffect(() => {
    (async () => {
      await fetchMemberList();
      setInvestor(user?.basicid)
    })();

  }, [cartCount]);

  return (
    <Layout>
      <PageHeader title="Scheme" rightContent={<></>} />
      <Box className="row">
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <SchemeListPage />
        </Box>
      </Box>
    </Layout>
  );
};

export default SchemeListModule;
