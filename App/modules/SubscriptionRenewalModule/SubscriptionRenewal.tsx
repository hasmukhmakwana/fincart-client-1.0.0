import Holder from 'App/shared/components/Holder/Holder'
import Layout from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import React from 'react'
import SubscriptionRenewalPage from './components/SubscriptionRenewalPage'

const SubscriptionRenewalModule = () => {
  return (
    <Layout>
    <PageHeader title='Subscription' rightContent={<Holder/>} />
    <SubscriptionRenewalPage/>
    </Layout>
  )
}

export default SubscriptionRenewalModule