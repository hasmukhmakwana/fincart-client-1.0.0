import React from "react";
import Box from "../../../ui/Box/Box";
import { GroupBox } from "../../../ui/Group/Group.styles";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import CartFill from "App/icons/CartFill";
import { Button } from "@ui/Button/Button";
import HourglassSplit from "App/icons/HourglassSplit";
import style from "../SubscriptionRenewal.module.scss";
import GripVertical from "App/icons/GripVertical";
import ArrowRightLong from "App/icons/ArrowRightLong";
const SubscriptionRenewalPage = () => {
  return (
    <Box background="gray">
      <Box className="container">
        <Box className="row">
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Box className={style.subsBox} css={{ p: 20, pt: 50 }}>
              <Box>
                <GroupBox color="white">
                  <Box className={style.dot}>
                    <GroupBox>
                      <GripVertical color="white" />
                    </GroupBox>
                  </Box>
                  <Box>
                    <Text weight="bold" color="white" size="h3">
                      Plan : Classic
                    </Text>
                  </Box>
                </GroupBox>
                <Box css={{ mt: 20 }}>
                  <Text weight="bold" color="white" size="h4">
                    Status : Active
                  </Text>
                </Box>
                <Box>
                  <Text weight="bold" color="white" size="h4">
                    Valid Till : 15th June 2022
                  </Text>
                </Box>
              </Box>
              <GroupBox position="apart" css={{ mt: 20 }}>
                <Box>
                  <ArrowRightLong color="white" />
                </Box>
                <Box css={{ mt: 20 }}>
                  <Button color="yellow">Renew</Button>
                </Box>
              </GroupBox>
            </Box>
          </Box>
          <Box className="col-md-8 col-sm-8 col-lg-8">
            <Card>
              <Text weight="bold" size="h3" border="default">
                Transaction
              </Text>
              <Box className="row" css={{ p: 22, mb: 80 }}>
                <Box className="col-4">
                  <Box>
                    <Text>No Transaction</Text>
                  </Box>
                </Box>
              </Box>
            </Card>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default SubscriptionRenewalPage;
