import Box from "@ui/Box/Box";
import { getAllGoalInvestment } from "App/api/goalPlan";
import { toastAlert } from "App/shared/components/Layout";
import { GoalsSections } from "App/utils/constants";
import { encryptData, getUser } from "App/utils/helpers";
import { useEffect, useState } from "react";
import { RadioGroup } from "@radix-ui/react-radio-group";
import useGoalInvestmentsStore from "../store";
import Radio from "@ui/Radio/Radio";
import Loader from "App/shared/components/Loader";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import router from "next/router";
import style from "../goalsInvestment.module.scss";
import { GroupBox } from "@ui/Group/Group.styles";
import Basket2fill from "App/icons/Basket2fill";
import CartFill from "App/icons/CartFill";
import Piggybankfill from "App/icons/Piggybankfill";
import ArrowLeftRight from "App/icons/ArrowLeftRight";
import Shuffle from "App/icons/Shuffle";
import Input from "@ui/Input";
import Label from "@ui/Label/Label";
import Bezier from "App/icons/Bezier";
import ArrowDownUp from "App/icons/ArrowDownUp";
import HandHoldingUsd from "App/icons/HandHoldingUsd";
import CheckCircleFill from "App/icons/CheckCircleFill";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import {
    PurchaseModal,
    RedeemModal,
    SIPModal,
    STPModal,
    SwitchModal,
    SwpModal,
} from "../../ProductModule/MutualFundModule/components/Modals";


const GoalsInvestmentsPage = () => {
    const user: any = getUser();
    const [loader, setLoader] = useState<boolean>(false);
    const [currentSection, setCurrentSection] = useState<GoalsSections>();
    const [filtertype, setFilterType] = useState("NONZERO");
    const [openPurchase, setOpenPurchase] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [openRedeem, setOpenRedeem] = useState(false);
    const [openSIP, setOpenSIP] = useState(false);
    const [openSTP, setOpenSTP] = useState(false);
    const [openSwitch, setOpenSwitch] = useState(false);
    const [openSwp, setOpenSwp] = useState(false);
    const [selectedScheme, setSelectedScheme] = useState<any>([]);
    const [messageTxt, setMessageTxt] = useState<any>("");
    const {
        goalsInvestmentList,
        setGoalsInvestmentList
    } = useGoalInvestmentsStore();

    const fetchGoalsInvestmentsList = async () => {
        try {
            if (!user?.basicid) return;

            setLoader(true);

            let currentUserGoalId: any = localStorage.getItem("userGoalId");

            let section: any = localStorage.getItem("section");

            setCurrentSection(section);

            const obj: any = {
                usergoalId: currentUserGoalId,
                filtertype: filtertype //ALL/NONZERO/ZERO
            }

            const enc: any = encryptData(obj);
            //console.log(enc);
            const result: any = await getAllGoalInvestment(enc);
            setLoader(false);
            setGoalsInvestmentList(result?.data);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    }

    useEffect(() => {
        (async () => {
            await fetchGoalsInvestmentsList();
        })();

    }, [filtertype]);

    return (
        <>
            <Box className="container">
                <Box className="row row-cols-1 row-cols-md-2">
                    <Box className="col-md-12 col-sm-12 col-lg-12 text-left modal-header p-2" css={{ borderRadius: "8px" }}>
                        <RadioGroup
                            defaultValue={filtertype}
                            className="inlineRadio"
                            onValueChange={(e: any) => {
                                setFilterType(e);
                            }}
                        >
                            <Radio value="ALL" label="All Folios" id="all" />
                            <Radio value="NONZERO" label="Active Folios" id="active" />
                            <Radio value="ZERO" label="Inactive Folios" id="inactive" />
                        </RadioGroup>
                    </Box>
                </Box>
                <Box className="row row-cols-1 row-cols-md-2">
                    {
                        loader ?
                            <Loader />
                            : <>
                                <Box className="col-md-12 col-sm-12 col-lg-12">
                                    <Box className="row">
                                        {goalsInvestmentList?.map((currentScheme: any, index: number) => {
                                            // console.log(currentScheme, "currentScheme")
                                            return (
                                                <Box className="col-12 col-md-6 col-lg-6 my-2">
                                                    <Card
                                                        css={{
                                                            borderRadius: "8px 8px 8px 8px",
                                                            border: "1px solid #82afd9",
                                                        }}
                                                        className="h-100"
                                                    >
                                                        <Box className="row border-bottom px-1 pb-1">
                                                            <Box className="col-auto ">
                                                                <Text size="h5" css={{ color: "var(--colors-blue1)" }}> Invested by {currentScheme?.memberName} : [{currentScheme?.InvestProfile}]</Text>
                                                            </Box>
                                                        </Box>
                                                        <Box className="row mb-3 py-1 justify-content-between">
                                                            <Box className="col-auto">
                                                                <Box className="row">
                                                                    <Box className="col-auto text-start my-auto">
                                                                        <img
                                                                            width={60}
                                                                            // height={"auto"}
                                                                            src={currentScheme?.fundImg}
                                                                            alt="logo"
                                                                        />

                                                                    </Box>
                                                                    <Box className="col-auto pe-0 me-0 ">
                                                                        <Text size="h5" weight="bold" >{currentScheme?.schemeName}</Text>
                                                                        <Text size="h6" weight="bold" css={{ fontSize: "0.70rem" }}>Folio no: {currentScheme?.folioNo}

                                                                        </Text>
                                                                        <Text className="my-1" css={{ fontSize: "0.60rem", color: "var(--colors-blue1)" }}>
                                                                            {currentScheme?.obj} - {currentScheme?.subObj}
                                                                        </Text>

                                                                    </Box>
                                                                </Box>
                                                            </Box>

                                                            <Box className="col-auto text-end">
                                                                <Box className={`badge ${currentScheme?.trxnSource === "External" ? style.externalColor : style.internalColor}`}>{currentScheme?.trxnSource} </Box>
                                                            </Box>
                                                        </Box>
                                                        <Box className="row justify-content-center my-1">
                                                            <Box className="col-auto col-lg-3 col-md-4">
                                                                <Box className="row mx-1 px-0 mx-auto mb-2" css={{ borderRadius: "8px 8px 8px 8px" }}>{/*  backgroundColor: "#0b57a4", color: "#FFF", */}
                                                                    <Box className="col-12 ">
                                                                        <Text size="h6" className="text-center ">Net Invested</Text>
                                                                    </Box>
                                                                    <Box className="col-12 ">
                                                                        <Text size="h5" className="text-center " >&#x20B9;{Number(currentScheme?.investValue).toLocaleString("en-IN")}</Text>
                                                                    </Box>

                                                                </Box>
                                                            </Box>
                                                            <Box className="col-auto col-lg-3 col-md-4">
                                                                <Box className="row mx-1 px-0 mx-auto mb-2" css={{ borderRadius: "8px 8px 8px 8px" }}>{/*  backgroundColor: "#0b57a4", color: "#FFF", */}
                                                                    <Box className="col-12 ">
                                                                        <Text size="h6" className="text-center ">Gain</Text>
                                                                    </Box>
                                                                    <Box className="col-12 ">
                                                                        <Text size="h5" className={`text-center`} >&#x20B9;{(Number(currentScheme?.currValue || 0) - Number(currentScheme?.investValue || 0)).toLocaleString("en-IN")}</Text>
                                                                    </Box>
                                                                </Box>
                                                            </Box>
                                                            <Box className="col-auto col-lg-3 col-md-4">
                                                                <Box className="row mx-1 px-0 mx-auto mb-2" css={{ borderRadius: "8px 8px 8px 8px" }}>{/*  backgroundColor: "#0b57a4", color: "#FFF", */}
                                                                    <Box className="col-12 ">
                                                                        <Text size="h6" className="text-center ">Current Amount</Text>
                                                                    </Box>
                                                                    <Box className="col-12 ">
                                                                        <Text size="h5" className="text-center " >&#x20B9;{Number(currentScheme?.currValue).toLocaleString("en-IN")}</Text>
                                                                    </Box>

                                                                </Box>
                                                            </Box>
                                                            <Box className="col-auto col-lg-3 col-md-4">
                                                                <Box className="row mx-1 px-0 mx-auto mb-2" css={{ borderRadius: "8px 8px 8px 8px" }}>{/*  backgroundColor: "#0b57a4", color: "#FFF", */}
                                                                    <Box className="col-12 ">
                                                                        <Text size="h6" className="text-center ">XIRR</Text>
                                                                    </Box>
                                                                    <Box className="col-12 ">
                                                                        <Text size="h5" className={`text-center ${(parseInt(currentScheme?.CAGR) <= 0) ? style.redFont : style.greenFont}`} >% {currentScheme?.CAGR}</Text>
                                                                    </Box>

                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                        <Box className="row justify-content-center">
                                                            <Box className="col-auto pt-1">
                                                                <Box className="row mx-1 px-0 py-2 mx-auto mb-2" css={{ backgroundColor: "#0b57a4", color: "#FFF", borderRadius: "8px 8px 8px 8px" }}>{/*   */}
                                                                    <Box className="col-12 ">
                                                                        <Text size="h6" className="text-center text-white">Available Amount</Text>
                                                                    </Box>
                                                                    <Box className="col-12 ">
                                                                        <Text size="h5" className="text-center text-white" >&#x20B9;{Number(currentScheme?.AvlBalAmt).toLocaleString("en-IN")}</Text>
                                                                    </Box>
                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                        {/* <Box className="row justify-content-center">
                                                            <Box className="col-lg-9 col-md-12 col-sm-12 border-bottom">

                                                                <Text size="h5" className="text-center" css={{ color: "#0861b6" }}>Annual Returns</Text>
                                                            </Box>
                                                            <Box className="col-lg-10">

                                                                <Text size="h5" className="text-center">
                                                                    <b style={{ fontSize: "0.8rem" }}>1 Year:</b>&nbsp;<span>{currentScheme?._1Y}</span>&nbsp;&nbsp;
                                                                    <span className="border-end" style={{ fontSize: "0.8rem" }}></span>&nbsp;&nbsp;
                                                                    <b style={{ fontSize: "0.8rem" }}>3 Year:</b>&nbsp;
                                                                    <span style={{ fontSize: "0.8rem" }}>{currentScheme?._3Y}</span>&nbsp;&nbsp;
                                                                    <span className="border-end"></span>&nbsp;&nbsp;
                                                                    <b style={{ fontSize: "0.8rem" }}>5 Year:</b>&nbsp;<span style={{ fontSize: "0.8rem" }}>{currentScheme?._5Y}</span>
                                                                </Text>
                                                            </Box>
                                                        </Box> */}
                                                        <Box className="row justify-content-center mt-3 mb-1 mx-2">
                                                            {currentScheme?.trxnSource === "External" ? <><Box className={`mb-2 `}></Box></> : <>
                                                                <Button
                                                                    className={`col-auto mb-2 `}
                                                                    color="yellowGroup"
                                                                    size="md"
                                                                    title="purchase"
                                                                    disabled={currentScheme?.isPurAllow === "Y" ? false : true}
                                                                    onClick={() => { setOpenPurchase(true); setSelectedScheme(currentScheme) }}
                                                                >
                                                                    <GroupBox
                                                                        align="center"
                                                                        className={`${style.hovBtn}`}
                                                                    >
                                                                        <Box className="row">
                                                                            <Box className="px-auto col-auto">
                                                                                <Basket2fill />
                                                                            </Box>
                                                                        </Box>
                                                                    </GroupBox>
                                                                </Button>
                                                                <Button
                                                                    className="col-auto mb-2 "
                                                                    color="yellowGroup"
                                                                    size="md"
                                                                    title="Redeem"
                                                                    disabled={currentScheme?.isRedAllow === "Y" ? false : true}
                                                                    onClick={(e: any) => { setOpenRedeem(true); setSelectedScheme(currentScheme) }}
                                                                >
                                                                    <GroupBox
                                                                        align="center"
                                                                        className={`${style.hovBtn}`}
                                                                    >
                                                                        <Box className="row">
                                                                            <Box className="px-auto col-auto">
                                                                                <CartFill />
                                                                            </Box>
                                                                        </Box>
                                                                    </GroupBox>
                                                                </Button>
                                                                <Button
                                                                    className="col-auto mb-2 "
                                                                    color="yellowGroup"
                                                                    size="md"
                                                                    title="SIP"
                                                                    disabled={currentScheme?.isSIPAllow === "Y" ? false : true}
                                                                    onClick={(e: any) => { setOpenSIP(true); setSelectedScheme(currentScheme) }}
                                                                >
                                                                    <GroupBox
                                                                        align="center"
                                                                        className={`${style.hovBtn}`}
                                                                    >
                                                                        <Box className="row">
                                                                            <Box className="px-auto col-auto">
                                                                                <Piggybankfill />
                                                                            </Box>
                                                                        </Box>
                                                                    </GroupBox>
                                                                </Button>
                                                                <Button
                                                                    className="col-auto mb-2 "
                                                                    color="yellowGroup"
                                                                    size="md"
                                                                    title="STP"
                                                                    disabled={currentScheme?.isSTPAllow === "Y" ? false : true}
                                                                    onClick={(e: any) => { setOpenSTP(true); setSelectedScheme(currentScheme) }}
                                                                >
                                                                    <GroupBox
                                                                        align="center"
                                                                        className={`${style.hovBtn}`}
                                                                    >
                                                                        <Box className="row">
                                                                            <Box className="px-auto col-auto">
                                                                                <ArrowLeftRight />
                                                                            </Box>
                                                                        </Box>
                                                                    </GroupBox>
                                                                </Button>
                                                                <Button
                                                                    className="col-auto mb-2 "
                                                                    color="yellowGroup"
                                                                    size="md"
                                                                    title="Switch"
                                                                    disabled={currentScheme?.isSwitchAllow === "Y" ? false : true}
                                                                    onClick={(e: any) => { setOpenSwitch(true); setSelectedScheme(currentScheme) }}
                                                                >
                                                                    <GroupBox
                                                                        align="center"
                                                                        className={`${style.hovBtn}`}
                                                                    >
                                                                        <Box className="row">
                                                                            <Box className="px-auto col-auto">
                                                                                <Shuffle />
                                                                            </Box>
                                                                        </Box>
                                                                    </GroupBox>
                                                                </Button>
                                                                <Button
                                                                    className="col-auto mb-2 "
                                                                    color="yellowGroup"
                                                                    size="md"
                                                                    title="SWP"
                                                                    disabled={currentScheme?.isSWPAllow === "Y" ? false : true}
                                                                    onClick={(e: any) => { setOpenSwp(true); setSelectedScheme(currentScheme) }}
                                                                >
                                                                    <GroupBox
                                                                        align="center"
                                                                        className={`${style.hovBtn}`}
                                                                    >
                                                                        <Box className="row">
                                                                            <Box className="px-auto col-auto">
                                                                                {/* <Bezier /> */}
                                                                                {/* <ArrowDownUp /> */}
                                                                                <HandHoldingUsd />
                                                                            </Box>
                                                                        </Box>
                                                                    </GroupBox>
                                                                </Button>
                                                            </>}
                                                        </Box>
                                                    </Card>
                                                </Box>
                                            );
                                        })}
                                        <Box className="col-lg-4 my-1">
                                            <Card
                                                css={{
                                                    borderRadius: "8px 8px 8px 8px",
                                                    border: "1px solid #82afd9",
                                                }}
                                            >
                                                <Box className="row p-3">
                                                    <Box className="col-12 text-center mb-3">
                                                        Not Satisfied with Recommended Funds?
                                                    </Box>
                                                    <Box className="col-12 text-center">
                                                        <Button
                                                            className="mt-2 px-3 py-1"
                                                            color="yellowGroup"
                                                            size="sm"
                                                            onClick={() => {
                                                                router.push("/SchemeList")
                                                            }}
                                                        >
                                                            <Text
                                                                // @ts-ignore
                                                                size="h6"
                                                                //@ts-ignore
                                                                color="gray8"
                                                                className={style.button}
                                                            >
                                                                Click here to find more Funds
                                                            </Text>
                                                        </Button>
                                                    </Box>
                                                </Box>
                                            </Card>
                                        </Box>
                                    </Box>
                                </Box>
                            </>
                    }
                    <DialogModal
                        open={openPurchase}
                        setOpen={setOpenPurchase}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "50%" },
                        }}
                    >
                        <PurchaseModal selectedData={selectedScheme} setOpenPurchase={setOpenPurchase} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                    </DialogModal>
                    <DialogModal
                        open={openRedeem}
                        setOpen={setOpenRedeem}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "50%" },
                        }}
                    >
                        <RedeemModal selectedData={selectedScheme} setOpenRedeem={setOpenRedeem} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                    </DialogModal>
                    <DialogModal
                        open={openSIP}
                        setOpen={setOpenSIP}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "50%" },
                        }}
                    >
                        <SIPModal selectedData={selectedScheme} setOpenSIP={setOpenSIP} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                    </DialogModal>
                    <DialogModal
                        open={openSTP}
                        setOpen={setOpenSTP}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "50%" },
                        }}
                    >
                        <STPModal selectedData={selectedScheme} setOpenSTP={setOpenSTP} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                    </DialogModal>
                    <DialogModal
                        open={openSwitch}
                        setOpen={setOpenSwitch}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "50%" },
                        }}
                    >
                        <SwitchModal selectedData={selectedScheme} setOpenSwitch={setOpenSwitch} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                    </DialogModal>
                    <DialogModal
                        open={openSwp}
                        setOpen={setOpenSwp}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "50%" },
                        }}
                    >
                        <SwpModal selectedData={selectedScheme} setOpenSwp={setOpenSwp} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                    </DialogModal>
                    <DialogModal
                        open={openSuccess}
                        setOpen={setOpenSuccess}
                        css={{
                            "@bp0": { width: "60%" },
                            "@bp1": { width: "35%" },
                        }}
                    >
                        {/* @ts-ignore */}
                        <Box className="col-md-12 col-sm-12 col-lg-12">
                            <Box className="modal-body modal-body-scroll">
                                <Box className="row justify-content-center">
                                    <Box className="col-lg-12 text-center" css={{ py: 50 }}>
                                        <CheckCircleFill color="orange" size="2rem" />
                                        {/* @ts-ignore */}
                                        <Text size="h5" className="text-center mt-2" css={{ color: "var(--colors-blue1)" }}>{messageTxt}</Text>
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                    </DialogModal>
                </Box>
            </Box>
        </>);
}

export default GoalsInvestmentsPage;