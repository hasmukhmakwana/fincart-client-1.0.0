import Layout from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import GoalsInvestmentsPage from "./components/GoalsInvestmentPage";

const GoalsInvestmentsModule = (...props: any) => {
  return (
    <Layout>
      <PageHeader title={props.title} rightContent={<></>} />
      <GoalsInvestmentsPage />
    </Layout>
  )
}

export default GoalsInvestmentsModule;