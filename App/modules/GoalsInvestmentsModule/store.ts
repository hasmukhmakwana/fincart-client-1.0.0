import create from 'zustand';
import { GoalsInvestedSchemes } from './GoalsInvestmentType';

interface StoreTypes {
    goalsInvestmentList: GoalsInvestedSchemes[];
    setGoalsInvestmentList: (payload: GoalsInvestedSchemes[]) => void;
}

const useGoalInvestmentsStore = create<StoreTypes>((set) => ({
    goalsInvestmentList: [],

    setGoalsInvestmentList: (payload) => set((state) => ({
        ...state,
        goalsInvestmentList: payload
    })),
}))

export default useGoalInvestmentsStore;