import create from "zustand";
import { RecommendationData, RecommendedSchemes, MOHmember, profileListType } from "./RecommendationTypes";

interface StoreTypes {
    recommendedSchemesList: RecommendedSchemes[];
    recommendationDataObj: RecommendationData;
    mohMember: MOHmember[];
    profileList: profileListType[];
    allMemberList: any;
    setAllMemberList: (payload: any) => void;
    setProfileList: (payload: profileListType[]) => void;
    setMOHmember: (payload: MOHmember[]) => void;
    setRecommendedSchemesList: (payload: RecommendedSchemes[]) => void;
    setRecommendationData: (payload: RecommendationData) => void;
}

const useRecommendedSchemesStore = create<StoreTypes>((set) => ({
    recommendedSchemesList: [],
    recommendationDataObj: {
        amount: 0,
        duration: "",
        userGoalId: "",
        section: "GOAL_PLANNING"
    },

    allMemberList: [],
    profileList: [],
    mohMember: [],

    setAllMemberList: (payload) =>
        set((state) => ({
            ...state,
            allMemberList: payload,
        })),
    setProfileList: (payload) =>
        set((state) => ({
            ...state,
            profileList: payload,
        })),
    setMOHmember: (payload) =>
        set((state) => ({
            ...state,
            mohMember: payload,
        })),

    setRecommendedSchemesList: (payload) =>
        set((state) => ({
            ...state,
            recommendedSchemesList: payload || [],
        })),

    setRecommendationData: (payload) =>
        set((state) => ({
            ...state,
            recommendationDataObj: payload
        }))

}));

export default useRecommendedSchemesStore;