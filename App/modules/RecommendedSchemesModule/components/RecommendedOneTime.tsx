import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import SelectMenu from "@ui/Select/Select";
import { useEffect, useState } from "react";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import style from "../RecommendedSchemes.module.scss";
import { encryptData, getUser } from "App/utils/helpers";
import useRecommendedSchemesStore from "../store";
import { toastAlert } from "App/shared/components/Layout";
import { addingTocart } from "App/api/cart";
import { Formik } from "formik";
import create from "zustand";
import * as Yup from "yup";
import CartFill from "App/icons/CartFill";
import StarFill from "App/icons/StarFill";
import { fetchAllInvestments } from "App/api/recommendedSchemes";
import Loader from "App/shared/components/Loader";
import useHeaderStore from "App/shared/components/Header/store";
import { getSLInvestorDD } from 'App/api/schemeList';

export type VerifyPurchaseTypes = {
    // Name: string;
    investor: string;
    investorAccount: string;
    folioNo: string;
    Amount: string;
    // InvestDate: string;
};

interface StoreTypes {
    verifyPay: VerifyPurchaseTypes;
}

type NameEventTypes = {
    schemeData: (value: any) => void;
    setOpenPurchase: (value: boolean) => void;
    setOpenSuccess: (value: boolean) => void;
    setMessageTxt: (value: string) => void;
};

const RecommendedOneTime = ({ schemeData, setOpenPurchase, setOpenSuccess, setMessageTxt }: NameEventTypes) => {
    const {
        mohMember,
        profileList,
        allMemberList,
    } = useRecommendedSchemesStore();

    const { cartCount, setCartCount } = useHeaderStore();

    const useFromStore = create<StoreTypes>((set) => ({
        //* initial state
        verifyPay: {
            // Name: "",
            investor: mohMember[0]?.basicid || "",
            investorAccount: "",
            folioNo: "",
            Amount: "",
            // InvestDate: "",
        }
    }))
    const [load, setLoad] = useState<any>(false);
    const [investorAccountList, setInvestorAccountList] = useState<any>([]);
    const { verifyPay, loading } = useFromStore((state: any) => state);
    const [purchaseData, setPurchaseData] = useState<any>(schemeData);
    const [amountType, setAmountType] = useState<any>("new");
    const [investorId, setInvestorId] = useState<any>(mohMember[0]?.basicid || "");
    const [accountId, setAccountId] = useState<any>("");
    const [loadingCart, setLoadingCart] = useState(false);
    const [loadFol, setLoadFol] = useState(false);
    const [foliosList, setFoliosList] = useState<any>([]);

    const localUser: any = getUser();

    const purchaseModalObj = {
        mandatory: {
            investor: Yup.string()
                .required("Investor is required")
                .trim(),

            investorAccount: Yup.string()
                .required("Investor Account is required")
                .trim(),

            folioNo: Yup.string()
                .required("folio No is required")
                .trim(),
        },
        initialAmount: {
            Amount: Yup.number()
                .required("Amount is required")
                .positive()
                .integer()
                .min(purchaseData?.minAmt, `Min is ${purchaseData?.minAmt}`)
                .max(purchaseData?.maxAmt, `Max is ${purchaseData?.maxAmt}`),
        },
        addedAmount: {
            Amount: Yup.number()
                .required("Amount is required")
                .positive()
                .integer()
                .min(purchaseData?.addminAmt, `Min is ${purchaseData?.addminAmt}`)
                .max(purchaseData?.addMaxAmt, `Max is ${purchaseData?.addMaxAmt}`),
        }

    }

    const [validationSchema, setValidationSchema] = useState(purchaseModalObj.mandatory);

    const fetchAllInvest = async (bid: any, pid: any) => {
        try {
            setLoad(true);
            let obj: any = {
                basicid: bid,
                profileid: pid,
                filtertype: 'ALL'
            }
            let enc: any = encryptData(obj);
            let result: any = await fetchAllInvestments(enc);
            console.log(result?.data);
            let val = result?.data?.filter((i: any) => i.schemeId === purchaseData?.schemeId).length
            if (val === 0) {
                setAmountType("new")
            } else {
                setAmountType("add");
            }
            setLoad(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoad(false);
        }
    }

    const fetchInvestorDD = async (Id: any) => {
        try {
            setLoadFol(true);
            let obj: any = {
                basicId: Id,
                fundid: purchaseData?.fundId
            }
            const enc: any = encryptData(obj);
            const result: any = await getSLInvestorDD(enc);
            setFoliosList(result?.data?.folioList?.filter((i: any) => (i?.basicid === Id)));
            setLoadFol(false);
        } catch (error) {
            setLoadFol(false);
            console.log(error);
            toastAlert("error", error);
        }
    }

    const addToCart = async (val: any) => {

        let bankDetails: any = allMemberList?.bankList.filter((record: any) => record?.basicid === val.investor);

        if (val?.Amount === "") {
            toastAlert("error", "Amount should not be empty while adding to Cart");
        }
        else {
            try {
                setLoadingCart(true);
                let userGoalId = localStorage.getItem("userGoalId");
                let obj =
                {
                    'basicID': val?.investor,
                    'ProfileID': val?.investorAccount,
                    'purSchemeId': purchaseData?.schemeId,
                    'tranType': 'NOR',
                    'Amount': Number(val?.Amount),
                    'No_of_Installment': null,
                    'MDate': null,
                    'PurFolioNo': val?.folioNo,
                    'bankId': bankDetails[0]?.bankid,
                    'userGoalId': userGoalId,
                    'startDate': null
                }

                const enc: any = encryptData(obj);
                const result: any = await addingTocart(enc);
                setOpenPurchase(false);
                setMessageTxt("Purchase of ₹" + val?.Amount + " is added to Cart Successfully")
                setOpenSuccess(true);
                setLoadingCart(false);
                let iCartCount = cartCount + 1;
                setCartCount(1);
            }
            catch (error) {
                setLoadingCart(false);
                console.log(error);
                toastAlert("error", error);
            }
        }
    }

    useEffect(() => {
        let arr = profileList.filter((item: any) => {
            return item?.basicid === investorId;
        });
        fetchInvestorDD(investorId);
        setInvestorAccountList(arr);
    }, [investorId])


    useEffect(() => {
        (async () => {
            if (investorId !== "" && accountId !== "") {
                await fetchAllInvest(investorId, accountId);
            }
        })();
        return () => { }
    }, [investorId, accountId])

    useEffect(() => {
        let obj = purchaseModalObj.mandatory;
        if (amountType === "add") {
            obj = {
                ...obj,
                ...purchaseModalObj.addedAmount,
            }
        } else {
            obj = {
                ...obj,
                ...purchaseModalObj.initialAmount,
            }
        }
        setValidationSchema(obj)

        return () => { }
    }, [amountType])


    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}>Purchase</Text>
                </Box>

                <Box className="modal-body p-3">
                    <Formik
                        initialValues={verifyPay}
                        validationSchema={Yup.object().shape(validationSchema)}
                        enableReinitialize
                        onSubmit={(values, { resetForm }) => {
                            addToCart(values);
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount
                        }) => (
                            <>
                                <Box className="row border-bottom border-warning mx-1" >
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text className="text-capitalize" >{purchaseData?.schemeName} </Text>
                                            </Box>
                                            <Box className="col-auto pt-0" >
                                                {
                                                    <>
                                                        {
                                                            (() => {
                                                                const arr = [];
                                                                for (let i = 0; i < purchaseData?.rating; i++) {
                                                                    arr.push(
                                                                        <StarFill color="orange" className="me-1" />
                                                                    );
                                                                }
                                                                return arr;
                                                            })()
                                                        }
                                                    </>
                                                }
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="col-12 py-2">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{purchaseData?.obj}</Text>
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{purchaseData?.subObj}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="row mt-2 mx-1 ">
                                    <Box className="col-6">
                                        <SelectMenu
                                            name='investor'
                                            value={values.investor}
                                            items={mohMember}
                                            bindValue={"basicid"}
                                            bindName={"memberName"}
                                            label={"Investor"}
                                            placeholder={"Select"}
                                            onChange={(e: any) => {
                                                // console.log(e?.basicid);
                                                setFieldValue("investor", e?.basicid);
                                                setInvestorId(e?.basicid)
                                                setFieldValue("folioNo", "");
                                            }}
                                            required
                                            // @ts-ignore
                                            error={submitCount ? errors.investor : null}
                                        />
                                    </Box>
                                    <Box className="col-6">
                                        <SelectMenu
                                            name='investorAccount'
                                            value={values.investorAccount}
                                            items={investorAccountList}
                                            bindValue={"profileID"}
                                            bindName={"InvestProfile"}
                                            label={"Investor Account"}
                                            placeholder={"Select"}
                                            onChange={(e: any) => {
                                                console.log(e?.profileID);
                                                setFieldValue("investorAccount", e?.profileID);
                                                setAccountId(e?.profileID)
                                            }}
                                            required
                                            // @ts-ignore
                                            error={submitCount ? errors.investorAccount : null}
                                        />
                                    </Box>
                                    <Box className="col-6">
                                        <SelectMenu
                                            name='folioNo'
                                            value={values.folioNo}
                                            items={foliosList}
                                            bindValue={"folioNo"}
                                            bindName={"folioNo"}
                                            disabled={loadFol}
                                            label={`Folio ${loadFol ? ": loading.." : ""}`}
                                            placeholder={"Select"}
                                            onChange={(e: any) => {
                                                setFieldValue("folioNo", e?.folioNo);
                                            }}
                                            required
                                            // @ts-ignore
                                            error={submitCount ? errors.folioNo : null}
                                        />
                                    </Box>

                                    <Box className="col-6">
                                        <Input
                                            label="Enter Amount"
                                            name="Amount"
                                            placeholder="Enter Amount"
                                            className="border border-2"
                                            //@ts-ignore
                                            error={submitCount ? errors.Amount : null}
                                            onChange={(e: any) => { setFieldValue("Amount", e?.target?.value.length === 1 && e?.target?.value === "0" ? "" : e?.target?.value.replace(/\D/g, '')) }}
                                            value={values?.Amount}
                                            autocomplete="off"
                                        />
                                    </Box>
                                    <Box className="col-auto">
                                        {load ? <><Loader /></> : <></>}
                                    </Box>
                                </Box>

                                <Box className="row justify-content-end ms-1 pb-1">
                                    <Box className="col-auto ">
                                        <Button
                                            type="button"
                                            css={{ backgroundColor: "orange" }}
                                            onClick={handleSubmit}
                                            disabled={loadingCart || loadFol}
                                        >
                                            <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                {loadingCart ? <>Loading... </> : <><CartFill className="me-1" /> Add to Cart </>}
                                            </Text>
                                        </Button>
                                    </Box>
                                </Box>
                            </>
                        )}</Formik>
                </Box>
            </Box>
        </>
    )
}

export default RecommendedOneTime;