import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import Input from "@ui/Input";
import Text from "@ui/Text/Text";
import React, { useState, useEffect, ReactNode } from "react";
import Radio from "@ui/Radio/Radio";
import SelectMenu from "@ui/Select/Select";
import { RadioGroup } from "@radix-ui/react-radio-group";
import { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Loader from "App/shared/components/Loader";
import style from "../RecommendedSchemes.module.scss";
import ViewModal from "@ui/ModalDialog/ModalDialog";
import useRecommendedSchemesStore from "../store";
import { encryptData, getUser } from "App/utils/helpers";
import { getRecommendedSchemes } from "App/api/recommendedSchemes";
import { Risk } from "../RecommendationTypes";
import StarFill from "App/icons/StarFill";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import RecommendedOneTime from "./RecommendedOneTime";
import RecommendedMonthly from "./RecommendedMonthly";
import router from "next/router";
import CheckCircleFill from "App/icons/CheckCircleFill";
import { GoalsSections } from "App/utils/constants";
import useHeaderStore from "App/shared/components/Header/store";
import useSchemeStore from "App/modules/SchemeListModule/store";
import SchemeDetails from "App/modules/SchemeListModule/components/SchemeDetails"


const RecommendedSchemesPage = () => {
  const user: any = getUser();
  const [loader, setLoader] = useState<boolean>(false);
  const [currentSection, setCurrentSection] = useState<GoalsSections>();
  const [riskSelected, setRiskSelection] = useState<Risk>("M");
  const [schemeName, setSchemeName] = useState("");
  const [openModal, setOpenModal] = useState(false);
  const [openPurchaseModal, setOpenPurchaseModal] = useState(false);
  const [openSIPModal, setOpenSIPModal] = useState(false);
  const [openSuccess, setOpenSuccess] = useState(false);
  const [messageTxt, setMessageTxt] = useState<any>("");
  const [selectedData, setSelectedData] = useState<any>([]);
  const [open, setOpen] = useState(false);


  //* functions
  const getStars = (count: number) => {
    count = count > 5 ? 5 : count;
    const start: ReactNode[] = [];
    for (let i = 1; i <= count; i++) {
      start.push(<StarFill height={10} width={10} color="orange" />);
    }
    return start;
  };

  const {
    mohMember,
    recommendedSchemesList,
    setRecommendedSchemesList
  } = useRecommendedSchemesStore();

  const { setSchemeDetails, setSchemeSrchList, schemeSrchList } = useSchemeStore();

  const fetchRecommendationsList = async () => {
    try {

      if (!user?.basicid) return;

      setLoader(true);

      let userGoalId: any = localStorage.getItem("userGoalId");

      let section: any = localStorage.getItem("section");
      setCurrentSection(section);

      let amount: any = localStorage.getItem("amount");

      let duration: any = localStorage.getItem("duration");

      const obj: any = {
        risk: riskSelected,
        amount: amount,
        duration: duration,
        usergoalId: userGoalId,
        basicId: user?.basicid
      }

      const enc: any = encryptData(obj);
      //console.log(enc);
      const result: any = await getRecommendedSchemes(enc);
      setLoader(false);
      console.log(result?.data);
      setRecommendedSchemesList(result?.data);
      setSchemeSrchList(result?.data);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  const assignSchemeDetails = (schemeId: String) => {
    setSchemeDetails(schemeSrchList.filter(e => e.schemeId === schemeId)[0])
  }

  useEffect(() => {
    (async () => {
      await fetchRecommendationsList();
    })();

  }, [riskSelected]);

  return (
    <>
      <Box className="container">
        {currentSection === "TAX_SAVING" ? <></> : <>
          <Box className="row row-cols-1 row-cols-md-2">
            <Box className="col-md-12 col-sm-12 col-lg-12 text-left modal-header p-2" css={{ borderRadius: "8px" }}>
              <RadioGroup
                defaultValue={riskSelected}
                className="inlineRadio"
                onValueChange={(e: any) => {
                  setRiskSelection(e);
                }}
              >
                <Radio value="L" label="Low Risk Funds" id="low" />
                <Radio value="M" label="Moderate Risk Funds" id="moderate" />
                <Radio value="H" label="High Risk Funds" id="high" />
              </RadioGroup>
            </Box>
          </Box></>}
        <Box className="row row-cols-1 row-cols-md-2">
          {
            loader ?
              <Loader />
              : <>
                <Box className="col-md-12 col-sm-12 col-lg-12">
                  <Box className="row">
                    {recommendedSchemesList?.map((currentScheme: any, index: number) => {
                      return (
                        <Box className="col-lg-4 mt-2">
                          <Card
                            css={{
                              borderRadius: "8px 8px 8px 8px",
                              border: "1px solid #82afd9",
                            }}
                          >
                            <Box className="row">
                              <Box className="col-9">
                                {/* @ts-ignore */}
                                <Text size="h5" weight="bold" css={{ fontSize: "0.8rem", cursor: "pointer" }} onClick={(e: any) => { assignSchemeDetails(currentScheme?.schemeId), setOpen(true) }} >{currentScheme?.schemeName}</Text>
                                {
                                  <>
                                    {
                                      (() => {
                                        const arr = [];
                                        for (let i = 0; i < currentScheme?.rating; i++) {
                                          arr.push(
                                            <StarFill color="orange" className="me-1" />
                                          );
                                        }
                                        return arr;
                                      })()
                                    }
                                  </>
                                }

                                {/* @ts-ignore */}
                                <Text size="h6" className="mt-1" css={{ color: "var(--colors-blue1)" }}>
                                  {currentScheme?.obj} - {currentScheme?.subObj}
                                </Text>
                              </Box>
                              <Box className="col-3 text-end">
                                <img
                                  width={40}
                                  height={40}
                                  src={currentScheme?.goalImg}
                                  alt="logo"
                                />
                                <span style={{ fontSize: "0.8rem" }}>{currentScheme?.goalName}</span>
                              </Box>
                            </Box>
                            <Box className="row justify-content-center">
                              <Box className="col-lg-6 col-md-5 col-sm-5">
                                <Box className="row mx-1 px-0 mx-auto mb-2" css={{ backgroundColor: "#0b57a4", color: "#FFF", borderRadius: "8px 8px 8px 8px" }}>
                                  <Box className="col-12 px-0">
                                    {/* @ts-ignore */}
                                    <Text size="h6" className="text-center text-white">AUM</Text>
                                  </Box>
                                  <Box className="col-12 px-0">
                                    {/* @ts-ignore */}
                                    <Text size="h6" className="text-center text-white" >&#x20B9;{currentScheme?.aum}</Text>
                                  </Box>
                                </Box>
                              </Box>
                            </Box>
                            <Box className="row justify-content-center">
                              <Box className="col-lg-9 col-md-12 col-sm-12 border-bottom">
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-center" css={{ color: "#0861b6" }}>Annual Returns</Text>
                              </Box>
                              <Box className="col-lg-10">
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-center">
                                  <b style={{ fontSize: "0.8rem" }}>1 Year:</b>&nbsp;<span>{currentScheme?._1Y}</span>&nbsp;&nbsp;
                                  <span className="border-end" style={{ fontSize: "0.8rem" }}></span>&nbsp;&nbsp;
                                  <b style={{ fontSize: "0.8rem" }}>3 Year:</b>&nbsp;
                                  <span style={{ fontSize: "0.8rem" }}>{currentScheme?._3Y}</span>&nbsp;&nbsp;
                                  <span className="border-end"></span>&nbsp;&nbsp;
                                  <b style={{ fontSize: "0.8rem" }}>5 Year:</b>&nbsp;<span style={{ fontSize: "0.8rem" }}>{currentScheme?._5Y}</span>
                                </Text>
                              </Box>
                            </Box>
                            <Box className="row justify-content-end mx-1">
                              {currentSection === "QUICK_SIP" ?
                                <>
                                  <Button
                                    className="mt-2 px-3 py-1"
                                    color="yellowGroup"
                                    size="xs"
                                    onClick={() => {
                                      if (mohMember[0]?.isNominee) {
                                        setSchemeName(currentScheme?.schemeName);
                                        setOpenSIPModal(true);
                                        setSelectedData(currentScheme);
                                      } else {
                                        setOpenModal(true);
                                      }
                                    }}
                                  >
                                    <Text
                                      // @ts-ignore
                                      size="h6"
                                      //@ts-ignore
                                      color="gray8"
                                      className={style.button}
                                    >
                                      SIP
                                    </Text>
                                  </Button>
                                </>
                                : currentSection === "TAX_SAVING" ?
                                  <>
                                    <Button
                                      className="mt-2 px-3 py-1"
                                      color="yellowGroup"
                                      size="xs"
                                      onClick={() => {
                                        if (mohMember[0]?.isNominee) {
                                          setSchemeName(currentScheme?.schemeName);
                                          setOpenPurchaseModal(true);
                                          setSelectedData(currentScheme);
                                        } else {
                                          setOpenModal(true);
                                        }
                                      }}
                                    >
                                      <Text
                                        // @ts-ignore
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={style.button}
                                      >
                                        Purchase
                                      </Text>
                                    </Button>
                                  </> :
                                  <>
                                    <Button
                                      className="mt-2 px-3 py-1"
                                      color="yellowGroup"
                                      size="xs"
                                      onClick={() => {
                                        if (mohMember[0]?.isNominee) {
                                          setSchemeName(currentScheme?.schemeName);
                                          setOpenPurchaseModal(true);
                                          setSelectedData(currentScheme);
                                        } else {
                                          setOpenModal(true);
                                        }
                                      }}
                                    >
                                      <Text
                                        // @ts-ignore
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={style.button}
                                      >
                                        Purchase
                                      </Text>
                                    </Button>
                                    <Button
                                      className="mt-2 px-3 py-1"
                                      color="yellowGroup"
                                      size="xs"
                                      onClick={() => {
                                        if (mohMember[0]?.isNominee) {
                                          setSchemeName(currentScheme?.schemeName);
                                          setOpenSIPModal(true);
                                          setSelectedData(currentScheme);
                                        } else {
                                          setOpenModal(true);
                                        }
                                      }}
                                    >
                                      <Text
                                        // @ts-ignore
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={style.button}
                                      >
                                        SIP
                                      </Text>
                                    </Button>
                                  </>
                              }
                            </Box>
                          </Card>
                        </Box>
                      );
                    })}
                    {currentSection === "TAX_SAVING" ? <></> : <>
                      <Box className="col-lg-4">
                        <Card
                          css={{
                            borderRadius: "8px 8px 8px 8px",
                            border: "1px solid #82afd9",
                          }}
                        >
                          <Box className="row p-3">
                            <Box className="col-12 text-center mb-3">
                              Not Satisfied with Recommended Funds?
                            </Box>
                            <Box className="col-12 text-center">
                              <Button
                                className="mt-2 px-3 py-1"
                                color="yellowGroup"
                                size="sm"
                                onClick={() => {
                                  setSchemeSrchList([]);
                                  router.push("/SchemeList")
                                }}
                              >
                                <Text
                                  // @ts-ignore
                                  size="h6"
                                  //@ts-ignore
                                  color="gray8"
                                  className={style.button}
                                >
                                  Click here to find more Funds
                                </Text>
                              </Button>
                            </Box>
                          </Box>
                        </Card>
                      </Box>
                      {/* MODAL */}
                      <ViewModal
                        open={open}
                        setOpen={setOpen}
                        css={{
                          "@bp0": { width: "100%" },
                          "@bp1": { width: "45%" },
                        }}
                      >
                        <SchemeDetails getStars={getStars} />
                      </ViewModal>
                    </>}
                  </Box>
                </Box>
              </>
          }
        </Box>
      </Box>

      <DialogModal
        open={openModal}
        setOpen={setOpenModal}
        css={{
          "@bp0": { width: "70%" },
          "@bp1": { width: "40%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            {/* @ts-ignore */}
            <Text css={{ color: "var(--colors-blue1)" }}> Nominee Warning</Text>
          </Box>
          <Box className="modal-body p-3">
            <Box className="row justify-content-center mx-1 py-1 " >
              <Text>Nominee Details are Mandatory. Please click on Continue, to proceed</Text>
            </Box>
            <Box className="row justify-content-end mx-1 pb-1" >
              <Box className="col-auto px-0 pt-3">
                <Button
                  type="button"
                  css={{ backgroundColor: "orange" }}
                  onClick={() => router.push("/Profile")}

                >
                  <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                    {/* <CartFill className="me-1" /> */}
                    Continue
                  </Text>
                </Button>
              </Box>
            </Box>
          </Box>
        </Box>
      </DialogModal>



      {/* Onetime MODAL */}
      <DialogModal
        open={openPurchaseModal}
        setOpen={setOpenPurchaseModal}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "50%" },
        }}
      >
        <RecommendedOneTime schemeData={selectedData} setOpenPurchase={setOpenPurchaseModal} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
      </DialogModal>

      <DialogModal
        open={openSIPModal}
        setOpen={setOpenSIPModal}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "50%" },
        }}
      >
        <RecommendedMonthly schemeData={selectedData} setOpenSIP={setOpenSIPModal} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
      </DialogModal>

      <DialogModal
        open={openSuccess}
        setOpen={setOpenSuccess}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "35%" },
        }}
      >
        {/* @ts-ignore */}
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-body modal-body-scroll p-3">
            <Box className="row justify-content-center">
              <Box className="col-lg-12 text-center" css={{ py: 50 }}>
                <CheckCircleFill color="orange" size="2rem" />
                {/* @ts-ignore */}
                <Text size="h5" className="text-center mt-2" css={{ color: "var(--colors-blue1)" }}>{messageTxt}</Text>
              </Box>
            </Box>
          </Box>
        </Box>
      </DialogModal>
    </>
  );
};

export default RecommendedSchemesPage;
