import React, { useEffect, useState } from 'react'
import Box from '@ui/Box/Box';
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import Input from "App/ui/Input";
import { StyledTabs, TabsContent, TabsList } from "@ui/Tabs/Tabs.styles";
import Layout, { toastAlert } from "App/shared/components/Layout";
import StarFill from 'App/icons/StarFill';
import { useRouter } from "next/router";
import SelectMenu from '@ui/Select/Select';
import style from "../RecommendedSchemes.module.scss";
import CartFill from 'App/icons/CartFill';
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { encryptData, getUser } from "App/utils/helpers";
import { schemeValidDates } from "App/api/transactions";
import { getMandateList } from "App/api/mandate";
import { sendOTP, validateOTP, investTransaction } from "App/api/transactions";
import { addingToSystematicCart, addingTocart } from "App/api/cart";
// import useSchemeStore from '../../store';
import useRecommendedSchemesStore from "./../store"
import useHeaderStore from 'App/shared/components/Header/store';
import Loader from 'App/shared/components/Loader';
import Piggybankfill from 'App/icons/Piggybankfill';
import { getSLInvestorDD } from 'App/api/schemeList';

export type VerifySIPTypes = {
    investor: string;
    investorAccount: string;
    folioNo: string;
    SIPAmount: string;
    SIPDate: string;
    SIPYears: string;
    SIPMandate: string;
};

interface StoreTypes {
    verifySIP: VerifySIPTypes;
}

type SIPEventTypes = {
    schemeData: (value: any) => void;
    setOpenSIP: (value: boolean) => void;
    setOpenSuccess: (value: boolean) => void;
    setMessageTxt: (value: string) => void;
};

const RecommendedMonthly = ({ schemeData, setOpenSIP, setOpenSuccess, setMessageTxt }: SIPEventTypes) => {
    const user: any = getUser();
    const router = useRouter();
    const {
        mohMember,
        profileList,
        allMemberList
    } = useRecommendedSchemesStore();

    const [investorId, setInvestorId] = useState<any>(mohMember[0]?.basicid || "");
    const [investorAccountList, setInvestorAccountList] = useState<any>([]);
    const [purchaseData, setPurchaseData] = useState<any>(schemeData);
    const [loadingCart, setLoadingCart] = useState(false);
    const [loadMan, setLoadMan] = useState(false);
    const [showInvest, setShowInvest] = useState(false);
    const [investStatus, setInvestStatus] = useState(false);
    const [sipDates, setSipDates] = useState<any>([]);
    const [showOtpPart, setShowOtpPart] = useState(false);
    const [btnStateOTP, setBtnStateOTP] = useState(false);
    const [isMandate, setIsMandate] = useState(false);
    const [activeMandates, setActiveMandates] = useState<any>([]);
    const [foliosList, setFoliosList] = useState<any>([]);

    const { cartCount, setCartCount } = useHeaderStore();

    const useSIPStore = create<StoreTypes>((set) => ({
        //* initial state
        verifySIP: {
            investor: mohMember[0]?.basicid || "",
            investorAccount: "",
            folioNo: "",
            SIPAmount: "",
            SIPDate: "",
            SIPYears: "",
            SIPMandate: "",
        }
    }))
    const {
        verifySIP,
        loading,
        moh
    } = useSIPStore((state: any) => state);


    const SIPModalObj: any = {
        mandatory: {
            investor: Yup.string()
                .required("Investor is required")
                .trim(),

            investorAccount: Yup.string()
                .required("Investor Account is required")
                .trim(),

            folioNo: Yup.string()
                .required("folio No is required")
                .trim(),

            SIPAmount: Yup.number()
                .required("Amount is required")
                .positive()
                .integer()
                .min(purchaseData?.sipminAmt, `Min is ${purchaseData?.sipminAmt}`)
                .max(purchaseData?.sipmaxAmt, `Max is ${purchaseData?.sipmaxAmt}`),

            SIPDate: Yup.string()
                .required("Date is required"),

            SIPYears: Yup.string()
                .required("Years is required"),
        },
        invest: {
            SIPMandate: Yup.string()
                .required("Mandate is required"),
        }
    };

    const [validationSchema, setValidationSchema] = useState(
        SIPModalObj.mandatory
    );

    const fetchInvestorDD = async (Id: any) => {
        try {
            setLoadMan(true);
            let obj: any = {
                basicId: Id,
                fundid: purchaseData?.fundId
            }
            const enc: any = encryptData(obj);
            const result: any = await getSLInvestorDD(enc);
            setFoliosList(result?.data?.folioList?.filter((i: any) => (i?.basicid === Id)));
            // console.log(result?.data?.folioList?.filter((i: any) => (i?.basicid === Id)))
            setLoadMan(false);
        } catch (error) {
            setLoadMan(false);
            console.log(error);
            toastAlert("error", error);
        }
    }

    const getEndDate = (selectedDate:any, selectedYear:any) => {
        const d = new Date(selectedDate);
        var newDate = new Date(d.setMonth(d.getMonth() + selectedYear));
        let ed = `${newDate.getFullYear()}-${
          newDate.getMonth() + 1 < 10
            ? '0' + (newDate.getMonth() + 1)
            : newDate.getMonth() + 1
        }-${newDate.getDate() < 10 ? '0' + newDate.getDate() : newDate.getDate()}`;
        return ed;
      };

    const addToCart = async (val: any) => {
        // console.log(val);
        let bankDetails: any = allMemberList?.bankList.filter((record: any) => record?.basicid === val.investor);
        // console.log(bankDetails[0].bankid, "bankDetails")
        if (val?.SIPAmount === "") {
            toastAlert("error", "Amount should not be empty while adding to Cart");
        }
        else {
            setLoadingCart(true);

            if (activeMandates.length === 0) {
                try {
                    let mDate = new Date(val?.SIPDate);
                    let userGoalId = localStorage.getItem("userGoalId");
                    // let goalid=purchaseData?.goalList[0]===undefined?"0":purchaseData?.goalList[0].userGoalId;
                    let obj =
                    {
                        'basicID': val?.investor,
                        'ProfileID': val?.investorAccount,
                        'purSchemeId': purchaseData?.schemeId,
                        'tranType': 'ISIP',
                        'Amount': val?.SIPAmount,
                        'No_of_Installment': parseInt(val?.SIPYears),
                        'MDate': `${mDate.getDate()}`,
                        'PurFolioNo': val?.folioNo,
                        'bankId': bankDetails[0]?.bankid,//purchaseData?.bankId,
                        'userGoalId': userGoalId,
                        'startDate': val?.SIPDate
                    }

                    const enc: any = encryptData(obj);
                    await addingTocart(enc);
                    setOpenSIP(false);
                    setLoadingCart(false);
                    setMessageTxt("SIP of ₹" + val?.SIPAmount + " is added to Cart Successfully")
                    let iCartCount = cartCount + 1;
                    setCartCount(1);
                    setOpenSuccess(true);
                }
                catch (error) {
                    setLoadingCart(false);
                    console.log(error);
                    toastAlert("error", error);
                }
            }
            else {
                try {
                    let dt = new Date();
                    let userGoalId = localStorage.getItem("userGoalId");
                    let mDate = new Date(val?.SIPDate);
                    let endDt = getEndDate(val?.SIPDate, val?.SIPYears);
                    let obj = {
                        basicid: user?.basicid,
                        CartData: [
                            {
                                "PurFolioNo": val?.folioNo,
                                "fromSchemeId": purchaseData?.schemeId,
                                "sellFolioNo": "",
                                "toSchemeID": "",
                                "amount": val?.SIPAmount,
                                "startDate": val?.SIPDate,
                                "endDate": endDt,
                                "No_of_Installment": val?.SIPYears,
                                "MDate": `${mDate.getDate()}`,
                                "Units": "0",
                                "bankId": bankDetails[0]?.bankid,
                                "accountNo": bankDetails[0]?.accountNo,
                                "goalCode": purchaseData?.goalCode,
                                "tranType": "SIP",
                                "trxnTypeId": "12",
                                "userGoalid": userGoalId,
                                "mandateID": val?.SIPMandate,//apply
                                "trxnDate": `${dt.getFullYear()}-${dt.getMonth() < 9 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1)}-${dt.getDate()}`,
                                "profileId": val?.investorAccount,
                                "remark": "",
                                "bankName": bankDetails[0]?.bankName,
                                "empCode": "",
                                "profileName": user?.name
                            }
                        ]
                    }
                    //console.log(obj)
                    const enc: any = encryptData(obj);
                    await addingToSystematicCart(enc);
                    setOpenSIP(false);
                    setLoadingCart(false);
                    setMessageTxt("SIP of ₹" + val?.SIPAmount + " is added to Cart Successfully")
                    setOpenSuccess(true);
                }
                catch (error) {
                    setLoadingCart(false);
                    console.log(error);
                    toastAlert("error", error);
                }
            }
        }
    }

    const SIPFun = async (e: any) => {
        // clickOnSubmit(e);
        // console.log(e);
        let userGoalId = localStorage.getItem("userGoalId");
        let bankDetails: any = allMemberList?.bankList.filter((record: any) => record?.basicid === e.investor);
        if (showOtpPart === false || e?.otp === undefined || e?.otp === "") {
            try {
                setBtnStateOTP(true);
                let obj = {
                    'userid': user?.userid,
                    'mobile': user?.mobile,
                    'name': user?.name,
                    'txntype': "SIP",
                    'purscheme': purchaseData?.schemeName,
                    'sellscheme': "",
                    'rmname': user?.RmName,
                    'amt': e?.SIPAmount
                }
                // console.log(obj);

                const enc: any = encryptData(obj);
                // console.log(enc);
                const result: any = await sendOTP(enc);
                toastAlert("success", "OTP Sent Successfully");
                setShowOtpPart(true);
                setBtnStateOTP(false);
            }
            catch (error) {
                console.log(error);
                setBtnStateOTP(false);
                toastAlert("error", error);
            }
        }
        else if (showOtpPart === true) {
            try {
                setBtnStateOTP(true);
                let dt = new Date();
                let mDate = new Date(e?.SIPDate);
                // console.log(mDate.getDate());

                const enc: any = encryptData(e?.otp, true);
                // console.log(enc);
                const result: any = await validateOTP(enc);
                if (result?.status === "Success") {
                    let obj1 = {
                        "id": '0',
                        "basicID": e?.investor,
                        "ProfileID": e?.investorAccount,
                        "purSchemeId": purchaseData?.schemeId,
                        "sellSchemeId": "",
                        "tranType": "SIP",
                        "PurFolioNo": e?.folioNo,
                        "SellFolioNo": "",
                        "Amount": e?.SIPAmount,
                        "No_of_Installment": e?.SIPYears,
                        "MDate": `${mDate.getDate()}`,
                        "userGoalId": userGoalId,
                        "Units": "0",
                        "bankId": bankDetails[0]?.bankid,
                        "mandateId": e?.SIPMandate,
                        "startDate": e?.SIPDate
                    }
                    const enc1: any = encryptData(obj1);
                    await investTransaction(enc1);
                    setBtnStateOTP(false);
                    setOpenSIP(false);
                    setMessageTxt("SIP of ₹" + e?.SIPAmount + " is registered from '" + purchaseData?.schemeName)
                    setOpenSuccess(true);
                }
            }
            catch (error) {
                console.log(error);
                setBtnStateOTP(false);
                toastAlert("error", error);
            }
        }
    };

    const yearInstallments = [
        { id: "12", name: "1 Years" },
        { id: "18", name: "1.5 Years" },
        { id: "24", name: "2 Years" },
        { id: "30", name: "2.5 Years" },
        { id: "36", name: "3 Years" },
        { id: "42", name: "3.5 Years" },
        { id: "48", name: "4 Years" },
        { id: "54", name: "4.5 Years" },
        { id: "60", name: "5 Years" },
        { id: "66", name: "5.5 Years" },
        { id: "72", name: "6 Years" },
        { id: "78", name: "6.5 Years" },
        { id: "84", name: "7 Years" },
        { id: "90", name: "7.5 Years" },
        { id: "96", name: "8 Years" },
        { id: "102", name: "8.5 Years" },
        { id: "108", name: "9 Years" },
        { id: "114", name: "9.5 Years" },
        { id: "120", name: "10 Years" },
        { id: "126", name: "10.5 Years" },
        { id: "132", name: "11 Years" },
        { id: "138", name: "11.5 Years" },
        { id: "144", name: "12 Years" },
        { id: "150", name: "12.5 Years" },
        { id: "156", name: "13 Years" },
        { id: "162", name: "13.5 Years" },
        { id: "168", name: "14 Years" },
        { id: "174", name: "14.5 Years" },
        { id: "180", name: "15 Years" },
        { id: "186", name: "15.5 Years" },
        { id: "192", name: "16 Years" },
        { id: "198", name: "16.5 Years" },
        { id: "204", name: "17 Years" },
        { id: "210", name: "17.5 Years" },
        { id: "216", name: "18 Years" },
        { id: "222", name: "18.5 Years" },
        { id: "228", name: "19 Years" },
        { id: "234", name: "19.5 Years" },
        { id: "240", name: "20 Years" },
        { id: "246", name: "20.5 Years" },
        { id: "252", name: "21 Years" },
        { id: "258", name: "21.5 Years" },
        { id: "264", name: "22 Years" },
        { id: "270", name: "22.5 Years" },
        { id: "276", name: "23 Years" },
        { id: "282", name: "23.5 Years" },
        { id: "288", name: "24 Years" },
        { id: "294", name: "24.5 Years" },
        { id: "300", name: "25 Years" },
        { id: "306", name: "25.5 Years" },
        { id: "312", name: "26 Years" },
        { id: "318", name: "26.5 Years" },
        { id: "324", name: "27 Years" },
        { id: "330", name: "27.5 Years" },
        { id: "336", name: "28 Years" },
        { id: "342", name: "28.5 Years" },
        { id: "348", name: "29 Years" },
        { id: "354", name: "29.5 Years" },
        { id: "360", name: "30 Years" },
        { id: "366", name: "30.5 Years" },
        { id: "372", name: "31 Years" },
        { id: "378", name: "31.5 Years" },
        { id: "384", name: "32 Years" },
        { id: "390", name: "32.5 Years" },
        { id: "396", name: "33 Years" },
        { id: "402", name: "33.5 Years" },
        { id: "408", name: "34 Years" },
        { id: "414", name: "34.5 Years" },
        { id: "420", name: "35 Years" },
        { id: "426", name: "35.5 Years" },
        { id: "432", name: "36 Years" },
        { id: "438", name: "36.5 Years" },
        { id: "444", name: "37 Years" },
        { id: "450", name: "37.5 Years" },
        { id: "456", name: "38 Years" },
        { id: "462", name: "38.5 Years" },
        { id: "468", name: "39 Years" },
        { id: "474", name: "39.5 Years" },
        { id: "480", name: "40 Years" },
        { id: "486", name: "40.5 Years" },
        { id: "492", name: "41 Years" },
        { id: "498", name: "41.5 Years" },
        { id: "504", name: "42 Years" },
        { id: "510", name: "42.5 Years" },
        { id: "516", name: "43 Years" },
        { id: "522", name: "43.5 Years" },
        { id: "528", name: "44 Years" },
        { id: "534", name: "44.5 Years" },
        { id: "540", name: "45 Years" },
        { id: "546", name: "45.5 Years" },
        { id: "552", name: "46 Years" },
        { id: "558", name: "46.5 Years" },
        { id: "564", name: "47 Years" },
        { id: "570", name: "47.5 Years" },
        { id: "576", name: "48 Years" },
        { id: "582", name: "48.5 Years" },
        { id: "588", name: "49 Years" },
        { id: "594", name: "49.5 Years" },
        { id: "600", name: "50 Years" },
    ];

    const fetchActiveMandates = async (basicID: any) => {
        try {
            setLoadMan(true);
            const enc: any = encryptData(basicID, true);
            // console.log(enc);
            const result: any = await getMandateList(enc);
            let ac: any[] = [];
            // console.log(result?.data?.MandateList)
            let manList = result?.data?.MandateList;
            if (manList.length !== 0) {

                manList.map((ele: any) => {
                    if (ele.Active === "Y")
                        ac.push(ele);
                })
                if (ac.length > 0) {
                    setShowInvest(true);
                }
                setActiveMandates(ac);
                setIsMandate(true);
            } else {
                setShowInvest(false);
                setIsMandate(false);
            }
            setLoadMan(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoadMan(false);
        }
    }

    const fetchSchemeValidDates = async () => {
        // console.log(purchaseData);
        try {
            const obj: any = {
                schemeId: purchaseData?.schemeId,
                mandateStatus: "Y",
                trxnType: "SIP",
            };
            const enc: any = encryptData(obj);
            // console.log(obj);
            const result: any = await schemeValidDates(enc);
            setSipDates(result?.data)
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
        }
    }
    useEffect(() => {
        // console.log(purchaseData);
        fetchSchemeValidDates();
        // fetchActiveMandates();
        // console.log(allMemberList)
    }, [])

    useEffect(() => {
        let arr = profileList.filter((item: any) => {
            return item?.basicid === investorId;
        });
        // console.log(arr, "arr");
        // console.log(investorId, "investorId")
        fetchActiveMandates(investorId);
        fetchInvestorDD(investorId);
        setInvestorAccountList(arr);
    }, [investorId])

    useEffect(() => {
        let obj = SIPModalObj.mandatory;
        if (showInvest) {
            obj = {
                ...obj,
                ...SIPModalObj.invest
            }
        }
        setValidationSchema(obj);
        return () => { }
    }, [showInvest])

    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}> SIP Details</Text>
                </Box>
                <Box className="modal-body p-3">
                    <Formik
                        initialValues={verifySIP}
                        validationSchema={Yup.object().shape(validationSchema)}
                        enableReinitialize
                        onSubmit={(values, { resetForm }) => {
                            // console.log(values)
                            // addToCart(values);
                            if (investStatus) {
                                // console.log(values, "SIPFun");
                                SIPFun(values);
                            } else {
                                // console.log(values, "addToCart");
                                addToCart(values)
                            }
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount
                        }) => (
                            <>
                                <Box className="row border-bottom border-warning mx-1" >
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text className="text-capitalize" >{purchaseData?.schemeName} </Text>
                                            </Box>
                                            <Box className="col-auto pt-0">
                                                {
                                                    <>
                                                        {
                                                            (() => {
                                                                const arr = [];
                                                                for (let i = 0; i < purchaseData?.rating; i++) {
                                                                    arr.push(
                                                                        <StarFill color="orange" className="me-1" />
                                                                    );
                                                                }
                                                                return arr;
                                                            })()
                                                        }
                                                    </>
                                                }
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="col-12 py-2">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{purchaseData?.obj}</Text>
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{purchaseData?.subObj}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>

                                <Box className="row mt-2 mx-1">
                                    {!showOtpPart ?
                                        <Box className="col-12">
                                            <Box className="row">

                                            </Box>
                                            <Box className="row">
                                                <Box className="col-6">
                                                    <SelectMenu
                                                        name='investor'
                                                        value={values.investor}
                                                        items={mohMember}
                                                        bindValue={"basicid"}
                                                        bindName={"memberName"}
                                                        label={"Investor"}
                                                        placeholder={"Select"}
                                                        onChange={(e: any) => {
                                                            setFieldValue("investor", e?.basicid);
                                                            setInvestorId(e?.basicid);
                                                            setFieldValue("folioNo", "");
                                                        }}
                                                        required
                                                        // @ts-ignore
                                                        error={submitCount ? errors.investor : null}
                                                    />
                                                </Box>
                                                <Box className="col-6">
                                                    <SelectMenu
                                                        name='investorAccount'
                                                        value={values.investorAccount}
                                                        items={investorAccountList}
                                                        bindValue={"profileID"}
                                                        bindName={"InvestProfile"}
                                                        label={"Investor Account"}
                                                        disabled={!isMandate}
                                                        placeholder={"Select"}
                                                        onChange={(e: any) => {
                                                            setFieldValue("investorAccount", e?.profileID);
                                                        }}
                                                        required
                                                        // @ts-ignore
                                                        error={submitCount ? errors.investorAccount : null}
                                                    />
                                                </Box>
                                                <Box className="col-6">
                                                    <SelectMenu
                                                        name='folioNo'
                                                        value={values.folioNo}
                                                        items={foliosList}
                                                        bindValue={"folioNo"}
                                                        bindName={"folioNo"}
                                                        disabled={!isMandate}
                                                        label={`Folio ${loadMan ? ": loading.." : ""}`}
                                                        placeholder={"Select"}
                                                        onChange={(e: any) => {
                                                            setFieldValue("folioNo", e?.folioNo);
                                                        }}
                                                        required
                                                        // @ts-ignore
                                                        error={submitCount ? errors.folioNo : null}
                                                    />
                                                </Box>
                                                <Box className="col-6">
                                                    <Input
                                                        //  type="number"
                                                        label="Enter Amount"
                                                        name="SIPAmount"
                                                        placeholder="Enter Amount"
                                                        className="border border-2"
                                                        disabled={!isMandate}
                                                        // @ts-ignore
                                                        error={submitCount ? errors.SIPAmount : null}
                                                        onChange={(e: any) => { setFieldValue("SIPAmount", e?.target?.value.length === 1 && e?.target?.value === "0" ? "" : e?.target?.value.replace(/\D/g, '')) }}
                                                        value={values?.SIPAmount}
                                                        autocomplete="off"
                                                        required
                                                    />
                                                </Box>
                                                <Box className="col-6">
                                                    <SelectMenu
                                                        name='SIPDate'
                                                        value={values.SIPDate}
                                                        items={sipDates}
                                                        bindValue={"Date"}
                                                        bindName={"Date"}
                                                        disabled={!isMandate}
                                                        label={"Date"}
                                                        placeholder={"Select"}
                                                        onChange={(e: any) => {
                                                            setFieldValue("SIPDate", e.Date);
                                                        }}
                                                        required
                                                        // @ts-ignore
                                                        error={submitCount ? errors.SIPYears : null}
                                                    />
                                                </Box>
                                                {/* </Box>
                                            <Box className="row"> */}
                                                <Box className="col-6">
                                                    <SelectMenu
                                                        name='SIPYears'
                                                        value={values.SIPYears}
                                                        items={yearInstallments}
                                                        bindValue={"id"}
                                                        disabled={!isMandate}
                                                        bindName={"name"}
                                                        label={"No of Years"}
                                                        placeholder={"Select"}
                                                        onChange={(e: any) => {
                                                            setFieldValue("SIPYears", e.id);
                                                        }}
                                                        required
                                                        // @ts-ignore
                                                        error={submitCount ? errors.SIPYears : null}
                                                    />
                                                </Box>
                                                {loadMan ? <Box className="col-6"><Loader /></Box> : <>
                                                    {showInvest ?
                                                        <Box className="col-6">
                                                            <SelectMenu
                                                                label="Select From Current Mandate"
                                                                name="SIPMandate"
                                                                value={values?.SIPMandate}
                                                                items={activeMandates}
                                                                bindValue={"MandateID"}
                                                                bindName={"MandateID"}
                                                                onChange={(e: any) => {
                                                                    setFieldValue("SIPMandate", e.MandateID);
                                                                }}
                                                                required
                                                                // @ts-ignore
                                                                error={submitCount ? errors.SIPMandate : null}
                                                            />
                                                        </Box>
                                                        : ""} </>}
                                            </Box>
                                        </Box>
                                        :
                                        <Box className="col-12">
                                            <Box className="row">
                                                <Box className="col-7">
                                                    <Input
                                                        label="Enter Amount"
                                                        name="SIPAmount"
                                                        className="border border-2"
                                                        value={values?.SIPAmount}
                                                        disabled
                                                    />
                                                </Box>
                                                <Box className="col-7">
                                                    <Input
                                                        label="Enter OTP"
                                                        name="otp"
                                                        value={values?.otp}
                                                        onChange={handleChange}
                                                        className="border border-2"
                                                    />
                                                </Box>
                                            </Box>
                                        </Box>
                                    }
                                </Box>
                                {isMandate ? <>
                                    {/* <Box className="row justify-content-end mx-1 pb-1" >
                                        <Box className="col-auto px-0">
                                            <Button
                                                type="button"
                                                css={{ backgroundColor: "orange" }}
                                                disabled={loadingCart || loadMan}
                                                // onClick={() => { addToCart(values) }}
                                                onClick={handleSubmit}
                                            >
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                    {loadingCart ? <>Loading... </> : <><CartFill className="me-1" /> {showInvest ? "Add to Systematic Cart" : "Add to Cart"} </>}
                                                </Text>
                                            </Button>
                                        </Box>
                                    </Box> */}
                                    <Box className="row justify-content-end" >
                                        <Box className="col-auto px-0">
                                            {showOtpPart ? (<>
                                                <Button
                                                    type="submit"
                                                    css={{ backgroundColor: "orange" }}
                                                    onClick={() => {
                                                        setShowOtpPart(false);
                                                        // setInvestStatus(false);
                                                    }}
                                                    disabled={btnStateOTP}>
                                                    <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                        Back
                                                    </Text>
                                                </Button>
                                            </>
                                            ) : (<>
                                                <Button
                                                    type="button"
                                                    css={{ backgroundColor: "orange" }}
                                                    onClick={handleSubmit}
                                                    disabled={loadingCart || loadMan}
                                                >
                                                    <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                        {loadingCart ? <>Loading... </> : <><CartFill className="me-1" />{showInvest ? "Add to Systematic Cart" : "Add to Cart"}  </>}
                                                    </Text>
                                                </Button>
                                            </>)}
                                        </Box>
                                        <Box className="col-auto">
                                            {showOtpPart ? <Button
                                                type="submit"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={() => {
                                                    setInvestStatus(true);
                                                    handleSubmit()
                                                }}
                                                disabled={btnStateOTP}
                                                loading={loading}
                                            >
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                    {btnStateOTP ? <>Loading... </> : <>Confirm </>} </Text>
                                            </Button>
                                                :
                                                <>
                                                    {showInvest ?
                                                        <Button
                                                            type="submit"
                                                            css={{ backgroundColor: "orange" }}
                                                            onClick={() => {
                                                                setInvestStatus(true);
                                                                handleSubmit()
                                                            }}
                                                            disabled={loadingCart || loadMan}
                                                            loading={loading}
                                                        >
                                                            <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                                {loadingCart ? <>Loading... </> : <><Piggybankfill className="me-2" color="white" /> Initiate SIP </>}
                                                            </Text>
                                                        </Button>
                                                        : ""}
                                                </>
                                            }
                                        </Box>
                                    </Box>
                                </> : <>
                                    <Box className="row justify-content-between" >
                                        <Box className="col-auto ms-4  p-2">
                                            <Text size="h6">
                                                {/* @ts-ignore */}
                                                No mandates found, please click on <b>'Create Mandate'</b> button, to proceed
                                            </Text>
                                        </Box>
                                        <Box className="col-auto px-2">
                                            <Button
                                                type="submit"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={() => {
                                                    router.push("/Mandate/MandateList")
                                                }}
                                                disabled={loadingCart || loadMan}
                                                loading={loading}
                                            >
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                    {loadingCart ? <>Loading... </> : <>Create Mandate </>}
                                                </Text>
                                            </Button>
                                        </Box>
                                    </Box>
                                </>}
                            </>
                        )}</Formik>
                </Box>
            </Box>
        </>
    )
}

export default RecommendedMonthly
