import Holder from 'App/shared/components/Holder/Holder'
import Layout, { toastAlert } from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import { encryptData, getUser } from 'App/utils/helpers'
import React, { useEffect } from 'react'
import RecommendedSchemesPage from './components/RecommendedSchemesPage'
import {
  getAllMemberPurchaseDetails
} from "App/api/mandate"
import useRecommendedSchemesStore from './store'
import useHeaderStore from 'App/shared/components/Header/store'

const RecommendedSchemesModule = () => {
  const local: any = getUser();
  const {
    setMOHmember,
    setProfileList,
    setAllMemberList
  } = useRecommendedSchemesStore();

  const { cartCount } = useHeaderStore();

  const fetchMemberList = async () => {
    try {
      const enc: any = encryptData({
        basicId: local?.basicid,
        fundid: "0",
      });
      console.log(enc);

      let result = await getAllMemberPurchaseDetails(enc);
      console.log(result?.data);
      setAllMemberList(result?.data);
      setMOHmember(result?.data?.memberList);
      setProfileList(result?.data?.profileList);

    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  useEffect(() => {
    (async () => {
      await fetchMemberList();
    })();
  }, [cartCount]);

  return (
    <Layout>
      <PageHeader title='Recommended Schemes' rightContent={<></>} />
      <RecommendedSchemesPage />
    </Layout>
  )
}

export default RecommendedSchemesModule