import { GoalsSections } from "App/utils/constants";

export type Risk = "L" | "M" | "H";

export type RecommendedSchemes = {
    vendorName: string;
    rating: number;
    _1M: number;
    _3M: number;
    _1Y: number;
    _3Y: number;
    _5Y: number;
    minAmt: number;
    maxAmt: number;
    addminAmt: number;
    addMaxAmt: number;
    sipminAmt: number;
    sipmaxAmt: number;
    fundName: string;
    fundImg: string;
    fundId: number;
    goalName: string;
    goalCode: string;
    goalImage: string;
    typeCode: string;
    isPurAllow: string;
    isSIPAllow: string;
    isSwitchAllow: string;
    isSWPAllow: string;
    isRedAllow: string;
    isSTPAllow: string;
    multiples: number;
    schemeId: number;
    schemeName: string;
    orgSchemeName: string;
    obj: string;
    subObj: string;
    status: boolean;
    aum: number;
    section: GoalsSections;
}

export type RecommendationData = {
    userGoalId: string,
    section: GoalsSections,
    amount: number,
    duration: string
}

export type MOHmember = {
    memberName: string;
    basicid: string;
    isNominee: boolean;
    CAF_Status: string;
}

export type profileListType = {
    profileID: string;
    InvestProfile: string;
    basicid: string;
}