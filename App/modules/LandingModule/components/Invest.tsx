import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import React from "react";
import style from "../Landing.module.scss";
const Invest = () => {
  return (
    <Box className="row">
      <Box className="col-lg-12 col-md-12 col-sm-12">
        <Box className="row">
          <Box className="col-lg-6 col-md-6 col-sm-6">
            <Box className={style.nowContainer}>
              <Text
                color="default"
                size="h5"
                weight="semibold"
                css={{ mb: 15 }}
              >
                Invest Now
              </Text>
              <Text size="h7" className={style.investContent}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                hendrerit convallis dolor. Sed scelerisque, hendrerit convallis dolor.
                Sed scelerisque,{" "}
              </Text>
              <Button size="fullwidth" color="yellow">
                View More
              </Button>
            </Box>
          </Box>
          <Box className="col-lg-6 col-md-6 col-sm-6">
            <Box className={style.nowContainer}>
              <Text
                color="default"
                size="h5"
                weight="semibold"
                css={{ mb: 15 }}
              >
                Invest Now
              </Text>
              <Text size="h7" className={style.investContent}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                hendrerit convallis dolor. Sed scelerisque, hendrerit convallis dolor.
                Sed scelerisque,{" "}
              </Text>
              <Button size="fullwidth" color="yellow">
                View More
              </Button>
            </Box>
          </Box>
          <Box className="col-lg-6 col-md-6 col-sm-6">
            <Box className={style.nowContainer}>
              <Text
                color="default"
                size="h5"
                weight="semibold"
                css={{ mb: 15 }}
              >
                Invest Now
              </Text>
              <Text size="h7" className={style.investContent}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                hendrerit convallis dolor. Sed scelerisque, hendrerit convallis dolor.
                Sed scelerisque,{" "}
              </Text>
              <Button size="fullwidth" color="yellow">
                View More
              </Button>
            </Box>
          </Box>
          <Box className="col-lg-6 col-md-6 col-sm-6">
            <Box className={style.nowContainer}>
              <Text
                color="default"
                size="h5"
                weight="semibold"
                css={{ mb: 15 }}
              >
                Invest Now
              </Text>
              <Text size="h7" className={style.investContent}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                hendrerit convallis dolor. Sed scelerisque, hendrerit convallis dolor.
                Sed scelerisque,{" "}
              </Text>
              <Button size="fullwidth" color="yellow">
                View More
              </Button>
            </Box>
          </Box>
        </Box>

      </Box>
    </Box>

  );
};
export default Invest;
