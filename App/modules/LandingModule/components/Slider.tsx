import React from 'react'
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import style from "../Landing.module.scss";
import Box from './../../../ui/Box/Box';

const Slider = () => {
  return (
      <>
    <Box className={style.sliderContainer}>
        <Text color="white" size="h1" weight="semibold" css={{mb:25}} >
          Company Info
        </Text>
        <Text size="h6" color="white" className={style.investContent}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar, sem in egestas efficitur, lorem ligula finibus augue, a pharetra metus velit sed est. Phasellus id mi quis est hendrerit tincidunt. Pellentesque at risus eu enim mattis lobortis. Suspendisse rutrum sollicitudin risus, et imperdiet ante placerat non. Nulla vel rutrum enim. Sed at sapien et sapien hendrerit vestibulum. Curabitur nec orci sed nulla consectetur porttitor commodo eu ligula. Donec eu diam ac massa consequat facilisis a vel lacus. Aenean euismod dolor venenatis, vehicula nunc id, tempor sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas cursus nisl ex, accumsan venenatis ligula dignissim sit amet. Proin dignissim orci a convallis interdum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin aliquam lectus quis enim faucibus, nec aliquet ipsum pellentesque. Ut in nunc ornare, malesuada enim nec, aliquet magna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;    </Text>
        <Button full color="yellow">View More</Button>



      </Box>
      {/* <AwesomeSlider  className={style.sliderContainer}>
    <Box>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar, sem in egestas efficitur, lorem ligula finibus augue, a pharetra metus velit sed est. Phasellus id mi quis est hendrerit tincidunt. Pellentesque at risus eu enim mattis lobortis. Suspendisse rutrum sollicitudin risus, et imperdiet ante placerat non. Nulla vel rutrum enim. Sed at sapien et sapien hendrerit vestibulum. Curabitur nec orci sed nulla consectetur porttitor commodo eu ligula. Donec eu diam ac massa consequat facilisis a vel lacus. Aenean euismod dolor venenatis, vehicula nunc id, tempor sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas cursus nisl ex, accumsan venenatis ligula dignissim sit amet. Proin dignissim orci a convallis interdum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin aliquam lectus quis enim faucibus, nec aliquet ipsum pellentesque. Ut in nunc ornare, malesuada enim nec, aliquet magna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; </Box>
    <Box>2</Box>
    <Box>3</Box>
    <Box>4</Box>
  </AwesomeSlider> */}

  </>
  )
}

export default Slider