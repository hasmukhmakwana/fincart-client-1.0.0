import React from "react";
import Invest from "./components/Invest";
import Slider from "./components/Slider";
import style from "./Landing.module.scss";
import Box from "./../../ui/Box/Box";
const LandingModule = () => {
  return (
    <div className={style.landingbg} >
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-sm-6 col-lg-6"  >
            <Slider/>
          </div>
          <div className="col-md-6 col-sm-6 col-lg-6 ">
            <Invest />
          </div>
        </div>
      </div>
    </div>
  );
};
export default LandingModule;
