import React from "react";
import Box from "../../../ui/Box/Box";
import style from "../Portfolio.module.scss";
import Text from "@ui/Text/Text";
import { GroupBox } from "./../../../ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import EditIcon from "App/icons/EditIcon";
import Card from "@ui/Card";
import Image from "next/image";
import Badge from "@ui/Badge/Badge";
import Basket2fill from "App/icons/Basket2fill";
import CartFill from "App/icons/CartFill";
import Piggybankfill from "App/icons/Piggybankfill";
import ArrowLeftRight from "App/icons/ArrowLeftRight";
import Shuffle from "App/icons/Shuffle";
import UpArrowLong from "App/icons/UpArrowLong";
import DownArrowLong from "App/icons/DownArrowLong";
const PortfolioaPage = () => {
  return (
    // <Box background="gray">
    //   <Box className="container-fluid">
        <Box className="row">
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Card>
                        {/* @ts-ignore */}
              <Text weight="bold" size="h4" border="default">
                Portfolio
              </Text>
              <Box background="default" css={{ pt: 10, pb: 10 }} >
                        {/* @ts-ignore */}
                <GroupBox position="apart" css={{ pl: 20, pr: 20 }} color="blue">
                  <Box>
                    <GroupBox>
                      <GroupBox>
                        {/* @ts-ignore */}
                        <input type="radio" className="form-check-input" value="Male" name="gender" /><Text size="h5" color="gray9">All</Text>
                      </GroupBox>
                      <GroupBox>
                        {/* @ts-ignore */}
                        <input type="radio" className="form-check-input" value="Male" name="gender" /><Text size="h5" color="gray9">Equity</Text>
                      </GroupBox>
                      <GroupBox>
                        {/* @ts-ignore */}
                        <input type="radio" className="form-check-input" value="Male" name="gender" /><Text size="h5" color="gray9">Debt</Text>
                      </GroupBox>
                    </GroupBox>
                  </Box>
                  <Box>
                    <GroupBox>
                      <GroupBox >
                        {/* @ts-ignore */}
                        <Text weight="extrabold" color="blue">Invested Value :</Text>
                        {/* @ts-ignore */}
                        <Text color="blue">49000</Text>
                      </GroupBox>
                      <GroupBox >
                        {/* @ts-ignore */}
                        <Text weight="extrabold" color="blue">Current Value :</Text>
                        {/* @ts-ignore */}
                        <Text color="blue">55,800</Text><Text color="blue" weight="extrabold">(12.73%)</Text>
                      </GroupBox>
                    </GroupBox>
                  </Box>
                </GroupBox>
              </Box>
              <Box>
                        {/* @ts-ignore */}
              <Text weight="bold" transform="uppercase" size="h5" color="blue2" css={{ pl: 20, pr: 20, pt: 10 }}>
                aditry birla sun life liquid fund-direct plan
              </Text>
                        {/* @ts-ignore */}
              <GroupBox css={{ p: 20 }} position="apart" >
                <Box>
                  <Text>Folio</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">10549464</Text>
                </Box>
                <Box>
                  <Text>Invested Value</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">10549464.95</Text>
                </Box>
                <Box>
                  <Text>Units</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">100 </Text>
                </Box>
                <Box>
                  <Text>Current Nav</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">134</Text>
                </Box>
                <Box>
                  <Text>Current Value</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">13,400</Text>
                </Box>
                <Box >
                <Text>Gain/Loss</Text>
                        {/* @ts-ignore */}
                  <GroupBox position="left">
                    <Box>
                        {/* @ts-ignore */}
                      <Text weight="bold" color="lightGreen">34</Text>
                    </Box>
                    <Box className="gainUp">
                      <UpArrowLong size="10"></UpArrowLong>
                    </Box>
                  </GroupBox>
                </Box>
              </GroupBox>
                        {/* @ts-ignore */}
              <GroupBox borderyellowBottom css={{ p: 20 }} position="right">

                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <Basket2fill color="yellow10"></Basket2fill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8">Purchase</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <CartFill color="yellow10"></CartFill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >Redeem</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <Piggybankfill color="yellow10"></Piggybankfill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >SIP</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <ArrowLeftRight color="yellow10"></ArrowLeftRight></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >STP</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box><Shuffle color="yellow10"></Shuffle></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" 
                      //@ts-ignore
                      color="gray8" size="h5">Switch</Text>
                  </GroupBox>
                </Button>
              </GroupBox>
              </Box>
              <Box>
                        {/* @ts-ignore */}
              <Text weight="bold" transform="uppercase" size="h5" color="blue2" css={{ pl: 20, pr: 20, pt: 10 }}>
                aditry birla sun life liquid fund-direct plan
              </Text>
              <GroupBox css={{ p: 20 }} position="apart" >
                        {/* @ts-ignore */}
                <Box>
                  <Text>Folio</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">10549464</Text>
                </Box>
                <Box>
                  <Text>Invested Value</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">10549464.95</Text>
                </Box>
                <Box>
                  <Text>Units</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">100 </Text>
                </Box>
                <Box>
                  <Text>Current Nav</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">134</Text>
                </Box>
                <Box>
                  <Text>Current Value</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">13,400</Text>
                </Box>
                <Box >
                  <Text>Gain/Loss</Text>
                        {/* @ts-ignore */}
                  <GroupBox position="left">
                    <Box>
                        {/* @ts-ignore */}
                      <Text weight="bold" className="text-danger">-34%</Text>
                    </Box>

                  </GroupBox>
                </Box>
              </GroupBox>
                        {/* @ts-ignore */}
              <GroupBox borderyellowBottom css={{ p: 20 }} position="right">

                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <Basket2fill color="yellow10"></Basket2fill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8">Purchase</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <CartFill color="yellow10"></CartFill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >Redeem</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <Piggybankfill color="yellow10"></Piggybankfill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >SIP</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <ArrowLeftRight color="yellow10"></ArrowLeftRight></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >STP</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box><Shuffle color="yellow10"></Shuffle></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" 
                      //@ts-ignore
                      color="gray8" size="h5">Switch</Text>
                  </GroupBox>
                </Button>
              </GroupBox>
              </Box>
              <Box>
                        {/* @ts-ignore */}
              <Text weight="bold" transform="uppercase" size="h5" color="blue2" css={{ pl: 20, pr: 20, pt: 10 }}>
               Mirae Asset Tax Saver Fund-Regular Growth
              </Text>
                        {/* @ts-ignore */}
              <GroupBox css={{ p: 20 }} position="apart" >
                <Box>
                  <Text>Folio</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">10549464</Text>
                </Box>
                <Box>
                  <Text>Invested Value</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">10549464.95</Text>
                </Box>
                <Box>
                  <Text>Units</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">100 </Text>
                </Box>
                <Box>
                  <Text>Current Nav</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">134</Text>
                </Box>
                <Box>
                  <Text>Current Value</Text>
                        {/* @ts-ignore */}
                  <Text weight="bold">13,400</Text>
                </Box>
                <Box >
                <Text>Gain/Loss</Text>
                        {/* @ts-ignore */}
                  <GroupBox position="left">
                    <Box>
                        {/* @ts-ignore */}
                      <Text weight="bold" className="text-danger">-34%</Text>
                    </Box>
                    <Box className="lossDown">

                      <DownArrowLong size="10"></DownArrowLong>
                    </Box>
                  </GroupBox>
                </Box>
              </GroupBox>
                        {/* @ts-ignore */}
              <GroupBox borderyellowBottom css={{ p: 20 }} position="right">

                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <Basket2fill color="yellow10"></Basket2fill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8">Purchase</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <CartFill color="yellow10"></CartFill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >Redeem</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <Piggybankfill color="yellow10"></Piggybankfill></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >SIP</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box> <ArrowLeftRight color="yellow10"></ArrowLeftRight></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" size="h5" 
                      //@ts-ignore
                      color="gray8" >STP</Text>
                  </GroupBox>
                </Button>
                <Button color="orangelightGroup" size="xs">
                  <GroupBox>
                    <Box><Shuffle color="yellow10"></Shuffle></Box>
                        {/* @ts-ignore */}
                    <Text weight="extrabold" 
                      //@ts-ignore
                      color="gray8" size="h5">Switch</Text>
                  </GroupBox>
                </Button>
              </GroupBox>
              </Box>
            </Card>
          </Box>
        </Box>
    //   </Box>
    // </Box>
  );
};
export default PortfolioaPage;
