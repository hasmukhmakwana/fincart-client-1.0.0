import React from 'react'
import PortfolioPage from './components/PortfolioPage'
import Layout from "App/shared/components/Layout"
import Holder from 'App/shared/components/Holder/Holder'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
const PortfolioModule = () => {
  return (
    <Layout>
      <PageHeader title='Portfolio' rightContent={<Holder />} />
      <PortfolioPage />
    </Layout>
  )
}

export default PortfolioModule