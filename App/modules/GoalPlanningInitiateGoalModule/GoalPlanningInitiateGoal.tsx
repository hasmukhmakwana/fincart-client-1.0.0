import Layout from 'App/shared/components/Layout';
import React from 'react'
import GoalPlanningInitiateGoalPage from './components/GoalPlanningInitiateGoalPage';
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import Holder from 'App/shared/components/Holder/Holder'
const GoalPlanningInitiateGoalModule = () => {
  return (
    <Layout>
       <PageHeader title='Goal Planning Initiate Goal' rightContent={<Holder/>} />
    <GoalPlanningInitiateGoalPage/>
    </Layout>
  )
}

export default GoalPlanningInitiateGoalModule