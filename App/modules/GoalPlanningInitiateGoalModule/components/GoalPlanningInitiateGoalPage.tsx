import Badge from "@ui/Badge/Badge";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import EditIcon from "App/icons/EditIcon";
import HomeIcon from "App/icons/HomeIcon";
import Image from "next/image";
import React from "react";
import style from "../GoalPlanningInitiateGoal.module.scss";
import Box from "../../../ui/Box/Box";
import { GroupBox } from "../../../ui/Group/Group.styles";
import Link from "next/link";
import { WEB_URL } from "App/utils/constants";
const GoalPlanningInitiateGoalPage = () => {
  return (
    <Box background="">
      <Box className="">
        <Box className="row">
          <Box className="col-md-4 col-sm-4 col-lg-4 mb-2">
            <Card>
              <Box css={{p:35}}>
                <GroupBox position="center" className="text-center" horizontal>
                  <Box>
                    <img
                        width={72}
                        height={55}
                        src={`${WEB_URL}/FontAwsome (home).png`}
                        alt="logo"
                      />
                    {/* <HomeIcon size="75" color="blue" /> */}
                  </Box>
                  <Box css={{m:10}}>
                    <Text weight="bold" color="default" size="h4">
                      Home
                    </Text>
                  </Box>
                  <Box className={style.textLeft}>
                    <Text color="default" size="h6">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque vitae commodo nibh. Sed pellentesque felis
                      placerat efficitur facilisis. Praesent quis eleifend quam.
                      Pellentesque vitae aliquet lacus, a condimentum arcu.
                    </Text>
                  </Box>
                  <Box css={{m:10}}>
                        <Button color="yellow"><Link href='/GoalPlanningSteps'>Select Goal</Link></Button>
                  </Box>
                </GroupBox>
              </Box>
            </Card>
          </Box>

          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Card>
              <Box css={{p:35}}>
                <GroupBox position="center" className="text-center" horizontal>
                  <Box>
                    <img
                        width={54}
                        height={54}
                        src={`${WEB_URL}/profilePicture.png`}
                        alt="taxsave"
                      />
                    {/* <HomeIcon size="75" color="blue" /> */}
                  </Box>
                  <Box css={{m:10}}>
                    <Text weight="bold" color="default" size="h4">
                      Save Tax
                    </Text>
                  </Box>
                  <Box className={style.textLeft}>
                    <Text color="default" size="h6">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque vitae commodo nibh. Sed pellentesque felis
                      placerat efficitur facilisis. Praesent quis eleifend quam.
                      Pellentesque vitae aliquet lacus, a condimentum arcu.
                    </Text>
                  </Box>
                  <Box css={{m:10}}>
                        <Button color="yellow"><Link href='/GoalPlanningSteps'>Select Goal</Link></Button>
                  </Box>
                </GroupBox>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Card>
              <Box css={{p:35}}>
                <GroupBox position="center" className="text-center" horizontal>
                  <Box>
                    <img
                        width={54}
                        height={54}
                        src={`${WEB_URL}/FontAwsome (wheelchair).png`}
                        alt="retirementplanning"
                      />
                    {/* <HomeIcon size="75" color="blue" /> */}
                  </Box>
                  <Box css={{m:10}}>
                    <Text weight="bold" color="default" size="h4">
                      Retirement Planning
                    </Text>
                  </Box>
                  <Box className={style.textLeft}>
                    <Text color="default" size="h6">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque vitae commodo nibh. Sed pellentesque felis
                      placerat efficitur facilisis. Praesent quis eleifend quam.
                      Pellentesque vitae aliquet lacus, a condimentum arcu.
                    </Text>
                  </Box>
                  <Box css={{m:10}}>
                        <Button color="yellow"><Link href='/GoalPlanningSteps'>Select Goal</Link></Button>
                  </Box>
                </GroupBox>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Card>
              <Box css={{p:35}}>
                <GroupBox position="center" className="text-center" horizontal>
                  <Box>
                    <img
                        width={54}
                        height={54}
                        src={`${WEB_URL}/FontAwsome (book-reader).png`}
                        alt="education"
                      />
                    {/* <HomeIcon size="75" color="blue" /> */}
                  </Box>
                  <Box css={{m:10}}>
                    <Text weight="bold" color="default" size="h4">
                      Education
                    </Text>
                  </Box>
                  <Box className={style.textLeft}>
                    <Text color="default" size="h6">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque vitae commodo nibh. Sed pellentesque felis
                      placerat efficitur facilisis. Praesent quis eleifend quam.
                      Pellentesque vitae aliquet lacus, a condimentum arcu.
                    </Text>
                  </Box>
                  <Box css={{m:10}}>
                        <Button color="yellow"><Link href='/GoalPlanningSteps'>Select Goal</Link></Button>
                  </Box>
                </GroupBox>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Card>
              <Box css={{p:35}}>
                <GroupBox position="center" className="text-center" horizontal>
                  <Box>
                    <img
                        width={54}
                        height={42}
                        src={`${WEB_URL}/FontAwsome (ring).png`}
                        alt="wedding"
                      />
                    {/* <HomeIcon size="75" color="blue" /> */}
                  </Box>
                  <Box css={{m:10}}>
                    <Text weight="bold" color="default" size="h4">
                      Wedding
                    </Text>
                  </Box>
                  <Box className={style.textLeft}>
                    <Text color="default" size="h6">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque vitae commodo nibh. Sed pellentesque felis
                      placerat efficitur facilisis. Praesent quis eleifend quam.
                      Pellentesque vitae aliquet lacus, a condimentum arcu.
                    </Text>
                  </Box>
                  <Box css={{m:10}}>
                        <Button color="yellow"><Link href='/GoalPlanningSteps'>Select Goal</Link></Button>
                  </Box>
                </GroupBox>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Card>
              <Box css={{p:35}}>
                <GroupBox position="center" className="text-center" horizontal>
                  <Box>
                    <img
                        width={55}
                        height={44}
                        src={`${WEB_URL}/FontAwsome (umbrella-beach).png`}
                        alt="vacation"
                      />
                    {/* <HomeIcon size="75" color="blue" /> */}
                  </Box>
                  <Box css={{m:10}}>
                    <Text weight="bold" color="default" size="h4">
                      Vacation
                    </Text>
                  </Box>
                  <Box className={style.textLeft}>
                    <Text color="default" size="h6">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Quisque vitae commodo nibh. Sed pellentesque felis
                      placerat efficitur facilisis. Praesent quis eleifend quam.
                      Pellentesque vitae aliquet lacus, a condimentum arcu.
                    </Text>
                  </Box>
                  <Box css={{m:10}}>
                        <Button color="yellow"><Link href='/GoalPlanningSteps'>Select Goal</Link></Button>
                  </Box>
                </GroupBox>
              </Box>
            </Card>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default GoalPlanningInitiateGoalPage;
