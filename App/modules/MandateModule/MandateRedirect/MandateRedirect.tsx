import React, { FC, useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import { getLS } from "@ui/DataGrid/utils";
import { MANDATE_TXN_ID, WEB_URL } from "App/utils/constants";
import Card from "@ui/Card";
import { useRouter } from "next/router";
import { checkMandateStatus } from "App/api/mandate";
import Layout, { toastAlert } from "App/shared/components/Layout";
import clsx from "clsx";
import { DataCell, DataParent, DataRow, Table } from "@ui/DataGrid/DataGrid.styles";
import { encryptData, formatDate } from "App/utils/helpers";
const Module: FC = () => {
  const router = useRouter();
  const [mandate, setmandate] = useState<any>();
  /** @USE_EFFECTS */
  useEffect(() => {
    const checkMandate = async () => {
      const mandateID = getLS(MANDATE_TXN_ID);
      if (mandateID) {
        /**
         * check the mandate status
         */
        try {
          let result: any = await checkMandateStatus(mandateID);
          setmandate(result?.data || {});
          console.log(result);
        } catch (error) {
          console.log(error);
          toastAlert("error", error);
        }
      }
    };
    checkMandate();
    return () => { };
  }, []);

  const getStatus = () => {
    if (mandate?.Status === "-1") return "failed";
    if (mandate?.Status === "1") return "success";
    return "pending";
  };
  return (
    <Layout>
      <Box className="row row-cols-1 row-cols-md-2 justify-content-center">
        <Box className="col">
          <Card noshadow css={{ p: 50 }}>
            <Box>
              <Text className={clsx({
                ["text-danger"]: mandate?.Status === "-1",
                ["text-success"]: mandate?.Status === "1",
                ["text-primary"]: mandate?.Status === "0",
              })} size="h3" space="letterspace">
                E-Mandate Registration
              </Text>
              <br />
              <Text
                className={clsx({
                  ["text-danger"]: mandate?.Status === "-1",
                  ["text-success"]: mandate?.Status === "1",
                  ["text-primary"]: mandate?.Status === "0",
                })}
                size="h4"
                css={{ textTransform: "uppercase" }}
              >
                {getStatus()}
              </Text>
              <Table>
                <DataParent css={{ fontSize: "0.8rem" }}>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      Transaction No
                    </DataCell>
                    <DataCell css={{ float: "right" }}>
                      {mandate?.TxnReferenceNo || ""}
                    </DataCell>
                  </DataRow>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      Bank Account Type
                    </DataCell>
                    <DataCell css={{ textAlign: "right" }}>
                      {mandate?.accounttype || "Saving"}
                    </DataCell>
                  </DataRow>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      Account No
                    </DataCell>
                    <DataCell css={{ textAlign: "right" }}>
                      {mandate?.accountnumber || ""}
                    </DataCell>
                  </DataRow>
                  {(mandate?.Status === "-1") ? <> <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      Error Description :
                    </DataCell>
                    <DataCell css={{ textAlign: "right" }}>
                      {mandate?.ErrorDescription}
                    </DataCell>
                  </DataRow>
                    <DataRow>
                      <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                        For Clarifications
                      </DataCell>
                      <DataCell css={{ textAlign: "right" }}>
                        please write to operations@fincart.com or call +91-9811781438
                      </DataCell>
                    </DataRow>
                  </> : <></>}
                </DataParent>
              </Table>
            </Box>
            <Box css={{ pt: 10 }} className="text-end">
              <Button
                type="submit"
                color="yellow"
                onClick={() => router.push("/Mandate/MandateList")}
              >
                {`View Mandate(s)`}
              </Button>
            </Box>
            <Box css={{ pt: 10 }} className="text-center">
              <img
                src={`${WEB_URL}/nach_emandate_logo.png`}
                alt="nach_emandate_logo"
              />
            </Box>
          </Card>
        </Box>
      </Box >
    </Layout >
  );
};

export default Module;
