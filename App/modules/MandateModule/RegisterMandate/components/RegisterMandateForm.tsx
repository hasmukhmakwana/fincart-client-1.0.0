import React, { FC, useEffect, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import Input from "@ui/Input";
import Radio from "@ui/Radio/Radio";
import SelectMenu from "@ui/Select/Select";
import { RadioGroup } from "@radix-ui/react-radio-group";
import Checkbox from "@ui/Checkbox/Checkbox";
import { FormValueTypes } from "../../MandateTypes";
import { MANDATE_TYPES } from "App/utils/constants";
import EMandateForMF from "./EMandateForMutualFund";
import {
  encryptData,
  formatDate,
  getArrayFromKey,
  getUser,
} from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import {
  getAllMemberPurchaseDetails,
  getBankList,
  getMandateMaster,
  getMandateBankList
} from "App/api/mandate";
import useMandatListStore from "../../store";
import Label from "@ui/Label/Label";
import { useRouter } from "next/router";
import Loader from "App/shared/components/Loader";
import DialogModal from "@ui/ModalDialog/ModalDialog";
/**CONSTANTS */
// per day limit range min: ₹5,000 and max: ₹5,00,000
const perDayLimitList = [
  { id: "5000.00", name: "5000" },
  { id: "15000.00", name: "15000" },
  { id: "50000.00", name: "50000" },
  { id: "100000.00", name: "100000" },
  { id: "250000.00", name: "250000" },
  { id: "300000.00", name: "300000" },
  { id: "350000.00", name: "350000" },
  { id: "400000.00", name: "400000" },
  { id: "450000.00", name: "450000" },
  { id: "500000.00", name: "500000" },
  { id: "other", name: "Custom" },
];

const schemaObjList: any = {
  mandatory: {
    // MandateType: Yup.string().required("MandateType is required"),
    FromDate: Yup.string().required("From Date is required"),
    PerDayLimit: Yup.string().required("Per Day Limit is required"),
    NameAsPerBank: Yup.string().required("Name is required"),
    MobileAsPerBank: Yup.string().required("Mobile is required"),
    EmailAsPerBank: Yup.string().required("Email ID is required"),
  },
  customSchema: {
    FromDate: Yup.string().required("From Date is required"),
    PerDayLimit: Yup.string().required("Per Day Limit is required"),
    OtherAmount: Yup.number().required("Custom Amount is Required").positive().integer().min(5000, "Minimum mandate amount should be Rs.5,000").max(500000, "Mandate should not exceed Rs.5,00,000")
  }
}

type EventTypes = {
  handleData: (value: FormValueTypes) => void;
};

export default ({ handleData }: EventTypes) => {
  const router = useRouter();
  const user: any = getUser();
  const [validationSchema, setValidationSchema] = useState(schemaObjList.mandatory);
  const {
    formType,
    formValues,
    setFormValues,
    setFormType,
    loading,
    mandateList,
    setMandateList,
    setLoader,
    setSelectedMemberDetails,
    memberPurchaseDetails,
    selectedMemberDetails,
    setMemberPurchaseDetails,
  } = useMandatListStore();

  const [myUntillCancel, setmyUntillCancel] = useState<boolean>(
    !!formValues?.UntilCancel
  );

  const [banks, setbanks] = useState<any[]>([]);
  const [mandateMaster, setmandateMaster] = useState<any[]>([]);
  const [selectedInvestor, setSelectedInvestor] = useState<any>(user?.basicid);
  // const [mandateSet, setMandateSet] = useState<any>(MANDATE_TYPES.Physical);
  const [localLoader, setlocalLoader] = useState<boolean>(true);
  const [popupLoader, setPopupLoader] = useState<boolean>(false);
  const [mandateBankList, setMandateBankList] = useState<any>([]);
  const [openManBankList, setOpenManBankList] = useState<boolean>(false);

  const fetchMandateBankList = async () => {
    try {
      setPopupLoader(true);
      let result: any = await getMandateBankList();
      setMandateBankList(result?.data);
      setPopupLoader(false);
    } catch (error) {
      console.log(error);
      setPopupLoader(false);
      toastAlert("error", error);
    }
  }

  useEffect(() => {
    const fetchMandateMaster = async () => {
      try {
        let result = await getMandateMaster();
        setmandateMaster(result?.data || []);
        // console.log(result?.data,"result?.data");
      } catch (error: any) {
        console.log(error);
        toastAlert("error", error);
      }
    };
    fetchMandateMaster();
    return () => { };
  }, []);

  useEffect(() => {
    const fetchBankList = async () => {
      try {
        setlocalLoader(true);
        const basicID: any = selectedMemberDetails?.basicid;
        console.log(basicID, "checksum");

        if (basicID != undefined) {
          const encrypted: any = encryptData(basicID, true);
          let result = await getBankList(encrypted);
          // console.log(result?.data, "banklist");
          let banks = [];
          if (result?.data?.length >= 0) {
            banks = result?.data;
          } else {
            banks = [result?.data || {}];
          }
          banks = banks.map((i: any) => ({
            ...i,
            DisplayName: `${i.EmandateBankCode} - ${i.AccountNumber}`,
          }));
          // console.log(banks, "banklist");
          setFormValues({
            MandateType: MANDATE_TYPES.Physical,
            AccountNo: banks?.[0]?.AccountNumber || "",
            BranchName: banks?.[0]?.Branch || "",
            IFSC: banks?.[0]?.IFSC || "",
            MICR: banks?.[0]?.MICR || "",
            EmandateBankCode: banks?.[0]?.EmandateBankCode || "",
            NameAsPerBank: banks?.[0]?.NameAsPerBank || "",
            MobileAsPerBank: banks?.[0]?.Mobile || "",
            EmailAsPerBank: banks?.[0]?.Email || "",
            AuthenticationMode: banks?.[0]?.NetBankingActive === "Y" ? "N" : "D",
          });
          setbanks(banks);
        }

        setlocalLoader(false);
      } catch (error: any) {
        setlocalLoader(false);
        console.log(error);
        toastAlert("error", error);
      }
    };
    if (selectedInvestor) {
      fetchBankList();
      ////EmandateActive
    }
    return () => { };
  }, [selectedMemberDetails?.basicid]);

  return (
    <>
      {localLoader ? <Loader></Loader> :
        <Formik
          initialValues={formValues}
          validationSchema={Yup.object().shape(validationSchema)}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleData(values);
            resetForm();
          }}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue,
          }) => (
            <>
              <Box className="col-md-12 col-sm-12 col-lg-12">
                <Card css={{ p: 20 }}>
                  <Box className="row" position="middle">
                    <Box className="row mb-3">
                      <Box className="col col-12 col-md-3">
                        <Label htmlFor="MandateType" label="Mandate Type" />
                        <GroupBox>
                          <RadioGroup
                            defaultValue={values.MandateType}
                            className="inlineRadio"
                            name="MandateType"
                            value={values.MandateType}
                            onValueChange={(e: any) => {
                              // console.log(e);
                              if (e === "E-Mandate" && banks?.[0]?.EmandateActive === "N") {
                                console.log(banks?.[0], "set")
                                fetchMandateBankList();
                                setOpenManBankList(true);
                              }
                              else {
                                // setMandateSet(e)
                                setFieldValue("MandateType", e);
                              }
                            }}
                          >
                            <Radio
                              value={MANDATE_TYPES.Physical}
                              label={MANDATE_TYPES.Physical}
                              id={MANDATE_TYPES.Physical}
                            />
                            <Radio
                              value={MANDATE_TYPES.Emandate}
                              label={MANDATE_TYPES.Emandate}
                              id={MANDATE_TYPES.Emandate}
                            />
                          </RadioGroup>
                        </GroupBox>
                      </Box>
                    </Box>
                    <Box className="col col-12 col-md-3">
                      <SelectMenu
                        items={banks}
                        bindValue="AccountNumber"
                        bindName="DisplayName"
                        label="Bank Account No"
                        placeholder="Select"
                        name="AccountNo"
                        value={values.AccountNo}
                        onChange={(e: any) => {
                          console.log(e);
                          setFieldValue("AccountNo", e.AccountNumber);
                          setFieldValue("BranchName", e.Branch);
                          setFieldValue("IFSC", e.IFSC);
                          setFieldValue("MICR", e.MICR);
                          setFieldValue("EmandateBankCode", e.EmandateBankCode);
                          setFieldValue("NameAsPerBank", e.NameAsPerBank);
                          setFieldValue("MobileAsPerBank", e.Mobile);
                          setFieldValue("EmailAsPerBank", e.Email);
                          setFieldValue(
                            "AuthenticationMode",
                            e.NetBankingActive === "Y" ? "N" : "D"
                          );
                        }}
                      />
                    </Box>
                    <Box className="col col-12 col-md-3">
                      <Input
                        label="Bank Branch"
                        name="BranchName"
                        value={values.BranchName}
                        onChange={handleChange}
                        placeholder="Enter branch name"
                        disabled
                      />
                    </Box>

                    <Box className="col col-12 col-md-3">
                      <Input
                        label="IFSC Code"
                        name="IFSC"
                        value={values.IFSC}
                        onChange={handleChange}
                        placeholder="Enter IFSC Code"
                        disabled
                      />
                    </Box>
                    <Box className="col col-12 col-md-3">
                      <Input
                        label="MICR Code"
                        name="MICR"
                        value={values.MICR}
                        onChange={handleChange}
                        placeholder="MICR Code"
                        disabled
                      />
                    </Box>
                    {/* start */}
                    {values.MandateType === MANDATE_TYPES.Emandate && (
                      <>
                        <Box className="col col-12 col-md-3">
                          <Input
                            label="Name As Per Bank"
                            name="NameAsPerBank"
                            value={values.NameAsPerBank}
                            onChange={handleChange}
                            placeholder="Name As Per Bank"
                            required
                            error={errors.NameAsPerBank}
                          />
                        </Box>
                        <Box className="col col-12 col-md-3">
                          <Input
                            label="Mobile As Per Bank"
                            name="MobileAsPerBank"
                            value={values.MobileAsPerBank}
                            onChange={handleChange}
                            placeholder="Mobile As Per Bank"
                            required
                            error={errors.MobileAsPerBank}
                          />
                        </Box>
                        <Box className="col col-12 col-md-3">
                          <Input
                            label="Email As Per Bank"
                            name="EmailAsPerBank"
                            value={values.EmailAsPerBank}
                            onChange={handleChange}
                            placeholder="Email As Per Bank"
                            required
                            error={errors.EmailAsPerBank}
                          />
                        </Box>
                        <Box className="col col-12 col-md-3">
                          <SelectMenu
                            items={getArrayFromKey(
                              mandateMaster,
                              "AuthMode_List"
                            )}
                            bindValue={"Value"}
                            bindName={"Text"}
                            label="Authentication Mode"
                            placeholder={"Select"}
                            value={values.AuthenticationMode}
                            onChange={(e: any) => {
                              setFieldValue("AuthenticationMode", e.Value);
                            }}
                            required
                          />
                        </Box>
                      </>
                    )}
                    {/* end */}
                    <Box className="col col-12 col-md-3">
                      <SelectMenu
                        items={perDayLimitList}
                        bindValue={"id"}
                        bindName={"name"}
                        label={`${values.MandateType === MANDATE_TYPES.Emandate
                          ? "Per Day Limit"
                          : "SI Amount"
                          }`}
                        placeholder={"Select"}
                        value={values.PerDayLimit}
                        onChange={(e: any) => {
                          setFieldValue("PerDayLimit", e.id);
                          (e.id === "other" ? setValidationSchema(schemaObjList.customSchema) : setValidationSchema(schemaObjList.mandatory))
                        }}
                        required
                        error={errors.PerDayLimit}
                      />
                    </Box>
                    {values.PerDayLimit === "other" && (
                      <Box className="col col-12 col-md-3">
                        <Input
                          //type="number"
                          label="Custom Amount"
                          name="OtherAmount"
                          value={values?.OtherAmount}
                          onChange={(e: any) => {
                            setFieldValue("OtherAmount", e.target.value.replace(/\D/g, ''));
                          }}
                          placeholder="Custom Amount"
                          required
                          error={errors?.OtherAmount}
                        />
                      </Box>
                    )}
                    <Box className="col col-12 col-md-3">
                      <Input
                        type="date"
                        label="From Date"
                        name="FromDate"
                        value={values.FromDate}
                        onChange={handleChange}
                        placeholder="From Date"
                        required
                        error={errors.FromDate}
                      />
                    </Box>
                    <Box className="col col-12 col-md-3">
                      <Input
                        type="date"
                        label="End Date"
                        name="ToDate"
                        value={values.ToDate}
                        onChange={handleChange}
                        placeholder="End Date"
                        required
                        disabled={values.UntilCancel}
                      ></Input>
                    </Box>
                    <Box className="col col-12 col-md-3">
                      <GroupBox>
                        <Checkbox
                          label="Until Cancelled"
                          id="UntilCancel"
                          name="UntilCancel"
                          checked={!!values?.UntilCancel}
                          // onChange={handleChange}
                          onChange={(e: any) => {
                            // @ts-ignore
                            const value = e?.target?.checked || false;
                            setFieldValue("UntilCancel", value);
                            setmyUntillCancel(value);
                            const today = new Date();
                            if (value) {
                              setFieldValue(
                                "ToDate",
                                formatDate(
                                  formatDate(new Date(today.setFullYear(2099)))
                                )
                              );
                            } else {
                              setFieldValue(
                                "ToDate",
                                formatDate(
                                  new Date(today.setMonth(today.getMonth() + 2))
                                )
                              );
                            }
                          }}
                        />
                      </GroupBox>
                    </Box>
                  </Box>
                  <Box className="text-end">
                    <Button
                      type="submit"
                      color="yellow"
                      onClick={() => router.push("/Mandate/MandateList")}
                    >
                      Back
                      {/* Open Mandate List */}
                    </Button>
                    <Button
                      type="submit"
                      color="yellow"
                      onClick={handleSubmit}
                      loading={loading}
                    >
                      Register
                    </Button>
                  </Box>
                </Card>
              </Box>
            </>
          )}
        </Formik>
      }
      <DialogModal
        open={openManBankList}
        setOpen={setOpenManBankList}
        css={{
          "@bp0": { width: "100%" },
          "@bp1": { width: "30%" },
        }}
      >
        <EMandateForMF loading={popupLoader} bankSet={mandateBankList} bankName={banks?.[0]?.BankName} />
      </DialogModal>
    </>
  );
};
