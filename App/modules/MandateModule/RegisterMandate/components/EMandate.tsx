import React, { FC, useEffect, useState } from "react";
import useMandatListStore from "../../store";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Divider from "@ui/Divider/Divider";
import styles from "./MandateList.module.scss";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/DataGrid/DataGrid.styles";
import Text from "@ui/Text/Text";
import { GroupBox } from "@ui/Group/Group.styles";
import Checkbox from "@ui/Checkbox/Checkbox";
import { formatDate } from "App/utils/helpers";
import { WEB_URL } from "App/utils/constants";

interface EMandateTypes {
  open: boolean;
  setOpen: (value: boolean) => void;
  handleProceed: () => void;
}
const EMandate = ({ open, setOpen, handleProceed }: EMandateTypes) => {
  const {
    formType,
    formValues,
    setFormType,
    loading,
    setLoader,
    mandateList,
    setMandateList,
  } = useMandatListStore();

  // console.log(formValues);
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3">
        <Box className="row">
          <Box className="col-auto text-light">E-Mandate</Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box
        className="modal-body modal-body-scroll"
        css={{ fontSize: "0.8rem" }}
      >
        <Box className="container">
          <Box className="row">
            <Box className="col-12">
              <Table>
                <DataParent>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      Bank Account
                    </DataCell>
                    <DataCell css={{ float: "right" }}>
                      {formValues?.accountType || "Saving"}
                    </DataCell>
                  </DataRow>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      Bank Branch
                    </DataCell>
                    <DataCell css={{ float: "right" }}>
                      {formValues?.BranchName || ""}
                    </DataCell>
                  </DataRow>

                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      IFSC Code
                    </DataCell>
                    <DataCell css={{ float: "right" }}>
                      {formValues?.IFSC || ""}
                    </DataCell>
                  </DataRow>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      MICR Code
                    </DataCell>
                    <DataCell css={{ float: "right" }}>
                      {formValues?.MICR || ""}
                    </DataCell>
                  </DataRow>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      Invest
                    </DataCell>
                    <DataCell css={{ float: "right" }}>
                      {(formValues?.PerDayLimit === "other"
                        ? formValues?.OtherAmount
                        : formValues?.PerDayLimit) || 0}
                    </DataCell>
                  </DataRow>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      Start Date
                    </DataCell>
                    <DataCell css={{ float: "right" }}>
                      {formatDate(formValues?.FromDate, "d-m-y")}
                    </DataCell>
                  </DataRow>
                  <DataRow>
                    <DataCell css={{ p: 0, color: "var(--colors-blue1)" }}>
                      End Date
                    </DataCell>
                    <DataCell css={{ float: "right" }}>
                      {formatDate(formValues?.ToDate, "d-m-y")}
                    </DataCell>
                  </DataRow>
                </DataParent>
              </Table>
              <Box css={{ pt: 10 }}>
                <GroupBox>
                  <Checkbox
                    label="Until Cancelled"
                    id="UntilCancelModal"
                    name="UntilCancel"
                    checked={!!formValues?.UntilCancel}
                    disabled={true}
                  />
                </GroupBox>
              </Box>
              <Box css={{ pt: 10 }} className="text-end">
                <Button
                  type="submit"
                  color="yellow"
                  onClick={setOpen}
                  loading={loading}
                >
                  Cancel
                </Button>
                <Button
                  type="submit"
                  color="yellow"
                  onClick={() => handleProceed()}
                  loading={loading}
                >
                  Proceed
                </Button>
              </Box>
              <Box css={{ pt: 10 }} className="text-center">
                <img
                  src={`${WEB_URL}/nach_emandate_logo.png`}   
                  alt="nach_emandate_logo"
                />
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default EMandate;
