import React, { FC, useEffect, useState } from "react";
import useMandatListStore from "../../store";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import { WEB_URL } from "App/utils/constants";
import Loader from "App/shared/components/Loader";
import styles from "../../Mandate.module.scss"

const EMandateForMF = ({ loading, bankSet, bankName }: any) => {
    return (
        <Box className="col-md-12 col-sm-12 col-lg-12">
            <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                <Box className="row">
                    <Box className="col-auto text-light">E-Mandate For Mutual Fund</Box>
                    <Box className="col"></Box>
                    <Box className="col-auto"></Box>
                </Box>
            </Box>

            <Box className="modal-body p-3 pb-1"
            // css={{ fontSize: "0.8rem" }}
            >
                <Box className="container">
                    <Box className="row mt-1">
                        <Box className="col-12">
                            <Text size="h4">
                                Sorry! Your bank, <span style={{ fontSize: "1.1rem", color: "var(--colors-blue1)" }}>{bankName}</span> does not allow E-mandate registration.
                            </Text>
                        </Box>
                        <Box className="col-12 mt-3">
                            <Text size="h5" css={{ fontWeight: "bold !important" }}>
                                {/*Bank List For E-Mandate */}
                                Currently, E-Mandate feature is available only for the banks listed below:
                            </Text>
                        </Box>
                        <Box className="col-12 mt-2" css={{ overflow: "auto" }}>
                            <Box className={`${styles.scrollit}`}>
                                {loading ? <><Loader /></> : <>
                                    {bankSet?.map((item: any) => {
                                        return (<Text size="h6" css={{ fontSize: "0.8rem" }}>
                                            - {item}
                                        </Text>);
                                    })}
                                </>}
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Box>

            <Box css={{ pt: 10 }} className="text-center">
                <img
                    src={`${WEB_URL}/nach_emandate_logo.png`}
                    alt="nach_emandate_logo"
                />
            </Box>
        </Box >
    )
}

export default EMandateForMF