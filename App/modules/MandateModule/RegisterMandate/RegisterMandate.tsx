import React, { FC, useEffect, useReducer, useState } from "react";
import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import { AddFormValueTypes, FormValueTypes } from "../MandateTypes";
import RegisterMandateForm from "./components/RegisterMandateForm";
import { MANDATE_TXN_ID, MANDATE_TYPES, WEB_URL } from "App/utils/constants";
import styles from "../Mandate.module.scss";
import {
  compareDates,
  decryptData,
  encryptData,
  formatDate,
  getArryOfDMY,
  getUser,
  openTab,
} from "App/utils/helpers";
import { addPhysicalMandate, addEMandate } from "App/api/mandate";
import useMandatListStore from "../store";
import EMandate from "./components/EMandate";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import { useRouter } from "next/router";
import MemberList from "../common/MemberList";
import { setLS } from "@ui/DataGrid/utils";
import Box from "@ui/Box/Box";
/** @CONSTANTS */
const type = "emandate";

const Module: FC = () => {

  const user: any = getUser();
  const router = useRouter();
  // console.log(router.query);

  const {
    formType,
    formValues,
    setFormType,
    setFormValues,
    loading,
    mandateList,
    setMandateList,
    setLoader,
    mandateID,
    setMandateID,
    selectedMemberDetails,
  } = useMandatListStore();
  // console.log(selectedMemberDetails, "selectedMemberDetails");

  /** @STATES */
  const [open, setOpen] = useState<boolean>(false);

  const [formData, setformData] = useState<any>(null);

  /** @FUNCTIONS */
  const handleData = async (postData: FormValueTypes) => {
    // console.log("handle data");
    let temp: any = postData;

    temp.dmy_from = getArryOfDMY(postData.FromDate);
    temp.dmy_to = getArryOfDMY(postData.ToDate);

    //comparing the dates
    if (compareDates(postData.FromDate, postData.ToDate, "gte")) {
      toastAlert("warn", "End Date should be greater than From Date");
      return;
    }

    setformData(temp);
    setFormValues(postData);

    // check mandate type
    if (postData.MandateType === MANDATE_TYPES.Emandate) {
      setOpen(true);
    } else {
      // submit form
      handleProceed(postData);
    }
  };

  const handleProceed = async (data: any = null) => {
    try {
      setLoader(true);
      const basicid = selectedMemberDetails?.basicid || user?.basicid || "";
      const reqData: any = data || formData;

      //E-Mandate
      if (reqData.MandateType === MANDATE_TYPES.Emandate) {
        const postData = {
          basicid: basicid,
          profileid: selectedMemberDetails?.profileID || "",
          CustomerID: null,
          accountnumber: reqData?.AccountNo,
          accounttype: "SB",
          TxnAmount: null,
          SIamount:
            (reqData?.PerDayLimit === "other"
              ? parseFloat(reqData?.OtherAmount).toFixed(2)
              : parseFloat(reqData?.PerDayLimit).toFixed(2)) || 0.00,
          SIamounttype: "Max",
          fromDate: reqData?.dmy_from[0] || "",
          fromMonth: reqData?.dmy_from[1] || "",
          fromYear: reqData?.dmy_from[2] || "",
          toDate: reqData?.dmy_to[0] || "",
          toMonth: reqData?.dmy_to[1] || "",
          toYear: reqData?.dmy_to[2] || "",
          frequency: "ADHO",
          Ref1: null,
          Ref2: null,
          customername: reqData?.NameAsPerBank,
          mandaterefno: null,
          IfscCode: reqData?.IFSC,
          Micr: reqData?.MICR,
          filler: null,
          MobileNumber: reqData?.MobileAsPerBank,
          EmailId: reqData?.EmailAsPerBank,
          ModeofAuthentication: reqData?.AuthenticationMode,
          Until_Cancel: reqData?.UntilCancel ? "Y" : "N",
          LiveBankCode: reqData?.EmandateBankCode,
          TxnReferenceNo: null,
          BankReferenceNo: null,
          TxnDate: null,
          UMRN: null,
          AuthStatus: null,
          ErrorStatus: null,
          ErrorDescription: null,
          Active: null,
          Status: null,
          Device:'WEB',
          redirect_url: `${WEB_URL}/Mandate/MandateRedirect`
        };

        const encrypted: any = encryptData(postData);

        let result: any = await addEMandate(encrypted);

        // console.log("api triggered");
        // console.log(result);

        setLS(MANDATE_TXN_ID, encryptData(result?.msg || 0, true));
        setLoader(false);
        setOpen(false);
        openTab(result?.data, true);
        router.push("/Mandate/MandateList");
      } else {
        const postData: any = {
          BasicID: basicid,
          ProfileID: selectedMemberDetails?.profileID,
          FromMonth: reqData?.dmy_from[1] || "",
          FromYear: reqData?.dmy_from[2] || "",
          ToMonth: reqData?.dmy_to[1] || "",
          ToYear: reqData?.dmy_to[2] || "",
          PerDayLimit: reqData?.PerDayLimit
            ? parseInt(
              (reqData?.PerDayLimit === "other"
                ? reqData?.OtherAmount
                : reqData?.PerDayLimit) || 0
            )
            : 0,
          UntilCancel: reqData?.UntilCancel ? "Y" : "N",
          // ClientName: reqData?.NameAsPerBank
        };

        console.log({ postData });
        const encrypted: any = encryptData(postData);
        let result = await addPhysicalMandate(encrypted);
        setLoader(false);

        // console.log(result);

        setTimeout(() => {
          router.push("/Mandate/MandateList");
        }, 1000);

        return toastAlert("success", "Mandate Registered Succcessfully");
      }
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  return (
    <Layout>
      <PageHeader title="Register Mandate" rightContent={<></>} />
      <Box className="row p-4" css={{ backgroundColor: "white", borderRadius: "8px", border: "1px solid #82afd9" }}>
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className={styles.mandateList}>
            <Box className="row">
              <Box className="col-12 col-sm-8">
                <MemberList />
              </Box>
              <Box className="col-12 col-sm-4 d-flex align-content-center align-items-center justify-content-end">
                <Box className="text-end p-1 justify-content-end">&nbsp;
                </Box>
              </Box>
            </Box>
            <Box className="row">
              <Box className="col-12">
                <RegisterMandateForm handleData={handleData} />
                <DialogModal
                  open={open}
                  setOpen={setOpen}
                  css={{
                    "@bp0": { width: "100%" },
                    "@bp1": { width: "30%" },
                  }}
                >
                  <EMandate
                    open={open}
                    setOpen={() => setOpen(!open)}
                    handleProceed={handleProceed}
                  />
                </DialogModal>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default Module;
