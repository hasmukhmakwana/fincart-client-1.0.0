import { MANDATE_TYPES } from "App/utils/constants";
import { formatDate } from "App/utils/helpers";
import create from "zustand";
import { FormTypes, FormValueTypes } from "./MandateTypes";

interface StoreTypes {
  basicid: number;
  formType: FormTypes;
  formValues: any;
  mandateList: FormValueTypes[];
  singleMandate: any;
  loading: boolean;
  memberPurchaseDetails: any;
  selectedMemberDetails: any;
  mandateID: number;
  member: any;
  setMember: (payload: any) => void;
  setBasicid: (payload: number) => void;
  setFormType: (payload: FormTypes) => void;
  setFormValues: (payload: any) => void;
  setMandateList: (payload: FormValueTypes[]) => void;
  setSingleMandate: (payload: any) => void;
  setLoader: (payload: boolean) => void;
  setMemberPurchaseDetails: (payload: any) => void;
  setSelectedMemberDetails: (payload: any) => void;
  setMandateID: (payload: number) => void;
}

const today = new Date();

const useMandatListStore = create<StoreTypes>((set) => ({
  //* initial state
  basicid: 0,
  formType: "List",
  formValues: {
    AccountNo: "",
    NameAsPerBank: "",
    MobileAsPerBank: "",
    EmailAsPerBank: "",
    BranchName: "",
    IFSC: "",
    MICR: "",
    EmandateBankCode: "",
    FromDate: formatDate(today),
    ToDate: formatDate(new Date(today.setFullYear(2099))),
    PerDayLimit: "",
    OtherAmount: "",
    UntilCancel: true,
    AuthenticationMode: "N", // "N" for Net Banking and "D" for Debit card
    MandateType: MANDATE_TYPES.Physical,
  },
  mandateList: [],
  singleMandate: {
    AccountNo: "",
    BranchName: "",
    IFSC: "",
    MICR: "",
    FromDate: "",
    ToDate: "",
    PerDayLimit: "",
    UntilCancel: false,
    MandateType: "",
  },
  loading: false,
  memberPurchaseDetails: {},
  selectedMemberDetails: {},
  mandateID: 0,
  member: "",
  setMember: (payload) =>
    set((state) => ({
      ...state,
      member: payload,
    })),
  //* methods for manipulating state
  setBasicid: (payload) =>
    set((state) => ({
      ...state,
      basicid: payload,
    })),
  setFormType: (payload) =>
    set((state) => ({
      ...state,
      formType: payload,
    })),

  setFormValues: (payload) =>
    set((state) => ({
      ...state,
      formValues: { ...state.formValues, ...payload },
    })),

  setMandateList: (payload) =>
    set((state) => ({
      ...state,
      mandateList: payload,
    })),

  setSingleMandate: (payload) =>
    set((state) => ({
      ...state,
      singleMandate: payload,
    })),

  setLoader: (payload) =>
    set((state) => ({
      ...state,
      loading: payload,
    })),

  setSelectedMemberDetails: (payload) =>
    set((state) => ({
      ...state,
      selectedMemberDetails: { ...state.selectedMemberDetails, ...payload },
    })),

  setMemberPurchaseDetails: (payload) =>
    set((state) => ({
      ...state,
      memberPurchaseDetails: { ...state.memberPurchaseDetails, ...payload },
    })),
  setMandateID: (payload: number) =>
    set((state) => ({
      ...state,
      mandateID: payload,
    })),
}));

export default useMandatListStore;
