import Pill from "@ui/Pill/Pill";
import React, { memo } from "react";

function StatusFormatter({ value }: { value: "N" | "Y" }) {
  return (
    <span className={`pill pill_${value === "Y" ? "Active" : "Inactive"}`}>
      <Pill>{value === "Y" ? "Active" : "Inactive"}</Pill>
    </span>
  );
}

export default memo(StatusFormatter);
