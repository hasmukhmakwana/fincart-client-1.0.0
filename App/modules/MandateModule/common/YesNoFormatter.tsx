import Pill from "@ui/Pill/Pill";
import React, { memo } from "react";

function YesNoFormatter({ value }: { value: "N" | "Y" }) {
  return (
    <span className={`pill pill_${value === "Y" ? "Active" : "Inactive"}`}>
      <Pill>{value === "Y" ? "Yes" : "No"}</Pill>
    </span>
  );
}

export default memo(YesNoFormatter);
