import Box from "@ui/Box/Box";
import SelectMenu from "@ui/Select/Select";
import { getAllMemberPurchaseDetails } from "App/api/mandate";
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import React, { useEffect } from "react";
import useMandatListStore from "../store";

const MemberList = () => {
  const user: any = getUser();
  const {
    basicid,
    setBasicid,
    selectedMemberDetails,
    setSelectedMemberDetails,
    memberPurchaseDetails,
    setMember,
    setMemberPurchaseDetails,
  } = useMandatListStore();

  useEffect(() => {
    const fetchMemberList = async () => {
      try {
        const check: boolean = basicid === user?.basicid;

        if (!check) {
          setBasicid(user?.basicid);
          setMemberPurchaseDetails({});
        }
        const enc: any = encryptData({
          basicId: user?.basicid,
          fundid: "0",
        });

        if (!check || !Object.keys(memberPurchaseDetails)?.length) {
          let result = await getAllMemberPurchaseDetails(enc);
          console.log(result?.data,"mandate");
          
          let obj: any = result?.data || {};
          setMember(result?.data?.memberList?.filter((record: any) => record?.basicid === user?.basicid)[0] || [])

          if (Object.keys(obj)?.length) {
            obj.myBackup = obj;
            const profileList: any = obj?.profileList?.filter(
              (i: any) => i.basicid === user?.basicid
            );

            setMemberPurchaseDetails({ ...obj, profileList });
            setSelectedMemberDetails({
              basicid: user?.basicid,
              profileID: result?.data?.["profileList"]?.[0]?.profileID,
            });
          }
        }
      } catch (error: any) {
        console.log(error);
        toastAlert("error", error);
      }
    };
    fetchMemberList();
    return () => { };
  }, []);

  const handleChange = (e: any) => {
    let obj: any = selectedMemberDetails;
    if (e.hasOwnProperty("profileID")) {
      obj = {
        ...obj,
        profileID: e.profileID,
      };
    } else {
      const profileList: any =
        memberPurchaseDetails?.myBackup?.profileList?.filter(
          (i: any) => i.basicid === e.basicid
        );
      setMemberPurchaseDetails({
        ...memberPurchaseDetails,
        profileList: profileList || [],
      });
      obj = {
        ...obj,
        basicid: e.basicid,
        profileID: profileList?.[0]?.profileID,
      };
    }
    setSelectedMemberDetails(obj);
  };

  return (
    <>
      <Box>
        <Box className="row">
          <Box className="col">
            <SelectMenu
              items={memberPurchaseDetails?.memberList || []}
              bindValue="basicid"
              bindName="memberName"
              label="Investor"
              placeholder={"Select"}
              value={selectedMemberDetails?.basicid || null}
              onChange={(e: any) => {
                handleChange(e);
                // console.log(e,"e");
                setMember(e);
              }}
            />
          </Box>
          <Box className="col">
            <SelectMenu
              items={memberPurchaseDetails?.profileList || []}
              bindValue="profileID"
              bindName="InvestProfile"
              label="Investor A/C"
              placeholder={"Select"}
              value={selectedMemberDetails?.profileID || null}
              onChange={handleChange}
            />
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default MemberList;
