import Pill from "@ui/Pill/Pill";
import React, { memo } from "react";

function MandateTypeFormatter({ value }: { value: "P" | "E" }) {
  return (
    <span className={`pill pill_${value === "P" ? "Active" : "Inactive"}`}>
      <Pill>{value === "P" ? "Physical" : "E-Mandate"}</Pill>
    </span>
  );
}

export default memo(MandateTypeFormatter);
