import Box from "@ui/Box/Box";
import useMandatListStore from "../../store";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";

interface DeleteMandateTypes {
  setOpen: (value: boolean) => void;
  handleDeleteMandate: () => void;
}

const DeleteMandate = ({
  setOpen,
  handleDeleteMandate,
}: DeleteMandateTypes) => {
  const { loading } = useMandatListStore();

  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{borderRadius: "15px 15px 0 0"}}>
        <Box className="row">
          <Box className="col-auto text-light"><Text>Delete Mandate Warning</Text></Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box
        className="text-center modal-body modal-body-scroll p-3"
        css={{ padding: "0.5rem" }}
      >
        <Text css={{ mt: 20, pt: 10 }}>
          Are you sure to delete this mandate?
        </Text>
        <Box css={{ mt: 20, pt: 10 }} className="text-end">
          <Button type="submit" color="yellow" onClick={() => setOpen(false)}>
            Cancel
          </Button>
          <Button
            type="submit"
            color="yellow"
            onClick={handleDeleteMandate}
            loading={loading}
          >
            Delete Now
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default DeleteMandate;
