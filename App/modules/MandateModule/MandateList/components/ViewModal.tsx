import Box from "@ui/Box/Box";
import Divider from "@ui/Divider/Divider";
import styles from "../../Mandate.module.scss";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/DataGrid/DataGrid.styles";
import Text from "@ui/Text/Text";
import useMandatListStore from "../../store";
import { encryptData, formatDate } from "App/utils/helpers";
import { useEffect, useState } from "react";
import { toastAlert } from "App/shared/components/Layout";
import { getTransactionList } from "App/api/mandate";
import Link from "next/link";
interface ViewModalTypes { }

const ViewModal: React.FC = () => {
  const { singleMandate } = useMandatListStore();

  const [transactions, settransactions] = useState<any[]>([]);

  useEffect(() => {
    const getList = async () => {
      try {
        const enc: any = encryptData(singleMandate?.ID, true);
        const result: any = await getTransactionList(enc);
        console.log(result?.data);
        settransactions(result?.data?.Transaction_List || []);
      } catch (error: any) {
        console.log(error);
        toastAlert("error", error);
      }
    };
    getList();
  }, [singleMandate]);

  const getValidityText = () => {
    const fromDate = `${singleMandate?.FromDay || "01"}-${singleMandate?.FromMonth || "00"
      }-${singleMandate?.FromYear || "0000"}`;

    const toDate = `${singleMandate?.ToDay || "01"}-${singleMandate?.ToMonth || "00"
      }-${singleMandate?.ToYear || "0000"}`;

    if (singleMandate?.UntilCancel === "N") {
      return fromDate + " To " + toDate;
    }
    return fromDate + " To Until Cancelled";
  };
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{borderRadius: "15px 15px 0 0"}}>
        <Box className="row">
          <Box className="col-auto text-light"><Text>View Details</Text></Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box
        className="modal-body modal-body-scroll p-3"
        css={{ padding: "0.5rem", fontSize: "0.8rem" }}
      >
        <table className={styles.key_value_table}>
          <tbody>
            <tr>
              <th>Mandate Code:</th>
              <td>{singleMandate?.MandateID || ""}</td>
            </tr>
            <tr>
              <th>Bank Account No:</th>
              <td>{singleMandate?.AccountNo || ""}</td>
            </tr>
            <tr>
              <th>IFSC Code:</th>
              <td>{singleMandate?.IFSC || ""}</td>
            </tr>
            <tr>
              <th>MICR Code:</th>
              <td>{singleMandate?.MICR || ""}</td>
            </tr>
            <tr>
              <th>Amount:</th>
              <td>{singleMandate?.PerDayLimit || ""}</td>
            </tr>
            <tr>
              <th>Validity:</th>
              <td>{getValidityText()}</td>
            </tr>
            <tr>
              <th>Signature:</th>
              <td>
                <Link href={`${singleMandate?.signPath}`}>
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    className="link-item"
                  >
                    View
                  </a>
                </Link>
              </td>
            </tr>
          </tbody>
        </table>
        <Divider />
        <Text css={{ mb: 10, }}>
          Linked Transactions - {transactions.length}
        </Text>

        {/* @ts-ignore */}
        <Table bordered css={{ color: "var(--colors-blue1)" }}>
          <ColumnParent css={{ fontWeight: "bolder" }}>
            <ColumnRow>
              <Column>Folio No</Column>
              <Column>Scheme Name</Column>
              <Column>Transaction Type</Column>
              {/* <Column>Amount (Rs.)</Column> */}
            </ColumnRow>
          </ColumnParent>
          <DataParent>
            {transactions.length ? (
              transactions.map((record) => (
                <DataRow>
                  <DataCell>{record?.FolioNo}</DataCell>
                  <DataCell>{record?.SchemeName}</DataCell>
                  <DataCell>{record?.Trxn_Type}</DataCell>
                  {/* <DataCell>1,000</DataCell> */}
                </DataRow>
              ))
            ) : (
              <DataRow>
                <DataCell colSpan={4} align="center">
                  No Data Found!
                </DataCell>
              </DataRow>
            )}
          </DataParent>
        </Table>
      </Box>
    </Box>
  );
};

export default ViewModal;
