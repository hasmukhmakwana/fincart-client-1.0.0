import Box from "@ui/Box/Box";
import useMandatListStore from "../../store";
import { convertBase64 } from "App/utils/helpers";
import { useState } from "react";
import { toastAlert } from "App/shared/components/Layout";
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";

interface UpdateSignTypes {
  setOpen: (value: boolean) => void;
  handleSignUpload: (value: boolean) => void;
}

const UpdateSign = ({ setOpen, handleSignUpload }: UpdateSignTypes) => {
  const { loading, singleMandate } = useMandatListStore();

  const [file, setfile] = useState<any>(null);

  const validateFile = (currentFile: File) => {
    if (!currentFile) return false;

    if (currentFile.type === 'image/png' || currentFile.type === 'image/jpg' || currentFile.type === 'image/jpeg')
      return true;
    else
      return false;
  }

  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light"><Text>Update Signature</Text></Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>

      <Box
        className="modal-body modal-body-scroll p-3 border"
        css={{ padding: "0.5rem", fontSize: "0.8rem" }}
      >
        <Box className="row align-items-end">
          <Box className="col-sm-3"></Box>
          <Box className="col-sm-6">
            <img src={file || ""} width={"100%"} className="mt-4"></img>
            <Box css={{ mt: 10 }}>
              <Input
                parentCSS={{ mb: 0 }}
                type="file"
                className="form-control"
                name="signature"
                accept="image/*"
                onChange={async (e: React.ChangeEvent<HTMLInputElement>) => {
                  try {
                    const file: any = e?.target?.files?.[0] || null;

                    let isValid = validateFile(file);

                    if (isValid) {
                      const base64 = await convertBase64(file);
                      setfile(base64);
                    }
                    else {
                      toastAlert("warn", "The selected file is not a valid image file");
                    }
                  } catch (error: any) {
                    toastAlert("error", error);
                  }
                }}
              />
            </Box>
          </Box>
          <Box className="col-sm-3"></Box>
        </Box>
        <Box className="row justify-content-center">
          <Box className="col-sm-3"></Box>
          <Box className="col-sm-6">
            <span style={{ color: "#3f51b5" }}>Note:-&nbsp;</span>
            Signature should be in image format and without background.
          </Box>
          <Box className="col-sm-3"></Box>
        </Box>
        <Box css={{ pt: 10 }} className="text-end">
          <Button type="submit" color="yellow" onClick={() => setOpen(false)}>
            Cancel
          </Button>
          <Button type="submit" color="yellow" onClick={() => handleSignUpload(file)} loading={loading}>
            Submit
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default UpdateSign;
