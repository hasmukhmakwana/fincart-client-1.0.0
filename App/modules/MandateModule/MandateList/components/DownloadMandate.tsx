import Box from "@ui/Box/Box";
import useMandatListStore from "../../store";
import { downloadFile, encryptData, openTab } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import { downloadMandate } from "App/api/mandate";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";

interface DownloadMandateTypes {
  setOpen: (value: boolean) => void;
}

const DownloadMandate = ({ setOpen }: DownloadMandateTypes) => {
  const { loading, setLoader, singleMandate } = useMandatListStore();

  const getDownloadToken = async (type: "yes" | "no") => {
    try {
      setLoader(true);
      console.log(singleMandate);

      const enc: any = encryptData(singleMandate?.BasicID, true);
      const result = await downloadMandate(enc);

      openTab(
        `https://reports.fincart.com/Mandate.aspx?bid=${singleMandate?.BasicID}&mid=${singleMandate?.MandateID}&sign=${type}&token=${result?.data?.Token}`
      );

      setLoader(false);
      // setOpen(false);
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };
  return (
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light"><Text>Download Mandate</Text></Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box
        className="text-center modal-body modal-body-scroll"
        css={{ padding: "0.5rem" }}
      >
        <Box className="row m-5">
          <Box className="col-12 col-md-6">
            <Button
              css={{ textTransform: "uppercase", letterSpacing: "2px" }}
              type="submit"
              color="yellow"
              onClick={() => getDownloadToken("yes")}
              loading={loading}
            >
              Download with signature
            </Button>
          </Box>
          <Box className="col-12 col-md-6">
            <Button
              css={{ textTransform: "uppercase", letterSpacing: "2px" }}
              type="submit"
              color="yellow"
              onClick={() => getDownloadToken("no")}
              loading={loading}
            >
              Download without signature
            </Button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default DownloadMandate;
