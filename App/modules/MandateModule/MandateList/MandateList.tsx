import React, { FC, useEffect, useState } from "react";
import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import GridWrapper from "App/shared/components/Grid/GridWrapper";
import DataGrid from "@ui/SimpleGrid/DataGrid";
import { actionButtonType, headerObject } from "@ui/SimpleGrid/type";
import EditIcon from "App/icons/EditIcon";
import DeleteIcon from "App/icons/DeleteIcon";
import Info from "App/icons/Info";
import Download from "App/icons/Download";
import {
  deleteMandate,
  getMandateList,
  updateMandateSign,
} from "App/api/mandate";
import YesNoFormatter from "App/modules/MandateModule/common/YesNoFormatter";

import {
  decryptData,
  downloadFile,
  encryptData,
  getUser,
  plainBase64,
} from "App/utils/helpers";
import useMandatListStore from "../store";
import { FormTypes } from "../MandateTypes";
import ViewModal from "./components/ViewModal";
import ModalDialog from "@ui/ModalDialog/ModalDialog";
import styles from "../Mandate.module.scss";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import { useRouter } from "next/router";
import MemberList from "../common/MemberList";

import StatusFormatter from "../common/StatusFormatter";
import MandateTypeFormatter from "../common/MandateTypeFormatter";
import UpdateSign from "./components/UpdateSign";
import DownloaMandate from "./components/DownloadMandate";
import DeleteMandate from "./components/DeleteMandate";
import FileUpload from "App/icons/FileUpload";
import Add from "App/icons/Add";
import useRegistrationStore from "App/modules/RegistrationModule/store";
// import useHeaderStore from "App/shared/components/Header/store";


const header: headerObject[] = [
  {
    field: "MandateID",
    title: "Mandate ID",
    align: "center",
  },
  {
    field: "MandateType",
    title: "Mandate Type",
    formatter: (value) => <MandateTypeFormatter value={value} />,
  },
  {
    field: "Bank",
    title: "Bank",
  },
  {
    field: "AccountNo",
    title: "Account No",
  },
  // {
  //   field: "ProfileName",
  //   title: "Invest A/C",
  // },
  // {
  //   field: "PerDayLimit",
  //   title: "Per Day Limit",
  // },
  // {
  //   field: "UploadStatus",
  //   title: "Verification",
  // },
  // {
  //   field: "ActiveTransaction_Count",
  //   title: "Linked Transactions",
  // },
  // {
  //   field: "UntilCancel",
  //   title: "Until Cancel?",
  //   formatter: (value) => <YesNoFormatter value={value} />,
  // },
  {
    field: "Active",
    title: "Status",
    formatter: (value) => <StatusFormatter value={value} />,
  },
  {
    field: "GridAction",
    title: "Action",
  },
];

const actionButtons: actionButtonType[] = [
  {
    icon: <Info color="#202020" height={14} width={14} className="ActionBtn" />,
    title: "Info",
    tooltip: "Mandate Info",
  },
  {
    icon: <Download color="#202020" height={14} width={14} />,
    title: "Download",
    tooltip: "Download Mandate",
  },
  {
    icon: <FileUpload color="#202020" height={14} width={14} />,
    title: "Edit",
    tooltip: "Update Signature",
  },

  {
    icon: <DeleteIcon color="#202020" height={14} width={14} />,
    title: "Delete",
    tooltip: "Delete Mandate",
    confirmBox: {
      title: "Are you sure ?",
      confirmText: "Yes",
      cancelText: "Cancel",
    },
  },
];

const Module: FC = () => {
  const user: any = getUser();
  const router = useRouter();
  const {
    formType,
    setFormType,
    loading,
    member,
    setLoader,
    mandateList,
    singleMandate,
    setSingleMandate,
    setMandateList,
    selectedMemberDetails,
  } = useMandatListStore();
  const { setBasicID, setRegiType } = useRegistrationStore();
  const [open, setOpen] = useState<boolean>(false);

  useEffect(() => {
    getList(selectedMemberDetails?.basicid || user?.basicid);

    return () => { };
  }, [selectedMemberDetails?.basicid]);

  const getList = async (basicid = null) => {
    // console.log({ basicid });

    try {
      basicid = basicid || selectedMemberDetails?.basicid || user?.basicid;
      if (!basicid) return;

      setLoader(true);
      const enc: any = encryptData(basicid, true);
      const result: any = await getMandateList(enc);
      setLoader(false);
      // console.log(result?.data);
      setMandateList(result?.data?.MandateList || []);
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  /**
   *
   * @update_signature
   */
  const handleSignUpload = async (file: any) => {
    try {
      if (!file) return toastAlert("warn", "Please upload signature!");
      setLoader(true);
      const obj: any = {
        BasicID: user?.basicid,
        Sign_File: plainBase64(file),
      };

      const enc: any = encryptData(obj);
      const result = await updateMandateSign(enc);
      // console.log({ result });
      toastAlert("success", "Signature Updated Successfully!");
      setLoader(false);
      setOpen(false);
      getList();
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  /***
   * @delete_mandate
   */
  const handleDeleteMandate = async () => {
    try {
      setLoader(true);

      const enc: any = encryptData(singleMandate?.ID, true);
      await deleteMandate(enc);
      toastAlert("success", "Mandate Deleted Successfully!");
      setLoader(false);
      setOpen(false);
      getList();
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  /**
   *
   * @action_buttons_click
   */
  const clickOnAction = async (e: FormTypes, index: number) => {
    const item: any = mandateList?.[index];
    setFormType(e);
    setSingleMandate(item);
    setOpen(true);
  };

  return (
    <Layout>
      <PageHeader title="Mandate List" rightContent={<></>} />
      <Box className="row p-4" css={{ backgroundColor: "white", borderRadius: "8px" }}>
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className={styles.mandateList}>
            <Box className="row">
              <Box className="col-12 col-sm-8">
                <MemberList />
              </Box>
              {member?.CAF_Status === "success" ? <>
                {selectedMemberDetails?.profileID &&
                  <Box className="col-12 col-sm-4 d-flex align-content-center align-items-center justify-content-end">
                    <Box className="text-end p-1 justify-content-end">
                      <Button
                        color="yellow"
                        onClick={() => router.push("/Mandate/RegisterMandate")}
                      >
                        Add New
                      </Button>
                    </Box>
                  </Box>}
              </> : <>
                <Box className="col-12 col-sm-4 d-flex align-content-center align-items-center justify-content-end">
                  <Box className="text-end p-1 justify-content-end">
                    <Button
                      color="yellow"
                      onClick={() => {
                        setRegiType("PENDING");
                        setBasicID(member?.basicid);
                        router.push("/Registration");
                      }}
                    >
                      Complete Registration
                    </Button>
                  </Box>
                </Box>
              </>}
            </Box>

            <GridWrapper>
              <DataGrid
                loading={loading}
                header={header || []}
                data={mandateList || []}
                actionButtons={actionButtons || []}
                clickOnAction={clickOnAction}
              />
              <ModalDialog
                open={open}
                setOpen={setOpen}
                css={{
                  "@bp0": { width: "100%" },
                  "@bp1": { width: "40%" },
                }}
              >
                {formType === "Info" && <ViewModal />}
                {formType === "Edit" && (
                  <UpdateSign
                    setOpen={setOpen}
                    handleSignUpload={handleSignUpload}
                  />
                )}
                {formType === "Download" && <DownloaMandate setOpen={setOpen} />}
                {formType === "Delete" && (
                  <DeleteMandate
                    setOpen={setOpen}
                    handleDeleteMandate={handleDeleteMandate}
                  />
                )}
              </ModalDialog>
            </GridWrapper>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default Module;
