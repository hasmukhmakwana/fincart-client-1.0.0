export type FormValueTypes = {
  AccountNo: string;
  BranchName: string;
  IFSC: string;
  MICR: string;
  FromDate: string;
  ToDate: string;
  PerDayLimit: string;
  UntilCancel: boolean;
  MandateType: string;
};

export type AddFormValueTypes = {
  BasicID: string;
  ProfileID: string;
  FromMonth: number;
  FromYear: number;
  ToMonth: number;
  ToYear: number;
  PerDayLimit: number;
  UntilCancel: string;
};

export type FormTypes = "List" | "Info" | "Download" | "Edit" | "Delete";
