import React, { useEffect } from 'react'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout, { toastAlert } from 'App/shared/components/Layout'
import FullFinancialPlanPage from './components/FullFinancialPlanPage';
import { Button } from "@ui/Button/Button";
import { encryptData, getUser } from 'App/utils/helpers';
import style from "./FullFinancialPlan.module.scss";
import {
    getFPPlanId
} from "App/api/financialPlan"
import useFullFinancialPlanStore from './store';
const local: any = getUser();
const FullFinancialPlan = () => {


    return (
        <>
            <Layout>
                <PageHeader title="Full Financial Plan"
                    rightContent={
                        <>
                            {/* <Button
                                color="yellow">
                                Execute My Plan
                            </Button> */}
                        </>
                    } />
                <FullFinancialPlanPage />
            </Layout>
        </>
    )
}

export default FullFinancialPlan