export type GoalBasedSchemes = {
    goalname: String,
    category: String,
    schemeListByMF: [],
    schemeListByOT: []
}

export type GoalListType = {
    Amount: String,
    GetAmount: String,
    GoalStart_Date: String,
    GoalEnd_Date: String,
    goalPriority: String,
    goalYear: String,
    goalname: String,
    goalCode: String,
    TypeCode: String,
    usergoalId: String,
    Duration: String,
    SipTenure: String,
    CurrAmt: String,
    ROR: String,
}

export type MemberListType = {
    memberName: String,
    age: String,
    dob: String,
    RelationName: String,
    CompanyName: String,
    Designation: String,
    profilePic: String,
}