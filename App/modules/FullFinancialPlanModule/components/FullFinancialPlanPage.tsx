import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import { StyledTabs, TabsList, TabsTrigger, TabsContentNoValue } from "@ui/Tabs/Tabs.styles";
import style from "../FullFinancialPlan.module.scss";
import MyDreamsTab from "./Forms/MyDreamsTab";
import CashInflowOutflowTab from "./Forms/CashInflowOutflowTab";
import AvailableResourcesTab from "./Forms/AvailableResourcesTab";
import WayForwardTab from "./Forms/WayForwardTab";
import RevisedCashInflowOutflowTab from "./Forms/RevisedCashInflowOutflowTab";
import SchemeSelectionTab from "./Forms/SchemeSelectionTab";
import PlanAssumptionsTab from "./Forms/PlanAssumptionsTab";
import DisclosuresDisclaimersTab from "./Forms/DisclosuresDisclaimersTab";
import { TabsContent } from "@radix-ui/react-tabs";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "@ui/Accordian/Accordian";
import Emergency from "App/modules/PersonalFinancialPlanModule/components/Forms/Emergency";
import Risk from "App/modules/PersonalFinancialPlanModule/components/Forms/Risk";
import Retirement from "App/modules/PersonalFinancialPlanModule/components/Forms/Retirement";
import clsx from "clsx";
import { encryptData, getUser } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import useFullFinancialPlanStore from "../store";
import { getFPPlanId } from "App/api/financialPlan";
import useHeaderStore from "App/shared/components/Header/store";
import { getMyDreamDetails } from "App/api/fullfinancialplan";
import Loader from "App/shared/components/Loader";

const TAB_TYPES = {
    MyDreams: "My Dreams",
    CashInflowOutflow: "Cash Inflow & Outflow",
    AvailableResources: "Available Resources",
    WayForward: "Summary",
    RevisedCashInflowOutflow: "Revised Cash Inflow & Outflow",
    SchemeSelection: "Scheme Selection",
    PlanAssumptions: "Plan Assumptions",
    DisclosuresDisclaimers: "Disclosures & Disclaimers",
};

const ComponentList = {
    [TAB_TYPES.MyDreams]: MyDreamsTab,
    [TAB_TYPES.CashInflowOutflow]: CashInflowOutflowTab,
    [TAB_TYPES.AvailableResources]: AvailableResourcesTab,
    [TAB_TYPES.WayForward]: WayForwardTab,
    [TAB_TYPES.RevisedCashInflowOutflow]: RevisedCashInflowOutflowTab,
    [TAB_TYPES.SchemeSelection]: SchemeSelectionTab,
    [TAB_TYPES.PlanAssumptions]: PlanAssumptionsTab,
    [TAB_TYPES.DisclosuresDisclaimers]: DisclosuresDisclaimersTab,
};

const FinancialPlanPage = () => {
    const [defaultValue, setdefaultValue] = useState<string>(
        TAB_TYPES.WayForward
    );
    const {
        userInfo
    } = useHeaderStore();
    const [local, setLocal] = useState<any>(userInfo || getUser());
    const [loading, setLoader] = useState<any>(false);
    let planId = "";
    const [tabList, settabList] = useState<string[]>([defaultValue]);

    const goToCurrentTab = (value: any = null) => {
        const check: string = value || defaultValue;

        switch (check) {
            case TAB_TYPES.MyDreams:
                setdefaultValue(TAB_TYPES.MyDreams);
                settabList([...tabList, TAB_TYPES.MyDreams]);
                break;

            case TAB_TYPES.CashInflowOutflow:
                setdefaultValue(TAB_TYPES.CashInflowOutflow);
                settabList([...tabList, TAB_TYPES.CashInflowOutflow]);
                break;

            case TAB_TYPES.AvailableResources:
                setdefaultValue(TAB_TYPES.AvailableResources);
                settabList([...tabList, TAB_TYPES.AvailableResources]);
                break;

            case TAB_TYPES.WayForward:
                setdefaultValue(TAB_TYPES.WayForward);
                settabList([...tabList, TAB_TYPES.WayForward]);
                break;

            case TAB_TYPES.RevisedCashInflowOutflow:
                setdefaultValue(TAB_TYPES.RevisedCashInflowOutflow);
                settabList([...tabList, TAB_TYPES.RevisedCashInflowOutflow]);
                break;

            case TAB_TYPES.SchemeSelection:
                setdefaultValue(TAB_TYPES.SchemeSelection);
                settabList([...tabList, TAB_TYPES.SchemeSelection]);
                break;

            case TAB_TYPES.PlanAssumptions:
                setdefaultValue(TAB_TYPES.PlanAssumptions);
                settabList([...tabList, TAB_TYPES.PlanAssumptions]);
                break;

            case TAB_TYPES.DisclosuresDisclaimers:
                setdefaultValue(TAB_TYPES.DisclosuresDisclaimers);
                settabList([...tabList, TAB_TYPES.DisclosuresDisclaimers]);
                break;
            default:
                break;
        }
    };

    const setCompletedTabStyles = (currentType: string): boolean => {
        return tabList.includes(currentType);
    };

    const setCurrentTabStyles = (currentType: string): boolean => {
        return currentType === defaultValue;
    };

    const getTabContent = () => {
        const Comp: any = ComponentList[defaultValue];
        return (
            <Comp />
        );
    };

    const onTabClick = (tab: string) => {
        setdefaultValue(tab);
    };

    const {
        // loading,
        memberList,
        // setLoader,
        setLocalUser,
        setLocalPlanId,
        setGoalList,
        setMemberList
    } = useFullFinancialPlanStore();

    const fetchPlanId = async () => {
        try {
            setLoader(true);
            const enc: any = encryptData(local?.basicid, true);
            let result: any = await getFPPlanId(enc);
            // console.log("planid");
            // console.log(result?.data?.basicid);
            // setPlanId(result?.data?.basicid);
            planId = result?.data?.basicid;
            setLocalPlanId(result?.data?.basicid);
            setLoader(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error + "asd");
            setLoader(false);
        }
    }

    const fetchDreamDetails = async () => {
        try {
            setLoader(true);
            const obj = {
                basicId: local?.basicid,//"702128",
                planid: planId,//"3214",
                step: 0,
            }
            const enc: any = encryptData(obj);

            let result = await getMyDreamDetails(enc);
            console.log(result, "result")
            setGoalList(result?.data?.goalList);
            setMemberList(result?.data?.memberlist);
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    };

    useEffect(() => {
        (async () => {
            await fetchPlanId();
            await fetchDreamDetails();
        })();
        setLocalUser(local?.basicid)
        return () => {
            setGoalList([]);
            setMemberList([]);
        }
    }, [])
    return (
        <>
            <Box background="">
                <Box className="">
                    <Box className="row">
                        <Box>
                            <StyledTabs
                                className="horizontal">
                                <TabsList className="horizontal d-block" aria-label="View full financial plan">
                                    <TabsTrigger
                                        className={clsx("btn", {
                                            [style.completedTab]: setCompletedTabStyles(
                                                TAB_TYPES.WayForward
                                            ),
                                            [style.runningTab]: setCurrentTabStyles(
                                                TAB_TYPES.WayForward
                                            ),
                                        })}
                                        value={TAB_TYPES.WayForward}
                                        onClick={() => {
                                            onTabClick(TAB_TYPES.WayForward);
                                        }}
                                        data-state="active"
                                    // disabled={!summaryInfo?.Steps?.isBank}
                                    >
                                        Summary
                                        {/* Way Forward */}
                                    </TabsTrigger>
                                    <TabsTrigger
                                        className={clsx("btn", {
                                            [style.completedTab]: setCompletedTabStyles(
                                                TAB_TYPES.MyDreams
                                            ),
                                            [style.runningTab]: setCurrentTabStyles(
                                                TAB_TYPES.MyDreams
                                            )
                                        })}
                                        value={TAB_TYPES.MyDreams}
                                        onClick={() => onTabClick(TAB_TYPES.MyDreams)}

                                    >
                                        My Dreams
                                    </TabsTrigger>
                                    <TabsTrigger
                                        className={clsx("btn", {
                                            [style.completedTab]: setCompletedTabStyles(
                                                TAB_TYPES.CashInflowOutflow
                                            ),
                                            [style.runningTab]: setCurrentTabStyles(
                                                TAB_TYPES.CashInflowOutflow
                                            ),
                                        })}
                                        value={TAB_TYPES.CashInflowOutflow}
                                        onClick={() => onTabClick(TAB_TYPES.CashInflowOutflow)}
                                    // disabled={!summaryInfo?.Steps?.isBasic}
                                    >
                                        Cash Inflow & Outflow
                                    </TabsTrigger>
                                    <TabsTrigger
                                        className={clsx("btn", {
                                            [style.completedTab]: setCompletedTabStyles(
                                                TAB_TYPES.AvailableResources
                                            ),
                                            [style.runningTab]: setCurrentTabStyles(
                                                TAB_TYPES.AvailableResources
                                            ),
                                        })}
                                        value={TAB_TYPES.AvailableResources}
                                        onClick={() => {
                                            onTabClick(TAB_TYPES.AvailableResources);
                                        }}
                                    //disabled={!summaryInfo?.Steps?.isBank}
                                    >
                                        Available Resources
                                    </TabsTrigger>

                                    {/* <TabsTrigger
                                        className={clsx("btn", {
                                            [style.completedTab]: setCompletedTabStyles(
                                                TAB_TYPES.RevisedCashInflowOutflow
                                            ),
                                            [style.runningTab]: setCurrentTabStyles(
                                                TAB_TYPES.RevisedCashInflowOutflow
                                            ),
                                        })}
                                        value={TAB_TYPES.RevisedCashInflowOutflow}
                                        onClick={() => {
                                            onTabClick(TAB_TYPES.RevisedCashInflowOutflow);
                                        }}
                                    >
                                        Revised Cash Inflow & Outflow
                                    </TabsTrigger> */}
                                    <TabsTrigger
                                        className={clsx("btn", {
                                            [style.completedTab]: setCompletedTabStyles(
                                                TAB_TYPES.SchemeSelection
                                            ),
                                            [style.runningTab]: setCurrentTabStyles(
                                                TAB_TYPES.SchemeSelection
                                            ),
                                        })}
                                        value={TAB_TYPES.SchemeSelection}
                                        onClick={() => {
                                            onTabClick(TAB_TYPES.SchemeSelection);
                                        }}
                                    >
                                        Scheme Selection
                                    </TabsTrigger>
                                    <TabsTrigger
                                        className={clsx("btn", {
                                            [style.completedTab]: setCompletedTabStyles(
                                                TAB_TYPES.PlanAssumptions
                                            ),
                                            [style.runningTab]: setCurrentTabStyles(
                                                TAB_TYPES.PlanAssumptions
                                            ),
                                        })}
                                        value={TAB_TYPES.PlanAssumptions}
                                        onClick={() => {
                                            onTabClick(TAB_TYPES.PlanAssumptions);
                                        }}
                                    >
                                        Plan Assumptions
                                    </TabsTrigger>
                                    <TabsTrigger
                                        className={clsx("btn", {
                                            [style.completedTab]: setCompletedTabStyles(
                                                TAB_TYPES.DisclosuresDisclaimers
                                            ),
                                            [style.runningTab]: setCurrentTabStyles(
                                                TAB_TYPES.DisclosuresDisclaimers
                                            ),
                                        })}
                                        value={TAB_TYPES.DisclosuresDisclaimers}
                                        onClick={() => {
                                            onTabClick(TAB_TYPES.DisclosuresDisclaimers);
                                        }}
                                    >
                                        Disclosures Disclaimers
                                    </TabsTrigger>
                                </TabsList>
                                <TabsContentNoValue className="horizontal">
                                    <Card css={{ pb: 25, pt: 12 }}>
                                        <Box className="row ps-2 pe-2">
                                            <Box style={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                {/* @ts-ignore */}
                                                <Text size="h4" className="text-start">{`${defaultValue}`}</Text>
                                                <Box className="text-end">
                                                    <Box className={`mb-1 py-1 ${style.darkBlueBoxText}`}>
                                                        <Text>Family members</Text>
                                                        {loading ? <Box className={`ps-2 pe-1 border-0 text-end ${style.familyMemberList}`}><Loader /></Box> : <>
                                                            {memberList?.filter((rec: any) => rec?.RelationName !== "self").map((mem: any) => {
                                                                return (
                                                                    <span className={`ps-2 pe-1 border-0 text-capitalize ${style.familyMemberList}`} title={`"${mem?.memberName.toUpperCase()}"`}>
                                                                        {mem?.RelationName} , {mem?.age !== '0' ? <>{mem?.age} yrs</> : <>( {mem?.dob.split(" ", 1)} )</>}
                                                                    </span>
                                                                );
                                                            })}
                                                        </>}
                                                    </Box>
                                                </Box>
                                            </Box>
                                            {getTabContent()}
                                        </Box>
                                    </Card>
                                </TabsContentNoValue>
                            </StyledTabs>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default FinancialPlanPage