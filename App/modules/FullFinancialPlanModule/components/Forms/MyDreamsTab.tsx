import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import style from "../../FullFinancialPlan.module.scss";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { encryptData } from "App/utils/helpers";
import { toastAlert } from 'App/shared/components/Layout'
import { getMyDreamDetails } from "App/api/fullfinancialplan"
import useFullFinancialPlanStore from '../../store';
import Switch from "App/ui/Switch/Switch";

const MyDreams = () => {
    const {
        loading,
        localUser,
        localPlanId,
        goalList,
        memberList,
        setLoader,
    } = useFullFinancialPlanStore();
    const [goalLt, setGoalLt] = useState<any>(goalList);
    const [totalDreams, setTotalDreams] = useState<any>(0);
    const [totalCrrAmt, setTotalCrrAmt] = useState<any>(0);
    const [totalMaturityAmt, setTotalMaturityAmt] = useState<any>(0);



    const funPriority = () => {
        let Sortdata = [...goalLt].sort((a, b) => {
            return parseInt(a?.goalPriority) > parseInt(b?.goalPriority) ? 1 : -1;
        })

        setGoalLt(Sortdata);

        // console.log("Priority ", Sortdata);
    }

    const funGoalYear = () => {
        let Sortdata = [...goalLt].sort((a, b) => {
            return parseInt(a?.goalYear) >= parseInt(b?.goalYear) ? 1 : -1;
        })

        setGoalLt(Sortdata);

        // console.log("Year ", Sortdata);
    }

    const fetchDreamDetails = async () => {
        try {
            setLoader(true);
            let TotalDreams: any = goalLt.length;
            let TotalCrrAmt: any = 0;
            let TotalMaturityAmt: any = 0;
            // @ts-ignore
            goalLt.forEach(record => {
                TotalCrrAmt += parseInt(record?.CurrAmt || 0)
            });
            // @ts-ignore
            goalLt.forEach(record => {
                TotalMaturityAmt += parseInt(record?.GetAmount || 0)
            });

            setTotalDreams(TotalDreams);
            setTotalCrrAmt(TotalCrrAmt);
            setTotalMaturityAmt(TotalMaturityAmt);
            setLoader(false);
            // console.log(result?.data);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
    };

    useEffect(() => {
        (async () => {
            await fetchDreamDetails();
        })();

        return () => {
        }
    }, [goalList])


    return (
        <>
            <Card css={{ pb: 25, pt: 12 }}>
                <Box className="row ps-2 pe-2">
                    <Box className="d-flex justify-content-end pe-4 mt-1">
                        {/* <Switch></Switch> */}
                        <Button color="yellow" onClick={() => { funPriority() }}>Priority</Button>
                        <Button color="yellow" onClick={() => { funGoalYear() }}>Year</Button>
                    </Box>
                    <Box className={`mt-2 ms-4 ${style.blueBgBoxDrm}`}>
                        <Box className="row pt-3 pb-3">
                            <Box className="col-12 col-lg-4 text-center">
                                <Box className={style.blueBoxText}>Total Dreams</Box>
                                <Box className={style.blueBoxText2}>{totalDreams}</Box>
                            </Box>
                            <Box className="col-12 col-lg-4 text-center">
                                <Box className={style.blueBoxText}>Total Current Amount (Rs.)</Box>
                                <Box className={style.blueBoxText2}>{Number(totalCrrAmt).toLocaleString("en-IN")}</Box>
                            </Box>
                            <Box className="col-12 col-lg-4 text-center">
                                <Box className={style.blueBoxText}>Total Maturity Amount(Rs.)</Box>
                                <Box className={style.blueBoxText2}>{Number(totalMaturityAmt).toLocaleString("en-IN")}</Box>
                            </Box>
                        </Box>
                    </Box>
                    <Box>
                        <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                            <Table>
                                <ColumnParent
                                    //@ts-ignore  borderRadius: 7 
                                    className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                    <ColumnRow>
                                        <Column className='col-lg-2'>Year</Column>
                                        <Column>My Dreams</Column>
                                        <Column>Current Amount(Rs.)</Column>
                                        <Column>Maturity Amount(Rs.)</Column>
                                        <Column>Priority</Column>
                                    </ColumnRow>
                                </ColumnParent>
                                <DataParent>
                                    {goalLt.length === 0 ? <>
                                        <DataRow>
                                            <DataCell colSpan={5} className="text-center">
                                                No Dreams Found
                                            </DataCell>
                                        </DataRow>
                                    </> : <>
                                        {goalLt.map((record: any) => {
                                            return (
                                                <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                    <DataCell className="text-center" css={{ backgroundColor: "#005CB3", color: "#FFFFFF" }}>{record?.goalYear}</DataCell>
                                                    <DataCell className="text-center">{record?.goalname}</DataCell>
                                                    <DataCell className="text-center">
                                                        {" "}
                                                        {/* &#x20B9;{" "} */}
                                                        {Number(record?.Amount).toLocaleString("en-IN")}</DataCell>
                                                    <DataCell className="text-center">{Number(record?.GetAmount).toLocaleString("en-IN")}</DataCell>
                                                    <DataCell className="text-center">{record?.goalPriority}</DataCell>
                                                </DataRow>
                                            );
                                        })}
                                    </>} </DataParent>
                            </Table>
                        </Box>
                    </Box>
                </Box>
            </Card>
        </>
    )
}

export default MyDreams