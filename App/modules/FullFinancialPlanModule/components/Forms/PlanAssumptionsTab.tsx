import React, { useEffect, useState } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import style from "../../FullFinancialPlan.module.scss";
import Card from "@ui/Card";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { encryptData, getUser } from "App/utils/helpers";
import { getPlanAssumption } from "App/api/fullfinancialplan"
import create from "zustand";
import { Payload } from 'echarts';
import { toastAlert } from 'App/shared/components/Layout';
import useFullFinancialPlanStore from '../../store';
import Loader from 'App/shared/components/Loader';

const PlanAssumptionsTab = () => {
    const [goalAssumption, setgoalAssumption] = useState<any>([]);
    const [productAssumption, setproductAssumption] = useState<any>([]);


    const {
        loading,
        localUser,
        localPlanId,
        setLoader,
    } = useFullFinancialPlanStore();


    const fetchPlanAssumption = async () => {
        try {
            setLoader(true);
            const obj = {
                basicId: localUser,
                planid: localPlanId,
            }
            const enc: any = encryptData(obj);
            // console.log(obj);
            // console.log(localUser);

            let result = await getPlanAssumption(enc);
            // console.log(enc);
            // console.log("test");
            // console.log(result?.data);
            setgoalAssumption(result?.data?.goalAssumption);
            setproductAssumption(result?.data?.productAssumption);
            setLoader(false);
            // console.log(result?.data);
        } catch (error: any) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error);
        }
    };
    useEffect(() => {
        fetchPlanAssumption();
        return () => {
        }
    }, [localUser])

    return (<>
        <Card css={{ pb: 25, pt: 12 }}>
            <Box className="row ps-2 pe-2">

                <Box className="mt-2" style={{ borderBottom: "solid 1px #FAAC05" }}>
                    {/* @ts-ignore */}
                    <Text size="h5" className="text-start pb-2">Goal Assumptions</Text>
                </Box>

                <Box>
                    <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                        <Table>
                            <ColumnParent
                                className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                    <Column className='col-lg-6 ps-5 text-start'>Goal Name</Column>
                                    <Column className='col-lg-3 text-end pe-5'>Inflation Rate</Column>
                                    <Column className='col-lg-3 text-end pe-5'>Growth Rate</Column>

                                </ColumnRow>
                            </ColumnParent>
                            <DataParent>
                                {loading ? <DataRow>
                                    <DataCell colSpan={3} className="text-center">
                                        <Loader />
                                    </DataCell>
                                </DataRow> : <>
                                    {goalAssumption.length === 0 ? <>
                                        <DataRow>
                                            <DataCell colSpan={3} className="text-center">

                                                No Record
                                            </DataCell>
                                        </DataRow>
                                    </> : <>
                                        {goalAssumption.map((record: any) => {
                                            return (
                                                <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                    <DataCell className='col-lg-6 ps-5 text-start'>{record?.goal}</DataCell>
                                                    <DataCell className='col-lg-3 text-end pe-5'>{record?.inflationRate * 100} %</DataCell>
                                                    <DataCell className='col-lg-3 text-end pe-5'>{record?.ror}%</DataCell>
                                                </DataRow>
                                            );
                                        })}
                                    </>}
                                </>}
                            </DataParent>
                        </Table>
                    </Box>
                </Box>

                <Box className="mt-3" style={{ borderBottom: "solid 1px #FAAC05" }}>
                    {/* @ts-ignore */}
                    <Text size="h5" className="text-start pb-1">Product Assumptions</Text>
                </Box>
                <Box>
                    <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                        <Table>
                            <ColumnParent
                                className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                    <Column className='col-lg-9 text-start ps-5'>Product Name</Column>
                                    <Column className='col-lg-3 text-end pe-5'>Growth Rate</Column>
                                </ColumnRow>
                            </ColumnParent>
                            <DataParent>
                                {loading ? <DataRow>
                                    <DataCell colSpan={3} className="text-center">
                                        <Loader />
                                    </DataCell>
                                </DataRow> : <>
                                    {productAssumption.length === 0 ? <>
                                        <DataRow>
                                            <DataCell colSpan={2} className="text-center">

                                                No Record
                                            </DataCell>
                                        </DataRow>
                                    </> : <>
                                        {productAssumption.map((record: any) => {
                                            return (
                                                <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                    <DataCell className="text-start ps-5">{record?.assetName}</DataCell>
                                                    <DataCell className="text-end pe-5">{record?.ror}%</DataCell>
                                                </DataRow>
                                            );

                                        })}
                                    </>}
                                </>}
                            </DataParent>
                        </Table>
                    </Box>
                </Box>
                <Box className="row mx-2">

                    <Box className="col-lg-12 col-md-12 col-sm-12 mb-3 px-0">

                        <Text className="p-2 rounded"
                            // @ts-ignore
                            weight="normal"
                            size="h5"
                            css={{ color: "var(--colors-blue1)" }}
                        >
                            Note:<br />
                            1.None of the returns are guaranteed.<br />
                            2.Rest of the Product are growing as per the Growth Rate of respective goals.
                        </Text>
                    </Box>

                </Box>
            </Box>
        </Card>
    </>

    )
}

export default PlanAssumptionsTab