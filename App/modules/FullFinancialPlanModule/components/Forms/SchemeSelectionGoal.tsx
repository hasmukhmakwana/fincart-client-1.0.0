import React, { useState, useEffect } from 'react'
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import useFullFinancialPlanStore from '../../store';
import { encryptData } from "App/utils/helpers";
import { toastAlert } from 'App/shared/components/Layout'
import { getSchemeDetails } from "App/api/fullfinancialplan";
import Loader from 'App/shared/components/Loader';
const SchemeSelectionGoal = () => {
    const [sortedGoalsData, setSortedGoalsData] = useState<any>([]);
    const {
        loading,
        localUser,
        localPlanId,
        setLoader,
    } = useFullFinancialPlanStore();

    const fetchSchemeSelection = async () => {
        try {
            setLoader(true);
            const obj = {
                basicId: localUser,//"702128",
                planid: localPlanId,//"3214",
                filter: "goal"
            }
            const enc: any = encryptData(obj);
            // console.log("obj");
            // console.log(obj);
            let result = await getSchemeDetails(enc);
            // console.log(enc);
            // console.log(result?.data, "goal");
            //filter data based on goal
            //setSchemeDetails(result?.data);
            assignFilteredData(result?.data?.SchemeData);
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error);
        }
    }

    const assignFilteredData = (records: any) => {
        let prevGoalName = "";
        let counter = 0;
        let currentObjColl: any[] = [];
        let currentObj = {};

        if (records != undefined && records.length > 0) {
            records.map((currentRecord: any) => {
                if (counter == 0) {
                    prevGoalName = currentRecord?.goalname;

                    currentObj = {
                        goalName: currentRecord?.goalname, category: currentRecord?.category,
                        schemeListByMF: records.filter((e: any) => e.category === "MF" && e.goalname === currentRecord.goalname),
                        schemeListByOT: records.filter((e: any) => e.category === "OT" && e.goalname === currentRecord.goalname),
                        schemeListByIN: records.filter((e: any) => e.category === "IN" && e.goalname === currentRecord.goalname)
                    };

                    currentObjColl.push(currentObj);
                }
                else if (prevGoalName != currentRecord?.goalname) {

                    prevGoalName = currentRecord?.goalname;

                    currentObj = {
                        goalName: currentRecord?.goalname, category: currentRecord?.category,
                        schemeListByMF: records.filter((e: any) => e.category === "MF" && e.goalname === currentRecord.goalname),
                        schemeListByOT: records.filter((e: any) => e.category === "OT" && e.goalname === currentRecord.goalname),
                        schemeListByIN: records.filter((e: any) => e.category === "IN" && e.goalname === currentRecord.goalname)
                    };
                    currentObjColl.push(currentObj);
                }
                counter += 1;
            });
            setSortedGoalsData(currentObjColl);
            // console.log("New FilterData");
            // console.log(currentObjColl);
        }
    }

    useEffect(() => {
        fetchSchemeSelection();
        return () => { }
    }, [])
    return (
        <>
            {sortedGoalsData.map((record: any) => {
                return (
                    <>
                        <Box className="text-center mt-3" css={{ color: "#005CB3" }}>
                            {/* @ts-ignore */}
                            {record?.goalName}
                            {/* @ts-ignore
                            <Text size="h5" className="text-start pb-1" css={{ background: "#fffcd8", padding: 10 }}>{record?.category}</Text> */}
                        </Box>
                        {/* record?.schemeListByMF != undefined  
                        Title : Mutual Fund
                        Bind List by schemeListByMF.map*/}
                        <Box>
                            <Box className="mt-3" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-1">Mutual Fund</Text>
                            </Box>
                            {loading ? <><Loader /></> : <>
                                {record?.schemeListByMF?.length > 0 && (<> {/* @ts-ignore */}

                                    <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                        <Table>
                                            <ColumnParent
                                                className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                <ColumnRow css={{ textAlign: "center", borderRadius: "10px" }}>
                                                    <Column>Category</Column>
                                                    <Column>Scheme</Column>
                                                    <Column>Time Horizon</Column>
                                                    <Column>Lumpsum</Column>
                                                    <Column>Monthly SIP</Column>
                                                    <Column>Status</Column>
                                                </ColumnRow>
                                            </ColumnParent>
                                            {record?.schemeListByMF?.map((record: any) => {
                                                return (
                                                    <DataParent>
                                                        <DataCell className="text-center">{record?.objective}</DataCell>
                                                        <DataCell className="text-center">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} Year</DataCell>
                                                        <DataCell className="text-center">{record?.txnAmountLumpsum}</DataCell>
                                                        <DataCell className="text-center">{record?.txnAmountSIP}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataParent>
                                                )
                                            })}

                                        </Table>
                                    </Box>
                                </>)}
                            </>}
                            {record?.schemeListByOT?.length > 0 && (<> {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-1" css={{ background: "#fffcd8", padding: 10 }}>Other Assets</Text>
                                <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                    <Table>
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore  borderRadius: 7 
                                            className="text-capitalize" css={{ borderRadius: "10px", borderEndEndRadius: "10px", borderTopRightRadius: "20%" }}>
                                            <ColumnRow css={{ textAlign: "center", borderRadius: "10px" }}>
                                                <Column>Category</Column>
                                                <Column>Scheme</Column>
                                                <Column>Time Horizon</Column>
                                                <Column>Current Value</Column>
                                                <Column>Additional Contribution</Column>
                                                <Column>Frequency</Column>
                                                <Column>Status</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        {record?.schemeListByOT?.map((record: any) => {
                                            return (
                                                <DataParent>
                                                    <DataCell className="text-center">{record?.objective}</DataCell>
                                                    <DataCell className="text-center">{record?.SchemeName}</DataCell>
                                                    <DataCell className="text-center">{record?.duration} Year</DataCell>
                                                    <DataCell className="text-center">{record?.txnAmountLumpsum}</DataCell>
                                                    <DataCell className="text-center">{record?.txnAmountSIP}</DataCell>
                                                    <DataCell className="text-center">{record?.txnTypeName}</DataCell>
                                                    {record?.status === "Continue" ?
                                                        <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                        <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                            {record?.status}</DataCell>
                                                    }
                                                </DataParent>
                                            )
                                        })}

                                    </Table>
                                </Box>
                            </>)}


                            {record?.schemeListByIN?.length > 0 && (<> {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-1" css={{ background: "#fffcd8", padding: 10 }}>Insurance</Text>
                                <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                    <Table>
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore  borderRadius: 7 
                                            className="text-capitalize" css={{ borderRadius: "10px", borderEndEndRadius: "10px", borderTopRightRadius: "20%" }}>
                                            <ColumnRow css={{ textAlign: "center", borderRadius: "10px" }}>
                                                <Column>Category</Column>
                                                <Column>Scheme</Column>
                                                <Column>Time Horizon</Column>
                                                <Column>Lumpsum</Column>
                                                <Column>Monthly SIP</Column>
                                                <Column>Status</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        {record?.schemeListByIN?.map((record: any) => {
                                            // console.log(record,"ulip insu")
                                            return (
                                                <DataParent>
                                                    <DataCell className="text-center">{record?.objective}</DataCell>
                                                    <DataCell className="text-center">{record?.SchemeName}</DataCell>
                                                    <DataCell className="text-center">{record?.duration} Year</DataCell>
                                                    <DataCell className="text-center">{record?.txnAmountLumpsum}</DataCell>
                                                    <DataCell className="text-center">{record?.txnAmountSIP}</DataCell>
                                                    {record?.status === "Continue" ?
                                                        <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                        <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                            {record?.status}</DataCell>
                                                    }
                                                </DataParent>
                                            )
                                        })}

                                    </Table>
                                </Box>
                            </>)}
                        </Box>

                        {/* record?.schemeListByOT != undefined  
                        Title : Other Assets
                        Bind List by schemeListByMF.map*/}

                    </>
                )
            })}
        </>
    )
}

export default SchemeSelectionGoal