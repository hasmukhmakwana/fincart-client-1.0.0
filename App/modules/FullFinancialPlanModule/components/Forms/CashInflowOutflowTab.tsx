import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import style from "../../FullFinancialPlan.module.scss";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { encryptData } from "App/utils/helpers";
import { toastAlert } from 'App/shared/components/Layout'
import { getMyDreamDetails } from "App/api/fullfinancialplan"
import useFullFinancialPlanStore from '../../store';
import Loader from "App/shared/components/Loader";

let CashInOutTabs = [
    {
        title: "Current",
        value: "Current",
    },
    {
        title: "Recommended",
        value: "Recommended",
    },
];

const CashInflowOutflow = () => {
    //#region CurrentTab
    const [cashIncList, setCashIncList] = useState<any>([]);
    const [cashFixedList, setCashFixedList] = useState<any>([]);
    const [cashDiscretionaryList, setCashDiscretionaryList] = useState<any>([]);
    const [cashSavingList, setCashSavingList] = useState<any>([]);

    const [FixedListAmt, setFixedListAmt] = useState<any>(0);
    const [DiscretionaryListAmt, setDiscretionaryListAmt] = useState<any>(0);
    const [incSavingList, setSavingList] = useState<any>(0);
    const [incMonthAmt, setIncMonthAmt] = useState<any>(0);

    const [totalExpenseMonth, setTotalExpenseMonth] = useState<any>(0);
    //#endregion
    //#region Recommended Tab
    const [cashIncListRev, setCashIncListRev] = useState<any>([]);
    const [cashFixedListRev, setCashFixedListRev] = useState<any>([]);
    const [cashDiscretionaryListRev, setCashDiscretionaryListRev] = useState<any>([]);
    const [cashSavingListRev, setCashSavingListRev] = useState<any>([]);

    const [incMonthAmtRev, setIncMonthAmtRev] = useState<any>(0);
    const [FixedListAmtRev, setFixedListAmtRev] = useState<any>(0);
    const [DiscretionaryListAmtRev, setDiscretionaryListAmtRev] = useState<any>(0);
    const [incSavingListRev, setSavingListRev] = useState<any>(0);
    const [totalExpenseMonthRev, setTotalExpenseMonthRev] = useState<any>(0);
    //#endregion

    const {
        loading,
        localUser,
        localPlanId,
        setLoader,
    } = useFullFinancialPlanStore();

    let TotalYearlyAmt: any = 0;
    let TotalMonthlyAmt: any = 0;
    let FixedYearAmt: any = 0;
    let FixedMonthAmt: any = 0;
    let DiscretionaryYearAmt: any = 0;
    let DiscretionaryMonthAmt: any = 0;
    let SavingsYearAmt: any = 0;
    let SavingsMonthAmt: any = 0;

    let TotalYearlyAmtRev: any = 0
    let TotalMonthlyAmtRev: any = 0
    let FixedYearAmtRev: any = 0;
    let FixedMonthAmtRev: any = 0;
    let DiscretionaryYearAmtRev: any = 0;
    let DiscretionaryMonthAmtRev: any = 0;
    let SavingsYearAmtRev: any = 0;
    let SavingsMonthAmtRev: any = 0;

    const fetchCashInandOutDetails = async () => {
        try {
            setLoader(true);
            const obj = {
                basicId: localUser,//"702128",
                planid: localPlanId,//"3214",
                step: 1,
            }
            const enc: any = encryptData(obj);
            // console.log("obj");
            // console.log(obj);
            // console.log(localUser);
            let result = await getMyDreamDetails(enc);
            // console.log(enc);
            // console.log("ResCashIN", result);

            let objCIN: any = [];
            let objFixed: any = [];
            let objDiscretionary: any = [];
            let objSaving: any = [];

            //@ts-ignore
            result?.data.forEach(record => {
                if (record?.cashFlowType === "CIN") {
                    objCIN.push(record);
                    //TotalYearlyAmt += parseInt(record?.yearlyAmount);
                    TotalMonthlyAmt += parseInt(record?.monthlyAmount);
                    //v+=c1
                    //v2+=c2


                }
                else if (record?.cashFlowType === "COUT") {
                    switch (record?.expenseTypeName) {
                        case "Fixed":
                            objFixed.push(record);
                            FixedMonthAmt += parseInt(record?.monthlyAmount);
                            break;

                        case "Discretionary":
                            objDiscretionary.push(record);
                            DiscretionaryMonthAmt += parseInt(record?.monthlyAmount);
                            break;

                        case "Saving":
                            objSaving.push(record);
                            SavingsMonthAmt += parseInt(record?.monthlyAmount);
                            break;
                    }
                }
            });
            //console.log("inc", objCIN);
            setCashIncList(objCIN);
            setCashFixedList(objFixed);
            setCashDiscretionaryList(objDiscretionary);
            setCashSavingList(objSaving);
            //set total expense
            setIncMonthAmt(TotalMonthlyAmt);
            setFixedListAmt(FixedMonthAmt);
            setDiscretionaryListAmt(DiscretionaryMonthAmt);
            setSavingList(SavingsMonthAmt);
            setTotalExpenseMonth(FixedMonthAmt + DiscretionaryMonthAmt + SavingsMonthAmt);
            setLoader(false);
            // console.log(result?.data);
        } catch (error: any) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error);
        }
    };
    const fetchCashInandOutDetailsRevised = async () => {
        try {
            setLoader(true);
            const obj = {
                basicId: localUser,//"702128",
                planid: localPlanId,//"3214",
                step: 3,
            }
            const enc: any = encryptData(obj);
            console.log("obj");
            console.log(obj);
            console.log(localUser);
            let result = await getMyDreamDetails(enc);
            console.log(enc);
            console.log("ResCashINRev", result);
            let objCINRev: any = [];
            let objFixedRev: any = [];
            let objDiscretionaryRev: any = [];
            let objSavingRev: any = [];

            //@ts-ignore
            result?.data.forEach(record => {
                if (record?.cashFlowType === "CIN") {
                    objCINRev.push(record);
                    TotalMonthlyAmtRev += parseInt(record?.monthlyAmount);
                }
                else if (record?.cashFlowType === "COUT") {
                    switch (record?.expenseTypeName) {
                        case "Fixed":
                            objFixedRev.push(record);
                            FixedMonthAmtRev += parseInt(record?.monthlyAmount);
                            break;

                        case "Discretionary":
                            objDiscretionaryRev.push(record);
                            DiscretionaryMonthAmtRev += parseInt(record?.monthlyAmount);
                            break;

                        case "Saving":
                            objSavingRev.push(record);
                            SavingsMonthAmtRev += parseInt(record?.monthlyAmount);
                            break;
                    }
                }
            });
            setCashIncListRev(objCINRev);
            setCashFixedListRev(objFixedRev);
            setCashDiscretionaryListRev(objDiscretionaryRev);
            setCashSavingListRev(objSavingRev);
            //set total expense
            setIncMonthAmtRev(TotalMonthlyAmtRev);
            setFixedListAmtRev(FixedMonthAmtRev);
            setDiscretionaryListAmtRev(DiscretionaryMonthAmtRev);
            setSavingListRev(SavingsMonthAmtRev);
            setTotalExpenseMonthRev(FixedMonthAmtRev + DiscretionaryMonthAmtRev + SavingsMonthAmtRev);
            setLoader(false);
            //console.log(result?.data);
        } catch (error: any) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error);
        }
    };
    useEffect(() => {
        fetchCashInandOutDetails();
        fetchCashInandOutDetailsRevised();
        return () => {
        }
    }, [localUser])
    return (
        <>
            <Card css={{ pb: 25, pt: 12 }}>
                <Box className="row ps-2 pe-2">

                    <StyledTabs defaultValue="Current">
                        <TabsList
                            aria-label="Manage your account"
                            className="justify-content-left mt-3"
                            css={{
                                borderBottom: "1px solid var(--colors-gray7)",
                                overflowX: "auto",
                            }}
                        >
                            {CashInOutTabs.map((item, index) => {
                                return (
                                    <TabsTrigger
                                        value={item.value}
                                        className="tabs me-3"
                                        key={item.value}
                                        css={{ border: "solid 1px var(--colors-blue1)", height: "25px" }}
                                    >
                                        {/* @ts-ignore */}
                                        <Text size="h5">
                                            {item.title}
                                        </Text>
                                    </TabsTrigger>
                                );
                            })}
                        </TabsList>
                        <TabsContent value="Current">
                            <Box className={`mt-2 ${style.blueBgBox}`}>
                                {/* @ts-ignore */}
                                <Box className="row pt-3 pb-3">
                                    <Box className="col-12 col-lg-4 text-center">
                                        <Box className={style.blueBoxText}>Total Monthly Income(Rs)</Box>
                                        <Box className={style.blueBoxText2}>{Number(incMonthAmt).toLocaleString("en-IN")}</Box>
                                    </Box>
                                    <Box className="col-12 col-lg-4 text-center">
                                        <Box className={style.blueBoxText}>Total Expense Monthly (Rs.)</Box>
                                        <Box className={style.blueBoxText2}>{Number(totalExpenseMonth).toLocaleString("en-IN")}</Box>
                                    </Box>
                                    <Box className="col-12 col-lg-4 text-center">
                                        <Box className={style.blueBoxText}>Total Surplus(Rs.)</Box>
                                        <Box className={style.blueBoxText2}>{Number(incMonthAmt - totalExpenseMonth).toLocaleString("en-IN")}</Box>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="mt-3" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-2">Income Source</Text>
                            </Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center" }}>
                                            <Column className='col-lg-6 ps-5 text-start'>Source</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Yearly</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Monthly</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {loading ? <DataRow>
                                            <DataCell colSpan={3} className="text-center">
                                                <Loader />
                                            </DataCell>
                                        </DataRow> : <>
                                            {cashIncList.length === 0 ? <>
                                                <DataRow>
                                                    <DataCell colSpan={3} className="text-center">
                                                        No Income Source Found
                                                    </DataCell>
                                                </DataRow>
                                            </> : <>
                                                {cashIncList.map((record: any) => {
                                                    TotalYearlyAmt += parseInt(record?.yearlyAmount);
                                                    // TotalMonthlyAmt += parseInt(record?.monthlyAmount);
                                                    return (
                                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                            <DataCell className='col-lg-6 ps-5 text-start'>{record?.cashFlowName}</DataCell>
                                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.yearlyAmount).toLocaleString("en-IN")}</DataCell>
                                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.monthlyAmount).toLocaleString("en-IN")}</DataCell>
                                                        </DataRow>
                                                    );
                                                })}
                                            </>}</>}
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4", backgroundColor: "var(--colors-blue1)", color: "white" }}>
                                            <DataCell className='col-lg-6 ps-5 text-start'>Total</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(TotalYearlyAmt).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(incMonthAmt).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>
                            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-2">Fixed Expense</Text>
                            </Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-6 ps-5 text-start'>Source</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Yearly</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Monthly</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>{loading ? <DataRow>
                                        <DataCell colSpan={3} className="text-center">
                                            <Loader />
                                        </DataCell>
                                    </DataRow> : <>
                                        {cashFixedList.length === 0 ? <>
                                            <DataRow>
                                                <DataCell colSpan={3} className="text-center">
                                                    No Fixed Expense Found
                                                </DataCell>
                                            </DataRow>
                                        </> : <>
                                            {cashFixedList.map((record: any) => {
                                                FixedYearAmt += parseInt(record?.yearlyAmount);
                                                // FixedMonthAmt += parseInt(record?.monthlyAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className='col-lg-6 ps-5 text-start'>{record?.cashFlowName}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.yearlyAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.monthlyAmount).toLocaleString("en-IN")}</DataCell>
                                                    </DataRow>
                                                )
                                            })}
                                        </>}</>}
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4", backgroundColor: "var(--colors-blue1)", color: "white" }}>
                                            <DataCell className='col-lg-6 ps-5 text-start'>Total</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(FixedYearAmt).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(FixedListAmt).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>
                            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-2">Discretionary Expense</Text>
                            </Box>

                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-6 ps-5 text-start'>Source</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Yearly</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Monthly</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {loading ? <DataRow>
                                            <DataCell colSpan={3} className="text-center">
                                                <Loader />
                                            </DataCell>
                                        </DataRow> : <>
                                            {cashDiscretionaryList.map((record: any) => {
                                                DiscretionaryYearAmt += parseInt(record?.yearlyAmount);
                                                // DiscretionaryMonthAmt += parseInt(record?.monthlyAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className='col-lg-6 ps-5 text-start'>{record?.cashFlowName}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.yearlyAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.monthlyAmount).toLocaleString("en-IN")}</DataCell>
                                                    </DataRow>
                                                )
                                            })}</>}
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4", backgroundColor: "var(--colors-blue1)", color: "white" }}>
                                            <DataCell className='col-lg-6 ps-5 text-start'>Total</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(DiscretionaryYearAmt).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(DiscretionaryListAmt).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>
                            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-2">Committed Savings</Text>
                            </Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-6 ps-5 text-start'>Source</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Yearly</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Monthly</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {loading ? <DataRow>
                                            <DataCell colSpan={3} className="text-center">
                                                <Loader />
                                            </DataCell>
                                        </DataRow> : <>
                                            {cashSavingList.map((record: any) => {
                                                SavingsYearAmt += parseInt(record?.yearlyAmount);
                                                // SavingsMonthAmt += parseInt(record?.monthlyAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className='col-lg-6 ps-5 text-start'>{record?.cashFlowName}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.yearlyAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.monthlyAmount).toLocaleString("en-IN")}</DataCell>
                                                    </DataRow>
                                                )
                                            })}</>}

                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4", backgroundColor: "var(--colors-blue1)", color: "white" }}>
                                            <DataCell className='col-lg-6 ps-5 text-start'>Total</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(SavingsYearAmt).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(incSavingList).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>
                        </TabsContent>
                        <TabsContent value="Recommended">
                            <Box className={`mt-2 ${style.blueBgBox}`}>
                                {/* @ts-ignore */}
                                <Box className="row pt-3 pb-3">
                                    <Box className="col-12 col-lg-4 text-center">
                                        <Box className={style.blueBoxText}>Total Monthly Income(Rs)</Box>
                                        <Box className={style.blueBoxText2}>{Number(incMonthAmtRev).toLocaleString("en-IN")}</Box>
                                    </Box>
                                    <Box className="col-12 col-lg-4 text-center">
                                        <Box className={style.blueBoxText}>Total Expense Monthly (Rs.)</Box>
                                        <Box className={style.blueBoxText2}>{Number(totalExpenseMonthRev).toLocaleString("en-IN")}</Box>
                                    </Box>
                                    <Box className="col-12 col-lg-4 text-center">
                                        <Box className={style.blueBoxText}>Total Surplus(Rs.)</Box>
                                        <Box className={style.blueBoxText2}>{Number(incMonthAmtRev - totalExpenseMonthRev).toLocaleString("en-IN")}</Box>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="mt-3" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-2">Income Source</Text>
                            </Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-6 ps-5 text-start'>Source</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Yearly</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Monthly</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {loading ? <DataRow>
                                            <DataCell colSpan={3} className="text-center">
                                                <Loader />
                                            </DataCell>
                                        </DataRow> : <>
                                            {cashIncListRev.map((record: any) => {
                                                TotalYearlyAmtRev += parseInt(record?.yearlyAmount);
                                                // TotalMonthlyAmtRev += parseInt(record?.monthlyAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className='col-lg-6 ps-5 text-start'>{record?.cashFlowName}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.yearlyAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.monthlyAmount).toLocaleString("en-IN")}</DataCell>
                                                    </DataRow>
                                                )
                                            })}</>}
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4", backgroundColor: "var(--colors-blue1)", color: "white" }}>
                                            <DataCell className='col-lg-6 ps-5 text-start'>Total</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(TotalYearlyAmtRev).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(incMonthAmtRev).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>

                            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-2">Fixed Expense</Text>
                            </Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-6 ps-5 text-start'>Source</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Yearly</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Monthly</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {loading ? <DataRow>
                                            <DataCell colSpan={3} className="text-center">
                                                <Loader />
                                            </DataCell>
                                        </DataRow> : <>
                                            {cashFixedListRev.map((record: any) => {
                                                FixedYearAmtRev += parseInt(record?.yearlyAmount);
                                                // FixedMonthAmtRev += parseInt(record?.monthlyAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className='col-lg-6 ps-5 text-start'>{record?.cashFlowName}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.yearlyAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.monthlyAmount).toLocaleString("en-IN")}</DataCell>
                                                    </DataRow>
                                                )
                                            })}</>}
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4", backgroundColor: "var(--colors-blue1)", color: "white" }}>
                                            <DataCell className='col-lg-6 ps-5 text-start'>Total</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(FixedYearAmtRev).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(FixedListAmtRev).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>

                            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-2">Discretionary Expense</Text>
                            </Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-6 ps-5 text-start'>Source</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Yearly</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Monthly</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {loading ? <DataRow>
                                            <DataCell colSpan={3} className="text-center">
                                                <Loader />
                                            </DataCell>
                                        </DataRow> : <>
                                            {cashDiscretionaryListRev.map((record: any) => {
                                                DiscretionaryYearAmtRev += parseInt(record?.yearlyAmount);
                                                // DiscretionaryMonthAmtRev += parseInt(record?.monthlyAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className='col-lg-6 ps-5 text-start'>{record?.cashFlowName}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.yearlyAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.monthlyAmount).toLocaleString("en-IN")}</DataCell>
                                                    </DataRow>
                                                )
                                            })}</>}

                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4", backgroundColor: "var(--colors-blue1)", color: "white" }}>
                                            <DataCell className='col-lg-6 ps-5 text-start'>Total</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(DiscretionaryYearAmtRev).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(DiscretionaryListAmtRev).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>


                            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-start pb-2">Committed Savings</Text>
                            </Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center" }}>
                                            <Column className='col-lg-6 ps-5 text-start'>Source</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Yearly</Column>
                                            <Column className='col-lg-3 text-end pe-5'>Monthly</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {loading ? <DataRow>
                                            <DataCell colSpan={3} className="text-center">
                                                <Loader />
                                            </DataCell>
                                        </DataRow> : <>
                                            {cashSavingListRev.map((record: any) => {
                                                SavingsYearAmtRev += parseInt(record?.yearlyAmount);
                                                // SavingsMonthAmtRev += parseInt(record?.monthlyAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className='col-lg-6 ps-5 text-start'>{record?.cashFlowName}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.yearlyAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className='col-lg-3 text-end pe-5'>{Number(record?.monthlyAmount).toLocaleString("en-IN")}</DataCell>
                                                    </DataRow>
                                                )
                                            })}</>}
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4", backgroundColor: "var(--colors-blue1)", color: "white" }}>
                                            <DataCell className='col-lg-6 ps-5 text-start'>Total</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(SavingsYearAmtRev).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className='col-lg-3 text-end pe-5'>{Number(incSavingListRev).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>
                        </TabsContent>
                    </StyledTabs>
                </Box>
            </Card>
        </>
    )
}

export default CashInflowOutflow
