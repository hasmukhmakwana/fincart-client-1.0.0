import { Label } from '@radix-ui/react-dropdown-menu';
import Box from '@ui/Box/Box';
import React from 'react';
import style from "../../FullFinancialPlan.module.scss";

const DisclosuresDisclaimersTab = () => {
    return (
        <>
            <Box className="row mt-2">
                <Box className="col-12">
                    <Box className={`${style.bodyContent}`}>
                        Fincart has no control over the accuracy of the information provided by clients. Fincart does NOT guarantee
                        the results for your plan. the calculation is as per the data provided by client & necessary assumptions.
                        Estimates of income and growth in the plan will be based on assessments of prevailing economic conditions
                        and investment manager performance. However no guarantee of future performance will be given and results
                        may vary from the estimates shown. The figures in the plan include various estimates with respect to taxation
                        and other laws which we believe to be relevant. This service agreement is not enforceable by law and is a
                        document of trust and faith.
                    </Box>
                </Box>
            </Box>
            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                <Label className={`text-start pb-2 ${style.headersLabel}`}>Purpose and Nature of this Report</Label>
            </Box>

            <Box className="row">
                <Box className="col-12">
                    <Box className={`${style.bodyContent}`}>
                        This confidential report has been developed to provide you background information on issues you may want
                        to discuss further as part of your personal financial planning. You may be familiar with some of these subjects.
                        Others may be new to you and may present aspects that you have not yet considered. This financial plan is a
                        general plan based on information you supplied. Where we had no information from you, we made a few
                        general assumptions which may not be suitable for you depending on your own specific needs and
                        circumstances. The usefulness of this analysis will depend on the accuracy and completeness of that
                        information. The results provide: An overall view of your present financial position. An analysis of your
                        family's future financial needs. An examination of the impact of time and potential inflation. An evaluation of
                        your goals to see if they are practical and achievable. Possible adjustments or changes to help achieve your
                        goals. This financial analysis will show you how much money you need to save and invest, the rate of return
                        needed to reach your goals, and how much insurance you need, if any. It will also show you how to carry out
                        your course of action. Setting goals, planning, implementing and monitoring, results in a complete program.
                        During the plan delivery session, you may discuss more facts and circumstances relevant to your family's
                        finances that may alter the recommendations made here. The overall results you achieve when you carry
                        out some or all of the recommendations contained in this plan. Changes that occur in your financial
                        circumstances as well as in the economy can affect this plan. Information about market values reflects
                        values on the date provided by you. It is important that you keep track of your progress and make changes
                        as needs arise; therefore we recommend annual updates. As with any plan, the final responsibility for any
                        action you take rests with you.
                    </Box>
                </Box>
            </Box>

            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                <Label className={`text-start pb-2 ${style.headersLabel}`}>Important</Label>
            </Box>

            <Box className="row">
                <Box className="col-12">
                    <Box className={`${style.bodyContent}`}>
                        The projections or other information made by Plan Builder about the likelihood of various investment outcomes
                        are hypothetical in nature, and do not reflect actual investment results and are not guarantees of future results.
                    </Box>
                </Box>
            </Box>

            <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                <Label className={`text-start pb-2 ${style.headersLabel}`}>Privacy</Label>
            </Box>

            <Box className="row">
                <Box className="col-12">
                    <Box className={`${style.bodyContent}`}>
                        We treat the information gathered during the planning process as strictly confidential and will use any such
                        information only for business purposes you have with me. The information obtained will not be disclosed to
                        any unaffiliated third-party except if such information is required by law or regulatory process or authorized
                        by you in writing.
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default DisclosuresDisclaimersTab