import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import ReactECharts from "echarts-for-react";
import style from "../../FullFinancialPlan.module.scss";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import SelectMenu from "@ui/Select/Select";
import useFullFinancialPlanStore from '../../store';
import { encryptData } from "App/utils/helpers";
import { toastAlert } from 'App/shared/components/Layout'
import { getSchemeDetails } from "App/api/fullfinancialplan";
import SchemeSelectionGoal from "./SchemeSelectionGoal";
import SchemeSelectionCategory from "./SchemeSelectionCategory";


const SchemeSelectionTab = () => {
    // const { loading, setLoader, accumulationChart, accumulationChartData, setAccumulationChart, setAccumulationChartData } = useRetirementStore();
    const [categoryMFChart, setCategoryMFChart] = useState<any>([]);
    const [categoryAssetAllocationChart, setCategoryAssetAllocationChart] = useState<any>([]);

    const {
        loading,
        localUser,
        localPlanId,
        setLoader,
    } = useFullFinancialPlanStore();

    const fetchSchemeSelection = async () => {
        try {
            setLoader(true);
            const obj = {
                basicId: localUser,//"702128",
                planid: localPlanId,//"3214",
                filter: "category"
            }
            const enc: any = encryptData(obj);
            // console.log("obj");
            // console.log(obj);
            const result: any = await getSchemeDetails(enc);
            // console.log(enc);
            // console.log(result?.data, "New Dat");
            // let MFChart: any = [];
            // result?.data?.map((ele: any) => {
            //     MFChart.push(ele?.MFChart)
            // })
            // console.log(MFChart, "MFChart");
            setCategoryMFChart(result?.data?.MFChart);
            setCategoryAssetAllocationChart(result?.data?.AssetAllocation)
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error);
        }
    }
    useEffect(() => {
        fetchSchemeSelection();
        return () => { }
    }, [])
    const [sortCategory, setSortCategory] = useState<any>("1");
    const [Show, setShow] = useState(true);
    const AllAssetsAllocation = {
        title: {
            left: 'center'
        },
        tooltip: {
            trigger: 'item'
        },
        series: [
            {
                //name: 'Access From',
                type: 'pie',
                radius: '75%',
                data: [
                    { value: categoryAssetAllocationChart?.MF || "0", name: 'MF' },
                    { value: categoryAssetAllocationChart?.ULIP || "0", name: 'ULIP' },
                    { value: categoryAssetAllocationChart?.EPF || "0", name: 'EPF' },
                    { value: categoryAssetAllocationChart?.Other_Asset || "0", name: 'Other Asset:' },
                    { value: categoryAssetAllocationChart?.Endowment_plan || "0", name: 'Endowment Plan' },
                    { value: categoryAssetAllocationChart?.Pension_plan || "0", name: 'Pension Plan' },
                    { value: categoryAssetAllocationChart?.PPF || "0", name: 'PPF(Monthly)' },
                    { value: categoryAssetAllocationChart?.PMS || "0", name: 'PMS' }
                ],
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

    const MFAllocation = {
        title: {
            left: 'center'
        },
        tooltip: {
            trigger: 'item'
        },
        series: [
            {
                //name: 'Access From',
                type: 'pie',
                radius: '75%',
                data: [
                    { value: categoryMFChart?.equity || "0", name: 'Equity' },
                    { value: categoryMFChart?.hybrid|| "0", name: 'Hybrid' },
                    { value: categoryMFChart?.debt|| "0", name: 'Debt' },

                ],
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };


    return (
        <>
            {/* {console.log(categoryMFChart, "categoryMFChart")} */}

            <Card css={{ pb: 25, pt: 12 }}>
                <Box className="row ps-2 pe-2">
                 
                    {/* Graph Section Start */}
                    <Box className="row mt-3">
                        <Box className="col-6 text-center pe-0">
                            {/* @ts-ignore */}
                            <Text size="h5" className="pb-1" css={{ color: "#005CB3" }}>All Assets Allocation</Text>
                            <Box>
                                <ReactECharts
                                    option={AllAssetsAllocation}
                                />
                            </Box>
                        </Box>
                        {/* <Box className="col-2 ps-0 pe-0">
                            <Button
                                color="yellow">
                                Category
                            </Button>
                            <Button
                                color="yellow">
                                Goals
                            </Button>
                        </Box> */}
                        <Box className="col-6 text-center ps-0">
                            {/* @ts-ignore */}
                            <Text size="h5" className="pb-1" css={{ color: "#005CB3" }}>Mutual Fund Allocation</Text>
                            <Box>
                                <ReactECharts
                                    option={MFAllocation}
                                />
                            </Box>
                        </Box>
                    </Box>
                    {/* Graph Section End */}
                    <Box className="mt-2 mb-2" style={{ borderBottom: "solid 2px #B3DAFF" }}></Box>
                    <Box className="row justify-content-between">
                        <Box className="mt-3 col-lg-3" css={{ color: "#005CB3" }}>Your Schemes</Box>
                        <Box className="mt-3 col-lg-3 text-end px-0" css={{ color: "#005CB3" }}>
                            <SelectMenu
                                items={[
                                    { id: "1", Category: "Category" },
                                    { id: "2", Category: "Goal" },
                                ]}
                                bindValue={"id"}
                                bindName={"Category"}
                                label={"Sort By"}
                                name={"Category"}
                                value={sortCategory}
                                onChange={(e: any) => {
                                    // { e?.id === 2 ?<SchemeSelectionGoal/> : "" }
                                    if (e?.id == 2) {
                                        // <SchemeSelectionGoal />
                                        setShow(false);
                                        // console.log("Goal", e);
                                    } else {
                                        // <SchemeSelectionCategory />
                                        setShow(true);
                                        // console.log("Category", e);
                                    }
                                    setSortCategory(e?.id || "")
                                }}
                            /></Box>
                        {Show ?
                            <><SchemeSelectionCategory /></> :
                            <> <SchemeSelectionGoal /></>}
                    </Box>
                </Box>
            </Card>
        </>
    )
}

export default SchemeSelectionTab