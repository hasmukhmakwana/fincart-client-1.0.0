import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import Emergency from "App/modules/PersonalFinancialPlanModule/components/Forms/Emergency";
import Risk from "App/modules/PersonalFinancialPlanModule/components/Forms/Risk";
import Retirement from "App/modules/PersonalFinancialPlanModule/components/Forms/Retirement";
import Goals from "App/modules/PersonalFinancialPlanModule/components/Forms/Goals";
import Estate from "App/modules/PersonalFinancialPlanModule/components/Forms/Estate";
import { getSummaryDetails } from "App/api/fullfinancialplan";
import useFullFinancialPlanStore from '../../store';
import { encryptData } from "App/utils/helpers";
import { toastAlert } from 'App/shared/components/Layout'

let FinancialPlanTabs = [
    {
        title: "Emergency",
        value: "Emergency",
    },
    {
        title: "Risk",
        value: "Risk",
    },
    {
        title: "Goals",
        value: "Goals",
    },
    {
        title: "Retirement",
        value: "Retirement",
    },
    {
        title: "Estate",
        value: "Estate",
    },
];
const WayForwardTab = () => {
    const {
        loading,
        localUser,
        localPlanId,
        setLoader,
    } = useFullFinancialPlanStore();

    const [goalEmergency, setGoalEmergency] = useState<any>([]);
    const [goalRetirement, setGoalRetirement] = useState<any>([]);
    const [goals, setGoals] = useState<any>([]);
    const [riskDetails, setRiskDetails] = useState<any>([]);
    const [riskTabContentDetails, setRiskTabContentDetails] = useState<any>([]);
    const [emergencyDetails, setEmergencyDetails] = useState<any>([]);
    const [retirementDetails, setRetirementDetails] = useState<any>([]);
    const [goalWiseDetails, setGoalWiseDetails] = useState<any>([]);
    const [policyBeneficiaries, setPolicyBeneficiaries] = useState<any>([]);
    const [retirementCal, setRetirementCal] = useState<any>([]);

    const fetchSummaryDetails = async () => {
        try {
            // console.log("SummaryStarts");
            setLoader(true);
            const obj = {
                basicId: localUser,//"702128",
                planid: localPlanId//"3214",
            }
            const enc: any = encryptData(obj);
          
            let result = await getSummaryDetails(enc);
          
            let objEmergency: any = [];
            let objRetirement: any = [];
            let objGoal: any = [];
            //@ts-ignore
            //All Accordian Data
            result?.data?.goalDetails?.forEach(record => {
                switch (record?.goalType) {
                    case "Emergency":
                        objEmergency.push(record);
                        break;

                    case "Retirement":
                        objRetirement.push(record);
                        break;

                    default:
                        objGoal.push(record);
                        break;
                }
            });
            setRiskDetails(result?.data?.riskBrief);
            setRiskTabContentDetails(result?.data?.riskwiseAssets)
            setPolicyBeneficiaries(result?.data?.PolicyBeneficiaries)
            setRetirementCal(result?.data?.RetirementCalculation)
            setGoalEmergency(objEmergency);
            setGoalRetirement(objRetirement);
            setGoals(objGoal);

            let objEmergencyTab: any = [];
            let objGoalsTab: any = [];
            let objRetirementTab: any = [];
            //@ts-ignore
            //Emergency Retirement and Goals tab content data
            result?.data?.goalwiseDetails?.forEach(record => {
                switch (record.goalname) {
                    case "Emergency":
                        objEmergencyTab.push(record);
                        break;

                    case "Retirement":
                        objRetirementTab.push(record);
                        break;

                    default:
                        objGoalsTab.push(record);
                        break;
                }
            });
            setGoalWiseDetails(objGoalsTab);
            setEmergencyDetails(objEmergencyTab);
            setRetirementDetails(objRetirementTab);
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
    };
    useEffect(() => {
        if (localUser && localPlanId) {
            fetchSummaryDetails();
        }
        return () => { }
    }, [localUser, localPlanId])

    return (
        <>
            <Box className="container">
                <Box className="row">
                    <Box className="col-md-12 col-sm-12 col-lg-12">
                        <Card>
                            <StyledTabs defaultValue="Emergency">
                                <TabsList
                                    aria-label="Manage your account"
                                    className="justify-content-center mt-3"
                                    css={{
                                        borderBottom: "1px solid var(--colors-gray7)",
                                        overflowX: "auto",
                                    }}
                                >
                                    {FinancialPlanTabs.map((item, index) => {
                                        return (
                                            <TabsTrigger
                                                value={item.value}
                                                className="tabs me-3"
                                                key={item.value}
                                                css={{ border: "solid 1px var(--colors-blue1)", height: "25px" }}
                                            >
                                                {/* @ts-ignore */}
                                                <Text size="h5">
                                                    {item.title}
                                                </Text>
                                            </TabsTrigger>
                                        );
                                    })}
                                </TabsList>
                                <TabsContent value="Emergency" className="mt-2"><Emergency fetchEmergencyData={goalEmergency} fetchEmergencyTabData={emergencyDetails} /></TabsContent>
                                <TabsContent value="Risk" className="mt-2"><Risk fetchRiskData={riskDetails} fetchRiskTabData={riskTabContentDetails} fetchPolicyBene={policyBeneficiaries}/></TabsContent>
                                <TabsContent value="Goals" className="mt-2"><Goals fetchGoalData={goals} fetchGoalTabData={goalWiseDetails} setFilteredData={setGoals} /></TabsContent>
                                <TabsContent value="Retirement" className="mt-2"><Retirement fetchRetirementData={goalRetirement} fetchRetirementTabData={retirementDetails} fetchRetirementCal={retirementCal} /></TabsContent>
                                <TabsContent value="Estate" className="mt-2"><Estate /></TabsContent>
                            </StyledTabs>
                        </Card>
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default WayForwardTab