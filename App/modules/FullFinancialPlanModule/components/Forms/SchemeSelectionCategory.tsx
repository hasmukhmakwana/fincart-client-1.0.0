import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import useFullFinancialPlanStore from '../../store';
import { encryptData } from "App/utils/helpers";
import { toastAlert } from 'App/shared/components/Layout'
import { getSchemeDetails } from "App/api/fullfinancialplan";
import Loader from "App/shared/components/Loader";

const SchemeSelectionCategory = () => {
    const [schemeDetails, setSchemeDetails] = useState<any>([]);
    const {
        loading,
        localUser,
        localPlanId,
        setLoader,
    } = useFullFinancialPlanStore();

    const fetchSchemeSelection = async () => {
        try {
            setLoader(true);
            const obj = {
                basicId: localUser,//"702128",
                planid: localPlanId,//"3214",
                filter: "category"
            }
            const enc: any = encryptData(obj);
            // console.log("obj");
            // console.log(obj);
            let result = await getSchemeDetails(enc);
            // console.log(enc);
            // console.log(result?.data, "category");
            setSchemeDetails(result?.data?.SchemeData);
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            setLoader(false);
            toastAlert("error", error);
        }
    }
    useEffect(() => {
        fetchSchemeSelection();
        return () => { }
    }, [])
    return (
        <>
            <Box className="text-center mt-1" css={{ color: "#005CB3" }}>Mutual Fund</Box>
            {loading ?
                <Box className="my-2" css={{ borderBottom: "solid 1px #FAAC05" }}>
                    {/* @ts-ignore */}
                    <Text size="h5" className="text-center pb-1"><Loader /></Text>
                </Box> : <>
                    {schemeDetails?.filter((record: any) => record?.objective === "Debt").length !== 0 ? <>
                        <Box style={{ borderBottom: "solid 1px #FAAC05" }}>
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-start pb-1">Mutual Fund - Debt</Text>
                        </Box>
                        <Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-3 text-start ps-2'>Scheme</Column>
                                            <Column>Time Horizon</Column>
                                            <Column>Lumpsum</Column>
                                            <Column>Monthly SIP</Column>
                                            <Column>Goal Name</Column>
                                            <Column>Status</Column>

                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {schemeDetails?.map((record: any) => {
                                            if (record?.objective === "Debt") {
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="text-start">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} year</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountLumpsum).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountSIP).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{record?.goalname}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataRow>
                                                )
                                            }
                                        })}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </> : null}
                    {schemeDetails?.filter((record: any) => record?.objective === "Equity").length !== 0 ? <>
                        <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-start pb-1">Mutual Fund - Equity</Text>
                        </Box>
                        <Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-3 text-start ps-2'>Scheme</Column>
                                            <Column>Time Horizon</Column>
                                            <Column>Lumpsum</Column>
                                            <Column>Monthly SIP</Column>
                                            <Column>Goal Name</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {schemeDetails?.map((record: any) => {
                                            if (record?.objective === "Equity") {
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="text-start">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} year</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountLumpsum).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountSIP).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{record?.goalname}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataRow>
                                                )
                                            }
                                        })}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </> : null}
                    {schemeDetails?.filter((record: any) => record?.objective === "Hybrid").length !== 0 ? <>
                        <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-start pb-1">Mutual Fund - Debt</Text>
                        </Box>
                        <Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-3 text-start ps-2'>Scheme</Column>
                                            <Column>Time Horizon</Column>
                                            <Column>Lumpsum</Column>
                                            <Column>Monthly SIP</Column>
                                            <Column>Goal Name</Column>
                                            <Column>Status</Column>

                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {schemeDetails?.map((record: any) => {
                                            if (record?.objective === "Hybrid") {
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="text-start">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} year</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountLumpsum).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountSIP).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{record?.goalname}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataRow>
                                                )
                                            }
                                        })}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </> : null}
                </>
            }
            <Box className="text-center mt-3" css={{ color: "#005CB3" }}>Other Assets</Box>
            {loading ?
                <Box className="my-2" css={{ borderBottom: "solid 1px #FAAC05" }}>
                    {/* @ts-ignore */}
                    <Text size="h5" className="text-center pb-1"><Loader /></Text>
                </Box> : <>
                    {schemeDetails?.filter((record: any) => record?.objective === "Pension Plan").length !== 0 ? <>
                        <Box className="mt-3" style={{ borderBottom: "solid 1px #FAAC05" }}>
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-start pb-1">Pension Plan</Text>
                        </Box>
                        <Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-3 text-start ps-2'>Scheme</Column>
                                            <Column>Time Horizon</Column>
                                            <Column>Current Value</Column>
                                            <Column>Additional Contribution</Column>
                                            <Column>Frequency</Column>
                                            <Column>Goal Name</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {schemeDetails?.map((record: any) => {
                                            if (record?.objective === "Pension Plan") {
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="text-start">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} year</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountLumpsum).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountSIP).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{record?.txnTypeName}</DataCell>
                                                        <DataCell className="text-center">{record?.goalname}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataRow>
                                                )
                                            }
                                        })}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </> : null}
                    {schemeDetails?.filter((record: any) => record?.objective === "ULIP").length !== 0 ? <>
                        <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-start pb-1">ULIP</Text>
                        </Box>
                        <Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-3 text-start ps-2'>Scheme</Column>
                                            <Column>Time Horizon</Column>
                                            <Column>Current Value</Column>
                                            <Column>Additional Contribution</Column>
                                            <Column>Frequency</Column>
                                            <Column>Goal Name</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {schemeDetails?.map((record: any) => {
                                            if (record?.objective === "ULIP") {
                                                // console.log(record,"ulip")
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="text-start">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} year</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountLumpsum).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountSIP).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{record?.txnTypeName}</DataCell>
                                                        <DataCell className="text-center">{record?.goalname}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataRow>
                                                )
                                            }
                                        })}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </> : null}
                    {schemeDetails?.filter((record: any) => record?.objective === "EPF").length !== 0 ? <>
                        <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-start pb-1">EPF</Text>
                        </Box>
                        <Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-3 text-start ps-2'>Scheme</Column>
                                            <Column>Time Horizon</Column>
                                            <Column>Current Value</Column>
                                            <Column>Additional Contribution</Column>
                                            <Column>Frequency</Column>
                                            <Column>Goal Name</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {schemeDetails?.map((record: any) => {
                                            if (record?.objective === "EPF") {
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="text-start">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} year</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountLumpsum).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountSIP).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{record?.txnTypeName}</DataCell>
                                                        <DataCell className="text-center">{record?.goalname}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataRow>
                                                )
                                            }
                                        })}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </> : null}
                    {schemeDetails?.filter((record: any) => record?.objective === "Other Asset").length !== 0 ? <>
                        <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-start pb-1">Other Asset</Text>
                        </Box>
                        <Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-3 text-start ps-2'>Scheme</Column>
                                            <Column>Time Horizon</Column>
                                            <Column>Current Value</Column>
                                            <Column>Additional Contribution</Column>
                                            <Column>Frequency</Column>
                                            <Column>Goal Name</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {schemeDetails?.map((record: any) => {
                                            if (record?.objective === "Other Asset") {
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="text-start">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} year</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountLumpsum).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountSIP).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{record?.txnTypeName}</DataCell>
                                                        <DataCell className="text-center">{record?.goalname}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataRow>
                                                )
                                            }
                                        })}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </> : null}
                    {schemeDetails?.filter((record: any) => record?.objective === "PPF(Monthly)").length !== 0 ? <>
                        <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-start pb-1">PPF(Monthly)</Text>
                        </Box>
                        <Box>
                            <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                                <Table>
                                    <ColumnParent
                                        className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                        <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                            <Column className='col-lg-3 text-start ps-2'>Scheme</Column>
                                            <Column>Time Horizon</Column>
                                            <Column>Current Value</Column>
                                            <Column>Additional Contribution</Column>
                                            <Column>Frequency</Column>
                                            <Column>Goal Name</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {schemeDetails?.map((record: any) => {
                                            if (record?.objective === "PPF(Monthly)") {
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="text-start">{record?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">{record?.duration} year</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountLumpsum).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{Number(record?.txnAmountSIP).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-center">{record?.txnTypeName}</DataCell>
                                                        <DataCell className="text-center">{record?.goalname}</DataCell>
                                                        {record?.status === "Continue" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>{record?.status}</DataCell> :
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "#007bff", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                {record?.status}</DataCell>
                                                        }
                                                    </DataRow>
                                                )
                                            }
                                        })}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </> : null}
                </>
            }
        </>
    )
}

export default SchemeSelectionCategory