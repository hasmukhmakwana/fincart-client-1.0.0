import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import { getMyDreamDetails } from "App/api/fullfinancialplan";
import { encryptData, getUser } from "App/utils/helpers";
import style from "../../FullFinancialPlan.module.scss";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { toastAlert } from 'App/shared/components/Layout';
import useFullFinancialPlanStore from '../../store';
import Loader from "App/shared/components/Loader";


const AvailableResources = () => {
    const [totals, setTotals] = useState<any>([]);
    const [mutualFund, setMutualFund] = useState<any>([]);
    const [insurance, setInsurance] = useState<any>([]);
    const [otherAsset, setOtherAsset] = useState<any>([]);
    const [loan, setLoan] = useState<any>([]);
    const [remain, setRemain] = useState<any>([]);
    const [load, setLoad] = useState(false);
    const {
        loading,
        localUser,
        localPlanId,
        setLoader,
    } = useFullFinancialPlanStore();

    let CurrentAmtMF: any = 0;
    let AllocatedAmtMF: any = 0;
    let CurrentAmtIN: any = 0;
    let AllocatedAmtIN: any = 0;
    let CurrentAmtOT: any = 0;
    let AllocatedAmtOT: any = 0;
    let CurrentAmtLN: any = 0;
    let AllocatedAmtLN: any = 0;
    //const fetchAvailableResources = () => {
    const fetch = async () => {
        try {
            setLoad(true);
            const obj = {
                basicId: localUser,
                planid: localPlanId,
                step: "2" //For availabe Resource
            }
            const enc: any = encryptData(obj);
            console.log(obj);
            console.log(localUser);

            let result = await getMyDreamDetails(enc);
            console.log(enc);
            console.log("test");
            console.log(result?.data);
            let objOAV: any = [];

            let objMf: any = [];
            let objIn: any = [];
            let objLn: any = [];
            let objOt: any = [];
            let objUlip: any = [];
            // let CurrentAmtMF: any = 0;
            // let AllocatedAmtMF: any = 0;
            result?.data?.availableResources.map((record: any) => {
                switch (record.category) {
                    case "MF":
                        // setMfDetailsList(result?.data[i]);
                        objMf.push(record);
                        // CurrentAmtMF += parseInt(record?.CurrentValue);
                        // AllocatedAmtMF += parseInt(record?.AllocatedAmount);
                        break;

                    case "IN":
                        // setInDetailsList(result?.data[i]);
                        objIn.push(record);
                        break;

                    case "LN":
                        // setLnDetailsList(result?.data[i]);
                        objLn.push(record);
                        break;

                    case "OT":
                        // setOtDetailsList(result?.data[i]);
                        objOt.push(record);
                        break;

                    default:
                        // setUlipDetailsList(result?.data[i]);
                        objUlip.push(record);
                        break;
                }
            })
            console.log(objOAV);
            setMutualFund(objMf);
            setInsurance(objIn);
            setOtherAsset(objOt);
            setLoan(objLn);
            setRemain(objUlip);
            //Calulate Totals
            // setCrrMFAmt(CurrentAmtMF);
            // setAllocateMFAmt(AllocatedAmtMF);
            setTotals(result?.data?.totalValues);
            console.log(result?.data?.totalValues);
            //setLoader(false);
            // console.log(result?.data);
            setLoad(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
            setLoad(false);
        }
    };
    useEffect(() => {
        fetch();
        // fetchAvailableResources();
        return () => {
        }
    }, [localUser])


    return (
        <>
            <Card css={{ pb: 25, pt: 12 }}>
                <Box className="row ps-2 pe-2">
                    <Box className={`mt-2 ms-4 ${style.blueBgBoxDrm}`}>
                        {/* @ts-ignore */}
                        <Box className="row pt-3 pb-3">
                            <Box className="col-12 col-lg-4 text-center">
                                <Box className={style.blueBoxText}>Total Assets Value(Rs)</Box>
                                <Box className={style.blueBoxText2}>&#x20B9;{" "}{Number(totals?.TotalAssetValue || "0.0").toLocaleString("en-IN") || "0.0"}</Box>
                            </Box>
                            <Box className="col-12 col-lg-4 text-center">
                                <Box className={style.blueBoxText}>Total Loan Liabilities (Rs.)</Box>
                                <Box className={style.blueBoxText2}>&#x20B9;{" "}{Number(totals?.TotalLoanValue || "0.0").toLocaleString("en-IN") || "0.0"}</Box>
                            </Box>
                            <Box className="col-12 col-lg-4 text-center">
                                <Box className={style.blueBoxText}>Total Maturity Amount(Rs.)</Box>
                                <Box className={style.blueBoxText2}>&#x20B9;{" "}{Number(totals?.TotalNetWorth || "0.0").toLocaleString("en-IN") || "0.0"}</Box>
                            </Box>
                        </Box>
                    </Box>
                    <Box className="text-center mt-3" css={{ color: "#005CB3" }}>All Assets</Box>
                    <Box style={{ borderBottom: "solid 1px #FAAC05" }}>
                        {/* @ts-ignore */}
                        <Text size="h5" className="text-start pb-1">Mutual Fund</Text>
                    </Box>
                    <Box>
                        <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                            <Table>
                                <ColumnParent
                                    className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                    <ColumnRow css={{ textAlign: "center" }}>
                                        <Column className='col-lg-4 text-start ps-2'>Asset name</Column>
                                        <Column>Current Amount</Column>
                                        <Column>Allocated Amount</Column>
                                        <Column>Goal name</Column>
                                        <Column>Liquidity Type</Column>
                                    </ColumnRow>
                                </ColumnParent>
                                <DataParent>
                                    {load ? <>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell colSpan={5}><Loader /></DataCell>
                                        </DataRow>
                                    </> : <>
                                        {mutualFund.length === 0 ? <>
                                            <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                <DataCell colSpan={5}> No Record Available</DataCell>
                                            </DataRow>
                                        </> : <>
                                            {mutualFund.map((record: any,index: any) => {
                                                CurrentAmtMF += parseInt(record?.assetName === mutualFund?.[index - 1]?.assetName && record?.CurrentValue === mutualFund?.[index - 1]?.CurrentValue ? "0" : record?.CurrentValue);
                                                AllocatedAmtMF += parseInt(record?.AllocatedAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="ps-2 text-start">{record?.assetName === mutualFund?.[index - 1]?.assetName ? "" : record?.assetName }</DataCell>
                                                        <DataCell className="ps-5 text-start">{record?.assetName === mutualFund?.[index - 1]?.assetName && record?.CurrentValue === mutualFund?.[index - 1]?.CurrentValue ? "" : Number(record?.CurrentValue).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-end pe-5">{Number(record?.AllocatedAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-end pe-5">{record?.goalname}</DataCell>
                                                        <DataCell className="text-end pe-5">{record?.LiquidityType}</DataCell>
                                                    </DataRow>
                                                );
                                            })}
                                            <DataRow css={{ color: "#FFFFFF", backgroundColor: "#005CB3" }}>
                                                <DataCell className="text-start">Total</DataCell>
                                                <DataCell className="text-center">{Number(CurrentAmtMF).toLocaleString("en-IN")}</DataCell>
                                                <DataCell className="text-center">{Number(AllocatedAmtMF).toLocaleString("en-IN")}</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                        </>}
                                    </>}

                                </DataParent>
                            </Table>
                        </Box></Box>

                    <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                        {/* @ts-ignore */}
                        <Text size="h5" className="text-start pb-1">Insurance</Text>
                    </Box>
                    <Box>
                        <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                            <Table>
                                <ColumnParent
                                    className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                    <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                        <Column className='col-lg-4 text-start ps-2'>Asset name</Column>
                                        <Column>Current Amount</Column>
                                        <Column>Allocated Amount</Column>
                                        <Column>Goal name</Column>
                                        <Column>Liquidity Type</Column>
                                    </ColumnRow>
                                </ColumnParent>
                                <DataParent>
                                    {load ? <>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell colSpan={5}><Loader /></DataCell>
                                        </DataRow>
                                    </> : <>
                                        {insurance.length === 0 ? <>
                                            <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                <DataCell colSpan={5}> No Record Available</DataCell>
                                            </DataRow>
                                        </> : <>
                                            {insurance.map((record: any, index: any) => {
                                                // console.log(record,"insurance")
                                                CurrentAmtIN += parseInt(record?.assetName === insurance?.[index - 1]?.assetName && record?.CurrentValue === insurance?.[index - 1]?.CurrentValue ? "0" : record?.CurrentValue);
                                                AllocatedAmtIN += parseInt(record?.AllocatedAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="ps-2 text-start">{record?.assetName === insurance?.[index - 1]?.assetName ? "" : record?.assetName }</DataCell>
                                                        <DataCell className="ps-5 text-start">{record?.assetName === insurance?.[index - 1]?.assetName && record?.CurrentValue === insurance?.[index - 1]?.CurrentValue ? "" : Number(record?.CurrentValue).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-end pe-5">{Number(record?.AllocatedAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-end pe-5">{record?.goalname}</DataCell>
                                                        <DataCell className="text-end pe-5">{record?.LiquidityType}</DataCell>
                                                    </DataRow>
                                                );
                                            }
                                            )}
                                            <DataRow css={{ color: "#FFFFFF", backgroundColor: "#005CB3" }}>
                                                <DataCell className="text-start">Total</DataCell>
                                                <DataCell className="text-center">{Number(CurrentAmtIN).toLocaleString("en-IN")}</DataCell>
                                                <DataCell className="text-center">{Number(AllocatedAmtIN).toLocaleString("en-IN")}</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                        </>}
                                    </>}
                                </DataParent>
                            </Table>
                        </Box>
                    </Box>

                    <Box className="mt-4" style={{ borderBottom: "solid 1px #FAAC05" }}>
                        {/* @ts-ignore */}
                        <Text size="h5" className="text-start pb-1">Other Asset</Text>
                    </Box>
                    <Box>
                        <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                            <Table>
                                <ColumnParent
                                    className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                    <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                        <Column className='col-lg-4 text-start ps-2'>Asset name</Column>
                                        <Column>Current Amount</Column>
                                        <Column>Allocated Amount</Column>
                                        <Column>Goal name</Column>
                                        <Column>Liquidity Type</Column>
                                    </ColumnRow>
                                </ColumnParent>
                                <DataParent>
                                    {load ? <>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell colSpan={5}><Loader /></DataCell>
                                        </DataRow>
                                    </> : <>
                                        {otherAsset.length === 0 ? <>
                                            <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                <DataCell colSpan={5}> No Record Available</DataCell>
                                            </DataRow>
                                        </> : <>
                                            {otherAsset.map((record: any,index:any) => {
                                                CurrentAmtOT += parseInt(record?.assetName === otherAsset?.[index - 1]?.assetName && record?.CurrentValue === otherAsset?.[index - 1]?.CurrentValue ? "0" : record?.CurrentValue);
                                                AllocatedAmtOT += parseInt(record?.AllocatedAmount);
                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell className="ps-2 text-start">{record?.assetName === otherAsset?.[index - 1]?.assetName ? "" : record?.assetName }</DataCell>
                                                        <DataCell className="ps-5 text-start">{record?.assetName === otherAsset?.[index - 1]?.assetName && record?.CurrentValue === otherAsset?.[index - 1]?.CurrentValue ? "" : Number(record?.CurrentValue).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-end pe-5">{Number(record?.AllocatedAmount).toLocaleString("en-IN")}</DataCell>
                                                        <DataCell className="text-end pe-5">{record?.goalname}</DataCell>
                                                        <DataCell className="text-end pe-5">{record?.LiquidityType}</DataCell>
                                                    </DataRow>
                                                );
                                            }
                                            )}
                                            <DataRow css={{ color: "#FFFFFF", backgroundColor: "#005CB3" }}>
                                                <DataCell className="text-start">Total</DataCell>
                                                <DataCell className="text-center">{Number(CurrentAmtOT).toLocaleString("en-IN")}</DataCell>
                                                <DataCell className="text-center">{Number(AllocatedAmtOT).toLocaleString("en-IN")}</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                        </>}
                                    </>}
                                </DataParent>
                            </Table>
                        </Box>
                    </Box>


                    <Box className="text-center mt-4" css={{ color: "#005CB3" }}>Loan Liabilities</Box>
                    <Box className="mt-3" style={{ borderBottom: "solid 1px #FAAC05" }}>
                        {/* @ts-ignore */}
                        <Text size="h5" className="text-start pb-1">Loan</Text>
                    </Box>
                    <Box>
                    <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                        <Table>
                            <ColumnParent
                                className="text-capitalize" css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                <ColumnRow css={{ textAlign: "center", borderRadius: "10px", }}>
                                    <Column className='col-lg-4 text-start ps-2'>Asset name</Column>
                                    <Column>Outstanding amount</Column>
                                    <Column>Allocated Amount</Column>
                                    <Column>Goal name</Column>
                                    <Column>Liquidity Type</Column>
                                </ColumnRow>
                            </ColumnParent>

                            <DataParent>
                                {load ? <>
                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                        <DataCell colSpan={5}><Loader /></DataCell>
                                    </DataRow>
                                </> : <>
                                    {loan.length === 0 ? <>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="text-center" colSpan={5}> No Record Available</DataCell>
                                        </DataRow>
                                    </> : <>
                                        {loan.map((record: any, index:any) => {
                                            CurrentAmtLN += parseInt(record?.assetName === loan?.[index - 1]?.assetName && record?.CurrentValue === loan?.[index - 1]?.CurrentValue ? "0" : record?.CurrentValue);
                                            AllocatedAmtLN += parseInt(record?.AllocatedAmount);
                                            return (
                                                <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                    <DataCell className="ps-2 text-start">{record?.assetName === loan?.[index - 1]?.assetName ? "" : record?.assetName }</DataCell>
                                                    <DataCell className="ps-5 text-start">{record?.assetName === loan?.[index - 1]?.assetName && record?.CurrentValue === loan?.[index - 1]?.CurrentValue ? "" : Number(record?.CurrentValue).toLocaleString("en-IN")}</DataCell>
                                                    <DataCell className="text-end pe-5">{Number(record?.AllocatedAmount).toLocaleString("en-IN")}</DataCell>
                                                    <DataCell className="text-end pe-5">{record?.goalname}</DataCell>
                                                    <DataCell className="text-end pe-5">{record?.LiquidityType}</DataCell>
                                                </DataRow>
                                            );
                                        }
                                        )}
                                        <DataRow css={{ color: "#FFFFFF", backgroundColor: "#005CB3" }}>
                                            <DataCell className="text-start">Total</DataCell>
                                            <DataCell className="text-center">{Number(CurrentAmtLN).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className="text-center">{Number(AllocatedAmtLN).toLocaleString("en-IN")}</DataCell>
                                            <DataCell className="text-center"></DataCell>
                                            <DataCell className="text-center"></DataCell>
                                        </DataRow>
                                    </>}
                                </>}
                            </DataParent>
                        </Table>
                    </Box>
                    </Box>
                </Box>
            </Card>
        </>
    );
}

export default AvailableResources
