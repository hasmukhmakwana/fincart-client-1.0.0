import create from "zustand";
import { GoalListType, MemberListType } from "./FullFinancialPlanType";
interface StoreTypes {
    localUser: string;
    localPlanId: string;
    loading: boolean;
    goalList: any;
    memberList: any;
    setGoalList: (payload: any) => void;
    setMemberList: (payload: any) => void;
    setLocalUser: (payload: string) => void;
    setLocalPlanId: (payload: string) => void;
    setLoader: (payload: boolean) => void;
}

const useFullFinancialPlanStore = create<StoreTypes>((set) => ({
    localUser: "",
    localPlanId: "",
    loading: false,
    goalList: [],
    memberList: [],
    setLoader: (payload) =>
        set((state) => ({
            ...state,
            loading: payload,
        })),

    setLocalUser: (payload) =>
        set((state) => ({
            ...state,
            localUser: payload,
        })),

    setLocalPlanId: (payload) =>
        set((state) => ({
            ...state,
            localPlanId: payload,
        })),
    setGoalList: (payload) =>
        set((state) => ({
            ...state,
            goalList: payload,
        })),
    setMemberList: (payload) =>
        set((state) => ({
            ...state,
            memberList: payload,
        })),
}))

export default useFullFinancialPlanStore;