import Layout from 'App/shared/components/Layout'
import React from 'react'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import UploadCAS from "./components/UploadCasPage"
const UploadCas = () => {
    return (
        <Layout>
            <PageHeader title="Consolidated Account Statement" rightContent={<></>} />
            <UploadCAS />
        </Layout>
    )
}

export default UploadCas