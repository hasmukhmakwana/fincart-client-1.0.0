import React, { useState, useEffect } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import * as Select from '@radix-ui/react-select'
import style from "../../Product.module.scss";
import Upload from "App/icons/Upload";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import Input from "@ui/Input/Input";
import Card from "@ui/Card/Card";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { GroupBox } from "@ui/Group/Group.styles";
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser, convertBase64, plainBase64 } from "App/utils/helpers";
import { uploadFilePDF, getUploadCASHistory } from "App/api/financialPlan";
import DeleteIcon from "App/icons/DeleteIcon";
import DialogModal from "@ui/ModalDialog/ModalDialog";


const CASModalObj = {
    OTPcas: Yup.string()
        .required("Password is required")
        .trim(),
    // .matches(/{6}/g, "Only Numbers are allowed"),
    Filecas: Yup.string()
        .required("File is required"),
}
let validationSchema = Yup.object().shape(CASModalObj);
export type VerifyCASTypes = {
    // Name: string;
    OTPcas: string;
    Filecas: any;
    // InvestDate: string;
};

interface StoreTypes {
    verifyPay: VerifyCASTypes;
}
const useRegistrationStore = create<StoreTypes>((set) => ({
    //* initial state
    verifyPay: {
        // Name: "",
        OTPcas: "",
        Filecas: "",
        // InvestDate: "",
    }
}))

// type NameEventTypes = {
//     clickOnSubmit: (value: VerifyCASTypes) => any;
// };

const UploadCasPage = () => {
    const [fieldValue, setFieldValue] = useState();
    const [uploadCASHistory, setUploadCASHistory] = useState<any>([]);
    const local: any = getUser();
    const { verifyPay, loading } = useRegistrationStore((state: any) => state);
    const [btnState, setBtnState] = useState(false);
    const [openFeedback, setOpenFeedback] = useState(false);
    const PurchaseFun = (e: any) => {
        // clickOnSubmit(e);
        console.log(e);
    };
    const [loaderApi, setLoaderApi] = useState(false);
    const UploadCASFile = async (data: any) => {
        data = data || {};
        console.log("FileData: ", data);
        setBtnState(true);
        try {
            setLoaderApi(true);
            const obj: any =
            {
                basicid: local?.basicid,
                password: data?.OTPcas,//"Test@123",
                Pdf_File: plainBase64(data?.Filecas)//""
            }
            const enc: any = encryptData(obj);
            // console.log("obj");
            // console.log(obj);
            const result: any = await uploadFilePDF(enc);
            setBtnState(false);
            // console.log(result);
            // console.log(result?.data);
            //fetchData();
            UploadCASFileHistory();
            toastAlert("success", "File Successfully Uploaded. Portfolio will be synced after 24 hrs");
            setLoaderApi(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
            setBtnState(false);
            setLoaderApi(false);
        }
    }

    const UploadCASFileHistory = async () => {
        try {
            setLoaderApi(true);
            const enc: any = encryptData(local?.basicid, true);
            console.log(enc);
            let result: any = await getUploadCASHistory(enc);
            console.log(result);
            setUploadCASHistory(result?.data);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoaderApi(false);
        }
    }
    useEffect(() => {
        if (local) {
            UploadCASFileHistory();
        }
        return () => { }
    }, [])
    return (
        <>
            <Card className="p-3 pt-0">
                <Box className="col-md-12 col-sm-12 col-lg-12">
                    {/* <Box >
                        <Text weight="bold" css={{ color: "var(--colors-blue1)" }}> Upload CAS</Text>
                    </Box> */}
                    <Box >
                        <Box className="row justify-content-start">
                            <Box className="col-auto mt-3 ">
                                {/* @ts-ignore */}
                                <Text color="blue1" className="text-capitalize">Select your Consolidated Account Statement (CAS) </Text>
                            </Box>
                        </Box>
                        <Formik
                            initialValues={verifyPay}
                            validationSchema={validationSchema}
                            enableReinitialize
                            onSubmit={(values, { resetForm }) => {
                                UploadCASFile(values);
                                //PurchaseFun(values);
                            }}
                        >
                            {({
                                values,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                errors,
                                touched,
                                setFieldValue,
                                submitCount
                            }) => (
                                <>
                                    <Box className="row justify-content-start">
                                        <Box className=" col-auto col-lg-4 ">
                                            <Input
                                                label="Select File"
                                                type="file"
                                                name="Filecas"
                                                // style={{ border: "solid 1px #00000 90%"}}
                                                className={`px-0 border border-2 ${style.uploadBtn}`}
                                                // error={submitCount ? errors.Filecas : null}
                                                // onChange={handleChange}
                                                // value={values?.Filecas}
                                                autocomplete="off"
                                                accept=".pdf"
                                                onChange={async (
                                                    e: React.ChangeEvent<HTMLInputElement>
                                                ) => {
                                                    try {
                                                        const file: any =
                                                            e?.target?.files?.[0] || null;
                                                        const base64 = await convertBase64(file);
                                                        setFieldValue(
                                                            "Filecas",
                                                            base64
                                                        );
                                                        console.log(file)
                                                    } catch (error: any) {
                                                        toastAlert("error", error);
                                                    }
                                                }}
                                                required
                                                error={submitCount ? errors?.Filecas : null}
                                            />
                                        </Box>
                                        <Box className="col-auto col-lg-4 ">
                                            <Input
                                                type="password"
                                                label="Enter Password"
                                                name="OTPcas"
                                                className="border border-2 pt-1"
                                                error={submitCount ? errors.OTPcas : null}
                                                onChange={handleChange}
                                                value={values?.OTPcas}
                                            //autocomplete="off"
                                            />
                                        </Box>

                                        <Box className="col-auto col-lg-4 mt-4 ">
                                            {/* <Button
                                                type="submit"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={() => { handleSubmit() }}
                                            >
                                                <Text className="p-1" css={{ color: "white" }}> <Upload className="me-2" color="white" /> Upload</Text>
                                            </Button> */}
                                            <Button
                                                className={`col-auto mb-2 `}
                                                color="yellowGroup"
                                                size="md"
                                                onClick={() => { handleSubmit() }}
                                                disabled={btnState}

                                            >
                                                {/* @ts-ignore */}
                                                <GroupBox align="center" className={`${style.hovBtn}`}>
                                                    <Box className="row">
                                                        <Box className="ps-2 col-auto">
                                                            <Upload style={{ weight: "bold" }} />
                                                        </Box>
                                                        {/* @ts-ignore */}
                                                        <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                                            {/* @ts-ignore */}
                                                            <Text size="h6">
                                                                {btnState ? <>Loading... </> : <> Upload </>}

                                                            </Text>
                                                        </Box>
                                                    </Box>
                                                </GroupBox>
                                            </Button>
                                        </Box>
                                    </Box>
                                </>)}</Formik>
                        <Box className="text-center mt-3 mb-2">
                            <Text size="h5" color="blue1" className="text-capitalize" >Previously Uploaded Statement(s) </Text></Box>
                        {/* <Box className="row justify-content-start mt-2"> */}
                        <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table
                                // css={{ color: "var(--colors-blue1)" }}
                                >
                                    <ColumnParent className="text-capitalize border border-bottom">
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center", borderRadius: "8px" }}>
                                            <Column className="text-center"><Text>Date</Text></Column>
                                            <Column className="text-center"><Text>Registrar</Text></Column>
                                            <Column className="text-center"><Text>Total</Text></Column>
                                            <Column className="text-center"><Text>Failed</Text></Column>
                                            <Column className="text-center"><Text>Already Exists</Text></Column>
                                            <Column className="text-center"><Text>Success</Text></Column>
                                            <Column className="text-center"><Text>PDF File</Text></Column>
                                            {/* <Column className="text-center"><Text>Action</Text></Column> */}
                                        </ColumnRow>
                                    </ColumnParent>

                                    <DataParent>
                                        {uploadCASHistory?.map((record: any) => {
                                            return (
                                                <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                    <DataCell className="text-center">{record?.entryDate}</DataCell>
                                                    <DataCell className="text-center">{record?.fileRegistrar}</DataCell>
                                                    <DataCell className="text-center">{record?.totalRecords}</DataCell>
                                                    <DataCell className="text-center">{record?.failedRecords}</DataCell>
                                                    <DataCell className="text-center">{record?.duplicateRecords}</DataCell>
                                                    <DataCell className="text-center">{record?.successRecords}</DataCell>
                                                    <DataCell className="text-center"><a href={record?.fullPath}>Download</a> </DataCell>
                                                    {/*<DataCell className="text-center">
                                                         <Box css={{ cursur: "pointer" }}>
                                                            <DeleteIcon color="orange" className="mt-1" onClick={setOpenFeedback} />
                                                        </Box> 
                                                    </DataCell>*/}
                                                </DataRow>
                                            )
                                        })}
                                        {/* <DataRow>
                                            <DataCell className="text-center">18 Oct 2022 </DataCell>
                                            <DataCell className="text-center">CAMS</DataCell>
                                            <DataCell className="text-center">1546 </DataCell>
                                            <DataCell className="text-center">89 </DataCell>
                                            <DataCell className="text-center">5</DataCell>
                                            <DataCell className="text-center">2562 </DataCell>
                                            <DataCell className="text-center"><a href="#">Download </a></DataCell>
                                        </DataRow>
                                        <DataRow>
                                            <DataCell className="text-center">30 Oct 2022 </DataCell>
                                            <DataCell className="text-center">CAMS</DataCell>
                                            <DataCell className="text-center">2021 </DataCell>
                                            <DataCell className="text-center">21 </DataCell>
                                            <DataCell className="text-center">7</DataCell>
                                            <DataCell className="text-center">2485 </DataCell>
                                            <DataCell className="text-center"><a href="#">Download </a></DataCell>
                                        </DataRow> */}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </Box>

                    {/* </Box> */}
                </Box>
            </Card>
            <DialogModal
                open={openFeedback}
                setOpen={setOpenFeedback}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "40%" },
                }}
            >


                <Box className="row">
                    <Box className="col-md-12 col-sm-12 col-lg-12 text-end" css={{ color: "var(--colors-blue1)" }}>
                        {/* @ts-ignore */}
                        <Text size="h4" className="text-center rounded" css={{ py: 50 }}>
                            Are you sure you want to delete!
                        </Text>
                        <Box className="row justify-content-center mb-3">
                            {/* <Box className="col-auto"></Box> */}
                            <Button
                                className="mb-2 p-1 px-2"
                                color="yellowGroup"
                                size="md"
                                onClick={() => { setOpenFeedback(false) }}
                            >
                                <Text
                                    // @ts-ignore
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                >
                                    Ok
                                </Text>
                            </Button>
                        </Box>
                    </Box>
                </Box>

            </DialogModal>
        </>
    );
}
export default (UploadCasPage);