import React from 'react';
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import CheckCircleFill from "App/icons/CheckCircleFill";
import { GroupBox } from "@ui/Group/Group.styles";
import Loader from 'App/shared/components/Loader';
type SaveTypes = {
    modalData: (value: any) => void;
    load: (value: any) => boolean;
};
const ProtectionViewModal = ({ modalData, load }: SaveTypes) => {

    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    <Text css={{ color: "var(--colors-blue1)" }}> View Details</Text>
                </Box>
                <Box className="modal-body modal-body-scroll p-3 mb-2">
                    {/* @ts-ignore */}
                    {load ? <><Box className="py-1 my-1"><Loader /></Box></> : <>
                        {/* @ts-ignore */}
                        {modalData?.map((item: any) => {
                            // console.log(item);

                            return (
                                <Box className="row my-2 mx-2">
                                    <Box className="col-auto">
                                        <Text css={{ my: 10 }} color="mediumBlue">
                                            {item?.policyName}
                                        </Text><hr />
                                        <Box className="row">
                                            <Box className="col-6 col-md-4 col-lg-auto">
                                                {/* &#x20B9; */}
                                                <Text>PolicyNo / Status</Text>
                                                <GroupBox className="">
                                                    <Box><CheckCircleFill color="var(--colors-blue1)"></CheckCircleFill></Box>
                                                    <Box><Text>{item?.policyNo}</Text></Box>
                                                </GroupBox>
                                            </Box>
                                            <Box className="py-2 col-6 col-md-4 col-lg-auto py-md-2 py-lg-0">
                                                <Text>Client ID</Text>
                                                <Text>{item?.clientID}</Text>
                                            </Box>
                                            <Box className="py-2 col-6 col-md-4 col-lg-auto py-md-2 py-lg-0">
                                                <Text>Cost Value</Text>
                                                <Text>&#x20B9;{" "}{Number(item?.investedAmt).toLocaleString("en-IN")}</Text>
                                            </Box>
                                            <Box className="py-2 col-6 col-md-4 col-lg-auto py-md-2 py-lg-0">
                                                <Text>Market Value</Text>
                                                <Text>&#x20B9;{" "}{Number(item?.currentAmt).toLocaleString("en-IN")}</Text>
                                            </Box>
                                            <Box className="py-2 col-6 col-md-4 col-lg-auto py-md-2 py-lg-0">
                                                <Text>Premium</Text>
                                                <Text>&#x20B9;{" "}{Number(item?.premiumAmt).toLocaleString("en-IN")}/{item?.frequency}</Text>
                                            </Box>
                                            <Box className="py-2 col-6 col-md-4 col-lg-auto py-md-2 py-lg-0">
                                                <Text>Ins. Paid</Text>
                                                <Text >{item?.total_premium_paid} / {item?.ppt}</Text>
                                            </Box>
                                            <Box className="py-2 col-6 col-md-4 col-lg-auto py-md-2 py-lg-0">
                                                <Text>Policy Term</Text>
                                                <Text>{item?.policyTerm} Years</Text>
                                            </Box>
                                            <Box className="py-2 col-6 col-md-4 col-lg-auto py-md-2 py-lg-0">
                                                <Text>Due Date</Text>
                                                <Text>{item?.nextDueDate}</Text>
                                            </Box>
                                            <Box className="py-2 col-6 col-md-4 col-lg-auto py-md-2 py-lg-0">
                                                <Text>SI</Text>
                                                <Text>{item?.siEnabled}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                            );

                        })}
                    </>}
                </Box>
            </Box>
        </>
    )
}

export default ProtectionViewModal