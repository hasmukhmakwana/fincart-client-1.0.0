import React, { useState } from 'react'
import Box from '@ui/Box/Box'
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import styles from "../../Product.module.scss";
import style from "../../../DashboardModule/Dashboard.module.scss";
import usePortfolioStore from "App/modules/DashboardModule/store";
import Loader from "App/shared/components/Loader";
import { useRouter } from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';
const EPFPage = ({ load }: any) => {
    const router = useRouter();
    const {
        epfPortfolioData
    } = usePortfolioStore();
    const { setContactUsVal } = useContactUsStore();
    return (
        <>
            {load ? (<>
                <Loader />
            </>) : (<>
                {epfPortfolioData.length === 0 ? <>
                    <Card css={{ p: 15, mb: 20 }}>
                        <Box>
                            <Box className="container pt-5 ps-3 pb-4">
                                <Box className={`${style.sectionMainHeading} pb-3 text-center text-sm-start`}>What is EPF?
                                </Box>
                                <Box className="row d-flex justify-content-center text-center">
                                    <Box className="col-2">&nbsp;</Box>
                                    <Box className={`col-8 ${style.lightTextBlue} ${style.yellowBorderBottom} text-center`}> The
                                        Employees' Provident Fund (EPF) is a scheme in which retirement benefits
                                        are
                                        accumulated. Under the scheme, an employee has to pay a certain
                                        contribution
                                        towards the scheme and an equal contribution is paid by the employer.
                                    </Box>
                                    <Box className="col-2">&nbsp;</Box>
                                </Box>
                                <Box className={`${style.subHeading} text-center pt-3`}>Benefits Of EPF</Box>
                                <Box className="row mt-5 align-items-end align-sm-items-center">
                                    <Box className="d-none d-md-block col-md-1">&nbsp;</Box>
                                    <Box className="col-md-6 col-sm-12 d-flex align-self-center">
                                        <img src="/EPF.png" alt="EPF" className="img-fluid mx-auto" />
                                    </Box>
                                    <Box className="col-md-4 col-sm-12">
                                        <Box className="row">
                                            <Box
                                                className="col-4 col-md-4 col-sm-4 pt-lg-0 pt-md-0 pt-sm-4 text-end">
                                                <img src="/Contributions.png" alt="Contributions" />
                                            </Box>
                                            <Box
                                                className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                                                Contributions</Box>
                                        </Box>
                                        <Box className="row">
                                            <Box
                                                className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-end ps-0 my-auto`}>
                                                Emergency
                                                Corpus
                                            </Box>
                                            <Box
                                                className="col-4 col-md-4 col-sm-4 pt-lg-0 pt-md-0 pt-sm-4 text-left">
                                                <img src="/Emergency.png" alt="Emergency" />
                                            </Box>
                                        </Box>
                                        <Box className="row mt-4">
                                            <Box className="col-4 col-md-4 col-sm-4 text-end">
                                                <img src="/addMoney.png" alt="Premature Withdrawals" />
                                            </Box>
                                            <Box
                                                className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                                                Premature Withdrawals</Box>
                                        </Box>
                                        <Box className="row mt-4">
                                            <Box
                                                className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-end my-auto`}>
                                                Nomination Facility
                                            </Box>
                                            <Box className="col-4 col-md-4 col-sm-4 text-left">
                                                <img src="/Nomination.png" alt="Nomination" />
                                            </Box>
                                        </Box>
                                        <Box className="row mt-4">
                                            <Box className="col-4 col-md-4 col-sm-4 text-end">
                                                <img src="/Tax.png" alt="Tax Benefits" />
                                            </Box>
                                            <Box
                                                className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                                                Tax Benefits</Box>

                                        </Box>
                                    </Box>
                                    <Box className="d-none d-md-block col-md-1">&nbsp;</Box>
                                </Box>
                                <Box className="mt-4 text-end">
                                    <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow" onClick={() => {
                                        setContactUsVal({
                                            SubjectId: "8",
                                            Description: "Book Appointment to Know more About EPF"
                                        }
                                        );
                                        router.push('/Support/ContactUs');
                                    }}>
                                        <GroupBox>
                                            {/*@ts-ignore */}
                                            <Text size="h5">Contact Us</Text>
                                        </GroupBox>
                                    </Button>
                                </Box>
                            </Box>
                        </Box>
                    </Card>
                </> : <>
                    <Card css={{ p: 15, mb: 20, pt: 0 }}>
                        <Box className="row" css={{ backgroundColor: "#b3daff" }}>
                            <Box className="col-12 mb-1">&nbsp;</Box>
                        </Box>
                        <Box>
                            {epfPortfolioData.map((record) => {
                                return (<>
                                    <Box className="row py-2">
                                        <Box className="col-9">
                                            {/* @ts-ignore */}
                                            <Text css={{ mb: 10 }} weight="extrabold" color="mediumBlue">
                                                EPF Details
                                            </Text>
                                            <Box className="row">
                                                <Box className="py-2 col-lg-3 col-md-3 col-sm-6 py-md-0 py-lg-0">
                                                    {/* @ts-ignore */}
                                                    <Text weight="extrabold">EUIN No.</Text>
                                                    {/* @ts-ignore */}
                                                    <Text weight="extrabold">1234124</Text>
                                                </Box>
                                                <Box className="py-2 col-lg-3 col-md-3 col-sm-6 py-md-0 py-lg-0">
                                                    {/* @ts-ignore */}
                                                    <Text weight="extrabold">PAN no.</Text>
                                                    {/* @ts-ignore */}
                                                    <Text weight="extrabold">124124124</Text>
                                                </Box>
                                            </Box>
                                        </Box>
                                        <Box className="col-lg-3 py-4 text-end" alignSelf="middle">
                                            <Button
                                                className="col-auto mb-2 "
                                                color="yellowGroup"
                                                size="md"
                                            // onClick={setOpen}
                                            >
                                                <GroupBox
                                                    //@ts-ignore
                                                    className={`${styles.hovBtn}`}
                                                >
                                                    <Box className="row">
                                                        {/* <Box className="ps-2 col-auto">
                                                            <EyeIcon />
                                                        </Box> */}
                                                        {/* @ts-ignore */}
                                                        <Box className="col-auto ps-3 p-2 text-center ">
                                                            {/* @ts-ignore */}
                                                            <Text weight="extrabold" size="h6">
                                                                Sync
                                                            </Text>
                                                        </Box>
                                                    </Box>
                                                </GroupBox>
                                            </Button>
                                        </Box>
                                    </Box>
                                </>)
                            })}
                            {/* </>)*/}
                        </Box>
                    </Card>
                </>}
            </>)}
        </>
    )
}

export default EPFPage