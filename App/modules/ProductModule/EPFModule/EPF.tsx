import React, { useEffect, useState } from 'react'
import Layout, { toastAlert } from 'App/shared/components/Layout'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import EPFPage from './components/EPFPage'
import usePortfolioStore from "App/modules/DashboardModule/store";
import { getOverallPortfolioData } from "App/api/dashboard";
import { encryptData, getUser } from "App/utils/helpers";

const EPF = () => {
    const user: any = getUser();
    const [dataLoad, setDataLoad] = useState(false);
   
    const {
        epfPortfolioData,
        setEpfPortfolioData,
        // setLoader,
    } = usePortfolioStore()
    const handleActiveCall = async (e:any) => {

        //   console.log(e);
        try {
            let isActive = "";
            if (e === "active") {
                isActive = "true";
            } else {
                isActive = "false";
            }
            // console.log(user?.basicid);
            setDataLoad(true);
            const obj: any = {
                basicID: user?.basicid,
                assetType: "",
                isActive: isActive
            };
            if (user?.basicid === "null" || user?.basicid === undefined) {
                return;
            }
            const enc: any = encryptData(obj);
            // console.log(enc);
            const result: any = await getOverallPortfolioData(enc);
            // console.log(result?.data?.productWisePortfolio)
            for (let i = 0; i < result?.data?.productWisePortfolio.length; i++) {

                if (result?.data?.productWisePortfolio[i].investment_type === "EPF") {
                    setEpfPortfolioData(result?.data?.productWisePortfolio[i].data);
                }
            }
            // console.log("endif");
            setDataLoad(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setDataLoad(false);
            // setDataLoad(false);
        }
    };
    useEffect(() => {
        if (epfPortfolioData.length === 0) {
            handleActiveCall("active");
        }

        return () => {

        }
    }, [])
    return (
        <Layout>
            <PageHeader title="Employees Provident Fund" rightContent={<></>} />
            <EPFPage load={dataLoad} />
        </Layout>
    )
}

export default EPF