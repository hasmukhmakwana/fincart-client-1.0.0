import React, { useState } from 'react'
import Box from '@ui/Box/Box'
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import EyeIcon from 'App/icons/EyeIcon';
import DialogModal from "@ui/ModalDialog/ModalDialog";
import PMSViewModal from './PMSViewModal';
import DownArrowLong from 'App/icons/DownArrowLong';
import styles from "../../Product.module.scss";
import style from "../../../DashboardModule/Dashboard.module.scss";
import usePortfolioStore from "App/modules/DashboardModule/store";
import Loader from "App/shared/components/Loader";
import UpArrowLong from 'App/icons/UpArrowLong';
import { fetchPolicyDetailsChart } from "App/api/dashboard";
import { encryptData, getUser } from 'App/utils/helpers';
import { toastAlert } from 'App/shared/components/Layout';
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from '@ui/Accordian/Accordian';
import DownArrow from 'App/icons/DownArrow';
import {
  TxnD,
} from "../../CommonModal"
import AllActiveInvestments from '../../AllActiveInvestments/AllActiveInvestments';
import { useRouter } from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';
import { MOTILAL_PMS_PARTNER_ID } from 'App/utils/constants';
const PMSPage = ({ load, id, userGoalId, dataType, setDataType, isGoalWisePortfolio }: any) => {
  const router = useRouter();
  const { setContactUsVal } = useContactUsStore();
  const [loader, setLoader] = useState<any>(false);
  const [pmsData, setPMSData] = useState<any>([]);
  const [openTxn, setOpenTxn] = useState(false);
  const [basicId, setBasicId] = useState<any>("")
  const [policyNo, setPolicyNo] = useState<any>("")
  const [partnerId, setPartnerId] = useState<any>("")

  const fetchPms = async (currentRecord: any) => {
    if (currentRecord.length === 0) return "";

    let val = JSON.parse(currentRecord);
    setPartnerId(val?.partner_id);
    try {
      setLoader(true);

      let obj: any = {
        basicID: "0",
        productId: val?.id,
        investment_type: val?.investment_type_name,//"PMS"
        partnerId: val?.partner_id,
        isActive: false
      }

      const enc: any = encryptData(obj);
      let result: any = await fetchPolicyDetailsChart(enc);
      console.log(result, "pms");

      setPMSData(result?.data);
      setLoader(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  }

  const {
    loading,
    pmsPortfolioData
  } = usePortfolioStore()
  return (
    <>
      {load ? (<>
        <Loader />
      </>) : (<>
        {pmsPortfolioData.length === 0 ?
          (isGoalWisePortfolio ? <>
            <Card css={{ px: 15, py: 2, mb: 15 }} >
              <Box>
                <Box className="container pt-4 ps-3 pb-3">
                  <Box className={`pb-3 text-center text-sm-start ${style.sectionMainHeading}`}>No records
                  </Box>
                </Box>
              </Box>
            </Card>
          </> :
            <>
              <Card css={{ px: 15, py: 2, mb: 20 }}>
                <Box>
                  <Box className="container pt-4 ps-3 pb-4">
                    <Box className={`pb-3 text-center text-sm-start ${style.sectionMainHeading}`}>What Is PMS?</Box>
                    <Box className="row">
                      {/* <Box className="col-md-2 col-lg-2">&nbsp;</Box> */}
                      <Box className="col-md-12 col-lg-12 text-center" >
                        <Box className="row">
                          <Box className={`${style.lightTextBlue} ${style.yellowBorderBottom}`}>
                            A Portfolio Management Service(PMS) is a wealth Management service.<br />
                            Qualified fund managers manage investors' money by investing it
                            in aportfolio of investment assets like<br />
                            stocks and fixed income securities for a fee.

                          </Box>
                        </Box>
                      </Box>
                      <Box className="col-md-12 col-lg-12 text-center" >
                        <Box className="row justify-content-center ">
                          {/* <Box className="col-md-2 mr-2">&nbsp;</Box> */}
                          <Box className="col-auto mt-5">
                            <Box className="row ">
                              <Box className="col-12 col-md-6 col-lg-6  mt-5 p-0">
                                <img className={`img-fluid ${style.image}`} src="/meeting2.png" />
                              </Box>
                              <Box className="col-12 col-md-6 col-lg-6 p-0">
                                <Box className="justify-content-center">
                                  <Box className="row m-2 mt-3">
                                    <Box
                                      className="col col-sm-2 col-md-3 col-lg-3  p-0">
                                      <img className={`img-fluid ${style.investImg}`}
                                        src="/ProfessionallyManaged.png" />
                                    </Box>
                                    <Box
                                      className={`col col-sm-7 col-md-7 col-lg-7 ${style.pointerHeading} text-start`}>
                                      Professionally<br />
                                      Managed
                                    </Box>
                                  </Box>
                                  <Box className="row m-2">
                                    <Box className="col col-sm-2 col-md-3 col-lg-3 p-0">
                                      <img className={`img-fluid ${style.investImg}`}
                                        src="/Transparent.png" />
                                    </Box>
                                    <Box
                                      className={`col col-sm-7 col-md-7 col-lg-7 ${style.pointerHeading} text-start`}>
                                      Transparent<br />
                                      Transactions
                                    </Box>
                                  </Box>

                                  <Box className="row m-2">
                                    <Box className="col col-sm-3 col-md-3 col-lg-3 p-0">
                                      <img className={`img-fluid ${style.investImg}`}
                                        src="/Flexibility.png" />
                                    </Box>
                                    <Box
                                      className={`col col-sm-7 col-md-7 col-lg-7 ${style.pointerHeading} text-start`}>
                                      Provides<br />
                                      Flexibility
                                    </Box>
                                  </Box>

                                  <Box className="row m-2">
                                    <Box className="col col-sm-3 col-md-3 col-lg-3 p-0">
                                      <img className={`img-fluid ${style.investImg}`}
                                        src="/Monitoring.png" />
                                    </Box>
                                    <Box
                                      className={`col col-sm-7 col-md-7 col-lg-7 ${style.pointerHeading} text-start`}>
                                      Regular<br />
                                      Monitoring
                                    </Box>
                                  </Box>

                                </Box>
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                        <Box className="row justify-content-center">

                          <Box className="col-lg-auto col-md-auto">
                            <Box className={`py-3 px-5 ${style.lightTextBlue} ${style.info} ${style.rounded}`}>
                              PMS offer customised investment solutions to investors
                              to help them attain their financial goals.
                            </Box>
                          </Box>

                        </Box>
                      </Box>
                    </Box>
                    <Box className="m-4 text-end">
                      <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow"
                        onClick={() => {
                          setContactUsVal({
                            SubjectId: "1",
                            Description: ""
                          }
                          );
                          router.push('/Support/ContactUs')
                        }}>
                        <GroupBox>
                          {/* @ts-ignore */}
                          <Text size="h5">Contact Us</Text>
                        </GroupBox>
                      </Button>
                    </Box>
                  </Box>
                </Box>
              </Card>
            </>) : <>
            <Box className="col-md-12 col-sm-12 col-lg-12 mt-2">
              <Box background="default" css={{ p: 10, borderRadius: "8px" }}>
                {/* @ts-ignore */}
                <Box className="row">
                  <AllActiveInvestments type={"PMS"} basicId={id} userGoalId={userGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={isGoalWisePortfolio} />
                </Box>
              </Box>
            </Box>
            {loading ?
              <Loader /> :
              <Card className="mt-0" css={{ p: 15 }}>
                <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                  {/* @ts-ignore */}
                  <Accordion
                    type="single"
                    collapsible
                    onValueChange={(e: any) => {
                      fetchPms(e);
                    }}
                  >
                    {pmsPortfolioData?.map((record, index) => {
                      return (
                        <AccordionItem value={JSON.stringify(record)}>
                          <AccordionTrigger className="row p-0">
                            <Box className="col">
                              <Box className="tabtitle">
                                <Text size="h4">{record?.partner_name}</Text>
                              </Box>
                              <Box className="row row-cols-1 row-cols-md-6 tabtitlecontent">
                                <Box className="col">
                                  <Text size="h6">Cost Value</Text>
                                  <Text size="h6">
                                    {" "}
                                    &#x20B9;{" "}
                                    {Number(record?.investment).toLocaleString("en-IN")}
                                  </Text>
                                  {isGoalWisePortfolio && <>
                                    <Text size="h6" className="fw-bold">
                                      Allocated Value
                                    </Text>
                                    <Text size="h6">
                                      {" "}
                                      &#x20B9;{" "}
                                      {Number(record?.curr_goal_invested_amt).toLocaleString("en-IN")}
                                    </Text>
                                  </>
                                  }
                                </Box>
                                <Box className="col">
                                  <Text size="h6">Market Value</Text>
                                  <Text size="h6">
                                    {" "}
                                    &#x20B9;{" "}
                                    {Number(record?.current_value).toLocaleString(
                                      "en-IN"
                                    )}
                                  </Text>
                                  {isGoalWisePortfolio && <>
                                    <Text size="h6" className="fw-bold">
                                      Allocated Value
                                    </Text>
                                    <Text size="h6">
                                      {" "}
                                      &#x20B9;{" "}
                                      {Number(record?.curr_goal_current_amt).toLocaleString("en-IN")}
                                    </Text>
                                  </>
                                  }
                                </Box>
                                <Box className="col">
                                  <Text size="h6">Gain/Loss</Text>
                                  <GroupBox position="left">
                                    {parseFloat(record?.netgain) >= 0 ? (
                                      <>
                                        <Box>
                                          {/* @ts-ignore */}
                                          <Text size='h6'>
                                            {Number(record?.netgain).toLocaleString("en-IN")}
                                          </Text>
                                        </Box>
                                        <Box className="gainUp">
                                          <UpArrowLong size="10"></UpArrowLong>
                                        </Box>
                                      </>
                                    ) : (
                                      <>
                                        <Box>
                                          {/* @ts-ignore */}
                                          <Text size='h6'>
                                            {Number(record?.netgain).toLocaleString("en-IN")}
                                          </Text>
                                        </Box>
                                        <Box className="lossDown">
                                          <DownArrowLong size="10"></DownArrowLong>
                                        </Box>
                                      </>
                                    )}
                                  </GroupBox>
                                </Box>
                                <Box className="col">
                                  <Text size="h6">XIRR</Text>
                                  <Text size="h6">
                                    {" "}
                                    {Number(record?.xirr).toLocaleString(
                                      "en-IN"
                                    )}%
                                  </Text>
                                </Box>
                              </Box>
                            </Box>
                            <Box className="col col-auto action">
                              <DownArrow />
                            </Box>
                          </AccordionTrigger>

                          <AccordionContent className="border p-3">
                            {loader ? (
                              <Loader />
                            ) : (
                              <Box className="row">
                                <Box className="col-md-12 col-sm-12 col-lg-12">
                                  {/* <Card> */}
                                  {pmsData?.map((item: any, idx: any) => {
                                    return (
                                      <Box className="portfolioRow p-2">
                                        <Box className="row">
                                          <Box className="col col-12">
                                            {/* @ts-ignore */}
                                            <Text
                                              weight="normal"
                                              transform="uppercase"
                                              size="h5"
                                              color="blue1"
                                              css={{ pl: 0, pr: 0, pt: 10, pb: 0 }}
                                            >
                                              {item?.policyName}
                                            </Text>
                                          </Box>

                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Client ID</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">{item?.clientID}</Text>
                                          </Box>
                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Cost Value</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">
                                              &#x20B9;{" "}
                                              {Number(item?.investedAmt).toLocaleString(
                                                "en-IN"
                                              )}
                                            </Text>
                                          </Box>
                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Market Value</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">
                                              {" "}
                                              &#x20B9;{" "}
                                              {Number(item?.currentAmt).toLocaleString(
                                                "en-IN"
                                              )}
                                            </Text>
                                          </Box>
                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Initial Corpus</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">
                                              &#x20B9;{" "}
                                              {Number(item?.initCorpus).toLocaleString(
                                                "en-IN"
                                              )}
                                            </Text>
                                          </Box>
                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Additions</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">
                                              &#x20B9;{" "}
                                              {Number(item?.totalAddition).toLocaleString(
                                                "en-IN"
                                              )}
                                            </Text>
                                          </Box>
                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Withdrawals</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">
                                              &#x20B9;{" "}
                                              {Number(item?.totalWithdrawl).toLocaleString(
                                                "en-IN"
                                              )}
                                            </Text>
                                          </Box>
                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Gain</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">
                                              &#x20B9;{" "}
                                              {Number(item?.netIncome).toLocaleString(
                                                "en-IN"
                                              )}
                                            </Text>
                                          </Box>
                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Dividend Paid</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">
                                              &#x20B9;{" "}
                                              {Number(item?.divPaid).toLocaleString(
                                                "en-IN"
                                              )}
                                            </Text>
                                          </Box>
                                          <Box className="col mb-3">
                                            <Text size="h6" weight="bold">Inception Date</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="normal" size="h6">
                                              {item?.policyStartDate}
                                            </Text>
                                          </Box>
                                        </Box>
                                        {/* <Box> */}
                                        {/* @ts-ignore */}
                                        <Box className="row justify-content-end">
                                          <Box className="col-auto">
                                            <Box className="row justify-content-evenly mt-1 pe-3">
                                              {partnerId == MOTILAL_PMS_PARTNER_ID && <> <Button
                                                className="col-auto mb-2 "
                                                color="yellowGroup"
                                                size="md"
                                                onClick={() => {
                                                  localStorage.setItem("pms_policy_no", item?.policyNo);
                                                  localStorage.setItem("partner_name", record?.partner_name);

                                                  router.push("/Products/PMSPerformance");
                                                }}
                                              >
                                                <GroupBox
                                                  //@ts-ignore
                                                  align="center"
                                                  className={`${styles.hovBtn}`}
                                                >
                                                  <Box className="row">
                                                    {/* @ts-ignore */}
                                                    <Box className="col-auto py-2 d-flex">
                                                      {/* @ts-ignore */}
                                                      <Text weight="normal" size="h6">
                                                        View Performance
                                                      </Text>
                                                    </Box>
                                                  </Box>
                                                </GroupBox>
                                              </Button>&nbsp;&nbsp;</>}
                                              <Button
                                                className="col-auto mb-2 "
                                                color="yellowGroup"
                                                size="md"
                                                onClick={() => {
                                                  setOpenTxn(true);
                                                  setPolicyNo(item?.policyNo);
                                                  setBasicId(item?.basic_Id)
                                                }}
                                              >
                                                <GroupBox
                                                  //@ts-ignore
                                                  align="center"
                                                  className={`${styles.hovBtn}`}
                                                >
                                                  <Box className="row">
                                                    {/* @ts-ignore */}
                                                    <Box className="col-auto py-2 d-flex">
                                                      {/* @ts-ignore */}
                                                      <Text weight="normal" size="h6">
                                                        Transaction Details
                                                      </Text>
                                                    </Box>
                                                  </Box>
                                                </GroupBox>
                                              </Button>
                                            </Box>
                                          </Box>
                                        </Box>
                                        {/* </Box> */}
                                      </Box>
                                    );
                                  })}
                                  {/* </Card> */}
                                </Box>
                              </Box>
                            )}
                          </AccordionContent>
                        </AccordionItem>
                      );
                    })}
                  </Accordion>
                  <DialogModal
                    open={openTxn}
                    setOpen={setOpenTxn}
                    css={{
                      "@bp0": { width: "80%" },
                      "@bp1": { width: "40%" },
                    }}
                  // className=""
                  >
                    <TxnD setOpen={setOpenTxn} id={basicId} polid={policyNo} partid={partnerId} type={"PMS"} />
                  </DialogModal>
                </Box>
              </Card>}
          </>}
      </>)
      }
    </>
  )
}

export default PMSPage