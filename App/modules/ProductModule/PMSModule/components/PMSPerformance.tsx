import Box from "@ui/Box/Box";
import Loader from "@ui/SimpleGrid/components/Loader";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { getMotilalPMSDetails } from "App/api/dashboard";
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, isNumeric } from "App/utils/helpers";
import { round } from "lodash";
import { useEffect, useState } from "react";
import Text from "@ui/Text/Text";

const PMSPerformance = () => {

    const [policyNo, setPolicyNo] = useState("");
    const [partnerName, setPartnerName] = useState("");
    const [loader, setLoader] = useState(false);
    const [pmsPerfDetails, setPMSPerfDetails] = useState<any>([]);

    const getClientName = (value: string, searchValue: string) => {
        if (value == undefined) return "";

        let strValue = value;

        strValue = strValue.replace(searchValue, "");

        return strValue;
    }

    const fetchPMSPerformance = async () => {
        let val: string = (localStorage.getItem("pms_policy_no")?.toString() || "");

        let ptnerName: string = (localStorage.getItem("partner_name")?.toString() || "");

        setPolicyNo(val);
        setPartnerName(ptnerName);

        try {
            setLoader(true);

            let obj: any = {
                PmsCode: val
            }
            const enc: any = encryptData(obj);

            let result: any = await getMotilalPMSDetails(enc);
            setPMSPerfDetails(result?.data);

            setLoader(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    }

    useEffect(() => {
        fetchPMSPerformance();
    }, []);

    return (
        <>
            {loader ? <>
                <Box className="text-center"><Loader></Loader>
                </Box></> : pmsPerfDetails?.length != 0 &&
            <Box className="container">
                <Box className="row">
                    <Box className="col-12">
                        <Text size="h5">
                            <span style={{ color: "var(--colors-blue1)", fontWeight: "bold" }}>{partnerName}</span> - <span>{pmsPerfDetails?.clientInfo?.schemename} ({pmsPerfDetails?.clientInfo?.backofficecodeequity})</span>
                        </Text>
                    </Box>
                </Box>
                <Box className="row">
                    <Box className="col-12">
                        <Text size="h5">
                            <span style={{ color: "var(--colors-blue1)", fontWeight: "bold" }}>{getClientName(pmsPerfDetails?.clientInfo?.cl_name, pmsPerfDetails?.clientInfo?.backofficecodeequity)} Portfolio</span> - <span>Since Inception</span>
                        </Text>
                    </Box>
                </Box>
                <Box className="row">
                    <Box className="col-12">
                        &nbsp;
                    </Box>
                </Box>
                <Box className="row">
                    <Box className="col-12">
                        <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto", maxHeight: "400px" }}>
                                <Table>
                                    <ColumnParent css={{ position: "sticky", top: "0" }}
                                        className="border border-bottom borderRadiusForTable text-capitalize">
                                        <ColumnRow className="borderRadiusForTable"
                                            css={{ color: "var(--colors-blue1)", textAlign: "center", borderRadius: "8px" }}>
                                            <Column className="ps-2" css={{ textAlign: "left" }}>Period</Column>
                                            <Column>MTD</Column>
                                            <Column>YTD</Column>
                                            <Column>1 YEAR</Column>
                                            <Column>3 YEARS</Column>
                                            <Column>Since Inception</Column>
                                            <Column>Selected Period</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        <DataRow css={{ textAlign: "center", borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Your Portfolio</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.mtdRet, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.ytdRet, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.last1YrRet, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.last3YrRet, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.sinInceRet, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.periodRet, 2)}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ textAlign: "center" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>{pmsPerfDetails?.clientInfo?.defindexname}</DataCell>
                                            <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.mtdBen), 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.ytdBen, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.last1YrTWRR, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.last3YrsTWRR, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.sinInceBen, 2)}</DataCell>
                                            <DataCell>{round(pmsPerfDetails?.clientInfo?.periodBen, 2)}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </Box>
                </Box>
                <Box className="row mx-auto" css={{ borderTop: "2px solid #005CB3" }}>
                    <Box className="col-12">&nbsp;</Box>
                </Box>
                <Box className="row">
                    <Box className="col-12 col-sm-4">
                        <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto", maxHeight: "400px" }}>
                                <Table>
                                    <ColumnParent css={{ position: "sticky", top: "0" }}
                                        className="border border-bottom borderRadiusForTable text-capitalize" >
                                        <ColumnRow className="borderRadiusForTable"
                                            css={{ color: "var(--colors-blue1)", textAlign: "center", borderRadius: "8px" }}>
                                            <Column className="ps-2" colSpan={2} css={{ textAlign: "left" }}>Corpus Details</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Portfolio Inception Date</DataCell>
                                            <DataCell css={{ textAlign: "right" }}>{pmsPerfDetails?.clientInfo?.cntjoindate}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Initial Corpus</DataCell>
                                            <DataCell css={{ textAlign: "right" }}>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.initcorpus), 2).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Additions</DataCell>
                                            <DataCell css={{ textAlign: "right" }}>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.totaladditions), 2).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Withdrawals</DataCell>
                                            <DataCell css={{ textAlign: "right" }}>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.totalwithdrawal), 2).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Net Capital Invested</DataCell>
                                            <DataCell css={{ textAlign: "right" }}>&#x20B9;{(round(Number(pmsPerfDetails?.clientInfo?.initcorpus), 2) +
                                                round(Number(pmsPerfDetails?.clientInfo?.totaladditions), 2) -
                                                round(Number(pmsPerfDetails?.clientInfo?.totalwithdrawal), 2)).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Dividend Paid</DataCell>
                                            <DataCell css={{ textAlign: "right" }}>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.totaldividend), 2).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Net Income</DataCell>
                                            <DataCell css={{ textAlign: "right" }}>&#x20B9;{((round(Number(pmsPerfDetails?.clientInfo?.total), 2) +
                                                round(Number(pmsPerfDetails?.clientInfo?.totaldividend), 2)) -
                                                (round(Number(pmsPerfDetails?.clientInfo?.initcorpus), 2) +
                                                    round(Number(pmsPerfDetails?.clientInfo?.totaladditions), 2)) -
                                                (round(Number(pmsPerfDetails?.clientInfo?.totalwithdrawal), 2))).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Current Strategy Value</DataCell>
                                            <DataCell css={{ textAlign: "right" }}>&#x20B9;{(round(Number(pmsPerfDetails?.clientInfo?.total), 2)).toLocaleString("en-IN")}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ color: "#FFFFFF", backgroundColor: "#005CB3" }}>
                                            <DataCell colSpan={2}>&nbsp;</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </Box>
                    <Box className="col-12 col-sm-4">
                        <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto", maxHeight: "400px" }}>
                                <Table>
                                    <ColumnParent css={{ position: "sticky", top: "0" }}
                                        className="border border-bottom borderRadiusForTable text-capitalize">
                                        <ColumnRow className="borderRadiusForTable"
                                            css={{ color: "var(--colors-blue1)", textAlign: "center", borderRadius: "8px" }}>
                                            <Column className="ps-2" colSpan={3} css={{ textAlign: "center" }}>Asset Allocation</Column>
                                        </ColumnRow>
                                        <ColumnRow>
                                            <Column className="ps-2" css={{ textAlign: "left" }}>Asset Class</Column>
                                            <Column className="ps-2" css={{ textAlign: "left" }}>Market Value</Column>
                                            <Column className="ps-2" css={{ textAlign: "left" }}>%</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Equity</DataCell>
                                            <DataCell>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.strmktvalue), 2).toLocaleString("en-IN")}</DataCell>
                                            <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.equityPer), 2)}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Debt</DataCell>
                                            <DataCell>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.debtmktvalue), 2).toLocaleString("en-IN")}</DataCell>
                                            <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.debtPer), 2)}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Derivatives</DataCell>
                                            <DataCell>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.derivmktvalue), 2).toLocaleString("en-IN")}</DataCell>
                                            <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.derivativesPer), 2)}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Mutual Funds</DataCell>
                                            <DataCell>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.strmfmktvalue), 2).toLocaleString("en-IN")}</DataCell>
                                            <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.mutualFundsPer), 2)}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Cash & Cash Equivalent</DataCell>
                                            <DataCell>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.strfundspendinvst), 2).toLocaleString("en-IN")}</DataCell>
                                            <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.cashEquivalentPer), 2)}</DataCell>
                                        </DataRow>
                                        <DataRow css={{ color: "#FFFFFF", backgroundColor: "#005CB3" }}>
                                            <DataCell className="ps-1" css={{ textAlign: "left" }}>Total</DataCell>
                                            <DataCell>&#x20B9;{round(Number(pmsPerfDetails?.clientInfo?.total), 2).toLocaleString("en-IN")}</DataCell>
                                            <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.totalPer), 2)}</DataCell>
                                        </DataRow>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </Box>
                    <Box className="col-12 col-sm-4">
                        <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto", maxHeight: "400px" }}>
                                <Table>
                                    <ColumnParent css={{ position: "sticky", top: "0" }}
                                        className="border border-bottom borderRadiusForTable text-capitalize">
                                        <ColumnRow className="borderRadiusForTable"
                                            css={{ color: "var(--colors-blue1)", textAlign: "center", borderRadius: "8px" }}>
                                            <Column className="ps-2" css={{ textAlign: "left" }}>Portfolio Holdings</Column>
                                            <Column className="ps-2" css={{ textAlign: "left" }}>Quantity</Column>
                                            <Column className="ps-2" css={{ textAlign: "left" }}>(%) of Portfolio</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {pmsPerfDetails?.lstDetails.length != 0 && <>
                                            {pmsPerfDetails?.lstDetails?.map((record: any) => {
                                                return (<DataRow>
                                                    <DataCell className="ps-1" css={{ textAlign: "left" }}>{(record?.shortname)}</DataCell>
                                                    <DataCell>{isNumeric(record?.qty) ? round(Number((record?.qty)), 2).toLocaleString("en-IN") : record?.qty}</DataCell>
                                                    <DataCell>{round(Number(record?.mreturn), 2)}</DataCell>
                                                </DataRow>);
                                            })}
                                            <DataRow css={{ color: "#FFFFFF", backgroundColor: "#005CB3" }}>
                                                <DataCell className="ps-1" css={{ textAlign: "left" }}>Total</DataCell>
                                                <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.total), 2).toLocaleString("en-IN")}</DataCell>
                                                <DataCell>{round(Number(pmsPerfDetails?.clientInfo?.totalPer), 2)}</DataCell>
                                            </DataRow>
                                        </>}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Box>
            }
        </>
    );
}

export default PMSPerformance;