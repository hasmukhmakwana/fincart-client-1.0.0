import React from 'react';
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import CheckCircleFill from "App/icons/CheckCircleFill";
import { GroupBox } from "@ui/Group/Group.styles";
import Loader from 'App/shared/components/Loader';
type SaveTypes = {
    modalData: (value: any) => void;
    load: (value: any) => boolean;
};
const PMSViewModal = ({ modalData, load }: SaveTypes) => {
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text weight="bold" css={{ color: "var(--colors-blue1)" }}> View Details</Text>
                </Box>
                <Box className="modal-body modal-body-scroll p-3">
                    {/* @ts-ignore */}
                    {load ? <><Box className="py-1 my-1"><Loader /></Box></> : <>
                        {/* @ts-ignore */}

                        {modalData.length === 0 ? <> <Box className="text-start">No record Found</Box></> : <>
                            {/* @ts-ignore */}
                            {modalData?.map((item: any) => {
                                return (<>
                                    <Box className="text-start">
                                        {/* @ts-ignore */}
                                        <Text css={{ mb: 10 }} color="mediumBlue" className="my-1 px-1 mx-2">
                                            {item?.policyName}
                                        </Text>
                                        <hr className="p-0 m-0 mb-2" />
                                        <Box className="row px-1 pb-2">
                                            <Box className="col-12">
                                                <Box className="row">
                                                    <Box className="col-5 col-lg-2 col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >PolicyNo / Status</Text>
                                                        <GroupBox>
                                                            <Box><CheckCircleFill color="var(--colors-blue1)"></CheckCircleFill></Box>
                                                            {/* @ts-ignore */}
                                                            <Box><Text >{item?.policyNo}</Text></Box>
                                                        </GroupBox>
                                                    </Box>
                                                    <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >Cost Value</Text>
                                                        {/* @ts-ignore */}
                                                        <Text >{Number(Math.round(item?.investedAmt)).toLocaleString("en-IN")}</Text>
                                                    </Box>
                                                    <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >Market Value</Text>
                                                        {/* @ts-ignore */}
                                                        <Text >{Number(Math.round(item?.currentAmt)).toLocaleString("en-IN")}</Text>
                                                    </Box>
                                                    <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >Initial Corpus</Text>
                                                        {/* @ts-ignore */}
                                                        <Text >{Number(Math.round(item?.initCorpus)).toLocaleString("en-IN")}</Text>
                                                    </Box>
                                                    <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >Additions</Text>
                                                        {/* @ts-ignore */}
                                                        <Text >{Number(Math.round(item?.totalAddition)).toLocaleString("en-IN")}</Text>
                                                    </Box>
                                                    <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >Withdrawals</Text>
                                                        {/* @ts-ignore */}
                                                        <Text >{Number(Math.round(item?.totalWithdrawl)).toLocaleString("en-IN")}</Text>
                                                    </Box>
                                                    <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >Gain</Text>
                                                        {/* @ts-ignore */}
                                                        <Text >{Number(Math.round(item?.netIncome)).toLocaleString("en-IN")}</Text>
                                                    </Box>
                                                    <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >Divided Paid</Text>
                                                        {/* @ts-ignore */}
                                                        <Text >{Number(Math.round(item?.divPaid)).toLocaleString("en-IN")}</Text>
                                                    </Box>
                                                    <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                                                        {/* @ts-ignore */}
                                                        <Text >Inception Date</Text>
                                                        {/* @ts-ignore */}
                                                        <Text >{item?.policyStartDate}</Text>
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </Box>
                                    </Box>
                                </>);
                            })}
                        </>}
                    </>}

                </Box>
            </Box>
        </>
    )
}

export default PMSViewModal