import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import Layout from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import router from "next/router";
import PMSPerformance from "./PMSPerformance";
import Text from "@ui/Text/Text";
import style from "../../Product.module.scss";

const PMSPerformanceDetailsPage = () => {
    return (
        <Layout>
            <PageHeader title="Portfolio Management Service - Performance" rightContent={<>
                <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow"
                    onClick={() => { router.push("/Products/PMS"); }}>
                    <GroupBox>
                        <Text size="h5">Back</Text>
                    </GroupBox>
                </Button>
            </>} />
            <Card css={{ p: 15, mb: 20 }}>
                <PMSPerformance></PMSPerformance>
            </Card>
        </Layout>
    )
}

export default PMSPerformanceDetailsPage;