import Box from '@ui/Box/Box'
import React, { useEffect } from 'react'
import Text from "@ui/Text/Text";
import {
    Column,
    ColumnParent,
    ColumnRow,
    DataCell,
    DataParent,
    DataRow,
    Table,
} from "@ui/SimpleGrid/DataGrid.styles";
// @ts-ignore
import useInvestmentStore from "../../InvestmentModule/store";
import Loader from "App/shared/components/Loader";
import style from "../Product.module.scss";

// @ts-ignore
const MoreDetails = ({ loadManStatus }) => {
    const {
        mandateList
    } = useInvestmentStore();

    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}>
                        {" "}
                        MoreDetails
                        {/*  */}
                    </Text>
                </Box>
                <Box className={`modal-body p-3 ${style.scrollit}`}  >
                    <Box>
                        <Table className={`text-capitalize table table-striped`} css={{ color: "var(--colors-blue1)" }} >
                            <ColumnParent className="text-capitalize sticky">
                                <ColumnRow css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}>
                                    <Column><Text size="h5">Name</Text></Column>
                                    <Column><Text size="h5">Relation</Text></Column>
                                    <Column><Text size="h5">Share</Text></Column>
                                    {/* <Column><Text size="h5">Current Value(Rs.)</Text></Column> */}
                                </ColumnRow>
                            </ColumnParent>
                            <DataParent css={{ textAlign: "center" }} >
                                {loadManStatus ? (
                                    <DataCell colSpan={3}>
                                        <Loader />
                                    </DataCell>
                                ) : (
                                    <>
                                        {mandateList.map((record: any, index: any) => {
                                            console.log(record, "man record");

                                            return (
                                                <DataRow>
                                                    <DataCell >
                                                        <Box>
                                                            <Text className={`${style.cellStyle}`} css={{ textAlign: "left" }}>
                                                                {" "}
                                                                {record?.MandateID}<br />
                                                                {" "}
                                                                <span style={{ fontSize: "0.65rem", color: "grey" }}>[{record?.mandatetype === "P" ? "Physical" : "E-Mandate"}]</span>
                                                            </Text>
                                                        </Box>
                                                    </DataCell>
                                                    <DataCell >
                                                        <Box>
                                                            <Text className={`${style.cellStyle}`} css={{ textAlign: "left" }}>
                                                                {" "}
                                                                {record?.profilename}<br />
                                                                {" "}
                                                                <span style={{ fontSize: "0.65rem", color: "grey" }}>[{record?.moh}]</span>
                                                            </Text>
                                                        </Box>
                                                    </DataCell>
                                                    <DataCell >
                                                        <Text className={`${style.cellStyle}`}>
                                                            {" "}
                                                            {record?.Upload_status}
                                                            {" "}
                                                        </Text>
                                                    </DataCell>
                                                </DataRow>
                                            )
                                        })}
                                    </>
                                )}
                            </DataParent>
                        </Table>
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default MoreDetails
