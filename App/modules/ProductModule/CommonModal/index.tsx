export { default as TxnD } from './TxnDetails'
export { default as Fund } from './FundDetails'
export { default as Nominee } from './NomineeDetails'
export { default as More } from './MoreDetails'