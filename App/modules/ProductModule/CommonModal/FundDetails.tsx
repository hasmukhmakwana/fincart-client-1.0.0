import Box from '@ui/Box/Box'
import React, { useEffect, useState } from 'react'
import Text from "@ui/Text/Text";
import {
    Column,
    ColumnParent,
    ColumnRow,
    DataCell,
    DataParent,
    DataRow,
    Table,
} from "@ui/SimpleGrid/DataGrid.styles";

import Loader from "App/shared/components/Loader";
import style from "../Product.module.scss";
import { getFundDetails } from 'App/api/dashboard';
import { toastAlert } from 'App/shared/components/Layout';
import { encryptData } from 'App/utils/helpers';
const FundDetails = ({ setOpen, id, polid, partid, type }: any) => {
    const [detailsLoader, setDetailsLoader] = useState(false);
    const [fundData, setFundData] = useState<any>([]);
    const [totVal, setTotalVal] = useState<any>("");
    const fetchFundDetails = async () => {

        try {
            setDetailsLoader(true);
            let obj: any = {
                basicId: id,
                productId: polid,
                investment_type: type,
                partnerId: partid
            }
            const enc: any = encryptData(obj);
            let result: any = await getFundDetails(enc);
            setFundData(result?.data);
            setTotalVal(result?.data.map((i: any) => i.currentAmt).reduce((a: any, b: any) => parseInt(a) + parseInt(b), 0));
            setDetailsLoader(false);
        } catch (error: any) {
            console.log(error);
            {
                error?.msg === 'No Data Found!!' ?
                    toastAlert("warn", error)
                    :
                    toastAlert("error", error)
            }
            setDetailsLoader(false);
        }

    }
    useEffect(() => {
        fetchFundDetails();
        return () => {
            setFundData([]);
        }
    }, [])
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}>
                        {" "}
                        Fund Allocation details
                        {/*  */}
                    </Text>
                </Box>
                <Box className={`modal-body p-3 ${style.scrollit}`}  >
                    <Box>
                        <Table className={`text-capitalize table table-striped`} css={{ color: "var(--colors-blue1)" }} >
                            <ColumnParent className="text-capitalize sticky">
                                <ColumnRow className={`${style.scroll}`}>
                                    <Column><Text size="h5">Fund</Text></Column>
                                    <Column><Text size="h5">Units</Text></Column>
                                    <Column><Text size="h5">Nav(Rs.)</Text></Column>
                                    <Column><Text size="h5">Current Value(Rs.)</Text></Column>
                                </ColumnRow>
                            </ColumnParent>
                            <DataParent css={{ textAlign: "center" }} >
                                {detailsLoader ? (
                                    <DataCell colSpan={4}>
                                        <Loader />
                                    </DataCell>
                                ) : (
                                    <>
                                        {fundData?.length === 0 ? <>
                                            <DataCell colSpan={4}>
                                                <Box>
                                                    <Text className={`${style.cellStyle}`} css={{ textAlign: "center" }}>
                                                        {" "}
                                                        No Record Found
                                                        {" "}
                                                    </Text>
                                                </Box>
                                            </DataCell>
                                        </> :
                                            <>
                                                {fundData.map((record: any, index: any) => {
                                                    return (
                                                        <DataRow>
                                                            <DataCell >
                                                                <Box>
                                                                    <Text className={`${style.cellStyle}`} css={{ textAlign: "left" }}>
                                                                        {" "}
                                                                        {record?.fundName}
                                                                    </Text>
                                                                </Box>
                                                            </DataCell>
                                                            <DataCell >
                                                                <Box>
                                                                    <Text className={`${style.cellStyle}`} css={{ textAlign: "left" }}>
                                                                        {" "}
                                                                        {record?.unit}
                                                                        {" "}
                                                                    </Text>
                                                                </Box>
                                                            </DataCell>
                                                            <DataCell >
                                                                <Text className={`${style.cellStyle}`}>
                                                                    {" "}
                                                                    {record?.nav}
                                                                    {" "}
                                                                </Text>
                                                            </DataCell>
                                                            <DataCell >
                                                                <Text className={`${style.cellStyle}`}>
                                                                    {" "}
                                                                    {Number(record?.currentAmt).toLocaleString("en-IN") || "0.0"}
                                                                    {" "}
                                                                </Text>
                                                            </DataCell>
                                                        </DataRow>
                                                    )
                                                })}
                                            </>}
                                    </>
                                )}
                                <DataRow>
                                    <DataCell colSpan={3}>
                                        <Box>
                                            <Text className={`${style.cellStyle}`} css={{ textAlign: "left" }}>
                                                {" "}
                                                Total Current Value(Rs.) :
                                                {" "}
                                            </Text>
                                        </Box>
                                    </DataCell>
                                    <DataCell >
                                        <Box>
                                            <Text className={`${style.cellStyle}`} css={{ textAlign: "center" }}>
                                                {" "}
                                                {Number(totVal).toLocaleString("en-IN") || "0.0"}
                                            </Text>
                                        </Box>
                                    </DataCell>
                                </DataRow>
                            </DataParent>
                        </Table>
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default FundDetails
