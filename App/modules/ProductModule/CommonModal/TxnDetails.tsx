import Box from '@ui/Box/Box'
import React, { useEffect, useState } from 'react'
import Text from "@ui/Text/Text";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/SimpleGrid/DataGrid.styles";
import Loader from "App/shared/components/Loader";
import style from "../Product.module.scss";
import { getPrePaidDetails } from 'App/api/dashboard';
import { toastAlert } from 'App/shared/components/Layout';
import { encryptData } from 'App/utils/helpers';
// @ts-ignore
const TxnDetails = ({ setOpen, id, polid, partid, type }: any) => {

  const [detailsLoader, setDetailsLoader] = useState(false);
  const [txnData, setTxnData] = useState<any>([]);
  const fetchTxnDetails = async () => {

    try {
      setDetailsLoader(true);
      let obj: any = {
        basicId: id,
        productId: polid,
        investment_type: type,
        partnerId: partid
      }
      const enc: any = encryptData(obj);
      let result: any = await getPrePaidDetails(enc);
      setTxnData(result?.data);

      setDetailsLoader(false);
    } catch (error: any) {
      console.log(error);
      {
        error?.msg === 'No Data Found!!' ?
          toastAlert("warn", error)
          :
          toastAlert("error", error)
      }
      setDetailsLoader(false);
    }

  }
  useEffect(() => {
    fetchTxnDetails();
    return () => {
      setTxnData([]);
    }
  }, [])

  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}>
            {" "}
            Transactions details
            {/*  */}
          </Text>
        </Box>
        <Box className={`modal-body p-3 ${style.scrollit}`}  >
          <Box>
            <Table className={`text-capitalize table table-striped`} css={{ color: "var(--colors-blue1)" }} >
              <ColumnParent className="text-capitalize sticky">
                <ColumnRow className={`${style.scroll}`}>
                  <Column><Text size="h5">Amount</Text></Column>
                  <Column><Text size="h5">Status</Text></Column>
                  <Column><Text size="h5">Date</Text></Column>
                  <Column><Text size="h5">Mode</Text></Column>
                </ColumnRow>
              </ColumnParent>
              <DataParent css={{ textAlign: "center" }} >
                {detailsLoader ? (
                  <DataCell colSpan={4}>
                    <Loader />
                  </DataCell>
                ) : (
                  <>
                    {txnData?.length === 0 ? <>
                      <DataCell colSpan={4}>
                        <Box>
                          <Text className={`${style.cellStyle}`} css={{ textAlign: "center" }}>
                            {" "}
                            No Record Found
                            {" "}
                          </Text>
                        </Box>
                      </DataCell>
                    </> :
                      <>
                        {txnData?.map((record: any, index: any) => {

                          return (
                            <DataRow>
                              <DataCell >
                                <Box>
                                  <Text className={`${style.cellStyle}`} css={{ textAlign: "center" }}>
                                    {" "}
                                    {record?.amt}<br />
                                    {" "}
                                  </Text>
                                </Box>
                              </DataCell>
                              <DataCell >
                                <Box>
                                  <Text className={`${style.cellStyle}`} css={{ textAlign: "center" }}>
                                    {" "}
                                    {record?.status}<br />
                                    {" "}
                                  </Text>
                                </Box>
                              </DataCell>
                              <DataCell >
                                <Text className={`${style.cellStyle}`}>
                                  {" "}
                                  {record?.date}
                                  {" "}
                                </Text>
                              </DataCell>
                              <DataCell >
                                <Text className={`${style.cellStyle}`}>
                                  {" "}
                                  {record?.mode}
                                  {" "}
                                </Text>
                              </DataCell>
                            </DataRow>
                          )
                        })}</>}
                  </>
                )}
              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default TxnDetails
