import Layout from 'App/shared/components/Layout'
import React from 'react'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import UnMappedInvest from "./components/UnMappedInvestPage"
const UnMappedInvests = () => {
    return (
        <Layout>
            <PageHeader title="UnMapped Investment" rightContent={<></>} />
            <UnMappedInvest />
        </Layout>
    )
}

export default UnMappedInvests