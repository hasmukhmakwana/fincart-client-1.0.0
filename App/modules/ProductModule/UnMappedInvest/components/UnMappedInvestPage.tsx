import React, { useState, useEffect } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import * as Select from '@radix-ui/react-select'
import style from "../../Product.module.scss";
import router from "next/router";
import { Formik } from "formik";
import * as Yup from "yup";
import { RadioGroup } from "@radix-ui/react-radio-group";
import Radio from "@ui/Radio/Radio";
import Card from "@ui/Card/Card";
import Input from "@ui/Input";
import { GroupBox } from "@ui/Group/Group.styles";
import { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import { allUnmappedInvestments, mapUnMappedInvestment } from "App/api/tools";
import { getAllMemberPurchaseDetails } from "App/api/mandate";
import { GoalsSections } from "App/utils/constants";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import CheckCircleFill from "App/icons/CheckCircleFill";
import Loader from "App/shared/components/Loader";
import SelectMenu from "@ui/Select/Select";
import useRegistrationStore from "App/modules/RegistrationModule/store";

const UnMappedInvestPage = () => {
    const user: any = getUser();
    const [filtertype, setFilterType] = useState("NONZERO");
    const [currentSection, setCurrentSection] = useState<GoalsSections>();
    const [openModal, setOpenModal] = useState<any>(false);
    const [openSuccess, setOpenSuccess] = useState<any>(false);
    const local: any = getUser();
    const [checkedAll, setCheckedAll] = useState(false);
    const [checked, setChecked] = useState<any>([]);
    const [goalsInvestmentList, setGoalsInvestmentList] = useState<any>([])
    const [member, setMember] = useState<any>([])
    const [selectMember, setSelectedMember] = useState<any>({});
    const [memberAcc, setMemberAcc] = useState<any>([])
    const [bankList, setBankList] = useState<any>([])
    const [openInvestor, setOpenInvestor] = useState<any>("");
    const [openInvestorAcc, setOpenInvestorAcc] = useState<any>("");
    const [investorAccountList, setInvestorAccountList] = useState<any>([]);
    const [loader, setLoader] = useState(false);
    const [loaderM, setLoaderM] = useState(false);
    const { setBasicID, setRegiType } = useRegistrationStore();

    const fetchGoalsInvestmentsList = async () => {
        try {
            if (!user?.basicid) return;

            setLoader(true);

            let currentUserGoalId: any = localStorage.getItem("userGoalId");

            let section: any = localStorage.getItem("section");

            setCurrentSection(section);

            let obj: any = {
                basicid: user?.basicid,
                filtertype: filtertype //ALL/NONZERO/ZERO
            }

            const enc: any = encryptData(obj);
            //console.log(enc);
            const result: any = await allUnmappedInvestments(enc);
            setCheckedAll(false);
            setChecked([]);
            setLoader(false);
            setGoalsInvestmentList(result?.data.map((i: any, index: any) => ({ ...i, number: `${index + 1}` })));

        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    }

    const fetchMemberList = async () => {
        try {
            setLoaderM(true);
            const enc: any = encryptData({
                basicId: user?.basicid,
                fundid: "0",
            });

            let result = await getAllMemberPurchaseDetails(enc);

            setMember(result?.data?.memberList);
            setMemberAcc(result?.data?.profileList);
            setBankList(result?.data?.bankList);
            setLoaderM(false);
            setSelectedMember(result?.data?.memberList?.[0])
            setOpenInvestor(result?.data?.memberList?.[0]?.basicid)
            let arr: any = result?.data?.profileList.filter((item: any) => {
                return item?.basicid === result?.data?.memberList?.[0]?.basicid;
            });
            setInvestorAccountList(arr);
            setOpenInvestorAcc(arr[0]?.profileID);
        } catch (error: any) {
            console.log(error);
            setLoaderM(false);
            toastAlert("error", error);
        }
    };

    const saveUnMappedInvestment = async () => {
        // console.log("call");
        if (openInvestor === "" || openInvestorAcc === "" || openInvestor === undefined || openInvestorAcc === undefined) {
            // console.log(openInvestor,openInvestorAcc)
            toastAlert("warn", "Please, Select Investor and Investor account ");
            return;
        }
        try {
            setLoaderM(true);
            // console.log(checked)

            let obj: any = checked?.map((i: any) => {
                return (
                    {
                        basicid: i?.basicid,
                        foliono: i?.foliono,
                        profileid: openInvestorAcc,
                        schemeid: i?.schemeid,
                        newbasicid: openInvestor,
                    });
            })
            // console.log(obj, "obj")
            const enc: any = encryptData(obj);
            // console.log(enc, "enc");
            let result: any = await mapUnMappedInvestment(enc);

            if (result != undefined && result?.status == "Success") {
                toastAlert("success", "Investment(s) Mapped Successfully!, Portfolio will be synced after 24 hrs");
            }

            fetchGoalsInvestmentsList();
            setOpenModal(false);
            // setChecked([]);
            // setCheckedAll(false);
            setLoaderM(false);

        } catch (error: any) {
            console.log(error);
            setLoaderM(false);
            toastAlert("error", error);
        }
    }

    const selectAll = (val: any) => {
        // console.log(val);
        // console.log(goalsInvestmentList, "click")
        if (val) {
            let arr: any = [];
            goalsInvestmentList.forEach((ele: any) => {
                // if (ele?.isDelete === "Y") {
                let i: any = ele.number;
                let check: any = document.getElementById(i);
                check.checked = val
                let obj: any = {
                    basicid: ele?.basicID,
                    foliono: ele?.folioNo,
                    profileid: "",
                    schemeid: ele?.schemeId,
                    newbasicid: "",
                    indexNo: ele?.number
                }
                arr.push(obj);
                // }
            });
            setChecked(arr);
        } else {
            goalsInvestmentList.forEach((ele: any) => {
                // if (ele?.isDelete === "Y") {
                let i: any = ele.number;
                let check: any = document.getElementById(i);
                check.checked = val
                // }
            });
            setChecked([]);
        }
    };

    useEffect(() => {
        (async () => {
            await fetchGoalsInvestmentsList();

        })();

    }, [filtertype]);

    useEffect(() => {
        (async () => {
            await fetchMemberList();
        })();
        return () => {
            setChecked([])
        }
    }, []);

    useEffect(() => {
        let arr = memberAcc.filter((item: any) => {
            return item?.basicid === openInvestor;
        });
        // console.log(arr, "arr");
        setInvestorAccountList(arr);
        setOpenInvestorAcc(arr[0]?.profileID);
    }, [openInvestor])


    useEffect(() => {
        // console.log(checked, "checked");

        if (goalsInvestmentList.length > 0 && goalsInvestmentList.length === checked.length) {
            setCheckedAll(true);
        } else {
            setCheckedAll(false);
        }
        return () => {
        }
    }, [checked])


    return (
        <>
            <Box className="container">
                <Box className="row row-cols-1 row-cols-md-2 justify-content-start">
                    {/* <Box className="col-md-12 col-sm-12 col-lg-12 text-start"> */}
                    <Button
                        className={`col-auto mb-2`}
                        color="yellowGroup"
                        size="md"
                        onClick={() => router.push("/Products/MutualFund")}
                    >
                        {/* @ts-ignore */}
                        <GroupBox align="center" className={`${style.hovBtn}`}>
                            <Box className="row">
                                <Box className="ps-2 col-auto">

                                </Box>
                                {/* @ts-ignore */}
                                <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                    {/* @ts-ignore */}
                                    <Text size="h6">
                                        Back
                                    </Text>
                                </Box>
                            </Box>
                        </GroupBox>
                    </Button>
                    {/* </Box> */}
                </Box>
                <Box className="row row-cols-1 row-cols-md-2">
                    <Box className="col-md-12 col-sm-12 col-lg-12 text-left modal-header p-2" css={{ borderRadius: "8px" }}>
                        <Box className="row justify-content-between">
                            <Box className="col-auto">
                                <RadioGroup
                                    defaultValue={filtertype}
                                    className="inlineRadio"
                                    onValueChange={(e: any) => {
                                        setFilterType(e);
                                    }}
                                >
                                    <Radio value="ALL" label="All Folios" id="all" />
                                    <Radio value="NONZERO" label="Active Folios" id="active" />
                                    <Radio value="ZERO" label="Inactive Folios" id="inactive" />
                                </RadioGroup>
                            </Box>
                            <Box className="col-auto">
                                <Box className="row justify-content-between">
                                    <Box className="col-auto">
                                        <Box className="row pt-1">
                                            {goalsInvestmentList.length > 1 ? <>

                                                <Box className="col-auto pe-0">
                                                    <Input
                                                        className={`px-0`}
                                                        type="checkbox"
                                                        name={`checkAll`}
                                                        // label="Select All"
                                                        // value={checkedAll}
                                                        disabled={loader}
                                                        checked={checkedAll}
                                                        onChange={(e: any) => {
                                                            selectAll(e.target.checked)
                                                            setCheckedAll(e.target.checked)
                                                        }}
                                                    />
                                                </Box>
                                                <Box className="col-auto ps-1">
                                                    <Text
                                                        className={`px-0 pt-1`}
                                                        //@ts-ignore
                                                        size="h6"
                                                        css={{ color: "var(--colors-blue1)" }} >
                                                        Select All
                                                    </Text>
                                                </Box>
                                            </> : <></>}
                                        </Box>

                                    </Box>
                                    <Box className="col-auto">
                                        <Button
                                            // className={`mb-2`}
                                            color="yellowGroup"
                                            size="md"
                                            disabled={goalsInvestmentList.length === 0 || loader}
                                            onClick={() => {
                                                // if (member !== null) {
                                                if (checked.length !== 0) {
                                                    setOpenModal(true);
                                                    fetchMemberList();
                                                } else {
                                                    toastAlert("warn", "Please select atleast one investment to map!")
                                                }
                                                // }
                                            }}
                                        >
                                            {/* @ts-ignore */}
                                            <GroupBox align="center" className={`${style.hovBtn}`}>
                                                <Box className="row">
                                                    <Box className="ps-2 col-auto">
                                                        {/* <Upload style={{ weight: "bold" }} /> */}
                                                    </Box>
                                                    {/* @ts-ignore */}
                                                    <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                                        {/* @ts-ignore */}
                                                        <Text size="h6">
                                                            Map Member
                                                        </Text>
                                                    </Box>
                                                </Box>
                                            </GroupBox>
                                        </Button>
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Box>
                <Box className="row row-cols-1 row-cols-md-2">
                    {
                        loader ?
                            <Loader />
                            : <>
                                <Box className="col-md-12 col-sm-12 col-lg-12">
                                    <Box className="row">
                                        {goalsInvestmentList?.length !== 0 ? <>
                                            {goalsInvestmentList?.map((currentScheme: any) => {
                                                // console.log(currentScheme, "currentScheme")
                                                return (
                                                    <Box className="col-12 col-md-6 col-lg-6 my-2">
                                                        <Card
                                                            css={{
                                                                borderRadius: "8px 8px 8px 8px",
                                                                border: "1px solid #82afd9",
                                                            }}
                                                            className="h-100"
                                                        >
                                                            <Box className="row border-bottom px-1 pb-1">
                                                                <Box className="col-auto ">
                                                                    <Input
                                                                        id={currentScheme?.number}
                                                                        value={currentScheme?.number}
                                                                        type="checkbox"
                                                                        // disabled={period === "1"}
                                                                        onChange={(e: any) => {
                                                                            // console.log(e.target.value);
                                                                            if (e.target.checked) {
                                                                                // console.log(record,"record");
                                                                                let obj: any = {
                                                                                    basicid: currentScheme?.basicID,
                                                                                    foliono: currentScheme?.folioNo,
                                                                                    profileid: "",
                                                                                    schemeid: currentScheme?.schemeId,
                                                                                    newbasicid: "",
                                                                                    indexNo: currentScheme?.number
                                                                                }
                                                                                setChecked([...checked, obj])
                                                                            } else {
                                                                                let arr = checked.filter((item: any) => {

                                                                                    return item.indexNo !== String(e.target.value);
                                                                                });
                                                                                setChecked(arr);
                                                                            }
                                                                        }}
                                                                    />
                                                                </Box>
                                                            </Box>
                                                            <Box className="row mb-3 py-1 justify-content-between">
                                                                <Box className="col-auto">
                                                                    <Box className="row">
                                                                        <Box className="col-auto text-start my-auto">
                                                                            <img
                                                                                width={60}
                                                                                // height={auto}
                                                                                src={currentScheme?.fundImg}
                                                                                alt="logo"
                                                                            />

                                                                        </Box>
                                                                        <Box className="col-auto pe-0 me-0 ">
                                                                            <Text size="h5" weight="bold" >{currentScheme?.schemeName}</Text>
                                                                            <Text size="h6" weight="bold" css={{ fontSize: "0.70rem" }}>Folio no: {currentScheme?.folioNo}

                                                                            </Text>
                                                                            <Text className="my-1" css={{ fontSize: "0.60rem", color: "var(--colors-blue1)" }}>
                                                                                {currentScheme?.obj} - {currentScheme?.subObj}
                                                                            </Text>

                                                                        </Box>
                                                                    </Box>
                                                                </Box>

                                                                <Box className="col-auto text-end">
                                                                    <Box className={`badge ${currentScheme?.trxnSource === "External" ? style.externalColor : style.internalColor}`}>{currentScheme?.trxnSource} </Box>
                                                                </Box>
                                                            </Box>
                                                            <Box className="row justify-content-center my-1">
                                                                <Box className="col-auto col-lg-3 col-md-4">
                                                                    <Box className="row mx-1 px-0 mx-auto mb-2" css={{ borderRadius: "8px 8px 8px 8px" }}>{/*  backgroundColor: "#0b57a4", color: "#FFF", */}
                                                                        <Box className="col-12 ">
                                                                            <Text size="h6" className="text-center ">Net Invested</Text>
                                                                        </Box>
                                                                        <Box className="col-12 ">
                                                                            <Text size="h5" className="text-center " >&#x20B9;{Number(currentScheme?.investValue).toLocaleString("en-IN")}</Text>
                                                                        </Box>

                                                                    </Box>
                                                                </Box>
                                                                <Box className="col-auto col-lg-3 col-md-4">
                                                                    <Box className="row mx-1 px-0 mx-auto mb-2" css={{ borderRadius: "8px 8px 8px 8px" }}>{/*  backgroundColor: "#0b57a4", color: "#FFF", */}
                                                                        <Box className="col-12 ">
                                                                            <Text size="h6" className="text-center ">Gain</Text>
                                                                        </Box>
                                                                        <Box className="col-12 ">
                                                                            <Text size="h5" className={`text-center`} >&#x20B9;{Number(currentScheme?.currValue).toLocaleString("en-IN")}</Text>
                                                                        </Box>

                                                                    </Box>
                                                                </Box>
                                                                <Box className="col-auto col-lg-3 col-md-4">
                                                                    <Box className="row mx-1 px-0 mx-auto mb-2" css={{ borderRadius: "8px 8px 8px 8px" }}>{/*  backgroundColor: "#0b57a4", color: "#FFF", */}
                                                                        <Box className="col-12 ">
                                                                            <Text size="h6" className="text-center ">Current Amount</Text>
                                                                        </Box>
                                                                        <Box className="col-12 ">
                                                                            <Text size="h5" className="text-center " >&#x20B9;{Number(currentScheme?.currValue).toLocaleString("en-IN")}</Text>
                                                                        </Box>

                                                                    </Box>
                                                                </Box>
                                                                <Box className="col-auto col-lg-3 col-md-4">
                                                                    <Box className="row mx-1 px-0 mx-auto mb-2" css={{ borderRadius: "8px 8px 8px 8px" }}>{/*  backgroundColor: "#0b57a4", color: "#FFF", */}
                                                                        <Box className="col-12 ">
                                                                            <Text size="h6" className="text-center ">XIRR</Text>
                                                                        </Box>
                                                                        <Box className="col-12 ">
                                                                            <Text size="h5" className={`text-center ${(parseInt(currentScheme?.CAGR) <= 0) ? style.redFont : style.greenFont}`} >% {currentScheme?.CAGR}</Text>
                                                                        </Box>

                                                                    </Box>
                                                                </Box>
                                                            </Box>
                                                            <Box className="row justify-content-center">
                                                                <Box className="col-auto pt-1">
                                                                    <Box className="row mx-1 px-0 py-2 mx-auto mb-2" css={{ backgroundColor: "#0b57a4", color: "#FFF", borderRadius: "8px 8px 8px 8px" }}>{/*   */}
                                                                        <Box className="col-12 ">
                                                                            <Text size="h6" className="text-center text-white">Available Amount</Text>
                                                                        </Box>
                                                                        <Box className="col-12 ">
                                                                            <Text size="h5" className="text-center text-white" >&#x20B9;{Number(currentScheme?.AvlBalAmt).toLocaleString("en-IN")}</Text>
                                                                        </Box>
                                                                    </Box>
                                                                </Box>
                                                            </Box>
                                                        </Card>
                                                    </Box>
                                                );
                                            })}</> : <>
                                            <Box className="col-12 my-1 text-center">
                                                <Card
                                                    css={{
                                                        borderRadius: "8px 8px 8px 8px",
                                                        border: "1px solid #82afd9",
                                                    }}>
                                                    <Text>No UnMapped Investment Found</Text>
                                                </Card>
                                            </Box>
                                        </>}

                                    </Box>
                                </Box>
                            </>
                    }
                    <DialogModal
                        open={openModal}
                        setOpen={setOpenModal}
                        css={{
                            "@bp0": { width: "80%" },
                            "@bp1": { width: "40%" },
                        }}
                    >
                        <Box className="col-md-12 col-sm-12 col-lg-12">
                            <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                                {/* @ts-ignore */}
                                <Text css={{ color: "var(--colors-blue1)" }}>Map member</Text>
                            </Box>
                            <Box className="modal-body p-3">
                                {member !== null ? <>
                                    <Box className="row">
                                        <Box className="col">
                                            <SelectMenu
                                                items={member || []}
                                                bindValue="basicid"
                                                bindName="memberName"
                                                label={`Investor *`}

                                                placeholder={"Select"}
                                                value={openInvestor}
                                                onChange={(e) => {
                                                    setOpenInvestor(e?.basicid || "");
                                                    setSelectedMember(e || {});
                                                }
                                                }
                                            />
                                        </Box>
                                        <Box className="col">
                                            <SelectMenu
                                                items={investorAccountList || []}
                                                bindValue="profileID"
                                                bindName="InvestProfile"
                                                label="Investor A/C *"
                                                placeholder={"Select"}
                                                value={openInvestorAcc}
                                                onChange={(e) =>
                                                    setOpenInvestorAcc(e?.profileID || "")
                                                }
                                            />
                                        </Box>
                                    </Box>
                                    {selectMember?.CAF_Status === "success" ? <>
                                        <Box className=" row text-end p-1 justify-content-end">
                                            <Button
                                                color="yellow"
                                                disabled={openInvestor === ""}
                                                onClick={() => {
                                                    saveUnMappedInvestment()
                                                }}
                                            >
                                                Save
                                            </Button>
                                        </Box>
                                    </> :
                                        <>
                                            <Text>No profile found, To complete profile registration, click on 'Proceed' button.</Text>
                                            <Box className=" row text-end p-1 justify-content-end">
                                                <Button
                                                    color="yellow"
                                                    disabled={openInvestor === ""}
                                                    onClick={() => {
                                                        setRegiType("PENDING");
                                                        setBasicID(selectMember?.basicid);
                                                        router.push("/PROFILE");
                                                    }}
                                                >
                                                    Proceed
                                                </Button>
                                            </Box>
                                        </>}
                                </> : <>
                                    <Text>No profile found, To complete profile, click on 'Proceed' button.</Text>
                                    <Box className=" row text-end p-1 justify-content-end">
                                        <Button
                                            color="yellow"
                                            onClick={() => {
                                                setRegiType("PENDING");
                                                setBasicID(user?.basicid);
                                                router.push("/Profile");
                                            }}
                                        >
                                            Proceed
                                        </Button>
                                    </Box>
                                </>}
                            </Box>
                        </Box>
                    </DialogModal>
                    <DialogModal
                        open={openSuccess}
                        setOpen={setOpenSuccess}
                        css={{
                            "@bp0": { width: "60%" },
                            "@bp1": { width: "35%" },
                        }}
                    >
                        {/* @ts-ignore */}
                        <Box className="col-md-12 col-sm-12 col-lg-12">
                            <Box className="modal-body modal-body-scroll">
                                <Box className="row justify-content-center">
                                    <Box className="col-lg-12 text-center" css={{ py: 50 }}>
                                        <CheckCircleFill color="orange" size="2rem" />
                                        {/* @ts-ignore */}
                                        {/* <Text size="h5" className="text-center mt-2" css={{ color: "var(--colors-blue1)" }}>{messageTxt}</Text> */}
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                    </DialogModal>
                </Box>
            </Box>
        </>
    );
}
export default (UnMappedInvestPage);