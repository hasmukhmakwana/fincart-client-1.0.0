import React, { useState, useEffect } from 'react'
import Layout, { toastAlert } from 'App/shared/components/Layout'
import InsurancePage from './components/InsurancePage';
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import usePortfolioStore from "App/modules/DashboardModule/store";
import { getOverallPortfolioData } from "App/api/dashboard";
import { encryptData, getUser } from "App/utils/helpers";
import { getInvestorDD, getInvestorsNonMF } from 'App/api/UpcomingTransactions';
import Box from '@ui/Box/Box';
import SelectMenu from '@ui/Select/Select';
import Card from '@ui/Card';
const user: any = getUser();
const Insurance = () => {
    const [dataLoad, setDataLoad] = useState(false);
    const [dataType, setDataType] = useState<any>("active");
    const {
        insurancePortfolioData,
        setInsurancePortfolioData,
        setLoader,
    } = usePortfolioStore()

    const [investorId, setInvestorId] = useState<any>("");
    const [investorList, setInvestorList] = useState<any>([]);

    const fetchMemberList = async (id: any) => {
        try {
            setLoader(true);
            const enc: any = encryptData(id, true);
            let result = await getInvestorsNonMF(enc);
            setInvestorList(result?.data?.memberList);

            if (result?.data?.memberList?.length > 0)
                setInvestorId(result?.data?.memberList[0].basicid);
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    };

    const handleActiveCall = async (e: any) => {
        try {
            let isActive = "";
            if (e === "active") {
                isActive = "true";
            } else {
                isActive = "false";
            }
            setDataLoad(true);
            const obj: any = {
                basicID: investorId,
                assetType: "",
                isActive: isActive
            };
            if (investorId === "null" || investorId === undefined) {
                return;
            }
            const enc: any = encryptData(obj);
            const result: any = await getOverallPortfolioData(enc);

            for (let i = 0; i < result?.data?.productWisePortfolio.length; i++) {
                if (result?.data?.productWisePortfolio[i].investment_type === "OTHER INSURANCE") {
                    // console.log(result?.data?.productWisePortfolio[i].data[0]);
                    setInsurancePortfolioData(result?.data?.productWisePortfolio[i].data);
                }
            }
            setDataLoad(false);

        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setDataLoad(false);
            // setDataLoad(false);
        }
    };
    useEffect(() => {
        if (investorId === "null" || investorId === undefined) {
            return;
        }
        handleActiveCall("active");
    }, [investorId]);

    useEffect(() => {
        if (user?.basicid === "null" || user?.basicid === undefined) {
            return;
        }
        (async () => {
            await fetchMemberList(user?.basicid);
        })();
        return () => { }
    }, [user])

    return (
        <Layout>
            <PageHeader title="Insurance" rightContent={<></>} />
            <Card css={{ p: 15, mb: 20 }}>
                {/* <Box> */}
                <Box className="row justify-content-between mx-1">
                    <Box className="col col-auto d-flex">
                        <Box css={{ width: "200px" }}>
                            <SelectMenu
                                value={investorId}
                                items={investorList}
                                label="Investor"
                                bindValue={"basicid"}
                                bindName={"memberName"}
                                onChange={(e: any) => {
                                    setInvestorId(e?.basicid || "")
                                }
                                }
                            />
                        </Box>
                    </Box>
                </Box>
                {/* </Box> */}
                <InsurancePage load={dataLoad} id={investorId} dataType={dataType} setDataType={setDataType} />
            </Card>
        </Layout>
    )
}

export default Insurance