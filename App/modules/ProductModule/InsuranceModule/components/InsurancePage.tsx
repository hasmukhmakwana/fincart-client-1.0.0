import React, { useState } from 'react'
import Box from '@ui/Box/Box'
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import EyeIcon from 'App/icons/EyeIcon';
import DialogModal from "@ui/ModalDialog/ModalDialog";
import InsuranceViewModal from './InsuranceViewModal';
import styles from "../../Product.module.scss";
import style from "../../../DashboardModule/Dashboard.module.scss";
import usePortfolioStore from "App/modules/DashboardModule/store";
import UpArrowLong from 'App/icons/UpArrowLong';
import Loader from "App/shared/components/Loader";
import DownArrowLong from 'App/icons/DownArrowLong';
import { encryptData, getUser } from 'App/utils/helpers';
import { fetchPolicyDetailsChart } from 'App/api/dashboard';
import { toastAlert } from 'App/shared/components/Layout';
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from '@ui/Accordian/Accordian';
import DownArrow from 'App/icons/DownArrow';
import {
    TxnD,
    Nominee,
    More
} from "../../CommonModal"
import AllActiveInvestments from '../../AllActiveInvestments/AllActiveInvestments';
import { useRouter } from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';

const user: any = getUser();
const InsurancePage = ({ load, id, userGoalId, dataType, setDataType, isGoalWisePortfolio }: any) => {
    const router = useRouter();
    const { setContactUsVal } = useContactUsStore();
    const [loader, setLoader] = useState<any>(false);
    const [insuranceData, setInsuranceData] = useState<any>([]);
    const [openTxn, setOpenTxn] = useState(false);
    const [openNominee, setOpenNominee] = useState(false);
    const [basicId, setBasicId] = useState<any>("")
    const [policyNo, setPolicyNo] = useState<any>("")
    const [partnerId, setPartnerId] = useState<any>("")

    const fetchInc = async (currentRecord: any) => {
        if (currentRecord.length === 0) return "";
        let val = JSON.parse(currentRecord);
        setPartnerId(val?.partner_id);
        try {
            setLoader(true);
            let obj: any = {
                basicID: "0",
                productId: val?.id,
                investment_type: val?.investment_type_name,
                partnerId: val?.partner_id,
                isActive: false
            }

            const enc: any = encryptData(obj);
            let result: any = await fetchPolicyDetailsChart(enc);
            setInsuranceData(result?.data);
            setLoader(false);
        } catch (error) {
            setInsuranceData([]);
            toastAlert("error", error);
            setLoader(false);
        }
    }
    const {
        loading,
        insurancePortfolioData
    } = usePortfolioStore()
    return (
        <>
            {load ? (<>
                <Loader />
            </>) : (
                <>
                    {insurancePortfolioData.length === 0 ?
                        (isGoalWisePortfolio ? <>
                            <Card css={{ px: 15, py: 2, mb: 15 }} >
                                <Box>
                                    <Box className="container pt-4 ps-3 pb-3">
                                        <Box className={`pb-3 text-center text-sm-start ${style.sectionMainHeading}`}>No records
                                        </Box>
                                    </Box>
                                </Box>
                            </Card>
                        </> :
                            <>
                                <Card css={{ p: 15, mb: 20 }}>
                                    <Box>
                                        <Box className="container pt-5 ps-3 pb-4">
                                            <Box className={`pb-3 text-center text-sm-start ${style.sectionMainHeading}`}>Why Do We Need
                                                Insurance?</Box>
                                            <Box className="row d-flex justify-content-center text-center">
                                                <Box className="col-2">&nbsp;</Box>
                                                <Box className={`col-8 ${style.lightTextBlue} ${style.yellowBorderBottom} text-center`}> Insurance
                                                    policy is a contract used to indemnify
                                                    individuals and organizations for covered losses.
                                                    The second benefit of insurance is managing cash flow uncertainty.
                                                    Insurance provides payment for
                                                    covered losses when they occur.</Box>
                                                <Box className="col-2">&nbsp;</Box>
                                            </Box>
                                            <Box className={`${style.subHeading} text-center pt-3`}>Benefits Of Getting Insured</Box>
                                            <Box className="row mt-5 align-items-end align-sm-items-center">
                                                <Box className="d-none d-md-block col-md-1">&nbsp;</Box>
                                                <Box className="col-md-6 col-sm-12 d-flex align-self-center">
                                                    <img src="/insurance.png" alt="Insurance" className="img-fluid mx-auto" />
                                                </Box>
                                                <Box className="col-md-4 col-sm-12">
                                                    <Box className="row">
                                                        <Box
                                                            className="col-4 col-md-4 col-sm-4 pt-lg-0 pt-md-0 pt-sm-4 text-end">
                                                            <img src="/FinancialSecurity.png"
                                                                alt="Financial Security" />
                                                        </Box>
                                                        <Box
                                                            className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                                                            Financial Security</Box>
                                                    </Box>
                                                    <Box className="row">
                                                        <Box
                                                            className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-end ps-0 my-auto`}>
                                                            Secure Your
                                                            Child's Future</Box>
                                                        <Box
                                                            className="col-4 col-md-4 col-sm-4 pt-lg-0 pt-md-0 pt-sm-4 text-left">
                                                            <img src="/ChildSecurity.png" alt="Child Security" />
                                                        </Box>
                                                    </Box>
                                                    <Box className="row mt-4">
                                                        <Box className="col-4 col-md-4 col-sm-4 text-end">
                                                            <img src="/LoanBurden.png" alt="Loan Burden" />
                                                        </Box>
                                                        <Box
                                                            className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                                                            Prevent Loan
                                                            Burden</Box>

                                                    </Box>
                                                    <Box className="row mt-4">
                                                        <Box
                                                            className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-end my-auto`}>
                                                            Retirement
                                                            Planning</Box>
                                                        <Box className="col-4 col-md-4 col-sm-4 text-left">
                                                            <img src="/RetirementPlanning.png" alt="Retirement" />
                                                        </Box>
                                                    </Box>
                                                    <Box className="row mt-4">
                                                        <Box className="col-4 col-md-4 col-sm-4 text-end">
                                                            <img src="/Tax.png" alt="Tax Benefits" />
                                                        </Box>
                                                        <Box
                                                            className={`col-8 col-md-8 col-sm-8 ${style.pointerHeading} text-left my-auto`}>
                                                            Tax Benefits</Box>
                                                    </Box>
                                                </Box>
                                                <Box className="d-none d-md-block col-md-1">&nbsp;</Box>
                                            </Box>
                                            <Box className="m-4 text-end">
                                                <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow" onClick={() => {
                                                    setContactUsVal({
                                                        SubjectId: "6",
                                                        Description: ""
                                                    }
                                                    );
                                                   router.push('/Support/ContactUs');
                                                }}>
                                                    <GroupBox>
                                                        {/*@ts-ignore*/}
                                                        <Text size="h5">Contact Us</Text>
                                                    </GroupBox>
                                                </Button>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Card>
                            </>) : <>
                            <Box className="col-md-12 col-sm-12 col-lg-12 mt-2">
                                <Box background="default" css={{ p: 10, borderRadius: "8px" }}>
                                    {/* @ts-ignore */}
                                    <Box className="row">
                                        <AllActiveInvestments type={"insurance"} basicId={id} userGoalId={userGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={isGoalWisePortfolio} />
                                    </Box>
                                </Box>
                            </Box>
                            {loading ?
                                <Loader /> :
                                <Card className="mt-0" css={{ p: 15 }}>
                                    <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                                        {/* @ts-ignore */}
                                        <Accordion
                                            type="single"
                                            collapsible
                                            onValueChange={(e: any) => {
                                                fetchInc(e);
                                            }}
                                        >
                                            {insurancePortfolioData?.map((record, index) => {
                                                return (
                                                    <AccordionItem value={JSON.stringify(record)}>
                                                        <AccordionTrigger className="row p-0">
                                                            <Box className="col">
                                                                <Box className="tabtitle">
                                                                    <Text size="h4">{record?.partner_name}</Text>
                                                                </Box>
                                                                <Box className="row row-cols-1 row-cols-md-6 tabtitlecontent">
                                                                    <Box className="col">
                                                                        <Text size="h6">Cost Value</Text>
                                                                        <Text size="h6">
                                                                            {" "}
                                                                            &#x20B9;{" "}
                                                                            {Number(record?.investment).toLocaleString("en-IN")}
                                                                        </Text>
                                                                        {isGoalWisePortfolio && <>
                                                                            <Text size="h6" className="fw-bold">
                                                                                Allocated Value
                                                                            </Text>
                                                                            <Text size="h6">
                                                                                {" "}
                                                                                &#x20B9;{" "}
                                                                                {Number(record?.curr_goal_invested_amt).toLocaleString("en-IN")}
                                                                            </Text>
                                                                        </>
                                                                        }
                                                                    </Box>
                                                                    <Box className="col">
                                                                        <Text size="h6">Market Value</Text>
                                                                        <Text size="h6">
                                                                            {" "}
                                                                            &#x20B9;{" "}
                                                                            {Number(record?.current_value).toLocaleString(
                                                                                "en-IN"
                                                                            )}
                                                                        </Text>
                                                                        {isGoalWisePortfolio && <>
                                                                            <Text size="h6" className="fw-bold">
                                                                                Allocated Value
                                                                            </Text>
                                                                            <Text size="h6">
                                                                                {" "}
                                                                                &#x20B9;{" "}
                                                                                {Number(record?.curr_goal_current_amt).toLocaleString("en-IN")}
                                                                            </Text>
                                                                        </>
                                                                        }
                                                                    </Box>
                                                                    <Box className="col">
                                                                        <Text size="h6">Sum Assured</Text>
                                                                        <GroupBox position="left">
                                                                            {parseFloat(record?.netgain) >= 0 ? (
                                                                                <>
                                                                                    <Box>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text size='h6'>
                                                                                            {Number(record?.netgain).toLocaleString("en-IN")}
                                                                                        </Text>
                                                                                    </Box>
                                                                                    <Box className="gainUp">
                                                                                        <UpArrowLong size="10"></UpArrowLong>
                                                                                    </Box>
                                                                                </>
                                                                            ) : (
                                                                                <>
                                                                                    <Box>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text size='h6'>
                                                                                            {Number(record?.netgain).toLocaleString("en-IN")}
                                                                                        </Text>
                                                                                    </Box>
                                                                                    <Box className="lossDown">
                                                                                        <DownArrowLong size="10"></DownArrowLong>
                                                                                    </Box>
                                                                                </>
                                                                            )}
                                                                        </GroupBox>
                                                                    </Box>

                                                                </Box>
                                                            </Box>
                                                            <Box className="col col-auto action">
                                                                <DownArrow />
                                                            </Box>
                                                        </AccordionTrigger>

                                                        <AccordionContent className="border p-3">
                                                            {loader ? (
                                                                <Loader />
                                                            ) : (
                                                                <Box className="row">
                                                                    <Box className="col-md-12 col-sm-12 col-lg-12">
                                                                        {/* <Card> */}
                                                                        {insuranceData?.map((item: any, idx: any) => {
                                                                            return (
                                                                                <Box className="portfolioRow p-2">
                                                                                    <Box className="row">
                                                                                        <Box className="col col-12">
                                                                                            {/* @ts-ignore */}
                                                                                            <Text
                                                                                                weight="normal"
                                                                                                transform="uppercase"
                                                                                                size="h5"
                                                                                                color="blue1"
                                                                                                css={{ pl: 0, pr: 0, pt: 10, pb: 0 }}
                                                                                            >
                                                                                                {item?.policyName}
                                                                                            </Text>
                                                                                        </Box>
                                                                                        {/* @ts-ignore */}
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">Policy Number</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">{item?.policyNo}</Text>
                                                                                        </Box>
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">Client ID</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">{item?.clientID}</Text>
                                                                                        </Box>
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">Cost Value</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">
                                                                                                &#x20B9;{" "}
                                                                                                {Number(item?.investedAmt).toLocaleString(
                                                                                                    "en-IN"
                                                                                                )}
                                                                                            </Text>
                                                                                        </Box>
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">Market Value</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">
                                                                                                {" "}
                                                                                                &#x20B9;{" "}
                                                                                                {Number(item?.currentAmt).toLocaleString(
                                                                                                    "en-IN"
                                                                                                )}
                                                                                            </Text>
                                                                                        </Box>
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">Premium Amount</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">
                                                                                                {" "}
                                                                                                &#x20B9;{" "}
                                                                                                {Number(item?.premiumAmt).toLocaleString(
                                                                                                    "en-IN"
                                                                                                )}
                                                                                                {" / "}{item?.frequency}
                                                                                            </Text>
                                                                                        </Box>
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">Ins. Paid</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">{item?.total_premium_paid}{" / "}
                                                                                                {item?.ppt}</Text>
                                                                                        </Box>
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">Policy Term</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">{item?.policyTerm} years</Text>
                                                                                        </Box>
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">Due Date</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">{item?.nextDueDate}</Text>
                                                                                        </Box>
                                                                                        <Box className="col mb-3">
                                                                                            <Text size="h6" weight="bold">SI</Text>
                                                                                            {/* @ts-ignore */}
                                                                                            <Text weight="normal" size="h6">{item?.siEnabled}</Text>
                                                                                        </Box>
                                                                                    </Box>
                                                                                    {/* <Box> */}
                                                                                    {/* @ts-ignore */}
                                                                                    <Box className="row justify-content-end">
                                                                                        <Box className="col-auto">
                                                                                            <Box className="row justify-content-evenly mt-1 pe-3">
                                                                                                <Button
                                                                                                    className="col-auto mb-2 "
                                                                                                    color="yellowGroup"
                                                                                                    size="md"
                                                                                                    onClick={() => {
                                                                                                        setBasicId(item?.basic_Id)
                                                                                                        setOpenTxn(true);
                                                                                                        setPolicyNo(item?.policyNo);
                                                                                                    }}
                                                                                                >
                                                                                                    <GroupBox
                                                                                                        //@ts-ignore
                                                                                                        align="center"
                                                                                                        className={`${styles.hovBtn}`}
                                                                                                    >
                                                                                                        <Box className="row">
                                                                                                            {/* @ts-ignore */}
                                                                                                            <Box className="col-auto py-2 d-flex">
                                                                                                                {/* @ts-ignore */}
                                                                                                                <Text weight="normal" size="h6">
                                                                                                                    Transaction Details
                                                                                                                </Text>
                                                                                                            </Box>
                                                                                                        </Box>
                                                                                                    </GroupBox>
                                                                                                </Button>
                                                                                                <Button
                                                                                                    className="col-auto mb-2 "
                                                                                                    color="yellowGroup"
                                                                                                    size="md"
                                                                                                    onClick={() => {
                                                                                                        setBasicId(item?.basic_Id)
                                                                                                        setOpenNominee(true);
                                                                                                        setPolicyNo(item?.policyNo);
                                                                                                    }}
                                                                                                >
                                                                                                    <GroupBox
                                                                                                        //@ts-ignore
                                                                                                        align="center"
                                                                                                        className={`${styles.hovBtn}`}
                                                                                                    >
                                                                                                        <Box className="row">
                                                                                                            {/* @ts-ignore */}
                                                                                                            <Box className="col-auto py-2 d-flex">
                                                                                                                {/* @ts-ignore */}
                                                                                                                <Text weight="normal" size="h6">
                                                                                                                    Nominee Details
                                                                                                                </Text>
                                                                                                            </Box>
                                                                                                        </Box>
                                                                                                    </GroupBox>
                                                                                                </Button>
                                                                                            </Box>
                                                                                        </Box>
                                                                                    </Box>
                                                                                    {/* </Box> */}
                                                                                </Box>
                                                                            );
                                                                        })}
                                                                        {/* </Card> */}
                                                                    </Box>
                                                                </Box>
                                                            )}
                                                        </AccordionContent>
                                                    </AccordionItem>
                                                );
                                            })}
                                        </Accordion>
                                        <DialogModal
                                            open={openTxn}
                                            setOpen={setOpenTxn}
                                            css={{
                                                "@bp0": { width: "80%" },
                                                "@bp1": { width: "40%" },
                                            }}
                                        // className=""
                                        >
                                            <TxnD setOpen={setOpenTxn} id={basicId} polid={policyNo} partid={partnerId} type={"OTHER"} />
                                        </DialogModal>
                                        <DialogModal
                                            open={openNominee}
                                            setOpen={setOpenNominee}
                                            css={{
                                                "@bp0": { width: "80%" },
                                                "@bp1": { width: "40%" },
                                            }}
                                        // className=""
                                        >
                                            <Nominee setOpen={setOpenTxn} id={basicId} polid={policyNo} partid={partnerId} type={"OTHER"} />
                                        </DialogModal>
                                    </Box>
                                </Card>}
                        </>}
                </>)}
        </>
    )
}

export default InsurancePage
