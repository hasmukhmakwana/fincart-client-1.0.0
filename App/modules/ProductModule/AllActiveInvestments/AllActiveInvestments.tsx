import React, { useEffect, useState } from "react";
import Layout, { toastAlert } from "App/shared/components/Layout";
import Box from "@ui/Box/Box";
import { encryptData, getUser } from "App/utils/helpers";
import usePortfolioStore from "App/modules/DashboardModule/store";
import { getGoalWisePortfolioDetail, getOverallPortfolioData } from "App/api/dashboard";
import { RadioGroup } from "@radix-ui/react-radio-group";
import Radio from "@ui/Radio/Radio";
import SelectMenu from "@ui/Select/Select";
import { Label } from "@radix-ui/react-dropdown-menu";

const Category = [
  { id: "All", title: "All Categories" },
  { id: "Debt", title: "Debt" },
  { id: "Equity", title: "Equity" },
  { id: "Hybrid", title: "Hybrid" },
  { id: "Other", title: "Other" }
]

const assetTypeList = [
  { id: "ALL", name: 'Any Source' },
  { id: 'FIN', name: 'Fincart' },
  { id: 'EXT', name: 'External' }
]

const AllActiveInvestments = ({ assetType, setAssetType, selectCategory, setSelectCategory, type, basicId, dataType, userGoalId, setDataType, isGoalWisePortfolio }: any) => {
  const {
    loading,
    portfolioData,
    mutualFundPortfolioData,
    setOverallPortfolioData,
    setPortfolioData,
    setButtonTextList,
    setMutualFundPortfolioData,
    setUlipPortfolioData,
    setPmsPortfolioData,
    setInsurancePortfolioData,
    setEpfPortfolioData,
    setLiquiloanPortfolioData,
    setProtectionPortfolioData,
    setDigitalGoldPortfolioData,
    setGain_percentage,
    setLoader,
    setLastUpdatedDate,
    setIsFilter,
  } = usePortfolioStore();


  const [dataLoad, setDataLoad] = useState(false);

  const handleActiveCall = async (e: any, cat: any, asset: any) => {

    try {
      setDataLoad(true);

      if (cat === "All" && asset === "ALL") {
        setIsFilter(false);
      } else {
        setIsFilter(true);
      }

      let isActive: any;
      if (e === "active") {
        isActive = "true";
      } else {
        isActive = "false";
      }

      setLoader(true);
      const obj: any = {
        basicID: basicId,
        assetType: "",
        isActive: isActive,
      };
      if (basicId === "null" || basicId === undefined) {
        return;
      }
      if (type === "MF") {
        // if (cat !== "All" && asset !== "ALL") {
        obj["objective"] = cat === "All" ?
          "" : cat;
        obj['investmentSource'] = asset;
        // }
      }
      const enc: any = encryptData(obj);
      const result: any = await getOverallPortfolioData(enc);
      setLoader(false);

      setOverallPortfolioData(result?.data?.overall_Portfolio_object);

      setPortfolioData(result?.data?.asset_wise_portfolio);
      setButtonTextList(result?.data?.Button_text_list);

      for (let i = 0; i < result?.data?.productWisePortfolio.length; i++) {

        switch (result?.data?.productWisePortfolio[i].investment_type) {
          case "MUTUAL FUND":
            result?.data?.productWisePortfolio[i].data.sort((a: any, b: any) =>
              a.partner_name > b.partner_name ? 1 : -1
            );
            setMutualFundPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
          case "ULIP":
            setUlipPortfolioData(result?.data?.productWisePortfolio[i].data);
            break;
          case "PMS":
            setPmsPortfolioData(result?.data?.productWisePortfolio[i].data);
            break;
          case "LIQUILOAN":
            setLiquiloanPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
          case "OTHER INSURANCE":
            setInsurancePortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
          case "PROTECTION":
            setProtectionPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
          case "EPF":
            setEpfPortfolioData(result?.data?.productWisePortfolio[i].data);
            break;
          case "DIGITAL GOLD":
            setDigitalGoldPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
        }
      }
      setGain_percentage(result?.data?.Gain_percentage);
      setDataLoad(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
      setDataLoad(false);
    }
  };

  const handleActiveCallForGoal = async (e: any, cat: any, asset: any) => {
    try {
      setLoader(true);
      if (cat === "All" && asset === "ALL") {
        setIsFilter(false);

      } else {
        setIsFilter(true);
      }

      const obj: any = {
        usergoalId: userGoalId,
        isActive: dataType == "active" ? true : false,
      };

      if (type === "MF") {
        obj["objective"] = cat === "All" ?
          "" : cat;
        obj['investmentSource'] = asset;
      }

      const enc: any = encryptData(obj);

      const result: any = await getGoalWisePortfolioDetail(enc);

      setLoader(false);

      if (result != undefined) {
        setLastUpdatedDate(result?.data?.last_Updated_date);

        for (let i = 0; i < result?.data?.productWiseList.length; i++) {
          switch (result?.data?.productWiseList[i].investment_type) {
            case "MUTUAL FUND":
              result?.data?.productWiseList[i].data.sort((a: any, b: any) =>
                a.partner_name > b.partner_name ? 1 : -1
              );
              setMutualFundPortfolioData(
                result?.data?.productWiseList[i].data
              );
              break;
            case "ULIP":
              setUlipPortfolioData(result?.data?.productWiseList[i].data);
              break;
            case "PMS":
              setPmsPortfolioData(result?.data?.productWiseList[i].data);
              break;
            case "LIQUILOAN":
              setLiquiloanPortfolioData(
                result?.data?.productWiseList[i].data
              );
              break;
            case "OTHER INSURANCE":
              setInsurancePortfolioData(
                result?.data?.productWiseList[i].data
              );
              break;
            case "PROTECTION":
              setProtectionPortfolioData(
                result?.data?.productWiseList[i].data
              );
              break;
            case "EPF":
              setEpfPortfolioData(result?.data?.productWiseList[i].data);
              break;
            case "DIGITAL GOLD":
              setDigitalGoldPortfolioData(
                result?.data?.productWiseList[i].data
              );
              break;
          }
        }
      }
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  return (
    <Box className="col col-12 col-md-6 col-lg-6 border">
      <Box className="row">
        <Box className="col-auto">
          <RadioGroup
            defaultValue={dataType}
            className="inlineRadio"
            value={dataType}
            onValueChange={(e: any) => {
              if (isGoalWisePortfolio) {
                handleActiveCallForGoal(e, selectCategory, assetType);
              }
              else {
                handleActiveCall(e, selectCategory, assetType);
              }
              setDataType(e);
            }}
          >
            <Radio value="all" label="All" id="All" />
            <Radio
              value="active"
              label="Active Investments"
              id="activeInvestments"
            />
          </RadioGroup>
        </Box>
        {type === "MF" ? (<>
          <Box className="col-auto">
            <Box className="row" css={{ color: "black !important" }}>
              <Box className="col-auto">
                <SelectMenu
                  placeholder="Select a category"
                  name={"TicketCategory"}
                  value={selectCategory}
                  items={Category}
                  onChange={(e: any) => {
                    setSelectCategory(e?.id);
                    if (isGoalWisePortfolio) {
                      handleActiveCallForGoal(dataType, e?.id, assetType);
                    }
                    else {
                      handleActiveCall(dataType, e?.id, assetType);
                    }
                  }}
                  bindValue={"id"}
                  bindName={"title"}
                />
              </Box>
              <Box className="col-auto">
                <SelectMenu
                  name={"TicketCategory"}
                  value={assetType}
                  items={assetTypeList}
                  placeholder="Select an asset type"
                  onChange={(e: any) => {
                    setAssetType(e?.id);
                    if (isGoalWisePortfolio) {
                      handleActiveCallForGoal(dataType, selectCategory, e?.id);
                    }
                    else {
                      handleActiveCall(dataType, selectCategory, e.id)
                    }
                  }}
                  bindValue={"id"}
                  bindName={"name"}
                />
              </Box>
            </Box>
          </Box>
        </>) : null}
      </Box>
    </Box >
  );
};

export default AllActiveInvestments;
