import React, { useState } from 'react'
import Box from '@ui/Box/Box'
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import EyeIcon from 'App/icons/EyeIcon';
import DialogModal from "@ui/ModalDialog/ModalDialog";
import LiquiloanViewModal from "./LiquiloanViewModal";
import UpArrowLong from 'App/icons/UpArrowLong';
import usePortfolioStore from "App/modules/DashboardModule/store";
import styles from "../../Product.module.scss";
import style from "../../../DashboardModule/Dashboard.module.scss";
import Loader from "App/shared/components/Loader";
import DownArrowLong from 'App/icons/DownArrowLong';
import { toastAlert } from 'App/shared/components/Layout';
import { encryptData, getUser } from 'App/utils/helpers';
import { fetchPolicyDetailsChart } from 'App/api/dashboard';
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from '@ui/Accordian/Accordian';
import DownArrow from 'App/icons/DownArrow';
import {
    TxnD,
    Nominee,
    More
} from "../../CommonModal"
import AllActiveInvestments from '../../AllActiveInvestments/AllActiveInvestments';
import { useRouter } from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';
const user: any = getUser();
const LiquiloanPage = ({ load, id, userGoalId, dataType, setDataType, isGoalWisePortfolio }: any) => {
    const router = useRouter();
const { setContactUsVal } = useContactUsStore();
    const [loader, setLoader] = useState<any>(false);
    const [liquiLoanData, setLiquiLoanData] = useState<any>([]);
    const [openTxn, setOpenTxn] = useState(false);
    const [openNominee, setOpenNominee] = useState(false);
    const [basicId, setBasicId] = useState<any>("")
    const [policyNo, setPolicyNo] = useState<any>("")
    const [partnerId, setPartnerId] = useState<any>("")

    const fetchLl = async (currentRecord: any) => {
        if (currentRecord.length === 0) return "";

        let val = JSON.parse(currentRecord);
        setPartnerId(val?.partner_id);
        setPolicyNo(val?.id)

        console.log(val, "val");

        try {
            setLoader(true);
            let obj: any = {
                basicID: "0",
                productId: val?.id,
                investment_type: val?.investment_type_name,//"Liquiloans"
                partnerId: val?.partner_id,
                isActive: false
            }
            const enc: any = encryptData(obj);
            let result: any = await fetchPolicyDetailsChart(enc);
            console.log(result?.data, "result");
            setBasicId(result?.data?.[0]?.basic_Id)
            setLiquiLoanData(result?.data);
            setLoader(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    }

    const {
        loading,
        liquiloanPortfolioData
    } = usePortfolioStore()
    return (
        <>
            {load ? (<>
                <Loader />
            </>) : (<>
                {liquiloanPortfolioData.length === 0 ?
                    (isGoalWisePortfolio ? <>
                        <Card css={{ px: 15, py: 2, mb: 15 }} >
                            <Box>
                                <Box className="container pt-4 ps-3 pb-3">
                                    <Box className={`pb-3 text-center text-sm-start ${style.sectionMainHeading}`}>No records
                                    </Box>
                                </Box>
                            </Box>
                        </Card>
                    </> :
                        <>
                            <Card css={{ p: 15, mb: 20 }}>
                                <Box>
                                    <Box className="container pt-1 ps-3 pb-4">
                                        <Box className="row mt-5 align-items-end align-sm-items-center">
                                            <Box className="col-lg-8 col-md-6 col-sm-12 d-flex align-self-center">
                                                <img src="/Liquiloans.png" alt="Liquiloans" className="img-fluid mx-auto" />
                                            </Box>
                                            <Box className="col-lg-4 col-md-6 col-sm-12">
                                                <Box className="row">
                                                    <Box
                                                        className="col-4 col-md-4 col-sm-4 pt-lg-0 pt-md-0 pt-sm-4 text-end">
                                                        <img src="/Risk.png" alt="Low Risk" />
                                                    </Box>
                                                    <Box
                                                        className={`col-8 col-md-8 col-sm-8 text-left ps-0 my-auto ${style.pointerHeading}`}>
                                                        Consistent returns with low risk</Box>
                                                </Box>
                                                <Box className="row">

                                                    <Box
                                                        className={`col-8 col-md-8 col-sm-8 text-left ps-0 my-auto ${style.pointerHeading}`}>
                                                        No account opening fees</Box>
                                                    <Box
                                                        className="col-4 col-md-4 col-sm-4 pt-lg-0 pt-md-0 pt-sm-4 text-left">
                                                        <img src="/NoAccount.png" alt="No Fees" />
                                                    </Box>
                                                </Box>
                                                <Box className="row mt-4">
                                                    <Box className="col-4 col-md-4 col-sm-4 text-end">
                                                        <img src="/addMoney.png" alt="Add Money" />
                                                    </Box>
                                                    <Box
                                                        className={`col-8 col-md-8 col-sm-8 text-left my-auto ${style.pointerHeading}`}>
                                                        Effortlessly add & withdraw money</Box>

                                                </Box>
                                                <Box className="row mt-4">

                                                    <Box
                                                        className={`col-8 col-md-8 col-sm-8 text-left my-auto ${style.pointerHeading}`}>
                                                        Earn monthly interest</Box>
                                                    <Box className="col-4 col-md-4 col-sm-4 text-left">
                                                        <img src="/EarnInterest.png" alt="Earn Money" />
                                                    </Box>
                                                </Box>
                                                <Box className="row mt-4">
                                                    <Box className="col-4 col-md-4 col-sm-4 text-end">
                                                        <img src="/Invest.png" alt="Invest" />
                                                    </Box>
                                                    <Box
                                                        className={`col-8 col-md-8 col-sm-8 text-left my-auto ${style.pointerHeading}`}>
                                                        Invest lumpsum or via SIPs</Box>

                                                </Box>
                                            </Box>
                                            <Box className="row justify-content-md-center mt-4">
                                                <Box
                                                    className={`col-lg-5 col-md-8 col-sm-10  p-3 text-center ${style.rounded} ${style.lightBlueColorBg} ${style.lightTextBlue}`}>
                                                    {/* @ts-ignore */}
                                                    Earn up to <Text weight="bold">2X</Text> of fixed
                                                    deposits, Debt, Mutual Funds, etc.
                                                    By Lending to high CIBIL rated individuals
                                                </Box>
                                            </Box>

                                            <Box className={`pb-3 mt-4 ${style.sectionMainHeading}`}>How To Invest In LiquiLoans?</Box>
                                            <Box className="row justify-content-md-center mt-2">
                                                <Box
                                                    className={`col-lg-3 col-md-3 col-sm-12 p-3 ${style.rounded}  ${style.yellowBorderBox} ${style.myGoalsMainSubHeading}`}>
                                                    Step<br />
                                                    <label
                                                        className={`text-center ps-2 pe-2 mb-2 ${style.rounded} ${style.stepNumber} ${style.subHeading}`}>1</label><br />
                                                    Create account and submit your
                                                    KYC & bank account details
                                                </Box>

                                                <Box
                                                    className={`col-lg-3 col-md-3 col-sm-12  p-3 ms-lg-3 ms-md-3 ms-sm-0 me-lg-3 me-md-3 me-sm-0 ${style.rounded} ${style.yellowBorderBox} ${style.myGoalsMainSubHeading}`}>
                                                    Step<br />
                                                    <label
                                                        className={`text-center ps-2 pe-2 mb-2 ${style.rounded} ${style.stepNumber} ${style.subHeading}`}>2</label><br />
                                                    Sign Investor agreement and
                                                    begin an investment starting
                                                    at just Rs. 10,000
                                                </Box>
                                                <Box
                                                    className={`col-lg-3 col-md-3 col-sm-12 p-3 ${style.rounded}  ${style.yellowBorderBox} ${style.myGoalsMainSubHeading}`}>
                                                    Step<br />
                                                    <label
                                                        className={`text-center ps-2 pe-2 mb-2 ${style.rounded} ${style.stepNumber} ${style.subHeading}`}>3</label><br />
                                                    Sit back and watch your money
                                                    grow on Liquiloans dashboard
                                                </Box>
                                            </Box>

                                        </Box>
                                    </Box>
                                    <Box className="m-4 text-end">
                                        <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow" 
                                        onClick={() => { 
                                            setContactUsVal({
                                                SubjectId: "2",
                                                Description: ""
                                              }
                                              );
                                              router.push('/Support/ContactUs') }}>
                                            <GroupBox>
                                                {/* @ts-ignore */}
                                                <Text size="h5">Contact Us</Text>
                                            </GroupBox>
                                        </Button>
                                    </Box>
                                </Box>
                            </Card>
                        </>) : <>
                        <Box className="col-md-12 col-sm-12 col-lg-12 mt-2">
                            <Box background="default" css={{ p: 10, borderRadius: "8px" }}>
                                {/* @ts-ignore */}
                                <Box className="row">
                                    <AllActiveInvestments type={"liquiloan"} basicId={id} userGoalId={userGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={isGoalWisePortfolio} />
                                </Box>
                            </Box>
                        </Box>
                        {loading ?
                            <Loader /> :
                            <Card className="mt-0" css={{ p: 15 }}>
                                <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                                    {/* @ts-ignore */}
                                    <Accordion
                                        type="single"
                                        collapsible
                                        onValueChange={(e: any) => {
                                            fetchLl(e);
                                        }}
                                    >
                                        {liquiloanPortfolioData?.map((record, index) => {


                                            return (
                                                <AccordionItem value={JSON.stringify(record)}>
                                                    <AccordionTrigger className="row p-0">
                                                        <Box className="col">
                                                            <Box className="tabtitle">
                                                                <Text size="h4">{record?.partner_name}</Text>
                                                            </Box>
                                                            <Box className="row row-cols-1 row-cols-md-6 tabtitlecontent">
                                                                <Box className="col">
                                                                    <Text size="h6">Cost Value</Text>
                                                                    <Text size="h6">
                                                                        {" "}
                                                                        &#x20B9;{" "}
                                                                        {Number(record?.investment).toLocaleString("en-IN")}
                                                                    </Text>
                                                                    {isGoalWisePortfolio && <>
                                                                        <Text size="h6" className="fw-bold">
                                                                            Allocated Value
                                                                        </Text>
                                                                        <Text size="h6">
                                                                            {" "}
                                                                            &#x20B9;{" "}
                                                                            {Number(record?.curr_goal_invested_amt).toLocaleString("en-IN")}
                                                                        </Text>
                                                                    </>
                                                                    }
                                                                </Box>
                                                                <Box className="col">
                                                                    <Text size="h6">Market Value</Text>
                                                                    <Text size="h6">
                                                                        {" "}
                                                                        &#x20B9;{" "}
                                                                        {Number(record?.current_value).toLocaleString(
                                                                            "en-IN"
                                                                        )}
                                                                    </Text>
                                                                    {isGoalWisePortfolio && <>
                                                                        <Text size="h6" className="fw-bold">
                                                                            Allocated Value
                                                                        </Text>
                                                                        <Text size="h6">
                                                                            {" "}
                                                                            &#x20B9;{" "}
                                                                            {Number(record?.curr_goal_current_amt).toLocaleString("en-IN")}
                                                                        </Text>
                                                                    </>
                                                                    }
                                                                </Box>
                                                                <Box className="col">
                                                                    <Text size="h6">Gain/Loss</Text>
                                                                    <GroupBox position="left">
                                                                        {parseFloat(record?.netgain) >= 0 ? (
                                                                            <>
                                                                                <Box>
                                                                                    {/* @ts-ignore */}
                                                                                    <Text size='h6'>
                                                                                        {Number(record?.netgain).toLocaleString("en-IN")}
                                                                                    </Text>
                                                                                </Box>
                                                                                <Box className="gainUp">
                                                                                    <UpArrowLong size="10"></UpArrowLong>
                                                                                </Box>
                                                                            </>
                                                                        ) : (
                                                                            <>
                                                                                <Box>
                                                                                    {/* @ts-ignore */}
                                                                                    <Text size='h6'>
                                                                                        {Number(record?.netgain).toLocaleString("en-IN")}
                                                                                    </Text>
                                                                                </Box>
                                                                                <Box className="lossDown">
                                                                                    <DownArrowLong size="10"></DownArrowLong>
                                                                                </Box>
                                                                            </>
                                                                        )}
                                                                    </GroupBox>
                                                                </Box>
                                                                <Box className="col">
                                                                    <Text size="h6">XIRR</Text>
                                                                    <Text size="h6">
                                                                        {" "}
                                                                        {Number(record?.xirr).toLocaleString(
                                                                            "en-IN"
                                                                        )}%
                                                                    </Text>
                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                        <Box className="col col-auto action">
                                                            <DownArrow />
                                                        </Box>
                                                    </AccordionTrigger>

                                                    <AccordionContent className="border p-3">
                                                        {loader ? (
                                                            <Loader />
                                                        ) : (
                                                            <Box className="row">
                                                                <Box className="col-md-12 col-sm-12 col-lg-12">
                                                                    {/* <Card> */}
                                                                    <Box className="portfolioRow px-2">
                                                                        <Box className="row">
                                                                            <Box className="col col-12">
                                                                                <Box className="row justify-content-end pe-3">
                                                                                    <Button
                                                                                        className="col-auto mb-2"
                                                                                        color="yellowGroup"
                                                                                        size="md"
                                                                                        onClick={() => {
                                                                                            setOpenTxn(true);

                                                                                        }}  >
                                                                                        <GroupBox
                                                                                            //@ts-ignore
                                                                                            align="center"
                                                                                            className={`${styles.hovBtn}`}
                                                                                        >
                                                                                            <Box className="row">
                                                                                                {/* @ts-ignore */}
                                                                                                <Box className="col-auto py-2 d-flex">
                                                                                                    {/* @ts-ignore */}
                                                                                                    <Text weight="normal" size="h6">
                                                                                                        Transaction Details
                                                                                                    </Text>
                                                                                                </Box>
                                                                                            </Box>
                                                                                        </GroupBox>
                                                                                    </Button>
                                                                                    <Button
                                                                                        className="col-auto mb-2 "
                                                                                        color="yellowGroup"
                                                                                        size="md"
                                                                                        onClick={() => {
                                                                                            setOpenNominee(true);

                                                                                        }}  >
                                                                                        <GroupBox
                                                                                            //@ts-ignore
                                                                                            align="center"
                                                                                            className={`${styles.hovBtn}`}
                                                                                        >
                                                                                            <Box className="row">
                                                                                                {/* @ts-ignore */}
                                                                                                <Box className="col-auto py-2 d-flex">
                                                                                                    {/* @ts-ignore */}
                                                                                                    <Text weight="normal" size="h6">
                                                                                                        Nominee Details
                                                                                                    </Text>
                                                                                                </Box>
                                                                                            </Box>
                                                                                        </GroupBox>
                                                                                    </Button>

                                                                                </Box>
                                                                            </Box></Box></Box>
                                                                    {liquiLoanData?.map((item: any, idx: any) => {
                                                                        return (
                                                                            <Box className="portfolioRow p-2">
                                                                                <Box className="row">
                                                                                    <Box className="col col-12">
                                                                                        {/* @ts-ignore */}
                                                                                        <Text
                                                                                            weight="normal"
                                                                                            transform="uppercase"
                                                                                            size="h5"
                                                                                            color="blue1"
                                                                                            css={{ pl: 0, pr: 0, pt: 10, pb: 0 }}
                                                                                        >
                                                                                            {item?.policyName}
                                                                                        </Text>
                                                                                    </Box>

                                                                                    <Box className="col mb-3">
                                                                                        <Text size="h6" weight="bold">Client ID</Text>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text weight="normal" size="h6">{item?.clientID}</Text>
                                                                                    </Box>
                                                                                    <Box className="col mb-3">
                                                                                        <Text size="h6" weight="bold">Invested Value</Text>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text weight="normal" size="h6">
                                                                                            &#x20B9;{" "}
                                                                                            {Number(item?.investedAmt).toLocaleString(
                                                                                                "en-IN"
                                                                                            )}
                                                                                        </Text>
                                                                                    </Box>
                                                                                    <Box className="col mb-3">
                                                                                        <Text size="h6" weight="bold">Market Value</Text>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text weight="normal" size="h6">
                                                                                            {" "}
                                                                                            &#x20B9;{" "}
                                                                                            {Number(item?.currentAmt).toLocaleString(
                                                                                                "en-IN"
                                                                                            )}
                                                                                        </Text>
                                                                                    </Box>
                                                                                    <Box className="col mb-3">
                                                                                        <Text size="h6" weight="bold">Interest Paid</Text>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text weight="normal" size="h6">
                                                                                            &#x20B9;{" "}
                                                                                            {Number(item?.interestRepaid).toLocaleString(
                                                                                                "en-IN"
                                                                                            )}
                                                                                        </Text>
                                                                                    </Box>
                                                                                    <Box className="col mb-3">
                                                                                        <Text size="h6" weight="bold">Principal Repaid</Text>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text weight="normal" size="h6">
                                                                                            &#x20B9;{" "}
                                                                                            {Number(item?.principalRepaid).toLocaleString(
                                                                                                "en-IN"
                                                                                            )}
                                                                                        </Text>
                                                                                    </Box>
                                                                                    <Box className="col mb-3">
                                                                                        <Text size="h6" weight="bold">Total Repaid</Text>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text weight="normal" size="h6">
                                                                                            &#x20B9;{" "}
                                                                                            {Number(item?.totalRepaid).toLocaleString(
                                                                                                "en-IN"
                                                                                            )}
                                                                                        </Text>
                                                                                    </Box>
                                                                                    <Box className="col mb-3">
                                                                                        <Text size="h6" weight="bold">Start Date</Text>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text weight="normal" size="h6">
                                                                                            {item?.policyStartDate}
                                                                                        </Text>
                                                                                    </Box>
                                                                                    <Box className="col mb-3">
                                                                                        <Text size="h6" weight="bold">End Date</Text>
                                                                                        {/* @ts-ignore */}
                                                                                        <Text weight="normal" size="h6">
                                                                                            {item?.policyEndDate}
                                                                                        </Text>
                                                                                    </Box>
                                                                                </Box>

                                                                            </Box>
                                                                        );
                                                                    })}
                                                                    {/* </Card> */}
                                                                </Box>
                                                            </Box>
                                                        )}
                                                    </AccordionContent>
                                                </AccordionItem>
                                            );
                                        })}
                                    </Accordion>
                                    <DialogModal
                                        open={openTxn}
                                        setOpen={setOpenTxn}
                                        css={{
                                            "@bp0": { width: "80%" },
                                            "@bp1": { width: "40%" },
                                        }}
                                    // className=""
                                    >
                                        <TxnD setOpen={setOpenTxn} id={basicId} polid={policyNo} partid={partnerId} type={"LIQUILOAN"} />
                                    </DialogModal>
                                    <DialogModal
                                        open={openNominee}
                                        setOpen={setOpenNominee}
                                        css={{
                                            "@bp0": { width: "80%" },
                                            "@bp1": { width: "40%" },
                                        }}
                                    // className=""
                                    >
                                        <Nominee setOpen={setOpenTxn} id={basicId} polid={policyNo} partid={partnerId} type={"LIQUILOAN"} />
                                    </DialogModal>

                                </Box>
                            </Card>}
                    </>}
            </>)}
        </>
    )
}

export default LiquiloanPage