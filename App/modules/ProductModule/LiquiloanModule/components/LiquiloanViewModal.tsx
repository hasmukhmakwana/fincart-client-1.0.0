import React from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import CheckCircleFill from "App/icons/CheckCircleFill";
import { GroupBox } from "@ui/Group/Group.styles";
import Loader from "App/shared/components/Loader";
type SaveTypes = {
  modalData: (value: any) => void;
  load: (value: any) => boolean;
};
const LiquiloanViewModal = ({ modalData, load }: SaveTypes) => {
  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text weight="bold" css={{ color: "var(--colors-blue1)" }}> View Details</Text>
        </Box>
        <Box className="modal-body modal-body-scroll p-3">
          {/* @ts-ignore */}
          {load ? <><Box className="py-1 my-1"><Loader /></Box></> : <>
            {/* @ts-ignore */}

            {modalData.length === 0 ? <> <Box className="text-start">No record Found</Box></> : <>
              {/* @ts-ignore */}
              {modalData?.map((item: any) => {
                return (<>
                  <Box className="text-start">
                    {/* @ts-ignore */}
                    <Text css={{ mb: 10 }} color="mediumBlue" className="my-1 px-1">
                      {item?.policyName}{" "}[{item?.policyStatus}]
                    </Text><hr className="p-0 m-0 mb-2" />

                    <Box className="row px-1 pb-2">
                      <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                        {/* @ts-ignore */}
                        <Text >Client ID</Text>
                        {/* @ts-ignore */}
                        <Text >{item?.clientID}</Text>
                      </Box>
                      <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                        {/* @ts-ignore */}
                        <Text >Invested Value</Text>
                        {/* @ts-ignore */}
                        <Text >{Number(Math.round(item?.investedAmt)).toLocaleString("en-IN")}</Text>
                      </Box>
                      <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                        {/* @ts-ignore */}
                        <Text >Market Value</Text>
                        {/* @ts-ignore */}
                        <Text >{Number(Math.round(item?.currentAmt)).toLocaleString("en-IN")}</Text>
                      </Box>
                      <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                        {/* @ts-ignore */}
                        <Text >Interest Repaid</Text>
                        {/* @ts-ignore */}
                        <Text >{Number(Math.round(item?.interestRepaid)).toLocaleString("en-IN")}</Text>
                      </Box>
                      <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                        {/* @ts-ignore */}
                        <Text >Principle Repaid</Text>
                        {/* @ts-ignore */}
                        <Text >{Number(Math.round(item?.principalRepaid)).toLocaleString("en-IN")}</Text>
                      </Box>
                      <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                        {/* @ts-ignore */}
                        <Text >Total & Repaid</Text>
                        {/* @ts-ignore */}
                        <Text >{Number(Math.round(item?.totalRepaid)).toLocaleString("en-IN")}</Text>
                      </Box>
                      <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                        {/* @ts-ignore */}
                        <Text >Inv. Date</Text>
                        {/* @ts-ignore */}
                        <Text >{item?.policyStartDate}</Text>
                      </Box>
                      <Box className="col-5 col-lg-auto col-md-3 mx-2 mt-sm-1 mt-1 text-wrap">
                        {/* @ts-ignore */}
                        <Text >End & Date</Text>
                        {/* @ts-ignore */}
                        <Text >{item?.policyEndDate}</Text>
                      </Box>
                    </Box>
                  </Box></>);
              })}
            </>}</>}
        </Box>
      </Box>
    </>
  );
};

export default LiquiloanViewModal;
