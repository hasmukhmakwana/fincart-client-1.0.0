import React, { useState, useEffect } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import StarFill from "App/icons/StarFill";
import SelectMenu from "@ui/Select/Select";
import { GroupBox } from "@ui/Group/Group.styles";
import Radio from "@ui/Radio/Radio";
import { RadioGroup } from "@ui/Radio/";
import Input from "@ui/Input";
import style from "../../../Product.module.scss";
import ArrowLeftRight from 'App/icons/ArrowLeftRight';
import { Button } from '@ui/Button/Button';
import CartFill from 'App/icons/CartFill';
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { encryptData, getUser } from "App/utils/helpers";
import { schemeValidDates, searchSchemes, getFundList } from "App/api/transactions";
import Layout, { toastAlert } from "App/shared/components/Layout";
import { sendOTP, validateOTP, investTransaction } from "App/api/transactions";
import { addingToSystematicCart } from "App/api/cart";
import Loader from 'App/shared/components/Loader';
import useHeaderStore from 'App/shared/components/Header/store';

export type VerifySTPTypes = {
    STPTransferTo: string;
    STPTransferSchemeName: string;
    STPYears: string;
    STPDate: string;
    STPAmount: string;
};

interface StoreTypes {
    verifySTP: VerifySTPTypes;
    setVerifySTP: (payload: VerifySTPTypes) => void;
}

const useSTPStore = create<StoreTypes>((set) => ({
    //* initial state
    verifySTP: {
        STPTransferTo: "",
        STPTransferSchemeName: "",
        STPYears: "",
        STPDate: "",
        STPAmount: "",
    },
    setVerifySTP: (payload) =>
        set((state) => ({
            ...state,
            verifySTP: payload,
        })),
}))

type SwitchEventTypes = {
    selectedData: (value: any) => any;
    setOpenSTP: (value: boolean) => void;
    setOpenSuccess: (value: boolean) => void;
    setMessageTxt: (value: string) => void;
};

const STPModal = ({ selectedData, setOpenSTP, setOpenSuccess, setMessageTxt }: SwitchEventTypes) => {
    const user: any = getUser();
    const [purchaseData, setPurchaseData] = useState<any>(selectedData);
    const { cartCount, setCartCount } = useHeaderStore();//@SN-Cart Count Changes
    const [loadingCart, setLoadingCart] = useState(false);

    const STPModalObj = {

        STPTransferTo: Yup.string()
            .required("Transfer To is required"),

        STPYears: Yup.string()
            .required("Months is required"),

        STPDate: Yup.string()
            .required("Date is required"),

        STPAmount: Yup.number()
            .required("Amount is required")
            .positive()
            .integer()
            .min(1, `Min is 1`)
            .max(purchaseData?.AvlBalAmt, `Max is ${Number(purchaseData?.AvlBalAmt).toLocaleString("en-IN")}`),
    }

    let validationSchema = Yup.object().shape(STPModalObj);
    const { verifySTP, setVerifySTP, loading } = useSTPStore((state: any) => state);
    const [showOtpPart, setShowOtpPart] = useState(false);
    const [schemeList, setSchemeList] = useState<any>([]);
    const [investStatus, setInvestStatus] = useState(false);
    const [minL, setMinL] = useState(false);
    const [category, setCategory] = useState<any>([]);
    const [stpDates, setStpDates] = useState<any>([]);
    const [selectedCategory, setSelectedCategory] = useState<any>("");
    let cat: { objName: any; }[] = [];

    const addToCart = async (val: any) => {
        if (val?.STPAmount === "") {
            toastAlert("error", "Amount should not be empty while adding to Cart");
        }
        else {
            try {
                setLoadingCart(true);
                console.log(user)
                let dt = new Date();
                let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId
                let mDate = new Date(val?.STPDate);
                let obj = {
                    basicid: user?.basicid,
                    CartData: [
                        {
                            "PurFolioNo": purchaseData?.folioNo,
                            "fromSchemeId": val?.STPTransferTo,
                            "sellFolioNo": purchaseData?.folioNo,
                            "toSchemeID": purchaseData?.schemeId,
                            "amount": val?.STPAmount,
                            "startDate": val?.STPDate,
                            "endDate": "",
                            "No_of_Installment": val?.STPYears,
                            "MDate": `${mDate.getDate()}`,
                            "Units": "0",
                            "bankId": purchaseData?.bankId,
                            "accountNo": purchaseData?.bankAccNo,
                            "goalCode": "FG13",
                            "trxnTypeId": "16",
                            "userGoalid": goalid,
                            "mandateID": purchaseData?.mandateId,
                            "trxnDate": `${dt.getFullYear()}-${dt.getMonth() < 9 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1)}-${dt.getDate()}`,
                            "profileId": purchaseData?.ProfileID,
                            "remark": "",
                            "bankName": purchaseData?.bankName,
                            "empCode": "",
                            "profileName": user?.name
                        }
                    ]
                }

                const enc: any = encryptData(obj);
                const result: any = await addingToSystematicCart(enc);
                setOpenSTP(false);
                setMessageTxt("STP of ₹" + val?.STPAmount + " is added to Cart Successfully")
                setOpenSuccess(true);
                let iCartCount = cartCount + 1;//@SN-Cart Count Changes
                setCartCount(1);
                setLoadingCart(false);
            }
            catch (error) {
                setLoadingCart(false);
                console.log(error);
                toastAlert("error", error);
            }
        }

    }

    const STPFun = async (e: any) => {
        if (showOtpPart === false || e?.otp === undefined || e?.otp === "") {
            setLoadingCart(true);
            let obj = {
                'userid': user?.userid,
                'mobile': user?.mobile,
                'name': user?.name,
                'txntype': "STP",
                'purscheme': e?.STPTransferSchemeName,
                'sellscheme': purchaseData?.schemeName,
                'rmname': user?.RmName,
                'amt': e?.STPAmount
            }
            try {
                const enc: any = encryptData(obj);
                const result: any = await sendOTP(enc);
                setLoadingCart(false);
                setShowOtpPart(true)
                toastAlert("success", "OTP Sent Successfully");
            }
            catch (error) {
                console.log(error);
                setLoadingCart(false);
                toastAlert("error", error);
            }
        }
        else if (showOtpPart === true) {
            try {
                setLoadingCart(true);
                let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId
                let dt = new Date();
                let mDate = new Date(e?.STPDate);
                console.log(mDate.getDate());
                const enc: any = encryptData(e?.otp, true);
                // console.log(enc);
                const result: any = await validateOTP(enc);
                if (result?.status === "Success") {
                    let obj1 = {
                        "id": '0',
                        "basicID": user?.basicid,
                        "ProfileID": purchaseData?.ProfileID,
                        "purSchemeId": e?.STPTransferTo,
                        "sellSchemeId": purchaseData?.schemeId,
                        "tranType": "STP",
                        "PurFolioNo": purchaseData?.folioNo,
                        "SellFolioNo": purchaseData?.folioNo,
                        "Amount": e?.STPAmount,
                        "No_of_Installment": e?.STPYears,
                        "MDate": `${mDate.getDate()}`,
                        "userGoalId": goalid,
                        "Units": "0",
                        "bankId": purchaseData?.bankId,
                        "mandateId": purchaseData?.mandateId,
                        "startDate": e?.STPDate
                    }
                    const enc1: any = encryptData(obj1);
                    const result2: any = await investTransaction(enc1);

                    setOpenSTP(false);
                    setMessageTxt("STP of ₹" + e?.STPAmount + " is registered from '" + purchaseData?.schemeName + "'. Amount will be switched to '" + e?.STPTransferSchemeName + " from " + (dt.getMonth() + 1) + " " + dt.getDate() + ", " + dt.getFullYear() + " to next 1 Month.")
                    setOpenSuccess(true);
                    setLoadingCart(false);
                }

            }
            catch (error) {
                console.log(error);
                setLoadingCart(false);
                toastAlert("error", error);
            }
        }
    };
    const yearInstallments = [
        { id: "1", name: "1 Month" },
        { id: "2", name: "2 Months" },
        { id: "3", name: "3 Months" },
        { id: "4", name: "4 Months" },
        { id: "5", name: "5 Months" },
        { id: "6", name: "6 Months" },
        { id: "7", name: "7 Months" },
        { id: "8", name: "8 Months" },
        { id: "9", name: "9 Months" },
        { id: "10", name: "10 Months" },
        { id: "11", name: "11 Months" },
        { id: "12", name: "12 Months" },
        { id: "13", name: "13 Months" },
        { id: "14", name: "14 Months" },
        { id: "15", name: "15 Months" },
        { id: "16", name: "16 Months" },
        { id: "17", name: "17 Months" },
        { id: "18", name: "18 Months" },
        { id: "19", name: "19 Months" },
        { id: "20", name: "20 Months" },
        { id: "21", name: "21 Months" },
        { id: "22", name: "22 Months" },
        { id: "23", name: "23 Months" },
        { id: "24", name: "24 Months" },
    ];
    const fetchSchemeValidDates = async () => {
        try {
            const obj: any = {
                schemeId: purchaseData?.schemeId,
                mandateStatus: "Y",
                trxnType: "STP",
            };
            const enc: any = encryptData(obj);
            // console.log(enc);
            const result: any = await schemeValidDates(enc);
            console.log(result?.data)
            setStpDates(result?.data)
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
        }
    }

    const calMonthWise = (month: any, amount: any) => {

        if (purchaseData?.currValue >= (parseInt(month) * parseInt(amount))) {
            return (true);
        } else {
            toastAlert("warn",
                `Max Sum Of Investment For ${month} Months Must Be Less Than Or
             Equal To Avail Amt. i.e(₹${purchaseData?.currValue}), 
             But Your Limit Exceeds i.e(${month} x ₹${parseInt(amount)} = ₹${parseInt(month) * parseInt(amount)})`);
            return (false);
        }
    }

    const fetchFundList = async () => {
        try {
            let goalid = purchaseData?.goalList[0]?.userGoalId === undefined ? "0" : purchaseData?.goalList[0]?.userGoalId
            const enc1: any = encryptData(goalid, true);
            const result1: any = await getFundList(enc1);
            cat = result1?.data?.obj;
            setSelectedCategory(result1?.data?.obj[0]?.objName)
            setCategory(result1?.data?.obj);

        }
        catch (error) {
            console.log(error);
            toastAlert("error", error);
        }
    }

    const fetchSearchSchemes = async () => {
        setMinL(true);
        try {
            let goalid = purchaseData?.goalList[0]?.userGoalId === undefined ? "0" : purchaseData?.goalList[0]?.userGoalId
            const obj: any = {
                "fundid": purchaseData?.fundid,
                "obj": selectedCategory,
                "subObj": "",
                "schemeName": "",
                "userGoalId": goalid,
                "divOpt": "N",
                "clMnrStar": 'N'
            };
            const enc: any = encryptData(obj);
            const result: any = await searchSchemes(enc);
            setVerifySTP({
                ...verifySTP,
                STPTransferTo: result?.data[0]?.schemeId,
                STPTransferSchemeName: result?.data[0]?.schemeName,
            })
            setSchemeList(result?.data)
            setMinL(false);
        } catch (error) {
            setMinL(false);
            console.log(error);
            toastAlert("error", error);
        }
    }
    useEffect(() => {
        if (selectedCategory === "")
            return; 
        fetchSearchSchemes();

    }, [selectedCategory])
    useEffect(() => {
        fetchFundList();
        fetchSchemeValidDates();

        return () => {
            setVerifySTP({
                STPTransferTo: "",
                STPTransferSchemeName: "",
                STPYears: "",
                STPDate: "",
                STPAmount: "",
            })
        }
    }, [])
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}> STP Details</Text>
                </Box>
                <Box className="modal-body p-3">
                    <Formik
                        initialValues={verifySTP}
                        validationSchema={validationSchema}
                        enableReinitialize
                        onSubmit={(values, { resetForm }) => {
                            if (calMonthWise(values?.STPYears, values?.STPAmount)) {
                                if (investStatus) {
                                    STPFun(values);
                                } else {
                                    addToCart(values)
                                }
                            } else {
                                setVerifySTP({
                                    ...values,
                                    STPAmount: "",
                                })
                                resetForm();
                            }
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount
                        }) => (
                            <>
                                <Box className="row border-bottom border-warning mx-2">
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text className="text-capitalize" >{purchaseData?.schemeName} </Text>
                                            </Box>
                                            <Box className="col-auto pt-0" >
                                                {/* <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> 
                                                */}
                                                {
                                                    <>
                                                        {
                                                            (() => {
                                                                const arr = [];
                                                                for (let i = 0; i < purchaseData?.rating; i++) {
                                                                    arr.push(
                                                                        <StarFill color="orange" className="me-1" />
                                                                    );
                                                                }
                                                                return arr;
                                                            })()
                                                        }
                                                    </>
                                                }
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="col-12 py-2">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{purchaseData?.obj}</Text>
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{purchaseData?.subObj}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="row mt-2 mx-1">
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                <Text>Folio : <span>{purchaseData?.folioNo}</span></Text>
                                            </Box>
                                            <Box className="col-auto">
                                                <Text>Current Value : <span>{Number(purchaseData?.currValue).toLocaleString("en-IN")}</span></Text>
                                            </Box>
                                            <Box className="col-auto">
                                                <Text>Unit : <span>{purchaseData?.Units}</span></Text>
                                            </Box>
                                        </Box>
                                    </Box>

                                </Box>
                                {!showOtpPart ? <>

                                    <Box className="row justify-content-between">

                                        <Box className="col-auto">
                                            <Box>
                                                <GroupBox>
                                                    <RadioGroup className="inlineRadio" defaultValue="Debt" onValueChange={(e: any) => { setSelectedCategory(e); }}>
                                                        {category.map((el: any, i: number) => {
                                                            return (
                                                                <Radio value={el?.objName} label={el?.objName} id="all" key={i} />
                                                            )
                                                        })}

                                                    </RadioGroup>
                                                </GroupBox>
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="row mx-1">
                                        <Box className="col-11">
                                            <SelectMenu
                                                items={schemeList}
                                                bindValue={"schemeId"}
                                                bindName={"schemeName"}
                                                name="STPTransferTo"
                                                value={values?.STPTransferTo}
                                                label={"Transfer To"}
                                                placeholder={"Select"}
                                                onChange={(e: any) => {
                                                    setFieldValue("STPTransferTo", e.schemeId);
                                                    setFieldValue("STPTransferSchemeName", e.schemeName);
                                                }}
                                                required

                                                // @ts-ignore
                                                error={submitCount ? errors.STPTransferTo : null}
                                            />
                                        </Box>
                                        <Box className="col-auto">
                                            {minL ? <><Loader /></> : <></>}
                                        </Box>
                                    </Box>
                                    <Box className="row mx-1">
                                        <Box className="col-4">
                                            <Input
                                                label={`Enter Amount (Max.:${Number(purchaseData?.AvlBalAmt).toLocaleString("en-IN")})`}
                                                name="STPAmount"
                                                placeholder="Enter Amount"
                                                className="border border-2"
                                                // @ts-ignore
                                                error={submitCount ? errors.STPAmount : null}
                                                onChange={(e: any) => { setFieldValue("STPAmount", e?.target?.value.length === 1 && e?.target?.value === "0" ? "" : e?.target?.value.replace(/\D/g, '')) }}
                                                value={values?.STPAmount}
                                                autocomplete="off"
                                                required
                                            />
                                        </Box>
                                        <Box className="col-4">
                                            <SelectMenu
                                                name='STPDate'
                                                value={values.STPDate}
                                                items={stpDates}
                                                bindValue={"Date"}
                                                bindName={"Date"}
                                                label={"Date"}
                                                placeholder={"Select"}
                                                onChange={(e: any) => {
                                                    setFieldValue("STPDate", e.Date);
                                                }}
                                                required

                                                // @ts-ignore
                                                error={submitCount ? errors.STPDate : null}
                                            />
                                        </Box>
                                        <Box className="col-4">
                                            <SelectMenu
                                                items={yearInstallments}
                                                bindValue={"id"}
                                                bindName={"name"}
                                                value={values.STPYears}
                                                name="STPYears"
                                                label={"No of Months"}
                                                placeholder={"Select"}
                                                onChange={(e: any) => {
                                                    setFieldValue("STPYears", e.id);
                                                }}
                                                required
                                                // @ts-ignore
                                                error={submitCount ? errors.STPYears : null}
                                            />
                                        </Box>
                                    </Box>
                                </>
                                    :
                                    <>
                                        <Box className="row">
                                            <Box className="col-12">
                                                <Text css={{ color: "var(--colors-blue1)" }}>Transfer To</Text>
                                            </Box>
                                            <Box className="col-12">
                                                <SelectMenu
                                                    items={schemeList}
                                                    bindValue={"schemeId"}
                                                    bindName={"schemeName"}
                                                    name="STPTransferTo"
                                                    value={values?.STPTransferTo}// === "" ? schemeList[0]?.schemeId : values?.STPTransferTo
                                                    label={"Transfer To"}
                                                    placeholder={"Select"}
                                                    disabled
                                                    onChange={(e: any) => {
                                                        setFieldValue("STPTransferTo", e.schemeId);
                                                    }}

                                                />
                                            </Box>
                                        </Box>
                                        <Box className="row">
                                            <Box className="col-12">
                                                <Text css={{ color: "var(--colors-blue1)" }}>Amount</Text>
                                            </Box>
                                            <Box className="col-5">
                                                <Input
                                                    label=""
                                                    name="Value"
                                                    placeholder=""
                                                    disabled
                                                    defaultValue={values?.STPAmount}
                                                />
                                            </Box>
                                        </Box>
                                        <Box className="row">
                                            <Box className="col-12">
                                                <Text css={{ color: "var(--colors-blue1)" }}>Enter OTP</Text>
                                            </Box>
                                            <Box className="col-5">
                                                <Input
                                                    label=""
                                                    name="otp"
                                                    placeholder=""
                                                    defaultValue={values?.otp}
                                                    onChange={handleChange}
                                                />
                                            </Box>
                                        </Box>
                                    </>}
                                <Box className="row justify-content-end" >
                                    <Box className="col-auto px-0">

                                        {showOtpPart ? (<>
                                            <Button
                                                type="submit"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={() => {
                                                    setShowOtpPart(false);
                                                    setInvestStatus(false);
                                                }}>
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                    Back
                                                </Text>
                                            </Button>
                                        </>

                                        ) : (<>
                                            <Button
                                                type="button"
                                                css={{ backgroundColor: "orange" }}
                                                disabled={loadingCart}
                                                onClick={() => { setInvestStatus(false); handleSubmit() }}
                                            >
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                    {loadingCart ? <>Loading... </> : <><CartFill className="me-1" /> Add to Cart </>}
                                                </Text>
                                            </Button>
                                        </>)}
                                    </Box>
                                    <Box className="col-auto">
                                        <Button
                                            type="submit"
                                            css={{ backgroundColor: "orange" }}
                                            onClick={() => { setInvestStatus(true); handleSubmit() }}
                                            loading={loading}
                                            disabled={loadingCart}
                                        >
                                            {loadingCart ? <><Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>Loading... </Text></> : <>
                                                {showOtpPart ?
                                                    <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>Confirm </Text>
                                                    : <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                        <ArrowLeftRight className="me-2" color="white" /> Invest</Text>}
                                            </>}
                                        </Button>
                                    </Box>
                                </Box>
                            </>)}</Formik>
                </Box>
            </Box>
        </>
    )
}

export default (STPModal)