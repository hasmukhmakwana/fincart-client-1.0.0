import style from "../../../Product.module.scss";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import { GroupBox } from "@ui/Group/Group.styles";

const Goal = ({ setOpen, data }: any) => {
  return (
    <Box className="row">
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          <Box className="row">
            <Box className="col-auto text-light"><Text>Goal Linked</Text></Box>
            <Box className="col"></Box>
            <Box className="col-auto"></Box>
          </Box>
        </Box>
        <Box
          className="modal-body modal-body-scroll p-4 rounded-1"
          css={{ fontSize: "0.8rem" }}
        >
          <Box className="row row-cols-1 row-cols-md-3 justify-content-evenly">
            {data.map((item: any, index: any) => {
              return (
                <Box className="col mb-3">
                  <Card borderBlue css={{ p: 0 }}>
                    <Box className="row">
                      <Box className="col text-center">
                        <Box
                          className={style.iconBox}
                          display="inlineBlock"
                        >
                          <img
                            width={90}
                            height={80}
                            src={`${item?.goalImg || "/GoalFund.png"} `}
                            alt={item?.goalName || ""}
                          />
                          <Text size="h6" color="primary"> {item?.goalName || ""}</Text>
                        </Box>
                      </Box>
                    </Box>
                  </Card>
                </Box>
              )
            })}
          </Box>
          {/* <Box className="row justify-content-end">
            <Box className="col-auto">
              <Button
                className={`col-auto mb-2 `}
                color="yellowGroup"
                size="md"
                onClick={()=>setOpen(false)}
              >
                <GroupBox
                  align="center"
                >
                  <Box className="row">
                    <Box className="col-auto ms-1 p-2">
                      <Text size="h6" color="black" >
                        Close
                      </Text>
                    </Box>
                  </Box>
                </GroupBox>
              </Button>
            </Box>
          </Box> */}
        </Box>
      </Box>
    </Box>
  );
};
export default Goal;
