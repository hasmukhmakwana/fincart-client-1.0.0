import React, { useState, useEffect } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import SelectMenu from "@ui/Select/Select";
import Input from "@ui/Input";
import style from "../../../Product.module.scss";
import { Button } from '@ui/Button/Button';
import StarFill from "App/icons/StarFill";
import CartFill from 'App/icons/CartFill';
import Piggybankfill from 'App/icons/Piggybankfill';
import Layout, { toastAlert } from "App/shared/components/Layout";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { encryptData, getUser } from "App/utils/helpers";
import { schemeValidDates } from "App/api/transactions";
import { sendOTP, validateOTP, investTransaction } from "App/api/transactions";
import { addingToSystematicCart } from "App/api/cart";
import useHeaderStore from 'App/shared/components/Header/store';

export type VerifySwitchTypes = {
    SWPYears: string;
    SWPDate: string;
    SWPAmount: string;
};

interface StoreTypes {
    verifySwp: VerifySwitchTypes;
    setVerifySwp: (payload: VerifySwitchTypes) => void;
}

const useSWPStore = create<StoreTypes>((set) => ({
    //* initial state
    verifySwp: {
        SWPYears: "",
        SWPDate: "",
        SWPAmount: "",
    },
    setVerifySwp: (payload) =>
        set((state) => ({
            ...state,
            verifySwp: payload,
        })),
}))

type SwpEventTypes = {
    selectedData: (value: any) => any;
    setOpenSwp: (value: boolean) => void;
    setOpenSuccess: (value: boolean) => void;
    setMessageTxt: (value: string) => void;
};

const SwpModal = ({ selectedData, setOpenSwp, setOpenSuccess, setMessageTxt }: SwpEventTypes) => {
    const user: any = getUser();
    const { cartCount, setCartCount } = useHeaderStore();//@SN-Cart Count Changes
    const [purchaseData, setPurchaseData] = useState<any>(selectedData);
    const [btnStateOTP, setBtnStateOTP] = useState(false);
    const SWPModalObj = {

        SWPDate: Yup.string()
            .required("Date is required"),

        SWPYears: Yup.string()
            .required("Months is required"),

        SWPAmount: Yup.number()
            .required("Amount is required")
            .positive()
            .integer()
            .min(1, `Min is 1`)
            .max(purchaseData?.AvlBalAmt, `Max is ${Number(purchaseData?.AvlBalAmt).toLocaleString("en-IN")}`),
    }

    let validationSchema = Yup.object().shape(SWPModalObj);
    const { verifySwp, setVerifySwp, loading } = useSWPStore((state: any) => state);
    const [showOtpPart, setShowOtpPart] = useState(false);
    const [investStatus, setInvestStatus] = useState(false);
    const [swpDates, setSwpDates] = useState<any>([]);
    const [loadingCart, setLoadingCart] = useState(false);

    const addToCart = async (val: any) => {
        if (val?.SWPAmount === "") {
            toastAlert("error", "Amount should not be empty while adding to Cart");
        }
        else {
            try {
                setLoadingCart(true);
                let dt = new Date();
                let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId
                let mDate = new Date(val?.SWPDate);
                let obj = {
                    basicid: user?.basicid,
                    CartData: [
                        {
                            "PurFolioNo": "",
                            "fromSchemeId": "",
                            "sellFolioNo": purchaseData?.folioNo,
                            "toSchemeID": purchaseData?.schemeId,
                            "amount": val?.SWPAmount,
                            "startDate": val?.SWPDate,
                            "endDate": "",
                            "No_of_Installment": val?.SWPYears,
                            "MDate": `${mDate.getDate()}`,
                            "Units": "0",
                            "bankId": purchaseData?.bankId,
                            "accountNo": purchaseData?.bankAccNo,
                            "goalCode": "FG13",
                            "trxnTypeId": "17",
                            "userGoalid": goalid,
                            "mandateID": purchaseData?.mandateId,
                            "trxnDate": `${dt.getFullYear()}-${dt.getMonth() < 9 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1)}-${dt.getDate()}`,
                            "profileId": purchaseData?.ProfileID,
                            "remark": "",
                            "bankName": purchaseData?.bankName,
                            "empCode": "",
                            "profileName": user?.name
                        }
                    ]
                }
                const enc: any = encryptData(obj);
                const result: any = await addingToSystematicCart(enc);
                setOpenSwp(false);
                setMessageTxt("SWP of ₹" + val?.SWPAmount + " is added to Cart Successfully")
                setOpenSuccess(true);
                let iCartCount = cartCount + 1;//@SN-Cart Count Changes
                setCartCount(1);
                setLoadingCart(false);
            }
            catch (error) {
                setLoadingCart(false);
                console.log(error);
                toastAlert("error", error);
            }
        }
    }

    const SWPFun = async (e: any) => {

        if (showOtpPart === false || e?.otp === undefined || e?.otp === "") {
            let obj = {
                'userid': user?.userid,
                'mobile': user?.mobile,
                'name': user?.name,
                'txntype': "SWP",
                'purscheme': "",
                'sellscheme': purchaseData?.schemeName,
                'rmname': user?.RmName,
                'amt': e?.SWPAmount
            }

            try {
                const enc: any = encryptData(obj);

                const result: any = await sendOTP(enc);
                setShowOtpPart(true)
                toastAlert("success", "OTP Sent Successfully");
            }
            catch (error) {
                console.log(error);
                toastAlert("error", error);
            }
        }
        else if (showOtpPart === true) {
            setBtnStateOTP(true);
            try {
                let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId
                let dt = new Date();
                let mDate = new Date(e?.SWPDate);

                const enc: any = encryptData(e?.otp, true);
                // console.log(enc);
                const result: any = await validateOTP(enc);
                if (result?.status === "Success") {
                    let obj1 = {
                        "id": '0',
                        "basicID": user?.basicid,
                        "ProfileID": purchaseData?.ProfileID,
                        "purSchemeId": "",
                        "sellSchemeId": purchaseData?.schemeId,
                        "tranType": "SWP",
                        "PurFolioNo": "",
                        "SellFolioNo": purchaseData?.folioNo,
                        "Amount": e?.SWPAmount,
                        "No_of_Installment": e?.SWPYears,
                        "MDate": `${mDate.getDate()}`,
                        "userGoalId": goalid,
                        "Units": "0",
                        "bankId": purchaseData?.bankId,
                        "mandateId": purchaseData?.mandateId,
                        "startDate": e?.SWPDate
                    }
                    const enc1: any = encryptData(obj1);
                    const result2: any = await investTransaction(enc1);
                    setBtnStateOTP(false);
                    setOpenSwp(false);
                    setMessageTxt("SWP of ₹" + e?.SWPAmount + " is registered from '" + purchaseData?.schemeName + "'. Amount will be credited to (" + purchaseData?.bankName + "-" + purchaseData?.bankAccNo + ") from " + (dt.getMonth() + 1) + " " + dt.getDate() + ", " + dt.getFullYear() + " to next 1 Month.")
                    setOpenSuccess(true);
                }
                else {
                    toastAlert("error", result?.msg);
                }

            }
            catch (error) {
                console.log(error);
                setBtnStateOTP(false);
                toastAlert("error", error);
            }
        }
    };
    const yearInstallments = [
        { id: "1", name: "1 Month" },
        { id: "2", name: "2 Months" },
        { id: "3", name: "3 Months" },
        { id: "4", name: "4 Months" },
        { id: "5", name: "5 Months" },
        { id: "6", name: "6 Months" },
        { id: "7", name: "7 Months" },
        { id: "8", name: "8 Months" },
        { id: "9", name: "9 Months" },
        { id: "10", name: "10 Months" },
        { id: "11", name: "11 Months" },
        { id: "12", name: "12 Months" },
        { id: "13", name: "13 Months" },
        { id: "14", name: "14 Months" },
        { id: "15", name: "15 Months" },
        { id: "16", name: "16 Months" },
        { id: "17", name: "17 Months" },
        { id: "18", name: "18 Months" },
        { id: "19", name: "19 Months" },
        { id: "20", name: "20 Months" },
        { id: "21", name: "21 Months" },
        { id: "22", name: "22 Months" },
        { id: "23", name: "23 Months" },
        { id: "24", name: "24 Months" },
    ];
    const fetchSchemeValidDates = async () => {
        try {
            const obj: any = {
                schemeId: purchaseData?.schemeId,
                mandateStatus: "Y",
                trxnType: "SWP",
            };
            const enc: any = encryptData(obj);
            // console.log(enc);
            const result: any = await schemeValidDates(enc);
            setSwpDates(result?.data)
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
        }
    }

    const calMonthWise = (month: any, amount: any) => {
        if (purchaseData?.currValue >= (parseInt(month) * parseInt(amount))) {
            return (true);
        } else {
            toastAlert("warn",
                `Max Sum Of Investment For ${month} Months Must Be Less Than Or
             Equal To Avail Amt. i.e(₹${purchaseData?.currValue}), 
             But Your Limit Exceeds i.e(${month} x ₹${parseInt(amount)} = ₹${parseInt(month) * parseInt(amount)})`);
            return (false);
        }
    }

    useEffect(() => {
        fetchSchemeValidDates();
        return () => {
            setVerifySwp({
                SWPYears: "",
                SWPDate: "",
                SWPAmount: "",
            })
        }
    }, [])
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}> SWP Details</Text>
                </Box>
                <Box className="modal-body p-3">
                    <Formik
                        initialValues={verifySwp}
                        validationSchema={validationSchema}
                        enableReinitialize
                        onSubmit={(values, { resetForm }) => {
                            if (calMonthWise(values?.SWPYears, values?.SWPAmount)) {
                                if (investStatus) {
                                    SWPFun(values);
                                } else {
                                    addToCart(values)
                                }
                            } else {
                                setVerifySwp({
                                    ...values,
                                    SWPAmount: "",
                                })
                                resetForm();
                            }
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount,
                        }) => (
                            <>
                                <Box className="row border-bottom border-warning mx-2">
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text className="text-capitalize" >{purchaseData?.schemeName} </Text>
                                            </Box>
                                            <Box className="col-auto pt-0" >
                                                {/* <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> 
                                                */}
                                                {
                                                    <>
                                                        {
                                                            (() => {
                                                                const arr = [];
                                                                for (let i = 0; i < purchaseData?.rating; i++) {
                                                                    arr.push(
                                                                        <StarFill color="orange" className="me-1" />
                                                                    );
                                                                }
                                                                return arr;
                                                            })()
                                                        }
                                                    </>
                                                }
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="col-12 py-2">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{purchaseData?.obj}</Text>
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{purchaseData?.subObj}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="row mt-2 mx-1">
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                <Text>Folio : <span>{purchaseData?.folioNo}</span></Text>
                                            </Box>
                                            <Box className="col-auto">
                                                <Text>Current Value : <span>{Number(purchaseData?.currValue).toLocaleString("en-IN")}</span></Text>
                                            </Box>
                                            <Box className="col-auto">
                                                <Text>Unit : <span>{purchaseData?.Units}</span></Text>
                                            </Box>
                                        </Box>
                                    </Box>

                                </Box>
                                <Box className="row mx-2">
                                    {!showOtpPart ?
                                        <>
                                            <Box className="col-12">
                                                <Box className="row">
                                                    <Box className="col-6">
                                                        <Input
                                                            label={`Enter Amount (Max.:${Number(purchaseData?.AvlBalAmt).toLocaleString("en-IN")})`}
                                                            name="SWPAmount"
                                                            placeholder="Enter Amount"
                                                            className="border border-2"
                                                            // @ts-ignore
                                                            error={submitCount ? errors.SWPAmount : null}
                                                            onChange={(e: any) => { setFieldValue("SWPAmount", e?.target?.value.length === 1 && e?.target?.value === "0" ? "" : e?.target?.value.replace(/\D/g, '')) }}
                                                            value={values?.SWPAmount}
                                                            autocomplete="off"
                                                            required
                                                        />
                                                    </Box>
                                                    <Box className="col-6">

                                                        <SelectMenu
                                                            name='SWPDate'
                                                            value={values.SWPDate}
                                                            items={swpDates}
                                                            bindValue={"Date"}
                                                            bindName={"Date"}
                                                            label={"Date"}
                                                            placeholder={"Select"}
                                                            onChange={(e: any) => {
                                                                setFieldValue("SWPDate", e.Date);
                                                            }}
                                                            required

                                                            // @ts-ignore
                                                            error={submitCount ? errors.SWPDates : null}
                                                        />
                                                    </Box>
                                                </Box>
                                                <Box className="row">
                                                    <Box className="col-6">
                                                        <SelectMenu
                                                            name='SWPYears'
                                                            value={values.SWPYears}
                                                            items={yearInstallments}
                                                            bindValue={"id"}
                                                            bindName={"name"}
                                                            label={"No of Months"}
                                                            placeholder={"Select"}
                                                            onChange={(e: any) => {
                                                                setFieldValue("SWPYears", e.id);
                                                            }}
                                                            required
                                                            // @ts-ignore
                                                            error={submitCount ? errors.SWPYears : null}
                                                        />
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </>
                                        :
                                        <Box className="col-12">
                                            <Box className="row">
                                                <Box className="col-7">
                                                    <Input
                                                        label="Enter Amount"
                                                        name="SWPAmount"
                                                        value={values?.SWPAmount}
                                                        className="border border-2"
                                                    />
                                                </Box>
                                                <Box className="col-7">
                                                    <Input
                                                        label="Enter OTP"
                                                        name="otp"
                                                        onChange={handleChange}
                                                        value={values?.otp}
                                                        className="border border-2"
                                                    />
                                                </Box>
                                            </Box>
                                        </Box>
                                    }
                                </Box>
                                <Box className="row justify-content-end" >
                                    <Box className="col-auto px-0">
                                        {showOtpPart ? (<>
                                            <Button
                                                type="submit"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={() => {
                                                    setShowOtpPart(false);
                                                    setInvestStatus(false);
                                                }}
                                                disabled={btnStateOTP}>
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                    Back
                                                </Text>
                                            </Button>
                                        </>
                                        ) : (<>
                                            <Button
                                                type="button"
                                                css={{ backgroundColor: "orange" }}
                                                disabled={loadingCart}
                                                onClick={() => { setInvestStatus(false); handleSubmit() }}
                                            >
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                    {loadingCart ? <>Loading... </> : <><CartFill className="me-1" /> Add to Cart </>}
                                                </Text>
                                            </Button>
                                        </>)}
                                    </Box>
                                    <Box className="col-auto">
                                        {showOtpPart ? <Button
                                            type="submit"
                                            css={{ backgroundColor: "orange" }}
                                            onClick={() => { setInvestStatus(true); handleSubmit() }}
                                            loading={loading}
                                            disabled={btnStateOTP}
                                        >
                                            <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                {btnStateOTP ? <>Loading... </> : <>Confirm </>} </Text>
                                        </Button>
                                            :
                                            <Button
                                                type="submit"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={() => { setInvestStatus(true); handleSubmit() }}
                                                disabled={loadingCart}
                                                loading={loading}
                                            >

                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>
                                                    {loadingCart ? <>Loading... </> : <><Piggybankfill className="me-2" color="white" /> Invest </>}
                                                </Text>
                                            </Button>
                                        }
                                    </Box>
                                </Box>
                            </>
                        )}
                    </Formik>
                </Box>
            </Box>
        </>
    )
}

export default (SwpModal);
