import React, { useState, useEffect } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import StarFill from "App/icons/StarFill";
import SelectMenu from "@ui/Select/Select";
import Shuffle from 'App/icons/Shuffle';
import { GroupBox } from "@ui/Group/Group.styles";
import Radio from "@ui/Radio/Radio";
import { RadioGroup } from "@ui/Radio/";
import Input from "@ui/Input";
import style from "../../../Product.module.scss";
import { Button } from '@ui/Button/Button';
import CartFill from 'App/icons/CartFill';
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { encryptData, getUser } from "App/utils/helpers";
import { schemeValidDates, searchSchemes, getFundList } from "App/api/transactions";
import Layout, { toastAlert } from "App/shared/components/Layout";
import { sendOTP, validateOTP, investTransaction } from "App/api/transactions";
import { addingToSystematicCart } from "App/api/cart";
import Loader from 'App/shared/components/Loader';
import useHeaderStore from 'App/shared/components/Header/store';

export type VerifySwitchTypes = {
    SwitchTransferTo: string;
    SwitchAmountType: string;
    SwitchAmount: string;
    SwitchTransferSchemeName: string;
};

interface StoreTypes {
    verifySwitch: VerifySwitchTypes;
    setVerifySwitch: (payload: VerifySwitchTypes) => void;
}

const useSwitchStore = create<StoreTypes>((set) => ({
    //* initial state
    verifySwitch: {
        SwitchTransferTo: "",
        SwitchAmountType: "",
        SwitchAmount: "",
        SwitchTransferSchemeName: "",
    },
    setVerifySwitch: (payload) =>
        set((state) => ({
            ...state,
            verifySwitch: payload,
        })),
}))

type SwitchEventTypes = {
    selectedData: (value: any) => any;
    setOpenSwitch: (value: boolean) => void;
    setOpenSuccess: (value: boolean) => void;
    setMessageTxt: (value: string) => void;
};

const SwitchModal = ({ selectedData, setOpenSwitch, setOpenSuccess, setMessageTxt }: SwitchEventTypes) => {
    const user: any = getUser();
    const [purchaseData, setPurchaseData] = useState<any>(selectedData);
    const { cartCount, setCartCount } = useHeaderStore();//@SN-Cart Count Changes
    const SwitchModalObj = {
        SwitchTransferTo: Yup.string()
            .required("Transfer To is required"),

        SwitchAmount: Yup.number()
            .required("Amount is required")
            .positive()
            .integer()
            .min(1, `Min is 1`)
            .max(purchaseData?.AvlBalAmt, `Max is ${Number(purchaseData?.AvlBalAmt).toLocaleString("en-IN")}`),
    }

    let validationSchema = Yup.object().shape(SwitchModalObj);
    const { verifySwitch, setVerifySwitch, loading } = useSwitchStore((state: any) => state);
    const [showOtpPart, setShowOtpPart] = useState(false);
    const [schemeList, setSchemeList] = useState<any>([]);
    const [category, setCategory] = useState<any>([]);
    const [investStatus, setInvestStatus] = useState(false);
    const [minL, setMinL] = useState(false);
    const [selectedCategory, setSelectedCategory] = useState<any>("");
    const [loadingCart, setLoadingCart] = useState(false);

    let cat: { objName: any; }[] = [];

    const addToCart = async (val: any) => {
        if (val?.SwitchAmount === "") {
            toastAlert("error", "Amount should not be empty while adding to Cart");
        }
        else {
            try {
                setLoadingCart(true);
                let dt = new Date();
                let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId
                let obj = {
                    basicid: user?.basicid,
                    CartData: [
                        {
                            "PurFolioNo": purchaseData?.folioNo,
                            "fromSchemeId": val?.SwitchTransferTo,
                            "sellFolioNo": purchaseData?.folioNo,
                            "toSchemeID": purchaseData?.schemeId,
                            "amount": val?.SwitchAmount,
                            "startDate": "",
                            "endDate": "",
                            "No_of_Installment": "0",
                            "MDate": "",
                            "Units": "0",
                            "bankId": purchaseData?.bankId,
                            "accountNo": purchaseData?.bankAccNo,
                            "goalCode": "FG13",
                            "trxnTypeId": "14",
                            "userGoalid": goalid,
                            "mandateID": purchaseData?.mandateId,
                            "trxnDate": `${dt.getFullYear()}-${dt.getMonth() < 9 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1)}-${dt.getDate() < 10 ? '0' + (dt.getDate()) : dt.getDate()}`,
                            "profileId": purchaseData?.ProfileID,
                            "remark": "",
                            "bankName": purchaseData?.bankName,
                            "empCode": "",
                            "profileName": user?.name
                        }
                    ]
                }
                const enc: any = encryptData(obj);
                const result: any = await addingToSystematicCart(enc);
                setOpenSwitch(false);
                setMessageTxt("SWITCH of ₹" + val?.SwitchAmount + " is added to Cart Successfully")
                setOpenSuccess(true);
                let iCartCount = cartCount + 1;//@SN-Cart Count Changes
                setCartCount(1);
                setLoadingCart(false);
            }
            catch (error) {
                setLoadingCart(false);
                console.log(error);
                toastAlert("error", error);
            }
        }

    }
    const SwitchFun = async (e: any) => {

        if (showOtpPart === false || e?.otp === undefined || e?.otp === "") {
            setLoadingCart(true);
            let obj = {
                'userid': user?.userid,
                'mobile': user?.mobile,
                'name': user?.name,
                'txntype': "SWITCH",
                'purscheme': purchaseData?.schemeName,
                'sellscheme': e?.SwitchTransferSchemeName,
                'rmname': user?.RmName,
                'amt': e?.SwitchAmount
            }
            console.log(obj);
            try {
                const enc: any = encryptData(obj); 
                const result: any = await sendOTP(enc);
                setLoadingCart(false);
                setShowOtpPart(true)
                toastAlert("success", "OTP Sent Successfully");
            }
            catch (error) {
                console.log(error);
                setLoadingCart(false);
                toastAlert("error", error);
            }
        }
        else if (showOtpPart === true) {
            try {
                setLoadingCart(true);
                let goalid = purchaseData?.goalList[0] === undefined ? "0" : purchaseData?.goalList[0].userGoalId
                const enc: any = encryptData(e?.otp, true); 
                const result: any = await validateOTP(enc);
                if (result?.status === "Success") {
                    let obj1 = {
                        "id": '0',
                        "basicID": user?.basicid,
                        "ProfileID": purchaseData?.ProfileID,
                        "purSchemeId": e?.SwitchTransferTo,
                        "sellSchemeId": purchaseData?.schemeId,
                        "tranType": "SWITCH",
                        "PurFolioNo": purchaseData?.folioNo,
                        "SellFolioNo": purchaseData?.folioNo,
                        "Amount": e?.SwitchAmount,
                        "No_of_Installment": "0",
                        "MDate": "",
                        "userGoalId": goalid,
                        "Units": "0",
                        "bankId": "",
                        "mandateId": "",
                        "startDate": ""
                    }
                    const enc1: any = encryptData(obj1);
                    const result2: any = await investTransaction(enc1);

                    setOpenSwitch(false);
                    setMessageTxt("Switch of ₹" + e?.SwitchAmount + " is registered from '" + purchaseData?.schemeName + "'. Amount will be switched to '" + e?.SwitchTransferSchemeName + "' within 3 working days.")
                    setOpenSuccess(true);
                    setLoadingCart(false);
                }

            }
            catch (error) {
                console.log(error);
                setLoadingCart(false);
                toastAlert("error", error);
            }
        }
    };

    const fetchSearchSchemes = async () => {
        try {
            setMinL(true);
            let goalid = purchaseData?.goalList[0]?.userGoalId === undefined ? "0" : purchaseData?.goalList[0]?.userGoalId
            const obj: any = {
                "fundid": purchaseData?.fundid,
                "obj": selectedCategory,
                "subObj": "",
                "schemeName": "",
                "userGoalId": goalid,
                "divOpt": "N",
                "clMnrStar":'N'
            };
            const enc: any = encryptData(obj);
            const result: any = await searchSchemes(enc);
            setSchemeList(result?.data)
            setVerifySwitch({
                            ...verifySwitch,
                            SwitchTransferTo: result?.data[0]?.schemeId,
                            SwitchTransferSchemeName: result?.data[0]?.schemeName,
                        })
                        setMinL(false);
        } catch (error) {
            console.log(error);
            setMinL(false);
            toastAlert("error", error);
        }
    }
    useEffect(() => { 
        if (selectedCategory === "")
            return;
        
        fetchSearchSchemes();
    }, [selectedCategory])
    const fetchFundList= async()=>{
        try{
            let goalid = purchaseData?.goalList[0]?.userGoalId === undefined ? "0" : purchaseData?.goalList[0]?.userGoalId
            const enc1: any = encryptData(goalid, true);
            const result1: any = await getFundList(enc1);
            cat = result1?.data?.obj;
            setSelectedCategory(result1?.data?.obj[0]?.objName)
            setCategory(result1?.data?.obj);
           
        }
        catch(error){
            console.log(error);
            toastAlert("error", error);
        }
    }

    useEffect(() => {
        fetchFundList();
        
        return () => {
            setVerifySwitch({
                SwitchTransferTo: "",
                SwitchAmountType: "",
                SwitchAmount: "",
                SwitchTransferSchemeName: "",
            })
        }
    }, [])

    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}> Switch Details</Text>
                </Box>
                <Box className="modal-body p-3">
                    <Formik
                        initialValues={verifySwitch}
                        validationSchema={validationSchema}
                        enableReinitialize
                        onSubmit={(values) => {
                            if (investStatus) {
                                console.log(values, "SwitchFun");
                                SwitchFun(values);
                            } else {
                                console.log(values, "addToCart");
                                addToCart(values)
                            }
                            // SwitchFun(values);
                        }}
                    >
                        {({
                            values,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            errors,
                            touched,
                            setFieldValue,
                            submitCount,
                        }) => (
                            <>
                                <Box className="row border-bottom border-warning mx-2">
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text className="text-capitalize" >{purchaseData?.schemeName} </Text>
                                            </Box>
                                            <Box className="col-auto pt-0" >
                                                {/* <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> <StarFill color="var(--colors-yellow)" /> 
                                                */}
                                                     {
                                  <>
                                    {
                                      (() => {
                                        const arr = [];
                                        for (let i = 0; i < purchaseData?.rating; i++) {
                                          arr.push(
                                            <StarFill color="orange" className="me-1" />
                                          );
                                        }
                                        return arr;
                                      })()
                                    }
                                  </>
                                }
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="col-12 py-2">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >{purchaseData?.obj}</Text>
                                                {/* @ts-ignore */}
                                                <Text size="h6" className={`badge border rounded-pill ms-1 ${style.titleSubpill}`}>{purchaseData?.subObj}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="row mt-2 mx-1">
                                    <Box className="col-12">
                                        <Box className="row">
                                            <Box className="col-auto">
                                                <Text>Folio : <span>{purchaseData?.folioNo}</span></Text>
                                            </Box>
                                            <Box className="col-auto">
                                                <Text>Current Value : <span>{Number(purchaseData?.currValue).toLocaleString("en-IN")}</span></Text>
                                            </Box>
                                            <Box className="col-auto">
                                                <Text>Unit : <span>{purchaseData?.Units}</span></Text>
                                            </Box>
                                        </Box>
                                    </Box>

                                </Box>
                                {!showOtpPart ? <>

                                    <Box className="row justify-content-between">

                                        <Box className="col-auto mx-2">
                                            <Box>
                                                <GroupBox>
                                                    <RadioGroup className="inlineRadio" defaultValue="Debt" onValueChange={(e: any) => { console.log(e); console.log(selectedData); setSelectedCategory(e); }}>
                                                        {category.map((el: any, i: number) => {
                                                            return (
                                                                <Radio value={el?.objName} label={el?.objName} id="all" key={i} />
                                                            )
                                                        })}
                                                    </RadioGroup>
                                                </GroupBox>
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="row mx-2">
                                        <Box className="col-11">
                                            <SelectMenu
                                                items={schemeList}
                                                bindValue={"schemeId"}
                                                bindName={"schemeName"}
                                                placeholder={"Select"}
                                                value={values?.SwitchTransferTo === "" ? schemeList[0]?.schemeId : values?.SwitchTransferTo}
                                                label={"Transfer To"}
                                                onChange={(e: any) => {

                                                    setFieldValue("SwitchTransferTo", e.schemeId);
                                                    setFieldValue("SwitchTransferSchemeName", e.schemeName);
                                                }}
                                                required
                                                // @ts-ignore
                                                error={submitCount ? errors.SwitchTransferTo : null}
                                            />
                                        </Box>
                                        <Box className="col-auto">
                                            {minL ? <><Loader /></> : <></>}
                                        </Box>
                                    </Box>
                                    <Box className="row mx-1">
                                        <Box className="col-12">
                                            <GroupBox>
                                                <RadioGroup className="inlineRadio" defaultValue="Partial"
                                                    name="SwitchAmountType"
                                                    onValueChange={(e: any) => {
                                                        setFieldValue("SwitchAmountType", e);
                                                    }}>
                                                    <Radio value="Partial" label="Partial" id="Partial" />
                                                    <Radio value="Full" label="Full" id="Full" />
                                                </RadioGroup>
                                            </GroupBox>
                                        </Box>
                                        <Box className="col-auto">
                                            <Input
                                                label={`Enter Amount (Max.:${Number(purchaseData?.AvlBalAmt).toLocaleString("en-IN")})`}
                                                name="SwitchAmount"
                                                placeholder="Enter Amount"
                                                className="border border-2"
                                                // @ts-ignore
                                                error={submitCount ? errors.SwitchAmount : null}
                                                onChange={(e: any) => { setFieldValue("SwitchAmount", e?.target?.value.length === 1 && e?.target?.value === "0" ? "" : e?.target?.value.replace(/\D/g, '')) }}
                                                value={values?.SwitchAmount}
                                                autocomplete="off"
                                                required
                                            />
                                        </Box>
                                    </Box>
                                </>
                                    :
                                    <>
                                        <Box className="row mx-2">
                                            <Box className="col-12">
                                                <Text css={{ color: "var(--colors-blue1)" }}>Transfer To</Text>
                                            </Box>
                                            <Box className="col-12">
                                                <SelectMenu
                                                    items={schemeList}
                                                    bindValue={"schemeId"}
                                                    bindName={"schemeName"}
                                                    placeholder={"Select"}
                                                    value={values?.SwitchTransferTo === "" ? schemeList[0]?.schemeId : values?.SwitchTransferTo}
                                                    label={"Transfer To"}
                                                    onChange={(e: any) => {
                                                        console.log(e);
                                                        setFieldValue("SwitchTransferTo", e.schemeId);
                                                    }}
                                                    disabled
                                                    // @ts-ignore
                                                    error={submitCount ? errors.SwitchTransferTo : null}
                                                />
                                            </Box>
                                        </Box>
                                        <Box className="row mx-2">
                                            <Box className="col-12">
                                                <Text css={{ color: "var(--colors-blue1)" }}>Amount</Text>
                                            </Box>
                                            <Box className="col-5">
                                                <Input
                                                    label="Enter Amount"
                                                    name="SwitchAmount"
                                                    placeholder="Enter Amount"
                                                    className="border border-2"
                                                    // @ts-ignore
                                                    error={submitCount ? errors.SwitchAmount : null}
                                                    onChange={handleChange}
                                                    value={values?.SwitchAmount}
                                                    autocomplete="off"
                                                    disabled
                                                />
                                            </Box>
                                        </Box>
                                        <Box className="row mx-2">
                                            <Box className="col-12">
                                                <Text css={{ color: "var(--colors-blue1)" }}>Enter OTP</Text>
                                            </Box>
                                            <Box className="col-5">
                                                <Input
                                                    label=""
                                                    name="otp"
                                                    placeholder="Enter OTP"
                                                    defaultValue=""
                                                    onChange={handleChange}
                                                    value={values?.otp}
                                                />
                                            </Box>
                                        </Box>
                                    </>}
                                <Box className="row justify-content-end mx-2" >
                                    <Box className="col-auto px-0">
                                        {showOtpPart ? (<>
                                            <Button
                                                type="submit"
                                                css={{ backgroundColor: "orange" }}
                                                onClick={() => {
                                                    setShowOtpPart(false);
                                                    setInvestStatus(false);
                                                }}>
                                                <Text className="p-2" css={{ color: "white" }}>
                                                    Back
                                                </Text>
                                            </Button>

                                        </>

                                        ) : (<>
                                            <Button
                                                type="button"
                                                css={{ backgroundColor: "orange" }}
                                                disabled={loadingCart}
                                                onClick={() => { setInvestStatus(false); handleSubmit() }}
                                            >
                                                <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>
                                                    {loadingCart ? <>Loading... </> : <><CartFill className="me-1" /> Add to Cart </>}
                                                </Text>
                                            </Button>
                                        </>)}
                                    </Box>
                                    <Box className="col-auto">
                                        <Button
                                            type="submit"
                                            css={{ backgroundColor: "orange" }}
                                            onClick={() => { setInvestStatus(true); handleSubmit() }}
                                            loading={loading}
                                            disabled={loadingCart}
                                        >
                                            {loadingCart ? <><Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>Loading... </Text></> : <>
                                                {showOtpPart ?
                                                    <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}>Confirm </Text>
                                                    : <Text size="h6" className="p-2 rounded-4" css={{ color: "white" }}> <Shuffle className="me-2" color="white" /> Switch</Text>}
                                            </>}
                                        </Button>
                                    </Box>
                                </Box>
                            </>)}</Formik>
                </Box>
            </Box>
        </>
    )
}
export default (SwitchModal)
