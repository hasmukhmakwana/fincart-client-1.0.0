import React, { useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import router from 'next/router';
import style from "../../../Product.module.scss";
import "../../../Product.module.scss";
import { StyledTabs, TabsContent, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsTrigger } from "../TabTrigger.style";
import SelectMenu from "@ui/Select/Select";
import StarFill from "App/icons/StarFill";
import Input from "App/ui/Input";
import Basket2fill from "App/icons/Basket2fill";
import CartFill from "App/icons/CartFill";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { addingTocart, getBankPaymentModes } from "App/api/cart";
import { encryptData, getUser } from "App/utils/helpers";
import Layout, { toastAlert } from "App/shared/components/Layout";

const AvailableAmtModal = ({ selectedData, setOpenPurchase, inprocessTran }: any) => {
    console.log(inprocessTran);
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}> Available Amount</Text>
                </Box>
                <Box className="modal-body p-3">
                    <Box className="row border-bottom border-warning mx-2 pb-2">
                        <Box className="col-12">
                            <Box className="row">
                                <Box className="col-auto">
                                    {/* @ts-ignore */}
                                    <Text className="text-capitalize" >{selectedData?.schemeName}</Text>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                    <Box className="row mt-2 mx-2">
                        <Box className="col-12">
                            <Box className="row">
                                <Box className="col-auto">
                                    <Text>Current Amount : <span>₹ {Number(selectedData?.currValue).toLocaleString(
                                        "en-IN"
                                    )}</span></Text>
                                </Box>
                                <Box className="col-auto">
                                    <Text>Available Amount : <span>₹{Number(selectedData?.AvlBalAmt).toLocaleString(
                                        "en-IN"
                                    )}</span></Text>
                                </Box>
                            </Box>
                            <Box className="row">
                                <Box className="col-auto mt-3">
                                    <Text css={{ color: "var(--colors-blue1)" }}>In-Process Transaction: </Text>
                                </Box>
                            </Box>
                            {inprocessTran.map((ele: any) => {
                                return (
                                    <Box className="row">
                                        <Box className="col-auto">
                                            <Text css={{ color: "var(--colors-blue1)" }}>{ele?.trxntype === "R" ? 'Redemption' : ele?.trxntype} : ₹ {Number(ele?.inProcAmt).toLocaleString(
                                                "en-IN"
                                            )}</Text>
                                        </Box>
                                    </Box>

                                )
                            })}

                        </Box>

                    </Box>
                    <Box className="row justify-content-end mx-2 mt-3">
                        <Box className="col-auto px-0">


                            <Button
                                type="button"
                                css={{ backgroundColor: "orange" }}
                                onClick={() => { router.push("/UpcomingTransactions") }}
                            >
                                <Text size="h6" className="p-2 rounded-4" css={{ color: "#000" }}>

                                    {/* Add to Cart */}
                                    View Upcoming Transactions
                                </Text>
                            </Button>
                        </Box>
                    </Box>
                </Box>
            </Box>


        </>
    );
}

export default (AvailableAmtModal);