//@ts-nocheck
import React, { useEffect, useState } from "react";
import Layout, { toastAlert } from "App/shared/components/Layout";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { RadioGroup } from "@radix-ui/react-radio-group";
import Radio from "@ui/Radio/Radio";
import { GroupBox } from "@ui/Group/Group.styles";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@ui/Accordian/Accordian";
import DownArrow from "App/icons/DownArrow";
import UpArrowLong from "App/icons/UpArrowLong";
import { Button } from "@ui/Button/Button";
import CartFill from "App/icons/CartFill";
import Shuffle from "App/icons/Shuffle";
import DownArrowLong from "App/icons/DownArrowLong";
import Piggybankfill from "App/icons/Piggybankfill";
import ArrowLeftRight from "App/icons/ArrowLeftRight";
import CheckCircleFill from "App/icons/CheckCircleFill";
import Basket2fill from "App/icons/Basket2fill";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import Modal from "@ui/ModalDialog/ModalDialog"
import Loader from "App/shared/components/Loader";
import styles from "../../Product.module.scss";
import style from "../../../DashboardModule/Dashboard.module.scss";
import {
  Goal,
  AvailableAmtModal,
  PurchaseModal,
  RedeemModal,
  SIPModal,
  STPModal,
  SwitchModal,
  SwpModal,
} from "./Modals";
import usePortfolioStore from "App/modules/DashboardModule/store";
import { allAMCInvestments, getOverallPortfolioData } from "App/api/dashboard";
import { encryptData, getUser } from "App/utils/helpers";
import Card from "@ui/Card";
import Add from "App/icons/Add";
import AllActiveInvestments from "../../AllActiveInvestments/AllActiveInvestments";
import { useRouter } from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';
const MutualFundPage = ({ basicId, userGoalId, dataType, setDataType, isGoalWisePortfolio }: any) => {
  const user: any = getUser();
  const router = useRouter();
  const {
    isFilter,
    loading,
    portfolioData,
    mutualFundPortfolioData
  } = usePortfolioStore();
  const { setContactUsVal } = useContactUsStore();
  const [openAvailableAmt, setOpenAvailableAmt] = useState(false);
  const [openPurchase, setOpenPurchase] = useState(false);
  const [openSuccess, setOpenSuccess] = useState(false);
  const [openRedeem, setOpenRedeem] = useState(false);
  const [openSIP, setOpenSIP] = useState(false);
  const [openSTP, setOpenSTP] = useState(false);
  const [openSwitch, setOpenSwitch] = useState(false);
  const [openSwp, setOpenSwp] = useState(false);
  const [loadData, setLoadData] = useState(false);
  const [openGoal, setOpenGoal] = useState(false);
  const [mfData, setMfData] = useState<any>([]);
  const [dataLoad, setDataLoad] = useState(false);
  const [selectedData, setSelectedData] = useState<any>([]);
  const [inprocessTran, setInProcessTran] = useState<any>([]);
  const [selectedAvlData, setSelectedAvlData] = useState<any>([]);
  const [messageTxt, setMessageTxt] = useState<any>("");
  const [selectedFunid, setSelectedFunid] = useState<any>("");
  const [goalList, setGoalList] = useState<any>([]);
  const [selectCategory, setSelectCategory] = useState<any>("All");
  const [assetType, setAssetType] = useState<any>("ALL");
  const [totalInvestedValue, setTotalInvestedValue] = useState(0.0);
  const [totalCurrentValue, setTotalCurrentValue] = useState(0.0);

  const handleMfData = async (e: any) => {
    try {
      setLoadData(true);

      if (basicId === "null" || basicId === undefined) {
        return;
      }

      const obj: any = {
        basicid: basicId === "0" ? user?.basicid : basicId,
        fundId: e,
        filtertype: dataType == "active" ? "NONZERO" : "ALL",
        isTotalPortfolio: basicId === "0" ? "Y" : "N",
        investmentSource: assetType
      };

      if (selectCategory != "All")
        obj["objective"] = selectCategory;

      const enc: any = encryptData(obj);
      const result: any = await allAMCInvestments(enc);
      setMfData(result?.data);
      setLoadData(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoadData(false);
    }
  };


  const getGains = (currentValue, investedValue) => {
    if (currentValue != "undefined" && investedValue != "undefined") {

      if (investedValue == 0)
        return 0;

      return (((currentValue - investedValue) / investedValue) * 100).toFixed(2);
    }
  }

  const gainLoss = (mfGain, current_value) => {
    if (mfGain != "undefined" && current_value != "undefined") {
      if (current_value == 0)
        return 0;
      else
        return ((parseFloat(mfGain) / parseFloat(current_value)) * 100).toFixed(2);
    } else {
      return 0;
    }
  }

  const assignTotalInvestedValue = (portfolioDetails: ProductPortfolioTypes[]) => {
    if (portfolioDetails == undefined) return 0;

    if (portfolioDetails.length == 0)
      setTotalInvestedValue(0.0);

    let totalInvestedValue = 0;

    portfolioDetails.map((record) => {
      totalInvestedValue += Number.parseFloat(record?.investment);
    })

    setTotalInvestedValue(totalInvestedValue);
  }

  const assignTotalCurrentValue = (portfolioDetails: ProductPortfolioTypes[]) => {
    if (portfolioDetails == undefined) return 0;

    if (portfolioDetails.length == 0)
      setTotalCurrentValue(0.0);

    let totalCurrentValue = 0;

    portfolioDetails.map((record) => {
      totalCurrentValue += Number.parseFloat(record?.current_value);
    })

    setTotalCurrentValue(totalCurrentValue);
  }

  useEffect(() => {
    if (openSuccess) {
      if (selectedFunid !== "") {
        handleMfData(selectedFunid);
      }
    }
  }, [openSuccess])

  useEffect(() => {
    assignTotalInvestedValue(mutualFundPortfolioData);
    assignTotalCurrentValue(mutualFundPortfolioData);
  }, [mutualFundPortfolioData])

  return (
    <>
      {loading ? (<>
        <Loader />
      </>) : (<>
        {mutualFundPortfolioData.length === 0 && !isFilter ?
          (isGoalWisePortfolio ? <>
            <Card css={{ px: 15, py: 2, mb: 15 }} >
              <Box>
                <Box className="container pt-4 ps-3 pb-3">
                  <Box className={`pb-3 text-center text-sm-start ${style.sectionMainHeading}`}>No records
                  </Box>
                </Box>
              </Box>
            </Card>
          </> :
            <>
              <Card css={{ p: 15, mb: 20 }}>
                <Box>
                  <Box className="container pt-5">
                    <Box className="row d-flex justify-content-center text-center">
                      <Box className={`col-sm-auto col-md-10 col-lg-7 ${style.lightTextBlue} ${style.yellowBorderBottom} justify-content-center text-center ${style.mftitle}`}>
                        Mutual funds are easy to invest and offer a great deal of
                        Flexibility. Mutual funds have been proved as a great investment option
                        with multiple
                        advantages
                      </Box>
                    </Box>
                    <Box className="row d-flex justify-content-md-center">
                      <Box className="col-sm-12 col-md-auto col-lg-auto">
                        <Box className="row d-flex justify-content-center ">
                          <Box
                            className={`justify-content-center ${style.centeralign} col-8 col-md-4 col-lg-4`}>
                            <Box className="row text-end mt-5 my-2">
                              <Box
                                className={`col-8 col-md-8 col-lg-8 ${style.pointerHeading} ${style.rightalign} mt-2`}>
                                Flexibility
                              </Box>
                              <Box className="col-4 col-md-4 col-lg-4">
                                <img className={`img-fluid ${style.investImg}`}
                                  src="/Flexibility.png" />
                              </Box>
                            </Box>
                            <Box className="row text-end my-2">
                              <Box
                                className={`col-8 col-md-8 col-lg-8 ${style.pointerHeading} ${style.rightalign} mt-2`}>
                                Low&nbsp;Cost
                              </Box>
                              <Box className="col-4 col-md-4 col-lg-4">
                                <img className={`img-fluid ${style.investImg}`}
                                  src="/LowCost.png" />
                              </Box>
                            </Box>
                            <Box className="row text-end my-2">
                              <Box
                                className={`col-8 col-md-8 col-lg-8 ${style.pointerHeading} ${style.rightalign} mt-2`}>
                                Diversification
                              </Box>
                              <Box className="col-4 col-md-4 col-lg-4">
                                <img className={`img-fluid ${style.investImg}`}
                                  src="/Diversification.png" />
                              </Box>
                            </Box>
                            <Box className="row text-end my-2">
                              <Box
                                className={`col-8 col-md-8 col-lg-8 ${style.pointerHeading} ${style.rightalign} mt-2`}>
                                Professional Management
                              </Box>
                              <Box className="col-4 col-md-4 col-lg-4">
                                <img className={`img-fluid ${style.investImg}`}
                                  src="/ProfessionallyManaged.png" />
                              </Box>
                            </Box>
                          </Box>
                          <Box
                            className={`${style.centeralign} col-12 col-sm-12 col-md-3 col-lg-auto mt-4 p-0`}>
                            <Box className="row mt-4 justify-content-center">
                              <Box className="col-8 col-md-12 col-lg-8 text-center">
                                <img className={`img-fluid ${style.mutualfunfimg}`}
                                  src="/MutualFund.png" />
                              </Box>
                            </Box>
                            <Box className="row justify-content-center">
                              <Box className="col-3 col-md-5 col-lg-7 text-center">
                                <img className={`img-fluid ${style.investImg}`}
                                  src="/LowCost.png" />
                              </Box>
                              <Box className={`col-10 col-md-8 col-lg-7 ${style.pointerHeading} text-center`}
                                width="40%">
                                One Time Investment
                              </Box>
                            </Box>
                          </Box>
                          <Box className={`${style.centeralign} col-8 col-md-4 col-lg-4`}>
                            <Box className="row mt-5 my-2 text-start my-2">
                              <Box className="col col-4 col-md-4 col-lg-4 ">
                                <img className={`img-fluid ${style.investImg}`}
                                  src="/Variety.png" />
                              </Box>
                              <Box
                                className={`col-8 col-md-8 col-lg-8 ${style.pointerHeading} ${style.leftalign} mt-2`} >
                                Variety
                              </Box>
                            </Box>
                            <Box className="row text-start my-2">
                              <Box className="col-4 col-md-4 col-lg-4">
                                <img className={`img-fluid ${style.investImg}`}
                                  src="/Transparent.png" />
                              </Box>
                              <Box
                                className={`col-8 col-md-8 col-lg-8 ${style.pointerHeading} ${style.leftalign} mt-2`}>
                                Transparency
                              </Box>
                            </Box>
                            <Box className="row text-start my-2">
                              <Box className="col-4 col-md-4 col-lg-4 ">
                                <img className={`img-fluid ${style.investImg}`}
                                  src="/Liquidity.png" />
                              </Box>
                              <Box
                                className={`col-8 col-md-8 col-lg-8 ${style.pointerHeading} ${style.leftalign} mt-2`}>
                                Liquidity
                              </Box>
                            </Box>
                            <Box className="row text-start my-2">
                              <Box className="col-4 col-md-4 col-lg-4">
                                <img className={`img-fluid ${style.investImg}`} src="/Tax.png" />
                              </Box>
                              <Box
                                className={`col-8 col-md-8 col-lg-8 ${style.pointerHeading} ${style.leftalign} mt-2`}>
                                Tax&nbsp;Benefit
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                      </Box>
                    </Box>
                  </Box>
                  <Box className="m-4 text-end">
                    <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow"
                      onClick={() => {
                        setContactUsVal({
                          SubjectId: "3",
                          Description: ""
                        }
                        );
                        router.push('/Support/ContactUs')
                      }}>
                      <GroupBox>
                        {/* @ts-ignore */}
                        <Text size="h5">Contact Us</Text>
                      </GroupBox>
                    </Button>
                  </Box>
                </Box>
              </Card>
            </>) : <>
            <Box className="col-md-12 col-sm-12 col-lg-12 mt-2">
              <Box background="default" css={{ p: 10, borderRadius: "8px" }}>
                {/* @ts-ignore */}
                <Box className="row">
                  <AllActiveInvestments type={"MF"} assetType={assetType} setAssetType={setAssetType} selectCategory={selectCategory} setSelectCategory={setSelectCategory} basicId={basicId} userGoalId={userGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={isGoalWisePortfolio} />

                  <Box className="col col-12 col-md-6 col-lg-6 text-right mt-2 mt-md-0">
                    {/* @ts-ignore */}
                    <Box className="row justify-content-md-end me-3">
                      <Box className="col col-auto d-flex text-right">
                        {/* @ts-ignore */}
                        <Text weight="extrabold" color="blue">
                          Invested Value :
                        </Text>
                        {/* @ts-ignore */}
                        <Text color="blue">
                          {" "}
                          &#x20B9;{" "}
                          {/* {isNaN(record?.invested_value)
                            ? "0"
                            : Number(record?.invested_value).toLocaleString(
                              "en-IN"
                            )} */}
                          {Number(totalInvestedValue).toLocaleString("en-IN")}
                        </Text>
                      </Box>
                      <Box className="col col-auto d-flex">
                        {/* @ts-ignore */}
                        <Text weight="extrabold" color="blue">
                          Current Value :
                        </Text>
                        {/* @ts-ignore */}
                        <Text color="blue">
                          {" "}
                          &#x20B9;{" "}
                          {/* {Number(record?.current_value).toLocaleString(
                            "en-IN"
                          ) || "0.0"} */}
                          {Number(totalCurrentValue).toLocaleString("en-IN")}
                        </Text>
                        <Text color="red" weight="extrabold" className="ms-1">
                          ({getGains(totalCurrentValue, totalInvestedValue)}%)
                        </Text>
                      </Box>
                      {/* </>} */}
                    </Box>
                  </Box>
                </Box>
              </Box>
              {mutualFundPortfolioData.length === 0 && isFilter ?
                <>
                  <Card css={{ px: 15, py: 2, mb: 15 }} >
                    <Box>
                      <Box className="container pt-4 ps-3 pb-3">
                        <Box className={`pb-3 text-center text-sm-start ${style.sectionMainHeading}`}>No records
                        </Box>
                      </Box>
                    </Box>
                  </Card>
                </> : dataLoad ? (
                  <Loader />
                ) : (
                  <Card className="mt-0" css={{ p: 15 }}>
                    <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                      {/* @ts-ignore */}
                      <Accordion
                        type="single"
                        collapsible
                        onValueChange={(e) => {
                          setSelectedFunid(e);
                          handleMfData(e);
                        }}
                      >
                        {mutualFundPortfolioData.map((record, index) => {
                          return (
                            <AccordionItem value={record?.id.toString()}>
                              <AccordionTrigger className="row p-0">
                                <Box className="col">
                                  <Box className="tabtitle">
                                    <Text size="h4">{record?.partner_name}</Text>
                                  </Box>
                                  <Box className="row row-cols-1 row-cols-md-6 tabtitlecontent">
                                    <Box className="col">
                                      <Text size="h6">Net Investment Value</Text>
                                      <Text size="h6">
                                        {" "}
                                        &#x20B9;{" "}
                                        {Number(record?.investment).toLocaleString("en-IN")}
                                      </Text>
                                      {isGoalWisePortfolio && <>
                                        <Text size="h6" className="fw-bold">
                                          Allocated Value
                                        </Text>
                                        <Text size="h6">
                                          {" "}
                                          &#x20B9;{" "}
                                          {Number(record?.curr_goal_invested_amt).toLocaleString("en-IN")}
                                        </Text>
                                      </>
                                      }
                                    </Box>
                                    <Box className="col">
                                      <Text size="h6">Current Value</Text>
                                      <Text size="h6">
                                        {" "}
                                        &#x20B9;{" "}
                                        {Number(record?.current_value).toLocaleString(
                                          "en-IN"
                                        )}
                                      </Text>
                                      {isGoalWisePortfolio && <>
                                        <Text size="h6" className="fw-bold">Allocated Value</Text>
                                        <Text size="h6">
                                          {" "}
                                          &#x20B9;{" "}
                                          {Number(record?.curr_goal_current_amt).toLocaleString("en-IN")}
                                        </Text>
                                      </>}
                                    </Box>
                                    <Box className="col">
                                      <Box>
                                        <Text size="h6">Gain/Loss</Text>
                                      </Box>
                                      <Box>
                                        <GroupBox>
                                          <Text size="h6">
                                            {gainLoss(record?.mfGain, record?.current_value) === "Infinity" ? "0"
                                              : gainLoss(record?.mfGain, record?.current_value)}%
                                          </Text>
                                          <Box>
                                            {gainLoss(record?.mfGain, record?.current_value) < 0 ?
                                              <DownArrowLong size="10"></DownArrowLong> :
                                              gainLoss(record?.mfGain, record?.current_value) > 0 ? <UpArrowLong size="10"></UpArrowLong> : <></>
                                            }
                                          </Box>
                                        </GroupBox>
                                      </Box>
                                    </Box>
                                  </Box>
                                </Box>
                                <Box className="col col-auto action">
                                  <DownArrow />
                                </Box>
                              </AccordionTrigger>

                              <AccordionContent className="border p-3">
                                {loadData ? (
                                  <Loader />
                                ) : (
                                  <Box className="row">
                                    <Box className="col-md-12 col-sm-12 col-lg-12">
                                      {mfData?.length == 0 ? <>
                                        <Text
                                          weight="normal"
                                          size="h5"
                                          color="blue1"
                                        >
                                          No Investments Found
                                        </Text>
                                      </> :
                                        mfData.map((item: any, idx: any) => {
                                          return (
                                            <Box className="portfolioRow p-2">
                                              <Box className="row">
                                                <Box className="col col-12">
                                                  {/* @ts-ignore */}
                                                  <Text
                                                    weight="normal"
                                                    transform="uppercase"
                                                    size="h5"
                                                    color="blue1"
                                                    css={{ pl: 0, pr: 0, pt: 10, pb: 0 }}
                                                  >
                                                    {item?.schemeName}
                                                    <Box className={`badge ms-1 ${item?.trxnSource === "External" ? styles.externalColor : styles.internalColor}`}>{item?.trxnSource} </Box>
                                                    {item?.InvestProfile != "" && <Box className={`badge ms-1 ${styles.investProfileColor}`}>{item?.InvestProfile} </Box>}
                                                    {item?.goalList.length > 0 ? <>
                                                      <Box className={`badge ms-1 ${styles.internalColor}`}>Goal Linked :
                                                        (<span className="mb-2" style={{ fontSize: "0.8rem", fontWeight: "heavy !important" }}> {item?.goalList.length} </span>)
                                                        <Button css={{ width: "16px", height: "16px", backgroundColor: "orange", borderRadius: '10px 10px 10px 10px', cursor: "pointer", lineHeight: "1rem" }}
                                                          className="m-0 p-0 ms-2"
                                                          onClick={() => {
                                                            setOpenGoal(true);
                                                            setGoalList(item?.goalList);
                                                          }}
                                                        >
                                                          <Text className="mb-0 text-black">i</Text>
                                                        </Button>
                                                      </Box>
                                                    </> : <>
                                                      <Box className={`badge ms-1 ${styles.externalColor}`}>No Goal Linked </Box>
                                                    </>}
                                                  </Text>
                                                </Box>
                                                {/* @ts-ignore */}
                                                <Box className="col col-6 col-md-2 mb-3">
                                                  <Text>Folio</Text>
                                                  {/* @ts-ignore */}
                                                  <Text weight="normal">{item?.folioNo}</Text>
                                                </Box>
                                                <Box className="col col-6 col-md-2 mb-3">
                                                  <Text>Invested Value</Text>
                                                  {/* @ts-ignore */}
                                                  <Text weight="normal">
                                                    {" "}
                                                    &#x20B9;{" "}
                                                    {Number(item?.investValue).toLocaleString(
                                                      "en-IN"
                                                    )}
                                                  </Text>
                                                </Box>
                                                <Box className="col col-6 col-md-2 mb-3">
                                                  <Text>Units</Text>
                                                  {/* @ts-ignore */}
                                                  <Text weight="normal">{item?.balUnits} </Text>
                                                </Box>
                                                <Box className="col col-6 col-md-2 mb-3">
                                                  <Text>Current Nav</Text>
                                                  {/* @ts-ignore */}
                                                  <Text weight="normal">
                                                    {item?.current_nav}
                                                  </Text>
                                                </Box>
                                                <Box className="col col-6 col-md-2 mb-3">
                                                  <Text>Current Amount</Text>
                                                  {/* @ts-ignore */}
                                                  <Text weight="normal">
                                                    {" "}
                                                    &#x20B9;{" "}
                                                    {Number(item?.currValue).toLocaleString(
                                                      "en-IN"
                                                    )}
                                                  </Text>
                                                </Box>
                                                <Box className="col col-6 col-md-2 mb-3">
                                                  <Text>Gain/Loss</Text>
                                                  {/* @ts-ignore */}
                                                  <GroupBox position="left">
                                                    {gainLoss(item?.gain, item?.currValue) < 0 ?
                                                      <>
                                                        <Box>
                                                          {/* @ts-ignore */}
                                                          <Text weight="bold" css={{ color: "red" }}>
                                                            {gainLoss(item?.gain, item?.currValue)}%
                                                          </Text>
                                                        </Box>
                                                        <Box className="lossDown">
                                                          <DownArrowLong size="13" />
                                                        </Box>
                                                      </>
                                                      : <>
                                                        {gainLoss(item?.gain, item?.currValue).toString() !== "NaN" ? <>
                                                          <Box>
                                                            {/* @ts-ignore */}
                                                            <Text weight="bold" color="lightGreen">
                                                              {gainLoss(item?.gain, item?.currValue)}%
                                                            </Text>
                                                          </Box>
                                                          <Box className="gainUp">
                                                            <UpArrowLong size="13" />
                                                          </Box>
                                                        </> : <>
                                                          <Box>
                                                            {/* @ts-ignore */}
                                                            <Text weight="bold" color="gray">
                                                              0.00 %
                                                            </Text>
                                                          </Box>

                                                        </>}
                                                      </>
                                                    }

                                                  </GroupBox>
                                                </Box>
                                              </Box>
                                              {/* <Box> */}
                                              {/* @ts-ignore */}
                                              <Box className="row justify-content-start">

                                              </Box>
                                              <Box className="row justify-content-between">
                                                <Box className="col-auto pt-3">
                                                  <Box
                                                    disabled={item?.isPurAllow === "Y" ? false : true}
                                                    onClick={(e) => { setOpenAvailableAmt(true); setSelectedAvlData(item); setInProcessTran(item?.inProcUnitAmtList) }}
                                                    css={{
                                                      color: "var(--colors-blue1)",
                                                      cursor: "pointer",
                                                    }}
                                                  >
                                                    {/* @ts-ignore */}
                                                    <Text size="h6"><u>Available Amt: ₹{Number(item?.AvlBalAmt).toLocaleString(
                                                      "en-IN"
                                                    )}</u></Text>
                                                  </Box>
                                                </Box>


                                                <Box className="col-auto">
                                                  <Box className="row justify-content-evenly mt-1 pe-3">
                                                    {item?.trxnSource !== "External" ? <>
                                                      <Button
                                                        className={`col-auto mb-2 `}
                                                        color="yellowGroup"
                                                        size="md"
                                                        disabled={item?.isPurAllow === "Y" ? false : true}
                                                        onClick={(e) => { setOpenPurchase(true); setSelectedData(item); }}
                                                      >
                                                        <GroupBox
                                                          align="center"
                                                          className={`${styles.hovBtn}`}
                                                        >
                                                          <Box className="row">
                                                            <Box className="ps-2 col-auto">
                                                              <Basket2fill />
                                                            </Box>
                                                            {/* @ts-ignore */}
                                                            <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                                              <Text
                                                                size="h6"
                                                              // color="gray8"
                                                              >
                                                                Purchase
                                                              </Text>
                                                            </Box>
                                                          </Box>
                                                        </GroupBox>
                                                      </Button>
                                                      <Button
                                                        className="col-auto mb-2 "
                                                        color="yellowGroup"
                                                        size="md"
                                                        disabled={item?.isRedAllow === "Y" ? false : true}
                                                        onClick={(e) => { setOpenRedeem(true); setSelectedData(item); }}
                                                      >
                                                        <GroupBox
                                                          align="center"
                                                          className={`${styles.hovBtn}`}
                                                        >
                                                          <Box className="row">
                                                            <Box className="ps-2 col-auto">
                                                              <CartFill />
                                                            </Box>
                                                            {/* @ts-ignore */}
                                                            <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                                              <Text
                                                                size="h6"
                                                              >
                                                                Redeem
                                                              </Text>
                                                            </Box>
                                                          </Box>
                                                        </GroupBox>
                                                      </Button>
                                                      <Button
                                                        className="col-auto mb-2 "
                                                        color="yellowGroup"
                                                        size="md"
                                                        disabled={item?.isSIPAllow === "Y" ? false : true}
                                                        onClick={(e) => { setOpenSIP(true); setSelectedData(item); }}
                                                      >
                                                        <GroupBox
                                                          align="center"
                                                          className={`${styles.hovBtn}`}
                                                        >
                                                          <Box className="row">
                                                            <Box className="ps-2 col-auto">
                                                              <Piggybankfill />
                                                            </Box>
                                                            {/* @ts-ignore */}
                                                            <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                                              <Text
                                                                size="h6"
                                                              >
                                                                SIP
                                                              </Text>
                                                            </Box>
                                                          </Box>
                                                        </GroupBox>
                                                      </Button>
                                                      <Button
                                                        className="col-auto mb-2 "
                                                        color="yellowGroup"
                                                        size="md"
                                                        disabled={item?.isSTPAllow === "Y" ? false : true}
                                                        onClick={(e) => { setOpenSTP(true); setSelectedData(item); }}
                                                      >
                                                        <GroupBox
                                                          align="center"
                                                          className={`${styles.hovBtn}`}
                                                        >
                                                          <Box className="row">
                                                            <Box className="ps-2 col-auto">
                                                              <ArrowLeftRight />
                                                            </Box>
                                                            {/* @ts-ignore */}
                                                            <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                                              <Text
                                                                size="h6"
                                                              >
                                                                STP
                                                              </Text>
                                                            </Box>
                                                          </Box>
                                                        </GroupBox>
                                                      </Button>
                                                      <Button
                                                        className="col-auto mb-2 "
                                                        color="yellowGroup"
                                                        size="md"
                                                        disabled={item?.isSwitchAllow === "Y" ? false : true}
                                                        onClick={(e) => { setOpenSwitch(true); setSelectedData(item); }}
                                                      >
                                                        <GroupBox
                                                          align="center"
                                                          className={`${styles.hovBtn}`}
                                                        >
                                                          <Box className="row">
                                                            <Box className="ps-2 col-auto">
                                                              <Shuffle />
                                                            </Box>
                                                            {/* @ts-ignore */}
                                                            <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                                              <Text
                                                                size="h6"
                                                              >
                                                                Switch
                                                              </Text>
                                                            </Box>
                                                          </Box>
                                                        </GroupBox>
                                                      </Button>
                                                      <Button
                                                        className="col-auto mb-2 "
                                                        color="yellowGroup"
                                                        size="md"
                                                        disabled={item?.isSWPAllow === "Y" ? false : true}
                                                        onClick={(e) => { setOpenSwp(true); setSelectedData(item); }}
                                                      >
                                                        <GroupBox
                                                          align="center"
                                                          className={`${styles.hovBtn}`}
                                                        >
                                                          <Box className="row">
                                                            <Box className="ps-2 col-auto">
                                                              <Shuffle />
                                                            </Box>
                                                            {/* @ts-ignore */}
                                                            <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                                                              <Text
                                                                size="h6"
                                                              >
                                                                SWP
                                                              </Text>
                                                            </Box>
                                                          </Box>
                                                        </GroupBox>
                                                      </Button>
                                                    </> : <>
                                                      {/* <Button>??</Button> */}
                                                    </>}
                                                  </Box>
                                                </Box>
                                              </Box>
                                              {/* </Box> */}
                                            </Box>
                                          );
                                        })
                                      }
                                    </Box>
                                  </Box>
                                )}
                              </AccordionContent>
                            </AccordionItem>

                          );
                        })}
                      </Accordion>
                      <Modal
                        open={openGoal}
                        setOpen={setOpenGoal}
                        css={{
                          "@bp0": { width: "80%" },
                          "@bp1": { width: "40%" },
                        }}
                      // className=""
                      >
                        <Goal setOpen={setOpenGoal} data={goalList} />
                      </Modal>
                      <DialogModal
                        open={openAvailableAmt}
                        setOpen={setOpenAvailableAmt}
                        css={{
                          "@bp0": { width: "90%" },
                          "@bp1": { width: "50%" },
                        }}
                      >
                        <AvailableAmtModal selectedData={selectedAvlData} setOpenPurchase={setOpenAvailableAmt} inprocessTran={inprocessTran} />
                      </DialogModal>

                      <DialogModal
                        open={openPurchase}
                        setOpen={setOpenPurchase}
                        css={{
                          "@bp0": { width: "90%" },
                          "@bp1": { width: "50%" },
                        }}
                      >
                        <PurchaseModal selectedData={selectedData} setOpenPurchase={setOpenPurchase} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                      </DialogModal>
                      <DialogModal
                        open={openRedeem}
                        setOpen={setOpenRedeem}
                        css={{
                          "@bp0": { width: "90%" },
                          "@bp1": { width: "50%" },
                        }}
                      >
                        <RedeemModal selectedData={selectedData} setOpenRedeem={setOpenRedeem} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                      </DialogModal>
                      <DialogModal
                        open={openSIP}
                        setOpen={setOpenSIP}
                        css={{
                          "@bp0": { width: "90%" },
                          "@bp1": { width: "50%" },
                        }}
                      >
                        <SIPModal selectedData={selectedData} setOpenSIP={setOpenSIP} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                      </DialogModal>
                      <DialogModal
                        open={openSTP}
                        setOpen={setOpenSTP}
                        css={{
                          "@bp0": { width: "90%" },
                          "@bp1": { width: "50%" },
                        }}
                      >
                        <STPModal selectedData={selectedData} setOpenSTP={setOpenSTP} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                      </DialogModal>
                      <DialogModal
                        open={openSwitch}
                        setOpen={setOpenSwitch}
                        css={{
                          "@bp0": { width: "90%" },
                          "@bp1": { width: "50%" },
                        }}
                      >
                        <SwitchModal selectedData={selectedData} setOpenSwitch={setOpenSwitch} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                      </DialogModal>
                      <DialogModal
                        open={openSwp}
                        setOpen={setOpenSwp}
                        css={{
                          "@bp0": { width: "90%" },
                          "@bp1": { width: "50%" },
                        }}
                      >
                        <SwpModal selectedData={selectedData} setOpenSwp={setOpenSwp} setOpenSuccess={setOpenSuccess} setMessageTxt={setMessageTxt} />
                      </DialogModal>
                      <DialogModal
                        open={openSuccess}
                        setOpen={setOpenSuccess}
                        css={{
                          "@bp0": { width: "60%" },
                          "@bp1": { width: "35%" },
                        }}
                      >
                        {/* @ts-ignore */}
                        <Box className="col-md-12 col-sm-12 col-lg-12">
                          <Box className="modal-body modal-body-scroll">
                            <Box className="row justify-content-center">
                              <Box className="col-lg-12 text-center" css={{ py: 50 }}>
                                <CheckCircleFill color="orange" size="2rem" />
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-center mt-2" css={{ color: "var(--colors-blue1)" }}>{messageTxt}</Text>
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                      </DialogModal>
                    </Box>
                  </Card>
                )}
            </Box>
          </>}
      </>)}
    </>
  );
};

export default MutualFundPage;
