import create from "zustand";
interface StoreTypes {
    investorId: string;
    setInvestorId: (payload: string) => void;
}
const usePortfolioStore = create<StoreTypes>((set) => ({
    investorId: "",
    setInvestorId: (payload) =>
        set((state) => ({
            ...state,
            investorId: payload || "",
        })),
}))