import React, { useEffect, useState } from "react";
import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Box from "@ui/Box/Box";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import Upload from "App/icons/Upload";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import style from "../Product.module.scss";
import MutualFundPage from "./components/MutualFundPage";
import { encryptData, getUser } from "App/utils/helpers";
import usePortfolioStore from "App/modules/DashboardModule/store";
import { getOverallPortfolioData } from "App/api/dashboard";
import router from "next/router";
import SelectMenu from "@ui/Select/Select";
import { getInvestorDD } from "App/api/UpcomingTransactions";
import useHeaderStore from "App/shared/components/Header/store";
const MutualFund = () => {
  const user: any = getUser();
  const {
    //  loading,
    // portfolioData,
    mutualFundPortfolioData,
    setPortfolioScreen,
    setMutualFundPortfolioData,
    setPortfolioData,
    setLoader,
  } = usePortfolioStore();

  const { cartCount } = useHeaderStore();

  const [investorId, setInvestorId] = useState<any>("0");
  const [investorList, setInvestorList] = useState<any>([]);
  const [dataType, setDataType] = useState<any>("active");

  const fetchMemberList = async () => {
    // console.log(user?.basicid, "user");
    try {

      setLoader(true);
      let obj = {
        basicId: user?.basicid,
        fundid: "0",
      }
      const enc: any = encryptData(obj);
      let result = await getInvestorDD(enc);

      let memberList = result?.data?.memberList;

      if (memberList != undefined && memberList.length != 0) {
        setInvestorList(memberList);
        setInvestorId(result?.data?.memberList[0]?.basicid);
      }

      setLoader(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  const handleActiveCall = async (e: any) => {
    try {
      let isActive = "";
      if (e === "active") {
        isActive = "true";
      } else {
        isActive = "false";
      }
      setLoader(true);
      const obj: any = {
        basicID: investorId,
        assetType: "",
        isActive: isActive,
      };
      if (investorId === "null" || investorId === undefined) {
        return;
      }
      const enc: any = encryptData(obj);
      // console.log(enc);
      const result: any = await getOverallPortfolioData(enc);
      setLoader(false);
      setPortfolioData(result?.data?.asset_wise_portfolio);
      for (let i = 0; i < result?.data?.productWisePortfolio.length; i++) {
        //console.log(result?.data?.productWisePortfolio[i])
        switch (result?.data?.productWisePortfolio[i].investment_type) {
          case "MUTUAL FUND":
            result?.data?.productWisePortfolio[i].data.sort((a: any, b: any) =>
              a.partner_name > b.partner_name ? 1 : -1
            );
            setMutualFundPortfolioData(
              result?.data?.productWisePortfolio[i].data
            );
            break;
        }
      }
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  useEffect(() => {
    // console.log(investorId, "InvestorID");
    (async () => {
      if (investorId === "0" || investorId === "null" || investorId === undefined) {
        return;
      }
      await handleActiveCall("active");
    })();
    return () => { }
  }, [investorId]);

  useEffect(() => {
    setPortfolioScreen(false);

    (async () => {
      if (user?.basicid === "null" || user?.basicid === undefined) {
        return;
      }
      await fetchMemberList();
    })();
    return () => { }
  }, [cartCount])


  return (
    <Layout>
      <PageHeader title="Mutual Fund" rightContent={<></>} />
      <Box>
        <Card css={{ p: 15, mb: 20 }}>
          {/* <Box> */}
          <Box className="row justify-content-between mx-1">
            <Box className="col col-auto d-flex">
              <Box css={{ width: "200px" }}>
                <SelectMenu
                  value={investorId}
                  items={investorList}
                  label="Investor"
                  bindValue={"basicid"}
                  bindName={"memberName"}
                  onChange={(e: any) => {
                    setInvestorId(e?.basicid || "")
                  }}
                />
              </Box>
            </Box>
            <Box className="col col-auto d-flex">
              <Box className="row justify-content-end" >
                <Box className="col col-auto d-flex">
                  <Box className="mt-4">
                    <Button
                      className={`col-auto mb-2`}
                      color="yellowGroup"
                      size="md"
                      onClick={() => router.push("/Products/UnMappedInvest")}
                    >
                      {/* @ts-ignore */}
                      <GroupBox align="center" className={`${style.hovBtn}`}>
                        <Box className="row">
                          <Box className="ps-2 col-auto">
                          </Box>
                          {/* @ts-ignore */}
                          <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                            {/* @ts-ignore */}
                            <Text size="h6">
                              UnMapped Investment
                            </Text>
                          </Box>
                        </Box>
                      </GroupBox>
                    </Button>
                  </Box>
                </Box>
                <Box className="col col-auto d-flex">
                  <Box className="mt-4">
                    <Button
                      className={`col-auto mb-2`}
                      color="yellowGroup"
                      size="md"
                      onClick={() => router.push("/Products/UploadCas")}
                    >
                      {/* @ts-ignore */}
                      <GroupBox align="center" className={`${style.hovBtn}`}>
                        <Box className="row">
                          <Box className="ps-2 col-auto">
                            <Upload style={{ weight: "bold" }} />
                          </Box>
                          {/* @ts-ignore */}
                          <Box className="col-auto py-2 ps-0 d-none d-md-flex">
                            {/* @ts-ignore */}
                            <Text size="h6">
                              Upload CAS
                            </Text>
                          </Box>
                        </Box>
                      </GroupBox>
                    </Button>
                  </Box>
                </Box>
              </Box>
            </Box>

          </Box>
          {/* </Box> */}
          <MutualFundPage basicId={investorId} dataType={dataType} setDataType={setDataType} />
        </Card>
      </Box>
    </Layout>
  );
};

export default MutualFund;
