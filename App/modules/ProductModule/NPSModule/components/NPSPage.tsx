import React, { useState } from 'react'
import Box from '@ui/Box/Box'
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import EyeIcon from 'App/icons/EyeIcon';
import DialogModal from "@ui/ModalDialog/ModalDialog";
import DownArrowLong from 'App/icons/DownArrowLong';
import styles from "../../Product.module.scss";
import style from "../../../DashboardModule/Dashboard.module.scss";
import usePortfolioStore from "App/modules/DashboardModule/store";
import UpArrowLong from 'App/icons/UpArrowLong';
import Loader from "App/shared/components/Loader";
const NPSPage = () => {
    const {
        protectionPortfolioData
    } = usePortfolioStore();
    return (

        <>
            <Card css={{ p: 15, mb: 20, pt: 0 }}>
                <Box className="row" css={{ backgroundColor: "#b3daff" }}>
                    <Box className="col-12 mb-1">&nbsp;</Box>
                </Box>
                <Box>
                    {protectionPortfolioData.map((record) => {
                        return (<>
                            <Box className="row py-2">
                                <Box className="col-9">
                                    {/* @ts-ignore */}
                                    <Text css={{ mb: 10 }} weight="extrabold" color="mediumBlue">
                                        {record?.partner_name}
                                    </Text>
                                    <Box className="row">
                                        <Box className="py-2 col-lg-3 col-md-3 col-sm-6 py-md-0 py-lg-0">
                                            {/* @ts-ignore */}
                                            <Text weight="extrabold">Cost Value</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="extrabold">&#x20B9; {Number(record?.investment).toLocaleString("en-IN")}</Text>
                                        </Box>
                                        <Box className="py-2 col-lg-3 col-md-3 col-sm-6 py-md-0 py-lg-0">
                                            {/* @ts-ignore */}
                                            <Text weight="extrabold">Market Value</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="extrabold">&#x20B9; {Number(record?.current_value).toLocaleString("en-IN")}</Text>
                                        </Box>
                                        <Box className="py-2 col-lg-3 col-md-3 col-sm-6 py-md-0 py-lg-0">
                                            {/* @ts-ignore */}
                                            <Text weight="extrabold">Gains/Loss</Text>
                                            {/* @ts-ignore */}
                                            <GroupBox position="left">
                                                {parseFloat(record?.netgain) >= 0 ? (
                                                    <>
                                                        <Box>
                                                            {/* @ts-ignore */}
                                                            <Text color="lightGreen">
                                                                {Number(record?.netgain).toLocaleString("en-IN")}
                                                            </Text>
                                                        </Box>
                                                        <Box className="gainUp">
                                                            <UpArrowLong size="10"></UpArrowLong>
                                                        </Box>
                                                    </>
                                                ) : (
                                                    <>
                                                        <Box>
                                                            {/* @ts-ignore */}
                                                            <Text className="text-danger">
                                                                {Number(record?.netgain).toLocaleString("en-IN")}
                                                            </Text>
                                                        </Box>
                                                        <Box className="lossDown">
                                                            <DownArrowLong size="10"></DownArrowLong>
                                                        </Box>
                                                    </>
                                                )}
                                            </GroupBox>
                                        </Box>
                                        <Box className="py-2 col-lg-3 col-md-3 col-sm-6 py-md-0 py-lg-0">
                                            {/* @ts-ignore */}
                                            <Text weight="extrabold">XIRR</Text>
                                            {/* @ts-ignore */}
                                            <Text weight="extrabold">{record?.xirr}%</Text>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="col-lg-3 py-4 text-end" alignSelf="middle">
                                    <Button
                                        className="col-auto mb-2 "
                                        color="yellowGroup"
                                        size="md"
                                    // onClick={setOpen}
                                    >
                                        <GroupBox
                                            //@ts-ignore
                                            align="center"
                                            className={`${styles.hovBtn}`}
                                        >
                                            <Box className="row">
                                                <Box className="ps-2 col-auto">
                                                    <EyeIcon />
                                                </Box>
                                                {/* @ts-ignore */}
                                                <Box className="col-auto py-2 ps-0 d-flex">
                                                    {/* @ts-ignore */}
                                                    <Text weight="extrabold" size="h6">
                                                        View
                                                    </Text>
                                                </Box>
                                            </Box>
                                        </GroupBox>
                                    </Button>

                                </Box>
                            </Box>
                        </>)
                    })}
                </Box>
            </Card>
        </>
    )
}

export default NPSPage