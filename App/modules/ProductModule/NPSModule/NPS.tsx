import React, { useState, useEffect } from 'react'
import Layout, { toastAlert } from 'App/shared/components/Layout'
// import InsurancePage from './components/InsurancePage';
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import usePortfolioStore from "App/modules/DashboardModule/store";
import { getOverallPortfolioData } from "App/api/dashboard";
import { encryptData, getUser } from "App/utils/helpers";
const NPS = () => {
    const [dataLoad, setDataLoad] = useState(false);
    const user: any = getUser();
    const {
        protectionPortfolioData,
        setProtectionPortfolioData,
        // setLoader,
    } = usePortfolioStore()
    const handleActiveCall = async (e:any) => {

        try {
            let isActive = "";
            if (e === "active") {
              isActive = "true";
            } else {
              isActive = "false";
            }
           // console.log(user?.basicid);
            setDataLoad(true);
            const obj: any = {
                basicID: user.basicid,
                assetType: "",
                isActive: isActive
            };
            if (user.basicid === "null" || user.basicid === undefined) {
                return;
            }
            const enc: any = encryptData(obj);
            const result: any = await getOverallPortfolioData(enc);
            console.log(result?.data?.productWisePortfolio)
            for (let i = 0; i < result?.data?.productWisePortfolio.length; i++) {
                if (result?.data?.productWisePortfolio[i].investment_type === "PROTECTION") {
                    console.log(result?.data?.productWisePortfolio[i].data[0]);
                    setProtectionPortfolioData(result?.data?.productWisePortfolio[i].data);
                }
            }
            setDataLoad(false);

        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setDataLoad(false);
            // setDataLoad(false);
        }
    };
    useEffect(() => {
        if (protectionPortfolioData.length === 0) {
            handleActiveCall("active");
          }
        return () => {
        }
    }, [])

    return (
        <Layout>
            <PageHeader title="Protection" rightContent={<></>} />
            {/* <InsurancePage load={dataLoad} /> */}
        </Layout>
    )
}

export default NPS