import React from 'react'
import GoalPlanningPage from './components/GoalPlanningPage';
import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import Holder from 'App/shared/components/Holder/Holder'
const GoalPlaningModule = () => {
  return (
    <Layout>
       <PageHeader title='Goal Planning' rightContent={<Holder/>} />
    <GoalPlanningPage/>
    </Layout>
  )
}

export default GoalPlaningModule