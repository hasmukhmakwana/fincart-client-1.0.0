import Badge from "@ui/Badge/Badge";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import EditIcon from "App/icons/EditIcon";
import Info from "App/icons/Info";
import Image from "next/image";
import React from "react";
import style from "../GoalPlanning.module.scss";
import Box from "./../../../ui/Box/Box";
import { GroupBox } from "./../../../ui/Group/Group.styles";
import { StyledTabs, TabsContent, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsTrigger } from "@radix-ui/react-tabs";
import Link from "next/link";
import { WEB_URL } from "App/utils/constants";
const GoalPlanningPage = () => {
  return (
    <Box background="">
      <Box className="">
        <Box className="row">
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <StyledTabs defaultValue="activegoals">
              <TabsList aria-label="Manage your account">
                <TabsTrigger value="activegoals" className={style.activetab}>
                  Active Goals
                </TabsTrigger>
                <TabsTrigger
                  value="previousgoals"
                  className={style.inactivetab}
                >
                  Previous Goals
                </TabsTrigger>
              </TabsList>
              <TabsContent value="activegoals" className={style.tabsContent}>
                <Box className="row">
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Card>
                      <Box css={{ p: 20 }}>
                        <GroupBox
                          position="center"
                          className="text-center"
                          horizontal
                        >
                          <Box>
                            <Box
                              className={style.imgbg}
                              display="inlineBlock"
                              background="default"
                            >
                              <img
                                width={54}
                                height={54}
                                src={`${WEB_URL}/FontAwsome (book-reader).png`}
                                alt="logo"
                              />
                              {/* <HomeIcon size="75" color="blue" /> */}
                            </Box>
                          </Box>
                          <Box css={{ mt: 20 }}>
                            <Text
                              weight="extrabold"
                              transform="uppercase"
                              color="default"
                              size="h4"
                            >
                              GR'S education
                            </Text>
                          </Box>
                          <Box>
                            <Text
                              weight="bold"
                              transform="uppercase"
                              color="blue"
                              size="h4"
                            >
                              60.12%
                            </Text>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Goal Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                SIP Amount{" "} 
                                <Info color="#fbc03f"></Info>
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                23000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Projected Amount
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                180000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Lumpsum Amount
                              </Text>
                            </Box>
                            <Box></Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Shortfall on Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Topup
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                2000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.m10}>
                            <Button color="yellow"><Link href='/GoalPlanningInitiateGoal'>Initiate Topup</Link></Button>
                          </Box>
                        </GroupBox>
                      </Box>
                    </Card>
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Card>
                      <Box css={{ p: 20 }}>
                        <GroupBox
                          position="center"
                          className="text-center"
                          horizontal
                        >
                          <Box>
                            <Box
                              className={style.imgbg}
                              display="inlineBlock"
                              background="default"
                            >
                              <img
                                width={54}
                                height={54}
                                src={`${WEB_URL}/FontAwsome (baby).png`}
                                alt="familyplaninggoal"
                              />
                              {/* <HomeIcon size="75" color="blue" /> */}
                            </Box>
                          </Box>
                          <Box css={{ mt: 20 }}>
                            <Text
                              weight="extrabold"
                              transform="uppercase"
                              color="default"
                              size="h4"
                            >
                              Family Planning Goal
                            </Text>
                          </Box>
                          <Box>
                            <Text
                              weight="bold"
                              transform="uppercase"
                              color="blue"
                              size="h4"
                            >
                              60.12%
                            </Text>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Goal Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                SIP Amount{" "}  
                                <Info color="#fbc03f"></Info>
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                23000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Projected Amount
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                180000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Lumpsum Amount
                              </Text>
                            </Box>
                            <Box></Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Shortfall on Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Topup
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                2000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.m10}>
                            <Button color="yellow"><Link href='/GoalPlanningInitiateGoal'>Initiate Topup</Link></Button>
                          </Box>
                        </GroupBox>
                      </Box>
                    </Card>
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Card>
                      <Box css={{ p: 20 }}>
                        <GroupBox
                          position="center"
                          className="text-center"
                          horizontal
                        >
                          <Box>
                            <Box
                              className={style.imgbg}
                              display="inlineBlock"
                              background="default"
                            >
                              <img
                                width={54}
                                height={54}
                                src={`${WEB_URL}/FontAwsome (car).png`}
                                alt="car"
                              />
                              {/* <HomeIcon size="75" color="blue" /> */}
                            </Box>
                          </Box>
                          <Box css={{ mt: 20 }}>
                            <Text
                              weight="extrabold"
                              transform="uppercase"
                              color="default"
                              size="h4"
                            >
                              Car Goal
                            </Text>
                          </Box>
                          <Box>
                            <Text
                              weight="bold"
                              transform="uppercase"
                              color="blue"
                              size="h4"
                            >
                              60.12%
                            </Text>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Goal Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                SIP Amount{" "} 
                                <Info color="#fbc03f"></Info>
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                23000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Projected Amount
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                180000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Lumpsum Amount
                              </Text>
                            </Box>
                            <Box></Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Shortfall on Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Topup
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                2000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.m10}>
                            <Button color="yellow"><Link href='/GoalPlanningInitiateGoal'>Initiate Topup</Link></Button>
                          </Box>
                        </GroupBox>
                      </Box>
                    </Card>
                  </Box>
                </Box>
              </TabsContent>
              <TabsContent value="previousgoals" className={style.tabsContent}>
                <Box className="row">
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Card>
                      <Box css={{ p: 20 }}>
                        <GroupBox
                          position="center"
                          className="text-center"
                          horizontal
                        >
                          <Box>
                            <Box
                              className={style.imgbg}
                              display="inlineBlock"
                              background="default"
                            >
                              <img
                                width={54}
                                height={54}
                                src={`${WEB_URL}/FontAwsome (book-reader).png`}
                                alt="logo"
                              />
                              {/* <HomeIcon size="75" color="blue" /> */}
                            </Box>
                          </Box>
                          <Box css={{ mt: 20 }}>
                            <Text
                              weight="extrabold"
                              transform="uppercase"
                              color="default"
                              size="h4"
                            >
                              GR'S education
                            </Text>
                          </Box>
                          <Box>
                            <Text
                              weight="bold"
                              transform="uppercase"
                              color="blue"
                              size="h4"
                            >
                              60.12%
                            </Text>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Goal Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                SIP Amount{" "} 
                                <Info color="#fbc03f"></Info>
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                23000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Projected Amount
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                180000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Lumpsum Amount
                              </Text>
                            </Box>
                            <Box></Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Shortfall on Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Topup
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                2000
                              </Text>
                            </Box>
                          </Box>
                          {/* <Box className={style.m10}>
                            <Button color="yellow"><Link href='/GoalPlanningInitiateGoal'>Initiate Topup</Link></Button>
                          </Box> */}
                        </GroupBox>
                      </Box>
                    </Card>
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Card>
                      <Box css={{ p: 20 }}>
                        <GroupBox
                          position="center"
                          className="text-center"
                          horizontal
                        >
                          <Box>
                            <Box
                              className={style.imgbg}
                              display="inlineBlock"
                              background="default"
                            >
                              <img
                                width={54}
                                height={54}
                                src={`${WEB_URL}/FontAwsome (baby).png`}
                                alt="familyplaninggoal"
                              />
                              {/* <HomeIcon size="75" color="blue" /> */}
                            </Box>
                          </Box>
                          <Box css={{ mt: 20 }}>
                            <Text
                              weight="extrabold"
                              transform="uppercase"
                              color="default"
                              size="h4"
                            >
                              Family Planning Goal
                            </Text>
                          </Box>
                          <Box>
                            <Text
                              weight="bold"
                              transform="uppercase"
                              color="blue"
                              size="h4"
                            >
                              60.12%
                            </Text>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Goal Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                SIP Amount{" "}  
                                <Info color="#fbc03f"></Info>
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                23000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Projected Amount
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                180000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Lumpsum Amount
                              </Text>
                            </Box>
                            <Box></Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Shortfall on Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Topup
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                2000
                              </Text>
                            </Box>
                          </Box>
                          {/* <Box className={style.m10}>
                            <Button color="yellow"><Link href='/GoalPlanningInitiateGoal'>Initiate Topup</Link></Button>
                          </Box> */}
                        </GroupBox>
                      </Box>
                    </Card>
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Card>
                      <Box css={{ p: 20 }}>
                        <GroupBox
                          position="center"
                          className="text-center"
                          horizontal
                        >
                          <Box>
                            <Box
                              className={style.imgbg}
                              display="inlineBlock"
                              background="default"
                            >
                              <img
                                width={54}
                                height={54}
                                src={`${WEB_URL}/FontAwsome (car).png`}
                                alt="car"
                              />
                              {/* <HomeIcon size="75" color="blue" /> */}
                            </Box>
                          </Box>
                          <Box css={{ mt: 20 }}>
                            <Text
                              weight="extrabold"
                              transform="uppercase"
                              color="default"
                              size="h4"
                            >
                              Car Goal
                            </Text>
                          </Box>
                          <Box>
                            <Text
                              weight="bold"
                              transform="uppercase"
                              color="blue"
                              size="h4"
                            >
                              60.12%
                            </Text>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Goal Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                SIP Amount{" "} 
                                <Info color="#fbc03f"></Info>
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                23000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Projected Amount
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                180000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Lumpsum Amount
                              </Text>
                            </Box>
                            <Box></Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Shortfall on Target
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                200000
                              </Text>
                            </Box>
                          </Box>
                          <Box className={style.goalContent}>
                            <Box>
                              <Text color="default" size="h6">
                                Topup
                              </Text>
                            </Box>
                            <Box>
                              <Text color="default" size="h6" weight="bold">
                                2000
                              </Text>
                            </Box>
                          </Box>
                          {/* <Box className={style.m10}>
                            <Button color="yellow"><Link href='/GoalPlanningInitiateGoal'>Initiate Topup</Link></Button>
                          </Box> */}
                        </GroupBox>
                      </Box>
                    </Card>
                  </Box>
                </Box>
              </TabsContent>
            </StyledTabs>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default GoalPlanningPage;
