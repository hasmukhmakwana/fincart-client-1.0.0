import React from 'react'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout from "App/shared/components/Layout";
import OverallReportPage from './components/OverallReportPage';
const OverallReport = () => {
    return (
        <>
            <Layout>
                <PageHeader title="Overall MF Report" rightContent={<></>} />
                <OverallReportPage />
            </Layout>
        </>
    )
}

export default OverallReport