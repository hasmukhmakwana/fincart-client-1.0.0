import React, { useState, useEffect } from "react"
import Box from "@ui/Box/Box";
import DialogModal from '@ui/ModalDialog/ModalDialog';
import Input from "@ui/Input";
import Card from "App/ui/Card/Card";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import { WEB_URL } from "App/utils/constants";
import styles from "App/modules/PassbookModule/Passbook.module.scss"
import Search from 'App/icons/Search';
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { toastAlert } from 'App/shared/components/Layout'
import { encryptData, getUser } from "App/utils/helpers";
import { generateToken } from "App/api/passbook";
import { getToken } from "next-auth/jwt";

const OverallReportPage = () => {
    const local = getUser();
    const [open, setOpen] = useState(false);
    const today = new Date();
    const prevDate = today.getDate() - 30;
    today.setDate(prevDate);
    const prevMonth = today.toLocaleDateString('en-CA');
    const [token, setToken] = useState<any>();
    const [loaderApi, setLoaderApi] = useState(false);
    const todayDate = new Date();
    const yesterdayDate = todayDate.getDate();
    todayDate.setDate(yesterdayDate);
    const yesterday = todayDate.toLocaleDateString('en-CA');
    const [fromDate, setFromDate] = useState(prevMonth);
    const [toDate, setToDate] = useState(yesterday);

    const todayPost = new Date();
    const postDate = todayPost.getDate() + 30;
    todayPost.setDate(postDate);
    const postMonth = todayPost.toLocaleDateString('en-CA');

    const genToken = async (report: any) => {
        try {
            setLoaderApi(true);
            let enc: any = encryptData(local?.basicid, true)
            let result = await generateToken(enc)
            setToken(result?.data);
            switch (report) {
                case "unrealised":
                    window.open(`https://reports.fincart.com/Unrealizedreport.aspx?basicid=${local.basicid}&token=${result?.data}&From_Date=${fromDate}&end_date=${toDate}&userid=${local?.userid}`, '_blank');
                    break;
                case "realised":
                    window.open(`https://reports.fincart.com/Relezedgainreport.aspx?basicid=${local.basicid}&token=${result?.data}&From_Date=${fromDate}&end_date=${toDate}&userid=${local?.userid}`, '_blank');
                    break;
                case "scheme":
                    window.open(`https://reports.fincart.com/SchemeWiseReport.aspx?basicid=${local.basicid}&token=${result?.data}&From_Date=${fromDate}&end_date=${toDate}&userid=${local?.userid}`, '_blank');
                    break;
                case "portfolio":
                    window.open(`https://reports.fincart.com/Portfoliosum.aspx?basicid=${local.basicid}&token=${result?.data}&From_Date=${fromDate}&end_date=${toDate}&userid=${local?.userid}`, '_blank');
                    break;
            }
            setLoaderApi(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoaderApi(false);

        }
    }

    return (
        <>
            <Box className="row">
                <Box className="col-0 col-lg-2"></Box>
                <Box className="col-12 col-lg-8">
                    <Card css={{ p: 25 }} style={{ backgroundColor: "#b3daff", borderRadius: "8px" }}>
                        <Box className="row">
                            <Box className="col-lg-6">
                                <Input
                                    type="date"
                                    label="From Date"
                                    name="ReportGainDate"
                                    color="white"
                                    defaultValue={prevMonth}
                                    max={yesterday}
                                    onChange={(e: any) => {
                                        // console.log(e?.target.value),
                                        setFromDate(e?.target.value)
                                    }}
                                    className="mb-0"
                                />
                            </Box>
                            <Box className="col-lg-6">
                                <Input
                                    type="date"
                                    label="To Date"
                                    name="ReportGainDate"
                                    color="white"
                                    min={fromDate}
                                    defaultValue={yesterday}
                                    max={postMonth}
                                    onChange={(e: any) => {
                                        // console.log(e?.target.value),
                                        setToDate(e?.target.value)
                                    }}
                                />
                            </Box>
                        </Box>
                        <Box className="row p-1 pt-3 justify-content-center">
                            <Box className="col-12 text-center">
                                <Button
                                    className={`col-auto mb-2 py-1`}
                                    color="yellowGroup"
                                    size="md"

                                    onClick={() => {
                                        (async () => {
                                            await genToken("unrealised");

                                        })();
                                    }}
                                >
                                    <Box>
                                        <Text
                                            //@ts-ignore 
                                            size="h6"
                                            //@ts-ignore
                                            color="gray8"
                                            className={`${styles.button}`}
                                        >
                                            Unrealized Gain
                                        </Text>
                                    </Box>
                                </Button>


                                <Button
                                    className={`col-auto mb-2 py-1`}
                                    color="yellowGroup"
                                    size="md"
                                    onClick={() => {

                                        genToken("realised");

                                    }}
                                >
                                    <Box>
                                        <Text
                                            //@ts-ignore 
                                            size="h6"
                                            //@ts-ignore
                                            color="gray8"
                                            className={`${styles.button}`}
                                        >
                                            Realized Gain
                                        </Text>
                                    </Box>
                                </Button>

                                <Button
                                    className={`col-auto mb-2 py-1`}
                                    color="yellowGroup"
                                    size="md"
                                    onClick={() => {
                                        genToken("scheme");



                                    }}
                                >
                                    <Box>
                                        <Text
                                            //@ts-ignore 
                                            size="h6"
                                            //@ts-ignore
                                            color="gray8"
                                            className={`${styles.button}`}
                                        >
                                            Scheme Wise
                                        </Text>
                                    </Box>
                                </Button>

                                <Button
                                    className={`col-auto mb-2 py-1`}
                                    color="yellowGroup"
                                    size="md"
                                    onClick={() => {
                                        genToken("portfolio");

                                    }}
                                >
                                    <Box>
                                        <Text
                                            //@ts-ignore 
                                            size="h6"
                                            //@ts-ignore
                                            color="gray8"
                                            className={`${styles.button}`}
                                        >
                                            Portfolio Sum
                                        </Text>
                                    </Box>
                                </Button>

                            </Box>
                        </Box>
                    </Card>
                </Box>
                <Box className="col-0 col-lg-2"></Box>
            </Box>
        </>
    )
}

export default OverallReportPage