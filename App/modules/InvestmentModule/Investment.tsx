import React, { useEffect, useState } from "react";
import Layout, { toastAlert } from 'App/shared/components/Layout'
import InvestmentPage from "./components/InvestmentPage";
import {
  viewInvestmentProfile,
} from "App/api/profile"

import {
  getMandateMaster,
  getAllMemberPurchaseDetails
} from "App/api/mandate"
import useInvestmentStore from "./store";
import { encryptData, getUser } from "App/utils/helpers";
import { keys } from "lodash";

const Investment = () => {
  const localUser: any = getUser();
  const [user, setUser] = useState<any>(localUser?.basicid);
  const {
    investor,
    setLoader,
    setInvestor,
    setInvestmentProfile,
    setMOHList,
    setMember,
    setMOHmember,
  } = useInvestmentStore();

  const fetchInvestmentProfile = async () => {
    try {
      setLoader(true);
      const obj: any = {
        BasicId: user,
        All: 'N'
      };
      if (user === "null" || user === undefined) {
        return;
      }
      const enc: any = encryptData(obj);
      const result: any = await viewInvestmentProfile(enc);
      setInvestmentProfile(result?.data?.profile);
      setLoader(false);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false)
    }
  }

  const reloadInvestmentProfile = async (data: any) => {
    // data = data || {};
    //  console.log("reloadInvestmentProfile: ", data);
    //  return;
    try {
      setLoader(true);
      const obj: any = {
        BasicId: data,
        All: 'N'
      };
      if (data === "null" || data === undefined) {
        return;
      }
      const enc: any = encryptData(obj);
      const result: any = await viewInvestmentProfile(enc);
      setInvestmentProfile(result?.data?.profile);
      setLoader(false);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false)
    }
  }


  const fetchMandateMaster = async () => {
    try {
      let result = await getMandateMaster();
      //  console.log(result?.data[5]);
      setMOHList(result?.data[5]?.Value);
      //  console.log(result?.data[5]?.Value);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  const fetchMemberList = async () => {
    try {
      const enc: any = encryptData({
        basicId: user,
        fundid: "0",
      });
      // console.log(enc);

      let result = await getAllMemberPurchaseDetails(enc);
      // console.log(result?.data);
      setMOHmember(result?.data?.memberList || []);
      setMember(result?.data?.memberList?.filter((record: any) => record?.basicid === user)[0] || []);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  useEffect(() => {
    if (user !== null || user !== undefined || user !== "") {
      (async () => {
        await fetchInvestmentProfile();
        await fetchMandateMaster();
        await fetchMemberList();
        await setInvestor(user)
      })();
    }
    return () => { };
  }, [user]);

  useEffect(() => {
    (async () => {

      await fetchInvestmentProfile();
      await fetchMandateMaster();
      await fetchMemberList();

      setInvestor(localUser?.basicid)
      setUser(localUser?.basicid);
    })();
  }, []);
  return (
    <Layout>
      <InvestmentPage userID={setUser} reloadCall={reloadInvestmentProfile} />
    </Layout>
  )
}

export default (Investment);