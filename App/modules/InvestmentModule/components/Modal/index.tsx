export { default as AddInvestment } from './InvestmentModal'
export { default as LinkedMandate } from './LinkedMandateModal'
export { default as LinkedTransaction } from './LinkedTransactionModal'
export { default as UpdateInvestment } from './UpdateInvestmentModal'