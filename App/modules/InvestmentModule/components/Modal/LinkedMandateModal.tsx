import Box from '@ui/Box/Box'
import React, { useEffect } from 'react'
import Text from "@ui/Text/Text";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/SimpleGrid/DataGrid.styles";
import useInvestmentStore from "../../store";
import Loader from "App/shared/components/Loader";
import style from "../../Investment.module.scss";

// @ts-ignore
const LinkedMandateModal = ({ loadManStatus }) => {
  const {
    mandateList
  } = useInvestmentStore();

  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{borderRadius: "15px 15px 0 0"}}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}>
            {" "}
            Linked Mandate - {mandateList.length}
            {/*  */}
          </Text>
        </Box>
        <Box className={`modal-body p-3 ${style.scrollit}`}  >
          <Box>
            <Table className={`text-capitalize table table-striped`} css={{ color: "var(--colors-blue1)" }} >
              <ColumnParent className="text-capitalize sticky">
                <ColumnRow css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}>
                  <Column><Text size="h5">Mandate Id</Text></Column>
                  <Column><Text size="h5">Invest A/C</Text></Column>
                  <Column><Text size="h5">Verification</Text></Column>
                  <Column><Text size="h5">Bank</Text></Column>
                  <Column><Text size="h5">Status</Text></Column>
                  <Column><Text size="h5">Validity</Text></Column>
                  <Column><Text size="h5">Day Limit</Text></Column>
                  <Column><Text size="h5">UMRN</Text></Column>
                </ColumnRow>
              </ColumnParent>
              <DataParent css={{ textAlign: "center" }} >
                {loadManStatus ? (
                  <DataCell colSpan={8}>
                    <Loader />
                  </DataCell>
                ) : (
                  <>
                    {mandateList.map((record, index) => {
                      console.log(record,"man record");
                      
                      return (
                        <DataRow>
                          <DataCell >
                            <Box>
                              <Text className={`${style.cellStyle}`} css={{ textAlign:"left" }}>
                                {" "}
                                {record?.MandateID}<br/>
                                {" "}
                                <span style={{fontSize:"0.65rem", color:"grey"}}>[{record?.mandatetype === "P" ? "Physical" : "E-Mandate" }]</span>
                              </Text>
                            </Box>
                          </DataCell>
                          <DataCell >
                            <Box>
                              <Text className={`${style.cellStyle}`} css={{ textAlign:"left" }}>
                                {" "}
                                {record?.profilename}<br/>
                                {" "}
                                <span style={{fontSize:"0.65rem", color:"grey"}}>[{record?.moh}]</span>
                              </Text>
                            </Box>
                          </DataCell>
                          <DataCell >
                            <Text className={`${style.cellStyle}`}>
                              {" "}
                              {record?.Upload_status}
                              {" "}
                            </Text>
                          </DataCell>
                          <DataCell >
                            <Text className={`${style.cellStyle}`}>
                              {" "}
                              {record?.Bank}
                              {" "}
                              <span style={{fontSize:"0.65rem", color:"grey"}}>[{record?.AccountNo}]</span>
                            </Text>
                          </DataCell>
                          <DataCell>
                            <Text className={`${style.cellStyle}`}>
                              {" "}
                              {record?.Active === "N" ? "In Active" : "Active"}
                              {" "}
                            </Text>
                          </DataCell>
                          <DataCell >
                            <Text className={`${style.cellStyle}`} css={{ textAlign: "center"}} >
                              {" "}
                              {record?.Until_cancel === "Y" ? "Until Cancel" : <> {record?.ToMonth} / {record?.ToYear}</>}
                              {" "}
                            </Text>
                          </DataCell>
                          <DataCell >
                          <Text className={`${style.cellStyle}`} css={{ textAlign: "center"}} >
                              {" "}
                              &#x20B9; {record?.PerDayLimit}
                              {" "}
                            </Text>
                          </DataCell>
                          <DataCell >
                            <Text className={`${style.cellStyle}`}>
                              {" "}
                              <span style={{fontSize:"0.65rem", color:"grey"}}>{record?.UMRN_NO}</span>
                              
                              {" "}
                            </Text>
                          </DataCell>
                        </DataRow>
                      )
                    })}
                  </>
                )}
              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default LinkedMandateModal
