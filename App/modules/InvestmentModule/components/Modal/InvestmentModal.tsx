import React, { useState, useEffect } from "react";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import style from "../../Investment.module.scss";
import { toastAlert } from 'App/shared/components/Layout'
import SelectMenu from "@ui/Select/Select";
import Checkbox from "@ui/Checkbox/Checkbox";
import { encryptData, getUser } from "App/utils/helpers";
import {
  modifyInvestmentProfile
} from "App/api/profile"
import useInvestmentStore from "../../store";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";

const ObjectSchemaList = {
  mandatory: {
    MOH: Yup.string()
      .required("Holding Mode is required"),
    FirstApp: Yup.string()
      .required("First Applicant is required"),
  },
  TwoApp: {
    SecApp: Yup.string()
      .required("Second Applicant is required")
      .nullable(),
  },
  ThreeApp: {
    ThirdApp: Yup.string()
      .required("Third Applicant is required")
      .nullable(),
  }
}

export type VerifyInvestmentTypes = {
  MOH: string;
  FirstApp: string;
  SecApp: string;
  ThirdApp: string;
  AddThird: boolean;
};

interface StoreTypes {
  VerifyInvestment: VerifyInvestmentTypes;
  setVerifyInvestment: (payload: VerifyInvestmentTypes) => void;
}

type SaveTypes = {
  setAddModalOpen: (values: boolean) => void;
  reload: (values: string) => void;
};

const InvestmentModal = ({ setAddModalOpen, reload }: SaveTypes) => {

  const {
    mohList,
    mohMember,
    investor,
    setInvestor
  } = useInvestmentStore();

  const useInvestStore = create<StoreTypes>((set) => ({
    //* initial state
    VerifyInvestment: {
      MOH: mohList[0].Text,
      FirstApp: investor,
      SecApp: "",
      ThirdApp: "",
      AddThird: false,
    },
    setVerifyInvestment: (payload) =>
      set((state) => ({
        ...state,
        VerifyInvestment: payload,
      })),
  }))
  const { VerifyInvestment, loading } = useInvestStore((state: any) => state);
  const [validationSchema, setValidationSchema] = useState(ObjectSchemaList.mandatory);
  const [mohLocal, setMohLocal] = useState(mohList[0].Text);
  const [trdApp, setTrdApp] = useState(VerifyInvestment.AddThird);
  const [memList, setMemList] = useState<any>([]);
  const [lastmemList, setLastMemList] = useState<any>();
  const [secApp, setSecApp] = useState<any>("");
  const createInvestment = async (data: any = null) => {
    // data = data || {};
    // console.log("createInvestment: ", data);
    // return;
    try {
      const obj: any =
      {
        Id: 0,
        FirstApplBasicId: data?.FirstApp,
        SecondApplBasicId: data?.SecApp === undefined || data?.SecApp === "" ? null : data?.SecApp,
        ThirdApplBasicId: data?.ThirdApp === undefined || data?.ThirdApp === "" ? null : data?.ThirdApp,
        holdingMode: data?.MOH,
        Request_Type: "Create",
      };
      const enc: any = encryptData(obj);
      //console.log(obj);
      //console.log(enc);
      const result: any = await modifyInvestmentProfile(enc);
      //console.log(result.status);
      toastAlert("success", "Investment Account added successfully");
      setAddModalOpen(false);
      reload(data.FirstApp)
      setInvestor(data.FirstApp)
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }
  useEffect(() => {
    let obj = ObjectSchemaList.mandatory;
    if (mohLocal !== "Single") {
      setMemList(mohMember.filter((e: any) => e.basicid !== investor));
      obj = {
        ...obj,
        ...ObjectSchemaList.TwoApp
      }
      if (trdApp) {
        setLastMemList(memList.filter((e: any) => e.basicid !== secApp))
        obj = {
          ...obj,
          ...ObjectSchemaList.ThreeApp
        }
      }
    }
    // console.log(obj,"Obj")
    setValidationSchema(obj)
    return () => { }
  }, [mohLocal, secApp, trdApp])
  return (
    <>
      
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          <Text
            //@ts-ignore 
            size="h4"
            css={{ color: "var(--colors-blue1)" }}>
            {" "}
            Add Investment Account
          </Text>
        </Box>
        <Box className="modal-body p-3">
          <Formik
            initialValues={VerifyInvestment}
            validationSchema={Yup.object().shape(validationSchema)}
            enableReinitialize
            onSubmit={(values, { resetForm }) => {
               createInvestment(values);
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (
              <>
                <Box>
                  <SelectMenu
                    name="MOH"
                    menuPosition="absolute"
                    //  value="Risk"
                    items={mohList || []}
                    label={"Mode Of Holding"}
                    bindValue={"Text"}
                    bindName={"Value"}
                    value={values.MOH}
                    onChange={(e: any) => {
                      handleChange;
                      setFieldValue("MOH", e?.Text);
                      setMohLocal(e.Text)
                      if (e.Text === "Single") {
                        setFieldValue("SecApp", null);
                        setFieldValue("ThirdApp", null);
                        setFieldValue("AddThird", false);
                        setTrdApp(false);
                      }
                    }}
                    // @ts-ignore
                    error={submitCount ? errors.MOH : null}
                  />
                </Box>
                <Box>
                  <SelectMenu
                    name="FirstApp"
                    disabled
                    menuPosition="absolute"
                    label={"First Applicant"}
                    items={mohMember || []}
                    bindValue={"basicid"}
                    bindName={"memberName"}
                    onChange={(e) => { setFieldValue("FirstApp", investor) }}
                    value={investor}
                    // @ts-ignore
                    error={submitCount ? errors.FirstApp : null}
                  />
                </Box>
                {values.MOH === "Single" || values.MOH === "" ? "" : <>
                  <Box>
                    <SelectMenu
                      name="SecApp"
                      menuPosition="absolute"
                      label={"Second Applicant"}
                      items={memList || []}
                      bindValue={"basicid"}
                      bindName={"memberName"}
                      onChange={(e) => { setFieldValue("SecApp", e?.basicid); setSecApp(e?.basicid) }}
                      value={values.SecApp}
                      //@ts-ignore
                      error={submitCount ? errors.SecApp : null}
                    />
                  </Box>
                  {memList.length > 1 && values.SecApp !== "" ? <>
                    <Box>
                      <Box className="row">
                        <Box className="col-9 col-md-9 col-lg-10">
                          <SelectMenu
                            name="ThirdApp"
                            menuPosition="absolute"
                            label={"Third Applicant"}
                            items={lastmemList || []}
                            bindValue={"basicid"}
                            bindName={"memberName"}
                            onChange={(e) => setFieldValue("ThirdApp", e?.basicid)}
                            value={values.ThirdApp}
                            //@ts-ignore
                            error={trdApp ? errors.ThirdApp : null}
                            disabled={!values.AddThird}
                          />
                        </Box>
                        <Box className="col-auto my-auto align-content-end">
                          <Checkbox
                            label="Add"
                            id="AddThird"
                            name="AddThird"
                            checked={values?.AddThird}
                            disabled={values.SecApp !== "" ? false : true}
                            onChange={(e: any) => {
                              setFieldValue("AddThird", e?.target?.checked);
                              setFieldValue("ThirdApp", null);
                              setTrdApp(e?.target?.checked);
                            }}
                          />
                        </Box>
                      </Box>
                    </Box>
                  </>
                    : ""}
                </>}
                <Box className="row justify-content-end">
                  <Box className="col-auto pe-1">
                    <Button
                      type="submit"
                      color="yellow"
                      onClick={() => setAddModalOpen(false)}
                    // loading={loading}
                    >
                      Cancel
                    </Button>
                  </Box>
                  <Box className="col-auto ps-0">
                    <Button
                      type="submit"
                      color="yellow"
                      onClick={() => { handleSubmit() }}
                      loading={loading}
                    >
                      Confirm
                    </Button>
                  </Box>
                </Box>
              </>)}</Formik>
        </Box>
      </Box>
    </>
  );
};
export default InvestmentModal;
