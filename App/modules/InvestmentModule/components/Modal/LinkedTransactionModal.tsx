import Box from '@ui/Box/Box'
import React, { useEffect } from 'react'
import Text from "@ui/Text/Text";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/SimpleGrid/DataGrid.styles";
import useInvestmentStore from "../../store";
import Loader from "App/shared/components/Loader";
import style from "../../Investment.module.scss";

// @ts-ignore
const LinkedTransactionModal = ({ loadTranStatus }) => {
  const {
    transactionList
  } = useInvestmentStore();

  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{borderRadius: "15px 15px 0 0"}}>
          {/* @ts-ignore */}
          <Text size="h4" css={{ color: "var(--colors-blue1)" }}>
            {" "}
            Linked Transaction - {transactionList.length}
          </Text>
        </Box>
        <Box className={`modal-body p-3 ${style.scrollit}`}>
          <Box css={{ overflow: "auto" }}>
            <Table className="text-capitalize table table-striped" css={{ color: "var(--colors-blue1)" }}>
              <ColumnParent className="text-capitalize">
                <ColumnRow css={{ background: "var(--colors-blue1)", color: "#fff", textAlign: "center" }}>
                  {/* @ts-ignore */}
                  <Column><Text size="h5">Folio No.</Text></Column>
                  {/* @ts-ignore */}
                  <Column><Text size="h5">Scheme Name</Text></Column>
                  {/* @ts-ignore */}
                  <Column><Text size="h5">Transaction Type</Text></Column>
                </ColumnRow>
              </ColumnParent>
              <DataParent css={{ textAlign: "center" }}>
                {loadTranStatus ? (
                  <DataCell colSpan={5}>
                    <Loader />
                  </DataCell>
                ) : (
                  <>
                    {transactionList.map((record, index) => {
                      return (
                        <>
                          <DataRow >
                            <DataCell>
                              <Text css={{ color: "var(--colors-blue1)" }}>
                                {" "}
                                {record?.FolioNo}
                                {" "}
                              </Text>
                              {/* <span> {record?.SchemeID}</span> */}
                            </DataCell>
                            <DataCell>
                              <Text css={{ color: "var(--colors-blue1)" }}>
                                {" "}
                                {record?.SchemeName}
                                {" "}
                              </Text>
                            </DataCell>
                            <DataCell>
                              <Text css={{ color: "var(--colors-blue1)" }}>
                                {" "}
                                {record?.Trxn_Type}
                                {" "}
                              </Text>
                            </DataCell>
                          </DataRow>
                        </>
                      )
                    })}
                  </>
                )}
              </DataParent>
            </Table>
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default LinkedTransactionModal
