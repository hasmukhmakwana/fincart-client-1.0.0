import React, { useEffect, useState } from "react";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import style from "../Investment.module.scss";
import { toastAlert } from 'App/shared/components/Layout'
import SelectMenu from "@ui/Select/Select";
import Checkbox from "@ui/Checkbox/Checkbox";
import useInvestmentStore from "../../store";
import { encryptData, getUser } from "App/utils/helpers";
import {
  modifyInvestmentProfile,
  deleteInvestmentProfile
} from "App/api/profile"
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";

const investmentModalObj = {
  MOH: Yup.string()
    .required("Amount is required"),
  FirstApp: Yup.string()
    .required("Amount is required"),
  SecApp: Yup.string(),
  ThirdApp: Yup.string()
    .nullable(),
}

const ObjectSchemaList = {
  mandatory: {
    MOH: Yup.string()
      .required("Holding Mode is required"),
    FirstApp: Yup.string()
      .required("First Applicant is required"),
  },
  TwoApp: {
    SecApp: Yup.string()
      .required("Second Applicant is required")
      .nullable(),
  },
  ThreeApp: {
    ThirdApp: Yup.string()
      .required("Third Applicant is required")
      .nullable(),
  }
}

// let validationSchema = Yup.object().shape(investmentModalObj);

export type VerifyInvestmentTypes = {
  MOH: string;
  FirstApp: string;
  SecApp: string;
  ThirdApp: string;
  AddThird: boolean;
  Deactivate: boolean;
};

interface StoreTypes {
  VerifyInvestment: VerifyInvestmentTypes;
  setVerifyInvestment: (payload: VerifyInvestmentTypes) => void;
}

// @ts-ignore
const UpdateInvestmentModal = ({ setEditModalOpen, editBody, reload }) => {
  const useInvestStore = create<StoreTypes>((set) => ({
    //* initial state
    VerifyInvestment: {
      MOH: editBody.Moh,
      FirstApp: editBody.FirstApplicantBasicId,
      SecApp: editBody?.SecondApplicantBasicId,
      ThirdApp: editBody?.ThirdApplicantBasicId,
      AddThird: editBody?.ThirdApplicantBasicId === null ||
        editBody?.ThirdApplicantBasicId === "" ? false : true,
      Deactivate: false,
    },
    setVerifyInvestment: (payload) =>
      set((state) => ({
        ...state,
        VerifyInvestment: payload,
      })),
  }))
  const { VerifyInvestment, loading } = useInvestStore((state: any) => state);
  const {
    mohList,
    mohMember,
    investor
  } = useInvestmentStore();

  const [validationSchema, setValidationSchema] = useState({});
  const [mohLocal, setMohLocal] = useState(VerifyInvestment.MOH);
  const [trdApp, setTrdApp] = useState(VerifyInvestment.ThirdApp);
  const [deactive, setDeactive] = useState(false);
  const [memList, setMemList] = useState<any>([]);
  const [lastmemList, setLastMemList] = useState<any>([]);
  const [secApp, setSecApp] = useState<any>(VerifyInvestment.SecApp);
  const updateInvestment = async (data: any = null) => {
    // data = data || {};
    // console.log("UpdateInvestment: ", data);
    // return;
    try {

      if (data.Deactivate) {
        const enc: any = encryptData(parseInt(editBody.Id));
        console.log(enc);
        const result: any = await deleteInvestmentProfile(enc);
        console.log(result);
        toastAlert("success", "Investment Account deleted successfully");
        setEditModalOpen(false);
        reload(investor)
        //setInvestor(investor)
      }
      else {
        const obj: any =
        {
          Id: editBody.Id,
          FirstApplBasicId: data?.FirstApp,
          SecondApplBasicId: data?.SecApp === undefined || data?.SecApp === "" ? null : data?.SecApp,
          ThirdApplBasicId: data?.ThirdApp === undefined || data?.ThirdApp === "" ? null : data?.ThirdApp,
          holdingMode: data?.MOH,
          Request_Type: "Update",
        };
        const enc: any = encryptData(obj);
        console.log(obj);
        console.log(enc);
        const result: any = await modifyInvestmentProfile(enc);
        console.log(result.status);
        toastAlert("success", "Investment Account details updated successfully");
        setEditModalOpen(false);
        reload(investor)
      }
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }


  useEffect(() => {
    let obj = ObjectSchemaList.mandatory;
    if (!deactive) {
      if (mohLocal !== "Single") {
        setMemList(mohMember.filter((e: any) => e.basicid !== investor));
        obj = {
          ...obj,
          ...ObjectSchemaList.TwoApp
        }
        if (trdApp) {
          setLastMemList(memList.filter((e: any) => e.basicid !== secApp))
          obj = {
            ...obj,
            ...ObjectSchemaList.ThreeApp
          }
        }
      }
    } else {
      obj = ObjectSchemaList.mandatory;
    }
    setValidationSchema(obj)
    // console.log(obj)
    // console.log(deactive)

    return () => { }
  }, [mohLocal, secApp, trdApp, deactive])

  return (
    <>
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          {/* @ts-ignore */}
          <Text css={{ color: "var(--colors-blue1)" }}>
            {" "}
            Update Investment Account
          </Text>
        </Box>
        <Box className="modal-body p-3">
          <Formik
            initialValues={VerifyInvestment}
            validationSchema={Yup.object().shape(validationSchema)}
            enableReinitialize
            onSubmit={(values, { resetForm }) => {
              updateInvestment(values);
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              errors,
              touched,
              setFieldValue,
              submitCount
            }) => (
              <>
                <Box>
                  <SelectMenu
                    name="MOH"
                    menuPosition="absolute"
                    //  value="Risk"
                    items={mohList || []}
                    label={"Mode Of Holding"}
                    bindValue={"Text"}
                    bindName={"Value"}
                    value={values.MOH}
                    onChange={(e: any) => {
                      setFieldValue("MOH", e?.Text);
                      setMohLocal(e.Text)
                      if (e.Text === "Single") {
                        setFieldValue("SecApp", null);
                        setFieldValue("ThirdApp", null);
                        setFieldValue("AddThird", false);
                        setTrdApp(false);
                      }
                    }}
                    disabled={values.Deactivate}
                    // @ts-ignore
                    error={submitCount ? errors.MOH : null}
                  />
                </Box>
                <Box>
                  <SelectMenu
                    name="FirstApp"
                    menuPosition="absolute"
                    label={"First Applicant"}
                    items={mohMember || []}
                    bindValue={"basicid"}
                    bindName={"memberName"}
                    onChange={(e) => { setFieldValue("FirstApp", e?.basicid) }}
                    value={values.FirstApp}
                    disabled={values.Deactivate}
                    // @ts-ignore
                    error={submitCount ? errors.FirstApp : null}
                  />
                </Box>
                {values.MOH === "Single" || values.MOH === "" ? "" : <>
                  <Box>
                    <SelectMenu
                      name="SecApp"
                      menuPosition="absolute"
                      label={"Second Applicant"}
                      items={memList || []}
                      bindValue={"basicid"}
                      bindName={"memberName"}
                      onChange={(e) => { setFieldValue("SecApp", e?.basicid); setSecApp(e?.basicid) }}
                      value={values.SecApp}
                      disabled={values.Deactivate}
                      //@ts-ignore
                      error={submitCount ? errors.SecApp : null}
                    />
                    {/* </Box> */}
                  </Box>
                  {memList.length > 1 && values.SecApp !== "" ? <>
                    <Box>
                      <Box className="row">
                        <Box className="col-9 col-md-9 col-lg-10">
                          <SelectMenu
                            name="ThirdApp"
                            menuPosition="absolute"
                            label={"Third Applicant"}
                            items={lastmemList || []}
                            bindValue={"basicid"}
                            bindName={"memberName"}
                            onChange={(e) => setFieldValue("ThirdApp", e?.basicid)}
                            value={values.ThirdApp}
                            disabled={values.Deactivate || !values.AddThird ? true : false}
                            //@ts-ignore
                            error={trdApp ? errors.ThirdApp : null}
                          />
                        </Box>
                        <Box className="col-auto my-auto align-content-end">
                          <Checkbox
                            label="Add"
                            id="AddThird"
                            name="AddThird"
                            checked={values?.AddThird}
                            onChange={(e: any) => {
                              setFieldValue("AddThird", e?.target?.checked);
                              setFieldValue("ThirdApp", null);
                              setTrdApp(e?.target?.checked);
                            }}
                            disabled={values.Deactivate}
                          />
                        </Box>
                      </Box>
                    </Box>
                  </>
                    : ""}
                </>}
                {editBody?.isDelete !== "N" ? <>
                  <Box className="row justify-content-between">
                    <Box className="col-6">
                      <Box>
                        <Checkbox
                          label="Deactivate"
                          id="Deactivate"
                          name="Deactivate"
                          checked={values?.Deactivate}
                          onChange={(e: any) => {
                            setFieldValue("Deactivate", e?.target?.checked);
                            setDeactive(e?.target?.checked);
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </> : ""}
                <Box className="row justify-content-end">
                  <Box className="col-auto pe-1">
                    <Button
                      type="submit"
                      color="yellow"
                      onClick={() => setEditModalOpen(false)}
                    // loading={loading}
                    >
                      Cancel
                    </Button>
                  </Box>
                  <Box className="col-auto ps-0">
                    <Button
                      type="submit"
                      color="yellow"
                      name="update"
                      onClick={() => { handleSubmit() }}
                      loading={loading}
                    >
                      Confirm
                    </Button>
                  </Box>
                </Box>
              </>)}</Formik>
        </Box>
      </Box>
    </>
  );
}

export default UpdateInvestmentModal
