import React, { useEffect, useState } from "react";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import { toastAlert } from 'App/shared/components/Layout'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import style from "../Investment.module.scss";
import Text from "@ui/Text/Text";
import SelectMenu from "@ui/Select/Select";
import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/SimpleGrid/DataGrid.styles";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import Loader from "App/shared/components/Loader";
import EyeIcon from "App/icons/EyeIcon";
import PencilFill from "App/icons/PencilFill";
import useInvestmentStore from "../store";
import { encryptData, getUser } from "App/utils/helpers";
import {
  mandateByProfileID,
  mandateByTransactionID,
} from "App/api/profile"
import {
  AddInvestment,
  LinkedMandate,
  LinkedTransaction,
  UpdateInvestment
} from "./Modal";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import useRegistrationStore from "App/modules/RegistrationModule/store";
import { useRouter } from "next/router";
export type InvestProfileTypes = {
  investorID: string;
};

interface StoreTypes {
  VerifyProfile: InvestProfileTypes;
}


//@ts-ignore
const InvestmentPage = ({ userID, reloadCall }) => {//, reloadPage
  const router = useRouter();
  // const user: any = getUser();
  const {
    loading,
    investor,
    investmentProfile,
    member,
    mohMember,
    setMandateList,
    setTransactionList,
    setMember,
    // setLoader,
    // setInvestmentProfile
  } = useInvestmentStore();
  const useInvestStore = create<StoreTypes>((set) => ({
    //* initial state
    VerifyProfile: {
      investorID: investor,
    }
  }))

  const { VerifyProfile } = useInvestStore((state: any) => state);

  const [open, setOpen] = useState(false);
  const [openMandate, setOpenMandate] = useState(false);
  const [openTrans, setOpenTrans] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [loaderMan, setLoaderMan] = useState(false);
  const [loaderTran, setLoaderTran] = useState(false);
  const [editInvestId, setEditInvestId] = useState<any>();
  const [cafStatus, setCafStatus] = useState<any>("");
  const { setBasicID, setRegiType } = useRegistrationStore();
  // @ts-ignore
  const fetchMandateList = async (profileId) => {
    try {
      setLoaderMan(true);
      const enc: any = encryptData(parseInt(profileId));
      let result = await mandateByProfileID(enc);
      setMandateList(result?.data?.Mandate_Details);
      setLoaderMan(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };
  // @ts-ignore
  const fetchTransactionList = async (profileId) => {
    try {
      setLoaderTran(true);
      const enc: any = encryptData(parseInt(profileId));
      let result: any = await mandateByTransactionID(enc);
      setTransactionList(result?.data?.TransactionList);
      setLoaderTran(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  const reload = async (data: any = null) => {
    data = data || {};
    userID(data.investorID)
  }

  return (
    <>
      <PageHeader
        title="Investment Account"
        rightContent={<></>} />
      {/* MODAL */}
      <DialogModal
        open={open}
        setOpen={setOpen}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "40%" },
        }}
      >
        <AddInvestment setAddModalOpen={setOpen} reload={reloadCall} />
      </DialogModal>
      <Box background="gray">
        <Box className="container-fluid">
          <Box className="row">
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Card css={{ p: 20 }}>
                <Box>
                  <Box className="row justify-content-between">
                    <Box className="col-10 col-md-5 col-lg-3">
                      <Formik
                        initialValues={VerifyProfile}
                        // validationSchema={Yup.object().shape(validationSchema)}
                        enableReinitialize
                        onSubmit={(values, { resetForm }) => {
                          reload(values);
                        }}
                      >
                        {({
                          values,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,
                          errors,
                          touched,
                          setFieldValue,
                          submitCount
                        }) => (
                          <Box>
                            <SelectMenu
                              label="Investor"
                              name="investorID"
                              items={mohMember || []}
                              bindValue={"basicid"}
                              bindName={"memberName"}
                              onChange={(e: any) => {
                                setFieldValue("investorID", e?.basicid);
                                handleSubmit(e);
                                // console.log(e);
                                setMember(e);
                              }}
                              value={values.investorID}
                            />
                          </Box>
                        )}</Formik>
                    </Box>
                    {/* {console.log(mohMember,"success")} */}
                    {member?.CAF_Status === 'success' ? <>
                      <Box className="col-10 col-md-5 col-lg-3 d-flex mt-0 justify-content-end align-items-center">
                        <Button
                          color="yellow"
                          className={style.mt10}
                          onClick={setOpen}
                        >
                          Add Investment Account
                        </Button>
                      </Box>
                    </> : <>
                      <Box className="col-10 col-md-5 col-lg-3 d-flex mt-0 justify-content-end align-items-center">
                        {/* <Box className="text-end p-1 justify-content-end"> */}
                        <Button
                          color="yellow"
                          onClick={() => {
                            setRegiType("PENDING");
                            setBasicID(member?.basicid);
                            router.push("/Registration");
                          }}
                        >
                          Complete Registration
                        </Button>
                        {/* </Box> */}
                      </Box>
                    </>}
                  </Box>
                </Box>
                <Box className="table-responsive mt-3" css={{ borderRadius: "8px" }}>
                  <Box css={{ overflow: "auto" }}>
                    <Table>
                      <ColumnParent style={{ borderRadius: "8px" }}
                        className={`${style.borderRadiusForTable} border border-bottom`}
                        //@ts-ignore
                        className="text-capitalize"
                      >
                        <ColumnRow className={`${style.borderRadiusForTable}`} css={{ color: "var(--colors-blue1)", textAlign: "center", borderRadius: "8px" }}>
                          <Column>First Applicant</Column>
                          <Column>Second Applicant</Column>
                          <Column>Third Applicant</Column>
                          <Column>Mode of Holding</Column>
                          <Column>Linked Mandates</Column>
                          <Column>Linked Transactions</Column>
                          <Column>Action</Column>
                        </ColumnRow>
                      </ColumnParent>
                      <DataParent>
                        {loading ? (
                          <DataCell colSpan={7} className="text-center">
                            <Loader />
                          </DataCell>
                        ) : (
                          <>
                            {investmentProfile.length === 0 ?
                              <>
                                <DataCell colSpan={7} className="text-center">
                                  No Investment Account
                                </DataCell>
                              </> :
                              <>
                                {
                                  investmentProfile.map((record: any, index) => {

                                    return (
                                      <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                        <DataCell className="text-left">
                                          {/* @ts-ignore */}
                                          <Text size="h5" css={{ color: "var(--colors-blue1)" }}>
                                            {" "}
                                            {record?.FirstApplicant}
                                            {" "}
                                          </Text>
                                          <span>{record?.FirstPan}</span>
                                        </DataCell>
                                        <DataCell>
                                          {/* @ts-ignore */}
                                          <Text size="h5" css={{ color: "var(--colors-blue1)" }}>
                                            {" "}
                                            {record?.SecondApplicant}
                                            {" "}
                                          </Text>
                                          <span>{record?.SecondPan}</span>
                                        </DataCell>
                                        <DataCell>
                                          {/* @ts-ignore */}
                                          <Text size="h5" css={{ color: "var(--colors-blue1)" }}>
                                            {" "}
                                            {record?.ThirdApplicant}
                                            {" "}
                                          </Text>
                                          <span>{record?.ThirdPan}</span>
                                        </DataCell>
                                        <DataCell >
                                          {/* @ts-ignore */}
                                          <Text size="h5"
                                            css={{
                                              color: "var(--colors-blue1)",
                                              textAlign: "center",
                                            }}
                                          >
                                            {" "}
                                            {record?.Moh}
                                            {" "}
                                          </Text>
                                        </DataCell>
                                        <DataCell >
                                          {/* @ts-ignore */}
                                          {record?.TotalActiveMandate >= "1" ?
                                            // @ts-ignore
                                            <Text size="h5"
                                              css={{
                                                color: "var(--colors-blue1)",
                                                textAlign: "center",
                                                cursor: "pointer",
                                              }}
                                            >
                                              {" "}
                                              <a onClick={() => { setOpenMandate(true); fetchMandateList(record?.Profileid) }}>{record?.TotalActiveMandate}{"  "}<EyeIcon color="var(--colors-blue1)" /></a>{" "}
                                            </Text>
                                            :
                                            // @ts-ignore
                                            <Text size="h5"
                                              css={{
                                                color: "var(--colors-blue1)",
                                                textAlign: "center",
                                                cursor: "pointer",
                                              }}
                                            >
                                              {" "}
                                              <a >{record?.TotalActiveMandate}</a>{" "}
                                            </Text>
                                          }
                                        </DataCell>
                                        <DataCell >
                                          {record?.TotalActiveTransaction >= "1" ?
                                            // @ts-ignore
                                            <Text size="h5"
                                              css={{
                                                color: "var(--colors-blue1)",
                                                textAlign: "center",
                                                cursor: "pointer",
                                              }}
                                            >
                                              {" "}
                                              <a onClick={() => { setOpenTrans(true); fetchTransactionList(record?.Profileid) }}>{record?.TotalActiveTransaction} {"  "}<EyeIcon color="var(--colors-blue1)" /></a>{" "}
                                            </Text>
                                            :
                                            // @ts-ignore
                                            <Text size="h5"
                                              css={{
                                                color: "var(--colors-blue1)",
                                                textAlign: "center",
                                                cursor: "pointer",
                                              }}
                                            >
                                              {" "}
                                              <a >{record?.TotalActiveTransaction}</a>{" "}
                                            </Text>
                                          }
                                        </DataCell>
                                        <DataCell>
                                          {record?.isupdate !== "N" ? <>
                                            {/* @ts-ignore */}
                                            <Text size="h5"
                                              css={{
                                                color: "var(--colors-blue1)",
                                                textAlign: "center",
                                              }}
                                            >
                                              {" "}
                                              <a onClick={() => { setOpenEdit(true); setEditInvestId(record); }}><PencilFill color="var(--colors-blue1)" /></a>{" "}
                                            </Text>
                                          </> : <></>}
                                        </DataCell>
                                      </DataRow>
                                    )
                                  })
                                }
                              </>
                            }</>
                        )}
                      </DataParent>
                    </Table>
                    <DialogModal
                      open={openMandate}
                      setOpen={setOpenMandate}
                      css={{
                        "@bp0": { width: "90%" },
                        "@bp1": { width: "60%" },
                      }}
                    // className=""
                    >
                      <LinkedMandate loadManStatus={loaderMan} />
                    </DialogModal>
                    <DialogModal
                      open={openTrans}
                      setOpen={setOpenTrans}
                      css={{
                        "@bp0": { width: "90%" },
                        "@bp1": { width: "75%" },
                      }}
                    >
                      <LinkedTransaction loadTranStatus={loaderTran} />
                    </DialogModal>
                    <DialogModal
                      open={openEdit}
                      setOpen={setOpenEdit}
                      css={{
                        "@bp0": { width: "90%" },
                        "@bp1": { width: "40%" },
                      }}
                    >
                      <UpdateInvestment setEditModalOpen={setOpenEdit} editBody={editInvestId} reload={reloadCall} />
                    </DialogModal>
                  </Box>
                </Box>
              </Card>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default InvestmentPage;
