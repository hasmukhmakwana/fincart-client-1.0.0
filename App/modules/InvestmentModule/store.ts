import create from "zustand";

import { InvestmentProfileTypes, MOHTypes, MOHmember, MandateListTypes, TransactionListTypes } from "./InvestmentTypes"

interface StoreTypes {
  loading: boolean;
  investor: string;
  investmentProfile: InvestmentProfileTypes[];
  mohList: MOHTypes[];
  member: any;
  mohMember: MOHmember[];
  mandateList: MandateListTypes[];
  transactionList: TransactionListTypes[];
  setLoader: (payload: boolean) => void;
  setInvestor: (payload: string) => void;
  setInvestmentProfile: (payload: InvestmentProfileTypes[]) => void;
  setMOHList: (payload: MOHTypes[]) => void;
  setMember: (payload: any) => void;
  setMOHmember: (payload: MOHmember[]) => void;
  setMandateList: (payload: MandateListTypes[]) => void;
  setTransactionList: (payload: TransactionListTypes[]) => void;
}

const useInvestmentStore = create<StoreTypes>((set) => ({
  //* initial state
  loading: false,
  investor: "",
  investmentProfile: [],
  mohList: [],
  member: "",
  mohMember: [],
  mandateList: [],
  transactionList: [],

  setInvestor: (payload) =>
    set((state) => ({
      ...state,
      investor: payload,
    })),

  setInvestmentProfile: (payload) =>
    set((state) => ({
      ...state,
      investmentProfile: payload,
    })),

  setMOHList: (payload) =>
    set((state) => ({
      ...state,
      mohList: payload,
    })),

  setMember: (payload) =>
    set((state) => ({
      ...state,
      member: payload,
    })),
    
  setMOHmember: (payload) =>
    set((state) => ({
      ...state,
      mohMember: payload,
    })),

  setLoader: (payload) =>
    set((state) => ({
      ...state,
      loading: payload,
    })),

  setMandateList: (payload) =>
    set((state) => ({
      ...state,
      mandateList: payload,
    })),

  setTransactionList: (payload) =>
    set((state) => ({
      ...state,
      transactionList: payload,
    })),
}));

export default useInvestmentStore;