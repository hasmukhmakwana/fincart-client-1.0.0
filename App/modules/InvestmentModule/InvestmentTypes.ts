export type InvestmentProfileTypes = {
    Id: string;
    Profileid: string;
    FirstPan: string;
    SecondPan: string;
    ThirdPan: string;
    Moh: string;
    FirstApplicant: string;
    SecondApplicant: string;
    ThirdApplicant: string;
    FirstApplicantBasicId: string;
    SecondApplicantBasicId: string;
    ThirdApplicantBasicId: string;
    GroupLeaderBasicId: string;
    TotalActiveMandate: string;
    TotalActiveTransaction: string;
    isDelete: string;
    isUpdate: string;
};


export type MOHTypes = {
    Text: string;
    Value: string;
    Category: string;
}

export type MOHmember = {
    memberName: string;
    basicid: string;
    isNominee: boolean;
    CAF_Status: string;
}

export type MandateListTypes = {

    Id: string;
    Username: string;
    Recordid: string;
    Bid: string;
    GroupLeader: string;
    ProfileID: string;
    Bank: string;
    Branch: string;
    AccountNo: string;
    IFSC: string;
    MICR: string;
    FromMonth: string;
    FromYear: string;
    ToMonth: string;
    ToYear: string;
    PerDayLimit: string;
    Active: string;
    MandateID: string;
    Reason: string;
    UMRN_NO: string;
    Until_cancel: string;
    Basic_id: string;
    DeleteStatus: string;
    Failure_reason: string;
    Upload_status: string;
    Frequency: string;
    mandatetype: string;
    profilename: string;
    moh: string;
}

export type TransactionListTypes = {
    FolioNo: string;
    SchemeID: string;
    SchemeName: string;
    Amount: string;
    Trxn_typeid: string;
    Trxn_Type: string;
}