export type RegiTypeTypes = "MAIN" | "MEMBER" | "PENDING";

export type VerifyKYCTypes = {
  PAN_No: string;
};

export type NomineeTypes = {
  nomineeName: string;
  relation: string;
  nomineeDOB: string;
  percent: string;
  gaurdianName: string;
  gaurdianPan: string;
  nomineePan: string;
  Nominee_DOB_Proof_Type: string;
  document: string;
  Guardian_Rel_Type: string;
};

export type PersonalInfo = {
  gender: string;
  maritalStatus: string;
  fatherName: string;
  motherName: string;
  occupation: string;
  addressType: string;
  income: string;
  otherIncome: string;
  sourceOfIncome: string;
  nomineeList: NomineeTypes[];
};

export type BankDetails = {
  cancelledCheque?: any;
  accountNumber: string;
  bankAddress: string;
  bankName: string;
  branch: string;
  IFSC: string;
  MICR: string;
  nameAsBank: string;
  bankCity: string;
  accountType: string;
};

export type POITypes = {
  panCard: any;
  panNumber: string;
  name: string;
  fatherName: string;
  DOB: string;
};

export type POATypes = {
  DocType: string;
  aadharFront: any;
  aadharBack: any;
  aadharNumber: string;
  name: string;
  address: string;
  state: string;
  city: string;
  district: string;
  pincode: string;
  issueDate: string;
  expiryDate: string;
};

export type IPVTypes = {
  photo: any;
  video: any;
};

export type SIGNTypes = {
  signature: any;
};

export type DocumentTypes = {
  POI: POITypes;
  POA: POATypes;
  IPV: IPVTypes;
  SIGN: SIGNTypes;
};

export type FormTypes = {
  verifyKYC: VerifyKYCTypes;
  KYCStatus: KYCStausTypes;
  loading: boolean;
  submitAttempt: boolean;
};

export type BasicFormTypes = {
  PAN_No: string;
  Name: string;
  Email: string;
  Mobile: string;
};

export type OTPFormTypes = {
  OTP_E: string;
  OTP_M: string;
};

export type KYCStausTypes = null | "Y" | "N";
export type CommingFromTypes = "next" | "back" | "none";


