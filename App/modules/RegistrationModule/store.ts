import create from "zustand";
import {
  KYCStausTypes,
  VerifyKYCTypes,
  PersonalInfo,
  BankDetails,
  DocumentTypes,
  NomineeTypes,
  RegiTypeTypes,
  CommingFromTypes,
} from "./registrationTypes";

// @ts-ignore
const singleNominee: NomineeTypes = {
  nomineeName: "",
  relation: "",
  nomineeDOB: "",
  percent: "",
  gaurdianName: "",
  gaurdianPan: "",
  nomineePan: "",
  Nominee_DOB_Proof_Type: "",
  document: "",
  Guardian_Rel_Type: ""
};

interface StoreTypes {
  kycSt: boolean;
  BasicID: number;
  regiType: RegiTypeTypes;
  verifyKYC: VerifyKYCTypes;
  personalInfo: PersonalInfo;
  bankDetails: BankDetails;
  documents: DocumentTypes;
  KYCStatus: KYCStausTypes;
  loading: boolean;
  submitAttempt: boolean;
  KYC_CAF_Master: any[];
  summaryInfo: any;
  commingFrom: CommingFromTypes;
  setKycSt: (payload: boolean) => void;
  setLoader: (payload: boolean) => void;
  setKYCStatus: (payload: KYCStausTypes) => void;
  set_KYC_CAF_Master: (payload: any[]) => void;
  // setPersonalInfo: (payload: PersonalInfo) => void;
  setBankingDetails: (payload: BankDetails) => void;
  setDocumentDetails: (
    payload: any,
    type: "POI" | "POA" | "IPV" | "SIGN" | "ALL"
  ) => void;
  addNominee: () => void;
  setSummaryInfo: (payload: any) => void;
  setBasicID: (payload: number) => void;
  setRegiType: (payload: RegiTypeTypes) => void;
  setCommingFrom: (payload: CommingFromTypes) => void;
}

const useRegistrationStore = create<StoreTypes>((set) => ({
  //* initial state
  kycSt: false,
  BasicID: 0,
  regiType: "MAIN",
  verifyKYC: {
    PAN_No: "",
  },
  personalInfo: {
    gender: "M",
    maritalStatus: "MARRIED",
    fatherName: "",
    motherName: "",
    occupation: "",
    addressType: "",
    income: "",
    otherIncome: "",
    sourceOfIncome: "",
    nomineeList: [singleNominee],
  },
  bankDetails: {
    cancelledCheque: null,
    accountNumber: "",
    bankAddress: "",
    bankName: "",
    branch: "",
    IFSC: "",
    MICR: "",
    nameAsBank: "",
    bankCity: "",
    accountType: "",
  },
  documents: {
    POI: {
      panCard: null,
      panNumber: "",
      name: "",
      fatherName: "",
      DOB: "",
    },
    POA: {
      DocType: "",
      aadharFront: null,
      aadharBack: null,
      aadharNumber: "",
      name: "",
      address: "",
      state: "",
      city: "",
      district: "",
      pincode: "",
      issueDate: '',
      expiryDate: ''
    },
    IPV: {
      photo: null,
      video: null,
    },
    SIGN: {
      signature: null,
    },
  },
  KYCStatus: null,
  loading: false,
  submitAttempt: false,
  KYC_CAF_Master: [],
  summaryInfo: {},
  commingFrom: "none",

  //* methods for manipulating state
  setKycSt: (payload) =>
    set((state) => ({
      ...state,
      kycSt: payload,
    })),
  setBasicID: (payload) =>
    set((state) => ({
      ...state,
      BasicID: payload || 0,
    })),

  setRegiType: (payload) =>
    set((state) => ({
      ...state,
      regiType: payload || "",
    })),

  setLoader: (payload) =>
    set((state) => ({
      ...state,
      loading: payload,
    })),

  setKYCStatus: (payload) =>
    set((state) => ({
      ...state,
      KYCStatus: payload,
    })),

  set_KYC_CAF_Master: (payload) =>
    set((state) => ({
      ...state,
      KYC_CAF_Master: payload,
    })),

  // setPersonalInfo: (payload) =>
  //   set((state) => ({
  //     ...state,
  //     personalInfo: payload,
  //   })),

  setBankingDetails: (payload) =>
    set((state) => ({
      ...state,
      bankDetails: {
        ...state.bankDetails,
        ...payload,
      },
    })),

  setDocumentDetails: (payload, type) =>
    set((state) => {
      let obj: any = {
        [type]: payload,
      };
      if (!type || type === "ALL") {
        obj = payload;
      }

      return {
        ...state,
        documents: {
          ...state.documents,
          ...obj,
        },
      };
    }),

  addNominee: () =>
    set((state) => ({
      ...state,
      personalInfo: {
        ...state.personalInfo,
        nomineeList: [...state.personalInfo.nomineeList, singleNominee],
      },
    })),

  setSummaryInfo: (payload) =>
    set((state) => {
      return {
        ...state,
        summaryInfo: payload,
        KYCStatus: payload?.Steps?.isAccount
          ? payload?.Account_Init_Details?.KYC_Status || null
          : null,
        //KYCStatus: payload?.Account_Init_Details?.KYC_Status,
        personalInfo: {
          ...state.personalInfo,
          gender: payload?.Kyc_Details?.Gender_Value || "M",
          maritalStatus: payload?.Kyc_Details?.MaritalStatus_Value || "MARRIED",
          fatherName: payload?.Account_Init_Details?.FatherName || "",
          motherName: payload?.Kyc_Details?.MotherName || "",
          occupation: payload?.Kyc_Details?.Occupation_Value || "",
          addressType: payload?.Kyc_Details?.Address_Type_Value || "",
          income: payload?.Kyc_Details?.AnnualIncome_Value || "",
          // otherIncome: payload?.Kyc_Details?.MotherName || "",
          sourceOfIncome: payload?.Kyc_Details?.SourceOfIncome_Value || "",
          nomineeList: payload?.Nominee_Details?.length
            ? payload?.Nominee_Details?.map((i: any) => ({
              nomineeName: i?.Nominee_Name || "",
              relation: i?.Relation || "",
              nomineeDOB: i?.Nominee_DOB || "",
              percent: i?.Nominee_Percent || "",
              gaurdianName: i?.Guardian_Name || "",
              gaurdianPan: i?.Guardian_Pan || "",
            }))
            : [singleNominee],
        },
        bankDetails: {
          cancelledCheque:
            payload?.Bank_Details?.ChequeFilePath_Fincart || null,
          accountNumber: payload?.Bank_Details?.Account_No || "",
          bankAddress: payload?.Bank_Details?.Bank_Address || "",
          bankName: payload?.Bank_Details?.Bank_Name || "",
          branch: payload?.Bank_Details?.Branch_Name || "",
          IFSC: payload?.Bank_Details?.IFSC || "",
          MICR: payload?.Bank_Details?.MICR || "",
          nameAsBank: payload?.Bank_Details?.Name_As_Per_Bank || "",
          bankCity: payload?.Bank_Details?.Bank_City || "",
          accountType: payload?.Bank_Details?.AccountType || "",
        },
        documents: {
          POI: {
            panCard: payload?.POI_Details?.POI_Path_Fincart || null,
            panNumber: payload?.Account_Init_Details?.PanNumber || "",
            name: payload?.Account_Init_Details?.ClientName || "",
            fatherName: payload?.Account_Init_Details?.FatherName || "",
            DOB: payload?.Account_Init_Details?.DOB || "",
          },
          POA: {
            DocType: payload?.POA_Details?.POA_Type_Value || null,
            aadharFront: payload?.POA_Details?.POA_FrontPath_Fincart || null,
            aadharBack: payload?.POA_Details?.POA_BackPath_Fincart || null,
            aadharNumber: payload?.POA_Details?.Uid || "",
            name: payload?.POA_Details?.AadharName || "",
            address: payload?.POA_Details?.Address || "",
            state: payload?.POA_Details?.State || "",
            city: payload?.POA_Details?.City || "",
            district: payload?.POA_Details?.District || "",
            pincode: payload?.POA_Details?.PinCode || "",
            issueDate: payload?.POA_Details?.issueDate || "",
            expiryDate: payload?.POA_Details?.expiryDate || "",
          },
          IPV: {
            photo: payload?.IPV_Details?.Client_Photo_Fincart || null,
            video: payload?.IPV_Details?.Video_Fincart || null,
          },
          SIGN: {
            signature:
              payload?.Signature_Details?.SignaturePath_Fincart_WithBG || null,
          },
        },
      };
    }),

  setCommingFrom: (payload) =>
    set((state) => ({
      ...state,
      commingFrom: payload || "none",
    })),

  /** END */
}));

export default useRegistrationStore;
