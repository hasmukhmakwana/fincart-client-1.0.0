export const PAGE_TYPES = {
  IdentityProof: "IdentityProof",
  AddressProof: "AddressProof",
  CapturePhotoVideo: "CapturePhotoVideo",
  UploadSignature: "UploadSignature",
};
