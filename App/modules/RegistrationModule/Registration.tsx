import RegistrationPage from "./components/RegistrationPage";
import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import KYCVerification from "./components/KYCVerification";
import { BasicFormTypes, VerifyKYCTypes } from "./registrationTypes";
import {
  checkKYCStatus,
  getSummary,
  accountCreation,
  getCurrentLocation
} from "App/api/registration";
import { encryptData, getUser } from "App/utils/helpers";
import useRegistrationStore from "./store";
import { useEffect, useState } from "react";
import AccountCreation from "./components/AccountCreation";
import { useRouter } from "next/router";
import RegisterModal from "./components/RegisterModal";
import OtherCountry from "./components/OtherCountry";
import { Button } from "@ui/Button/Button";
import Box from "@ui/Box/Box";
import Recycle from "App/icons/Recycle";
import { resetKYC } from "App/api/profile";
//*main
const Module = () => {
  const router = useRouter();
  const user: any = getUser();
  const {
    BasicID,
    kycSt,
    setKycSt,
    setBasicID,
    regiType,
    KYCStatus,
    setKYCStatus,
    verifyKYC,
    setLoader,
    setSummaryInfo,
    setCommingFrom,
  } = useRegistrationStore();
  const [isAccount, setisAccount] = useState<boolean>(false);
  const [resetShow, setResetShow] = useState<boolean>(false);
  const [othCountryStus, setOthCountryStus] = useState<boolean>(false);
  const [basicDetails, setbasicDetails] = useState<BasicFormTypes>({
    PAN_No: verifyKYC?.PAN_No || "",
    Name: "",
    Email: "",
    Mobile: "",
  });

  //*useEffects
  useEffect(() => {
    callSummary();
    getCurrentLocationInReg();
    return () => {
      setKYCStatus(null);
      setCommingFrom("none");
    };
  }, []);

  //*functions

  const callSummary = async (b_id = 0) => {
    try {
      let basicid: number = b_id || 0;
      if (regiType === "MAIN") {
        basicid = user?.basicid;
      } else if (regiType === "PENDING") {
        basicid = BasicID;
      }

      //console.log({ basicid });
      if (!basicid) return;
      setBasicID(basicid);
      setLoader(true);
      const enc: any = encryptData(basicid, true);
      const result: any = await getSummary(enc);
      setLoader(false);
      //console.log(result);
      /**
       * in case of user refresh the page or url hit
       * check main user is already register or not
       */
      // (result?.data?.Steps?.isKYC && result?.data?.Steps?.Is_KYC_Confirmed) ||
      // (!result?.data?.Steps?.isKYC && result?.data?.Steps?.Is_CAF_Confirmed)
      if (result?.data?.Steps?.Is_CAF_Confirmed) {
        router.push("/Profile");
        return;
      }
      setisAccount(result?.data?.Steps?.isAccount);
      setSummaryInfo(result?.data);
    } catch (error) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  const handleCheckKYC = async (formValues: VerifyKYCTypes) => {
    let PanNumber: string = "";
    let Client_Name: string = "";
    let isKYC: string = "";
    try {
      // console.log(formValues);
      setLoader(true);
      const enc: any = encryptData(formValues?.PAN_No, true);
      const result: any = await checkKYCStatus(enc);

      if (
        result?.data?.KYC_FROM === "already" &&
        result?.data?.KYC_DATE
        // && result?.data?.KYC_STATUS === "Y"
      ) {
        setLoader(false);
        return toastAlert("warn", "This PAN already registered!!");
      }

      PanNumber = result?.data?.PAN_NUMBER || formValues?.PAN_No;
      Client_Name = result?.data?.PAN_NAME || (regiType == "MEMBER" ? "" : user?.name);
      isKYC = result?.data?.KYC_STATUS;

      setbasicDetails({
        PAN_No: PanNumber,
        Name: Client_Name,
        Email: regiType == "MEMBER" ? "" : user?.userid,
        Mobile: regiType == "MEMBER" ? "" : user?.mobile,
      });

      setLoader(false);
      setKYCStatus(result?.data?.KYC_STATUS);
      setKycSt(true);
      // if (isKYC === "Y") {
      if (BasicID && isKYC === "Y") {
        //account creation part optional for first registration
        handleAccountCreation({ Client_Name, PanNumber, isKYC });
      }
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  const handleAccountCreation = async (data: any = null) => {
    data = data || {};
    //console.log("handleAccountCreation: ", data);
    try {
      const obj: any = {
        PanNumber: data?.PanNumber || verifyKYC?.PAN_No,
        Client_Name: data?.Client_Name || user?.name,
        IsKYC: data?.isKYC || KYCStatus,
        // BasicID: user?.basicid,
        BasicID: BasicID,
        Client_Email: data?.Client_Email || user?.userid,
        Mobile: data?.Mobile || user?.mobile,
        GL_UserID: user?.userid,
        Is_Email_Verified: "Y",
        Is_Mobile_Verified: "Y",
        Sess_ID: "",
      };
      const encAccount: any = encryptData(obj);
      const result: any = await accountCreation(encAccount);
      // console.log(result?.data);
      callSummary(result?.data?.BasicId);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  const resetKYCDetails = async (member: any) => {
    try {
      if (member === undefined) return null;

      setLoader(true);
      const enc: any = encryptData(member, true);
      let result = await resetKYC(enc);

      if (result?.status != undefined && result?.status.toString() === "Success") {
        toastAlert("success", "Profile reset done successfully");
      }
      router.push("/Registration");
      setLoader(false);
    } catch (error) {
      setLoader(false);
      console.log(error);
      return toastAlert("error", error);
    }
  };

  const getCurrentLocationInReg = async () => {
    const result: any = await getCurrentLocation();

    if (result != undefined) {
      const locationResult = JSON.parse(result);

      if (locationResult?.country_name != "India" && locationResult?.country_code != "IN") {
        setOthCountryStus(true);
      } else {
        setOthCountryStus(false);
      }
    }
  }

  // console.log(KYCStatus);

  /**
   *
   * @RENDER_COMPONENT
   */
  const renderComponent = () => {
    // console.log({
    //   isAccount,
    //   KYCStatus,
    //   BasicID,
    //   check1: KYCStatus === "N" && !isAccount,
    //   check2: !BasicID && KYCStatus && !isAccount,
    // });
    if (othCountryStus) {
      return (<OtherCountry />);
    }
    else {
      if (kycSt) {
        return (<RegisterModal status={KYCStatus} />);
      } else if (isAccount) {
        return (<RegistrationPage btnShow={setResetShow} />);
      } else if (KYCStatus === "N" && !isAccount) {
        return (
          <>
            <AccountCreation
              basicDetails={basicDetails}
              handleAccountCreation={handleAccountCreation}
            />
          </>
        );
      } else if (!BasicID && KYCStatus && !isAccount) {
        return (
          <>
            <AccountCreation
              basicDetails={basicDetails}
              handleAccountCreation={handleAccountCreation}
            />
          </>
        );
      }
      return (<>
        <KYCVerification clickOnSubmit={handleCheckKYC} />
      </>);
    }
  };

  //*main return
  return (
    <Layout>
      <PageHeader title="Registration"
        rightContent={resetShow ?
          <Box className="m-0 p-0">
            <Button color="yellow" onClick={() => { console.log(BasicID); resetKYCDetails(BasicID) }}>
              <Recycle className="me-1" color="black" height={14} width={14} />Reset Profile
            </Button>
          </Box> : ""} />
      {renderComponent()}
    </Layout>
  );
};

export default Module;