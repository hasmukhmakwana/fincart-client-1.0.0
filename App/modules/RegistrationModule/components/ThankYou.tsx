import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import { useRouter } from "next/router";
import Layout, { toastAlert } from "App/shared/components/Layout";
import { useEffect, useState } from "react";
import useRegistrationStore from "../store";
import { encryptData } from "App/utils/helpers";
import { confirmCAF, confirmKYC, getSummary } from "App/api/registration";
import { getLS } from "@ui/DataGrid/utils";

// main
const ThankYou = ({ cb }: any) => {
  const { setLoader, loading } = useRegistrationStore();
  const [KYC, setKYC] = useState<boolean>(false);

  const router = useRouter();
  useEffect(() => {
    /**
     * @fincalCall - call api for non-kyc user
     */
    const fincalCall = async () => {
      try {
        setLoader(true);
        const enc_confirm: any = encryptData(BasicID, true);
        const result: any = await getSummary(enc_confirm);
        const kyc: boolean = result?.data?.Steps?.isKYC;
        setKYC(kyc);

        if (!kyc) {
          const result_confirm: any = await confirmKYC(enc_confirm);

          if (!kyc && result_confirm?.status === "Success") {
            setKYC(true);
            //msg acordingly here....
            await confirmCAF(enc_confirm);
            //const result_caf: any = await confirmCAF(enc_confirm);
            //console.log("Result CAF", result_caf);
          }
        }
        else {
          await confirmCAF(enc_confirm);
        }

        setLoader(false);
      } catch (error) {
        console.log(error);
        toastAlert("error", error);
        setLoader(false);
      }
    };

    const BasicID = getLS("KYC_BasicID");
    //console.log("Basic ID", BasicID);
    BasicID && fincalCall();
  }, []);

  return (
    <Layout>
      <Box className="row row-cols-1 row-cols-md-2 justify-content-center">
        <Box className="col">
          <Card noshadow css={{ p: 50 }}>
            {KYC ? (
              <>
                <Box>
                  <Text color="green" size="h3" space="letterspace">
                    Thank you ! The Registration process is completed successfully.
                  </Text>
                </Box>

                <Box css={{ pt: 30 }}>
                  <Text color="blue" size="h6">
                    Would you like to continue for Mandate Registration?
                  </Text>
                </Box>

                <Box css={{ pt: 50 }}>
                  <Button
                    type="submit"
                    color="yellow"
                    onClick={
                      cb ? cb : () => router.push("/Mandate/RegisterMandate")
                    }
                  >
                    Yes
                  </Button>
                  <Button
                    type="submit"
                    color="yellow"
                    onClick={() => router.push("/Dashboard")}
                  >
                    I'll do it later
                  </Button>
                </Box>
              </>
            ) : (
              <Box>
                <Text color="green" size="h3" space="letterspace">
                  {loading
                    ? "Loading..."
                    : "E-Aadhar Sign Verification Failed!"}
                </Text>
              </Box>
            )}
          </Card>
        </Box>
      </Box>
    </Layout>
  );
};
export default ThankYou;
