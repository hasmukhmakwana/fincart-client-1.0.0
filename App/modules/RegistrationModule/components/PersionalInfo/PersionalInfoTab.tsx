import React, { useEffect, useState } from "react";
import { Formik, FieldArray, Form } from "formik";
import * as Yup from "yup";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import { GroupBox } from ".././../../../ui/Group/Group.styles";
import Input from "@ui/Input";
import { RadioGroup } from "@ui/Radio/";
import Radio from "@ui/Radio/Radio";
import SelectMenu from "@ui/Select/Select";
import AddNominee from "./AddNominee";
import useRegistrationStore from "../../store";
import { decryptData, encryptData, getArrayFromKey, plainBase64 } from "App/utils/helpers";
import { NomineeTypes, PersonalInfo } from "../../registrationTypes";
import { toastAlert } from "App/shared/components/Layout";
import Badge from "@ui/Badge/Badge";
import { getSummary, updatePersonalInfo } from "App/api/registration";
import RightArrowShort from "App/icons/RightArrowShort";

const nomineeListSchema: any = {
  nomineeList: Yup.array().of(
    Yup.object().shape({
      nomineeName: Yup.string().required("Nominee Name is required"),
      relation: Yup.string().required("Relation is required"),
      nomineeDOB: Yup.string().required("DOB is required"),
      percent: Yup.string().required("Nominee Percent(%) is required"),
    })
  ),
};
let schemaObj: any = {
  gender: Yup.string().required("Gender is required"),
  maritalStatus: Yup.string().required("Marital Status is required"),
  motherName: Yup.string().required("Mother Name is required"),
  fatherName: Yup.string().required("Father Name is required"),
  occupation: Yup.string().required("Occupation is required"),
  addressType: Yup.string().required("Address Type is required"),
  income: Yup.string().required("Annual Income is required"),
  sourceOfIncome: Yup.string().required("Source Of Income is required"),
};

type PersonalInfoEventTypes = {
  onNextClickParent: () => void;
  onBackClickParent: () => void;
};

//*main
const PersonalInfoTab = ({
  onNextClickParent,
  onBackClickParent,
}: PersonalInfoEventTypes) => {
  const {
    BasicID,
    loading,
    setLoader,
    KYCStatus,
    personalInfo,
    KYC_CAF_Master,
    addNominee,
    summaryInfo,
    setSummaryInfo,
  } = useRegistrationStore();
  const [genderList, setgenderList] = useState<any[]>([]);
  const [maritalStatusList, setmaritalStatusList] = useState<any[]>([]);
  const [occupationList, setoccupationList] = useState<any[]>([]);
  const [addresTypeList, setaddresTypeList] = useState<any[]>([]);
  const [annualIncomeList, setannualIncomeList] = useState<any[]>([]);
  const [sourceOfIncomeList, setsourceOfIncomeList] = useState<any[]>([]);
  const [isBasic, setisBasic] = useState(summaryInfo?.Steps?.isBasic);

  // const [otherIncomOpted, setotherIncomOpted] = useState("");

  //*useEffects
  useEffect(() => {
    setgenderList(getArrayFromKey(KYC_CAF_Master, "Gender_list"));
    setmaritalStatusList(getArrayFromKey(KYC_CAF_Master, "MaritalStatus_list"));
    setoccupationList(getArrayFromKey(KYC_CAF_Master, "Occupation_list"));
    setaddresTypeList(getArrayFromKey(KYC_CAF_Master, "AddressType_list"));
    setannualIncomeList(getArrayFromKey(KYC_CAF_Master, "AnnualIncome_list"));
    setsourceOfIncomeList(
      getArrayFromKey(KYC_CAF_Master, "SourceOfIncome_list")
    );
  }, [KYC_CAF_Master]);

  //*functions
  const clickOnSubmit_bk = async (formValues: PersonalInfo) => {
    try {
      // console.log(formValues);
      setLoader(true);
      const postData: any = {
        BasicID: BasicID,
        Gender: formValues.gender,
        MaritalStatus: formValues.maritalStatus,
        FatherName: formValues.fatherName,
        MotherName: formValues.motherName,
        Occupation: formValues.occupation,
        AddressType: formValues.addressType,
        AnnualIncome: formValues.income,
        SourceOfIncome: formValues.sourceOfIncome,
        IsKYC: KYCStatus,
        Nominee_List: formValues.nomineeList.map((i) => ({
          Nom_Id: 0,
          Nominee_Name: i.nomineeName,
          Relation: i.relation,
          Nominee_DOB: i.nomineeDOB,
          Nominee_Percent: i.percent,
          Guardian_Name: i.gaurdianName,
          Guardian_Pan: i.gaurdianPan,
          Nominee_Pan: i.nomineePan,
          Nominee_DOB_Proof_Type: i.Nominee_DOB_Proof_Type,
          // Guardian_Rel_Type: i?.Guardian_Rel_Type,
          NOM1_File: i.document,
          BasicId: BasicID,
        })),
      };
      console.log(postData);
      const enc: any = encryptData(postData);
      const result = await updatePersonalInfo(enc);
      console.log(result);
      setLoader(false);
      onNextClickParent();
    } catch (error: any) {
      console.log(JSON.stringify(error));
      setLoader(false);
      toastAlert("error", error);
    }
  };

  const clickOnSubmit = async (formValues: PersonalInfo) => {
    try {
      // console.log(formValues);

      const nom: NomineeTypes[] = formValues?.nomineeList || [];
      if (!nom?.length) return toastAlert("warn", "A nominee is required");

      let per_total: number = 0;
      const Nominee_List = formValues.nomineeList.map((i) => {
        per_total += parseInt(i?.percent || "0");
        return {
          Nom_Id: 0,
          Nominee_Name: i?.nomineeName || "",
          Relation: i?.relation || "",
          Nominee_DOB: i?.nomineeDOB || "",
          Nominee_Percent: i?.percent || 0,
          Guardian_Name: i?.gaurdianName || "",
          Guardian_Pan: i?.gaurdianPan || "",
          Nominee_Pan: i?.nomineePan || "",
          Nominee_DOB_Proof_Type: i?.Nominee_DOB_Proof_Type || "",
          BasicId: BasicID,
          Guardian_Rel_Type: i?.Guardian_Rel_Type || "",
          NOM1_File: plainBase64(i?.document),
        };
      });

      console.log(per_total);
      if (per_total != 100)
        return toastAlert("warn", "Nominee_Percent sum must be 100");

      setLoader(true);
      const postData: any = {
        BasicID: BasicID,
        Gender: formValues.gender,
        MaritalStatus: formValues.maritalStatus,
        FatherName: formValues.fatherName,
        MotherName: formValues.motherName,
        Occupation: formValues.occupation,
        AddressType: formValues.addressType,
        AnnualIncome: formValues.income,
        SourceOfIncome: formValues.sourceOfIncome,
        IsKYC: KYCStatus,
        Nominee_List,
      };
      console.log("NomineeData");
      console.log(postData);
      const enc: any = encryptData(postData);
      console.log(enc);
      await updatePersonalInfo(enc);
      setLoader(false);
      callSummary();
      toastAlert("success", "Nominee Added Successfully");
      onNextClickParent();
    } catch (error: any) {
      console.log(JSON.stringify(error));
      setLoader(false);
      toastAlert("error", error);
    }
  };

  const callSummary = async () => {
    try {
      const enc: any = encryptData(BasicID, true);
      const result: any = await getSummary(enc);
      setSummaryInfo(result?.data);
    } catch (error) {
      console.log(error);
    }
  };

  //*main return
  return (
    <Box background="">
      <Box className="">
        <Box className="row">
          <Box>
            <Card noshadow>
              {/* @ts-ignore */}
              <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
                Personal Information
                {summaryInfo?.Account_Creation_Details?.ClientName && (
                  <span className="ms-1">
                    -
                    <span className="ms-1 text-primary">
                      {summaryInfo?.Account_Creation_Details?.ClientName}
                    </span>
                  </span>
                )}
              </Text>
              <Formik
                initialValues={personalInfo}
                validationSchema={Yup.object().shape({
                  ...schemaObj,
                  ...nomineeListSchema,
                })}
                // validationSchema={Yup.object().shape(
                //   KYCStatus === "N"
                //     ? { ...schemaObj, ...nomineeListSchema }
                //     : { ...schemaObj }
                // )}
                enableReinitialize
                onSubmit={(values, { resetForm }) => {
                  clickOnSubmit(values);
                  // resetForm();
                }}
              >
                {({
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  errors,
                  touched,
                  setFieldValue,
                  submitCount,
                }) => (
                  <Form>
                    <Box css={{ p: 20 }}>
                      <Box className="row" css={{ pl: 0, pr: 0 }}>
                        <Box className="col">
                          <Text
                            //@ts-ignore
                            weight="bold"
                            size="h4"
                            css={{ mb: 10, mt: 10 }}
                          >
                            General
                          </Text>
                        </Box>
                      </Box>
                      <Box className="row">
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          {/* @ts-ignore */}
                          <Text color="blue1" size="h6" space="letterspace">
                            Gender
                          </Text>
                          <GroupBox css={{ mb: 10 }}>
                            <RadioGroup
                              defaultValue={values.gender}
                              className="inlineRadio"
                              name="gender"
                              onValueChange={(e: any) => {
                                setFieldValue("gender", e);
                              }}
                            >
                              {genderList.map((i, index) => (
                                <React.Fragment key={index}>
                                  <Radio
                                    value={i.Value}
                                    label={i.Text}
                                    id={i.Value}
                                  />
                                </React.Fragment>
                              ))}
                            </RadioGroup>
                          </GroupBox>
                        </Box>
                        <Box className="col-md-5 col-sm-5 col-lg-5">
                          {/* @ts-ignore */}
                          <Text color="blue1" size="h6" space="letterspace">
                            Marital Status
                          </Text>
                          <GroupBox>
                            <RadioGroup
                              defaultValue={values.maritalStatus}
                              className="inlineRadio"
                              name="maritalStatus"
                              onValueChange={(e: any) => {
                                setFieldValue("maritalStatus", e);
                              }}
                            >
                              {maritalStatusList.map((i, index) => (
                                <React.Fragment key={index}>
                                  <Radio
                                    value={i.Value}
                                    label={i.Text}
                                    id={i.Value}
                                  />
                                </React.Fragment>
                              ))}
                            </RadioGroup>
                          </GroupBox>
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Father's Name"
                            name="fatherName"
                            value={values.fatherName}
                            onChange={(e: any) => {
                              setFieldValue("fatherName", e?.target?.value.length > 200 ? e?.target?.value.slice(0, 200) : e?.target?.value.replace(/[^a-zA-Z\s]+/g, ''))
                            }}
                            placeholder="Father's Name"
                            required
                            error={submitCount ? errors.fatherName : ""}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Mother's Name"
                            name="motherName"
                            value={values.motherName}
                            onChange={(e: any) => {
                              setFieldValue("motherName", e?.target?.value.length > 200 ? e?.target?.value.slice(0, 200) : e?.target?.value.replace(/[^a-zA-Z\s]+/g, ''))
                            }}
                            placeholder="Mother's Name"
                            required
                            error={submitCount ? errors.motherName : null}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <SelectMenu
                            items={occupationList}
                            bindValue={"Value"}
                            bindName={"Text"}
                            label={"Occupation"}
                            placeholder={"Select Occupation"}
                            name="occupation"
                            value={values.occupation}
                            onChange={(e: any) => {
                              setFieldValue("occupation", e.Value);
                            }}
                            required
                            error={submitCount ? errors.occupation : ""}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <SelectMenu
                            items={addresTypeList}
                            bindValue={"Value"}
                            bindName={"Text"}
                            label={"Address Type"}
                            placeholder={"Select Address Type"}
                            name="addressType"
                            value={values.addressType}
                            onChange={(e: any) => {
                              setFieldValue("addressType", e.Value);
                            }}
                            required
                            error={submitCount ? errors.addressType : ""}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <SelectMenu
                            // items={[
                            //   ...annualIncomeList,
                            //   { Text: "Other", Value: "Other", category: null },
                            // ]}
                            items={annualIncomeList}
                            bindValue={"Value"}
                            bindName={"Text"}
                            label={"Annual Income"}
                            placeholder={"Select Annual Income"}
                            name="income"
                            value={values.income}
                            onChange={(e: any) => {
                              setFieldValue("income", e.Value);
                            }}
                            required
                            error={submitCount ? errors.income : ""}
                          />
                        </Box>
                        {/* {otherIncomOpted && (
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Other Income"
                            name="otherIncome"
                            placeholder="Other Income"
                            value={values.otherIncome}
                            required
                            error={submitCount?errors.otherIncome:null}
                          />
                        </Box>
                      )} */}
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <SelectMenu
                            items={sourceOfIncomeList}
                            bindValue={"Value"}
                            bindName={"Text"}
                            label={"Source Of Income"}
                            placeholder={"Select Source Income"}
                            name="sourceOfIncome"
                            value={values.sourceOfIncome}
                            onChange={(e: any) => {
                              setFieldValue("sourceOfIncome", e.Value);
                            }}
                            required
                            error={submitCount ? errors.sourceOfIncome : ""}
                          />
                        </Box>
                        {/* @ts-ignore */}
                        <FieldArray name="nomineeList">
                          {({ insert, remove, push }) => (
                            <>
                              <Box className="row" css={{ pl: 10, pr: 0 }}>
                                <Box className="col">
                                  <Text
                                    //@ts-ignore
                                    weight="bold"
                                    size="h4"
                                    css={{ mb: 10, mt: 10 }}
                                  >
                                    Nominee(s)
                                    {KYCStatus === "N" && (
                                      <span className="c-gmqXFB">*</span>
                                    )}
                                    <Badge
                                      type={"dot"}
                                      color={"blue2"}
                                      css={{
                                        pt: 3,
                                        pr: 7,
                                        pl: 7,
                                        pb: 3,
                                      }}
                                      rounded
                                    >
                                      {values?.nomineeList?.length || 0}
                                    </Badge>
                                  </Text>
                                </Box>
                                <Box
                                  className="col col-sm-auto text-end"
                                  css={{ p: 0, mt: 4 }}
                                >
                                  <Button
                                    color="yellow"
                                    onClick={() => push({ nomineeName: "" })}
                                    disabled={values?.nomineeList?.length === 3}
                                  >
                                    Add Nominee
                                  </Button>
                                </Box>
                              </Box>
                              {values?.nomineeList?.length > 0 &&
                                values?.nomineeList?.map((item, index) => (
                                  <>
                                    {index !== 0 && (
                                      <hr className="mt-3 dotted" />
                                    )}
                                    <AddNominee
                                      index={index}
                                      item={item}
                                      setFieldValue={setFieldValue}
                                      handleChange={handleChange}
                                      errors={errors}
                                      submitCount={submitCount}
                                    />
                                    {values.nomineeList.length > 1 && (
                                      <Box key={index}>
                                        <Button
                                          color="yellow"
                                          onClick={() => remove(index)}
                                        >
                                          Remove
                                        </Button>
                                      </Box>
                                    )}
                                  </>
                                ))}
                            </>
                          )}
                        </FieldArray>
                      </Box>
                      <Box className="row">
                        <Box className="col"></Box>
                        <Box className="col col-sm-auto text-end">
                          {isBasic && (
                            <Button color="yellow" onClick={onNextClickParent}>
                              Next
                              <RightArrowShort size="20" />
                            </Button>
                          )}
                          <Button
                            type="submit"
                            color="yellow"
                            onClick={handleSubmit}
                            loading={loading}
                          >
                            Update & Next
                            <RightArrowShort size="20" />
                          </Button>
                        </Box>
                      </Box>
                    </Box>
                  </Form>
                )}
              </Formik>
            </Card>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default PersonalInfoTab;
