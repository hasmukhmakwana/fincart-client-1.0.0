import { Button } from "@ui/Button/Button";
import Input from "@ui/Input";
import React from "react";
import Box from "../../../../ui/Box/Box";
import { NomineeTypes } from "../../registrationTypes";
import { calculateAge, convertBase64, getArrayFromKey } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import SelectMenu from "@ui/Select/Select";
import Text from "@ui/Text/Text";
import useProfileStore from "App/modules/ProfileModule/store";

type NomineeEventType = {
  index: number;
  item: NomineeTypes;
  setFieldValue: any;
  handleChange: (e: any) => void;
  errors: any;
  submitCount: number;
};
const AddNominee = ({
  index,
  item,
  setFieldValue,
  handleChange,
  errors,
  submitCount,
}: NomineeEventType) => {
  const { KYC_CAF_MASTER } = useProfileStore();
  return (
    <React.Fragment>
      <Box className="col-md-4 col-sm-4 col-lg-4">
        <Input
          label="Nominee Name"
          placeholder="Nominee Name"
          name={`nomineeList.${index}.nomineeName`}
          value={item?.nomineeName || ""}
          onChange={(e: any) => {
            setFieldValue(`nomineeList.${index}.nomineeName`, e?.target?.value.length > 200 ? e?.target?.value.slice(0, 200) : e?.target?.value.replace(/[^a-zA-Z\s]+/g, ''))
          }}
          required
          error={submitCount ? errors?.nomineeList?.[index]?.nomineeName : null}
        />
      </Box>
      <Box className="col-md-4 col-sm-4 col-lg-4">
        <Input
          label="Relation"
          placeholder="Relation"
          name={`nomineeList.${index}.relation`}
          value={item?.relation || ""}
          onChange={(e: any) => {
            setFieldValue(`nomineeList.${index}.relation`, e?.target?.value.length > 50 ? e?.target?.value.slice(0, 50) : e?.target?.value.replace(/[^a-zA-Z\s]+/g, ''))
          }}
          required
          error={submitCount ? errors?.nomineeList?.[index]?.relation : null}
        />
      </Box>
      <Box className="col-md-4 col-sm-4 col-lg-4">
        <Input
          type="date"
          label="Date Of Birth"
          placeholder="Date Of Birth"
          name={`nomineeList.${index}.nomineeDOB`}
          value={item?.nomineeDOB || ""}
          onChange={handleChange}
          required
          error={submitCount ? errors?.nomineeList?.[index]?.nomineeDOB : null}
        />
      </Box>
      <Box className="col-md-4 col-sm-4 col-lg-4">
        <Input
          type="number"
          label="Nominee Percent (%)"
          placeholder="Nominee Percent"
          name={`nomineeList.${index}.percent`}
          value={item?.percent || ""}
          onChange={(e: any) => {
            setFieldValue(`nomineeList.${index}.percent`, e?.target?.value.length > 3 ? e?.target?.value.slice(0, 3) : e?.target?.value.replace(/\D/g, ''))
          }}
          required
          error={submitCount ? errors?.nomineeList?.[index]?.percent : null}
        />
      </Box>

      {calculateAge(item?.nomineeDOB)?.years < 18 ? (
        <>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Input
              label="Guardian Name"
              placeholder="Guardian Name"
              name={`nomineeList.${index}.gaurdianName`}
              value={item?.gaurdianName || ""}
              onChange={(e: any) => {
                setFieldValue(`nomineeList.${index}.gaurdianName`, (e?.target?.value.length > 200 ? e?.target?.value.slice(0, 200) : e?.target?.value).toString())
              }}
            />
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Input
              label="Guardian PAN"
              placeholder="Guardian PAN"
              name={`nomineeList.${index}.gaurdianPan`}
              value={item?.gaurdianPan || ""}
              onChange={handleChange}
              maxLength={10}
            />
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <SelectMenu
              items={getArrayFromKey(
                KYC_CAF_MASTER,
                "docSebiNominee"
              )}
              bindValue={"Value"}
              bindName={"Text"}
              label={"Nominee DOB Proof Type"}
              placeholder={"Select Nominee DOB Proof Type"}
              name={`nomineeList.${index}.Nominee_DOB_Proof_Type`}
              value={item?.Nominee_DOB_Proof_Type}
              onChange={(e: any) => {
                setFieldValue(
                  `nomineeList.${index}.Nominee_DOB_Proof_Type`,
                  e.Value
                );
              }}
              required
              error={
                submitCount
                  ? errors?.nomineeList?.[index]
                    ?.Nominee_DOB_Proof_Type
                  : null
              } />
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            {/* <Text
              //@ts-ignore
              color="mediumBlue"
              size="h6"
              space="letterspace"
            >
              Upload DOB Proof
              <span className="text-danger">*</span>
            </Text> */}
            <Input
              label="Upload DOB Proof"
              type="file"
              className="form-control"
              name={`nomineeList.${index}.document`}
              accept=".jpg,.png"
              required={calculateAge(item?.nomineeDOB)?.years < 18 ? true : false}
              onChange={async (
                e: React.ChangeEvent<HTMLInputElement>
              ) => {
                try {
                  const file: any =
                    e?.target?.files?.[0] || null;
                  const base64 = await convertBase64(file);
                  setFieldValue(
                    `nomineeList.${index}.document`,
                    base64
                  );
                } catch (error: any) {
                  toastAlert("error", error);
                }
              }}
              error={
                submitCount
                  ? errors?.nomineeList?.[index]?.document
                  : null
              }
            />
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <SelectMenu
              items={getArrayFromKey(
                KYC_CAF_MASTER,
                "relationlistSebiNominee"
              )}
              bindValue={"Value"}
              bindName={"Text"}
              label={"Gua. Rel. With Nominee"}
              placeholder={"Select Gua. Rel. With Nominee"}
              name={`nomineeList.${index}.Guardian_Rel_Type`}
              value={item?.Guardian_Rel_Type}
              onChange={(e: any) => {
                setFieldValue(
                  `nomineeList.${index}.Guardian_Rel_Type`,
                  e.Value
                );
              }}
            />
          </Box>
        </>) : (
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Nominee Pan No."
            placeholder="Nominee PAN"
            name={`nomineeList.${index}.nomineePan`}
            value={item?.nomineePan || ""}
            onChange={handleChange}
            maxLength={10}
            required
          />
        </Box>)
      }
    </React.Fragment >
  );
};
export default AddNominee;
