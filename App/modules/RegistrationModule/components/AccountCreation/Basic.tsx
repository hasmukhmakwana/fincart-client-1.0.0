import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { BasicFormTypes, VerifyKYCTypes } from "../../registrationTypes";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import useRegistrationStore from "../../store";
import { useEffect, useMemo, useState } from "react";
import { PAN_NO_REGEX } from "App/utils/constants";

const validationSchema = Yup.object().shape({
  PAN_No: Yup.string()
    .required("PAN Number is required")
    .trim()
    .matches(PAN_NO_REGEX, "Invalid PAN Number"),
  Name: Yup.string().required("Name is required"),
  Email: Yup.string().required("Email is required").email("Invalid Email"),
  Mobile: Yup.number()
    .required("Mobile is required")
    .min(6000000000, "invalid mobile number")
    .max(9999999999, "invalid mobile number"),
});

type BasicTypes = {
  handleSendOTP: (values: BasicFormTypes) => void;
  basicDetails: BasicFormTypes;
};

// main
const Basic = ({ handleSendOTP, basicDetails }: BasicTypes) => {
  const { loading, setKYCStatus } = useRegistrationStore();

  const [formType, setformType] = useState<"basic" | "opt">("basic");
  const [formValues, setformValues] = useState<BasicFormTypes>(basicDetails);

  // useEffect(() => {
  //   setformValues({ ...basicDetails });
  // }, [basicDetails]);

  return (
    <>
      {/* @ts-ignore */}
      <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
        Basic Details
      </Text>
      <Formik
        initialValues={formValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleSendOTP(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
        }) => (
          <Form>
            <Box css={{ p: 20 }}>
              <Box className="row">
                <Box className="col-md-6 col-sm-12 col-lg-6">
                  <Input
                    label="PAN Number"
                    name="PAN_No"
                    value={values?.PAN_No.toUpperCase()}
                    placeholder="PAN Number"
                    onChange={handleChange}
                    required
                    error={errors.PAN_No}
                    disabled
                  />
                </Box>
                <Box className="col-md-6 col-sm-12 col-lg-6">
                  <Input
                    label="Name As Per PAN"
                    name="Name"
                    value={values?.Name}
                    placeholder="Name As Per PAN"
                    onChange={handleChange}
                    required
                    error={errors.Name}
                  />
                </Box>
              </Box>
              <Box className="row">
                <Box className="col-md-6 col-sm-12 col-lg-6">
                  <Input
                    type="email"
                    label="Investor Email"
                    name="Email"
                    value={values?.Email}
                    placeholder="Investor Email"
                    onChange={handleChange}
                    required
                    error={errors.Email}
                  />
                </Box>
                <Box className="col-md-6 col-sm-12 col-lg-6">
                  <Input
                    type="number"
                    label="Investor Mobile"
                    name="Mobile"
                    value={values?.Mobile}
                    placeholder="Investor Mobile"
                    // onChange={handleChange}
                    onChange={(e: any) => {
                      const value: any = e?.target?.value || "";
                      if (value?.length > 10) return;
                      setFieldValue("Mobile", value);
                    }}
                    required
                    error={errors.Mobile}
                  />
                </Box>
              </Box>
              <Box className="row">
                <Box className="col"></Box>
                <Box className="col col-sm-auto">
                  <Button color="yellow" onClick={() => setKYCStatus(null)}>
                    Back
                  </Button>
                  <Button
                    type="submit"
                    color="yellow"
                    onClick={handleSubmit}
                    loading={loading}
                  >
                    Send OTP
                  </Button>
                </Box>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default Basic;
