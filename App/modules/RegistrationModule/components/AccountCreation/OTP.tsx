import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { OTPFormTypes } from "../../registrationTypes";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import useRegistrationStore from "../../store";
import { useEffect, useState } from "react";
import { encryptData } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import { generateOTP } from "App/api/registration";

const validationSchema = Yup.object().shape({
  OTP_E: Yup.string().required("Email OTP is required"),
  OTP_M: Yup.string().required("Mobile OTP is required"),
});

type OTPTypes = {
  handleVerifyOTP: (value: OTPFormTypes) => void;
  setformType: (type: "basic" | "otp") => void;
  accountObj: any;
};

// main
const OTP = ({ handleVerifyOTP, accountObj, setformType }: OTPTypes) => {
  const { loading } = useRegistrationStore((state: any) => state);
  const [formValues, setformValues] = useState<OTPFormTypes>({
    OTP_E: "",
    OTP_M: "",
  });
  const [counter, setcounter] = useState<number>(30);

  //*useEffects
  useEffect(() => {
    if (!counter) {
      return;
    }
    const interval = setTimeout(() => setcounter((prev) => prev - 1), 1000);
    return () => {
      clearInterval(interval);
    };
  }, [counter]);

  //*functions
  const resendOTP = async () => {
    try {
      setcounter(30);
      const obj: any = {
        email: accountObj?.Client_Email,
        mobile: accountObj?.Mobile,
        name: accountObj?.Client_Name,
        otpM: "",
        otpE: "",
      };
      // console.log(obj);
      const enc: any = encryptData(obj);
      await generateOTP(enc);
      toastAlert("success", "OTP has been resent!!");
    } catch (error: any) {
      console.log(error);
      setcounter(0);
      toastAlert("error", error);
    }
  };

  return (
    <>
      {/* @ts-ignore */}
      <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
        OTP Verification
      </Text>
      <Formik
        initialValues={formValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleVerifyOTP(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount,
        }) => (
          <Form>
            <Box css={{ p: 20 }}>
              <Box className="row">
                <Box className="col-md-6 col-sm-12 col-lg-12">
                  <Input
                    label="Email OTP"
                    name="OTP_E"
                    value={values?.OTP_E}
                    placeholder="Email OTP"
                    //onChange={handleChange}
                    // type="number"
                    //maxLength={4}
                    onChange={(e: any) => {
                      setFieldValue("OTP_E", e?.target?.value.length > 4 ? e?.target?.value.slice(0, 4) : e?.target?.value.replace(/\D/g, ''))
                    }}
                    required
                    error={submitCount ? errors.OTP_E : null}
                  />
                </Box>
              </Box>
              <Box className="row">
                <Box className="col-md-6 col-sm-12 col-lg-12">
                  <Input
                    label="Mobile OTP"
                    name="OTP_M"
                    value={values?.OTP_M}
                    placeholder="Mobile OTP"
                    //onChange={handleChange}
                    onChange={(e: any) => {
                      setFieldValue("OTP_M", e?.target?.value.length > 4 ? e?.target?.value.slice(0, 4) : e?.target?.value.replace(/\D/g, ''))
                    }}
                    required
                    error={submitCount ? errors.OTP_M : null}
                  />
                </Box>
              </Box>
              <Box className="row">
                <Box className="col"></Box>
                <Box className="col col-sm-auto">
                  <Button
                    color="yellow"
                    onClick={() => setformType("basic")}
                    loading={loading}
                  >
                    Back
                  </Button>
                  <Button
                    color="yellow"
                    onClick={resendOTP}
                    disabled={!!counter}
                  >
                    {counter ? `Resend OTP (${counter})` : `Resend OTP`}
                  </Button>
                  <Button
                    type="submit"
                    color="yellow"
                    onClick={handleSubmit}
                    loading={loading}
                  >
                    Verify OTP
                  </Button>
                </Box>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default OTP;
