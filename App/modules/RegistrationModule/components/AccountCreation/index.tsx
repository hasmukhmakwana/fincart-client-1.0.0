import Card from "@ui/Card";
import Box from "../../../../ui/Box/Box";
import {
  BasicFormTypes,
  OTPFormTypes,
  VerifyKYCTypes,
} from "../../registrationTypes";
import useRegistrationStore from "../../store";
import { useState } from "react";
import OTP from "./OTP";
import Basic from "./Basic";
import { generateOTP, validateOTP } from "App/api/registration";
import { encryptData } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";

type AccountCreationTypes = {
  handleAccountCreation: (value: any) => any;
  basicDetails: BasicFormTypes;
};

// main
const AccountCreation = ({
  handleAccountCreation,
  basicDetails,
}: AccountCreationTypes) => {
  const { verifyKYC, loading, setLoader } = useRegistrationStore(
    (state: any) => state
  );

  const [formType, setformType] = useState<"basic" | "otp">("basic");
  const [accountObj, setaccountObj] = useState<any>({
    Client_Name: "",
    Client_Email: "",
    Mobile: "",
  });

  const [basicValues, setbasicValues] = useState<any>(basicDetails);

  /** @handleSendOTP investor account creation */
  const handleSendOTP = async (formValues: BasicFormTypes) => {
    try {
      //set the basic details onchange values in basicvalues state
      setbasicValues({
        ...basicValues,
        Name: formValues?.Name,
        Email: formValues?.Email,
        Mobile: formValues?.Mobile,
      });

      setLoader(true);
      console.log("handleSendOTP: ", formValues);
      const obj: any = {
        email: formValues?.Email,
        mobile: formValues?.Mobile,
        name: formValues?.Name,
        otpM: "",
        otpE: "",
      };

      setaccountObj({
        PanNumber: formValues?.PAN_No,
        Client_Email: formValues?.Email,
        Mobile: formValues?.Mobile,
        Client_Name: formValues?.Name,
      });
      const encryptedData: any = encryptData(obj);
      await generateOTP(encryptedData);
      setLoader(false);
      setformType("otp");
      toastAlert("success", "OTP has been sent to your email and mobile");
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  /** @handleVerifyOTP email and mobile OTP */
  const handleVerifyOTP = async (formValues: OTPFormTypes) => {
    try {
      const obj: any = {
        email: accountObj?.Client_Email,
        mobile: accountObj?.Mobile,
        name: accountObj?.Client_Name,
        otpM: formValues?.OTP_M,
        otpE: formValues?.OTP_E,
      };

      setLoader(true);
      const encryptedData: any = encryptData(obj);
      await validateOTP(encryptedData);
      setLoader(false);

      /**
       * @ACCOUNT_CREATION
       *
       */
      handleAccountCreation(accountObj);
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  return (
    <Box background="">
      <Box className="">
        <Box className="row row-cols-1 row-cols-md-2 justify-content-center">
          <Box className="col">
            <Card noshadow>
              {formType === "basic" ? (
                <Basic
                  handleSendOTP={handleSendOTP}
                  basicDetails={basicValues}
                />
              ) : (
                <OTP
                  handleVerifyOTP={handleVerifyOTP}
                  accountObj={accountObj}
                  setformType={setformType}
                />
              )}
            </Card>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default AccountCreation;
