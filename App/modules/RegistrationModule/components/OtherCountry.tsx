import Box from '@ui/Box/Box'
import React from 'react'
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import { useRouter } from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';
const OtherCountry = () => {
    const router = useRouter();
    const { setContactUsVal } = useContactUsStore();
    return (
        <Box className="row my-4 justify-content-center" >
            <Box className="col-auto m-2 p-4 rounded-4" css={{ background: "var(--colors-blue)" }}>
                <Box className="px-md-4 my-4 text-center" >
                    <Text size="h5" css={{ color: "#005cb3" }}>Currently, the E-KYC process is not permitted outside India.<br />
                        Kindly reach out to our team by sharing your query. <br />
                        To proceed <Button className="ms-1" color="yellow" css={{ fontSize: "0.8rem" }}
                            onClick={() => {
                                setContactUsVal({
                                    SubjectId: "9",
                                    Description: ""
                                }
                                );
                                router.push('/Support/ContactUs');
                            }}>Click here</Button ></Text>
                </Box>
            </Box>
        </Box>
    )
}

export default OtherCountry