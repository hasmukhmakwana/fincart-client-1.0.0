import React, { useState, useEffect } from "react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import ArrowLeftShort from "App/icons/ArrowLeftShort";
import Input from "@ui/Input";
import useRegistrationStore from "../../store";
import { toastAlert } from "App/shared/components/Layout";
import { BankDetails } from "../../registrationTypes";
import Textarea from "@ui/Textarea";
import { uploadCancelledCheque, pennyTransfer, verifyBanking, getSummary, getKYC_CAF_Bank } from "App/api/registration";
import { convertBase64, encryptData, plainBase64 } from "App/utils/helpers";
import RightArrowShort from "App/icons/RightArrowShort";
import { WEB_URL } from "App/utils/constants";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import { parseInt } from "lodash";
import Loader from "App/shared/components/Loader";

/**
 * @CONSTANTS
 */

const AMOUNT_RANGE = [
  1.01, 1.02, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09, 1.1,
];

let schemaObj: any = {
  cancelledCheque: Yup.mixed().required("Cancelled Cheque is required"),
};

let schemaObj2: any = {
  accountNumber: Yup.string()
    .required("Account Number is required")
    .trim()
    .matches(/[0-9]{9,18}/, "Invalid Account Number"),
  bankAddress: Yup.string().required("Bank Address is required"),
  bankName: Yup.string().required("Bank Name is required"),
  branch: Yup.string().required("Branch Name is required"),
  IFSC: Yup.string().required("IFSC is required"),
  MICR: Yup.string().required("MICR is required"),
  nameAsBank: Yup.string().required("Name As Per Bank is required"),
  bankCity: Yup.string().required("Branch City is required"),
};

type BankDetailsEventTypes = {
  onNextClickParent: () => void;
  onBackClickParent: () => void;
};

//*main
const BankDetailsTab = ({
  onNextClickParent,
  onBackClickParent,
}: BankDetailsEventTypes) => {
  const {
    BasicID,
    loading,
    setLoader,
    bankDetails,
    setBankingDetails,
    summaryInfo,
    setSummaryInfo,
    setCommingFrom,
  } = useRegistrationStore();

  console.log(summaryInfo);
  const [openTransfer, setOpenTransfer] = useState<any>(false);
  const [openPenny, setOpenPenny] = useState<any>(false);
  const [isBank, setisBank] = useState<boolean>(summaryInfo?.Steps?.isBank);
  const [isKYC, setisKYC] = useState<boolean>((summaryInfo?.Account_Init_Details?.KYC_Status == "Y"));
  const [amount, setamount] = useState<string>("");
  const [amtErr, setAmtErr] = useState<boolean>(false);
  const [bankDetail, setBankDetail] = useState<any>({});
  const [scanned, setscanned] = useState<boolean>(
    !!summaryInfo?.Bank_Details?.ChequeFilePath_Signzy
  );

  const [isUploaded, setisUploaded] = useState<boolean>(
    !!summaryInfo?.Bank_Details?.ChequeFilePath_Signzy
  );

  const [isIFSCLoading, setIsIFSCLoading] = useState<boolean>(false);

  // const [documentValues, setdocumentValues] = useState<any>({
  //   CcDoc: bankDetails.cancelledCheque,
  // });
  //*functions
  //submit the page
  const clickOnSubmit = async (formValues: BankDetails) => {
    try {
      // console.log(formValues);
      if (!formValues.accountNumber) {
        toastAlert("warn", "Please upload valid cheque!");
        return;
      }
      setLoader(true);
      const pennyEnc: any = encryptData({
        BasicID: BasicID,
        accountnumber: formValues.accountNumber,
        bankaddress: formValues.bankAddress,
        bankname: formValues.bankName,
        branch: formValues.branch,
        ifsc: formValues.IFSC,
        micr: formValues.MICR,
        bankcity: formValues.bankCity,
        nameasperbank: formValues.nameAsBank,
        AccountType: formValues.accountType || "SB",
      });

      await pennyTransfer(pennyEnc);
      setOpenTransfer(false);
      setOpenPenny(true);
      /**amount range call */

    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  const verifyBankDetails = async () => {
    console.log("amount ", isNaN(Number(amount)));
    if (amount === "") {
      setAmtErr(true);
      return;
    }
    else if (isNaN(Number(amount))) {
      setAmtErr(true);
      return;
    }
    else if (parseFloat(amount) <= 1.0 && parseFloat(amount) >= 2.0) {
      setAmtErr(true);
      return;
    }
    else {
      let check: boolean = true;
      try {
        const verifyEnc: any = encryptData({
          BasicID: BasicID,
          Amount: amount,
        });

        const verifyResult: any = await verifyBanking(verifyEnc);
        // console.log({ verifyResult });
        if (verifyResult?.status === "Success") {
          setOpenPenny(false);
          check = false;
          setLoader(false);
          callSummary();
          onNextClickParent();
          return;
        }

        if (check) {
          setOpenPenny(false);
          toastAlert("error", "Bank Verification Failed!");
        }
      } catch (error: any) {
        console.log(error);
        setOpenPenny(false);
        setLoader(false);
        toastAlert("error", error);
      }
    }
  }
  //cancelled cheque SCAN
  const clickOnChequeScan = async (formValues: any) => {
    try {
      setLoader(true);
      const postData: any = {
        BasicID: BasicID,
        Cheque_File: plainBase64(formValues.cancelledCheque),
        Platform_Type: "WEB",
      };

      const enc: any = encryptData(postData);

      const result = await uploadCancelledCheque(enc);

      setLoader(false);
      bankDetailsSetup({
        cancelledCheque: formValues.cancelledCheque,
        ...result?.data,
      });
      setscanned(true);
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  //*helper funs
  const bankDetailsSetup = (data: any = {}) => {
    setBankingDetails({
      cancelledCheque: data?.cancelledCheque || null,
      accountNumber: data?.accountnumber || "",
      bankAddress: data?.bankaddress || "",
      bankName: data?.bankname || "",
      branch: data?.branch || "",
      IFSC: data?.ifsc || "",
      MICR: data?.micr || "",
      nameAsBank: data?.nameasperbank || "",
      bankCity: data?.bankcity || "",
      accountType: data?.AccountType || "",
    });
  };

  const callSummary = async () => {
    try {
      const enc: any = encryptData(BasicID, true);
      const result: any = await getSummary(enc);
      setSummaryInfo(result?.data);
    } catch (error) {
      console.log(error);
    }
  };

  console.log(bankDetails, "Bank Details...");

  useEffect(() => {
    console.log(summaryInfo, "basicDetails");
    return () => {

    }
  }, [])

  //*main return
  return (
    <Box background="">
      <Box className="">
        <Box className="row">
          <Box>
            <Card noshadow>
              <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
                Bank Account Details
                {summaryInfo?.Account_Creation_Details?.ClientName && (
                  <span className="ms-1">
                    -
                    <span className="ms-1 text-primary">
                      {summaryInfo?.Account_Creation_Details?.ClientName}
                    </span>
                  </span>
                )}
              </Text>
              <Formik
                initialValues={bankDetails}
                validationSchema={Yup.object().shape(schemaObj2)}
                enableReinitialize
                onSubmit={(values, { resetForm }) => {
                  setOpenTransfer(true);
                  setBankDetail(values);
                  console.log("transfer ", openTransfer);
                  console.log("Pennny ", openPenny);
                }}
              >
                {({
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  errors,
                  touched,
                  setFieldValue,
                  submitCount,
                }) => (
                  <Box css={{ p: 20 }}>
                    {summaryInfo?.Account_Init_Details?.KYC_Status === 'N' ?
                      <Formik
                        initialValues={bankDetails}
                        validationSchema={Yup.object().shape(schemaObj)}
                        enableReinitialize
                        onSubmit={(values) => {
                          clickOnChequeScan(values);
                        }}
                      >
                        {({
                          values,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,
                          errors,
                          touched,
                          setFieldValue,
                          submitCount,
                        }) => (
                          <Form>
                            <Box className="row align-items-end">
                              <Box className="col-9 col-md-4 col-sm-4 col-lg-4">
                                <Text
                                  color="mediumBlue"
                                  size="h6"
                                  space="letterspace"
                                >
                                  Upload Cancelled Cheque
                                </Text>
                                <Input
                                  parentCSS={{ mb: 0 }}
                                  type="file"
                                  className="form-control"
                                  name="cancelledCheque"
                                  // value={values?.cancelledCheque||""}
                                  onChange={async (
                                    e: React.ChangeEvent<HTMLInputElement>
                                  ) => {
                                    try {
                                      const file: any =
                                        e?.target?.files?.[0] || null;
                                      const base64 = await convertBase64(file);
                                      setFieldValue("cancelledCheque", base64);
                                      setisUploaded(false);
                                    } catch (error: any) {
                                      toastAlert("error", error);
                                    }
                                  }}
                                  required
                                  error={
                                    submitCount ? errors.cancelledCheque : null
                                  }
                                />
                              </Box>
                              <Box className="col col-auto col-sm-auto">
                                <Button
                                  color="yellow"
                                  onClick={handleSubmit}
                                  loading={loading}
                                  disabled={isUploaded}
                                >
                                  Scan
                                </Button>
                              </Box>
                              <Box className="col-12"></Box>
                              <Box className="col-md-4 col-sm-4 col-lg-4">
                                <img
                                  src={
                                    values?.cancelledCheque ||
                                    WEB_URL + "/CancelledChequeImage.png"
                                  }
                                  width={"100%"}
                                  className="mt-4"
                                ></img>
                              </Box>
                            </Box>
                          </Form>
                        )}
                      </Formik>
                      : null}
                    <Box className="row" css={{ mt: 20 }}>
                      <Box className="col-md-4 col-sm-4 col-lg-4">
                        <Input
                          label="IFSC Code"
                          name="IFSC"
                          value={values?.IFSC}
                          placeholder="IFSC Code"
                          onChange={async (e: any) => {
                            setFieldValue("IFSC", e.target.value);
                            if (e.target.value.length === 11) {
                              setIsIFSCLoading(true);
                              try {
                                const enc: any = encryptData(e.target.value, true);
                                const result: any = await getKYC_CAF_Bank(enc);
                                const IFSCCode = result?.data;
                                setFieldValue("bankAddress", IFSCCode?.BankAddress)
                                setFieldValue("bankName", IFSCCode?.BankName)
                                setFieldValue("bankCity", IFSCCode?.City)
                                setFieldValue("MICR", IFSCCode?.MICR)
                                setFieldValue("branch", IFSCCode?.Branch)
                              } catch (error) {
                                setFieldValue("bankAddress", "")
                                setFieldValue("bankName", "")
                                setFieldValue("bankCity", "")
                                setFieldValue("MICR", "")
                                setFieldValue("branch", "")

                                toastAlert("error", error);
                              }
                              setIsIFSCLoading(false);
                            }
                          }
                          }
                          disabled={!scanned && !isKYC}
                          required
                          error={submitCount ? errors.IFSC : null}
                          maxLength={11}
                        />
                      </Box>
                      {isIFSCLoading ? <Loader></Loader> : <>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Account Number"
                            name="accountNumber"
                            value={values?.accountNumber}
                            placeholder="Account Number"
                            onChange={handleChange}
                            disabled={!scanned && !isKYC}
                            maxLength={18}
                            required
                            error={submitCount ? errors.accountNumber : null}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Name As Per Bank"
                            name="nameAsBank"
                            value={values?.nameAsBank}
                            placeholder="Name As Per Bank"
                            onChange={(e: any) =>
                              setFieldValue("nameAsBank", e.target.value)
                            }
                            disabled={!scanned && !isKYC}
                            required
                            error={submitCount ? errors.nameAsBank : null}
                            maxLength={200}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Textarea
                            label="Bank Address"
                            placeholder="Bank Address"
                            name="bankAddress"
                            value={values?.bankAddress}
                            onChange={(e: any) =>
                              setFieldValue("bankAddress", e.target.value)
                            }
                            disabled={!scanned && !isKYC}
                            required
                            error={submitCount ? errors.bankAddress : null}
                          ></Textarea>
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Bank Name"
                            name="bankName"
                            value={values?.bankName}
                            placeholder="Bank Name"
                            onChange={(e: any) =>
                              setFieldValue("bankName", e.target.value)
                            }
                            disabled={!scanned && !isKYC}
                            required
                            error={submitCount ? errors.bankName : null}
                            maxLength={200}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Branch City"
                            name="bankCity"
                            value={values?.bankCity}
                            placeholder="Branch City"
                            onChange={(e: any) =>
                              setFieldValue("bankCity", e.target.value)
                            }
                            disabled={!scanned && !isKYC}
                            required
                            error={submitCount ? errors.bankCity : null}
                            maxLength={25}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            type="number"
                            label="MICR Code"
                            name="MICR"
                            value={values?.MICR}
                            placeholder="MICR Code"
                            onChange={(e: any) =>
                              setFieldValue("MICR", e.target.value)
                            }
                            disabled={!scanned && !isKYC}
                            required
                            error={submitCount ? errors.MICR : null}
                            maxLength={9}
                          />
                        </Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Input
                            label="Branch Name"
                            name="branch"
                            value={values?.branch}
                            placeholder="Branch"
                            onChange={(e: any) =>
                              setFieldValue("branch", e.target.value)
                            }
                            disabled={!scanned && !isKYC}
                            required
                            error={submitCount ? errors.branch : null}
                            maxLength={150}
                          />
                        </Box>
                      </>
                      }
                    </Box>
                    <Box className="row">
                      <Box className="col"></Box>
                      <Box className="col col-sm-auto text-end">
                        <Button color="yellow" onClick={onBackClickParent} loading={loading}>
                          <ArrowLeftShort size="20" />
                          Back
                        </Button>
                        {isBank && (
                          <Button
                            color="yellow"
                            loading={loading}
                            onClick={() => {
                              setCommingFrom("next");
                              onNextClickParent();
                            }}
                          >
                            Next
                            <RightArrowShort size="20" />
                          </Button>
                        )}
                        <Button
                          type="submit"
                          color="yellow"
                          onClick={handleSubmit}
                          loading={loading}
                        >
                          Update & Next
                          <RightArrowShort size="20" />
                        </Button>
                      </Box>
                    </Box>
                  </Box>
                )}
              </Formik>
            </Card>
          </Box>
        </Box>
      </Box>
      <DialogModal
        open={openTransfer}
        setOpen={setOpenTransfer}
        css={{
          "@bp0": { width: "40%" },
          "@bp1": { width: "30%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            <Box className="row">
              <Box className="col-auto"><Text css={{ color: "var(--colors-blue1)" }}>Penny Transfer</Text></Box>
              <Box className="col"></Box>
              <Box className="col-auto"></Box>
            </Box>
          </Box>
          <Box
            className="text-center modal-body p-3"
            css={{ padding: "0.5rem" }}
          >
            <Box className="row" css={{ ml: 10 }}>
              <Box className="col-lg-10 col-md-10 col-sm-12">
                <Box>
                  <Box>
                    Please confirm to initiate penny transfer to your account to confirm your bank details.
                  </Box>
                </Box>
              </Box>
              <Box css={{ mt: 20, pt: 10, mb: 20, mr: 20 }} className="text-end">
                <Button
                  type="submit"
                  color="yellow"
                  onClick={() => { setOpenTransfer(false) }}
                  loading={loading}>
                  Cancel
                </Button>
                <Button
                  type="submit"
                  color="yellow"
                  onClick={() => { clickOnSubmit(bankDetail); }}
                  loading={loading}
                >
                  Confirm
                </Button>
              </Box>
            </Box>
          </Box>
        </Box>
      </DialogModal>


      <DialogModal
        open={openPenny}
        setOpen={setOpenPenny}
        css={{
          "@bp0": { width: "40%" },
          "@bp1": { width: "30%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            <Box className="row">
              <Box className="col-auto"><Text css={{ color: "var(--colors-blue1)" }}>Bank Account - Amount Received</Text></Box>
              <Box className="col"></Box>
              <Box className="col-auto"></Box>
            </Box>
          </Box>
          <Box
            className="text-center modal-body p-3 modal-body-scroll"
            css={{ padding: "0.5rem" }}
          >
            <Box className="row" css={{ ml: 10 }}>
              <Box className="col-md-11 col-sm-12">
                <Box>
                  <Input label="Enter Exact Amount received in Account" name="Amount" placeholder="Enter Amount(e.g 1.50)"
                    required onChange={(e: any) => { setamount(e.target.value) }}
                    error={amtErr ? "Please Enter Valid Amount" : null} />
                </Box>
              </Box>
            </Box>
            <Box css={{ mt: 20, pt: 10, mb: 20, mr: 20 }} className="text-end">
              {/* <Button
                type="submit"
                color="yellow"
                onClick={() => { setOpenPenny(false) }}>
                Previous
              </Button> */}
              <Button
                type="submit"
                color="yellow"
                onClick={() => { verifyBankDetails() }}>
                Proceed
              </Button>
            </Box>
          </Box>
        </Box >
      </DialogModal >
    </Box >
  );
};
export default BankDetailsTab;
