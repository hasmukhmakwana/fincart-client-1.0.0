import Box from '@ui/Box/Box'
import React, { useEffect } from 'react'
import Text from '@ui/Text/Text'
import { Button } from '@ui/Button/Button'
// import ModalDialog from '@ui/ModalDialog/ModalDialog'
import ModalDialog from '@ui/AlertDialog/ModalDialog'
import useRegistrationStore from '../store'
const RegisterModal = ({ status }: any) => {
    const { kycSt, setKycSt } = useRegistrationStore();

    return (
        <ModalDialog
            open={kycSt}
            setOpen={setKycSt}
            hideCloseBtn={true}
            css={{
                "@bp0": { width: "60%" },
                "@bp1": { width: "30%" },
            }}
        >
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    <Box className="row justify-content-start">
                        <Box className="col-auto">
                            <Text css={{ color: "var(--colors-blue1)" }}>KYC Status</Text></Box>
                    </Box>
                </Box>
                <Box className="modal-body p-3">
                    <Box className="my-1 mx-2">
                        <Box css={{ padding: "0.3rem", fontSize: "0.9rem" }}>
                            <Text>{status === "Y" ?
                                <>Hurray ! The KYC process for the given PAN is already completed </> : <>
                                    Oh ! The KYC process for the given PAN is not completed, Please click on Proceed to complete</>}
                            </Text>
                        </Box>
                        <Box css={{ pt: 10 }} className="text-end">
                            <Button color="yellow" onClick={() => { setKycSt(false) }}>
                                Ok
                            </Button>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </ModalDialog>
    )
}

export default RegisterModal

