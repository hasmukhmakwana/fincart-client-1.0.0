import React, { useEffect, useState } from "react";
import { PAGE_TYPES } from "../../constants";
import POI from "./POI";
import POA from "./POA";
import IPV from "./IPV";
import SIGN from "./SIGN";
import useRegistrationStore from "../../store";

type DocumentsEventTypes = {
  onNextClickParent: () => void;
  onBackClickParent: () => void;
};

//*main
const DocumentsTab = ({
  onNextClickParent,
  onBackClickParent,
}: DocumentsEventTypes) => {
  const { KYCStatus, summaryInfo, commingFrom } = useRegistrationStore();
  const [pageType, setpageType] = useState<string>(PAGE_TYPES.IdentityProof);
  //* useEffects
  useEffect(() => {
    if (commingFrom === "next") {
      return;
    } else if (commingFrom === "back") {
      goToCurrentTab(PAGE_TYPES.CapturePhotoVideo);
    } else {
      if (summaryInfo?.Steps?.isPOI) {
        let comp: string = PAGE_TYPES.IdentityProof;
        if (summaryInfo?.Steps?.isPOA) {
          comp = PAGE_TYPES.AddressProof;
          if (
            summaryInfo?.Steps?.isVedio &&
            !!summaryInfo?.IPV_Details?.Client_Photo_Fincart
          ) {
            comp = PAGE_TYPES.CapturePhotoVideo;
            if (summaryInfo?.Steps?.isSign) {
              comp = PAGE_TYPES.UploadSignature;
            }
          }
        }
        goToCurrentTab(comp);
      }
    }
    return () => {};
  }, [summaryInfo]);

  //*functions

  const goToCurrentTab = (value: any = null) => {
    const check: string = value || pageType;
    switch (check) {
      case PAGE_TYPES.IdentityProof:
        setpageType(PAGE_TYPES.AddressProof);
        break;

      case PAGE_TYPES.AddressProof:
        setpageType(
          KYCStatus === "N"
            ? PAGE_TYPES.CapturePhotoVideo
            : PAGE_TYPES.UploadSignature
        );
        break;

      case PAGE_TYPES.CapturePhotoVideo:
        setpageType(PAGE_TYPES.UploadSignature);
        break;

      case PAGE_TYPES.UploadSignature:
        console.log("onNextClick --- ", PAGE_TYPES.UploadSignature);
        //successfully uploaded the signature then
        onNextClickParent();
        break;

      default:
        break;
    }
  };

  const onNextClick = () => {
    switch (pageType) {
      case PAGE_TYPES.IdentityProof:
        setpageType(PAGE_TYPES.AddressProof);
        break;

      case PAGE_TYPES.AddressProof:
        setpageType(
          KYCStatus === "N"
            ? PAGE_TYPES.CapturePhotoVideo
            : PAGE_TYPES.UploadSignature
        );
        break;

      case PAGE_TYPES.CapturePhotoVideo:
        setpageType(PAGE_TYPES.UploadSignature);
        break;

      case PAGE_TYPES.UploadSignature:
        console.log("onNextClick --- ", PAGE_TYPES.UploadSignature);
        //successfully uploaded the signature then
        onNextClickParent();
        break;

      default:
        break;
    }
  };

  const onBackClick = () => {
    switch (pageType) {
      case PAGE_TYPES.IdentityProof:
        // setpageType(PAGE_TYPES.AddressProof)
        break;

      case PAGE_TYPES.AddressProof:
        setpageType(PAGE_TYPES.IdentityProof);
        break;

      case PAGE_TYPES.CapturePhotoVideo:
        setpageType(PAGE_TYPES.AddressProof);
        break;

      case PAGE_TYPES.UploadSignature:
        setpageType(
          KYCStatus === "N"
            ? PAGE_TYPES.CapturePhotoVideo
            : PAGE_TYPES.AddressProof
        );

        break;

      default:
        break;
    }
  };

  const getCurrentPage = () => {
    switch (pageType) {
      case PAGE_TYPES.IdentityProof:
        return (
          <POI onNextClick={onNextClick} onBackClick={onBackClickParent} />
        );
      // return <POA onNextClick={onNextClick} onBackClick={onBackClick} />;
      // return <IPV onNextClick={onNextClick} onBackClick={onBackClick} />;
      // return <SIGN onNextClick={onNextClick} onBackClick={onBackClick} />;

      case PAGE_TYPES.AddressProof:
        return <POA onNextClick={onNextClick} onBackClick={onBackClick} />;

      case PAGE_TYPES.CapturePhotoVideo:
        return <IPV onNextClick={onNextClick} onBackClick={onBackClick} />;

      case PAGE_TYPES.UploadSignature:
        return <SIGN onNextClick={onNextClick} onBackClick={onBackClick} />;

      default:
        break;
    }
  };

  //*main return
  return <>{getCurrentPage()}</>;
};
export default DocumentsTab;
