import React, { useState } from "react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import ArrowLeftShort from "App/icons/ArrowLeftShort";
import Input from "@ui/Input";
import TabNumbers from "./TabNumbers";
import { PAGE_TYPES } from "../../constants";
import useRegistrationStore from "../../store";
import { toastAlert } from "App/shared/components/Layout";
import { POITypes } from "../../registrationTypes";
import { getSummary, updatePOI, uploadPOI } from "App/api/registration";
import {
  convertBase64,
  encryptData,
  formatDate,
  plainBase64,
} from "App/utils/helpers";
import RightArrowShort from "App/icons/RightArrowShort";
import { PAN_NO_REGEX, WEB_URL } from "App/utils/constants";

let schemaObj: any = {
  panCard: Yup.mixed().required("PAN Card is required"),
};

let schemaObj2: any = {
  name: Yup.string().required("Name is required"),
  DOB: Yup.string().required("DOB is required"),
  fatherName: Yup.string().required("Father Name is required"),
  panNumber: Yup.string()
    .required("PAN Number is required")
    .trim()
    .matches(PAN_NO_REGEX, "Invalid PAN Number"),
};

type DocumentsEventTypes = {
  onNextClick: () => void;
  onBackClick: () => void;
};

//*main
const POI = ({ onNextClick, onBackClick }: DocumentsEventTypes) => {
  const {
    BasicID,
    KYCStatus,
    loading,
    setLoader,
    documents,
    setDocumentDetails,
    summaryInfo,
    setSummaryInfo,
  } = useRegistrationStore();
  const [scanned, setscanned] = useState<boolean>(
    !!summaryInfo?.POI_Details?.POI_Path_Fincart
  );
  const [isPOI, setisPOI] = useState(summaryInfo?.Steps?.isPOI);
  const [isUploaded, setisUploaded] = useState<boolean>(!!summaryInfo?.POI_Details?.POI_Path_Fincart);

  //*functions
  //submit the page
  const clickOnSubmit = async (formValues: POITypes) => {
    try {
      setLoader(true);
      const obj: any = {
        BasicID: BasicID,
        DOB: formatDate(formValues.DOB, "m/d/y"),
        FatherName: formValues.fatherName,
        ClientName: formValues.name,
        IsKYC: KYCStatus,
        PanNumber: formValues.panNumber,
      };
      const enc: any = encryptData(obj);
      await updatePOI(enc);
      setLoader(false);
      callSummary();
      onNextClick();
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  //PAN SCAN
  const clickOnPanScan = async (formValues: { panCard: any }) => {
    try {
      setLoader(true);
      const obj: any = {
        BasicID: BasicID,
        Pan_File: plainBase64(formValues?.panCard),
        Platform_Type: "WEB",
      };

      const enc: any = encryptData(obj);
      const result = await uploadPOI(enc);
      setLoader(false);
      documentDetailsSetup(result?.data);
      setscanned(true);
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  //*helper funs
  const documentDetailsSetup = (data: any = {}) => {
    setDocumentDetails(
      {
        panCard: documents.POI.panCard,
        name: data?.Client_Name || "",
        DOB: formatDate(data?.dob || "d-m-y"),
        fatherName: data?.fathername || "",
        panNumber: data?.panNumber || "",
      },
      "POI"
    );
  };

  const callSummary = async () => {
    try {
      const enc: any = encryptData(BasicID, true);
      const result: any = await getSummary(enc);
      setSummaryInfo(result?.data);
    } catch (error) {
      console.log(error);
    }
  };

  //*main return
  return (
    <>
      <Card noshadow id="ProofofIdentity">
        {/* @ts-ignore */}
        <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
          Proof of Identity
          {summaryInfo?.Account_Creation_Details?.ClientName && (
            <span className="ms-1">
              -
              <span className="ms-1 text-primary">
                {summaryInfo?.Account_Creation_Details?.ClientName}
              </span>
            </span>
          )}
        </Text>
        <Formik
          initialValues={documents.POI}
          validationSchema={Yup.object().shape(schemaObj2)}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            clickOnSubmit(values);
            // resetForm();
          }}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue,
            submitCount,
          }) => (
            <Form>
              <Box css={{ p: 20 }}>
                <Formik
                  initialValues={{ panCard: documents.POI.panCard }}
                  validationSchema={Yup.object().shape(schemaObj)}
                  enableReinitialize
                  onSubmit={(values) => {
                    clickOnPanScan(values);
                  }}
                >
                  {({
                    values,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    errors,
                    touched,
                    setFieldValue,
                  }) => (
                    <Box className="row align-items-end">
                      <Box className="col-9 col-md-4 col-sm-4 col-lg-4">
                        {/* @ts-ignore */}
                        <Text color="mediumBlue" size="h6" space="letterspace">
                          Upload PAN Card
                        </Text>
                        <Box>
                          <Input
                            parentCSS={{ mb: 0 }}
                            type="file"
                            className="form-control"
                            name="panCard"
                            // value={values?.panCard}
                            onChange={async (
                              e: React.ChangeEvent<HTMLInputElement>
                            ) => {
                              try {
                                const file: any = e?.target?.files?.[0] || null;
                                const base64 = await convertBase64(file);
                                setisUploaded(false)
                                setFieldValue("panCard", base64);
                              } catch (error: any) {
                                toastAlert("error", error);
                              }
                            }}
                            required
                            error={errors.panCard}
                          />
                        </Box>
                      </Box>
                      <Box className="col col-auto col-sm-auto">
                        <Button
                          type="submit"
                          color="yellow"
                          onClick={handleSubmit}
                          loading={loading}
                          disabled={isUploaded}
                        >
                          Scan
                        </Button>
                      </Box>
                      <Box className="col-12"></Box>
                      <Box className="col-md-4 col-sm-4 col-lg-4">
                        <img
                          src={values?.panCard || WEB_URL + "/PanCard.png"}
                          width={"100%"}
                          className="mt-4"
                        ></img>
                      </Box>
                    </Box>
                  )}
                </Formik>

                <Box className="row" css={{ mt: 20 }}>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="Name"
                      placeholder="Name"
                      name="name"
                      value={values?.name}
                      onChange={handleChange}
                      disabled={!scanned}
                      required
                      error={submitCount ? errors.name : null}
                    />
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="Father's Name"
                      placeholder="Father Name"
                      name="fatherName"
                      value={values?.fatherName}
                      onChange={handleChange}
                      disabled={!scanned}
                      required
                      error={submitCount ? errors.fatherName : null}
                    />
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="Date of Birth"
                      placeholder="DD-MM-YYYY"
                      type="date"
                      name="DOB"
                      value={values?.DOB}
                      onChange={handleChange}
                      disabled={!scanned}
                      required
                      error={submitCount ? errors.DOB : null}
                    />
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="PAN Number"
                      placeholder="PAN Number"
                      name="panNumber"
                      value={values?.panNumber.toUpperCase()}
                      onChange={handleChange}
                      disabled={!scanned}
                      maxLength={10}
                      required
                      error={submitCount ? errors.panNumber : null}
                    />
                  </Box>
                </Box>
                <TabNumbers currentPage={PAGE_TYPES.IdentityProof} />
                <Box className="row">
                  <Box className="col"></Box>
                  <Box className="col col-auto text-end">
                    <Button color="yellow" onClick={onBackClick}>
                      <ArrowLeftShort size="20" /> Back
                    </Button>
                    {isPOI && (
                      <Button color="yellow" onClick={onNextClick}>
                        Next
                        <RightArrowShort size="20" />
                      </Button>
                    )}
                    <Button
                      type="submit"
                      color="yellow"
                      onClick={handleSubmit}
                      loading={loading}
                    >
                      Update & Next
                      <RightArrowShort size="20" />
                    </Button>
                  </Box>
                </Box>
              </Box>
            </Form>
          )}
        </Formik>
      </Card>
    </>
  );
};
export default POI;
