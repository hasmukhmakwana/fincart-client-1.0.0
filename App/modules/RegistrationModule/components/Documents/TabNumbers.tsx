import clsx from "clsx";
import { useState } from "react";
import Box from "../../../../ui/Box/Box";
import { PAGE_TYPES } from "../../constants";
import useRegistrationStore from "../../store";
type TabNumbersEventTypes = {
  currentPage: string;
};
const TabNumbers = ({ currentPage }: TabNumbersEventTypes) => {
  const { KYCStatus } = useRegistrationStore();

  const stepArr = [
    {
      value: PAGE_TYPES.IdentityProof,
      step: 1,
      label: "Proof of Identity",
      show: true,
    },
    {
      value: PAGE_TYPES.AddressProof,
      step: 2,
      label: "Proof of Address",
      show: true,
    },
    {
      value: PAGE_TYPES.CapturePhotoVideo,
      step: 3,
      label: "In Person Verification",
      show: KYCStatus === "N",
    },
    {
      value: PAGE_TYPES.UploadSignature,
      step: KYCStatus === "N" ? 4 : 3,
      label: "Upload Signature",
      show: true,
    },
  ];

  const [stepNo, setstepNo] = useState<any>(
    stepArr.filter((i) => i.value === currentPage)?.[0]?.step
  );

  return (
    <>
      <Box className="row">
        <Box className="col steps">
          {stepArr.map(
            (i, index) =>
              i.show && (
                <Box
                  key={index}
                  className={clsx("step", {
                    ["active"]: i.step < stepNo,
                  })}
                >
                  <Box className="stepNumber">{i.step}</Box>
                  <Box className="stepText">{i.label}</Box>
                </Box>
              )
          )}
        </Box>
      </Box>
    </>
  );
};
export default TabNumbers;
