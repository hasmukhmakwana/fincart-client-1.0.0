import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import ArrowLeftShort from "App/icons/ArrowLeftShort";
import Input from "@ui/Input";
import TabNumbers from "./TabNumbers";
import { PAGE_TYPES } from "../../constants";
import useRegistrationStore from "../../store";
import { toastAlert } from "App/shared/components/Layout";
import { SIGNTypes } from "../../registrationTypes";
import { uploadSign, createEAadhaarSign } from "App/api/registration";
import { convertBase64, encryptData, plainBase64 } from "App/utils/helpers";
import styles from "../../Registration.module.scss";

import RightArrowShort from "App/icons/RightArrowShort";

let schemaObj: any = {
  signature: Yup.mixed().required("Your Signature is required"),
};

type DocumentsEventTypes = {
  onNextClick: () => void;
  onBackClick: () => void;
};

//*main
const SIGN = ({ onNextClick, onBackClick }: DocumentsEventTypes) => {
  const {
    BasicID,
    loading,
    setLoader,
    documents,
    setBankingDetails,
    summaryInfo,
  } = useRegistrationStore();
  const [isSign, setisSign] = useState(summaryInfo?.Steps?.isSign);
  const [isSignChanged, setIsSignChanged] = useState(false);

  //*functions
  //submit the page
  const clickOnSubmit = async (formValues: SIGNTypes) => {
    try {
      //console.log(formValues);
      if (isSignChanged) {
        setLoader(true);
        const obj: any = {
          BasicID: BasicID,
          Platform_Type: "DESKTOP",
          Sign_File: plainBase64(formValues.signature),
        };

        console.log(obj);

        const enc: any = encryptData(obj);
        const result = await uploadSign(enc);

        console.log(result);

        setLoader(false);
      }
      onNextClick();
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  //*main return
  return (
    <>
      <Box background="">
        <Box className="">
          <Box className="row">
            <Box>
              <Card noshadow id="Signature">
                <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
                  Upload Signature
                  {summaryInfo?.Account_Creation_Details?.ClientName && (
                    <span className="ms-1">
                      -
                      <span className="ms-1 text-primary">
                        {summaryInfo?.Account_Creation_Details?.ClientName}
                      </span>
                    </span>
                  )}
                </Text>
                <Formik
                  initialValues={documents.SIGN}
                  validationSchema={Yup.object().shape(schemaObj)}
                  enableReinitialize
                  onSubmit={(values) => {
                    clickOnSubmit(values);
                  }}
                >
                  {({
                    values,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    errors,
                    touched,
                    setFieldValue,
                  }) => (
                    <Box css={{ p: 20 }}>
                      {/* <Box className="row align-items-end">
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <Text color="blue1" size="h6" space="letterspace">
                            Upload Signature
                          </Text>
                          <Box>
                            <Input
                              parentCSS={{ mb: 0 }}
                              type="file"
                              className="form-control"
                              name="signature"
                              // value={values?.signature}
                              onChange={(e: any) => {
                                console.log();

                                setFieldValue(
                                  "signature",
                                  URL.createObjectURL(e.target.files[0])
                                );
                              }}
                              required
                              error={errors.signature}
                            />
                          </Box>
                        </Box>
                        <Box className="col col-sm-auto">
                          <Button type="submit" color="yellow">
                            Scan
                          </Button>
                        </Box>
                        <Box className="col-12"></Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <img
                            src={values?.signature || "../signature.png"}
                            width={"100%"}
                            className="mt-4"
                          ></img>
                        </Box>
                      </Box> */}

                      <Box className="row align-items-end">
                        <Box className="col-md-4 col-sm-4 col-lg-4"></Box>
                        <Box className="col-md-4 col-sm-4 col-lg-4">
                          <img
                            src={values?.signature || "../signature.png"}
                            width={"100%"}
                            className="mt-4"
                          ></img>
                          <Box css={{ mt: 10 }}>
                            <Input
                              parentCSS={{ mb: 0 }}
                              type="file"
                              className="form-control"
                              name="signature"
                              onChange={async (
                                e: React.ChangeEvent<HTMLInputElement>
                              ) => {
                                try {
                                  const file: any =
                                    e?.target?.files?.[0] || null;
                                  const base64 = await convertBase64(file);
                                  setFieldValue("signature", base64);
                                  setIsSignChanged(true);
                                } catch (error: any) {
                                  toastAlert("error", error);
                                }
                              }}
                              required
                              error={errors.signature}
                            />
                          </Box>
                        </Box>
                      </Box>
                      <Box className="row" css={{ mt: 20 }}>
                        <Box className="col-12 col-sm-6 col-md-4"></Box>
                        <Box className="col-12 col-sm-6 col-md-4">
                          <Text size="h5">Signature Instruction</Text>
                          <ul className={`${styles.instruction_list}`} style={{ fontSize: "0.6rem;" }}>
                            <li>Sign on Plain white paper</li>
                            <li>Use blue or black pen</li>
                          </ul>
                        </Box>
                        <Box className="col-12 col-sm-6 col-md-4"></Box>
                      </Box>
                      <Box css={{ mt: 20 }}>
                        <TabNumbers currentPage={PAGE_TYPES.UploadSignature} />
                      </Box>

                      <Box className="row">
                        <Box className="col"></Box>
                        <Box className="col col-sm-auto text-end">
                          <Button color="yellow" onClick={onBackClick}>
                            <ArrowLeftShort size="20" /> Back
                          </Button>
                          {isSign && (
                            <Button
                              type="submit"
                              color="yellow"
                              onClick={onNextClick}
                              loading={loading}
                            >
                              Next
                              <RightArrowShort size="20" />
                            </Button>
                          )}
                          <Button
                            type="submit"
                            color="yellow"
                            onClick={handleSubmit}
                            loading={loading}
                          >
                            Update & Next
                            <RightArrowShort size="20" />
                          </Button>
                        </Box>
                      </Box>
                    </Box>
                  )}
                </Formik>
              </Card>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default SIGN;