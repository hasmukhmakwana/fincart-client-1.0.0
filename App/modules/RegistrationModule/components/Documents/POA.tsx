import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import ArrowLeftShort from "App/icons/ArrowLeftShort";
import Input from "@ui/Input";
import TabNumbers from "./TabNumbers";
import { PAGE_TYPES } from "../../constants";
import useRegistrationStore from "../../store";
import { toastAlert } from "App/shared/components/Layout";
import { POATypes } from "../../registrationTypes";
import { getSummary, updatePOA, uploadPOA } from "App/api/registration";
import {
  convertBase64,
  encryptData,
  formatDate,
  getArrayFromKey,
  plainBase64,
} from "App/utils/helpers";
import Textarea from "@ui/Textarea";
import RightArrowShort from "App/icons/RightArrowShort";
import SelectMenu from "@ui/Select/Select";

const getDate: any = (currentDate: any) => {

  let dateVar: string = "";

  if (currentDate != undefined && currentDate != "") {
    const issueDateR: string = currentDate;

    if (issueDateR.indexOf("/") > -1) {
      const dateArray = issueDateR.split("/");

      dateVar = `${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`;
    }
    else if (issueDateR.indexOf("-") > -1) {
      const dateArray = issueDateR.split("-");

      dateVar = `${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`;
    }
  }

  return dateVar;
}

let schemaObj: any = {
  DocType: Yup.string().required("Document must be Selected").trim().nullable(),
  aadharFront: Yup.mixed().required("Document Front is required"),
  aadharBack: Yup.mixed().required("Document Back is required"),
};

let schemaObj2: any = {
  name: Yup.string().required("Document Name is required"),
  aadharNumber: Yup.string().required("Document Number is required"),
  address: Yup.string().required("Address is required"),
  state: Yup.string().required("State is required"),
  city: Yup.string().required("City is required"),
  district: Yup.string().required("District is required"),
  pincode: Yup.string().required("Pincode is required"),
};

let schemaObj3: any = {
  name: Yup.string().required("Document Name is required"),
  aadharNumber: Yup.string().required("Document Number is required"),
  address: Yup.string().required("Address is required"),
  state: Yup.string().required("State is required"),
  city: Yup.string().required("City is required"),
  district: Yup.string().required("District is required"),
  pincode: Yup.string().required("Pincode is required"),
  issueDate: Yup.string().required("Issue Date is required"),
  expiryDate: Yup.string().required("Expiry Date is required")
};


type DocumentsEventTypes = {
  onNextClick: () => void;
  onBackClick: () => void;
};

const documentTypeList = [
  { value: "voterId", name: "VOTER ID" },
  { value: "passport", name: "PASSPORT" },
  { value: "drivingLicence", name: "DRIVING LICENSE" },
];


const documentKYCTypeList = [
  { value: "aadhaar", name: "AADHAR CARD" },
  { value: "voterId", name: "VOTER ID" },
  { value: "passport", name: "PASSPORT" },
  { value: "drivingLicence", name: "DRIVING LICENSE" },
];

//*main
const POA = ({ onNextClick, onBackClick }: DocumentsEventTypes) => {
  const {
    BasicID,
    loading,
    setLoader,
    documents,
    setBankingDetails,
    // docu
    setDocumentDetails,
    KYCStatus,
    summaryInfo,
    setSummaryInfo,
    KYC_CAF_Master,
  } = useRegistrationStore();
  const [scanned, setscanned] = useState<boolean>(
    !!summaryInfo?.POA_Details?.POA_FrontPath_Fincart
  );

  const [isPOA, setisPOA] = useState(summaryInfo?.Steps?.isPOA);
  const [stateList, setstateList] = useState<any[]>([]);
  const [hasExpiryDate, sethasExpiryDate] = useState((documents?.POA?.DocType == "passport" || documents?.POA?.DocType == "drivingLicence"));

  const [isKYC, setisKYC] = useState<boolean>(summaryInfo?.Steps?.isKYC);
  // const [documentType, setDocumentType] = useState();

  const [newDoc1, setnewDoc1] = useState<boolean>(false);
  const [newDoc2, setnewDoc2] = useState<boolean>(false);

  const [documentValues, setdocumentValues] = useState<any>({
    DocType: documents.POA.DocType,
    aadharFront: documents.POA.aadharFront,
    aadharBack: documents.POA.aadharBack,
  });

  const [otherValues, setotherValues] = useState<any>({
    aadharNumber: documents.POA?.aadharNumber || "",
    name: documents.POA?.name || "",
    address: documents.POA?.address || "",
    state: documents.POA?.state || "",
    city: documents.POA?.city || "",
    district: documents.POA?.district || "",
    pincode: documents.POA?.pincode || "",
    issuedate: documents.POA?.issueDate || "",
    expirydate: documents.POA?.expiryDate || ''
  });

  //*useEffects
  useEffect(() => {
    setstateList(getArrayFromKey(KYC_CAF_Master, "State_list"));
  }, [KYC_CAF_Master]);

  //*functions
  //submit the page
  const clickOnSubmit = async (formValues: POATypes) => {
    try {
      setLoader(true);
      const obj: any = {
        BasicID: BasicID,
        Address: formValues.address,
        City: formValues.city,
        District: formValues.district,
        State: formValues.state,
        PinCode: formValues.pincode,
        POAType: documentValues.DocType,
        Uid: formValues.aadharNumber,
        AadharName: formValues.name,
        IssueDate: (hasExpiryDate) ? formatDate(formValues.issueDate, "d/m/y") : "",
        ExpiryDate: (hasExpiryDate) ? formatDate(formValues.expiryDate, "d/m/y") : "",
        // IsKYC: KYCStatus,
        IsKYC: "",
      };

      console.log(obj, "POA Object");

      const enc: any = encryptData(obj);
      await updatePOA(enc);
      setDocumentDetails(
        {
          aadharFront: documentValues?.aadharFront || "",
          aadharBack: documentValues?.aadharBack || "",
          aadharNumber: formValues?.aadharNumber || "",
          name: formValues?.name || "",
          address: formValues?.address || "",
          state: formValues?.state || "",
          city: formValues?.city || "",
          district: formValues?.district || "",
          pincode: formValues?.pincode || "",
          issuedate: (hasExpiryDate) ? formValues.issueDate : "",
          expirydate: (hasExpiryDate) ? formValues.issueDate : ""
        },
        "POA"
      );

      setLoader(false);
      callSummary();
      onNextClick();
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  //AADHAR SCAN
  const clickOnAadharScan = async (formValues: {
    DocType: any;
    aadharFront: any;
    aadharBack: any;
  }) => {
    try {
      setLoader(true);
      const obj: any = {
        BasicID: BasicID,
        POAType: formValues?.DocType,
        Front_File: plainBase64(formValues?.aadharFront),
        Back_File: plainBase64(formValues?.aadharBack),
        Platform_Type: "WEB",
      };

      console.log(obj);

      const enc: any = encryptData(obj);
      const result = await uploadPOA(enc);
      setLoader(false);
      const data: any = result?.data || {};

      console.log("upload POA ", data);

      // setDocumentDetails(
      //   {
      //     aadharFront: formValues?.aadharFront || "",
      //     aadharBack: formValues?.aadharBack || "",
      //     aadharNumber: data?.AadharNo || "",
      //     name: data?.AadharName || "",
      //     address: data?.Address || "",
      //     state: data?.state || "",
      //     city: data?.City || "",
      //     district: data?.District || "",
      //     pincode: data?.PinCode || "",
      //   },
      //   "POA"
      // );
      const issueDate: any = getDate(data?.issueDate);

      const expiryDate: any = getDate(data?.expiryDate);

      setotherValues({
        aadharNumber: data?.docNo || "",
        name: data?.docName || "",
        address: data?.Address || "",
        state: data?.state || "",
        city: data?.City || "",
        district: data?.District || "",
        pincode: data?.PinCode || "",
        issueDate: issueDate,
        expiryDate: expiryDate
      });

      //after getting response
      setscanned(true);
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  const callSummary = async () => {
    try {
      const enc: any = encryptData(BasicID, true);
      const result: any = await getSummary(enc);
      setSummaryInfo(result?.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (summaryInfo?.Steps?.isKYC) {
      setisKYC(true);
    } else {
      setisKYC(false);
    }
    return () => { }
  }, [documents.POI.panNumber])


  return (
    <>
      {/* {console.log(documents.POI.panNumber, "PanNo")} */}
      <Card noshadow id="ProofofAddress">
        <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
          Proof of Address
          {summaryInfo?.Account_Creation_Details?.ClientName && (
            <span className="ms-1">
              -
              <span className="ms-1 text-primary">
                {summaryInfo?.Account_Creation_Details?.ClientName}
              </span>
            </span>
          )}
        </Text>
        <Formik
          initialValues={otherValues}
          validationSchema={hasExpiryDate ? Yup.object().shape(schemaObj3) : Yup.object().shape(schemaObj2)}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            clickOnSubmit(values);
            // resetForm();
          }}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue,
            submitCount,
          }) => (
            <Box css={{ px: 20, py: 5 }}>
              <Formik
                initialValues={documentValues}
                validationSchema={Yup.object().shape(schemaObj)}
                enableReinitialize
                onSubmit={(values) => {
                  setdocumentValues(values);
                  clickOnAadharScan(values);
                }}
              >
                {({
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  errors,
                  touched,
                  setFieldValue,
                  submitCount,
                }) => (
                  <Box className="row">
                    <Box className="col-12 col-md-12 col-lg-12 ">
                      <Box className="row ">
                        <Box className="col-auto">
                          <SelectMenu
                            name="DocType"
                            value={values?.DocType}
                            items={isKYC ? documentKYCTypeList : documentTypeList}
                            label="Select Document Type"
                            bindValue={"value"}
                            bindName={"name"}
                            onChange={(e: any) => {
                              if (e?.value == "passport" || e?.value == "drivingLicence")
                                sethasExpiryDate(true);
                              else
                                sethasExpiryDate(false);

                              setFieldValue("DocType", e?.value)
                            }}
                            required
                            error={submitCount ? errors?.DocType : ""}
                          />
                        </Box>
                      </Box>
                    </Box>
                    <Box className="col-md-4 col-sm-4 col-lg-4">
                      <Text color="mediumBlue" size="h6" space="letterspace">
                        Document Front Side <span className="text-capitalize" style={{ color: "black" }}> : {values?.DocType}</span>
                      </Text>
                      <Box>
                        <Input
                          parentCSS={{ mb: 0 }}
                          type="file"
                          className="form-control"
                          name="aadharFront"
                          // value={values?.aadharFront}
                          onChange={async (
                            e: React.ChangeEvent<HTMLInputElement>
                          ) => {
                            try {
                              const file: any = e?.target?.files?.[0] || null;
                              const base64 = await convertBase64(file);
                              setFieldValue("aadharFront", base64);
                              setnewDoc1(true);
                            } catch (error: any) {
                              toastAlert("error", error);
                            }
                          }}
                          required
                          error={submitCount ? errors.aadharFront : null}
                        />
                      </Box>
                      <img
                        src={values?.aadharFront || "../id-card-placeholder-frontSide.jpg"}
                        width={"100%"}
                        className="mt-4"
                      ></img>
                    </Box>
                    <Box className="col-md-4 col-sm-4 col-lg-4">
                      {/* @ts-ignore */}
                      <Text color="mediumBlue" size="h6" space="letterspace">
                        Document Back Side<span className="text-capitalize" style={{ color: "black" }}> : {values?.DocType}</span>
                      </Text>
                      <Box>
                        <Input
                          parentCSS={{ mb: 0 }}
                          type="file"
                          className="form-control"
                          name="aadharBack"
                          // value={values?.aadharBack}
                          onChange={async (
                            e: React.ChangeEvent<HTMLInputElement>
                          ) => {
                            try {
                              const file: any = e?.target?.files?.[0] || null;
                              const base64 = await convertBase64(file);
                              setFieldValue("aadharBack", base64);
                              setnewDoc2(true);
                            } catch (error: any) {
                              toastAlert("error", error);
                            }
                          }}
                          required
                          error={submitCount ? errors.aadharBack : null}
                        />
                      </Box>

                      <img
                        src={values?.aadharBack || "../id-card-placeholder-BackSide.jpg"}
                        width={"100%"}
                        className="mt-4"
                      ></img>
                    </Box>
                    <Box className="col col-sm-auto">
                      <br />
                      <Button
                        type="submit"
                        color="yellow"
                        onClick={handleSubmit}
                        loading={loading}
                        disabled={!(newDoc1 && newDoc2)}
                      >
                        Scan
                      </Button>
                    </Box>
                  </Box>
                )}
              </Formik>
              <Box className="row" css={{ mt: 20 }}>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="Document holder name"
                    placeholder="Document holder name"
                    name="name"
                    value={values?.name}
                    onChange={handleChange}
                    disabled={!scanned}
                    required
                    error={submitCount ? errors.name : null}
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    type="string"
                    label="Document Number"
                    placeholder="Document Number"
                    name="aadharNumber"
                    value={values?.aadharNumber}
                    onChange={(e: any) => {
                      const value = e?.target?.value || "";
                      if (value?.length > 12) return;
                      setFieldValue("aadharNumber", value);
                    }}
                    disabled={!scanned}
                    required
                    error={submitCount ? errors.aadharNumber : null}
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Textarea
                    label="Address"
                    placeholder="Address"
                    name="address"
                    value={values?.address}
                    onChange={(e: any) =>
                      setFieldValue("address", e.target.value)
                    }
                    disabled={!scanned}
                    required
                    error={submitCount ? errors.address : null}
                  ></Textarea>
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="City"
                    name="city"
                    placeholder="City"
                    value={values?.city}
                    onChange={handleChange}
                    disabled={!scanned}
                    required
                    error={submitCount ? errors.city : null}
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="District"
                    placeholder="District"
                    name="district"
                    value={values?.district}
                    onChange={handleChange}
                    disabled={!scanned}
                    required
                    error={submitCount ? errors.district : null}
                  />
                </Box>
                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <SelectMenu
                    items={stateList}
                    bindValue={"Value"}
                    bindName={"Text"}
                    label={"State"}
                    placeholder={"State"}
                    name="state"
                    value={
                      stateList.filter(
                        (i) =>
                          i?.Value?.toLowerCase() ==
                          values?.state?.toLowerCase()
                      )?.[0]?.Value
                    }
                    // value={values?.state}
                    onChange={(e: any) => {
                      setFieldValue("state", e?.Value || "");
                    }}
                    required
                    error={submitCount ? errors.state : ""}
                    isClearable={true}
                    disabled={!scanned}
                  />
                </Box>

                <Box className="col-md-4 col-sm-4 col-lg-4">
                  <Input
                    label="Pin Code"
                    placeholder="Pin Code"
                    name="pincode"
                    value={values?.pincode}
                    onChange={handleChange}
                    disabled={!scanned}
                    required
                    error={submitCount ? errors.pincode : null}
                    maxLength={6}
                  />
                </Box>
                {hasExpiryDate && <>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="Issue Date"
                      placeholder="Issue Date"
                      type="date"
                      name="issueDate"
                      value={values?.issueDate}
                      onChange={handleChange}
                      disabled={!scanned}
                      required
                      error={submitCount ? errors.issueDate : null}
                    />
                  </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4">
                    <Input
                      label="Expiry Date"
                      placeholder="Expiry Date"
                      type="date"
                      name="expiryDate"
                      value={values?.expiryDate}
                      onChange={handleChange}
                      disabled={!scanned}
                      required
                      error={submitCount ? errors.expiryDate : null}
                    />
                  </Box>
                </>
                }
                <TabNumbers currentPage={PAGE_TYPES.AddressProof} />

                <Box className="col-md-4 col-sm-4 col-lg-4"></Box>
                <Box className="col"></Box>

                <Box className="col col-sm-auto text-end">
                  <Button color="yellow" onClick={onBackClick}>
                    <ArrowLeftShort size="20" /> Back
                  </Button>
                  {isPOA && (
                    <Button color="yellow" onClick={onNextClick}>
                      Next
                      <RightArrowShort size="20" />
                    </Button>
                  )}
                  <Button
                    type="submit"
                    color="yellow"
                    onClick={handleSubmit}
                    loading={loading}
                  >
                    Update & Next
                    <RightArrowShort size="20" />
                  </Button>
                </Box>
              </Box>
            </Box>
          )}
        </Formik>
      </Card>
    </>
  );
};
export default POA;
