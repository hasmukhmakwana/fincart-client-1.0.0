import React, { useEffect, useState } from "react";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import ArrowLeftShort from "App/icons/ArrowLeftShort";
import TabNumbers from "./TabNumbers";
import { PAGE_TYPES } from "../../constants";
import useRegistrationStore from "../../store";
import { toastAlert } from "App/shared/components/Layout";
import style from "../../Registration.module.scss"
import {
  getSummary,
  getVideoOTP,
  uploadPhoto,
  uploadVideo,
} from "App/api/registration";
import Webcam from "react-webcam";
import { convertBase64, encryptData, plainBase64 } from "App/utils/helpers";
import RightArrowShort from "App/icons/RightArrowShort";
import Input from "@ui/Input";
import Info from "App/icons/Info";

const PHOTO_HW = {
  height: 300,
  width: 400,
};

type DocumentsEventTypes = {
  onNextClick: () => void;
  onBackClick: () => void;
};

// const getBase64FromUrl = async (url:any) => {
//   console.log(url,"URL");
//   const data = await fetch(url);
//   const blob = await data.blob();
//   return new Promise((resolve) => {
//     const reader = new FileReader();
//     reader.readAsDataURL(blob); 
//     reader.onloadend = () => {
//       const base64data = reader.result;   
//       resolve(base64data);
//     }
//   });
// }

//*main
const IPV = ({ onNextClick, onBackClick }: DocumentsEventTypes) => {
  const {
    BasicID,
    loading,
    setLoader,
    documents,
    summaryInfo,
    setSummaryInfo,
  } = useRegistrationStore();


  console.log(summaryInfo, "summaryInfo");


  const [OTP, setOTP] = useState<string>("");
  const [devicePermission, setDevicePermission] = useState<boolean>(false);
  const [capturedImage, setCapturedImage] = useState<any>(
    summaryInfo?.IPV_Details?.Client_Photo_Fincart || null
  );
  const [recordedVideo, setRecordedVideo] = useState<any>(
    summaryInfo?.IPV_Details?.Video_Fincart || null
  );
  const [openPhotoCamera, setOpenPhotoCamera] = useState<boolean>(false);
  const [openVideoCamera, setOpenVideoCamera] = useState<boolean>(false);

  const webcamRef: any = React.useRef(null);
  const mediaRecorderRef: any = React.useRef(null);
  const [capturing, setCapturing] = React.useState(false);
  const [recordedChunks, setRecordedChunks] = React.useState([]);
  const [isPhoto, setisPhoto] = useState(false);
  const [isVideo, setisVideo] = useState(false);

  const [isPhotoCopy, setisPhotoCopy] = useState(false);
  const [isVideoCopy, setisVideoCopy] = useState(false);
  const [resetFlag, setResetFlag] = useState(false);


  const [device, setDevice] = useState(false);
  // const [isUploaded, setisUploaded] = useState<boolean>(false);

  //*useEffects

  useEffect(() => {
    const photo: boolean = !!summaryInfo?.IPV_Details?.Client_Photo_Fincart;
    const video: boolean = !!summaryInfo?.Steps?.isVedio;
    setisPhoto(photo);
    setisVideo(video);
    setisPhotoCopy(photo);
    setisVideoCopy(video);
    setOpenPhotoCamera(photo);
    setOpenVideoCamera(video);
    return () => { };
  }, [summaryInfo]);

  useEffect(() => {
    (async () => {
      try {
        const constraints = {
          video: true,
          audio: true,
        };
        const stream = await navigator.mediaDevices.getUserMedia(constraints);
        console.log(stream.active);
        setDevicePermission(stream.active);
        setDevice(false);///device available 
      } catch (error) {
        console.log(error);
        setDevice(true);///device not available
        toastAlert("error", error);
      }
    })();
    return () => { };
  }, []);

  useEffect(() => {
    (async () => {
      try {
        const enc: any = encryptData(BasicID, true);
        const result = await getVideoOTP(enc);
        // console.log(result.data);
        setOTP(result?.data?.randomnumber || "");
      } catch (error) {
        console.log(error);
        toastAlert("error", error);
      }
    })();
    return () => { };
  }, []);

  //*functions

  const startRecording = React.useCallback(() => {
    setCapturing(true);
    mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
      mimeType: "video/webm",
    });
    mediaRecorderRef.current.addEventListener(
      "dataavailable",
      handleDataAvailable
    );
    mediaRecorderRef.current.start();
  }, [webcamRef, setCapturing, mediaRecorderRef]);

  const handleDataAvailable = React.useCallback(
    ({ data }) => {
      if (data.size > 0) {
        setRecordedChunks((prev) => prev.concat(data));
      }
    },
    [setRecordedChunks]
  );

  const stopRecording = React.useCallback(() => {
    mediaRecorderRef.current.stop();
    setCapturing(false);
  }, [mediaRecorderRef, webcamRef, setCapturing]);

  const handleDownload = React.useCallback(() => {
    if (recordedChunks.length) {
      const blob = new Blob(recordedChunks, {
        type: "video/webm",
      });

      const url = URL.createObjectURL(blob);
      const a: any = document.createElement("a");
      document.body.appendChild(a);
      a.style = "display: none";
      a.href = url;
      a.download = "react-webcam-stream-capture.webm";
      a.click();
      window.URL.revokeObjectURL(url);
      setRecordedChunks([]);
    }
  }, [recordedChunks]);

  const getVideoSource = React.useCallback(() => {
    if (recordedVideo) {
      return recordedVideo;
    }
    if (!recordedChunks) {
      return "";
    }
    const blob = new Blob(recordedChunks, {
      type: "video/mp4",
    });

    return URL.createObjectURL(blob);
  }, [recordedChunks]);

  //submit the page
  const clickOnSubmit = async () => {
    // console.log(isPhotoCopy,"IsPhoto");
    // return;
    try {
      setLoader(true);
      // const baseimage :any = await convertBase64(
      //   new Blob(capturedImage, {
      //     type: "image/jpg",
      //   })
      // );
      // console.log(baseimage, "capturedImage");
      const photo: any = plainBase64(capturedImage);
      console.log(photo, "photo");
      const base64: any = await convertBase64(
        new Blob(recordedChunks, {
          type: "video/mp4",
        })
      );
      const video: any = plainBase64(base64);

      console.log({ base64 });


      if (!isPhotoCopy) {
        if (!photo) {
          setLoader(false);
          toastAlert("warn", "Photo not found!");
          return;
        }
      }
      if (!isVideoCopy) {
        if (!video) {
          setLoader(false);
          toastAlert("warn", "Video not found!");
          return;
        }
      }

      if (!isPhotoCopy) {
        //photo upload
        const photoObj: any = {
          BasicID: BasicID,
          Photo_File: photo,
        };
        const encPhoto: any = encryptData(photoObj);
        await uploadPhoto(encPhoto);
      }
      if (!isVideoCopy) {
        const videoObj: any = {
          BasicID: BasicID,
          Video_File: video,
          platform_type: 'WEB'
        };
        const encVideo: any = encryptData(videoObj);
        await uploadVideo(encVideo);
      }
      callSummary();
      onNextClick();
      setLoader(false);
    } catch (error: any) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };


  const callSummary = async () => {
    try {
      const enc: any = encryptData(BasicID, true);
      const result: any = await getSummary(enc);
      setSummaryInfo(result?.data);
    } catch (error) {
      console.log(error);
    }
  };
  console.log({ isPhoto, isVideo });


  //*main return
  return (
    <>

      <Card noshadow id="Capture">
        <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
          Capture Photo
          {summaryInfo?.Account_Creation_Details?.ClientName && (
            <span className="ms-1">
              -
              <span className="ms-1 text-primary">
                {summaryInfo?.Account_Creation_Details?.ClientName}
              </span>
            </span>
          )}
        </Text>
        <Box css={{ p: 20 }}>
          <Box className="row text-center ">
            <Box className="col-md-12 col-sm-12 col-lg-12">
              <Text color="blue1" size="h6" space="letterspace">
                Capture Your Photo
              </Text>
              <Box className="row justify-content-center">
                <Box className="col" css={{ mt: 20 }}>
                  {openPhotoCamera ? (
                    capturedImage ? (
                      <>
                        <img
                          src={
                            capturedImage ? capturedImage : "../CameraIcon.png"
                          }
                          width={PHOTO_HW.width}
                          height={PHOTO_HW.height}
                          className="pointer"
                          onClick={() => setOpenPhotoCamera(true)}
                        ></img>
                        <br />
                        {isPhoto ? (
                          <Button
                            css={{ mt: 20 }}
                            type="submit"
                            color="yellow"
                            onClick={() => {
                              setisPhotoCopy(false);
                              setCapturedImage(null);
                            }}
                          >
                            Reset
                          </Button>
                        ) : null}
                      </>
                    ) : (
                      <>
                        <Box className="row justify-content-center ">
                          <Box className="col-auto ">
                            <Webcam
                              audio={false}
                              screenshotFormat="image/jpeg"
                              width={PHOTO_HW.width}
                              height={PHOTO_HW.height}
                              videoConstraints={{
                                width: PHOTO_HW.width,
                                height: PHOTO_HW.height,
                                facingMode: "user",
                              }}
                            >
                              {({ getScreenshot }) => (
                                <>
                                  <br />
                                  <Button
                                    css={{ mt: 20, mx: 1 }}
                                    type="submit"
                                    color="yellow"
                                    disabled={device}
                                    onClick={() => {
                                      setCapturedImage(getScreenshot());
                                      setisPhotoCopy(false);
                                    }}
                                  >
                                    Capture Photo
                                  </Button>
                                  {device ?
                                    <><br />
                                      <Box>
                                        <span title="Camera Device not found" style={{ fontSize: "0.75rem", color: "#005Cb3" }} className="text-danger border-bottom mx-1 px-1">Camera Device not found</span>
                                      </Box></> : ""}
                                </>
                              )}
                            </Webcam>
                            {/* </Box>
                        <Box className="col-5"> */}
                            <Box className="row justify-content-center">
                              <Box className="col-12 col-lg-12 col-md-12">
                                <Text color="blue1" size="h6" css={{ mt: 10 }} space="letterspace">
                                  Or
                                </Text>
                              </Box>
                              <Box className="col-auto col-lg-7 col-md-6">
                                <Input
                                  parentCSS={{ mb: 0 }}
                                  css={{ mt: 15 }}
                                  type="file"
                                  className={`px-1 align-self-center ${style.uploadbtn}`}
                                  accept="image/x-png,image/jpg,image/jpeg,image/png"
                                  // name="cancelledCheque"
                                  onChange={async (
                                    e: React.ChangeEvent<HTMLInputElement>
                                  ) => {
                                    try {
                                      const file: any =
                                        e?.target?.files?.[0] || null;
                                      const base64 = await convertBase64(file);
                                      setCapturedImage(base64);
                                      // setisUploaded(false);
                                      setisPhotoCopy(false);
                                    } catch (error: any) {
                                      toastAlert("error", error);
                                    }
                                  }

                                  }
                                // required
                                // error={
                                //   submitCount ? errors.cancelledCheque : null
                                // }
                                />
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                      </>
                    )
                  ) : (
                    <img
                      src="../CameraIcon.png"
                      width={100}
                      height={100}
                      className="pointer"
                      onClick={() => setOpenPhotoCamera(true)}
                    ></img>
                  )}
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
        <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
          Capture Video
        </Text>
        <Box css={{ p: 20 }}>
          <Box className="row text-center">
            <Box className="col-md-12 col-sm-12 col-lg-12 border">
              <Text color="blue1" size="h6" space="letterspace">
                Record Your Video. Please read out the displayed OTP.
              </Text>
              <Text size="h4" space="letterspace" className="mt-4">
                OTP: {OTP || "LOADING..."}
              </Text>
              <Box>
                <Box css={{ mt: 20, mb: 20 }}>
                  {openVideoCamera ? (
                    recordedChunks.length > 0 || isVideo ? (
                      <>
                        <video
                          width={PHOTO_HW.width}
                          height={PHOTO_HW.height}
                          controls
                        >
                          {/* <source src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-1080p.mp4" type='video/mp4' /> */}
                          <source src={getVideoSource()} type="video/mp4" />
                        </video>
                        <br />
                        {!isVideo ? (
                          <Button
                            color="yellow"
                            css={{ mt: 20 }}
                            onClick={() => setRecordedChunks([])}
                          >
                            Reset
                          </Button>
                        ) : null}
                      </>
                    ) : (
                      <>
                        <Webcam
                          ref={webcamRef}
                          audio={true}
                          width={PHOTO_HW.width}
                          height={PHOTO_HW.height}
                          videoConstraints={{
                            width: PHOTO_HW.width,
                            height: PHOTO_HW.height,
                            facingMode: "user",
                          }}
                        />
                        <br />
                        {capturing ? (
                          <Button
                            color="yellow"
                            css={{ mt: 20 }}
                            // disabled={device}
                            onClick={stopRecording}
                          >
                            Stop Recording
                          </Button>
                        ) : (
                          <Button
                            color="yellow"
                            css={{ mt: 20 }}
                            disabled={device}
                            onClick={() => {
                              startRecording();
                              setisVideoCopy(false);
                            }}
                          >
                            Start Recording
                          </Button>
                        )}
                        {device ?
                          <><br />
                            <Box>
                              <span title="Camera Device not found" style={{ fontSize: "0.75rem", color: "#005Cb3" }} className="text-danger border-bottom mx-1 px-1">Camera Device not found</span>
                            </Box></> : ""}
                      </>
                    )
                  ) : (
                    <>
                      <img
                        src="../VideoIcon.png"
                        width={100}
                        height={100}
                        className="pointer"
                        // disabled={device}
                        onClick={() => setOpenVideoCamera(true)}
                      ></img>
                      <Box class="row d-flex justify-content-center">
                        <Box className="col-md-6 text-start ps-5">
                          <Text className="p-3" size="h6" space="letterspace">
                            <b>Video Instruction</b><br />
                            1. Click on video icon for record your video.<br />
                            2. Place your face of the view finder with your PAN<br />
                            3. Read loud the number that are prompted on the screen
                          </Text>
                        </Box></Box>

                      {device ?
                        <><br />
                          <Box>
                            <span title="Camera Device not found" style={{ fontSize: "0.75rem", color: "#005Cb3" }} className="text-danger border-bottom mx-1 px-1">Camera Device not found</span>
                          </Box></> : ""}
                    </>
                  )}
                </Box>
              </Box>
            </Box>
          </Box>

          <Box css={{ mt: 20 }}>
            <TabNumbers currentPage={PAGE_TYPES.CapturePhotoVideo} />
          </Box>

          <Box className="row">
            <Box className="col"></Box>
            <Box className="col col-sm-auto" css={{ mt: 20 }}>
              <Button color="yellow" onClick={onBackClick}>
                <ArrowLeftShort size="20" /> Back
              </Button>
              {isPhoto && isVideo && (
                <Button type="submit" color="yellow" onClick={onNextClick}>
                  Next
                  <RightArrowShort size="20" />
                </Button>
              )}
              <Button
                type="submit"
                color="yellow"
                onClick={clickOnSubmit}
                loading={loading}
              >
                Update & Next
                <RightArrowShort size="20" />
              </Button>
            </Box>
          </Box>
        </Box>
      </Card>
    </>
  );
};
export default IPV;
