import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { VerifyKYCTypes } from "../registrationTypes";
import { Formik } from "formik";
import * as Yup from "yup";
import useRegistrationStore from "../store";
import { PAN_NO_REGEX } from "App/utils/constants";

const validationSchema = Yup.object().shape({
  PAN_No: Yup.string()
    .required("PAN Number is required")
    .trim()
    .matches(PAN_NO_REGEX, "Invalid PAN Number"),
});

type KYCEventTypes = {
  clickOnSubmit: (value: VerifyKYCTypes) => any;
};

// main
const KYCVerification = ({ clickOnSubmit }: KYCEventTypes) => {
  const { verifyKYC, loading } = useRegistrationStore((state: any) => state);

  return (
    <Box background="">
      <Box className="">
        <Box className="row row-cols-1 row-cols-md-2 justify-content-center">
          <Box className="col">
            <Card noshadow>
              {/* @ts-ignore */}
              <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
                KYC Verification
              </Text>
              <Formik
                initialValues={verifyKYC}
                validationSchema={validationSchema}
                enableReinitialize
                onSubmit={(values, { resetForm }) => {
                  clickOnSubmit(values);
                  // resetForm();
                }}
              >
                {({
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  errors,
                  touched,
                  setFieldValue,
                }) => (
                  <Box css={{ p: 20 }}>
                    <Box className="row">
                      <Box className="col-md-12 col-sm-12 col-lg-12">
                        <Input
                          label="PAN Number"
                          name="PAN_No"
                          value={values?.PAN_No.toUpperCase()}
                          placeholder="PAN Number"
                          onChange={handleChange}
                          required
                          error={errors.PAN_No}
                          maxLength={10}
                        />
                      </Box>
                    </Box>
                    <Box className="row">
                      <Box className="col"></Box>
                      <Box className="col col-sm-auto">
                        <Button
                          type="submit"
                          color="yellow"
                          onClick={handleSubmit}
                          loading={loading}
                        >
                          Check your KYC Status
                        </Button>
                      </Box>
                    </Box>
                  </Box>
                )}
              </Formik>
              {/* @ts-ignore */}
              <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
                Please Keep Handy
              </Text>
              <Box css={{ p: 20 }}>
                <Box className="row align-items-center" css={{ mb: 25 }}>
                  <Box className="col-auto text-center">
                    <img src="../PanCard.png" width={54}></img>
                  </Box>
                  <Box className="col-md-9 col-sm-9 col-lg-9">
                    {/* @ts-ignore */}
                    <Text weight="bold" size="h5" css={{ mb: 5 }}>
                      PAN Card
                    </Text>
                    {/* @ts-ignore */}
                    <Text size="h6" css={{ mb: 5 }}>
                      (Keep original PAN or a scan of the original PAN for
                      Identity)
                    </Text>
                  </Box>
                </Box>
                <Box className="row align-items-center" css={{ mb: 25 }}>
                  <Box className="col-auto text-center">
                    <img src="../AadhaarCard.png" width={54}></img>
                  </Box>
                  <Box className="col-md-9 col-sm-9 col-lg-9">
                    {/* @ts-ignore */}
                    <Text weight="bold" size="h5" css={{ mb: 5 }}>
                      Aadhaar Card
                    </Text>
                    {/* @ts-ignore */}
                    <Text size="h6" css={{ mb: 5 }}>
                      (Keep original Aadhaar Card with front and back for
                      address proof)
                    </Text>
                  </Box>
                </Box>
                <Box className="row align-items-center" css={{ mb: 25 }}>
                  <Box className="col-auto text-center">
                    <img src="../Camera.png" width={54}></img>
                  </Box>
                  <Box className="col-md-9 col-sm-9 col-lg-9">
                    {/* @ts-ignore */}
                    <Text weight="bold" size="h5" css={{ mb: 5 }}>
                      Camera
                    </Text>
                    {/* @ts-ignore */}
                    <Text size="h6" css={{ mb: 5 }}>
                      (Keep camera ready for In-Person verification(IPV).)
                    </Text>
                  </Box>
                </Box>
                <Box className="row align-items-center" css={{ mb: 25 }}>
                  <Box className="col-auto text-center">
                    <img src="../CancelledCheque.png" width={54}></img>
                  </Box>
                  <Box className="col-md-9 col-sm-9 col-lg-9">
                    {/* @ts-ignore */}
                    <Text weight="bold" size="h5" css={{ mb: 5 }}>
                      Cancelled Cheque
                    </Text>
                    {/* @ts-ignore */}
                    <Text size="h6" css={{ mb: 5 }}>
                      (Keep cancelled and signed cheque for bank verification)
                    </Text>
                  </Box>
                </Box>
                <Box className="row align-items-center" css={{ mb: 25 }}>
                  <Box className="col-auto text-center">
                    <img src="../AadhaarNumber.png" width={54}></img>
                  </Box>
                  <Box className="col-md-9 col-sm-9 col-lg-9">
                    {/* @ts-ignore */}
                    <Text weight="bold" size="h5" css={{ mb: 5 }}>
                      Aadhaar Number (UID)
                    </Text>
                    {/* @ts-ignore */}
                    <Text size="h6" css={{ mb: 5 }}>
                      (Keep 12 digit aadhaar number to e-sign using aadhaar
                      based e-sign from NSDL.)
                    </Text>
                  </Box>
                </Box>
              </Box>
            </Card>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default KYCVerification;
