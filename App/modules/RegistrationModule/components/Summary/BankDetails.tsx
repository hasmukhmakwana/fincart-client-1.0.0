import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";

interface BankDetailsTypes {
  summary: any;
}

const BankDetails = ({ summary }: BankDetailsTypes) => {
  //*main return
  return (
    <Box className="row">
      <Text weight="bold" size="h5" border="default2" css={{ mb: 10 }}>
        Bank Account Details
      </Text>
      <Box className="col-md-4 col-sm-4 col-lg-4">
        <Text color="blue1" size="h6" space="letterspace">
          Uploaded Cancelled Cheque
        </Text>
        <img
          src={
            summary?.Bank_Details?.ChequeFilePath_Fincart || "../no_image.png"
          }
          width={"100%"}
        ></img>
      </Box>
      <Box className="col-md-8 col-sm-8 col-lg-8">
        <Box className="row">
          <Box className="col-md-6 col-sm-6 col-lg-6">
            <Input
              label="Account Number"
              name="accountNumber"
              value={summary?.Bank_Details?.Account_No}
              disabled
            />
          </Box>
          <Box className="col-md-6 col-sm-6 col-lg-6">
            <Input
              label="Bank Address"
              name="bankAddress"
              value={summary?.Bank_Details?.Bank_Address}
              disabled
            />
          </Box>
          <Box className="col-md-6 col-sm-6 col-lg-6">
            <Input
              label="Bank Name"
              value={summary?.Bank_Details?.Bank_Name}
              disabled
            />
          </Box>
          <Box className="col-md-6 col-sm-6 col-lg-6">
            <Input
              label="Branch"
              value={summary?.Bank_Details?.Branch_Name}
              disabled
            />
          </Box>
          <Box className="col-md-6 col-sm-6 col-lg-6">
            <Input
              label="IFSC Code"
              name="IFSCCode"
              value={summary?.Bank_Details?.IFSC}
              disabled
            />
          </Box>
          <Box className="col-md-6 col-sm-6 col-lg-6">
            <Input
              label="MICR Code"
              name="MICRCode"
              value={summary?.Bank_Details?.MICR}
              disabled
            />
          </Box>
          <Box className="col-md-6 col-sm-6 col-lg-6">
            <Input
              label="Name As Per Bank"
              name="nameAsPerBank"
              value={summary?.Bank_Details?.Name_As_Per_Bank}
              disabled
            />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default BankDetails;
