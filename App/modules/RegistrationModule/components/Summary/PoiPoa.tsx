import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import Textarea from "@ui/Textarea";
import { useState } from "react";

interface PoiPoaTypes {
  summary: any;
}

const PoiPoa = ({ summary }: PoiPoaTypes) => {

  const [hasExpiryDate, sethasExpiryDate] = useState(summary?.POA_Details?.POA_Type_Value == "passport"
    || summary?.POA_Details?.POA_Type_Value == "drivingLicence");

  //*main return
  return (
    <>
      {console.log(summary)}
      <Box>
        <Text weight="bold" size="h5" border="default2" css={{ mb: 10 }}>
          Proof of Identity
        </Text>
        <Box className="row">
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Text color="blue1" size="h6" space="letterspace">
              Uploaded PAN Card
            </Text>
            <img
              src={summary?.POI_Details?.POI_Path_Fincart || "../no_image.png"}
              width={"100%"}
            ></img>
          </Box>
          <Box className="col-md-8 col-sm-8 col-lg-8">
            <Box className="row">
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Name"
                  name="name"
                  value={summary?.Account_Init_Details?.ClientName || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Father's Name"
                  name="fatherName"
                  value={summary?.Account_Init_Details?.FatherName || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Date of Birth"
                  name="DateofBirth"
                  value={summary?.Account_Init_Details?.DOB || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="PAN Number"
                  name="PAN Number"
                  value={summary?.Account_Init_Details?.PanNumber || ""}
                  disabled
                />
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      <Box css={{ pt: 20 }}>
        <Text weight="bold" size="h5" border="default2" css={{ mb: 10 }}>
          Proof of Address
        </Text>
        <Box className="row">
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Text color="blue1" size="h6" space="letterspace">
              Document Front Side
            </Text>
            <Box>
              <img
                src={
                  summary?.POA_Details?.POA_FrontPath_Fincart ||
                  "../no_image.png"
                }
                width={"100%"}
              ></img>
            </Box>
            {/* </Box>
                  <Box className="col-md-4 col-sm-4 col-lg-4"> */}

            <Text
              color="blue1"
              size="h6"
              space="letterspace"
              css={{ marginTop: "10px" }}
            >
              Document Back Side
            </Text>
            <Box>
              <img
                src={
                  summary?.POA_Details?.POA_BackPath_Fincart ||
                  "../no_image.png"
                }
                width={"100%"}
              ></img>
            </Box>
          </Box>
          <Box className="col-md-8 col-sm-8 col-lg-8">
            <Box className="row">
              {/* <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Document User Name"
                  name="aadhaarName"
                  value={summary?.POA_Details?.AadharName || ""}
                  disabled
                />
              </Box> */}
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Document Number"
                  name="aadhaarNumber"
                  value={summary?.POA_Details?.Uid || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Textarea
                  label="Address"
                  name="address"
                  value={summary?.POA_Details?.Address || ""}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="State"
                  name="state"
                  value={summary?.POA_Details?.State || ""}
                  disabled
                />
              </Box>
              {/* <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="District"
                  name="district"
                  value={summary?.POA_Details?.District || ""}
                  disabled
                />
              </Box> */}
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="City"
                  name="city"
                  value={summary?.POA_Details?.City}
                  disabled
                />
              </Box>
              <Box className="col-md-6 col-sm-6 col-lg-6">
                <Input
                  label="Pin Code"
                  name="pinCode"
                  value={summary?.POA_Details?.PinCode}
                  disabled
                />
              </Box>
              {hasExpiryDate && <>
                <Box className="col-md-6 col-sm-6 col-lg-6">
                  <Input
                    label="Issue Date"
                    name="issueDate"
                    value={summary?.POA_Details?.POA_issue_date}
                    disabled
                  />
                </Box>
                <Box className="col-md-6 col-sm-6 col-lg-6">
                  <Input
                    label="Expiry Date"
                    name="expiryDate"
                    value={summary?.POA_Details?.POA_expiry_date}
                    disabled
                  />
                </Box>
              </>}
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default PoiPoa;
