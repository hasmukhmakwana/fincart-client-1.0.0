import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";

import {
  Column,
  ColumnParent,
  ColumnRow,
  DataCell,
  DataParent,
  DataRow,
  Table,
} from "@ui/DataGrid/DataGrid.styles";
import Divider from "@ui/Divider/Divider";

interface PersonalInfoTypes {
  summary: any;
}
const PersonalInfo = ({ summary }: PersonalInfoTypes) => {
  //*main return
  return (
    <>
      <Box className="row">
        <Box className="col col-12">
          <Text weight="bold" size="h5" border="default2" css={{ mb: 10 }}>
            General
          </Text>
        </Box>
        <Box className="col-md-2 col-sm-2 col-lg-2">
          <Input
            label="Gender"
            name="gender"
            value={summary?.Kyc_Details?.Gender_Text}
            disabled
          />
        </Box>
        <Box className="col-md-2 col-sm-2 col-lg-2">
          <Input
            label="Marital Status"
            name="maritalStatus"
            value={summary?.Kyc_Details?.MaritalStatus_Text}
            disabled
          />
        </Box>
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Mother's Name"
            name="motherName"
            value={summary?.Kyc_Details?.MotherName}
            disabled
          />
        </Box>
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Occupation"
            name="Occupation"
            value={summary?.Kyc_Details?.Occupation_Text}
            disabled
          />
        </Box>
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Address Type"
            name="AddressType"
            value={summary?.Kyc_Details?.Address_Type_Text}
            disabled
          />
        </Box>
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Annual Income"
            name="AnnualIncome"
            value={summary?.Kyc_Details?.AnnualIncome_Text}
            disabled
          />
        </Box>
        <Box className="col-md-4 col-sm-4 col-lg-4">
          <Input
            label="Source Of Income"
            name="SourceOfIncome"
            value={summary?.Kyc_Details?.SourceOfIncome_Text}
            disabled
          />
        </Box>
      </Box>
      <Box>
        <Box className="row">
          <Box className="col col-12">
            <Text weight="bold" size="h5" border="default2" css={{ mb: 10 }}>
              Nominee(s)
            </Text>
          </Box>
        </Box>
        {summary?.Nominee_Details?.map((item: any, index: number) => (
          <>
            {index !== 0 && <Divider />}
            <Box className="row">
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Nominee Name"
                  name="nomineeName"
                  value={item?.Nominee_Name}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Relation"
                  name="relation"
                  value={item?.Relation}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Date of Birth"
                  name="DOB"
                  value={item?.Nominee_DOB}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Nominee Percent"
                  name="percent"
                  value={item?.Nominee_Percent}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Guardian Name"
                  name="gaurdianName"
                  value={item?.Guardian_Name}
                  disabled
                />
              </Box>
              <Box className="col-md-4 col-sm-4 col-lg-4">
                <Input
                  label="Guardian PAN"
                  name="gaurdianPan"
                  value={item?.Guardian_Pan}
                  disabled
                />
              </Box>
            </Box>
          </>
        ))}
      </Box>
    </>
  );
};

export default PersonalInfo;
