import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import useRegistrationStore from "../../store";

const orientation = {
  width: 400,
  height: 300,
};
interface MediaDataTypes {
  summary: any;
}
const MediaData = ({ summary }: MediaDataTypes) => {
  const { KYCStatus } = useRegistrationStore();

  //*main return
  return (
    <Box className="row">
      {KYCStatus === "N" && (
        <>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Text color="blue1" size="h6" space="letterspace">
              Your Photo
            </Text>
            <img
              src={
                summary?.IPV_Details?.Client_Photo_Fincart || "../no_image.png"
              }
              {...orientation}
            ></img>
          </Box>
          <Box className="col-md-4 col-sm-4 col-lg-4">
            <Text color="blue1" size="h6" space="letterspace">
              Your Video
            </Text>
            <video {...orientation} controls>
              <source
                src={
                  summary?.IPV_Details?.Video_Fincart ||
                  summary?.IPV_Details?.Video_Signzy
                }
                type="video/mp4"
              />
            </video>
          </Box>
        </>
      )}

      <Box className="col-md-4 col-sm-4 col-lg-4">
        <Text color="blue1" size="h6" space="letterspace">
          Your Signature
        </Text>
        <img
          src={
            summary?.Signature_Details?.SignaturePath_Fincart_WithBG ||
            "../no-image.png"
          }
          {...orientation}
        ></img>
      </Box>
    </Box>
  );
};

export default MediaData;
