import React, { useEffect, useState } from "react";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "../../../../ui/Box/Box";
import Text from "@ui/Text/Text";
import ArrowLeftShort from "App/icons/ArrowLeftShort";
import Checkbox from "@ui/Checkbox/Checkbox";
import {
  getSummary,
  resetAllSteps,
  createEAadhaarSign,
  confirmKYC,
  confirmCAF,
} from "App/api/registration";
import { toastAlert } from "App/shared/components/Layout";
import PoiPoa from "./PoiPoa";
import BankDetails from "./BankDetails";
import PersonalInfo from "./PersonalInfo";
import MediaData from "./MediaData";
import useRegistrationStore from "../../store";
import { encryptData, openTab } from "App/utils/helpers";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@ui/Accordian/Accordian";
import DownArrow from "App/icons/DownArrow";
import RightArrowShort from "App/icons/RightArrowShort";
import { useRouter } from "next/router";
import { WEB_URL } from "App/utils/constants";
import { setLS } from "@ui/DataGrid/utils";

interface SummaryEventTypes {
  onNextClickParent: () => void;
  onBackClickParent: () => void;
}

//*main
const Summary = ({
  onNextClickParent,
  onBackClickParent,
}: SummaryEventTypes) => {
  const {
    BasicID,
    loading,
    KYCStatus,
    summaryInfo,
    setLoader,
    setSummaryInfo,
    setCommingFrom,
  } = useRegistrationStore();
  const router = useRouter();
  // console.log({ KYCStatus, summaryInfo });

  const [summary, setSummary] = useState<any>(null);
  const [terms, setterms] = useState<boolean>(false);

  //*useEffects
  useEffect(() => {
    (async () => {
      try {
        const enc: any = encryptData(BasicID, true);
        const result = await getSummary(enc);
        console.log(result.data);
        setSummary(result.data);
      } catch (error) {
        console.log(error);
        toastAlert("error", error);
      }
    })();
  }, []);

  //*functions
  const onClickAgree = async () => {
    if (!terms) {
      toastAlert("warn", "Please check 'I agree'");
      return;
    }

    console.log(KYCStatus);
    //console.log("Basic ID", BasicID);

    setLS("KYC_BasicID", BasicID);
    //router.push("/RegistrationThankyou");
    //return;

    if (KYCStatus === "Y") {
      // kyc user
      try {
        setLoader(true);
        /**
         * @execute_confirmNonKYC
         */

        // const enc: any = encryptData(BasicID, true);
        // const result: any = await confirmKYC(enc);
        // console.log(result);
        setLoader(false);
        router.push("/RegistrationThankyou");
      } catch (error) {
        console.log(error);
        setLoader(false);
        toastAlert("error", error);
      }
    } else {
      try {
        setLoader(true);
        //e-aadhaar sign
        const obj: any = {
          BasicID: BasicID,
          redirectUrl: WEB_URL + "/RegistrationThankyou",
        };
        const enc_eAadhaar: any = encryptData(obj);
        const result_eAadhaar: any = await createEAadhaarSign(enc_eAadhaar);
        console.log(result_eAadhaar);

        if (result_eAadhaar?.data) {
          //*EXAMPLE
          const testing: any = {
            Pdf_File:
              "https://persist.signzy.tech/api/files/402441059/download/f27307c331704435a640f62c558f14813faf916ddb5544f0b72d16814f0d9e24.pdf",
            EAadhar_Sign_File:
              "https://esign2.signzy.tech/nsdl-esign-customer2/5c1b42d2ef70c61d04a420e0/token/PqvgTQO3qrLBEcdExj65H08ac5UyVfMooucKTiOtOyjeDO31sYI8DVarOZKy1661271284896",
          };

          //openTab(result_eAadhaar.data.Pdf_File);
          openTab(result_eAadhaar.data.EAadhar_Sign_File, true);
          setLoader(false);
          /**
           * @waiting_for_esign
           * @after_success__execute_confirmNonKYC
           */
        }
      } catch (error) {
        console.log(error);
        setLoader(false);
        toastAlert("error", error);
      }
    }
  };

  const handleReset = async () => {
    //confirm dialogue
    try {
      setLoader(true);
      const enc: any = encryptData(BasicID, true);
      await resetAllSteps(enc);

      setLoader(false);
      setSummaryInfo({});
    } catch (error) {
      console.log(error);
      setLoader(false);
      toastAlert("error", error);
    }
  };

  //*main return
  return (
    <Box>
      <Card noshadow>
        <Text weight="bold" size="h4" border="default" css={{ mb: 10 }}>
          Quick Summary
          {summaryInfo?.Account_Creation_Details?.ClientName && (
            <span className="ms-1">
              -
              <span className="ms-1 text-primary">
                {summaryInfo?.Account_Creation_Details?.ClientName}
              </span>
            </span>
          )}
        </Text>
        {/* <Box css={{ p: 20 }}>{renderComponent()}</Box> */}

        <Box css={{ p: 20 }}>
          <Accordion type="single" defaultValue="1" collapsible>
            <AccordionItem value="1">
              <AccordionTrigger>
                <Box>
                  <Text size="h4" css={{ textTransform: "uppercase" }}>
                    Personal Information
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent>
                {<PersonalInfo summary={summary} />}
              </AccordionContent>
            </AccordionItem>
            <AccordionItem value="2" className="mt-2">
              <AccordionTrigger>
                <Box>
                  <Text size="h4" css={{ textTransform: "uppercase" }}>
                    Bank Account Details
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent>
                {<BankDetails summary={summary} />}
              </AccordionContent>
            </AccordionItem>

            <AccordionItem value="3" className="mt-2">
              <AccordionTrigger>
                <Box>
                  <Text size="h4" css={{ textTransform: "uppercase" }}>
                    Proof of Identity
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent>
                {<PoiPoa summary={summary} />}
              </AccordionContent>
            </AccordionItem>

            <AccordionItem value="4" className="mt-2">
              <AccordionTrigger>
                <Box>
                  <Text size="h4" css={{ textTransform: "uppercase" }}>
                    {KYCStatus === "N"
                      ? "Photo, Video, Signature"
                      : "Signature"}
                    {/* Photo, Video, Signature */}
                  </Text>
                </Box>
                <Box className="action">
                  <DownArrow />
                </Box>
              </AccordionTrigger>
              <AccordionContent>
                {<MediaData summary={summary} />}
              </AccordionContent>
            </AccordionItem>
          </Accordion>
        </Box>

        <Box css={{ p: 20 }}>
          {!summaryInfo?.Steps?.Is_CAF_Confirmed && (
            <Box className="row">
              <Box className="col">
                <Box>
                  <Checkbox
                    label="I agree that the details are reviewed"
                    id="agree"
                    name="terms"
                    checked={terms}
                    onChange={(e: any) => setterms(e?.target?.checked || false)}
                  />
                </Box>
              </Box>
            </Box>
          )}
          <Box className="row">
            <Box className="col"></Box>
            <Box className="col col-auto col-sm-auto text-end" css={{ mt: 20 }}>
              <Button
                type="submit"
                color="yellow"
                onClick={() => {
                  setCommingFrom("back");
                  onBackClickParent();
                }}
              >
                <ArrowLeftShort size="20" />
                Back
              </Button>
              {!summaryInfo?.Steps?.Is_CAF_Confirmed && (
                <Button
                  type="submit"
                  color="yellow"
                  onClick={onClickAgree}
                  loading={loading}
                >
                  Agree & Continue
                  <RightArrowShort size="20" />
                </Button>
              )}
            </Box>
          </Box>
        </Box>
      </Card>
    </Box>
  );
};
export default Summary;
