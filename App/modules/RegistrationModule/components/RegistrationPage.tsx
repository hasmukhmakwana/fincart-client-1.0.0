import React, { useEffect, useState } from "react";
import Box from "../../../ui/Box/Box";
import style from "../Registration.module.scss";
import { StyledTabs, TabsContentNoValue, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsTrigger } from "@radix-ui/react-tabs";
import PersonalInfoTab from "./PersionalInfo/PersionalInfoTab";
import BankDetailsTab from "./BankDetails/BankDetailsTab";
import DocumentsTab from "./Documents/DocumentsTab";
import Summary from "./Summary/SummaryPage";
import clsx from "clsx";
import { toastAlert } from "App/shared/components/Layout";
import { getKYC_CAF_Master } from "App/api/registration";
import useRegistrationStore from "../store";

const TAB_TYPES = {
  PersonalInformation: "PersonalInformation",
  BankDetails: "BankDetails",
  Documents: "Documents",
  QuickSummary: "QuickSummary",
  ThankYou: "ThankYou",
};

const ComponentList = {
  [TAB_TYPES.PersonalInformation]: PersonalInfoTab,
  [TAB_TYPES.BankDetails]: BankDetailsTab,
  [TAB_TYPES.Documents]: DocumentsTab,
  [TAB_TYPES.QuickSummary]: Summary,
};

//*main
const RegistrationPage = ({ btnShow }: any) => {
  const { set_KYC_CAF_Master, summaryInfo, setCommingFrom } =
    useRegistrationStore();
  const [defaultValue, setdefaultValue] = useState<string>(
    TAB_TYPES.PersonalInformation
  );

  const [tabList, settabList] = useState<string[]>([defaultValue]);

  //*useEffects
  useEffect(() => {
    (async () => {
      try {
        const result = await getKYC_CAF_Master();
        // console.log(result.data);
        set_KYC_CAF_Master(result.data);
      } catch (error) {
        console.log(error);
        toastAlert("error", error);
      }
    })();
  }, []);

  useEffect(() => {
    if (summaryInfo?.Steps?.isBasic) {
      let comp: string = TAB_TYPES.PersonalInformation;
      if (summaryInfo?.Steps?.isBank) {
        comp = TAB_TYPES.BankDetails;
      }
      goToCurrentTab(comp);
      return () => { };
    }
  }, [summaryInfo]);

  const goToCurrentTab = (value: any = null) => {
    const check: string = value || defaultValue;
    switch (check) {
      case TAB_TYPES.PersonalInformation:
        setdefaultValue(TAB_TYPES.BankDetails);
        break;

      case TAB_TYPES.BankDetails:
        setdefaultValue(TAB_TYPES.Documents);
        settabList([...tabList, TAB_TYPES.BankDetails]);
        break;

      case TAB_TYPES.Documents:
        setdefaultValue(TAB_TYPES.QuickSummary);
        settabList([...tabList, TAB_TYPES.Documents]);
        break;

      case TAB_TYPES.QuickSummary:
        console.log("onNextClick --- ", TAB_TYPES.ThankYou);
        break;

      default:
        break;
    }
  };

  const onNextClick = () => {
    switch (defaultValue) {
      case TAB_TYPES.PersonalInformation:
        setdefaultValue(TAB_TYPES.BankDetails);
        break;

      case TAB_TYPES.BankDetails:
        setdefaultValue(TAB_TYPES.Documents);
        settabList([...tabList, TAB_TYPES.BankDetails]);
        break;

      case TAB_TYPES.Documents:
        setdefaultValue(TAB_TYPES.QuickSummary);
        settabList([...tabList, TAB_TYPES.Documents]);
        break;

      case TAB_TYPES.QuickSummary:
        console.log("onNextClick --- ", TAB_TYPES.ThankYou);

        break;

      default:
        break;
    }
  };

  const onBackClick = () => {
    switch (defaultValue) {
      case TAB_TYPES.PersonalInformation:
        console.log("onBackClick --- ", TAB_TYPES.PersonalInformation);
        break;

      case TAB_TYPES.BankDetails:
        setdefaultValue(TAB_TYPES.PersonalInformation);
        break;

      case TAB_TYPES.Documents:
        setdefaultValue(TAB_TYPES.BankDetails);
        break;

      case TAB_TYPES.QuickSummary:
        setdefaultValue(TAB_TYPES.Documents);
        break;

      case TAB_TYPES.ThankYou:
        setdefaultValue(TAB_TYPES.QuickSummary);
        break;

      default:
        break;
    }
  };

  const setCompletedTabStyles = (currentType: string): boolean => {
    return tabList.includes(currentType);
  };

  const setCurrentTabStyles = (currentType: string): boolean => {
    return currentType === defaultValue;
  };

  const getTabConetent = () => {
    const Comp: any = ComponentList[defaultValue];
    return (
      <Comp onNextClickParent={onNextClick} onBackClickParent={onBackClick} />
    );
  };

  const onTabClick = (tab: string) => {
    setdefaultValue(tab);
  };
  useEffect(() => {
    btnShow(true);

    return () => {
      btnShow(false);
    }
  }, [])

  //*main return
  return (
    <Box background="">
      <Box className="">
        <Box className="row">
          <Box>
            <StyledTabs
              className="horizontal"
            // defaultValue={defaultValue}
            // defaultValue={TAB_TYPES.PersonalInformation}
            // onValueChange={(e)=>setdefaultValue(e)}
            >
              <TabsList className="horizontal" aria-label="Manage your account">
                <TabsTrigger
                  className={clsx("btn", {
                    [style.completedTab]: setCompletedTabStyles(
                      TAB_TYPES.PersonalInformation
                    ),
                    [style.runningTab]: setCurrentTabStyles(
                      TAB_TYPES.PersonalInformation
                    ),
                  })}
                  value={TAB_TYPES.PersonalInformation}
                  onClick={() => onTabClick(TAB_TYPES.PersonalInformation)}
                >
                  Personal Information
                </TabsTrigger>
                <TabsTrigger
                  className={clsx("btn", {
                    [style.completedTab]: setCompletedTabStyles(
                      TAB_TYPES.BankDetails
                    ),
                    [style.runningTab]: setCurrentTabStyles(
                      TAB_TYPES.BankDetails
                    ),
                  })}
                  value={TAB_TYPES.BankDetails}
                  onClick={() => onTabClick(TAB_TYPES.BankDetails)}
                  disabled={!summaryInfo?.Steps?.isBasic}
                >
                  Bank Account Details
                </TabsTrigger>
                <TabsTrigger
                  className={clsx("btn", {
                    [style.completedTab]: setCompletedTabStyles(
                      TAB_TYPES.Documents
                    ),
                    [style.runningTab]: setCurrentTabStyles(
                      TAB_TYPES.Documents
                    ),
                  })}
                  value={TAB_TYPES.Documents}
                  onClick={() => {
                    setCommingFrom("next");
                    onTabClick(TAB_TYPES.Documents);
                  }}
                  disabled={!summaryInfo?.Steps?.isBank}
                >
                  Documents
                </TabsTrigger>
              </TabsList>
              <TabsContentNoValue className="horizontal">
                {getTabConetent()}
              </TabsContentNoValue>
            </StyledTabs>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default RegistrationPage;
