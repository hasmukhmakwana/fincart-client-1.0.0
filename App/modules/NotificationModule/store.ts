import create from "zustand";

// export type sysTrnListType = {
//     FinalFinTransactionNo: string;
//     SystemSeriesNo: string;
//     amount: string;
//     bankAcc: string;
//     basicId: string;
//     endDate: string;
//     folioNo: string;
//     isDelete: string;
//     isSelected: boolean,
//     memberName: string;
//     orgSchemeName: string;
//     remark: string;
//     schemeName: string;
//     startDate: string;
//     subTrxnType: string;
//     sysmCount: string;
//     sysmDate: string;
//     trxnType: string;
// }
interface StoreTypes {
    schemeID: string;
    sysTrnList: any[];

    setSchemeId: (payload: string) => void;
    setSysTrnList: (payload: any[]) => void;
}
const useNotificationStore = create<StoreTypes>((set) => ({
    schemeID: "0",
    sysTrnList:[],

    setSchemeId: (payload) =>
        set((state) => ({
            ...state,
            schemeID: payload,
        })),

    setSysTrnList: (payload) =>
        set((state) => ({
            ...state,
            sysTrnList: payload,
        })),

}))

export default useNotificationStore;