import Text from "@ui/Text/Text";
import React, { useState, useEffect } from "react";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import { getNotificationData } from "App/api/notification"
import { encryptData } from "App/utils/helpers";
import style from "../UpcomingTransactions.module.scss";
import Layout from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import router from "next/router";
import { now } from "lodash";
const NotificationPage = () => {
    const [notificationDetailsList, setNotificationDetailsList] = useState<any>();
    const fetchNotification = async () => {
        try {

            let result = await getNotificationData();

            setNotificationDetailsList(result?.data);
            console.log(result?.data);

            // setLoader(false);
        } catch (error: any) {
            console.log(error);
            //toastAlert("error", error);
        }
    };

    const getBirthdate = (birthDate: Date) => {
        if (Date === undefined) return "";

        const d = new Date(birthDate);

        const currentDate = new Date(new Date().getFullYear() + " " + (d.getMonth() + 1) + " " + d.getDate());

        return currentDate.toLocaleDateString("en-IN", { year: 'numeric', month: 'long', day: 'numeric' });
    }

    useEffect(() => {
        fetchNotification();
        return () => {
        }
    }, [])

    return (
        <>
            <Layout>
                <PageHeader title="Notifications" rightContent={<></>}></PageHeader>
                <Box className="container">
                    <Box className="row p-4" css={{ backgroundColor: "white", borderRadius: "8px" }}>
                        <Box className="col-md-12 col-sm-12 col-lg-12">
                            <Box className="row text-end">
                                <Box className="col ">
                                    <Button
                                        className="mt-2 px-3 py-1"
                                        color="yellowGroup"
                                        size="sm"
                                        onClick={() => { router.push("/UpcomingTransactions") }}
                                    >
                                        <Text
                                            // @ts-ignore
                                            weight="bold"
                                            size="h6"
                                            //@ts-ignore
                                            color="gray8"
                                        // className={style.button}
                                        >
                                            View Upcoming Transactions
                                        </Text>
                                    </Button>
                                </Box>
                            </Box>
                            <Box className="row">
                                {/* For birthday  */}
                                {notificationDetailsList?.birthdayAlert?.length > 0 ?
                                    notificationDetailsList?.birthdayAlert?.map((record: any) => {
                                        return <>
                                            <Box className="col-md-12 col-sm-12 col-lg-4">
                                                <Card
                                                    className="h-100"
                                                    css={{
                                                        borderRadius: "8px 8px 8px 8px",
                                                        border: "1px solid #82afd9",
                                                        backgroundColor: "#b3daff"
                                                    }}
                                                >
                                                    <Box className="row" >
                                                        <Box className="col-12">
                                                            <Box className="row">
                                                                <Box className="col-3"></Box>
                                                                <Box className="col-5">
                                                                    <Text
                                                                        // @ts-ignore
                                                                        weight="normal"
                                                                        size="h5"
                                                                        className="p-2 rounded text-center"
                                                                    >
                                                                        Happy Birthday
                                                                    </Text>
                                                                    {/* @ts-ignore */}
                                                                    <Text weight="bold" size="h4" className="text-center text-white py-1 "
                                                                        css={{ backgroundColor: "var(--colors-blue1)", borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }} >
                                                                        {getBirthdate(record?.birthDate)}
                                                                    </Text>
                                                                </Box>

                                                                <Box className="col-3 text-end pe-4 ">
                                                                    <img
                                                                        width={50}
                                                                        height={70}
                                                                        src={"/CakeIcon.png"}
                                                                        alt=""
                                                                    />
                                                                </Box>
                                                            </Box>
                                                            <hr />
                                                            <Box className="row mt-2">
                                                                <Box className="col-3">
                                                                    <img
                                                                        width={70}
                                                                        height={80}
                                                                        src={"/BalloonIcon.png"}
                                                                        alt=""
                                                                    />
                                                                </Box>
                                                                <Box className="col-9 text-center">
                                                                    <Text
                                                                        // @ts-ignore
                                                                        weight="normal"
                                                                        size="h6"
                                                                        // className="text-center"
                                                                        // className="p-2 rounded text-center text-white "
                                                                        css={{ color: "var(--colors-blue1)", fontSize: "0.825rem" }}
                                                                    >
                                                                        {record?.wishes}
                                                                    </Text>

                                                                    {/* <Text
                                                        // @ts-ignore
                                                        weight="normal"
                                                        size="h6"
                                                        // className="text-center"
                                                        // className="p-2 rounded text-center text-white "
                                                        css={{ color: "var(--colors-blue1)" }}
                                                    >

                                                        {record?.userName}
                                                    </Text> */}
                                                                </Box>
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                </Card>
                                            </Box>
                                        </>
                                    }) : <></>}

                                {/* for event */}
                                {notificationDetailsList?.eventAlert?.length > 0 ? notificationDetailsList?.eventAlert?.map((record: any) => {
                                    return (<>
                                        <Box className="col-md-12 col-sm-12 col-lg-4">
                                            <Card className="h-100"
                                                css={{
                                                    borderRadius: "8px 8px 8px 8px",
                                                    border: "1px solid #82afd9"
                                                }}
                                            >
                                                <Box className="row">
                                                    <Box className="col-12">
                                                        <Box className="row">
                                                            <Box className="col-2 text-start">
                                                                <img
                                                                    width={50}
                                                                    height={60}
                                                                    src={"/EventIcon.png"}
                                                                    alt=""
                                                                />
                                                            </Box>
                                                            <Box className="col-8 p-0">
                                                                <Text
                                                                    // @ts-ignore
                                                                    weight="normal"
                                                                    size="h5"
                                                                    className="rounded text-center "
                                                                >
                                                                    Invite you to attend
                                                                </Text>
                                                                {/* @ts-ignore */}
                                                                <Text size="h5" className="text-center py-1 " css={{ color: "var(--colors-blue1)" }} >
                                                                    Investor Awareness Programme(IAP)
                                                                </Text>
                                                                {/*  @ts-ignore */}
                                                                <Text size="h5" className="text-center py-1">
                                                                    {record?.eventName}
                                                                    {/* Mutual Funds Myths Vs Realities */}
                                                                    <hr className="my-0" style={{ color: "orange" }} />
                                                                </Text>
                                                            </Box>
                                                        </Box>
                                                        <Box className="row p-0 m-0">
                                                            <Box className="col-12">
                                                                <Text
                                                                    // @ts-ignore
                                                                    weight="normal"
                                                                    size="h6"
                                                                    className="text-center"
                                                                // className="p-2 rounded text-center text-white "
                                                                >
                                                                    {record?.eventMsg}
                                                                    {/* A seminar and discussion on service tax applicability <br />
                                                        for mutual fund distributors */}
                                                                </Text>
                                                            </Box>
                                                        </Box>
                                                        <Box className="row  justify-content-center p-1">
                                                            <Box className="col-7">
                                                                {/*  @ts-ignore */}
                                                                <Text size="h6" className="text-center text-white py-1 " css={{ backgroundColor: "var(--colors-blue1)", borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }} >Date<br />
                                                                    {record.eventDate}
                                                                    {/* Wednesday,November 15th 2022<br />
                                                        Time: 5.00 PM */}
                                                                </Text>
                                                            </Box>
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </Card>
                                        </Box>
                                    </>)
                                }) : <></>
                                }
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Layout>
        </>
    )
}

export default NotificationPage