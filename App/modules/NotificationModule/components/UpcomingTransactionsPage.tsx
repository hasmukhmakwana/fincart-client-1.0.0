import React, { useState, useEffect } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import SelectMenu from "@ui/Select/Select";
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import styles from "App/modules/NotificationModule/UpcomingTransactions.module.scss";
import Card from '@ui/Card/Card';
import DeleteIcon from "App/icons/DeleteIcon";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import { toastAlert } from 'App/shared/components/Layout';
import {
  getUpcomingTransactionsDD,
  getUpcomingTransactionsData,
  getInvestorDD,
  fetchSysTxnBunch,
  deleteTxnOtp,
  deleteTxnValidateotp,
  deleteTxn,
} from "App/api/UpcomingTransactions";
import useNotificationStore from "../store";
import { encryptData, getUser } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";
import EyeIcon from "App/icons/EyeIcon";
import router from "next/router";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
const local: any = getUser();

export type VerifyTypes = {
  otp: string;
};

interface StoreTypes {
  verifyPay: VerifyTypes;
}

const useStore = create<StoreTypes>((set) => ({
  //* initial state
  verifyPay: {
    otp: "",
  }
}))

const UpcomingTransactionsPage = () => {
  const [investor, setInvestor] = useState<any>([]);
  const [schemeList, setSchemeList] = useState<any>([]);
  const [transcationType, setTranscationType] = useState<any>([]);
  const [transcationDuration, setTranscationDuration] = useState<any>([]);
  const { verifyPay } = useStore((state: any) => state);
  const [openInvestor, setOpenInvestor] = useState<any>(local?.basicid);
  const [openschemeList, setOpenSchemeList] = useState<any>("0");
  const [opentranscationType, setOpenTranscationType] = useState<any>("0");
  const [opentranscationDuration, setOpenTranscationDuration] = useState<any>("1");

  const [load, setLoad] = useState(false);
  const [remain, setRemain] = useState<any>([]);
  const [upcomingData, setUpcomingData] = useState<any>([]);

  const [delSeriesData, setDelSeriesData] = useState<any>();

  const [openDelConfirmModal, setDelConfirmModal] = useState(false);
  const [confirmLoad, setConfirmLoad] = useState(false);
  const [validateLoad, setValidateLoad] = useState(false);
  const [openDelModal, setOpenDelModal] = useState(false);

  const delModalObj = {
    otp: Yup.string()
      .required("otp is required")
  }

  const [validationSchema, setValidationSchema] = useState(
    delModalObj
  );

  const {
    schemeID,
    setSchemeId,
    setSysTrnList,
  } = useNotificationStore();

  const fetchMemberList = async () => {
    try {
      // const enc: any = encryptData({
      //   basicId : local.basicid,
      //     fundid: "0",
      //   });
      //   console.log(enc);
      setLoad(true);
      let obj = {
        basicId: local?.basicid,
        fundid: "0",
      }
      const enc: any = encryptData(obj);
      let result = await getInvestorDD(enc);
      console.log("Investor");
      console.log(result?.data);
      setInvestor(result?.data?.memberList);
      setLoad(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoad(false);
    }
  };

  const getsystemTxnBunch = async () => {
    try {
      setLoad(true);
      let obj: any = {
        basicId: openInvestor !== undefined ? openInvestor : "",
        isGroupLeaderWise: 'N',
        period: opentranscationDuration !== undefined ? opentranscationDuration : "",// duration
        schmId: schemeID !== undefined ? schemeID : "",//
        txnType: opentranscationType !== undefined ? opentranscationType : "",//
        systemSeriesNo: '0'//
      }
      console.log(obj);
      let enc: any = encryptData(obj);
      let result: any = await fetchSysTxnBunch(enc);
      // console.log("Table Data");
      // console.log(result?.data);

      setUpcomingData(result?.data || []);
      // console.log(result?.data?.sysmTrxnlist);
      setLoad(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoad(false);
    }
  }

  const fetchDropDown = async () => {
    try {
      setLoad(true);
      let obj = {
        BasicID: local?.basicid,
        isGroupLeaderWise: 'N'
      }
      const enc: any = encryptData(obj);
      let result: any = await getUpcomingTransactionsDD(enc);

      // console.log(result?.data)
      // setInvestor(result?.data);
      setSchemeList(result?.data[0]?.Value);
      setTranscationType(result?.data[1]?.Value);
      setTranscationDuration(result?.data[2]?.Value);
      setLoad(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoad(false);
    }
  }

  const fetch = async () => {
    try {
      setLoad(true);

      let result = await getUpcomingTransactionsData();
      //console.log(enc);
      console.log("test");
      console.log(result?.data, "UpcomingTransactionsData");

      setUpcomingData(result?.data?.sysmSipTopReminders);

      setLoad(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
      setLoad(false);
    }
  };

  const deleteOtp = async (val: any) => {
    try {
      setConfirmLoad(true);
      let obj: any = {
        userid: local?.userid,
        mobile: local?.mobile,
        basicId: local?.basicid,
        name: val?.memberName,
        txncount: "ALL",
        txntype: val?.subTrxnType,
        scheme: val?.schemeName,
        rmname: local?.RmName,
        amt: val?.amount,
        systemSeriesNo: val?.SystemSeriesNo,
        otp: "",
      }
      console.log(obj);
      let enc: any = encryptData(obj);
      let result: any = await deleteTxnOtp(enc);
      if (result.status === "Success") {
        setConfirmLoad(false);
        setOpenDelModal(true); setDelConfirmModal(false);
        console.log(result);
        setLoad(false);
        toastAlert("success", "OTP sent Successfully");

      }
    } catch (error) {
      console.log(error);
      setConfirmLoad(false);
      toastAlert("error", error);
      setLoad(false);
    }
  }
  const deleteValidateOtp = async (val: any) => {
    try {
      setValidateLoad(true);
      let obj: any = {
        userid: local?.userid,
        mobile: local?.RmMobile,
        basicId: local?.basicid,
        name: delSeriesData?.memberName,
        txncount: "ALL",
        txntype: delSeriesData?.subTrxnType,
        scheme: delSeriesData?.schemeName,
        rmname: local?.RmName,
        amt: delSeriesData?.amount,
        systemSeriesNo: delSeriesData?.SystemSeriesNo,
        otp: val?.otp,
      }
      let enc: any = encryptData(obj);
      let result: any = await deleteTxnValidateotp(enc);
      if (result?.status === "Success") {
        toastAlert("success", "OTP Validated Successfully")
        deleteTran();
      }
      else {
        setValidateLoad(false);
        toastAlert("error", "Invalid otp");
      }
      console.log(obj, "validate")
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setValidateLoad(false);
    }
  }

  const deleteTran = async () => {
    try {
      console.log(delSeriesData, "delete");

      let subObj: any = [{
        basicId: delSeriesData?.basicId,
        finalFinTransactionNo: '',
        amount: delSeriesData?.amount,
        systemSeriesNo: delSeriesData?.SystemSeriesNo,
      },]

      let obj: any = {
        isBunchWise: "Y",
        txnType: "SIP",
        deleteTxnList: subObj,
      }
      console.log(obj, "deletetxn")
      let enc: any = encryptData(obj);
      let result: any = await deleteTxn(enc);
      if (result?.status === "Success") {
        setValidateLoad(false);
        setOpenDelModal(false);
        toastAlert("success", "Transactions Deleted Successfully");
        getsystemTxnBunch();
      }
      else {
        setValidateLoad(false);
        toastAlert("warn", result?.msg);
      }

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setValidateLoad(false);
    }
  }

  useEffect(() => {
    (async () => {
      await fetchMemberList();
      await fetchDropDown();
      await getsystemTxnBunch();
    })()
    return () => {
    }
  }, [local])


  return (
    <>
      <Box className="container">
        <Box className="row p-1" css={{ backgroundColor: "#b3daff", borderRadius: "8px" }}>
          <Box className="col-lg-3">

            <SelectMenu
              value={openInvestor}
              items={investor}
              label="Investor"
              bindValue={"basicid"}
              bindName={"memberName"}
              onChange={(e: any) => {
                console.log(investor);
                setOpenInvestor(e?.basicid || "")
              }
              }
            />
          </Box>
          <Box className="col-lg-3">
            <SelectMenu
              value={schemeID}
              items={schemeList}
              label="Invested Scheme"
              bindValue={"Value"}
              bindName={"Text"}
              onChange={(e: any) => {
                setSchemeId(e?.Value || "")
              }
              }
            />
          </Box>
          <Box className="col-lg-3">
            <SelectMenu
              value={opentranscationType}
              items={transcationType}
              label="Transaction Type"
              bindValue={"Value"}
              bindName={"Text"}
              onChange={(e: any) => {
                setOpenTranscationType(e?.Value || "")
              }
              }
            />
          </Box>
          <Box className="col-lg-3">
            <SelectMenu
              value={opentranscationDuration}
              items={transcationDuration}
              label="Transaction Duration"
              bindValue={"Value"}
              bindName={"Text"}
              onChange={(e: any) =>
                setOpenTranscationDuration(e?.Value || "")
              }
            />
          </Box>
          <Box className="row pe-0">
            <Box className="col-12 text-end pe-0">
              <Button
                className={`col-auto mb-2 py-1`}
                color="yellowGroup"
                size="md"
                onClick={() => { getsystemTxnBunch() }}
              >
                <Box>
                  <Text
                    //@ts-ignore 
                    size="h6"
                    //@ts-ignore
                    color="gray8"
                    className={`${styles.button}`}
                  >
                    Search
                  </Text>
                </Box>
              </Button>
            </Box>
          </Box>
        </Box>

      </Box>
      {load ? <>
        <Card>
          <Box className="row justify-content-center">
            <Box className="col-auto"><Loader /></Box>
          </Box>
        </Card>
      </> : <>
        {upcomingData.length === 0 ? <>

          <Card>
            <Box className="row justify-content-center">
              <Box className="col-auto">No Transaction Available</Box>
            </Box>
          </Card>
        </> : <>
          {upcomingData.map((record: any) => {
            return (
              <Card>
                <Box className="container-fluid">
                  <Box className="row mb-2">
                    <Box className="col-12">
                      <Text
                        //@ts-ignore 
                        size="h4" className="pt-2">
                        {record?.SystemSeriesDetails} <span style={{ color: "var(--colors-blue1)", fontSize: "0.85rem" }}></span>
                      </Text>
                    </Box>
                  </Box>
                  <Box className={styles.line}></Box>
                  <Box className="row">
                    <Box css={{ overflow: "auto" }}>
                      <Table>
                        <ColumnParent
                          //@ts-ignore
                          className="text-capitalize" css={{ backgroundColor: "#FFFFFF", height: "auto" }}
                        >
                          <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                            <Column className='col-lg-3 text-start'>Scheme</Column>
                            <Column>Type</Column>
                            <Column>Folio</Column>
                            <Column>Amount</Column>
                            <Column>Bank Account</Column>
                            <Column>Start Date</Column>
                            <Column>End Date</Column>
                            {/* <Column>Action</Column> */}
                          </ColumnRow>
                        </ColumnParent>
                        <DataParent>
                          <DataRow>
                            <DataCell className="text-start  py-0">{record?.sysmTrxnlist?.[0]?.schemeName}</DataCell>
                            <DataCell className="text-center py-0">{record?.sysmTrxnlist?.[0]?.trxnType}</DataCell>
                            <DataCell className="text-center py-0">{record?.sysmTrxnlist?.[0]?.folioNo}</DataCell>
                            <DataCell className="text-center py-0">₹ {record?.sysmTrxnlist?.[0]?.amount}</DataCell>
                            <DataCell className="text-center py-0">{record?.sysmTrxnlist?.[0]?.bankAcc}</DataCell>
                            <DataCell className="text-center py-0">{record?.sysmTrxnlist?.[0]?.startDate}</DataCell>
                            <DataCell className="text-center py-0">{record?.sysmTrxnlist?.[0]?.endDate}</DataCell>
                            <DataCell className="text-end">
                              {/* @ts-ignore */}
                              <Box className="row justify-content-end">
                                <Box className="col-auto ">
                                  {/* @ts-ignore */}
                                  <Text size="h6" onClick={() => {
                                    router.push("/UpcomingTransactions/TransactionDetails");
                                    console.log(record)
                                    localStorage.setItem('sysTrnList', JSON.stringify(record));
                                    setSysTrnList(record);
                                  }}
                                    css={{
                                      // color: "var(--colors-blue1)",
                                      textAlign: "center",
                                      cursor: "pointer",
                                    }} >
                                    <EyeIcon color="orange" className="mt-1" />
                                  </Text>
                                </Box>
                                <Box className="col-auto">
                                  <Button size="h6"
                                    // @ts-ignore
                                    onClick={() => {
                                      setDelSeriesData(record?.sysmTrxnlist?.[0]);
                                      setDelConfirmModal(true);
                                    }}
                                    css={{
                                      backgroundColor: "transparent",
                                      textAlign: "center",
                                      cursor: "pointer",
                                    }}
                                  >
                                    <DeleteIcon color="orange" className="mt-1" />
                                  </Button>
                                </Box>
                              </Box>
                            </DataCell>
                          </DataRow>
                        </DataParent>
                      </Table>
                    </Box>
                  </Box>
                </Box>
              </Card>
            );
          }
          )}
        </>}
      </>}
      <DialogModal
        open={openDelModal}
        setOpen={setOpenDelModal}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "30%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3">
            <Box className="row">
              <Box className="col-auto text-light">Delete Transaction Warning</Box>
            </Box>
          </Box>
          <Box
            className="text-start ms-4 modal-body modal-body-scroll"
            css={{ padding: "0.5rem" }} >
            <Text css={{ mt: 5, pt: 10 }}>
              SIP of {delSeriesData?.schemeName}<br /> [Series No.: {delSeriesData?.SystemSeriesNo}]<br /> [Investor: {delSeriesData?.memberName}]

            </Text>
            <Text css={{ mt: 5, pt: 10 }}>
              {/* @ts-ignore */}
              {/* Goal Name: <b>{fullGoal.goalName}</b><br /> */}
              Are you sure to delete this Transaction?

            </Text>
            <Formik
              initialValues={verifyPay}
              validationSchema={Yup.object().shape(validationSchema)}
              enableReinitialize
              onSubmit={(values, { resetForm }) => {
                console.log(values);

                deleteValidateOtp(values)
              }}
            >
              {({
                values,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                errors,
                touched,
                setFieldValue,
                submitCount
              }) => (
                <>
                  <Box css={{ mt: 5, pt: 10 }} className="text-start row">
                    <Box className="col-auto">
                      <Input
                        name="otp"
                        label="Enter OTP"
                        placeholder="Enter OTP"
                        className="border border-2"
                        //@ts-ignore
                        error={submitCount ? errors.otp : null}
                        onChange={handleChange}
                        value={values?.otp}

                      />
                    </Box>
                  </Box>

                  <Box css={{ mt: 5, pt: 10 }} className="text-end">
                    <Button
                      type="submit"
                      color="yellow"
                      disabled={validateLoad}
                      onClick={() => { setOpenDelModal(false) }}>
                      Cancel
                    </Button>
                    <Button
                      type="submit"
                      color="yellow"
                      //@ts-ignore
                      onClick={handleSubmit}
                      disabled={validateLoad}
                    >
                      {validateLoad ? <>Loading... </> : <> Delete Now</>}
                    </Button>
                  </Box>
                </>)}</Formik>
          </Box>
        </Box>

      </DialogModal>

      <DialogModal
        open={openDelConfirmModal}
        setOpen={setDelConfirmModal}
        css={{
          "@bp0": { width: "90%" },
          "@bp1": { width: "30%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3">
            <Box className="row">
              <Box className="col-auto text-light">Delete Transaction Warning</Box>
            </Box>
          </Box>
          <Box
            className="text-start ms-4 modal-body modal-body-scroll"
            css={{ padding: "0.5rem" }} >

            <Box className="row">

              <Box className="col-12 pt-4 pb-5 text-center">
                {/* @ts-ignore */}
                <Text size="h4" className="pb-1" css={{ color: "#005CB3" }}>Are you  sure want to
                  delete</Text>

                <Box css={{ mt: 5, pt: 10 }} className="text-end">
                  <Button
                    type="submit"
                    color="yellow"
                    //@ts-ignore
                    onClick={() => setDelConfirmModal(false)}
                    disabled={confirmLoad}
                  >
                    Cancel
                  </Button>
                  <Button
                    type="submit"
                    color="yellow"
                    onClick={() => { deleteOtp(delSeriesData) }}
                    disabled={confirmLoad}
                  >
                    {confirmLoad ? <>Loading... </> : <>Yes </>}
                  </Button>

                </Box>
              </Box>
            </Box>
          </Box>
        </Box>

      </DialogModal>
    </>
  )
}

export default UpcomingTransactionsPage