import Box from "@ui/Box/Box";
import React, { useState, useEffect } from 'react'
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import style from "../UpcomingTransactions.module.scss";
import { getNotificationData } from "App/api/notification"
import { encryptData } from "App/utils/helpers";
import { useRouter } from "next/router";
import notificationPage from "./NotificationPage"
const NotificationPopup = () => {
    const router = useRouter();
    const [notificationDetailsList, setNotificationDetailsList] = useState<any>();
    const fetchNotification = async () => {
        try {
            
            // setLoader(true);
         
            let result = await getNotificationData();
            console.log(result?.data);
            setNotificationDetailsList(result?.data?.sysmSipTopReminders);
            // setLoader(false);
            console.log(result?.data?.sysmSipTopReminders);
        } catch (error: any) {
            console.log(error);
            //toastAlert("error", error);
        }
    };
    useEffect(() => {
        fetchNotification();
        return () => {
        }
    }, [])

    return (
        <>
            <Box className="container">
                <Box className="row">
                    <Box className="col-5">
                        <Box className="row">

                            <Card className="p-0" css={{
                                borderRadius: "8px 8px 8px 8px",
                                border: "1px solid #82afd9",
                            }}>
                                <Box className="row ">

                                    <Box className="col" >

                                        <Text
                                            // @ts-ignore
                                            weight="normal"
                                            size="h6"
                                            className="p-2 rounded text-white"
                                            css={{ backgroundColor: "var(--colors-blue1)", borderBottom: "1px solid #4d7ea8 !important" }}
                                        >
                                            Notification
                                        </Text>
                                        {notificationDetailsList?.map((record: any) => {
                                            return (<>
                                                <Box className="row " >
                                                    <Box className="col-6">
                                                        <Text className="p-2 rounded"
                                                            // @ts-ignore
                                                            weight="normal"
                                                            size="h6"
                                                        // css={{ color: "var(--colors-blue1)" }}
                                                        >
                                                            {record?.trxnType} of {record?.amount} on {record?.sysmDate}
                                                        </Text>
                                                    </Box>
                                                    <Box className="col-6 ">
                                                        <Text className="p-2 rounded text-end"
                                                            // @ts-ignore
                                                            weight="normal"
                                                            size="h6"
                                                            css={{ color: "var(--colors-blue1)" }}
                                                        >
                                                            2 min ago
                                                        </Text>
                                                    </Box>
                                                </Box>
                                                <hr className="border border-primary my-0" />
                                                </>);
                                        })}{/* 
                                        <hr className="border border-primary my-0" />
                                        <Box className="row " >
                                            <Box className="col-6">
                                                <Text className="p-2 rounded"
                                                    // @ts-ignore
                                                    weight="normal"
                                                    size="h6"
                                                // css={{ color: "var(--colors-blue1)" }}
                                                >
                                                    SIP of 10,000 on 25 Nov 2022
                                                </Text>
                                            </Box>
                                            <Box className="col-6 ">
                                                <Text className="p-2 rounded text-end"
                                                    // @ts-ignore
                                                    weight="normal"
                                                    size="h6"
                                                    css={{ color: "var(--colors-blue1)" }}
                                                >
                                                    3 Oct 2022 at 11.15
                                                </Text>
                                            </Box>
                                        </Box>
                                        <hr className="border border-primary my-0" />
                                        <Box className="row" >
                                            <Box className="col-6">
                                                <Text className="p-2 rounded"
                                                    // @ts-ignore
                                                    weight="normal"
                                                    size="h6"
                                                //css={{ color: "var(--colors-blue1)" }}
                                                >
                                                    Your KYc Updated Sucessfully
                                                </Text>
                                            </Box>
                                            <Box className="col-6 ">
                                                <Text className="p-2 rounded text-end"
                                                    // @ts-ignore
                                                    weight="normal"
                                                    size="h6"
                                                    css={{ color: "var(--colors-blue1)" }}
                                                >
                                                    2 Oct 2022 at 16.20
                                                </Text>
                                            </Box>
                                        </Box>
                                        <hr className="border border-primary my-0" />
                                        <Box className="row " >
                                            <Box className="col-7">
                                                <Text className="p-2 rounded"
                                                    // @ts-ignore
                                                    weight="normal"
                                                    size="h6"
                                                // css={{ color: "var(--colors-blue1)" }}
                                                >
                                                    Your Transactioon pending from bank side
                                                </Text>
                                            </Box>
                                            <Box className="col-5">
                                                <Text className="p-2 rounded text-end"
                                                    // @ts-ignore
                                                    weight="normal"
                                                    size="h6"
                                                    css={{ color: "var(--colors-blue1)" }}
                                                >
                                                    3 Oct 2022 at 11.15
                                                </Text>
                                            </Box>
                                        </Box> */}
                                        <hr className="border border-primary my-0" />
                                        <Box className="row justify-content-end m-1">
                                            <Button
                                                className="mt-2 px-3 py-1 "
                                                color="yellowGroup"
                                                size="sm"
                                               // onClick={() => router.push("./NotificationP")}
                                            >
                                                <Text
                                                    // @ts-ignore
                                                    weight="bold"
                                                    size="h6"
                                                    //@ts-ignore
                                                    color="gray8"
                                             className={style.button}
                                                >
                                                    View All
                                                </Text>
                                            </Button>
                                        </Box>
                                    </Box>
                                </Box>
                            </Card>
                        </Box>
                    </Box>

                </Box>
            </Box>
            
        </>
    )
}

export default NotificationPopup