import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout, { toastAlert } from "App/shared/components/Layout";
import React, { useState, useEffect } from 'react'
import Box from "@ui/Box/Box";
import SelectMenu from "@ui/Select/Select";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import styles from "App/modules/NotificationModule/UpcomingTransactions.module.scss";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import Card from '@ui/Card/Card';
import DeleteIcon from "App/icons/DeleteIcon";
import router from "next/router";
import {
    getUpcomingTransactionsDD,
    fetchSysTxnSeriesWise,
    deleteTxnOtp,
    deleteTxnValidateotp,
    deleteTxn,
} from "App/api/UpcomingTransactions";
import useNotificationStore from "../store";
import { encryptData, getUser } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";
import DialogModal from "@ui/AlertDialog/ModalDialog";
import { Formik } from "formik";
import * as Yup from "yup";
import create from "zustand";
import { getLS } from "@ui/DataGrid/utils";

export type VerifyTypes = {
    otp: string;
};

interface StoreTypes {
    verifyPay: VerifyTypes;
}

const useStore = create<StoreTypes>((set) => ({
    //* initial state
    verifyPay: {
        otp: "",
    }
}))


const TransactionDetails = () => {
    const local: any = getUser();
    const [seriesWise, setSeriesWise] = useState<any>([])
    const { verifyPay } = useStore((state: any) => state);
    const [load, setLoad] = useState<any>(false);
    const [loadDel, setLoadDel] = useState<any>(false);
    const [loadConfirm, setLoadConfirm] = useState<any>(false);
    const [period, setPeriod] = useState<any>("1");
    const [durationType, setDurationType] = useState<any>([]);
    const [openDelModal, setOpenDelModal] = useState<any>(false)
    const [openMulDelModal, setOpenMulDelModal] = useState<any>(false)
    const [delTxn, setDelTxn] = useState<any>({});
    const [txnCount, setTxnCount] = useState<any>("");
    const [bunchSlt, setBunchSlt] = useState<any>("N");
    const [checkedAll, setCheckedAll] = useState(false);
    const [singleFlag, setSingleFlag] = useState<any>(false);
    const [checked, setChecked] = useState<any>([]);
    const [openDelConfirmModal, setDelConfirmModal] = useState(false);
    const [confirmLoad, setConfirmLoad] = useState(false);
    const [showDelete, setShowDelete] = useState(false);
    const {
        schemeID,
        sysTrnList,
        setSysTrnList
    } = useNotificationStore();
    const sys = getLS("sysTrnList");

    const delModalObj = {
        otp: Yup.string()
            .required("otp is required")
    }

    const [validationSchema, setValidationSchema] = useState(
        delModalObj
    );


    const fetchDurationDD = async () => {
        try {
            let obj = {
                //@ts-ignore
                BasicID: sysTrnList?.sysmTrxnlist?.[0].basicId === undefined ? sys?.sysmTrxnlist?.[0].basicId : sysTrnList?.sysmTrxnlist?.[0].basicId,
                isGroupLeaderWise: 'N'
            }
            const enc: any = encryptData(obj);
            let result: any = await getUpcomingTransactionsDD(enc);
            setDurationType(result?.data[2]?.Value);

        } catch (error) {
            console.log(error);
            toastAlert("error", error);

        }
    }

    const fetchSeriesWise = async (val: any) => {
        try {
            setLoad(true);

            let obj: any = {
                //@ts-ignore
                basicId: sysTrnList?.sysmTrxnlist?.[0].basicId === undefined ? sys?.sysmTrxnlist?.[0].basicId : sysTrnList?.sysmTrxnlist?.[0].basicId,
                isGroupLeaderWise: 'N',
                period: val,
                schmId: schemeID,
                //@ts-ignore
                txnType: sysTrnList?.sysmTrxnlist?.[0].trxnType === undefined ? sys?.sysmTrxnlist?.[0].trxnType : sysTrnList?.sysmTrxnlist?.[0].trxnType,
                //@ts-ignore
                systemSeriesNo: sysTrnList?.sysmTrxnlist?.[0].SystemSeriesNo === undefined ? sys?.sysmTrxnlist?.[0].SystemSeriesNo : sysTrnList?.sysmTrxnlist?.[0].SystemSeriesNo
            }
            // console.log(obj, "obj wise");
            let enc: any = encryptData(obj)
            let result: any = await fetchSysTxnSeriesWise(enc);
            // console.log(result, "apiout");
            setSeriesWise(result?.data)
            let arr: any = result?.data.filter((el: any) => el?.isDelete === 'Y')
            if (arr.length > 0) {
                setShowDelete(true);
            }
            setBunchSlt("N");
            setCheckedAll(false);
            setSingleFlag(false);
            setChecked([]);
            setTxnCount("");
            setLoad(false);
        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoad(false);
        }
    }

    const deleteOtp = async (val: any, txnCt: any) => {
        setConfirmLoad(true);

        try {
            setLoadDel(true);
            let obj: any = {
                userid: local?.userid,
                mobile: local?.mobile,
                basicId: local?.basicid,
                name: val?.memberName,
                txncount: txnCt,
                txntype: val?.subTrxnType,
                scheme: val?.schemeName,
                rmname: local?.RmName,
                amt: val?.amount,
                systemSeriesNo: val?.SystemSeriesNo,
                otp: "",
            }
            let enc: any = encryptData(obj);
            let result: any = await deleteTxnOtp(enc);
            if (result.status === "Success") {
                setConfirmLoad(false);
                if (checked.length > 0) {
                    setOpenMulDelModal(true);/// condition for selected add
                } else {
                    setOpenDelModal(true);
                }
                setDelConfirmModal(false);
                // console.log(obj)
                toastAlert("success", "OTP Sent Successfully");
                setLoadDel(false);
            }
        } catch (error) {
            console.log(error);
            setConfirmLoad(false);
            toastAlert("error", error);
            setLoadDel(false);
        }
    }
    const deleteValidateOtp = async (val: any) => {
        try {
            setLoadConfirm(true);
            let obj: any = {
                userid: local?.userid,
                mobile: local?.mobile,
                basicId: local?.basicid,
                name: delTxn?.memberName,
                txncount: txnCount,
                txntype: delTxn?.subTrxnType,
                scheme: delTxn?.schemeName,
                rmname: local?.RmName,
                amt: delTxn?.amount,
                systemSeriesNo: delTxn?.SystemSeriesNo,
                otp: val?.otp,
            }

            let enc: any = encryptData(obj)
            let result: any = await deleteTxnValidateotp(enc);
            if (result?.status === "Success") {
                toastAlert("success", "OTP Validated Successfully");
                deleteTran();
            }
            else {
                setLoadConfirm(false);
                setOpenDelModal(false);
                toastAlert("error", "Invalid otp");
            }
            // console.log(obj, "validate")
        } catch (error) {
            console.log(error);
            setLoadConfirm(false);
            toastAlert("error", error);
        }
    }

    const deleteTran = async () => {
        try {
            // console.log(delTxn, "delete");
            let subObj: any;
            if (bunchSlt === "Y") {
                subObj = [{
                    basicId: delTxn?.basicId,
                    finalFinTransactionNo: '',
                    amount: delTxn?.amount,
                    systemSeriesNo: delTxn?.SystemSeriesNo,
                },]
            } else {
                if (checked.length === 0) {
                    subObj = [{
                        basicId: delTxn?.basicId,
                        finalFinTransactionNo: delTxn?.FinalFinTransactionNo,
                        amount: delTxn?.amount,
                        systemSeriesNo: delTxn?.SystemSeriesNo,
                    },]
                } else {
                    subObj = checked;
                }
            }
            let obj: any = {
                isBunchWise: bunchSlt,
                txnType: 'SIP',
                deleteTxnList: subObj,
            }
            // console.log(obj, "deletetxn")
            let enc: any = encryptData(obj)
            let result: any = await deleteTxn(enc);
            if (result.status === "Success") {
                setOpenDelModal(false);
                toastAlert("success", "Transaction Deleted");
                setBunchSlt("N");
                setDelTxn("0");
                setLoadConfirm(false);
                fetchSeriesWise(period);
                fetchDurationDD();
            }
            else {
                setOpenDelModal(false);
                setLoadConfirm(false);
                toastAlert("warn", result?.msg);
            }
        } catch (error) {
            console.log(error);
            setOpenDelModal(false);
            toastAlert("error", error);
            setLoadConfirm(false);
        }
    }
    const selectAll = (val: any) => {
        // console.log(val);
        if (val) {
            let arr: any = [];
            seriesWise.forEach((ele: any) => {
                if (ele?.isDelete === "Y") {
                    let i: any = ele.FinalFinTransactionNo;
                    let check: any = document.getElementById(i);
                    check.checked = val
                    let obj: any = {
                        basicId: ele?.basicId,
                        finalFinTransactionNo: ele?.FinalFinTransactionNo,
                        amount: ele?.amount,
                        systemSeriesNo: ele?.SystemSeriesNo,
                    }
                    arr.push(obj);
                }
            });
            setChecked(arr);
        } else {
            seriesWise.forEach((ele: any) => {
                if (ele?.isDelete === "Y") {
                    let i: any = ele.FinalFinTransactionNo;
                    let check: any = document.getElementById(i);
                    check.checked = val
                }
            });
            setChecked([]);
        }
    };
    useEffect(() => {
        (async () => {
            await fetchSeriesWise(period);
        })()
        return () => { }
    }, [period])
    useEffect(() => {
        (async () => {
            await fetchDurationDD();
        })()
        return () => { }
    }, [])

    useEffect(() => {
        // console.log(checked.length,"checked")
        if (checked.length > 0) {
            setSingleFlag(true)
        } else {
            setSingleFlag(false)
        }

        return () => { }
    }, [checked])


    return (
        <>
            <Layout>
                <PageHeader title="Upcoming Transactions" rightContent={<></>} />
                <>
                    <Box className="row justify-content-start">
                        <Box className="col-auto text-end pe-0">
                            <Button
                                className="p-2"
                                color="yellowGroup"
                                size="md"
                                onClick={() => { router.push("/UpcomingTransactions") }}
                            >
                                <Box>
                                    <Text
                                        //@ts-ignore 
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                    // className={`${styles.button}`}
                                    >
                                        Back
                                    </Text>
                                </Box>
                            </Button>
                        </Box>
                    </Box>
                    <Box className="container">
                        <Box className="row justify-content-between ps-2 pe-4 py-1 rounded" css={{ backgroundColor: "#b3daff" }}>
                            <Box className="col-auto">
                                <Text
                                    //@ts-ignore 
                                    size="h4" className="pt-4">
                                    {/* @ts-ignore */}
                                    <span style={{ color: "var(--colors-blue1)", fontSize: "0.85rem" }}>Upcoming SIP of : </span> {sysTrnList?.SystemSeriesDetails === undefined ? sys?.SystemSeriesDetails : sysTrnList?.SystemSeriesDetails}
                                </Text>
                            </Box>
                            <Box className="col-auto">
                                <Box className="row justify-content-end" >
                                    <Box className="col-auto">

                                        <SelectMenu
                                            value={period}
                                            items={durationType}
                                            label="Investor"
                                            bindValue={"Value"}
                                            bindName={"Text"}
                                            onChange={(e: any) => {
                                                setPeriod(e?.Value || "")
                                            }
                                            }
                                        />
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                    <Box>
                        <Card>
                            <Box className="container-fluid">
                                <Box className="row">
                                    <Box css={{ overflow: "auto" }}>
                                        <span style={{ color: "var(--colors-blue1)", fontSize: "0.75rem" }}>Transactions Display Count: {load ? <> <Loader /></> : seriesWise?.length} </span>
                                        <Table>
                                            <ColumnParent
                                                //@ts-ignore
                                                className="text-capitalize" css={{ backgroundColor: "#FFFFFF", height: "auto" }}
                                            >
                                                <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center", borderBottom: "solid 1px #FAAC05" }}>
                                                    <Column className="text-start pt-4">
                                                        <Input type="checkbox"
                                                            name={`checkAll`}
                                                            // value={checkedAll}
                                                            disabled={period === "1"}
                                                            checked={checkedAll}
                                                            onChange={(e: any) => {
                                                                selectAll(e.target.checked)
                                                                setCheckedAll(e.target.checked)
                                                            }}
                                                        /></Column>
                                                    <Column>Txn No</Column>
                                                    <Column>Type</Column>
                                                    <Column>Scheme</Column>
                                                    <Column>Folio</Column>
                                                    <Column>Amount</Column>
                                                    <Column>Bank/AC</Column>
                                                    <Column>Txn Date</Column>
                                                    <Column>Paid</Column>
                                                    <Column>Action</Column>
                                                </ColumnRow>
                                            </ColumnParent>
                                            <DataParent>

                                                {load ? <>
                                                    <DataRow><DataCell colSpan={10}><Loader /></DataCell></DataRow>
                                                </> : <>
                                                    {seriesWise?.length === 0 ? <>
                                                        <DataRow><DataCell colSpan={10} >No Transaction Found</DataCell></DataRow>
                                                    </> : <>
                                                        {seriesWise?.map((record: any, i: any) => {
                                                            return (
                                                                <DataRow>
                                                                    <DataCell className="text-center pt-4">
                                                                        {record?.isDelete === "Y" ? <>
                                                                            <Input
                                                                                id={record?.FinalFinTransactionNo}
                                                                                value={record?.FinalFinTransactionNo}
                                                                                type="checkbox"
                                                                                disabled={period === "1"}
                                                                                onChange={(e: any) => {
                                                                                    // console.log(e.target.checked);
                                                                                    if (e.target.checked) {
                                                                                        // console.log(record,"record");
                                                                                        let obj: any = {
                                                                                            basicId: record?.basicId,
                                                                                            finalFinTransactionNo: record?.FinalFinTransactionNo,
                                                                                            amount: record?.amount,
                                                                                            systemSeriesNo: record?.SystemSeriesNo,
                                                                                        }
                                                                                        setChecked([...checked, obj])
                                                                                    } else {
                                                                                        let arr = checked.filter((item: any) => {

                                                                                            return item.finalFinTransactionNo !== e.target.value;
                                                                                        });
                                                                                        setChecked(arr);
                                                                                    }
                                                                                }}
                                                                            />
                                                                        </> : <></>}</DataCell>
                                                                    <DataCell className="text-center py-0">{record?.FinalFinTransactionNo}</DataCell>
                                                                    <DataCell className="text-center py-0">{record?.trxnType}</DataCell>
                                                                    <DataCell className="text-center py-0">{record?.orgSchemeName}</DataCell>
                                                                    <DataCell className="text-center py-0">{record?.folioNo}</DataCell>
                                                                    <DataCell className="text-center py-0">₹ {record?.amount}</DataCell>
                                                                    <DataCell className="text-center py-0">{record?.bankAcc}</DataCell>
                                                                    <DataCell className="text-center py-0">{record?.sysmDate}</DataCell>
                                                                    <DataCell className="text-center py-0">{record?.sysmCount}</DataCell>
                                                                    <DataCell className="text-end">
                                                                        {/* @ts-ignore */}
                                                                        <Box className="row justify-content-end">
                                                                            <Box className="col-auto">
                                                                                {/* @ts-ignore */}
                                                                                {record?.isDelete === "Y" ? <>
                                                                                    <Button size="h6"
                                                                                        disabled={singleFlag}
                                                                                        onClick={() => {
                                                                                            setTxnCount("1");
                                                                                            setBunchSlt("N");
                                                                                            setDelConfirmModal(true);
                                                                                            //setOpenDelModal(true);
                                                                                            setDelTxn(record);
                                                                                            //deleteOtp(record, "1")

                                                                                        }}
                                                                                        css={{
                                                                                            backgroundColor: "transparent",
                                                                                            textAlign: "center",
                                                                                            cursor: "pointer",
                                                                                        }}
                                                                                    >
                                                                                        <DeleteIcon color="orange" className="mt-1" />
                                                                                    </Button>
                                                                                </> : <>
                                                                                    <Text size="h6" css={{ textAlign: "center", color: "red", width: "80px" }}>
                                                                                        Cannot be delete (already processed)
                                                                                    </Text>
                                                                                </>}

                                                                            </Box>
                                                                        </Box>
                                                                    </DataCell>
                                                                </DataRow>
                                                            );
                                                        })}
                                                    </>}
                                                </>}
                                            </DataParent>
                                        </Table>
                                    </Box>
                                </Box>
                            </Box>
                        </Card>
                    </Box>
                    <Box>
                        {period === "1" ? <></> : <>
                            {showDelete ?
                                <Card>
                                    <Box css={{ m: 5, p: 5 }} className="text-end">
                                        <Button
                                            type="submit"
                                            color="yellow"
                                            disabled={checked.length === 0 || loadDel}
                                            onClick={() => {
                                                setDelConfirmModal(true)
                                                //  setOpenMulDelModal(true); 
                                                setTxnCount(checked.length);
                                                setDelTxn(seriesWise?.[0])
                                                setBunchSlt("N");
                                            }}>
                                            Delete Selected Transactions
                                        </Button>
                                        <Button
                                            type="submit"
                                            color="yellow"
                                            //@ts-ignore
                                            disabled={loadDel || load}
                                            onClick={() => {
                                                setBunchSlt("Y");
                                                setDelTxn(seriesWise?.[0])
                                                setTxnCount("ALL")
                                                setDelConfirmModal(true);
                                                // setOpenDelModal(true)
                                                // deleteOtp(seriesWise?.[0],"ALL")
                                            }}
                                        >
                                            {loadDel ? `Loading... ` : `Delete all ${seriesWise?.[0]?.SystemSeriesNo}`}
                                        </Button>
                                    </Box>
                                </Card> : null}
                        </>}
                    </Box>
                    <DialogModal
                        open={openDelModal}
                        setOpen={setOpenDelModal}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "30%" },
                        }}
                    >
                        <Box className="col-md-12 col-sm-12 col-lg-12">
                            <Box className="modal-header p-3">
                                <Box className="row">
                                    <Box className="col-auto text-light">Delete Transaction Warning</Box>
                                </Box>
                            </Box>
                            <Box
                                className="text-start ms-4 modal-body modal-body-scroll"
                                css={{ padding: "0.5rem" }} >
                                <Text css={{ mt: 5, pt: 10 }}>
                                    Txn({delTxn?.FinalFinTransactionNo}) of<br /> SIP of {delTxn?.schemeName}<br /> [Series No.: {delTxn?.SystemSeriesNo}]<br /> [Investor: {delTxn?.memberName}]

                                </Text>
                                <Text css={{ mt: 5, pt: 10 }}>
                                    {/* @ts-ignore */}
                                    {/* Goal Name: <b>{fullGoal.goalName}</b><br /> */}
                                    Are you sure to delete this Transaction?

                                </Text>
                                <Formik
                                    initialValues={verifyPay}
                                    validationSchema={Yup.object().shape(validationSchema)}
                                    enableReinitialize
                                    onSubmit={(values, { resetForm }) => {
                                        // console.log(values);

                                        deleteValidateOtp(values)
                                    }}
                                >
                                    {({
                                        values,
                                        handleChange,
                                        handleBlur,
                                        handleSubmit,
                                        isSubmitting,
                                        errors,
                                        touched,
                                        setFieldValue,
                                        submitCount
                                    }) => (
                                        <>
                                            <Box css={{ mt: 5, pt: 10 }} className="text-start row">
                                                <Box className="col-auto">
                                                    <Input
                                                        name="otp"
                                                        label="Enter OTP"
                                                        placeholder="Enter OTP"
                                                        className="border border-2"
                                                        //@ts-ignore
                                                        error={submitCount ? errors.otp : null}
                                                        onChange={handleChange}
                                                        value={values?.otp}
                                                    />
                                                </Box>
                                            </Box>

                                            <Box css={{ mt: 5, pt: 10 }} className="text-end">
                                                <Button
                                                    type="submit"
                                                    color="yellow"
                                                    disabled={loadConfirm}
                                                    onClick={() => { setOpenDelModal(false) }}>
                                                    Cancel
                                                </Button>
                                                <Button
                                                    type="submit"
                                                    color="yellow"
                                                    //@ts-ignore
                                                    onClick={handleSubmit}
                                                    disabled={loadConfirm}
                                                >
                                                    {loadConfirm ? `Loading... ` : ` Delete Now`}
                                                </Button>
                                            </Box>
                                        </>)}
                                </Formik>
                            </Box>
                        </Box>
                    </DialogModal>
                    <DialogModal
                        open={openMulDelModal}
                        setOpen={setOpenMulDelModal}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "30%" },
                        }}
                    >
                        <Box className="col-md-12 col-sm-12 col-lg-12">
                            <Box className="modal-header p-3">
                                <Box className="row">
                                    <Box className="col-auto text-light">Delete Transaction Warning</Box>
                                </Box>
                            </Box>
                            <Box
                                className="text-start ms-4 modal-body modal-body-scroll"
                                css={{ padding: "0.5rem" }} >
                                <Text css={{ mt: 5, pt: 10 }}>
                                    Delete {txnCount} Txn of<br /> SIP of {delTxn?.schemeName}<br /> [Series No.: {delTxn?.SystemSeriesNo}]<br /> [Investor: {delTxn?.memberName}]

                                </Text>
                                <Text css={{ mt: 5, pt: 10 }}>
                                    {/* @ts-ignore */}
                                    {/* Goal Name: <b>{fullGoal.goalName}</b><br /> */}
                                    Are you sure to delete these Transaction?

                                </Text>
                                <Formik
                                    initialValues={verifyPay}
                                    validationSchema={Yup.object().shape(validationSchema)}
                                    enableReinitialize
                                    onSubmit={(values, { resetForm }) => {
                                        // console.log(values);
                                        // setOpenMulDelModal(false);
                                        deleteValidateOtp(values)
                                    }}
                                >
                                    {({
                                        values,
                                        handleChange,
                                        handleBlur,
                                        handleSubmit,
                                        isSubmitting,
                                        errors,
                                        touched,
                                        setFieldValue,
                                        submitCount
                                    }) => (
                                        <>
                                            <Box css={{ mt: 5, pt: 10 }} className="text-start row">
                                                <Box className="col-auto">
                                                    <Input
                                                        name="otp"
                                                        label="Enter OTP"
                                                        placeholder="Enter OTP"
                                                        className="border border-2"
                                                        //@ts-ignore
                                                        error={submitCount ? errors.otp : null}
                                                        onChange={handleChange}
                                                        value={values?.otp}
                                                    />
                                                </Box>
                                            </Box>

                                            <Box css={{ mt: 5, pt: 10 }} className="text-end">
                                                <Button
                                                    type="submit"
                                                    color="yellow"
                                                    disabled={loadConfirm}
                                                    onClick={() => { setOpenMulDelModal(false) }}>
                                                    Cancel
                                                </Button>
                                                <Button
                                                    type="submit"
                                                    color="yellow"
                                                    //@ts-ignore
                                                    onClick={handleSubmit}
                                                    disabled={loadConfirm}
                                                >
                                                    {loadConfirm ? `Loading... ` : ` Delete Now`}
                                                </Button>
                                            </Box>
                                        </>)}
                                </Formik>
                            </Box>
                        </Box>

                    </DialogModal>
                    <DialogModal
                        open={openDelConfirmModal}
                        setOpen={setDelConfirmModal}
                        css={{
                            "@bp0": { width: "90%" },
                            "@bp1": { width: "30%" },
                        }}
                    >
                        <Box className="col-md-12 col-sm-12 col-lg-12">
                            <Box className="modal-header p-3">
                                <Box className="row">
                                    <Box className="col-auto text-light">Delete Transaction Warning</Box>
                                </Box>
                            </Box>
                            <Box
                                className="text-start ms-4 modal-body modal-body-scroll"
                                css={{ padding: "0.5rem" }} >

                                <Box className="row">

                                    <Box className="col-12 pt-4 pb-5 text-center">
                                        {/* @ts-ignore */}
                                        <Text size="h4" className="pb-1" css={{ color: "#005CB3" }}>Are you sure want to delete</Text>

                                        <Box css={{ mt: 5, pt: 10 }} className="text-end">
                                            <Button
                                                type="submit"
                                                color="yellow"
                                                //@ts-ignore
                                                onClick={() => setDelConfirmModal(false)}
                                                disabled={confirmLoad}
                                            >
                                                Cancel
                                            </Button>
                                            <Button
                                                type="submit"
                                                color="yellow"
                                                onClick={() => { deleteOtp(delTxn, txnCount) }}
                                                disabled={confirmLoad}
                                            >
                                                {confirmLoad ? <>Loading... </> : <>Yes </>}
                                            </Button>

                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                        </Box>

                    </DialogModal>
                </>
            </Layout>
        </>
    )
}

export default TransactionDetails