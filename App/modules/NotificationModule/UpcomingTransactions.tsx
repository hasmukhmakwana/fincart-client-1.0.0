import React from 'react'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout from "App/shared/components/Layout";
import UpcomingTransactionsPage  from './components/UpcomingTransactionsPage';

const UpcomingTransactions = () => {

  return (
    <>
        <Layout>
            <PageHeader title="Upcoming Transactions" rightContent={<></>} />
            <UpcomingTransactionsPage/>
        </Layout>
    </>
)
}

export default UpcomingTransactions