import Badge from "@ui/Badge/Badge";
import Box from "@ui/Box/Box";
import Card from "@ui/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import Input from "@ui/Input";
import Text from "@ui/Text/Text";
import DownArrow from "App/icons/DownArrow";
import StarFill from "App/icons/StarFill";
import X from "App/icons/X";
import XCircle from "App/icons/XCircle";
import Image from "next/image";
import React from "react";
import SelectMenu from "@ui/Select/Select";
import { StyledTabs, TabsList } from "@ui/Tabs/Tabs.styles";
import { TabsContent, TabsTrigger } from "@radix-ui/react-tabs";
import Radio from "@ui/Radio/Radio";
import { RadioGroup } from "@ui/Radio/";
import { Button } from "@ui/Button/Button";
import { WEB_URL } from "App/utils/constants";
const CheckoutPage = () => {
  return (
    <Box background="gray">
      <Box className="container-fluid">
        <Box className="row">
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Card>
              <Text weight="bold" size="h3" border="default">
                Your Cart
              </Text>
              <Box css={{ p: 20 }}>
                <Box className="row">
                  <GroupBox css={{ pb: 20 }} borderBottom>
                    <Box className="col-auto">
                      <Box>
                        <img
                          width={75}
                          height={75}
                          src={`${WEB_URL}/axis-bank.png`}
                          alt="logo"
                        />
                      </Box>
                    </Box>
                    <Box className="col">
                      <Box className="row">
                        <Box className="col-auto">
                          <Box css={{ mt: 15 }}>
                            <Text weight="extrabold" size="h2" color="blue">
                              Axis Mid Cap Fund Growth
                            </Text>
                          </Box>
                        </Box>
                        <Box className="col">
                          <Input
                            label="Folio"
                            name="firstName"
                            placeholder="Folio"
                          />
                        </Box>
                        <Box className="col">
                          <SelectMenu
                            items={[
                              { id: "1", name: "Baroda" },
                              { id: "2", name: "Surat" },
                            ]}
                            bindValue={"id"}
                            bindName={"Goal"}
                            label={"Goal"}
                            placeholder={"No Goal"}
                            onChange={()=>{}}
                          />
                        </Box>
                        <Box className="col">
                          <Input
                            label="Amount"
                            name="firstName"
                            placeholder="Amount"
                          />
                        </Box>
                        <Box className="col-auto text-end">
                          <GroupBox css={{ mt: 5 }} position="right">
                            <X color="orange" size="25" />
                          </GroupBox>
                        </Box>
                      </Box>
                    </Box>
                  </GroupBox>
                </Box>
                <Box className="row">
                  <GroupBox css={{ pb: 20, mt: 20 }} borderBottom>
                    <Box className="col-auto">
                      <Box>
                        <img
                          width={75}
                          height={75}
                          src={`${WEB_URL}/axis-bank.png`}
                          alt="logo"
                        />
                      </Box>
                    </Box>
                    <Box className="col">
                      <Box className="row">
                        <Box className="col-auto">
                          <Box css={{ mt: 15 }}>
                            <Text weight="extrabold" size="h2" color="blue">
                              Axis Mid Cap Fund Growth
                            </Text>
                          </Box>
                        </Box>
                        <Box className="col">
                          <Input
                            label="Folio"
                            name="firstName"
                            placeholder="Folio"
                          />
                        </Box>
                        <Box className="col">
                          <SelectMenu
                            items={[
                              { id: "1", name: "Baroda" },
                              { id: "2", name: "Surat" },
                            ]}
                            bindValue={"id"}
                            bindName={"Goal"}
                            label={"Goal"}
                            placeholder={"No Goal"}
                            onChange={()=>{}}
                          />
                        </Box>
                        <Box className="col">
                          <Input
                            label="Amount"
                            name="firstName"
                            placeholder="Amount"
                          />
                        </Box>
                        <Box className="col-auto text-end">
                          <GroupBox css={{ mt: 5 }} position="right">
                            <X color="orange" size="25" />
                          </GroupBox>
                        </Box>
                      </Box>
                    </Box>
                  </GroupBox>
                </Box>
              </Box>
            </Card>
          </Box>
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Card>
              <GroupBox
                position="apart"
                align="center"
                css={{ p: 20 }}
                borderBottom
              >
                <Box>
                  <Text weight="extrabold" size="h3">
                    Payments & Confirmation
                  </Text>
                </Box>
                <Box>
                  <Text weight="bold">Total Amount : 0</Text>
                </Box>
              </GroupBox>
              <Box css={{ p: 20 }}>
                <StyledTabs defaultValue="netbanking">
                  <TabsList aria-label="Manage your account" css={{ pb: 5 }}>
                    <TabsTrigger value="netbanking" className="activetab">
                      Net Banking
                    </TabsTrigger>
                    <TabsTrigger value="previousgoals" className="inactivetab">
                      UPI
                    </TabsTrigger>
                    <TabsTrigger value="previousgoals" className="inactivetab">
                      Neft
                    </TabsTrigger>
                    <TabsTrigger value="previousgoals" className="inactivetab">
                      One-Time Mandate
                    </TabsTrigger>
                  </TabsList>
                  <TabsContent value="netbanking">
                    <SelectMenu
                      items={[
                        { id: "1", name: "Baroda" },
                        { id: "2", name: "Surat" },
                      ]}
                      bindValue={"id"}
                      bindName={"Goal"}
                      label={"Source Account"}
                      placeholder={"No Folio"}
                      onChange={()=>{}}
                    />
                  </TabsContent>
                </StyledTabs>
              </Box>
            </Card>
            <RadioGroup defaultValue="1M" className="inlineRadio">
              <Radio
                value="1M"
                label="I/We hereby confirm that this is transaction done purely at my/our sole discretion"
                id="1M"
              />
            </RadioGroup>
            <Box className="text-end">
              <Button color="yellow">Cancel </Button>
              <Button color="yellow">Confirm</Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default CheckoutPage;
