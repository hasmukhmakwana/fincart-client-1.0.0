import Holder from 'App/shared/components/Holder/Holder'
import Layout from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import React from 'react'
import CheckoutPage from './components/CheckoutPage'

const CheckoutModule = () => {
  return (
    <Layout>
    <PageHeader title='Checkout' rightContent={<Holder/>} />
    <CheckoutPage/>
    </Layout>
  )
}

export default CheckoutModule