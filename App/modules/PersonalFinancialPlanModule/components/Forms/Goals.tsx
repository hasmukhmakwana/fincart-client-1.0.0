import React, { useState, useEffect } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Pill from "@ui/Pill/Pill";
import DownArrow from "App/icons/DownArrow";
import SelectMenu from "@ui/Select/Select";
import { Button } from "@ui/Button/Button";
// import styles from "../../FinancialPlan.module.scss";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "@ui/Accordian/Accordian";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";

type SaveTypes = {
    fetchGoalData: [];
    setFilteredData: (values: any) => void;
    fetchGoalTabData: [];
};

const Goals = ({ fetchGoalData, fetchGoalTabData, setFilteredData }: SaveTypes) => {
    // console.log(fetchGoalData);
    const [selectGoal, setSelectGoal] = useState<any>(null);
    const [contentArray, setContentArray] = useState<any>({
        ExistingAssets: [],
        NewRecommendation: []
    });
    let CrrDate = new Date();
    const getMonthDifference = (startDate: any, endDate: any) => {
        startDate = new Date(startDate)
        endDate = new Date(endDate)
        return (
            endDate.getMonth() - startDate.getMonth() + 12 * (endDate.getFullYear() - startDate.getFullYear())
        );
    }

    const funPriority = () => {
        let Sortdata = [...fetchGoalData].sort((a, b) => {//@ts-ignore
            return parseInt(a?.goalpriority) > parseInt(b?.goalpriority) ? 1 : -1;
        })
        // console.log(Sortdata);
        setFilteredData(Sortdata);
    }
    const funGoalYear = () => {
        let Sortdata = [...fetchGoalData].sort((a, b) => {//@ts-ignore
            return parseInt(a?.Duration) >= parseInt(b?.Duration) ? 1 : -1;
        })
        // console.log(Sortdata);
        setFilteredData(Sortdata);
    }
    const percentCalulate = (startDate: any, endDate: any) => {
        let TillMonth = getMonthDifference(startDate, CrrDate) + 1; //Month completed till now from start date
        let TotalMonths = getMonthDifference(startDate, endDate) + 1; // Total months between start date and end date
        return (
            (Math.round((TillMonth / TotalMonths) * 100))
        );
    }
    const handleGoalWiseData = async (e: any) => {
        // console.log("GoalID");
        // console.log(e);
        // @ts-ignore
        let ExistingAssets = [];
        // @ts-ignore
        let NewRecommendation = [];

        for (let item of fetchGoalTabData) {
            // @ts-ignore
            if (item.userGoalId === e) {
                // @ts-ignore
                if (item.status === "Continue" || item.status === "Discontinue") {
                    ExistingAssets?.push(item);
                }
                else {
                    NewRecommendation?.push(item);
                }
            }
        }
        setContentArray(
            {
                // @ts-ignore
                ExistingAssets, NewRecommendation
            });
    }
    useEffect(() => {
        // console.log(percentCalulate("2022-11-01", "2023-10-31"));
        // console.log(fetchGoalTabData, "GoalsData");

        return () => {
            setSelectGoal(null);
        }
    }, [])


    return (
        <>
            <Box className="row px-2" >
                {/* <Box className="col-auto mt-4 "> */}
                <Box className="col-auto mt-4">
                    <Text
                        //@ts-ignore
                        weight="normal"
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        css={{ color: "var(--colors-blue1)" }}
                    >
                        Your Goals
                    </Text>
                </Box>
                <Box className="col-lg-3 my-0 mt-3 p-0">
                    <SelectMenu
                        value={selectGoal?.usergoalid}
                        items={fetchGoalData}
                        //label={"Your Goals"}
                        bindValue={"usergoalid"}
                        bindName={"goalname"}
                        onChange={(e) => { setSelectGoal(e) }}
                        isClearable
                    />
                </Box>

                <Box className="col-lg-7 ms-5 text-end">
                    {selectGoal === null || selectGoal === undefined ? <>
                        <Text
                            //@ts-ignore
                            weight="normal"
                            size="h6"
                            //@ts-ignore
                            color="gray8"
                            // className={styles.button}
                            //className='me-5'
                            css={{ color: "var(--colors-blue1)", mr: 55 }}
                        >
                            Sort by
                        </Text>
                        <Button
                            className={`col-auto py-1 mx-0 px-4 `}
                            //   ${styles.button}
                            color="yellowGroup"
                            size="md"
                            onClick={() => {
                                funPriority()
                                // setVerifyExpenses('')
                                // setButtonText("Add");
                            }}
                        >
                            <Text
                                //@ts-ignore
                                weight="normal"
                                size="h6"
                                //@ts-ignore
                                color="gray8"
                            // className={styles.button}
                            >
                                Priority
                            </Text>
                        </Button>&nbsp;
                        <Button
                            className={`col-auto py-1 mx-0 px-4 text-end`}
                            //    ${styles.button}
                            color="yellowGroup"
                            size="md"
                            onClick={() => { funGoalYear() }}
                        //   loading={loading}
                        >
                            <Text
                                // @ts-ignore
                                weight="normal"
                                size="h6"
                                //@ts-ignore
                                color="gray8"
                            // className={styles.button}
                            >
                                Year
                                {/* {buttonText} */}
                            </Text>
                        </Button>
                    </> : <></>}
                </Box>
            </Box>
            <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                <Accordion className='p-2'
                    type="single"
                    // defaultValue={`${mutualFundPortfolioData[0]?.id}`}
                    collapsible
                    onValueChange={(e) => {
                        handleGoalWiseData(e);
                    }}
                >
                    {selectGoal === null ? <>
                        {/* @ts-ignore */}
                        {fetchGoalData.map((record: any) => {
                            // console.log(record, "item")
                            return (
                                <AccordionItem value={record?.usergoalid.toString()} css={{ borderRadius: "8px" }}>
                                    <AccordionTrigger className="row p-0">
                                        <Box className="col">
                                            <Box className="row tabtitle pe-0 justify-content-between">
                                                <Box className="col-auto text-black pt-1 pb-0">
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" className="text-capitalize">{record?.goalname || "--"}</Text>
                                                </Box>
                                                <Box className="col-auto my-0 py-0">
                                                    <Box className="text-center my-0 py-0">
                                                        {/* @ts-ignore */}
                                                        <Text size="h6" className='my-0 py-0'>Priority</Text>
                                                        {/* @ts-ignore */}
                                                        <Text size="h5" className='my-0 py-0' color="black">{record?.goalpriority || "0"}</Text>
                                                    </Box>
                                                </Box>
                                            </Box>
                                            <Box className="row tabtitlecontent">
                                                <Box className="col-lg-3">
                                                    {/* @ts-ignore */}
                                                    <Text size="h6">Current Value</Text>
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" color="black">{Number(record?.currentcost || "0").toLocaleString("en-IN")}</Text>
                                                </Box>
                                                <Box className="col-lg-3">
                                                    {/* @ts-ignore */}
                                                    <Text size="h6">Actual Net Deficit</Text>
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" color="black">{Number(record?.ActualNetDeficit || "0").toLocaleString("en-IN")}</Text>
                                                </Box>
                                                <Box className="col-lg-3">
                                                    {/* @ts-ignore */}
                                                    <Text size="h6">Total SIP Required</Text>
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" color="black">{Number(record?.TotReqSIP || "0").toLocaleString("en-IN")}</Text>
                                                </Box>
                                                <Box className="col-lg-3">
                                                    {/* @ts-ignore */}
                                                    <Text size="h6">Future Value</Text>
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" color="black">{Number(record?.futurecost || "0").toLocaleString("en-IN")}</Text>
                                                </Box>
                                            </Box>
                                            <Box className="row tabtitlecontent">
                                                <Box className="col-lg-3">
                                                    {/* @ts-ignore */}
                                                    <Text size="h6">Net Deficit</Text>
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" color="black">{Number(record?.deficit || "0").toLocaleString("en-IN")}</Text>
                                                </Box>
                                                <Box className="col-lg-3">
                                                    {/* @ts-ignore */}
                                                    <Text size="h6">Suggested SIP</Text>
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" color="black">{Number(record?.SuggestedSIP || "0").toLocaleString("en-IN")}</Text>
                                                </Box>
                                                <Box className="col-lg-3">
                                                    {/* @ts-ignore */}
                                                    <Text size="h6">Time to achieve {record?.Duration} year</Text>
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" color="black">{percentCalulate(record?.GoalStart_Date, record?.GoalEnd_Date) || "0"}%</Text>
                                                </Box>
                                                <Box className="col-lg-3">
                                                    {/* @ts-ignore */}
                                                    <Text size="h6">Addittional SIP Required</Text>
                                                    {/* @ts-ignore */}
                                                    <Text size="h5" color="black">{Number(record?.AddSip).toLocaleString("en-IN")}</Text>
                                                </Box>
                                            </Box>
                                        </Box>
                                        <Box className="col col-auto action">
                                            <DownArrow />
                                        </Box>
                                    </AccordionTrigger>
                                    <AccordionContent className="border p-3">
                                        {/* {console.log(record?.usergoalid)} */}
                                        <Box className="row border-bottom border-warning ">
                                            <Box className="col-lg-12 mb-2">
                                                {/* @ts-ignore */}
                                                <Text size="h4" >
                                                    Existing Assets(Potential Funding)
                                                </Text>
                                            </Box>
                                        </Box>
                                        {/* css={{ backgroundColor: "#b3daff" }} */}
                                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                            <Box css={{ overflow: "auto" }}>
                                                <Table>
                                                    <ColumnParent className="border border-bottom text-capitalize">
                                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                            <Column>Asset Name</Column>
                                                            <Column>Type Of Asset</Column>
                                                            <Column>Txn Type/Frequency</Column>
                                                            <Column>Current Value(Rs.)</Column>
                                                            <Column>Additional Contribution(Rs.)</Column>
                                                            <Column>Future Value(Rs.)</Column>
                                                            <Column>Time Period(Yrs)</Column>
                                                            <Column>Status</Column>
                                                        </ColumnRow>
                                                    </ColumnParent>
                                                    <DataParent>
                                                        {contentArray?.ExistingAssets.length === 0 ?
                                                            <DataRow>
                                                                <DataCell colSpan={8} className="text-center">
                                                                    No Existing Assets(Potential Funding) Found
                                                                </DataCell>
                                                            </DataRow> : <>
                                                                {contentArray?.ExistingAssets?.map((record: any) => {
                                                                    // console.log(record, "goal");

                                                                    return (
                                                                        <DataRow>
                                                                            <DataCell>{record?.SchemeName || "--"}</DataCell>
                                                                            <DataCell className="text-center">{record?.objective || "--"}</DataCell>
                                                                            <DataCell className="text-center">{record?.txnTypeName || "--"}</DataCell>
                                                                            <DataCell className="text-center">{Number(record?.assetAllocatedAmt || "0").toLocaleString("en-IN")}</DataCell>
                                                                            <DataCell className="text-center">{record?.txnTypeName === "Lumpsum" ? "--" : Number(record?.txnAmount || "0").toLocaleString("en-IN")}</DataCell>
                                                                            <DataCell className="text-center">{Number(record?.futurecost || "0").toLocaleString("en-IN")}</DataCell>
                                                                            <DataCell className="text-center">{record?.duration || "0"}</DataCell>
                                                                            {record?.status === "Discontinue" ?
                                                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                                    {record?.status}</DataCell>
                                                                                :
                                                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                                    {record?.status}</DataCell>
                                                                            }
                                                                        </DataRow>
                                                                    )
                                                                }
                                                                )}</>}
                                                    </DataParent>
                                                </Table>
                                            </Box>
                                        </Box>
                                        <Box className="row border-bottom border-warning">
                                            <Box className="col-lg-12  mb-2">
                                                {/* @ts-ignore  */}
                                                <Text size="h4" css={{ mt: 12 }}>
                                                    New Recommendations
                                                </Text>
                                            </Box>
                                        </Box>
                                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                            <Box css={{ overflow: "auto" }}>
                                                <Table >
                                                    <ColumnParent
                                                        className="border border-bottom"
                                                        //@ts-ignore
                                                        className="text-capitalize"
                                                    >
                                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                            <Column>Asset Name</Column>
                                                            <Column>Type Of Asset</Column>
                                                            <Column>Txn Type/Frequency</Column>
                                                            <Column>Current Value(Rs.)</Column>
                                                            <Column>Additional Contribution(Rs.)</Column>
                                                            <Column>Future Value(Rs.)</Column>
                                                            <Column>Time Period(Yrs)</Column>
                                                            <Column>Status</Column>
                                                        </ColumnRow>
                                                    </ColumnParent>
                                                    <DataParent>
                                                        {contentArray?.NewRecommendation.length === 0 ?
                                                            <DataRow>
                                                                <DataCell colSpan={8} className="text-center">
                                                                    No New Recommendations Found
                                                                </DataCell>
                                                            </DataRow> : <>
                                                                {contentArray?.NewRecommendation?.map((record: any) => (
                                                                    <DataRow>
                                                                        <DataCell>{record?.SchemeName || "--"}</DataCell>
                                                                        <DataCell className="text-center">{record?.objective || "--"}</DataCell>
                                                                        <DataCell className="text-center">{record?.txnTypeName || "--"}</DataCell>
                                                                        <DataCell className="text-center">{Number(record?.assetAllocatedAmt || "0").toLocaleString("en-IN")}</DataCell>
                                                                        <DataCell className="text-center">{record?.txnTypeName === "Lumpsum" ? "--" :Number(record?.txnAmount || "0").toLocaleString("en-IN")}</DataCell>
                                                                        <DataCell className="text-center">{Number(record?.futurecost || "0").toLocaleString("en-IN")}</DataCell>
                                                                        <DataCell className="text-center">{record?.duration || "0"}</DataCell>
                                                                        {record?.status === "Discontinue" ?
                                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                                {record?.status}</DataCell>
                                                                            :
                                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                                {record?.status}</DataCell>
                                                                        }
                                                                    </DataRow>
                                                                ))}</>}
                                                        <DataCell colSpan={8}></DataCell>
                                                    </DataParent>
                                                </Table>
                                            </Box>
                                        </Box>

                                        <Box className="row border-bottom border-warning">
                                            <Box className="col-lg-12 mb-2">
                                                {/* @ts-ignore */}
                                                <Text size="h4" css={{ mt: 12 }}>
                                                    RM Comments
                                                </Text>
                                            </Box>
                                        </Box>
                                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                            <Box css={{ overflow: "auto" }}>
                                                <Table>
                                                    <ColumnParent
                                                        className="border border-bottom"
                                                        //@ts-ignore  borderRadius: 7 
                                                        className="text-capitalize"
                                                    >
                                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                            <Column className='col-lg-6 text-start ps-4'>Comment</Column>
                                                            <Column>Policy Name</Column>
                                                            <Column>Date</Column>
                                                        </ColumnRow>
                                                    </ColumnParent>
                                                    <DataParent>
                                                        <DataRow className="text-center m-0 p-0 !important">
                                                            <DataCell colSpan={8}>No Comments Found</DataCell>
                                                        </DataRow>
                                                        <DataCell colSpan={7}></DataCell>
                                                    </DataParent>
                                                </Table>
                                            </Box>
                                        </Box>
                                    </AccordionContent>
                                </AccordionItem>
                            )
                        })}</> : <>
                        <AccordionItem value={selectGoal?.usergoalid?.toString()} css={{ borderRadius: "8px" }}>
                            <AccordionTrigger className="row p-0">
                                <Box className="col">
                                    <Box className="row tabtitle pe-0 justify-content-between">
                                        <Box className="col-auto text-black pt-1 pb-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-capitalize">{selectGoal?.goalname || "--"}</Text>
                                        </Box>
                                        <Box className="col-auto my-0 py-0">
                                            <Box className="text-center my-0 py-0">
                                                {/* @ts-ignore */}
                                                <Text size="h6" className='my-0 py-0'>Priority</Text>
                                                {/* @ts-ignore */}
                                                <Text size="h5" className='my-0 py-0' color="black">{selectGoal?.goalpriority || "0"}</Text>
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box className="row tabtitlecontent">
                                        <Box className="col-lg-3">
                                            {/* @ts-ignore */}
                                            <Text size="h6">Current Value</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" color="black">{Number(selectGoal?.currentcost || "0").toLocaleString("en-IN")}</Text>
                                        </Box>
                                        <Box className="col-lg-3">
                                            {/* @ts-ignore */}
                                            <Text size="h6">Actual Net Deficit</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" color="black">{Number(selectGoal?.ActualNetDeficit || "0").toLocaleString("en-IN")}</Text>
                                        </Box>
                                        <Box className="col-lg-3">
                                            {/* @ts-ignore */}
                                            <Text size="h6">Total SIP Required</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" color="black">{Number(selectGoal?.TotReqSIP || "0").toLocaleString("en-IN")}</Text>
                                        </Box>
                                        <Box className="col-lg-3">
                                            {/* @ts-ignore */}
                                            <Text size="h6">Future Value</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" color="black">{Number(selectGoal?.futurecost || "0").toLocaleString("en-IN")}</Text>
                                        </Box>
                                    </Box>
                                    <Box className="row tabtitlecontent">
                                        <Box className="col-lg-3">
                                            {/* @ts-ignore */}
                                            <Text size="h6">Net Deficit</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" color="black">{Number(selectGoal?.deficit || "0").toLocaleString("en-IN")}</Text>
                                        </Box>
                                        <Box className="col-lg-3">
                                            {/* @ts-ignore */}
                                            <Text size="h6">Suggested SIP</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" color="black">{Number(selectGoal?.SuggestedSIP || "0").toLocaleString("en-IN")}</Text>
                                        </Box>
                                        <Box className="col-lg-3">
                                            {/* @ts-ignore */}
                                            <Text size="h6">Time to achieve {selectGoal?.Duration} year</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" color="black">{percentCalulate(selectGoal?.GoalStart_Date, selectGoal?.GoalEnd_Date) || "0"}%</Text>
                                        </Box>
                                        <Box className="col-lg-3">
                                            {/* @ts-ignore */}
                                            <Text size="h6">Addittional SIP Required</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" color="black">{Number(selectGoal?.AddSip).toLocaleString("en-IN")}</Text>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="col col-auto action">
                                    <DownArrow />
                                </Box>
                            </AccordionTrigger>
                            <AccordionContent className="border p-3">
                                {/* {console.log(record?.usergoalid)} */}
                                <Box className="row border-bottom border-warning ">
                                    <Box className="col-lg-12 mb-2">
                                        {/* @ts-ignore */}
                                        <Text size="h4" >
                                            Existing Assets(Potential Funding)
                                        </Text>
                                    </Box>
                                </Box>
                                {/* css={{ backgroundColor: "#b3daff" }} */}
                                <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                    <Box css={{ overflow: "auto" }}>
                                        <Table>
                                            <ColumnParent className="border border-bottom text-capitalize">
                                                <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                    <Column>Asset Name</Column>
                                                    <Column>Type Of Asset</Column>
                                                    <Column>Txn Type/Frequency</Column>
                                                    <Column>Current Value(Rs.)</Column>
                                                    <Column>Additional Contribution(Rs.)</Column>
                                                    <Column>Future Value(Rs.)</Column>
                                                    <Column>Time Period(Yrs)</Column>
                                                    <Column>Status</Column>
                                                </ColumnRow>
                                            </ColumnParent>
                                            <DataParent>
                                                {contentArray?.ExistingAssets.length === 0 ?
                                                    <DataRow>
                                                        <DataCell colSpan={8} className="text-center">
                                                            No Existing Assets(Potential Funding) Found
                                                        </DataCell>
                                                    </DataRow> : <>
                                                        {contentArray?.ExistingAssets?.map((record: any) => {
                                                            // console.log(record, "goal");

                                                            return (
                                                                <DataRow>
                                                                    <DataCell>{record?.SchemeName || "--"}</DataCell>
                                                                    <DataCell className="text-center">{record?.objective || "--"}</DataCell>
                                                                    <DataCell className="text-center">{record?.txnTypeName || "--"}</DataCell>
                                                                    <DataCell className="text-center">{Number(record?.assetAllocatedAmt || "0").toLocaleString("en-IN")}</DataCell>
                                                                    <DataCell className="text-center">{record?.txnTypeName === "Lumpsum" ? "--" : Number(record?.txnAmount || "0").toLocaleString("en-IN")}</DataCell>
                                                                    <DataCell className="text-center">{Number(record?.futurecost || "0").toLocaleString("en-IN")}</DataCell>
                                                                    <DataCell className="text-center">{record?.duration || "0"}</DataCell>
                                                                    {record?.status === "Discontinue" ?
                                                                        <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                            {record?.status}</DataCell>
                                                                        :
                                                                        <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                            {record?.status}</DataCell>
                                                                    }
                                                                </DataRow>
                                                            )
                                                        }
                                                        )}</>}
                                            </DataParent>
                                        </Table>
                                    </Box>
                                </Box>
                                <Box className="row border-bottom border-warning">
                                    <Box className="col-lg-12  mb-2">
                                        {/* @ts-ignore  */}
                                        <Text size="h4" css={{ mt: 12 }}>
                                            New Recommendations
                                        </Text>
                                    </Box>
                                </Box>
                                <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                    <Box css={{ overflow: "auto" }}>
                                        <Table >
                                            <ColumnParent
                                                className="border border-bottom"
                                                //@ts-ignore
                                                className="text-capitalize"
                                            >
                                                <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                    <Column>Asset Name</Column>
                                                    <Column>Type Of Asset</Column>
                                                    <Column>Txn Type/Frequency</Column>
                                                    <Column>Current Value(Rs.)</Column>
                                                    <Column>Additional Contribution(Rs.)</Column>
                                                    <Column>Future Value(Rs.)</Column>
                                                    <Column>Time Period(Yrs)</Column>
                                                    <Column>Status</Column>
                                                </ColumnRow>
                                            </ColumnParent>
                                            <DataParent>
                                                {contentArray?.NewRecommendation.length === 0 ?
                                                    <DataRow>
                                                        <DataCell colSpan={8} className="text-center">
                                                            No New Recommendations Found
                                                        </DataCell>
                                                    </DataRow> : <>
                                                        {contentArray?.NewRecommendation?.map((record: any) => (
                                                           
                                                            <DataRow>

                                                                <DataCell>{record?.SchemeName || "--"}</DataCell>
                                                                <DataCell className="text-center">{record?.objective || "--"}</DataCell>
                                                                <DataCell className="text-center">{record?.txnTypeName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.assetAllocatedAmt || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.txnTypeName === "Lumpsum" ? "--": Number(record?.txnAmount || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.futurecost || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.duration || "0"}</DataCell>
                                                                {record?.status === "Discontinue" ?
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                    :
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                }
                                                            </DataRow>
                                                        ))}</>}
                                                <DataCell colSpan={8}></DataCell>
                                            </DataParent>
                                        </Table>
                                    </Box>
                                </Box>

                                <Box className="row border-bottom border-warning">
                                    <Box className="col-lg-12 mb-2">
                                        {/* @ts-ignore */}
                                        <Text size="h4" css={{ mt: 12 }}>
                                            RM Comments
                                        </Text>
                                    </Box>
                                </Box>
                                <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                    <Box css={{ overflow: "auto" }}>
                                        <Table>
                                            <ColumnParent
                                                className="border border-bottom"
                                                //@ts-ignore  borderRadius: 7 
                                                className="text-capitalize"
                                            >
                                                <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                    <Column className='col-lg-6 text-start ps-4'>Comment</Column>
                                                    <Column>Policy Name</Column>
                                                    <Column>Date</Column>
                                                </ColumnRow>
                                            </ColumnParent>
                                            <DataParent>
                                                <DataRow className="text-center m-0 p-0 !important">
                                                    <DataCell colSpan={8}>No Comments Found</DataCell>
                                                </DataRow>
                                                <DataCell colSpan={7}></DataCell>
                                            </DataParent>
                                        </Table>
                                    </Box>
                                </Box>
                            </AccordionContent>
                        </AccordionItem>
                    </>}

                </Accordion>

            </Box>
        </>
    )
}

export default Goals