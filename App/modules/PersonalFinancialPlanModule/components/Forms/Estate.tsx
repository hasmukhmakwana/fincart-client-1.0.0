import React from 'react'
import Card from "@ui/Card/Card";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";

const Estate = () => {
    return (
        <>
            <Card>
                <Box className="row mx-2">
                    <Box className="col-lg-12 col-md-12 col-sm-12 mb-3 px-0">

                        <Text
                            // @ts-ignore
                            weight="normal"
                            size="h6"
                            className="p-2"
                            css={{ backgroundColor: "#b3daff", borderBottom: "1px solid #4d7ea8 !important", borderRadius: "6px 6px 0px 0px" }}
                        >
                            General Practice
                        </Text>
                        <Box css={{ border: "1px solid #339cff !important", borderRadius: "0px 0px 6px 6px" }}>
                            <Text className="p-2"
                                // @ts-ignore
                                weight="normal"
                                size="h6"
                                css={{ color: "var(--colors-blue1)" }}
                            >
                                -Ensure that you  have appointed Nominee to all your investment.<br />
                                -Normally when a partner invests,the respective spouse must be added as nominee to all investment.
                            </Text>
                        </Box>
                    </Box>
                </Box>
                <Box className="row mx-2">
                    <Box className="col-lg-12 col-md-12 col-sm-12 mb-3 px-0">
                        <Text
                            // @ts-ignore
                            weight="normal"
                            size="h6"
                            className="p-2"
                            css={{ backgroundColor: "#b3daff", borderBottom: "1px solid #4d7ea8 !important", borderRadius: "6px 6px 0px 0px" }}
                        >
                            Recommended
                        </Text>
                        <Box css={{ border: "1px solid #339cff !important", borderRadius: "0px 0px 6px 6px" }}>
                        <Text className="p-2 rounded"
                            // @ts-ignore
                            weight="normal"
                            size="h6"
                            css={{ color: "var(--colors-blue1)" }}
                        >
                            - You can look forword at drafting & getting a WILL registered after sometime. This Would cost you only few thousands rupees but would give you a great peace of mind.
                        </Text>
                        </Box>
                    </Box>
                </Box>
            </Card>
        </>
    )
}
export default Estate