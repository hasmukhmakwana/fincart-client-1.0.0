import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "@ui/Accordian/Accordian";
import DownArrow from "App/icons/DownArrow";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";

type SaveTypes = {
    fetchRiskData: [];
    fetchRiskTabData: [];
    fetchPolicyBene: [];
};

const Risk = ({ fetchRiskData, fetchRiskTabData, fetchPolicyBene }: SaveTypes) => {

    const fetchBenef = (id: any) => {
        if (fetchPolicyBene !== null) {
            let array: any = fetchPolicyBene?.filter((i: any, e: any) => (i?.FPAssetEntId === id)).map((record: any) => {
                return String(`${record?.PolicyBeneficiaries}, `)
            })
            return array;
        } else {
            return [];
        }
    }

    return (
        <Box className="table-responsive">
            <Accordion type="single" collapsible onValueChange={(e) => { }}>
                <AccordionItem value="Risk" css={{ borderRadius: "8px" }}>
                    <AccordionTrigger className="row p-0">
                        <Box className="col">
                            <Box className="tabtitle text-black">
                                {/* @ts-ignore */}
                                <Text size="h5">Life and Term Cover</Text>
                            </Box>
                            <Box className="row tabtitlecontent">
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Existing HLV</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.HLV || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Current Assets</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.TotCurrAsset || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Existing Life Insurance</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.TotPrevLife || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Existing Term Insurance</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.TotPrevTerm || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                            </Box>
                            <Box className="row tabtitlecontent">
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Required Insurance</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.DeficitLifeTerm || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Suggested Life Insurance</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.TotNewLife || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Suggested Term Insurance</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.TotNewTerm || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                            </Box>
                        </Box>
                        <Box className="col col-auto action">
                            <DownArrow />
                        </Box>
                    </AccordionTrigger>
                    <AccordionContent className="border p-3">
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4" css={{ color: "#000000" }}>
                                    To cover your risk, you have the following - Life Insurance
                                </Text>
                                {/* @ts-ignore */}
                                <Text size="h4" css={{ mt: 12 }}>
                                    Life Insurance
                                </Text>
                            </Box>
                        </Box>
                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-2'>Asset Type</Column>
                                            <Column>Cover(Rs.)</Column>
                                            <Column>Premium(RS.)</Column>
                                            <Column>Owner</Column>
                                            <Column>Beneficiaries</Column>
                                            <Column>Term</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {fetchRiskTabData?.filter((record: any) => record?.RiskType === "LIFE" && record?.policyStatus !== "New").length === 0 || fetchRiskTabData === null ?
                                            <DataRow>
                                                <DataCell colSpan={8} className="text-center">
                                                    No Life Insurance Found
                                                </DataCell>
                                            </DataRow> : <>
                                                {fetchRiskTabData?.filter((record: any) => record?.RiskType === "LIFE" && record?.policyStatus !== "New").map((record: any) => {
                                                    // if (record?.RiskType === "LIFE" && record?.policyStatus !== "New") {
                                                    // console.log(record);

                                                    return (
                                                        <>
                                                            <DataRow className="text-center m-0 p-0 !important" css={{ background: "var(--colors-blue1)", color: "#fff" }}>
                                                                <DataCell colSpan={8}>{record?.PolicyName}</DataCell>
                                                            </DataRow>
                                                            <DataRow>
                                                                <DataCell className="text-center">{record?.AssetName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policySumAssured || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policyPremium || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.PolicyOwner || "--"}</DataCell>
                                                                <DataCell className="text-center">{fetchBenef(record?.assetId)?.length !== 0 ?
                                                                    fetchBenef(record?.assetId)?.map((i: any) => {
                                                                        return (<><span>{i}</span><br /></>);
                                                                    }) : "No policy Beneficiaries"}</DataCell>
                                                                <DataCell className="text-right">Policy Term : {record?.policyTerm || "0"}yrs<br />Premium paying term : {record?.premiumTerm || "0"}yrs</DataCell>
                                                                {record?.policyStatus === "Discontinue" ?
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.policyStatus}</DataCell>
                                                                    :
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.policyStatus}</DataCell>
                                                                }
                                                            </DataRow>
                                                        </>
                                                    )
                                                    // }
                                                })}</>}
                                        <DataCell colSpan={7}></DataCell>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4">
                                    Term Insurance
                                </Text>
                            </Box>
                        </Box>
                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-2'>Asset Type</Column>
                                            <Column>Cover(Rs.)</Column>
                                            <Column>Premium(RS.)</Column>
                                            <Column>Owner</Column>
                                            <Column>Beneficiaries</Column>
                                            <Column>Term</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {fetchRiskTabData?.filter((record: any) => record?.RiskType === "TERM" && record?.policyStatus !== "New").length === 0 || fetchRiskTabData === null ?
                                            <DataRow>
                                                <DataCell colSpan={8} className="text-center">
                                                    No New Recommendations Found
                                                </DataCell>
                                            </DataRow> : <>
                                                {fetchRiskTabData?.filter((record: any) => record?.RiskType === "TERM" && record?.policyStatus !== "New").map((record: any) => {
                                                    // if (record?.RiskType === "TERM" && record?.policyStatus !== "New") {
                                                    return (
                                                        <>
                                                            <DataRow className="text-center m-0 p-0 !important" css={{ background: "var(--colors-blue1)", color: "#fff" }}>
                                                                <DataCell colSpan={8}>{record?.PolicyName}</DataCell>
                                                            </DataRow>
                                                            <DataRow>
                                                                <DataCell className="text-center">{record?.AssetName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policySumAssured || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policyPremium || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.PolicyOwner || "--"}</DataCell>
                                                                <DataCell className="text-center">{fetchBenef(record?.assetId)?.length !== 0 ?
                                                                    fetchBenef(record?.assetId)?.map((i: any) => {
                                                                        return (<><span>{i}</span><br /></>);
                                                                    }) : "No policy Beneficiaries"}</DataCell>
                                                                <DataCell className="text-right">Policy Term : {record?.policyTerm || "0"}yrs<br />Premium paying term : {record?.premiumTerm || "0"}yrs</DataCell>
                                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                    {record?.policyStatus}</DataCell>
                                                            </DataRow>
                                                        </>
                                                    )
                                                    // }
                                                })}</>}
                                        <DataCell colSpan={7}></DataCell>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4">
                                    New Recommendations
                                </Text>
                            </Box>
                        </Box>
                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-2'>Asset Type</Column>
                                            <Column>Cover(Rs.)</Column>
                                            <Column>Premium(RS.)</Column>
                                            <Column>Owner</Column>
                                            <Column>Beneficiaries</Column>
                                            <Column>Term</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {fetchRiskTabData?.filter((record: any) => record?.RiskType !== "HEALTH" && record?.RiskType !== "OTHER" && record?.policyStatus === "New").length === 0 || fetchRiskTabData === null ?
                                            <DataRow>
                                                <DataCell colSpan={8} className="text-center">
                                                    No New Recommendations Found
                                                </DataCell>
                                            </DataRow> :
                                            <>
                                                {fetchRiskTabData?.filter((record: any) => record?.RiskType !== "HEALTH" && record?.RiskType !== "OTHER" && record?.policyStatus === "New").map((record: any) => {
                                                    // if (record?.RiskType !== "HEALTH" && record?.RiskType !== "OTHER" && record?.policyStatus === "New") {
                                                    return (
                                                        <>
                                                            <DataRow className="text-center m-0 p-0 !important" css={{ background: "var(--colors-blue1)", color: "#fff" }}>
                                                                <DataCell colSpan={8}>{record?.PolicyName}</DataCell>
                                                            </DataRow>
                                                            <DataRow>
                                                                <DataCell className="text-center">{record?.AssetName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policySumAssured || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policyPremium || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.PolicyOwner || "--"}</DataCell>
                                                                <DataCell className="text-center">{fetchBenef(record?.assetId)?.length !== 0 ?
                                                                    fetchBenef(record?.assetId)?.map((i: any) => {
                                                                        return (<><span>{i}</span><br /></>);

                                                                    }) : "No policy Beneficiaries"}</DataCell>
                                                                <DataCell className="text-right">Policy Term : {record?.policyTerm || "0"}yrs<br />Premium paying term : {record?.premiumTerm || "0"}yrs</DataCell>
                                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                    {record?.policyStatus}</DataCell>
                                                            </DataRow>
                                                        </>
                                                    )
                                                    // }
                                                })}</>}
                                        <DataCell colSpan={7}></DataCell>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4" css={{ mt: 12 }}>
                                    RM Comments
                                </Text>
                            </Box>
                        </Box>
                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-6 text-start ps-4'>Comment</Column>
                                            <Column>Policy Name</Column>
                                            <Column>Date</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        <DataRow>
                                            <DataCell colSpan={3} className="text-center" >
                                                No Comments Found
                                            </DataCell>
                                        </DataRow>
                                        {/* <DataCell colSpan={7}></DataCell> */}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </AccordionContent>
                </AccordionItem>
            </Accordion>
            <Accordion type="single" className="mt-1" collapsible onValueChange={(e) => { }}>
                <AccordionItem value="Risk" css={{ borderRadius: "8px" }}>
                    <AccordionTrigger className="row p-0">
                        <Box className="col">
                            <Box className="tabtitle">
                                {/* @ts-ignore */}
                                <Text size="h4">Health Cover</Text>
                            </Box>
                            <Box className="row tabtitlecontent">
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Existing Health Insurance</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.TotNewLife || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                                <Box className="col-lg-3">
                                    {/* @ts-ignore */}
                                    <Text size="h6">Suggested Health Insurance</Text>
                                    {/* @ts-ignore */}
                                    <Text size="h5" color="black">{Number(fetchRiskData?.TotNewHealth || "0").toLocaleString("en-IN")}</Text>
                                </Box>
                            </Box>
                        </Box>
                        <Box className="col col-auto action">
                            <DownArrow />
                        </Box>
                    </AccordionTrigger>
                    <AccordionContent className="border p-3">
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h5" css={{ mt: 12 }}>
                                    Existing Assets(Potential Funding)
                                </Text>
                            </Box>
                        </Box>
                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-2'>Asset Type</Column>
                                            <Column>Cover(Rs.)</Column>
                                            <Column>Premium(RS.)</Column>
                                            <Column>Owner</Column>
                                            <Column>Beneficiaries</Column>
                                            <Column>Term</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {fetchRiskTabData?.filter((record: any) => record?.RiskType === "HEALTH" && record?.policyStatus !== "New").length === 0 || fetchRiskTabData === null ?
                                            <DataRow>
                                                <DataCell colSpan={8} className="text-center">
                                                    No Existing Assets Found
                                                </DataCell>
                                            </DataRow> :
                                            <>
                                                {fetchRiskTabData?.filter((record: any) => record?.RiskType === "HEALTH" && record?.policyStatus !== "New").map((record: any) => {
                                                    // if (record?.RiskType === "HEALTH" && record?.policyStatus !== "New") {
                                                    return (
                                                        <>
                                                            <DataRow className="text-center m-0 p-0 !important" css={{ background: "var(--colors-blue1)", color: "#fff" }}>
                                                                <DataCell colSpan={8}>{record?.PolicyName}</DataCell>
                                                            </DataRow>
                                                            <DataRow>
                                                                <DataCell className="text-center">{record?.AssetName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policySumAssured || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policyPremium || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.PolicyOwner || "--"}</DataCell>
                                                                <DataCell className="text-center">{fetchBenef(record?.assetId)?.length !== 0 ?
                                                                    fetchBenef(record?.assetId)?.map((i: any) => {
                                                                        return (<><span>{i}</span><br /></>);
                                                                    }) : "No policy Beneficiaries"}</DataCell>
                                                                <DataCell className="text-right">Policy Term:{record?.policyTerm || "0"}yrs<br />Premium paying term: {record?.premiumTerm || "0"}yrs</DataCell>
                                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                    {record?.policyStatus}</DataCell>
                                                            </DataRow>
                                                        </>
                                                    )
                                                    // }
                                                })}</>}
                                        <DataCell colSpan={7}></DataCell>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4" css={{ mt: 12 }}>
                                    New Recommendations
                                </Text>
                            </Box>
                        </Box>
                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-2'>Asset Type</Column>
                                            <Column>Cover(Rs.)</Column>
                                            <Column>Premium(RS.)</Column>
                                            <Column>Owner</Column>
                                            <Column>Beneficiaries</Column>
                                            <Column>Term</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {fetchRiskTabData?.filter((record: any) => record?.RiskType === "HEALTH" && record?.policyStatus === "New").length === 0 || fetchRiskTabData === null ?
                                            <DataRow>
                                                <DataCell colSpan={8} className="text-center">
                                                    No New Recommendations Found
                                                </DataCell>
                                            </DataRow> :
                                            <>
                                                {fetchRiskTabData?.filter((record: any) => record?.RiskType === "HEALTH" && record?.policyStatus === "New").map((record: any) => {
                                                    // if (record?.RiskType === "HEALTH" && record?.policyStatus === "New") {
                                                    return (
                                                        <>
                                                            <DataRow className="text-center m-0 p-0 !important" css={{ background: "var(--colors-blue1)", color: "#fff" }}>
                                                                <DataCell colSpan={8}>{record?.PolicyName}</DataCell>
                                                            </DataRow>
                                                            <DataRow>
                                                                <DataCell className="text-center">{record?.AssetName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policySumAssured || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policyPremium || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.PolicyOwner || "--"}</DataCell>
                                                                <DataCell className="text-center">{fetchBenef(record?.assetId)?.length !== 0 ?
                                                                    fetchBenef(record?.assetId)?.map((i: any) => {
                                                                        return (<><span>{i}</span><br /></>);
                                                                    }) : "No policy Beneficiaries"}</DataCell>
                                                                <DataCell className="text-right">Policy Term:{record?.policyTerm || "0"}yrs<br />Premium paying term: {record?.premiumTerm || "0"}yrs</DataCell>
                                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                    {record?.policyStatus}</DataCell>
                                                            </DataRow>
                                                        </>
                                                    )
                                                    // }
                                                })}</>}
                                        <DataCell colSpan={7}></DataCell>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4" css={{ mt: 12 }}>
                                    RM Comments
                                </Text>
                            </Box>
                        </Box>
                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-6 text-start ps-4'>Comment</Column>
                                            <Column>Policy Name</Column>
                                            <Column>Date</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        <DataRow>
                                            <DataCell colSpan={3} className="text-center" >
                                                No Comments Found
                                            </DataCell>
                                        </DataRow>
                                        {/* <DataCell colSpan={7}></DataCell> */}
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </AccordionContent>
                </AccordionItem>
            </Accordion>
            <Accordion type="single" className="mt-1" collapsible onValueChange={(e) => { }}>
                <AccordionItem value="Risk" css={{ borderRadius: "8px" }}>
                    <AccordionTrigger className="row p-0">
                        <Box className="col">
                            <Box className="tabtitle">
                                {/* @ts-ignore */}
                                <Text size="h4">Other Cover</Text>
                            </Box>
                            <Box className="row tabtitlecontent">
                                <Box className="col">
                                    {/* @ts-ignore */}
                                    <Text size="h5"></Text>
                                </Box>
                                <Box className="col">
                                    {/* @ts-ignore */}
                                    <Text size="h5"></Text>
                                </Box>
                            </Box>
                        </Box>
                        <Box className="col col-auto action">
                            <DownArrow />
                        </Box>
                    </AccordionTrigger>
                    <AccordionContent className="border p-3">
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4" css={{ mt: 12 }}>
                                    Existing Assets(Potential Funding)
                                </Text>
                            </Box>
                        </Box><Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>

                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-2'>Asset Type</Column>
                                            <Column>Cover(Rs.)</Column>
                                            <Column>Premium(RS.)</Column>
                                            <Column>Owner</Column>
                                            <Column>Beneficiaries</Column>
                                            <Column>Term</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {fetchRiskTabData?.filter((record: any) => record?.RiskType === "OTHER" && record?.policyStatus !== "New").length === 0 || fetchRiskTabData === null ?
                                            <DataRow>
                                                <DataCell colSpan={8} className="text-center">
                                                    No Existing Assets Found
                                                </DataCell>
                                            </DataRow> :
                                            <>
                                                {fetchRiskTabData?.filter((record: any) => record?.RiskType === "OTHER" && record?.policyStatus !== "New").map((record: any) => {
                                                    // if (record?.RiskType === "OTHER" && record?.policyStatus !== "New") {
                                                    return (
                                                        <>
                                                            <DataRow className="text-center m-0 p-0 !important" css={{ background: "var(--colors-blue1)", color: "#fff" }}>
                                                                <DataCell colSpan={8}>{record?.PolicyName}</DataCell>
                                                            </DataRow>
                                                            <DataRow>
                                                                <DataCell className="text-center">{record?.AssetName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policySumAssured || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policyPremium || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.PolicyOwner || "--"}</DataCell>
                                                                <DataCell className="text-center">{fetchBenef(record?.assetId)?.length !== 0 ?
                                                                    fetchBenef(record?.assetId)?.map((i: any) => {
                                                                        return (<><span>{i}</span><br /></>);
                                                                    }) : "No policy Beneficiaries"}</DataCell>
                                                                <DataCell className="text-right">Policy Term:{record?.policyTerm || "0"}yrs<br />Premium paying term: {record?.premiumTerm || "0"}yrs</DataCell>
                                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                    {record?.policyStatus}</DataCell>
                                                            </DataRow>
                                                        </>
                                                    )
                                                    // }
                                                })}</>}
                                        <DataCell colSpan={7}></DataCell>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4" css={{ mt: 12 }}>
                                    New Recommendations
                                </Text>
                            </Box>
                        </Box><Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-2'>Asset Type</Column>
                                            <Column>Cover(Rs.)</Column>
                                            <Column>Premium(RS.)</Column>
                                            <Column>Owner</Column>
                                            <Column>Beneficiaries</Column>
                                            <Column>Term</Column>
                                            <Column>Status</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        {fetchRiskTabData?.filter((record: any) => record?.RiskType === "OTHER" && record?.policyStatus === "New").length === 0 || fetchRiskTabData === null ?
                                            <DataRow>
                                                <DataCell colSpan={8} className="text-center">
                                                    No New Recommendations Found
                                                </DataCell>
                                            </DataRow> :
                                            <>
                                                {fetchRiskTabData?.filter((record: any) => record?.RiskType === "OTHER" && record?.policyStatus === "New").map((record: any) => {
                                                    // if (record?.RiskType === "OTHER" && record?.policyStatus === "New") {
                                                    return (
                                                        <>
                                                            <DataRow className="text-center m-0 p-0 !important" css={{ background: "var(--colors-blue1)", color: "#fff" }}>
                                                                <DataCell colSpan={8}>{record?.PolicyName}</DataCell>
                                                            </DataRow>
                                                            <DataRow>
                                                                <DataCell className="text-center">{record?.AssetName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policySumAssured || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.policyPremium || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.PolicyOwner || "--"}</DataCell>
                                                                <DataCell className="text-center">{fetchBenef(record?.assetId)?.length !== 0 ?
                                                                    fetchBenef(record?.assetId)?.map((i: any) => {
                                                                        return (<><span>{i}</span><br /></>);
                                                                    }) : "No policy Beneficiaries"}</DataCell>
                                                                <DataCell className="text-right">Policy Term:{record?.policyTerm || "0"}yrs<br />Premium paying term: {record?.premiumTerm || "0"}yrs</DataCell>
                                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                    {record?.policyStatus}</DataCell>
                                                            </DataRow>
                                                        </>
                                                    )
                                                    // }
                                                })}</>}
                                        <DataCell colSpan={7}></DataCell>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                        <Box className="row border-bottom border-warning">
                            <Box className="col-lg-12 mb-2">
                                {/* @ts-ignore */}
                                <Text size="h4" css={{ mt: 12 }}>
                                    RM Comments
                                </Text>
                            </Box>
                        </Box>
                        <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                            <Box css={{ overflow: "auto" }}>
                                <Table>
                                    <ColumnParent
                                        className="border border-bottom"
                                        //@ts-ignore  borderRadius: 7 
                                        className="text-capitalize"
                                    >
                                        <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                            <Column className='col-lg-6 text-start ps-4'>Comment</Column>
                                            <Column>Policy Name</Column>
                                            <Column>Date</Column>
                                        </ColumnRow>
                                    </ColumnParent>
                                    <DataParent>
                                        <DataRow className="text-center m-0 p-0 !important">
                                            <DataCell colSpan={8}>No Comments Found</DataCell>
                                        </DataRow>
                                        <DataCell colSpan={7}></DataCell>
                                    </DataParent>
                                </Table>
                            </Box>
                        </Box>
                    </AccordionContent>
                </AccordionItem>
            </Accordion>
        </Box>
    )
}

export default Risk