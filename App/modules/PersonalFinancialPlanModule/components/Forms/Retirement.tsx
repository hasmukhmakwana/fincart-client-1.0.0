import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "@ui/Accordian/Accordian";
import DownArrow from "App/icons/DownArrow";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import ReactECharts from "echarts-for-react";
import useRetirementStore from "App/modules/PersonalFinancialPlanModule/store";
import { encryptData } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout"
import { retirementChartData } from "App/api/fullfinancialplan";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
// import useFullFinancialPlanStore from "App/modules/FullFinancialPlanModule/store";
// import { getSummaryDetails } from "App/api/fullfinancialplan";
let RetirementChartTabs = [
    {
        title: "Accumulation Phase",
        value: "Accumulation Phase",
    },
    {
        title: "Distribution Phase",
        value: "Distribution Phase",
    },
];
type SaveTypes = {
    fetchRetirementData: [];
    fetchRetirementTabData: [];
    fetchRetirementCal: any;
};

const Retirement = ({ fetchRetirementData, fetchRetirementTabData, fetchRetirementCal }: SaveTypes) => {


    const [accumulatedAge, setAccumulatedAge] = useState<any>([]);
    const [accumulatedMonthInv, setAccumulatedMonthInv] = useState<any>([]);
    const [accumulatedCorpus, setAccumulatedCorpus] = useState<any>([]);

    const [distributionAge, setDistributionAge] = useState<any>([]);
    const [distributionCorpus, setDistributionCorpus] = useState<any>([]);
    const [distributionExpense, setDistributionExpense] = useState<any>([]);

    const [currCorAcc, setCurrCorAcc] = useState<any>(0);


    const showRetirementChart = async () => {

        try {
            let AccmulatedChartAge: any = [];
            let AccmulatedMonthlyInv: any = [];
            let AccmulatedCorpus: any = [];

            fetchRetirementCal?.Accumulation_phase?.map((ele: any) => {
                AccmulatedChartAge.push(ele?.age);
                AccmulatedMonthlyInv.push(ele?.Annual_investment)
                AccmulatedCorpus.push(ele?.corpus);
            })
            // console.log(AccmulatedChartAge, "AccmulatedChartAge");
            setAccumulatedAge(AccmulatedChartAge);
            setAccumulatedMonthInv(AccmulatedMonthlyInv);
            setAccumulatedCorpus(AccmulatedCorpus);
            // console.log(accumulatedMonthInv);

            let DistributionChartAge: any = [];
            let DistributionCorpus: any = [];
            let DistributionExpense: any = [];
            fetchRetirementCal?.Distribution_phase?.map((ele: any) => {
                DistributionChartAge.push(ele?.age);
                DistributionCorpus.push(ele?.corpus_available)
                DistributionExpense.push(ele?.post_ret_annual_expense);
            })
            setDistributionAge(DistributionChartAge);
            setDistributionCorpus(DistributionCorpus);
            setDistributionExpense(DistributionExpense);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
    }
    useEffect(() => {
        showRetirementChart();
        return () => { }
    }, [fetchRetirementCal])

    useEffect(() => {
        let curSum: any = 0;
        fetchRetirementTabData?.map((record: any) => {
            if (record?.status !== "New") {
                curSum += parseInt(record?.assetAllocatedAmt);
            }
        })
        setCurrCorAcc(curSum);
        return () => { }
    }, [fetchRetirementTabData])


    const AccumulatedPhase = {
        xAxis: {
            // name of X Axis
            name: "X Axis",
            type: "category",
            data: accumulatedAge
        },
        yAxis: {
            // name of Y Axis
            name: "Y Axis",
            type: "value"
        },
        //To enable tooltips
        tooltip: {},
        legend: {},

        series: [{
            name: "Monthly Investment",
            data: accumulatedMonthInv,
            type: "line",
            smooth: true
        }, {
            name: "Corpus",
            data: accumulatedCorpus,
            type: "line",
            smooth: true
        }]
    };

    const DistributionPhase = {
        legend: {},
        tooltip: {},

        xAxis: { type: "category", data: distributionAge },
        yAxis: {},
        series: [{ type: "bar", data: distributionExpense, smooth: true, name: "Post Ret Annual Expense" },
        { type: "bar", data: distributionCorpus, smooth: true, name: "Corpus Available" }]

    };

    return (
        <>
            <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                <Accordion
                    type="single"
                    collapsible
                // onValueChange={(e) => { }}
                >
                    <AccordionItem value="Retirement" css={{ borderRadius: "8px" }}>
                        <AccordionTrigger className="row p-0">
                            <Box className="col">
                                <Box className="tabtitle text-black">
                                    {/* @ts-ignore */}
                                    <Text size="h4">{fetchRetirementData[0]?.goalname || "--"}</Text>
                                </Box>
                                <Box className="row tabtitlecontent">
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Retirement Age</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{fetchRetirementData[0]?.RetirementAge || "0"}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Life Expectancy</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{fetchRetirementData[0]?.LifeExpectency || "0"}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Year Left to Retirement</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{fetchRetirementData[0]?.YrsLefttoRetire || "0"}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Current Monthly Expenses</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.CurrMonthlyExpence || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                </Box>
                                <Box className="row tabtitlecontent">
                                    <Box className="col-lg-3 ">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Monthly Expenses at Retirement</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.RetireMonthlyExpence || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3 ">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Other Income</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.OtherIncome || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3 ">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Required Income  <br /> at Retirement</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.ReqIncome || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">ROI Pre-Retirement</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{fetchRetirementData[0]?.PreRetireRate || "0"}</Text>
                                    </Box>
                                </Box>
                                <Box className="row tabtitlecontent">
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">ROI Post-Retirement</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{fetchRetirementData[0]?.PostRetireRate || "0"}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Inflation Rate</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{fetchRetirementData[0]?.InflationRate || "0"}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Corpus Required at the Start of the Goal</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.CorpusReq || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Current Corpus Accumulated</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(currCorAcc || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                </Box>
                                <Box className="row tabtitlecontent">
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Net Deficit</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.deficit || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Actual Net Deficit</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.ActualNetDeficit || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Suggested SIP</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.SuggestedSIP || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Additional SIP Required</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.AddSip || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                </Box>
                                <Box className="row tabtitlecontent">
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Total SIP Required</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchRetirementData[0]?.TotReqSIP || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="col col-auto action">
                                <DownArrow />
                            </Box>
                        </AccordionTrigger>
                        <AccordionContent className="border p-3">
                            <Box className="row border-bottom border-warning ">
                                <Box className="col-lg-12 mb-2">
                                    {/* @ts-ignore */}
                                    <Text size="h4" >
                                        Existing Assets(Potential Funding)
                                    </Text>
                                </Box>
                            </Box>
                            {/* css={{ backgroundColor: "#b3daff" }} */}
                            <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                <Box css={{ overflow: "auto" }}>
                                    <Table>
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore
                                            className="text-capitalize"
                                        >
                                            <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                <Column>Asset Name</Column>
                                                <Column>Type Of Asset</Column>
                                                <Column>Txn Type/Frequency</Column>
                                                <Column>Current Value(Rs.)</Column>
                                                <Column>Additional Contribution(Rs.)</Column>
                                                <Column>Future Value(Rs.)</Column>
                                                <Column>Time Period(Yrs)</Column>
                                                <Column>Status</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        <DataParent>

                                            {fetchRetirementTabData?.filter((record: any) => record?.status !== "New").length === 0 || fetchRetirementTabData === null ?
                                                <DataRow>
                                                    <DataCell colSpan={8} className="text-center">
                                                        No Existing Assets Found
                                                    </DataCell>
                                                </DataRow> : <>
                                                    {/* {console.log(fetchRetirementTabData.filter((record:any)=>record?.status !== "New").length,"jakshdjk") } */}
                                                    {/* @ts-ignore */}
                                                    {fetchRetirementTabData?.filter((record: any) => record?.status !== "New").map((record: any) => {
                                                        return (
                                                            <DataRow>
                                                                <DataCell>{record?.SchemeName || "--"}</DataCell>
                                                                <DataCell className="text-center">{record?.objective || "--"}</DataCell>
                                                                <DataCell className="text-center">{record?.txnTypeName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.assetAllocatedAmt || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.txnAmount || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.futurecost || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.duration || "0"}</DataCell>
                                                                {record?.status === "Discontinue" ?
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                    :
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                }
                                                            </DataRow>
                                                        );
                                                    })}</>}
                                        </DataParent>
                                    </Table>
                                </Box>
                            </Box>
                            <Box className="row border-bottom border-warning">
                                <Box className="col-lg-12 mb-2">
                                    {/* @ts-ignore */}
                                    <Text size="h4" css={{ mt: 12 }}>
                                        New Recommendations
                                    </Text>
                                </Box>
                            </Box>
                            <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                <Box css={{ overflow: "auto" }}>
                                    <Table >
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore
                                            className="text-capitalize"
                                        >
                                            <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                <Column>Asset Name</Column>
                                                <Column>Type Of Asset</Column>
                                                <Column>Txn Type/Frequency</Column>
                                                <Column>Current Value(Rs.)</Column>
                                                <Column>Additional Contribution(Rs.)</Column>
                                                <Column>Future Value(Rs.)</Column>
                                                <Column>Time Period(Yrs)</Column>
                                                <Column>Status</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        <DataParent>
                                            {/* @ts-ignore */}
                                            {fetchRetirementTabData?.filter((record: any) => record?.status === "New").length === 0 || fetchRetirementTabData === null ?
                                                <DataRow>
                                                    <DataCell colSpan={8} className="text-center">
                                                        No New Recommendations Found
                                                    </DataCell>
                                                </DataRow> : <>
                                                    {/* @ts-ignore */}
                                                    {fetchRetirementTabData?.filter((record: any) => record?.status === "New").map((record: any) => {
                                                        return (
                                                            <DataRow>
                                                                <DataCell>{record?.SchemeName || "--"}</DataCell>
                                                                <DataCell className="text-center">{record?.objective || "--"}</DataCell>
                                                                <DataCell className="text-center">{record?.txnTypeName || "--"}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.assetAllocatedAmt || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.txnAmount || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.futurecost || "0").toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.duration || "0"}</DataCell>
                                                                {record?.status === "Discontinue" ?
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                    :
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                }
                                                            </DataRow>
                                                        );
                                                    })}</>}
                                        </DataParent>
                                    </Table>
                                </Box>
                            </Box>

                            <Box className="row border-bottom border-warning">
                                <Box className="col-lg-12 mb-2">
                                    {/* @ts-ignore */}
                                    <Text size="h4" css={{ mt: 12 }}>
                                        RM Comments
                                    </Text>
                                </Box>
                            </Box>
                            <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                <Box css={{ overflow: "auto" }}>
                                    <Table >
                                        <DataParent>
                                            <DataRow className="text-center m-0 p-0 !important">
                                                <DataCell colSpan={8}>No comments found</DataCell>
                                            </DataRow>
                                            <DataCell colSpan={8}></DataCell>
                                        </DataParent>
                                    </Table>
                                </Box>
                            </Box>

                        </AccordionContent>
                    </AccordionItem>
                </Accordion>
                <Box className="row mx-0">
                    <Box className="col-lg-12 mb-3">
                        {/* @ts-ignore */}
                        <Text size="h6">
                            *ROI - Return on Investment
                        </Text>
                    </Box>
                </Box>
                <Box className="row border-top border-warning mx-0">
                    <Box className="col-lg-12 mt-3">
                        {/* @ts-ignore */}
                        <Text size="h4" className="text-center" css={{ color: "var(--colors-blue1)" }}>
                            {/* @ts-ignore */}
                            Starting At Age <b> {(parseInt(fetchRetirementData?.[0]?.RetirementAge || "0") - parseInt(fetchRetirementData?.[0]?.YrsLefttoRetire || "0")).toString()}</b>, Retiring At Age <b>{fetchRetirementData?.[0]?.RetirementAge || "0"}</b> And Life Expectancy Until Age <b>{fetchRetirementData?.[0]?.LifeExpectency || "0"}</b>
                        </Text>
                        {/* @ts-ignore */}
                        <Text size="h6" className="text-center mt-3 me-2">
                            View By
                        </Text>
                    </Box>
                </Box>
                <StyledTabs defaultValue="Accumulation Phase">
                    <TabsList
                        aria-label="Manage your account"
                        className="justify-content-center mt-3"
                        css={{
                            borderBottom: "1px solid var(--colors-gray7)",
                            overflowX: "auto",
                        }}
                    >
                        {RetirementChartTabs.map((item, index) => {
                            return (
                                <TabsTrigger
                                    value={item.value}
                                    className="tabs me-3"
                                    key={item.value}
                                    css={{ border: "solid 1px var(--colors-blue1)", height: "25px" }}
                                >
                                    {/* @ts-ignore */}
                                    <Text size="h5">
                                        {item.title}
                                    </Text>
                                </TabsTrigger>
                            );
                        })}
                    </TabsList>
                    <TabsContent value="Accumulation Phase">
                        <Box>
                            <ReactECharts option={AccumulatedPhase} />
                        </Box>
                    </TabsContent>
                    <TabsContent value="Distribution Phase">
                        <Box>
                            <ReactECharts option={DistributionPhase} />
                        </Box>
                    </TabsContent>
                </StyledTabs>
            </Box>
        </>
    )
}

export default Retirement