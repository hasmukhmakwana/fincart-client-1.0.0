import React, { useEffect } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Pill from "@ui/Pill/Pill";
import DownArrow from "App/icons/DownArrow";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "@ui/Accordian/Accordian";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
type SaveTypes = {
    fetchEmergencyData: [];
    fetchEmergencyTabData: [];
};
const Emergency = ({ fetchEmergencyData, fetchEmergencyTabData }: SaveTypes) => {
    const getMonthDifference = (startDate: any, endDate: any) => {
        return (
            endDate.getMonth() - startDate.getMonth() + 12 * (endDate.getFullYear() - startDate.getFullYear())
        );
    }
    let CrrDate = new Date();
    //@ts-ignore
    let AllMonth = getMonthDifference(new Date(fetchEmergencyData[0]?.GoalStart_Date), new Date(fetchEmergencyData[0]?.GoalEnd_Date))
    //@ts-ignore
    let TillMonth = getMonthDifference(new Date(fetchEmergencyData[0]?.GoalStart_Date), new Date(CrrDate))
    // console.log(AllMonth, "AllMonth");
    // console.log(TillMonth, "TillMonth");
    let GetPercent = (Math.round((TillMonth / AllMonth) * 100), "%");
    // console.log(Math.round((TillMonth / AllMonth) * 100), "%");

    // // 👇️ 2
    // useEffect(() => {
    //     console.log("Diff", fetchEmergencyData);
    //     console.log("Diff", fetchEmergencyTabData);
    //     return () => {
    //     }
    // }, [fetchEmergencyData, fetchEmergencyTabData])

    return (
        <>
            <Box className="table-responsive">
                <Accordion
                    type="single"
                    // defaultValue={`${mutualFundPortfolioData[0]?.id}`}
                    collapsible
                    onValueChange={(e) => { }}
                >
                    <AccordionItem value="Emergency" css={{ borderRadius: "8px" }}>
                        <AccordionTrigger className="row p-0" >
                            <Box className="col">
                                <Box className="row tabtitle pe-0 justify-content-between">
                                    <Box className="col-auto text-black pt-1 pb-0">
                                        {/* @ts-ignore */}
                                        <Text>{fetchEmergencyData[0]?.goalname || "Emergency"}</Text>
                                    </Box>
                                    <Box className="col-auto my-0 py-0">
                                        <Box className="text-center my-0 py-0">
                                            {/* @ts-ignore */}
                                            <Text size="h6" className='my-0 py-0'>Priority</Text>
                                            {/* @ts-ignore */}
                                            <Text size="h5" className='my-0 py-0' color="black">{fetchEmergencyData[0]?.goalpriority || "0"}</Text>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className="row tabtitlecontent">
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Current Value</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{" "}{Number(fetchEmergencyData[0]?.currentcost || "0").toLocaleString("en-IN")}</Text>
                                    </Box>

                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Actual Net Deficit</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchEmergencyData[0]?.ActualNetDeficit || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Total SIP Required</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchEmergencyData[0]?.TotReqSIP || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Future Value</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchEmergencyData[0]?.futurecost || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                </Box>
                                <Box className="row tabtitlecontent">

                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Net Deficit</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchEmergencyData[0]?.deficit || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Suggested SIP</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchEmergencyData[0]?.SuggestedSIP || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Time to achieve {fetchEmergencyData[0]?.Duration || "0"} year</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Math.round((TillMonth / AllMonth) * 100) || "0.0"}%</Text>
                                    </Box>
                                    <Box className="col-lg-3">
                                        {/* @ts-ignore */}
                                        <Text size="h6">Additional SIP Required</Text>
                                        {/* @ts-ignore */}
                                        <Text size="h5" color="black">{Number(fetchEmergencyData[0]?.AddSip || "0").toLocaleString("en-IN")}</Text>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="col col-auto action">
                                <DownArrow />
                            </Box>
                        </AccordionTrigger>
                        <AccordionContent className="border p-3">
                            <Box className="row border-bottom border-warning ">
                                <Box className="col-lg-12 mb-2">
                                    {/* @ts-ignore */}
                                    <Text size="h4" >
                                        Existing Assets(Potential Funding)
                                    </Text>
                                </Box>
                            </Box>
                            {/* css={{ backgroundColor: "#b3daff" }} */}
                            <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                <Box css={{ overflow: "auto" }}>
                                    <Table>
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore
                                            className="text-capitalize"
                                        >
                                            <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center", borderRadius: "8px" }}>
                                                <Column>Asset Name</Column>
                                                <Column>Type Of Asset</Column>
                                                <Column>Txn Type/Frequency</Column>
                                                <Column>Current Value(Rs.)</Column>
                                                <Column>Additional Contribution(Rs.)</Column>
                                                <Column>Future Value(Rs.)</Column>
                                                <Column>Time Period(Yrs)</Column>
                                                <Column>Status</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        <DataParent>
    
                                            {fetchEmergencyTabData?.filter((record: any) => record?.status !== "New").length === 0 ?
                                                <DataRow>
                                                    <DataCell colSpan={8} className="text-center">
                                                        No Existing Assets(Potential Funding) Found
                                                    </DataCell>
                                                </DataRow> : <>
                                                    {/* @ts-ignore */}
                                                    {fetchEmergencyTabData?.filter((record: any) => record?.status !== "New").map((record: any) => {
                                                        // if (record?.status !== "New") {
                                                        return (
                                                            <DataRow>
                                                                <DataCell>{record?.SchemeName}</DataCell>
                                                                <DataCell className="text-center">{record?.objective}</DataCell>
                                                                <DataCell className="text-center">{record?.txnTypeName}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.assetAllocatedAmt).toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.txnAmount).toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.futurecost).toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.duration}</DataCell>
                                                                {record?.status === "Discontinue" ?
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                    :
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                }
                                                            </DataRow>
                                                        );
                                                        // }
                                                    })}</>}
                                        </DataParent>
                                    </Table>
                                </Box>
                            </Box>
                            <Box className="row border-bottom border-warning">
                                <Box className="col-lg-12 mt-1 mb-2">
                                    {/* @ts-ignore */}
                                    <Text size="h4" css={{ mt: 12 }}>
                                        New Recommendations
                                    </Text>
                                </Box>
                            </Box>
                            <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                <Box css={{ overflow: "auto" }}>
                                    <Table >
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore
                                            className="text-capitalize"
                                        >
                                            <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                <Column>Asset Name</Column>
                                                <Column>Type Of Asset</Column>
                                                <Column>Txn Type/Frequency</Column>
                                                <Column>Current Value(Rs.)</Column>
                                                <Column>Additional Contribution(Rs.)</Column>
                                                <Column>Future Value(Rs.)</Column>
                                                <Column>Time Period(Yrs)</Column>
                                                <Column>Status</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        <DataParent>
                                            {/* @ts-ignore */}
                                            {fetchEmergencyTabData?.filter((record: any) => record?.status === "New").length === 0 ?
                                                <DataRow>
                                                    <DataCell colSpan={8} className="text-center">
                                                        No New Recommendations Found
                                                    </DataCell>
                                                </DataRow> : <>
                                                    {/* @ts-ignore */}
                                                    {fetchEmergencyTabData?.filter((record: any) => record?.status === "New").map((record: any) => {
                                                        // if (record?.status == "New") {
                                                        return (
                                                            <DataRow>
                                                                <DataCell>{record?.SchemeName}</DataCell>
                                                                <DataCell className="text-center">{record?.objective}</DataCell>
                                                                <DataCell className="text-center">{record?.txnTypeName}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.assetAllocatedAmt).toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.txnAmount).toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{Number(record?.futurecost).toLocaleString("en-IN")}</DataCell>
                                                                <DataCell className="text-center">{record?.duration}</DataCell>
                                                                {record?.status === "Discontinue" ?
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                    :
                                                                    <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                        {record?.status}</DataCell>
                                                                }
                                                            </DataRow>
                                                        );
                                                        // }
                                                    })}</>}
                                        </DataParent>
                                    </Table>
                                </Box>
                            </Box>
                            <Box className="row border-bottom border-warning">
                                <Box className="col-lg-12 mt-1 mb-2">
                                    {/* @ts-ignore */}
                                    <Text size="h4" css={{ mt: 12 }}>
                                        RM Comments
                                    </Text>
                                </Box>
                            </Box>
                            <Box className="table-responsive mt-2" css={{ borderRadius: "8px" }}>
                                <Box css={{ overflow: "auto" }}>
                                    <Table>
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore  borderRadius: 7 
                                            className="text-capitalize"
                                        >
                                            <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                <Column className='col-lg-6 text-start ps-4'>Comment</Column>
                                                <Column>Policy Name</Column>
                                                <Column>Date</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        <DataParent>
                                            <DataRow>
                                                <DataCell colSpan={3} className="text-center" >
                                                    No Comments Found
                                                </DataCell>
                                            </DataRow>
                                            {/* <DataCell colSpan={7}></DataCell> */}
                                        </DataParent>
                                    </Table>
                                </Box>
                            </Box>
                        </AccordionContent>
                    </AccordionItem>
                </Accordion>
            </Box>
        </>
    )
}
export default Emergency