import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "@ui/Accordian/Accordian";
import Emergency from "./Forms/Emergency";
import Risk from "./Forms/Risk";
import Retirement from "./Forms/Retirement";
import Goals from "./Forms/Goals";
import Estate from "./Forms/Estate";


let FinancialPlanTabs = [
    {
        title: "Emergency",
        value: "Emergency",
    },
    {
        title: "Risk",
        value: "Risk",
    },
    {
        title: "Goals",
        value: "Goals",
    },
    {
        title: "Retirement",
        value: "Retirement",
    },
    {
        title: "Estate",
        value: "Estate",
    },
];

const FinancialPlanPage = () => {
    return (
        <>
            <Box className="container">
                <Box className="row">
                    <Box className="col-md-12 col-sm-12 col-lg-12">
                        <Box className="row text-end">
                            <Box className="col-lg-12 col-md-12 col-sm-12 mb-1">
                                <Button color="yellow" size="lg" onClick={() => { }}>
                                    View Full Financial Plan
                                </Button>
                                <Button color="yellow" size="lg" onClick={() => { }}>
                                    Execute My Plan
                                </Button>
                            </Box>
                        </Box>
                        <Card>
                            <Box className="row">
                                <Box className="col-lg-12">
                                    {/* @ts-ignore */}
                                    <Text weight="bold" size="h3" className="text-center mb-1" css={{ color: "var(--colors-blue1)" }}>Summary</Text>
                                </Box>
                            </Box>
                            <StyledTabs defaultValue="Emergency">
                                <TabsList
                                    aria-label="Manage your account"
                                    className="justify-content-center mt-3"
                                    css={{
                                        borderBottom: "1px solid var(--colors-gray7)",
                                        overflowX: "auto",
                                    }}
                                >
                                    {FinancialPlanTabs.map((item, index) => {
                                        return (
                                            <TabsTrigger
                                                value={item.value}
                                                className="tabs me-3"
                                                key={item.value}
                                                css={{ border: "solid 1px var(--colors-blue1)", height: "25px" }}
                                            >
                                                {/* @ts-ignore */}
                                                <Text weight="bold" size="h5">
                                                    {item.title}
                                                </Text>
                                            </TabsTrigger>
                                        );
                                    })}
                                </TabsList>
                                <TabsContent value="Emergency" className="mt-2"><Emergency fetchEmergencyData={[]} fetchEmergencyTabData={[]} /></TabsContent>
                                <TabsContent value="Risk" className="mt-2"><Risk fetchRiskData={[]} fetchRiskTabData={[]} fetchPolicyBene={[]}/></TabsContent>
                                <TabsContent value="Goals" className="mt-2"><Goals fetchGoalData={[]} fetchGoalTabData={[]} setFilteredData={() => { }} /></TabsContent>
                                <TabsContent value="Retirement" className="mt-2"><Retirement fetchRetirementData={[]} fetchRetirementTabData={[]} fetchRetirementCal={[]}/></TabsContent>
                                <TabsContent value="Estate" className="mt-2"><Estate /></TabsContent>
                            </StyledTabs>
                        </Card>
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default FinancialPlanPage