import React from 'react'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout, { toastAlert } from 'App/shared/components/Layout'
import FinancialPlanPage from './components/FinancialPlanPage';
const PersonalFinancialPlan = () => {
    return (
        <>
            <Layout>
                <PageHeader title="Personal Financial Plan" rightContent={<></>} />
                <FinancialPlanPage />
            </Layout>
        </>
    )
}

export default PersonalFinancialPlan