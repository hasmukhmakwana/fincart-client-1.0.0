import create from "zustand";
interface StoreTypes {
    loading: boolean;
    setLoader: (payload: boolean) => void;
    accumulationChartData: any;
    setAccumulationChartData: (payload: any) => void;
    distributionChartData: any;
    setDistributionChartData: (payload: any) => void;
}
const useRetirementStore = create<StoreTypes>((set) => ({
    accumulationChartData: [],
    distributionChartData:[],
    loading: false,

    setLoader: (payload) =>
        set((state) => ({
            ...state,
            loading: payload,
        })),

    setAccumulationChartData: (payload) =>
        set((state) => ({
            ...state,
            accumulationChartData: payload || [],
        })),

    setDistributionChartData: (payload) =>
        set((state) => ({
            ...state,
            distributionChartData: payload || [],
        })),
}));

export default useRetirementStore;