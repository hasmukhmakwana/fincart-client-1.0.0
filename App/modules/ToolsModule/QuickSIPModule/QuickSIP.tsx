import React, { useState, useEffect } from "react";
import Layout, { toastAlert } from "App/shared/components/Layout";
import QuickSIPPage from "./components/QuickSIPPage";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import { encryptData, getUser } from "App/utils/helpers";
import useQuickSipStore from "./store";
import {
  fetchQuickSips,
} from "App/api/tools"
const QuickSIP = () => {
  const user: any = getUser();
  const {
    setLoader,
    setQuickSipList,
    setMinDate,
    setMaxDate,
    setLocalUser
  } = useQuickSipStore();
  const fetchQuicks = async () => {
    // return;
    try {
      setLoader(true);
      const enc: any = encryptData(user?.basicid, true);
      let result: any = await fetchQuickSips(enc);
      console.log(result?.data);

      setQuickSipList(result?.data);
      setLoader(false);
      console.log(result?.data);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  };
  useEffect(() => {
    (async () => {
      await fetchQuicks()
    })();
    const date = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
    const MinDate = (date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDate()).padStart(2, '0'));
    const MaxDate = (date.getFullYear() + 99) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDate()).padStart(2, '0'));
    // console.log(date);
    setMinDate(MinDate);
    setMaxDate(MaxDate);
    setLocalUser(user?.basicid);
    return () => {
    }
  }, [])

  return (
    <Layout>
      <PageHeader title="My Quick SIP's" rightContent={<></>} />
      <QuickSIPPage fetchData={fetchQuicks} />
    </Layout>
  );
};

export default QuickSIP;
