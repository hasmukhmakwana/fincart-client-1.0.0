import create from "zustand";

import { QuickSipListTypes, finCalculatetype } from "./QuickSipTypes"

interface StoreTypes {
    loading : boolean;
    quickSipList : QuickSipListTypes[];
    finCalculate : finCalculatetype[];
    minDate: string;
    maxDate: string;
    localUser: string;
    setLoader: (payload : boolean) => void;
    setQuickSipList : (payload : QuickSipListTypes[]) => void;
    setFincalculate : (payload : finCalculatetype[]) => void;
    setMinDate:(payload: string ) => void;
    setMaxDate:(payload: string ) => void;
    setLocalUser: (payload: string) => void;
}

// const today = new Date();

const useQuickSipStore = create<StoreTypes>((set) => ({
    loading:false,
    minDate: "",
    maxDate: "",
    localUser:"",
    quickSipList: [],
    finCalculate: [],

    setLoader: (payload) =>
    set((state) => ({
      ...state,
      loading: payload,
    })),

    setMinDate: (payload) =>
    set((state) => ({
      ...state,
      minDate: payload,
    })),

    setMaxDate: (payload) =>
    set((state) => ({
      ...state,
      maxDate: payload,
    })),

    setLocalUser: (payload) =>
    set((state) => ({
      ...state,
      localUser: payload,
    })),

    setQuickSipList: (payload) =>
    set((state) => ({
      ...state,
      quickSipList: payload || [],
    })),

    setFincalculate: (payload) =>
    set((state) => ({
      ...state,
      finCalculate: payload || [],
    })),
}))

export default useQuickSipStore;