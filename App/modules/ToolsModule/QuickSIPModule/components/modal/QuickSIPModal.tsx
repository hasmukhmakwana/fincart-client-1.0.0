import React, { useState } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import * as Yup from "yup";
import { Formik } from "formik";
import styles from "../../../Tools.module.scss";
import create from "zustand";
import {
  getFinCalculatorData
} from "App/api/tools"
import {
  createGoal
} from "App/api/goalPlan"

import { toastAlert } from "App/shared/components/Layout";
import useQuickSipStore from "../../store";
import { encryptData } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";

const validationSchema = Yup.object().shape({
  GoalName: Yup.string()
    .required("Name is required")
    .trim()
    .matches(/^\s*[\S]/gms, "Please enter your Goal Name"),

  Amount: Yup.number().required("Amount is required").positive(),

  InvestDate: Yup.string()
    .required("Date is required")
    .trim(),
});

export type VerifyNameTypes = {
  GoalName: string;
  Amount: string;
  InvestDate: string;
};
interface StoreTypes {
  verifyName: VerifyNameTypes;
}

const useRegistrationStore = create<StoreTypes>((set) => ({
  //* initial state
  verifyName: {
    GoalName: "",
    Amount: "",
    InvestDate: "",
  }
}))

type SaveTypes = {
  setQuickModalOpen: (values: boolean) => void;
  reload: () => void;
};
const QuickSIPModal = ({ setQuickModalOpen, reload }: SaveTypes) => {
  const { verifyName, loading } = useRegistrationStore((state: any) => state);
  const [show, setShow] = useState(false);
  const [loaderApi, setLoaderApi] = useState(false);
  const [createSt, setCreateSt] = useState(false);

  const {
    minDate,
    maxDate,
    localUser,
    finCalculate,
    setFincalculate
  } = useQuickSipStore();

  const calculateAndCreate = async (data: any) => {
    data = data || {};
    console.log("createInvestment: ", data);
    // return;
    // console.log(createSt);

    //Created Goal API
    try {
      if (createSt) {
        setLoaderApi(true);
        const obj: any =
        {
          usergoalId: null,
          basicid: localUser,
          Relation: null,
          childName: null,
          age: "",
          gender: null,
          annualIncome: null,
          goalCode: "FG14",
          otherGoalName: data?.GoalName,
          typeCode: "FG220",
          monthlyAmount: null,
          presentValue: data?.Amount,
          risk: "M",
          goal_StartDate: null,
          goal_EndDate: data?.InvestDate,
          //@ts-ignore
          inflationRate: finCalculate?.Inflation,
          //@ts-ignore
          ror: finCalculate?.ROR,
          //@ts-ignore
          PMT: finCalculate?.PMT,
          downPaymentRate: null,
          trnx_Type: "S",//
          //@ts-ignore
          getAmount: finCalculate?.getSip,
          retirementAge: "",
          //@ts-ignore
          investAmount: finCalculate?.investSip,//
          people: null
        };

        const enc: any = encryptData(obj);
        console.log(obj);
        console.log(enc);
        let result: any = await createGoal(enc);
        console.log(result);
        reload();
        setQuickModalOpen(false);
        toastAlert("success", "New Quick Sip Successfully Created");
        setLoaderApi(false);
        setCreateSt(false);

        //Created Goal API end
      } else {
        //Fincalc api start
        setLoaderApi(true);
        const obj: any =
        {
          type: "ISAMT",
          currentAmount: data?.Amount,
          startDate: null,
          endDate: data?.InvestDate,
          goalCode: "FG14",
          otherGoalName: data?.GoalName,
          childName: null,
          travelPeople: null,
          locationCode: "FG220",
          budgetType: null,
          businessStartupCost: null,
          monthlyexpence: null
        };
        const enc: any = encryptData(obj);
        let result: any = await getFinCalculatorData(enc);
        //console.log(obj)
        //console.log(enc);
        // console.log(result?.data);
        setFincalculate(result?.data);
        setShow(true);
        setLoaderApi(false);
      }
      //Fincalc api end
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
    //created Goal Api end
  }

  return (
    <>
      <Formik
        initialValues={verifyName}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          calculateAndCreate(values);
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount
        }) => (
          <Box className="col-md-12 col-sm-12 col-lg-12 p-0">
            <Box>
              <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                {/* @ts-ignore */}
                <Text weight="bold" css={{ color: "var(--colors-blue1)" }}> Quick SIP</Text>
              </Box>
              <Box className="modal-body modal-body-scroll p-3">
                <Box>
                  <Text>What name would you give to your Quick SIP?</Text>
                  <Input
                    label="Enter Goal Name"
                    name="GoalName"
                    placeholder="Enter Goal Name"
                    error={submitCount ? errors.GoalName : null}
                    onChange={handleChange}
                    value={values?.GoalName}
                  />
                </Box>
                <Box>
                  <Text>How much money you can invest monthly?</Text>
                  <Input
                    type="text"
                    label="Enter Amount"
                    name="Amount"
                    placeholder="Enter Amount"
                    error={submitCount ? errors.Amount : null}
                    onChange={handleChange}
                    value={values?.Amount}
                  />
                </Box>
                <Box>
                  <Text>How long you want to invest?</Text>
                  <Input
                    type="date"
                    label="Enter Date"
                    name="InvestDate"
                    min={minDate}
                    max={maxDate}
                    error={submitCount ? errors.InvestDate : null}
                    onChange={handleChange}
                    value={values?.InvestDate}
                  />
                </Box>
                <Box className="row justify-content-end mx-0">
                  {/* <Box className="col-auto"></Box> */}
                  <Button
                    className="mb-2 p-2"
                    color="yellowGroup"
                    size="md"
                    onClick={() => { handleSubmit() }}
                    loading={loading}
                  //onClick={() => setShow(true)}
                  >
                    <Text
                      // @ts-ignore
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                      className={`${styles.button}`}
                    >
                      Calculate
                    </Text>
                  </Button>
                </Box>
                {show ? <>
                  {loaderApi ? (
                    <Loader />
                  ) : (<>
                    <Box className="row">
                      <Box className="col-lg-12" css={{ backgroundColor: "#b3daff", color: "#0861b6" }}>
                        {/* @ts-ignore */}
                        <Text weight="bold" size="h5" className={`text-center m-1 font`} >After {finCalculate?.time} year your required amount will be &#x20B9;{" "}{finCalculate?.getSip}</Text>
                      </Box>
                    </Box>
                    <Box className="row justify-content-end mx-0">
                      {/* <Box className="col-auto"></Box> */}
                      <Button
                        className="mt-2 p-2"
                        color="yellowGroup"
                        size="md"
                      >
                        <Text
                          //@ts-ignore
                          weight="extrabold"
                          size="h4"
                          //@ts-ignore
                          color="gray8"
                          className={styles.button}
                          onClick={() => {
                            setCreateSt(true);
                            handleSubmit()
                          }}
                          loading={loading}
                        >
                          Save
                        </Text>
                      </Button>
                    </Box>
                  </>)
                  }
                </> : ""}
              </Box>
            </Box>
          </Box>)}
      </Formik>
    </>
  )
}

export default QuickSIPModal;
