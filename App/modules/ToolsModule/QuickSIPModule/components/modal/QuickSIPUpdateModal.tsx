import React, { useState, useEffect } from "react";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import styles from "../../../Tools.module.scss";
import create from "zustand";
import * as Yup from "yup";
import { Formik } from "formik";
import Loader from "App/shared/components/Loader";
import {
  updateGoal,
  fetchGoalData
} from "App/api/goalPlan";
import {
  getFinCalculatorData
} from "App/api/tools"
import { encryptData } from "App/utils/helpers";
import { toastAlert } from "App/shared/components/Layout";
import useQuickSipStore from "../../store";

const validationSchema = Yup.object().shape({
  GoalName: Yup.string()
    .required("Name is required")
    .trim()
    .matches(/^\s*[\S]/gms, "Please enter your Goal Name"),

  Amount: Yup.number().required("Amount is required").positive(),

  InvestDate: Yup.string()
    .required("Date is required")
    .trim(),
});

export type VerifyNameTypes = {
  GoalName: string;
  Amount: string;
  InvestDate: string;
};
interface StoreTypes {
  verifyName: VerifyNameTypes;
}

type SaveTypes = {
  setEditModalOpen: (values: boolean) => void;
  IdPass: (values: string) => void;
  reload: () => void;
};

const QuickSIPUpdateModal = ({ setEditModalOpen, IdPass, reload }: SaveTypes) => {
  const [loadFetch, setLoadFetch] = useState(false);
  const [goalResult, setGoalResult] = useState<any>({});
  const [loaderApi, setLoaderApi] = useState(false);
  const [updateSt, setUpdateSt] = useState(false);
  const [show, setShow] = useState(false);
  const {
    minDate,
    maxDate,
    localUser,
    finCalculate,
    setFincalculate
  } = useQuickSipStore();

  const useRegistrationStore = create<StoreTypes>((set) => ({
    //* initial state
    verifyName: {
      //@ts-ignore
      GoalName: goalResult?.otherGoalName,
      //@ts-ignore
      Amount: goalResult?.investAmount,
      InvestDate: goalResult?.goal_EndDate,
    }
  }));
  const { verifyName, loading } = useRegistrationStore((state: any) => state);

  const goalData = async () => {
    try {
      setLoadFetch(true);
      //@ts-ignore
      const enc: any = encryptData(parseInt(IdPass));
      console.log(enc);
      let result: any = await fetchGoalData(enc);
      console.log(result);
      setGoalResult(result?.data)
      setLoadFetch(false);
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }
  const calculateAndUpdate = async (data: any) => {

    //Created Goal API
    try {
      if (updateSt) {
        setLoaderApi(true);
        const obj: any =
        {
          usergoalId: IdPass,
          basicid: localUser,
          Relation: null,
          childName: null,
          age: "",
          gender: null,
          annualIncome: null,
          goalCode: "FG14",
          otherGoalName: data?.GoalName,
          typeCode: "FG220",
          monthlyAmount: null,
          presentValue: data?.Amount,
          risk: "M",
          goal_StartDate: goalResult?.goal_StartDate,
          goal_EndDate: data?.InvestDate,
          //@ts-ignore
          inflationRate: finCalculate?.Inflation,
          //@ts-ignore
          ror: finCalculate?.ROR,
          //@ts-ignore
          PMT: finCalculate?.PMT,
          downPaymentRate: null,
          trnx_Type: "S",//
          //@ts-ignore
          getAmount: finCalculate?.getSip,
          retirementAge: "",
          //@ts-ignore
          investAmount: finCalculate?.investSip,//
          people: null
        };
        const enc: any = encryptData(obj);
        console.log(obj);
        console.log(enc);
        let result: any = await updateGoal(enc);
        console.log(result);
        reload();
        setEditModalOpen(false);
        toastAlert("success", "Quick Sip Successfully Updated");
        setLoaderApi(false);
        setUpdateSt(false);

        //Created Goal API end
      } else {
        //Fincalc api start
        setLoaderApi(true);
        const obj: any =
        {
          type: "ISAMT",
          currentAmount: data?.Amount,
          startDate: goalResult?.goal_StartDate,
          endDate: data?.InvestDate,
          goalCode: "FG14",
          otherGoalName: data?.GoalName,
          childName: null,
          travelPeople: null,
          locationCode: "FG220",
          budgetType: null,
          businessStartupCost: null,
          monthlyexpence: null
        };
        const enc: any = encryptData(obj);
        let result: any = await getFinCalculatorData(enc);
        console.log(obj)
        console.log(enc);
        console.log(result?.data);
        setFincalculate(result?.data);
        setShow(true);
        setLoaderApi(false);
      }
      //Fincalc api end
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
    //created Goal Api end
  }

  // const updateSip = (data: any) => {
  //   //if (createSt) {
  //   try {
  //     console.log("update");
  //     console.log(goalResult);

  //     // setLoader(true);
  //     // const obj: any =
  //     // {
  //     //   userGoalId: null,//
  //     //   basicid: "",//
  //     //   Relation: null,
  //     //   childName: null,
  //     //   age: null,
  //     //   gender: null,
  //     //   annualIncome: null,
  //     //   goalCode: "FG14",
  //     //   otherGoalName: data?.GoalName,//
  //     //   typeCode: "FG220",
  //     //   monthlyAmount: null,
  //     //   presentValue: data?.Amount,//
  //     //   risk: "M",
  //     //   goal_StartDate: null,
  //     //   goal_EndDate: data?.InvestDate,//
  //     //   //@ts-ignore
  //     //   inflationRate: finCalculate?.Inflation,//
  //     //   //@ts-ignore
  //     //   ror: finCalculate?.ROR,//
  //     //   //@ts-ignore
  //     //   PMT: finCalculate?.PMT,//
  //     //   downPaymentRate: null,//
  //     //   trnx_Type: "S",//
  //     //   //@ts-ignore
  //     //   getAmount: finCalculate?.getSip,//
  //     //   retirementAge: null,
  //     //   //@ts-ignore
  //     //   investAmount: finCalculate?.investSip,//
  //     //   people: null
  //     // };

  //     // const enc: any = encryptData(obj);
  //     // console.log(obj);
  //     // console.log(enc);
  //     // let result: any = await updateGoal(enc);
  //     // console.log(result);
  //     //  toastAlert("success", "New Quick Sip Successfully Created");
  //     // setLoader(false);
  //     // setQuickModalOpen(false);

  //     //Created Goal API end
  //   } catch (error: any) {
  //     console.log(error);
  //     toastAlert("error", error);
  //   }
  // }

  useEffect(() => {
    console.log(IdPass);
    goalData();

    return () => {
    }
  }, [])

  return (
    <>
      <Formik
        initialValues={verifyName}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          // clickOnSubmit(values);
          //     updateSip(values)
          calculateAndUpdate(values);
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount
        }) => (
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
              {/* @ts-ignore */}
              <Box className="col-auto text-light">
                <Text>
                  {" "}
                  Update Quick SIP
                </Text></Box>
            </Box>
            <Box className="modal-body modal-body-scroll p-3">
              {loadFetch ?
                <Loader /> : <>
                  <Box>
                    <Text>What name would you give to your Quick SIP?</Text>
                    <Input
                      label="Edit Goal Name"
                      name="GoalName"
                      // placeholder="Enter Name"
                      error={submitCount ? errors.GoalName : null}
                      onChange={handleChange}
                      value={values?.GoalName}
                    />
                  </Box>
                  <Box>
                    <Text>How much money you can invest monthly?</Text>
                    <Input
                      label="Edit Amount"
                      name="Amount"
                      // placeholder="Enter Amount"
                      //value="1500"
                      error={submitCount ? errors.Amount : null}
                      onChange={handleChange}
                      value={values?.Amount}
                    />
                  </Box>
                  <Box>
                    <Text>How long you want to invest?</Text>
                    <Input
                      type="date"
                      label="Enter Date"
                      name="InvestDate"
                      min={minDate}
                      max={maxDate}
                      error={submitCount ? errors.InvestDate : null}
                      onChange={handleChange}
                      value={values?.InvestDate}
                    />
                  </Box>
                  <Box className="row justify-content-end mx-0">
                    {/* <Box className="col-auto"></Box> */}
                    <Button
                      className="mb-2 p-2"
                      color="yellowGroup"
                      size="md"
                      onClick={() => {
                        handleSubmit();
                      }}
                      loading={loading}
                    >
                      <Text
                        // @ts-ignore
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                        className={`${styles.button}`}
                      >
                        Calculate
                      </Text>
                    </Button>
                  </Box>
                  {show ? <>
                    {loaderApi ? (
                      <Loader />
                    ) : (<>
                      <Box className="row">
                        <Box className="col-lg-12" css={{ backgroundColor: "#b3daff", color: "#0861b6" }}>
                          {/* @ts-ignore */}
                          <Text size="h5" className={`text-center m-1 font`} >After {finCalculate?.time} year your required amount will be &#x20B9;{" "}{finCalculate?.getSip}</Text>
                        </Box>
                      </Box>
                      <Box className="row justify-content-end mx-0">
                        {/* <Box className="col-auto"></Box> */}
                        <Button
                          className="mt-2 p-2"
                          color="yellowGroup"
                          size="md"
                        >
                          <Text
                            //@ts-ignore
                            size="h6"
                            //@ts-ignore
                            color="gray8"
                            className={styles.button}
                            onClick={() => {
                              setUpdateSt(true);
                              handleSubmit()
                            }}
                            loading={loading}
                          >
                            Save
                          </Text>
                        </Button>
                      </Box>
                    </>)
                    }
                  </> : ""}
                </>}
            </Box>
          </Box>
        )}
      </Formik>
    </>
  );
};

export default QuickSIPUpdateModal;
