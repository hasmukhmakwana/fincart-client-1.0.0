import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import Basket2fill from "App/icons/Basket2fill";
import PencilFill from "App/icons/PencilFill";
import DeleteIcon from "App/icons/DeleteIcon";
import Add from "App/icons/Add";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import styles from "../../Tools.module.scss";
import useQuickSipStore from "../store";
import Loader from "App/shared/components/Loader";
import {
  QuickSIPModal,
  QuickSIPUpdateModal,
  QuickSIPDelete
} from "./modal";

import { getUser } from "App/utils/helpers";
import useRegistrationStore from "App/modules/RegistrationModule/store";
import { toastAlert } from "App/shared/components/Layout";
import router from "next/router";

type SaveTypes = {
  // setEditModalOpen: (values: boolean) => void;
  fetchData: () => void;
};

const QuickSIPPage = ({ fetchData }: SaveTypes) => {

  const user: any = getUser();
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [openQuickSIP, setOpenQuickSIP] = useState(false);
  const [goalId, setGoalId] = useState("");
  const { setBasicID, setRegiType } = useRegistrationStore();
  const {
    loading,
    quickSipList
  } = useQuickSipStore();
  useEffect(() => {
    return () => {
    }
  }, [])


  return (
    <>
      <Box className="row p-4" css={{ backgroundColor: "white", borderRadius: "8px" }}>
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="row text-end">
            <Box className="col-lg-12 col-md-12 col-sm-12 mb-1">
              <Button color="yellow" size="md" onClick={setOpenQuickSIP}>
                Initiate Quick SIP <Add></Add>
              </Button>
            </Box>
          </Box>
          <Box className="row">
            {loading ? (
              <Loader />
            ) : (
              <>
                {quickSipList.map((record, index) => {
                  return (
                    <Box className="col-lg-4 col-md-6 col-12">
                      <Card css={{ p: 15, border: "solid 1px #b3daff" }}>
                        <Box className="row">
                          <Box className="col-4 col-lg-3">
                            <Box className={styles.imgbg} display="inlineBlock">
                              <img
                                width={50}
                                height={50}
                                src={record.goalImg}
                                alt="Quick SIP"
                              />
                            </Box>
                          </Box>
                          <Box className="col-8 col-lg-9 text-left">
                            {/* @ts-ignore  */}
                            <Text>
                              {record.goalName}
                            </Text>
                            <Text
                              // @ts-ignore 

                              size="h6" color="primary">
                              Achieved {record.goalPerc}%
                            </Text>
                            <Text size="h6" className={`badge mt-2 rounded-pill me-1 ${styles.titlepill}`} >Duration: {record.duration} yr(s)</Text>
                          </Box>
                        </Box>

                        <Box className="row my-3">
                          <Box className="col-6  text-center">
                            <Text color="primary">
                              SIP
                            </Text>
                            <Text
                              //@ts-ignore 
                              style={{ color: "#202020" }}>&#x20B9;{" "} {record.investAmount}
                            </Text>
                          </Box>
                          <Box className="col-6  text-center">
                            <Text color="primary">
                              Expected
                            </Text>
                            <Text
                              //@ts-ignore 
                              style={{ color: "#202020" }}>&#x20B9;{" "}{record.getAmount}
                            </Text>
                          </Box>
                        </Box>

                        <Box className="row justify-content-center mt-3 pe-3">
                          {user?.cafstatus === "success" ? <>
                            <Button
                              className={`col-auto mb-2`}
                              color="yellowGroup"
                              size="md"
                              onClick={() => {
                                //add items to localStorage
                                //set goalCode
                                console.log(record);

                                localStorage.setItem("userGoalId", record?.userGoalId)
                                //set section
                                localStorage.setItem("section", "QUICK_SIP")
                                //set amount
                                localStorage.setItem("amount", record?.investAmount)
                                //set duration
                                localStorage.setItem("duration", record?.duration)
                                router.push("/RecommendedSchemes")
                              }}
                            >
                              {/* @ts-ignore  */}
                              <GroupBox align="center">
                                <Box>
                                  {""}
                                  <Basket2fill />
                                </Box>
                                <Box>
                                  <Text
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                    className={styles.button}
                                  >
                                    Invest
                                  </Text>
                                </Box>
                              </GroupBox>
                            </Button>
                          </> : <>
                            <Button
                              color="yellowGroup"
                              size="md"
                              className={`col-auto mb-2`}
                              onClick={() => {
                                setRegiType("PENDING");
                                setBasicID(user?.basicid);
                                router.push("/Registration");
                              }}
                            >
                              <GroupBox align="center">
                                <Box>
                                  <Add />
                                </Box>
                                <Box>
                                  <Text
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                    className={styles.button}>
                                    Complete Reg.
                                  </Text>
                                </Box>
                              </GroupBox>
                            </Button>
                          </>}
                          <Button
                            className="col-auto mb-2 "
                            color="yellowGroup"
                            size="md"
                            onClick={() => {
                              setGoalId(record.userGoalId);
                              setOpenEdit(true);
                            }}
                          >
                            {/* @ts-ignore  */}
                            <GroupBox align="center">
                              <Box>
                                {" "}
                                <PencilFill></PencilFill>
                              </Box>

                              <Box>
                                <Text
                                  // @ts-ignore 
                                  size="h6"
                                  //@ts-ignore
                                  color="gray8"
                                  className={styles.button}
                                >
                                  Edit
                                </Text>
                              </Box>
                            </GroupBox>
                          </Button>
                          <Button
                            className="col-auto mb-2 "
                            color="yellowGroup"
                            size="md"
                            onClick={() => {
                              setGoalId(record.userGoalId);
                              setOpenDelete(true);
                            }}
                          >
                            {/* @ts-ignore */}
                            <GroupBox align="center">
                              <Box>
                                {" "}
                                <DeleteIcon></DeleteIcon>
                              </Box>
                              <Box>
                                <Text
                                  //@ts-ignore
                                  size="h6"
                                  //@ts-ignore
                                  color="gray8"
                                  className={styles.button}
                                >
                                  Delete
                                </Text>
                              </Box>
                            </GroupBox>
                          </Button>
                        </Box>
                      </Card>
                    </Box>

                  )
                })}
              </>
            )}
          </Box>
        </Box>
      </Box>
      {/* Added new Modal*/}
      <DialogModal
        open={openQuickSIP}
        setOpen={setOpenQuickSIP}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        <QuickSIPModal setQuickModalOpen={setOpenQuickSIP} reload={fetchData} />
      </DialogModal>
      {/* Update MODAL */}
      <DialogModal
        open={openEdit}
        setOpen={setOpenEdit}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        {/* @ts-ignore */}
        <QuickSIPUpdateModal setEditModalOpen={setOpenEdit} IdPass={goalId} reload={fetchData} />
      </DialogModal>
      {/* Delete Modal */}
      <DialogModal
        open={openDelete}
        setOpen={setOpenDelete}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        {/* @ts-ignore */}
        <QuickSIPDelete setDeleteModalOpen={setOpenDelete} IdPass={goalId} reload={fetchData} />

      </DialogModal>
    </>
  );
};

export default QuickSIPPage;
