import Layout from 'App/shared/components/Layout'
import React from 'react'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import TaxSavingInvest from './components/TaxSavingInvest'
const TaxOnetimePage = () => {
    return (
        <>
            <Layout>
                <PageHeader title="Tax Saving Invest" rightContent={<></>} />
                <TaxSavingInvest />
            </Layout>
        </>
    )
}

export default TaxOnetimePage