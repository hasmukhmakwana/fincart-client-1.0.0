import React, { useState } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import SelectMenu from "@ui/Select/Select";
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import styles from "../../Tools.module.scss";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import CheckCircleFill from "App/icons/CheckCircleFill";

const TaxSavingOnetime = () => {
    const [openInvestor, setOpenInvestor] = useState();
    const [openInvestorAcc, setOpenInvestorAcc] = useState();
    const [openCart, setOpenCart] = useState(false);
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-2">
                    {/* @ts-ignore */}
                    <Text weight="bold" css={{ color: "var(--colors-blue1)" }}>Onetime</Text>
                </Box>
                <Box className="modal-body modal-body-scroll">
                    <Box className="row">
                        <Box className="col-lg-12 border-bottom mx-0">
                            {/* @ts-ignore */}
                            <Text size="h5" className="text-center" css={{ color: "var(--colors-blue1)" }}>Aditya Birla SL - Tax Relief 96 Fund ELSS Reg (G)</Text>
                        </Box>
                    </Box>
                    <Box className="row">
                        <Box className="col-lg-12">
                            <SelectMenu
                                value={openInvestor}
                                items={[
                                    { id: "1", name: "D MANIKANDAN" },
                                ]}
                                label={"Investor"}
                                bindValue={"id"}
                                bindName={"name"}
                                onChange={(e) =>
                                    setOpenInvestor(e?.id || "")
                                } />
                        </Box>
                    </Box>
                    <Box className="row">
                        <Box className="col-lg-12">
                            <SelectMenu
                                value={openInvestorAcc}
                                items={[
                                    { id: "1", name: "D MANIKANDAN-Joint1-NA-Joint2-NA" },
                                ]}
                                label={"Investor"}
                                bindValue={"id"}
                                bindName={"name"}
                                onChange={(e) =>
                                    setOpenInvestorAcc(e?.id || "")
                                } />
                        </Box>
                    </Box>
                    <Box className="row">
                        <Box className="col-lg-12">
                            <Input
                                label="Amount (min 5000)"
                                name="Amount"
                                placeholder="Enter Amount"
                                value="5000"
                            />
                        </Box>
                    </Box>
                    <Box className="row justify-content-end mx-1">
                        {/* <Box className="col-auto"></Box> */}
                        <Button
                            className="mt-2 px-3 py-1"
                            color="yellowGroup"
                            size="sm"
                            onClick={() => {
                                setOpenCart(true);
                            }}
                        >
                            <Text
                                // @ts-ignore
                                weight="extrabold"
                                size="h6"
                                //@ts-ignore
                                color="gray8"
                                className={styles.button}
                            >
                                Add To Cart
                            </Text>
                        </Button>
                    </Box>
                </Box>
            </Box>

            {/* Cart MODAL */}
            <DialogModal
                open={openCart}
                setOpen={setOpenCart}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "35%" },
                }}
            >
                {/* @ts-ignore */}
                <Box className="col-md-12 col-sm-12 col-lg-12">
                    <Box className="modal-body modal-body-scroll">
                        <Box className="row justify-content-center">
                            <Box className="col-lg-12 text-center" css={{ py: 50 }}>
                                <CheckCircleFill color="orange" size="2rem" />
                                {/* @ts-ignore */}
                                <Text size="h5" className="text-center mt-2" css={{ color: "var(--colors-blue1)" }}>Scheme Added to Cart!!</Text>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </DialogModal>
        </>
    )
}

export default TaxSavingOnetime