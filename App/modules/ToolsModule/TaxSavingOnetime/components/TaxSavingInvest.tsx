import React, { useState } from 'react'
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import StarFill from "App/icons/StarFill";
import styles from "../../Tools.module.scss";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import TaxSavingOnetime from './TaxSavingOnetime';

const TaxSavingInvest = () => {
    const [openOnetime, setOpenOnetime] = useState(false);
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-2">
                    {/* @ts-ignore */}
                    <Text weight="bold" css={{ color: "var(--colors-blue1)" }}>Recommended Scheme for Tax Saving</Text>
                </Box>
                <Box className="row">
                    <Box className="col-lg-6">
                        <Card
                            css={{
                                borderRadius: "8px 8px 8px 8px",
                                border: "1px solid #82afd9",
                            }}
                        >
                            {/* @ts-ignore */}
                            <Text size="h5" weight="bold">Aditya Birla SL - Tax Relief 96 Fund ELSS Reg (G)</Text>
                            {/* @ts-ignore */}
                            <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                            {/* @ts-ignore */}
                            <Text size="h6" className="mt-1" css={{ color: "var(--colors-blue1)" }}>Equity - ELSS</Text>
                            <Box className="row justify-content-center">
                                <Box className="col-lg-6 col-md-5 col-sm-5">
                                    <Box className="row mx-1 px-0 mx-auto mb-2" css={{ backgroundColor: "#b3daff", color: "#0861b6", borderRadius: "8px 8px 8px 8px" }}>
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-center">Scheme Size</Text>
                                        </Box>
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-center text-black" >&#x20B9;10,000</Text>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="row justify-content-center">
                                <Box className="col-lg-9 col-md-12 col-sm-12 border-bottom">
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-center" css={{ color: "#0861b6" }}>Annual Returns</Text>
                                </Box>
                                <Box className="col-lg-10">
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-center">1 Year-6.5%&nbsp;<span className="border-end"></span>&nbsp;3 Year-10.7%&nbsp;<span className="border-end"></span>&nbsp;5 Year-6.5%</Text>
                                </Box>
                            </Box>
                            <Box className="row justify-content-end mx-1">
                                {/* <Box className="col-auto"></Box> */}
                                <Button
                                    className="mt-2 px-3 py-1"
                                    color="yellowGroup"
                                    size="sm"
                                >
                                    <Text
                                        // @ts-ignore
                                        weight="extrabold"
                                        size="h5"
                                        //@ts-ignore
                                        color="gray8"
                                        className={styles.button}
                                    >
                                        Onetime
                                    </Text>
                                </Button>
                            </Box>
                        </Card>
                    </Box>
                    <Box className="col-lg-6">
                        <Card
                            css={{
                                borderRadius: "8px 8px 8px 8px",
                                border: "1px solid #82afd9",
                            }}
                        >
                            {/* @ts-ignore */}
                            <Text size="h5" weight="bold">IDFC - Tax Advantage Reg (G)</Text>
                            {/* @ts-ignore */}
                            <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                            {/* @ts-ignore */}
                            <Text size="h6" className="mt-1" css={{ color: "var(--colors-blue1)" }}>Equity - ELSS</Text>

                            <Box className="row justify-content-center">
                                <Box className="col-lg-6 col-md-5 col-sm-5">
                                    <Box className="row mx-1 px-0 mx-auto mb-2" css={{ backgroundColor: "#b3daff", color: "#0861b6", borderRadius: "8px 8px 8px 8px" }}>
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-center">Scheme Size</Text>
                                        </Box>
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-center text-black" >&#x20B9;10,000</Text>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="row justify-content-center">
                                <Box className="col-lg-9 col-md-12 col-sm-12 border-bottom">
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-center" css={{ color: "#0861b6" }}>Annual Returns</Text>
                                </Box>
                                <Box className="col-lg-10">
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-center">1 Year-1.2%&nbsp;<span className="border-end"></span>&nbsp;3 Year-24.4%&nbsp;<span className="border-end"></span>&nbsp;5 Year-12.7%</Text>
                                </Box>
                            </Box>
                            <Box className="row justify-content-end mx-1">
                                {/* <Box className="col-auto"></Box> */}
                                <Button
                                    className="mt-2 px-3 py-1"
                                    color="yellowGroup"
                                    size="sm"
                                    onClick={(e: any) => { setOpenOnetime(true) }}
                                >
                                    <Text
                                        // @ts-ignore
                                        weight="extrabold"
                                        size="h5"
                                        //@ts-ignore
                                        color="gray8"
                                        className={styles.button}
                                    >
                                        Onetime
                                    </Text>
                                </Button>
                            </Box>
                        </Card>
                    </Box>
                </Box>
                <Box className="row">
                    <Box className="col-lg-6">
                        <Card
                            css={{
                                borderRadius: "8px 8px 8px 8px",
                                border: "1px solid #82afd9",
                            }}
                        >
                            {/* @ts-ignore */}
                            <Text size="h5" weight="bold">Axis - Long Term Equity Fund (G)</Text>
                            {/* @ts-ignore */}
                            <StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />&nbsp;&nbsp;<StarFill color="orange" />
                            {/* @ts-ignore */}
                            <Text size="h6" className="mt-1" css={{ color: "var(--colors-blue1)" }}>Equity - ELSS</Text>

                            <Box className="row justify-content-center">
                                <Box className="col-lg-6 col-md-5 col-sm-5">
                                    <Box className="row mx-1 px-0 mx-auto mb-2" css={{ backgroundColor: "#b3daff", color: "#0861b6", borderRadius: "8px 8px 8px 8px" }}>
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-center">Scheme Size</Text>
                                        </Box>
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-center text-black" >&#x20B9;10,000</Text>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                            <Box className="row justify-content-center">
                                <Box className="col-lg-9 col-md-12 col-sm-12 border-bottom">
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-center" css={{ color: "#0861b6" }}>Annual Returns</Text>
                                </Box>
                                <Box className="col-lg-10">
                                    {/* @ts-ignore */}
                                    <Text size="h5" className="text-center">1 Year-12.5%&nbsp;<span className="border-end"></span>&nbsp;3 Year-13.2%&nbsp;<span className="border-end"></span>&nbsp;5 Year-11.3%</Text>
                                </Box>
                            </Box>
                            <Box className="row justify-content-end mx-1">
                                {/* <Box className="col-auto"></Box> */}
                                <Button
                                    className="mt-2 px-3 py-1"
                                    color="yellowGroup"
                                    size="sm"
                                    onClick={() => {
                                        setOpenOnetime(true);
                                    }}
                                >
                                    <Text
                                        // @ts-ignore
                                        weight="extrabold"
                                        size="h5"
                                        //@ts-ignore
                                        color="gray8"
                                        className={styles.button}
                                    >
                                        Onetime
                                    </Text>
                                </Button>
                            </Box>
                        </Card>
                    </Box>
                </Box>
            </Box>

            {/* Onetime MODAL */}
            <DialogModal
                open={openOnetime}
                setOpen={setOpenOnetime}
                css={{
                    "@bp0": { width: "60%" },
                    "@bp1": { width: "35%" },
                }}
            >
                {/* @ts-ignore */}
                <TaxSavingOnetime />
            </DialogModal>
        </>
    )
}

export default TaxSavingInvest