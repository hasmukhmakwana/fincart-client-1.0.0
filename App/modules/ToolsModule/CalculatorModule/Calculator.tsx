import React, { useState } from "react";
import Layout from 'App/shared/components/Layout'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import { Button } from "@ui/Button/Button";
import DialogModal from "@ui/ModalDialog/ModalDialog";
// import style from "../Tools.module.scss";
import CalculatorPage from "./components/CalculatorPage";
const Calculator = () => {

    return (
        <Layout>
            <PageHeader title="Calculator" rightContent={<></>}/>
            <CalculatorPage/>
        </Layout>
    )
}

export default (Calculator);