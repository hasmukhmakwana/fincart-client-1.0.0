import React, { useEffect, useRef, useState } from "react";
import Layout, { toastAlert } from "App/shared/components/Layout";
import Box from "App/ui/Box/Box";
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
import ReactECharts from "echarts-for-react";
import Input from "@ui/Input";
import Radio from "@ui/Radio/Radio";
import { RadioGroup } from "@ui/Radio/";
import { GroupBox } from "@ui/Group/Group.styles";
// @ts-ignore
import { TabsContent } from "@radix-ui/react-tabs";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import "../CalculatorPage.module.scss";
import {
  getSIPCalculatorData,
  getLumpsumCalculatorData,
  getRetirementCalculatorData
} from "App/api/calculator";
import { encryptData, getUser } from "App/utils/helpers";

import styles from "../CalculatorPage.module.scss"

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@ui/Accordian/Accordian";
import DownArrow from "App/icons/DownArrow";
import Badge from "@ui/Badge/Badge";
import { Button } from "@ui/Button/Button";
import { useRouter } from "next/router";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';
import Info from "App/icons/Info";
let recentTabs = [
  {
    title: "SIP",
    value: "SIP",
  },
  {
    title: "Lumpsum",
    value: "Lumpsum",
  },
  {
    title: "Retirement",
    value: "Retirement",
  },
  // {
  //   title: "SWP",
  //   value: "SWP",
  // },
  // {
  //   title: "MF Return",
  //   value: "MFReturn",
  // },
  // {
  //   title: "Cost of Return",
  //   value: "CostOfReturn",
  // },
];

let RetirementChartTabs = [
  {
    title: "Accumulation Phase",
    value: "Accumulation Phase",
  },
  {
    title: "Distribution Phase",
    value: "Distribution Phase",
  },
];

const CalculatorPage = () => {
  const router = useRouter();
  const { setContactUsVal } = useContactUsStore();
  const [rangevalSIP1, setRangevalSIP1] = useState(500);
  const [rangevalSIP2, setRangevalSIP2] = useState(1);

  const [rangevalLumpsum1, setRangevalLumpsum1] = useState(1000);
  const [rangevalLumpsum2, setRangevalLumpsum2] = useState(1);

  const [rangevalRetire1, setRangevalRetire1] = useState(25);
  const [rangevalRetire2, setRangevalRetire2] = useState(45);
  const [rangevalRetire3, setRangevalRetire3] = useState(10000);
  const [rangevalRetire4, setRangevalRetire4] = useState<any>(rangevalRetire2 + 5);

  const [showStartAge, setShowStartAge] = useState(0);
  const [showRetireAge, setShowRetireAge] = useState(0);
  const [showExpectAge, setShowExpectAge] = useState(0);
  const [showMonthly, setShowMonthly] = useState(0);

  const [accumulatedAge, setAccumulatedAge] = useState<any>([]);
  const [accumulatedMonthInv, setAccumulatedMonthInv] = useState<any>([]);
  const [accumulatedCorpus, setAccumulatedCorpus] = useState<any>([]);

  const [distributionAge, setDistributionAge] = useState<any>([]);
  const [distributionCorpus, setDistributionCorpus] = useState<any>([]);
  const [distributionExpense, setDistributionExpense] = useState<any>([]);

  // const [rangevalSWP1, setRangevalSWP1] = useState(1);
  // const [rangevalSWP2, setRangevalSWP2] = useState(10000);
  // const [rangevalSWP3, setRangevalSWP3] = useState(500);
  // const [rangevalSWP4, setRangevalSWP4] = useState(1);

  // const [rangevalMF1, setRangevalMF1] = useState(1);
  // const [rangevalMF2, setRangevalMF2] = useState(500);
  // const [rangevalMF3, setRangevalMF3] = useState(1);

  const [riskSip, setRiskSip] = useState("");
  const [riskLumpsum, setRiskLumpsum] = useState("");
  const [riskRetire, setRiskRetire] = useState("");
  const [LumpsumData, setLumpsumData] = useState<any>({});
  const [SIPData, setSIPData] = useState<any>({});

  const [chartLumpsum, setChartLumpsum] = useState([0, 0]);
  const [chartSIP, setChartSIP] = useState([0, 0]);
  const [chartRetirement, setChartRetirement] = useState([0, 0]);

  const SIPInvestmentGrowth = {
    title: {
      // text: 'Sales last Week by Products',
      x: "left",
    },
    tooltip: {
      trigger: "item",
      formatter: (params: any) => {
        return `
            <div style="text-align: center;">
            <h6>${params.seriesName}</h6>
            <h5>${params.value}</h5>
            </div>
            `;
      },
    },
    color: ["#B3DAFF", "#005CB3"],
    legend: {
      type: "plain",
      orient: "horizontal",
      // right: '0',
      top: 40,
      itemWidth: 14,
      icon: "roundRect",
      data: ["Invested Amount", "Return Amount"],
    },
    grid: {
      // left: "4%",
      bottom: "10%",
      // right: "0",
    },
    xAxis: {
      data: ["Invested Amount", "Return Amount"],
      axisLine: {
        show: false,
      },
    },
    yAxis: {
      axisLine: {
        show: false,
      },
      splitLine: {
        show: false,
      },
    },
    series: [
      {
        name: "",
        type: "bar",
        smooth: "true",
        symbol: "none",
        data: chartSIP,
        lineStyle: {
          width: 5,
          color: "#FCDE9C",
          shadowBlur: 1.5,
          shadowColor: "rgba(0, 0, 0, 0.12)",
          shadowOffsetY: 4,
        },
      },
    ],
  };
  const LumpsumInvestmentGrowth = {
    title: {
      // text: 'Sales last Week by Products',
      x: "left",
    },
    tooltip: {
      trigger: "item",
      formatter: (params: any) => {
        return `
            <div style="text-align: center;">
            <h6>${params.seriesName}</h6>
            <h5>${params.value}</h5>
            </div>
            `;
      },
    },
    color: ["#B3DAFF", "#005CB3"],
    legend: {
      type: "plain",
      orient: "horizontal",
      // right: '0',
      top: 40,
      itemWidth: 14,
      icon: "roundRect",
      data: ["Invested Amount", "Return Amount"],
    },
    grid: {
      // left: "4%",
      bottom: "10%",
      // right: "0",
    },
    xAxis: {
      data: ["Invested Amount", "Return Amount"],
      axisLine: {
        show: false,
      },
    },
    yAxis: {
      axisLine: {
        show: false,
      },
      splitLine: {
        show: false,
      },
    },
    series: [
      {
        name: "",
        type: "bar",
        smooth: "true",
        symbol: "none",
        data: chartLumpsum,
        lineStyle: {
          width: 5,
          color: "#FCDE9C",
          shadowBlur: 1.5,
          shadowColor: "rgba(0, 0, 0, 0.12)",
          shadowOffsetY: 4,
        },
      },
    ],
  };
  const InvestmentGrowth = {
    title: {
      x: "left",
    },
    tooltip: {
      trigger: "item",
      formatter: (params: any) => {
        return `
            <div style="text-align: center;">
            <h6>${params.seriesName}</h6>
            <h5>${params.value}</h5>
            </div>
            `;
      },
    },
    color: ["#B3DAFF", "#005CB3"],
    legend: {
      type: "plain",
      orient: "horizontal",
      // right: '0',
      top: 40,
      itemWidth: 14,
      icon: "roundRect",
      data: ["Invested Amount", "Return Amount"],
    },
    grid: {
      // left: "4%",
      bottom: "10%",
      // right: "0",
    },
    xAxis: {
      data: ["Invested Amount", "Return Amount"],
      axisLine: {
        show: false,
      },
    },
    yAxis: {
      axisLine: {
        show: false,
      },
      splitLine: {
        show: false,
      },
    },
    series: [
      {
        name: "",
        type: "bar",
        smooth: "true",
        symbol: "none",
        data: chartRetirement,
        lineStyle: {
          width: 5,
          color: "#FCDE9C",
          shadowBlur: 1.5,
          shadowColor: "rgba(0, 0, 0, 0.12)",
          shadowOffsetY: 4,
        },
      },
    ],
  };
  const handleSIPCalculator = async () => {
    try {
      const obj: any = {
        amount: rangevalSIP1,
        risk: riskSip,
        time: rangevalSIP2,
      };
      const enc: any = encryptData(obj);

      const result: any = await getSIPCalculatorData(enc);
      setSIPData(result?.data || {});
      let arr = [];

      arr.push(result.data.IV_SIP);

      arr.push(result.data.FV_SIP);
      setChartSIP(arr);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  const handleLumpsumCalculator = async () => {
    try {
      const obj: any = {
        amount: rangevalLumpsum1,
        risk: riskLumpsum,
        time: rangevalLumpsum2,
      };
      const enc: any = encryptData(obj);
      const result: any = await getLumpsumCalculatorData(enc);
      setLumpsumData(result?.data || {});
      let arr = [];

      arr.push(result.data.IV_LUMPSUM);

      arr.push(result.data.FV_LUMPSUM);
      setChartLumpsum(arr);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  const handleRetirementCalculator = async () => {
    try {
      const obj: any = {
        current_age: rangevalRetire1,
        retirement_age: rangevalRetire2,
        monthly_expense: rangevalRetire3,
        risk: riskRetire,
        life_expectancy: rangevalRetire4
      }
      const enc: any = encryptData(obj);
      const result: any = await getRetirementCalculatorData(enc);
      setShowStartAge(rangevalRetire1);
      setShowRetireAge(rangevalRetire2);
      setShowExpectAge(rangevalRetire4);
      setShowMonthly(result?.data?.Accumulation_phase?.[0]?.monthly_investment || 0)
      let AccmulatedChartAge: any = [];
      let AccmulatedMonthlyInv: any = [];
      let AccmulatedCorpus: any = [];

      result?.data?.Accumulation_phase?.map((ele: any) => {
        AccmulatedChartAge.push(ele?.age);
        AccmulatedMonthlyInv.push(ele?.monthly_investment)
        AccmulatedCorpus.push(ele?.corpus);
      })
      setAccumulatedAge(AccmulatedChartAge);
      setAccumulatedMonthInv(AccmulatedMonthlyInv);
      setAccumulatedCorpus(AccmulatedCorpus);

      let DistributionChartAge: any = [];
      let DistributionCorpus: any = [];
      let DistributionExpense: any = [];
      result?.data?.Distribution_phase?.map((ele: any) => {
        DistributionChartAge.push(ele?.age);
        DistributionCorpus.push(ele?.corpus_available)
        DistributionExpense.push(ele?.post_ret_annual_expense);
      })
      setDistributionAge(DistributionChartAge);
      setDistributionCorpus(DistributionCorpus);
      setDistributionExpense(DistributionExpense);

    } catch (error) {
      console.log(error);
      toastAlert("error", error);
    }
  };

  const AccumulatedPhase = {
    xAxis: {
      // name of X Axis
      name: "X Axis",
      type: "category",
      data: accumulatedAge
    },
    yAxis: {
      // name of Y Axis
      name: "Y Axis",
      type: "value"
    },
    //To enable tooltips
    tooltip: {},
    legend: {},

    series: [{
      name: "Monthly Investment",
      data: accumulatedMonthInv,
      type: "line",
      smooth: true
    }, {
      name: "Corpus",
      data: accumulatedCorpus,
      type: "line",
      smooth: true
    }]
  };

  const DistributionPhase = {
    legend: {},
    tooltip: {},

    xAxis: { type: "category", data: distributionAge },
    yAxis: {},
    series: [{ type: "bar", data: distributionExpense, smooth: true, name: "Post Ret Annual Expense" },
    { type: "bar", data: distributionCorpus, smooth: true, name: "Corpus Available" }]

  };
  return (
    <>
      <Card css={{ p: 25 }}>
        <StyledTabs defaultValue="SIP">
          <TabsList
            aria-label="Manage your account"
            className="justify-content-center"
            css={{
              mb: 20,
              borderBottom: "1px solid var(--colors-gray7)",
              overflowX: "auto",
            }}
          >
            {recentTabs.map((item, index) => {
              return (
                <TabsTrigger
                  value={item.value}
                  className="tabs me-2"
                  key={item.value}
                  css={{ border: "solid 1px var(--colors-blue1)" }}
                >
                  <Text size="h5">
                    {item.title}
                  </Text>
                </TabsTrigger>
              );
            })}
          </TabsList>
          <TabsContent value="SIP">
            <Box className="row">
              <Box className="col-12 col-lg-6">
                <Box className="row justify-content-between">
                  <Box className="col-12 col-md-7 col-lg-7">
                    <Text size="h5">
                      Amount I want to invest monthly
                    </Text>
                  </Box>
                  <Box className="col-12 col-md-3 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box className="row">

                      <Box className="col-1 p-0 m-0 ">
                        {rangevalSIP1 >= 100 && rangevalSIP1 <= 500000 ? <></> : <>
                          <Box >
                            {/* @ts-ignore */}
                            <Text className="my-1 px-1" title="Amount Range is 100 to 5 lakhs">{" "}<Info size="0.9rem" color="red" css={{ color: "red" }} />{" "}</Text>
                          </Box>
                        </>}
                      </Box>

                      <Box className="col-10 ">
                        <Input
                          type="text"
                          size="h3"
                          value={rangevalSIP1}
                          className={`text-center ${rangevalSIP1 >= 100 && rangevalSIP1 <= 500000 ? styles.inputBlue : styles.inputRed}`}
                          note={"₹(Min.:₹100)"}
                          min={100}
                          max={500000}
                          onChange={(e: any) => { setRangevalSIP1(e.target.value.replace(/\D/g, '')) }}
                        />
                      </Box>

                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range slider w-100"
                          defaultValue="500"
                          value={rangevalSIP1}
                          min="100"
                          step={"100"}
                          max="500000"
                          onChange={(event: any) =>
                            setRangevalSIP1(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 justify-content-between">
                  <Box className="col-12 col-lg-7">
                    <Text size="h5">
                      I want to invest till
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 mx-lg-3 mx-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center ms-4"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalSIP2} Years
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="1"
                          min="1"
                          max="35"
                          onChange={(event: any) =>
                            setRangevalSIP2(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 mb-3 mb-lg-0">
                  <Box className="col-12">
                    <Text
                      size="h4"
                      // @ts-ignore
                      css={{ color: "var(--colors-blue1)" }}
                    >
                      My Risk Profile is
                    </Text>
                    <GroupBox>
                      <RadioGroup
                        // defaultValue={values.gender}
                        className="inlineRadio"
                        name="RiskProfileSip"
                        // required
                        value={riskSip}
                        onValueChange={(e: any) => {
                          setRiskSip(e);
                        }}

                      >
                        <Radio value="conservative" label="Conservative" id="conservativeSip" /><br />
                        <Radio value="moderately conservative" label="Moderately Conservative" id="moderately conservativeSip" /><br />
                        <Radio value="balanced" label="Balanced" id="balancedSip" /><br />
                        <Radio value="aggressive" label="Aggressive" id="aggressiveSip" /><br />
                        <Radio value="very aggressive" label="Very Aggressive" id="very aggressiveSip" /><br />
                      </RadioGroup>
                    </GroupBox>
                  </Box>
                </Box>
                <Box className="col-12 text-end">
                  <Button
                    className="p-2"
                    color="yellow"
                    onClick={() => {
                      if (rangevalSIP1 >= 100 && rangevalSIP1 <= 500000) {
                        handleSIPCalculator()
                      } else {
                        toastAlert("warn", "Amount Range is in between 100 to 5 lakhs")
                      }
                    }}
                  >
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Calculate
                    </Text>
                  </Button>
                </Box>
              </Box>

              <Box className="col-12 col-lg-6">
                <Box className="row d-flex justify-content-center">
                  <Box className="col-12 col-lg-5">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#FFFFFF",
                        border: "solid #B3DAFF",
                        borderWidth: "1px 1px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Invested Amount
                        </Text>
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          // @ts-ignore
                          css={{ color: "#005CB3" }}
                        >
                          {Number(SIPData?.IV_SIP || 0).toLocaleString("en-IN") || "0.0"}
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                  <Box className="col-12 col-lg-5 mt-2 mt-lg-0">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        border: "solid #005CB3",
                        borderWidth: "1px 1px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Return Amount
                        </Text>
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          // @ts-ignore
                          css={{ color: "#005CB3" }}
                        >
                          {Number(SIPData?.FV_SIP || 0).toLocaleString("en-IN") || "0.0"}
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box className="row mt-3">
                  <Box>
                    <ReactECharts
                      option={SIPInvestmentGrowth}
                      style={{ height: 300 }}
                      opts={{ renderer: "svg" }}
                    />
                  </Box>

                  <Box className="text-center">
                    <Text size="h6">
                      Note: Expected rate of return {SIPData?.ROR || "0.0"}%
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box
                className="pt-4"
                css={{ borderBottom: "solid 1px #B3DAFF" }}
              ></Box>
              <Box className="row mt-4">
                <Box className="col-12">
                  <Text size="h4" className="text-center mb-1">
                    That's good choice!
                  </Text>
                </Box>
              </Box>
              <Box className="row justify-content-center mt-3 text-center">
                <Box>
                  <Button className="p-2" color="yellow"
                    onClick={() => {
                      setContactUsVal({
                        SubjectId: "9",
                        Description: ""
                      }
                      );
                      router.push('/Support/ContactUs')
                    }}
                  >
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Request a Callback
                    </Text>
                  </Button>

                  <Button className="p-2" color="yellow" onClick={() => {
                    router.push("/Goals/MyGoals");
                  }}>
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Start Investing Now
                    </Text>
                  </Button>
                </Box>
              </Box>
            </Box>
          </TabsContent>

          <TabsContent value="Lumpsum">
            <Box className="row">
              <Box className="col-12 col-lg-6">
                <Box className="row justify-content-between">
                  <Box className="col-12 col-md-7 col-lg-7">
                    <Text size="h5">
                      Amount I want to invest
                    </Text>
                  </Box>
                  <Box className="col-12 col-md-3 col-lg-3">
                    <Box className="row " >
                      <Box className="col-1 p-0 m-0 ">
                        {rangevalLumpsum1 >= 1000 && rangevalLumpsum1 <= 1000000 ? <></> : <>
                          <Box >
                            {/* @ts-ignore */}
                            <Text className="my-1 px-1" title="Amount Range is 1000 to 10 lakhs">{" "}<Info size="0.9rem" color="red" css={{ color: "red" }} />{" "}</Text>
                          </Box>
                        </>}
                      </Box>
                      <Box className="col-10">
                        <Input
                          type="text"
                          size="h3"
                          value={rangevalLumpsum1}
                          className={`text-center ${rangevalLumpsum1 >= 1000 && rangevalLumpsum1 <= 1000000 ? styles.inputBlue : styles.inputRed}`}
                          note={"₹(Min.: ₹1000)"}
                          min={1000}
                          max={1000000}
                          onChange={(e: any) => { setRangevalLumpsum1(e.target.value.replace(/\D/g, '')) }}
                        />

                      </Box>
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range slider w-100"
                          defaultValue="5000"
                          value={rangevalLumpsum1}
                          min="1000"
                          step={"500"}
                          max="1000000"
                          onChange={(event: any) =>
                            setRangevalLumpsum1(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 justify-content-between">
                  <Box className="col-12 col-lg-7">
                    <Text size="h5">
                      I want to keep ot invest
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 mx-lg-3 mx-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center ms-4"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalLumpsum2} Years
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="1"
                          min="1"
                          max="35"
                          onChange={(event: any) =>
                            setRangevalLumpsum2(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 mb-3 mb-lg-0">
                  <Box className="col-12">
                    {/* @ts-ignore */}
                    <Text size="h4" css={{ color: "var(--colors-blue1)" }}  >
                      My Risk Profile is
                    </Text>
                    <GroupBox>
                      <RadioGroup
                        // defaultValue={values.gender}
                        className="inlineRadio"
                        name="RiskProfileLump"
                        // required
                        value={riskLumpsum}
                        onValueChange={(e: any) => {
                          setRiskLumpsum(e);
                        }}
                      // css={{ flexDirection: "column !important" }}
                      >
                        <Radio value="conservative" label="Conservative" id="conservativeLump" /><br />
                        <Radio value="moderately conservative" label="Moderately Conservative" id="moderately conservativeLump" /><br />
                        <Radio value="balanced" label="Balanced" id="balancedLump" /><br />
                        <Radio value="aggressive" label="Aggressive" id="aggressiveLump" /><br />
                        <Radio value="very aggressive" label="Very Aggressive" id="very aggressiveLump" /><br />
                      </RadioGroup>
                    </GroupBox>

                  </Box>
                  <Box className="col-12 text-end">
                    <Button
                      className="p-2"
                      color="yellow"
                      onClick={() => {
                        if (rangevalLumpsum1 >= 1000 && rangevalLumpsum1 <= 1000000) {
                          handleLumpsumCalculator()
                        } else {
                          toastAlert("warn", "Amount Range is in between 1000 to 10 lakhs")
                        }
                      }}
                    >
                      <Text
                        size="h6"
                        //@ts-ignore
                        color="gray8"
                      >
                        Calculate
                      </Text>
                    </Button>
                  </Box>
                </Box>
              </Box>
              <Box className="col-12 col-lg-6">
                <Box className="row d-flex justify-content-center">
                  <Box className="col-12 col-lg-5">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#FFFFFF",
                        border: "solid #B3DAFF",
                        borderWidth: "1px 1px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Invested Amount
                        </Text>
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          // @ts-ignore
                          css={{ color: "#005CB3" }}
                        >
                          {Number(LumpsumData?.IV_LUMPSUM || "0.0").toLocaleString("en-IN")}
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                  <Box className="col-12 col-lg-5 mt-2 mt-lg-0">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        border: "solid #005CB3",
                        borderWidth: "1px 1px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Return Amount
                        </Text>
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          // @ts-ignore
                          css={{ color: "#005CB3" }}
                        >
                          {Number(LumpsumData?.FV_LUMPSUM || "0.0").toLocaleString("en-IN") || "0.0"}
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box className="row mt-3">
                  <Box>
                    <ReactECharts
                      option={LumpsumInvestmentGrowth}
                      style={{ height: 300 }}
                      opts={{ renderer: "svg" }}
                    />
                  </Box>

                  <Box className="text-center">
                    <Text size="h6">
                      Note: Expected rate of return {LumpsumData?.ROR || "0.0"}%
                    </Text>
                  </Box>
                </Box>
              </Box>
              <Box
                className="pt-4"
                css={{ borderBottom: "solid 1px #B3DAFF" }}
              ></Box>
              <Box className="row mt-4">
                <Box className="col-12">
                  <Text size="h4" className="text-center mb-1">
                    That's good choice!
                  </Text>
                </Box>
              </Box>
              <Box className="row justify-content-center mt-3 text-center">
                <Box>
                  <Button className="p-2" color="yellow"
                    onClick={() => {
                      setContactUsVal({
                        SubjectId: "9",
                        Description: ""
                      }
                      );
                      router.push('/Support/ContactUs')
                    }}
                  >
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Request a Callback
                    </Text>
                  </Button>

                  <Button className="p-2" color="yellow">
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                      onClick={() => {
                        router.push("/Goals/MyGoals");
                      }}
                    >
                      Start Investing Now
                    </Text>
                  </Button>
                </Box>
              </Box>
            </Box>
          </TabsContent>

          <TabsContent value="Retirement">
            <Box className="row">
              <Box className="col-12 col-lg-6">
                <Box className="row justify-content-between">
                  <Box className="col-12 col-lg-7">
                    <Text size="h5">
                      My age is
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 mx-lg-3 mx-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center ms-4"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalRetire1} Years
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="18"
                          value={rangevalRetire1}
                          min="18"
                          max="60"
                          onChange={(event: any) =>
                            setRangevalRetire1(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 justify-content-between">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      I want to retire at the age of{" "}
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 mx-lg-3 mx-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center ms-4"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalRetire2} Years
                    </Box>
                  </Box>
                  <Box className="row mt-2 ">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="30"
                          value={rangevalRetire2}
                          min="30"
                          max="60"
                          onChange={(event: any) => {
                            setRangevalRetire2(event.target.value)
                            setRangevalRetire4((parseInt(event.target.value) + 10).toString())
                          }
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 justify-content-between">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      My current monthly expenditure is
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box className="row">

                      <Box className="col-1 p-0 m-0 ">
                        {rangevalRetire3 >= 100 && rangevalRetire3 <= 500000 ? <></> : <>
                          <Box >
                            {/* @ts-ignore */}
                            <Text className="my-1 px-1" title="Amount Range is 100 to 5 lakhs">{" "}<Info size="0.9rem" color="red" css={{ color: "red" }} />{" "}</Text>
                          </Box>
                        </>}
                      </Box>
                      <Box className="col-10 ">
                        <Input
                          type="text"
                          size="h3"
                          value={rangevalRetire3}
                          note={"₹(Min.:₹1000)"}
                          className={`text-center ${rangevalRetire3 >= 100 && rangevalRetire3 <= 500000 ? styles.inputBlue : styles.inputRed}`}
                          css={{
                            backgroundColor: "#b3daff",
                            border: "solid 1px #B3DAFF",
                            borderRadius: "5px",
                          }}
                          onChange={(e: any) => { setRangevalRetire3(e.target.value.replace(/\D/g, '')) }}
                        />
                      </Box>
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range slider w-100"
                          defaultValue="10000"
                          value={rangevalRetire3}
                          min="100"
                          step={"100"}
                          max="500000"
                          onChange={(event: any) =>
                            setRangevalRetire3(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 justify-content-between">
                  <Box className="col-12 col-lg-7">
                    <Text size="h5">
                      How many years should my savings last
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 mx-lg-3 mx-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center ms-4"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalRetire4} Years
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue={rangevalRetire2}
                          value={rangevalRetire4}
                          min={rangevalRetire2}
                          max="100"
                          onChange={(event: any) =>
                            setRangevalRetire4(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 mb-3 mb-lg-0">
                  <Box className="col-12">
                    <Text
                      size="h4"
                      // @ts-ignore
                      css={{ color: "var(--colors-blue1)" }}
                    >
                      My Risk Profile is
                    </Text>
                    <Box>
                      <GroupBox>
                        <RadioGroup
                          // defaultValue={values.gender}
                          className="inlineRadio"
                          name="RiskProfileRetire"
                          // required
                          value={riskRetire}
                          onValueChange={(e: any) => {
                            setRiskRetire(e);
                          }}
                        // css={{ flexDirection: "column !important" }}
                        >
                          <Radio value="conservative" label="Conservative" id="conservativeRetire" /><br />
                          <Radio value="moderately conservative" label="Moderately Conservative" id="moderately conservativeRetire" /><br />
                          <Radio value="balanced" label="Balanced" id="balancedRetire" /><br />
                          <Radio value="aggressive" label="Aggressive" id="aggressiveRetire" /><br />
                          <Radio value="very aggressive" label="Very Aggressive" id="very aggressiveRetire" /><br />
                        </RadioGroup>
                      </GroupBox>

                    </Box>
                    <Box className="col-12 text-end">
                      <Button
                        className="p-2"
                        color="yellow"
                        onClick={() => {
                          if (rangevalRetire3 >= 100 && rangevalRetire3 <= 500000) {
                            handleRetirementCalculator()
                          } else {
                            toastAlert("warn", "Amount Range is in between 1000 to 5 lakhs")
                          }
                        }}
                      >
                        <Text
                          size="h6"
                          //@ts-ignore
                          color="gray8"
                        >
                          Calculate
                        </Text>
                      </Button>
                    </Box>
                  </Box>
                </Box>
              </Box>
              <Box className="col-12 col-lg-6">
                <Box className="row mb-2">
                  <Box className="col-12 col-lg-4">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#b3daff",
                        color: "#0861b6",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Starting Age
                        </Text>
                      </Box>
                      <hr className="m-0 p-0 text-white" />
                      <Box className="col-12">
                        <Text
                          size="h4"
                          className="text-center mb-1"
                        >
                          {showStartAge} Years
                        </Text>
                      </Box>
                    </Box>
                  </Box>

                  <Box className="col-12 col-lg-4 mt-2 mt-lg-0">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#b3daff",
                        color: "#0861b6",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Retiring Age
                        </Text>
                      </Box>
                      <hr className="m-0 p-0 text-white" />
                      <Box className="col-12">
                        <Text
                          size="h4"
                          className="text-center mb-1"
                        >
                          {showRetireAge} Years
                        </Text>
                      </Box>
                    </Box>
                  </Box>

                  <Box className="col-12 col-lg-4 mt-2 mt-lg-0">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#b3daff",
                        color: "#0861b6",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Life Expectency Age
                        </Text>
                      </Box>
                      <hr className="m-0 p-0 text-white" />
                      <Box className="col-12">
                        <Text
                          size="h4"
                          className="text-center mb-1"
                        >
                          {showExpectAge} Years
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                {/* <Box className="row mb-2 justify-content-center">

                  <Box className="col-12 col-lg-6 mt-2 mt-lg-0">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#b3daff",
                        color: "#0861b6",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Monthly Investment
                        </Text>
                      </Box>
                      <hr className="m-0 p-0 text-white" />
                      <Box className="col-12">
                        <Text
                          size="h4"
                          className="text-center mb-1"
                        >
                           &#x20B9;{Number(Math.round(showMonthly /10)*10).toLocaleString(
                                  "en-IN"
                                ) || "0.0"}
                        </Text>
                      </Box>
                    </Box>
                  </Box>

                </Box> */}
                <Box className="row d-flex justify-content-center">
                  <Box className="col-12 col-lg-6">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#FFFFFF",
                        border: "solid #B3DAFF",
                        borderWidth: "1px 15px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Monthly Investment
                        </Text>
                      </Box>
                      <Box className="col-12">
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          // @ts-ignore
                          css={{ color: "#005CB3" }}
                        >
                          &#x20B9;{Number(Math.round(showMonthly / 10) * 10).toLocaleString(
                            "en-IN"
                          ) || "0.0"}
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <StyledTabs defaultValue="Accumulation Phase">
                  <TabsList
                    aria-label="Manage your account"
                    className="justify-content-center mt-3"
                    css={{
                      borderBottom: "1px solid var(--colors-gray7)",
                      overflowX: "auto",
                    }}
                  >
                    {RetirementChartTabs.map((item, index) => {
                      return (
                        <TabsTrigger
                          value={item.value}
                          className="tabs me-3"
                          key={item.value}
                          css={{ border: "solid 1px var(--colors-blue1)", height: "25px" }}
                        >
                          {/* @ts-ignore */}
                          <Text size="h5">
                            {item.title}
                          </Text>
                        </TabsTrigger>
                      );
                    })}
                  </TabsList>
                  <TabsContent value="Accumulation Phase">
                    <Box>
                      <ReactECharts option={AccumulatedPhase} />
                    </Box>
                  </TabsContent>
                  <TabsContent value="Distribution Phase">
                    <Box>
                      <ReactECharts option={DistributionPhase} />
                    </Box>
                  </TabsContent>
                </StyledTabs>
              </Box>
              <Box
                className="pt-4"
                css={{ borderBottom: "solid 1px #B3DAFF" }}
              ></Box>
              <Box className="row mt-4">
                <Box className="col-12">
                  <Text size="h4" className="text-center mb-1">
                    Let's begin, shall we?
                  </Text>
                </Box>
              </Box>
              <Box className="row justify-content-center mt-3 text-center">
                <Box>
                  <Button className="p-2" color="yellow"
                    onClick={() => {
                      setContactUsVal({
                        SubjectId: "9",
                        Description: ""
                      }
                      );
                      router.push('/Support/ContactUs')
                    }}
                  >
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Request a Callback
                    </Text>
                  </Button>

                  <Button className="p-2" color="yellow">
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                      onClick={() => {
                        router.push("/Goals/MyGoals");
                      }}
                    >
                      Start Investing Now
                    </Text>
                  </Button>
                </Box>
              </Box>
            </Box>
          </TabsContent>

          {/* <TabsContent value="SWP">
            <Box className="row">
              <Box className="col-12 col-lg-6">
                <Box className="row">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      Total Investment
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      RS. {rangevalSWP1}
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="1"
                          min="1"
                          max="30"
                          onChange={(event: any) =>
                            setRangevalSWP1(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      Withdrawal Per Month{" "}
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      Rs. {rangevalSWP2}{" "}
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="10000"
                          min="10000"
                          max="5000000"
                          onChange={(event: any) =>
                            setRangevalSWP2(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      Executed Return Rate (p.a)
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalSWP3}%
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="500"
                          min="500"
                          max="50000"
                          onChange={(event: any) =>
                            setRangevalSWP3(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      Time Period
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalSWP4} Years
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="1"
                          min="1"
                          max="35"
                          onChange={(event: any) =>
                            setRangevalSWP4(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 mb-3 mb-lg-0">
                  <Box className="col-md-12 col-sm-12 col-lg-12">
                    <Text
                      size="h4"
                      css={{ color: "var(--colors-blue1)" }}
                    >
                      My Risk Profile is
                    </Text>
                    <Box>
                      <Text size="h5">
                        <input
                          type="radio"
                          value="Conservative"
                          name="RiskProfile"
                        />{" "}
                        Conservative
                        <br />
                        <input
                          type="radio"
                          value="ModeratelyConservative"
                          name="RiskProfile"
                        />{" "}
                        Moderately Conservative
                        <br />
                        <input
                          type="radio"
                          value="Balanced"
                          name="RiskProfile"
                        />{" "}
                        Balanced
                        <br />
                        <input
                          type="radio"
                          value="Aggressive"
                          name="RiskProfile"
                        />{" "}
                        Aggressive
                        <br />
                        <input
                          type="radio"
                          value="VeryAggressive"
                          name="RiskProfile"
                        />{" "}
                        Very Aggressive
                        <br />
                      </Text>
                    </Box>
                  </Box>
                </Box>
              </Box>
              <Box className="col-12 col-lg-6">
                <Box className="row d-flex justify-content-center">
                  <Box className="col-12 col-lg-5">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#FFFFFF",
                        border: "solid #B3DAFF",
                        borderWidth: "1px 1px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Invested Amount
                        </Text>
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          css={{ color: "#005CB3" }}
                        >
                          5,00,000
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                  <Box className="col-12 col-lg-5 mt-2 mt-lg-0">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        border: "solid #005CB3",
                        borderWidth: "1px 1px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Return Amount
                        </Text>
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          css={{ color: "#005CB3" }}
                        >
                          6,00,000
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box className="row mt-3">
                  <Box>
                    <ReactECharts
                      option={InvestmentGrowth}
                      style={{ height: 300 }}
                      opts={{ renderer: "svg" }}
                    />
                  </Box>

                  <Box className="text-center">
                    <Text size="h6">Note: Expected rate of return 8%</Text>
                  </Box>
                </Box>
              </Box>
              <Box
                className="pt-4"
                css={{ borderBottom: "solid 1px #B3DAFF" }}
              ></Box>
              <Box className="row mt-4">
                <Box className="col-12">
                  <Text size="h4" className="text-center mb-1">
                    Let's begin, shall we?
                  </Text>
                </Box>
              </Box>
              <Box className="row justify-content-center mt-3 text-center">
                <Box>
                  <Button className="p-2" color="yellow"
                    onClick={() => {
                      setContactUsVal({
                        SubjectId: "9",
                        Description: ""
                      }
                      );
                      router.push('/Support/ContactUs')
                    }}
                  >
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Request a Callback
                    </Text>
                  </Button>

                  <Button className="p-2" color="yellow">
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Start Investing Now
                    </Text>
                  </Button>
                </Box>
              </Box>
            </Box>
          </TabsContent>

          <TabsContent value="MFReturn">
            <Box className="row">
              <Box className="col-12 col-lg-6">
                <Box className="row">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      Total Investment
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      Rs. {rangevalMF1}
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="1"
                          min="1"
                          max="50"
                          onChange={(event: any) =>
                            setRangevalMF1(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      Executed Return Rate (p.a)
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalMF2}%
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="500"
                          min="500"
                          max="2000000"
                          onChange={(event: any) =>
                            setRangevalMF2(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4">
                  <Box className="col-12 col-lg-8">
                    <Text size="h5">
                      Time Period
                    </Text>
                  </Box>
                  <Box className="col-12 col-lg-3 ms-lg-3 ms-0 mt-2 mt-lg-0">
                    <Box
                      size="h3"
                      className="text-center"
                      css={{
                        backgroundColor: "#b3daff",
                        border: "solid 1px #B3DAFF",
                        borderRadius: "5px",
                      }}
                    >
                      {rangevalMF3} Years
                    </Box>
                  </Box>
                  <Box className="row mt-2">
                    <Box className="col-12">
                      <Box>
                        <Input
                          type="range"
                          className="custom-range w-100"
                          defaultValue="1"
                          min="1"
                          max="35"
                          onChange={(event: any) =>
                            setRangevalMF3(event.target.value)
                          }
                          css={{
                            backgroundColor: "#B3DAFF",
                            border: "solid 1px #B3DAFF",
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>

                <Box className="row mt-4 mb-3 mb-lg-0">
                  <Box className="col-12">
                    <Text
                      size="h4"
                      css={{ color: "var(--colors-blue1)" }}
                    >
                      My Risk Profile is
                    </Text>
                    <Box>
                      <Text size="h5">
                        <input
                          type="radio"
                          value="Conservative"
                          name="RiskProfile"
                        />{" "}
                        Conservative
                        <br />
                        <input
                          type="radio"
                          value="ModeratelyConservative"
                          name="RiskProfile"
                        />{" "}
                        Moderately Conservative
                        <br />
                        <input
                          type="radio"
                          value="Balanced"
                          name="RiskProfile"
                        />{" "}
                        Balanced
                        <br />
                        <input
                          type="radio"
                          value="Aggressive"
                          name="RiskProfile"
                        />{" "}
                        Aggressive
                        <br />
                        <input
                          type="radio"
                          value="VeryAggressive"
                          name="RiskProfile"
                        />{" "}
                        Very Aggressive
                        <br />
                      </Text>
                    </Box>
                  </Box>
                </Box>
              </Box>
              <Box className="col-12 col-lg-6">
                <Box className="row d-flex justify-content-center">
                  <Box className="col-12 col-lg-5">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        backgroundColor: "#FFFFFF",
                        border: "solid #B3DAFF",
                        borderWidth: "1px 1px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Invested Amount
                        </Text>
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          css={{ color: "#005CB3" }}
                        >
                          25,000
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                  <Box className="col-12 col-lg-5 mt-2 mt-lg-0">
                    <Box
                      className="row mx-1 p-2"
                      css={{
                        border: "solid #005CB3",
                        borderWidth: "1px 1px 1px 15px",
                        borderRadius: "8px 8px 8px 8px",
                      }}
                    >
                      <Box className="col-12">
                        <Text
                          size="h5"
                          className="text-center mb-1"
                        >
                          Return Amount
                        </Text>
                        <Text
                          size="h4"
                          className="text-center mb-1"
                          css={{ color: "#005CB3" }}
                        >
                          77,646
                        </Text>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box className="row mt-3">
                  <Box>
                    <ReactECharts
                      option={InvestmentGrowth}
                      style={{ height: 300 }}
                      opts={{ renderer: "svg" }}
                    />
                  </Box>

                  <Box className="text-center">
                    <Text size="h6">Note: Expected rate of return 8%</Text>
                  </Box>
                </Box>
              </Box>
              <Box
                className="pt-4"
                css={{ borderBottom: "solid 1px #B3DAFF" }}
              ></Box>
              <Box className="row mt-4">
                <Box className="col-12">
                  <Text size="h4" className="text-center mb-1">
                    Let's begin, shall we?
                  </Text>
                </Box>
              </Box>
              <Box className="row justify-content-center mt-3 text-center">
                <Box>
                  <Button className="p-2" color="yellow"
                    onClick={() => {
                      setContactUsVal({
                        SubjectId: "9",
                        Description: ""
                      }
                      );
                      router.push('/Support/ContactUs')
                    }}
                  >
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Request a Callback
                    </Text>
                  </Button>

                  <Button className="p-2" color="yellow">
                    <Text
                      size="h6"
                      //@ts-ignore
                      color="gray8"
                    >
                      Start Investing Now
                    </Text>
                  </Button>
                </Box>
              </Box>
            </Box>
          </TabsContent>

          <TabsContent value="CostOfReturn">Work in progress</TabsContent> */}
        </StyledTabs>
      </Card>
    </>
  );
};

export default CalculatorPage;
