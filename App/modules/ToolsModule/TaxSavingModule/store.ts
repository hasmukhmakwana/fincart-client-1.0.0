import { TaxSavingListTypes, TaxCalculatetype } from "./TaxSavingTypes"
import create from "zustand";

interface StoreTypes {
    loading: boolean;
    taxSavingList: TaxSavingListTypes[];
    finCalculate: TaxCalculatetype;
    minDate: string;
    maxDate: string;
    localUser: string;
    setLoader: (payload: boolean) => void;
    setTaxSavingList: (payload: TaxSavingListTypes[]) => void;
    setFincalculate: (payload: TaxCalculatetype) => void;
    setMinDate: (payload: string) => void;
    setMaxDate: (payload: string) => void;
    setLocalUser: (payload: string) => void;
}

const useTaxSavingStore = create<StoreTypes>((set) => ({
    loading: false,
    minDate: "",
    maxDate: "",
    localUser: "",
    taxSavingList: [],
    finCalculate: {
        time: "",
        getLumpsum: "",
        getSip: "",
        investLumpsum: "",
        investSip: "",
        PMT: "",
        ROR: "",
        RORL: "",
        Inflation: "",
        goalName: ""
    },

    setLoader: (payload) =>
        set((state) => ({
            ...state,
            loading: payload,
        })),

    setMinDate: (payload) =>
        set((state) => ({
            ...state,
            minDate: payload,
        })),

    setMaxDate: (payload) =>
        set((state) => ({
            ...state,
            maxDate: payload,
        })),

    setLocalUser: (payload) =>
        set((state) => ({
            ...state,
            localUser: payload,
        })),

    setTaxSavingList: (payload) =>
        set((state) => ({
            ...state,
            taxSavingList: payload,
        })),

    setFincalculate: (payload) =>
        set((state) => ({
            ...state,
            finCalculate: payload,
        })),
}))

export default useTaxSavingStore;