import Box from "@ui/Box/Box";
import React, { useState, useEffect } from "react";
import Text from "@ui/Text/Text";
import Card from "@ui/Card";
import styles from "../../Tools.module.scss";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import Basket2fill from "App/icons/Basket2fill";
import PencilFill from "App/icons/PencilFill";
import DeleteIcon from "App/icons/DeleteIcon";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import Loader from "App/shared/components/Loader";
import useTaxSavingStore from "../store";
import TaxSavingAddModal, { VerifyNameTypes } from "./TaxSavingAddModal";
import TaxSavingUpdateModal from "./TaxSavingUpdateModal";
import TaxSavingDelete from "./TaxSavingDelete";
import TaxSavingInvest from "../../TaxSavingOnetime/components/TaxSavingInvest"
import { useRouter } from "next/router";
import Add from "App/icons/Add";
import { getUser } from "App/utils/helpers";
import useRegistrationStore from "App/modules/RegistrationModule/store";

type SaveTypes = {
  // setEditModalOpen: (values: boolean) => void;
  fetchData: () => void;
};

const TaxSavingPage = ({ fetchData }: SaveTypes) => {
  const user: any = getUser();
  const router = useRouter();
  const [openTaxSaving, setOpenTaxSaving] = useState(false);
  const [openTaxSavingInvest, setOpenTaxSavingInvest] = useState(false);
  const [openUpdateTaxSaving, setOpenUpdateTaxSaving] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [goalId, setGoalId] = useState("");
  const { loading, taxSavingList } = useTaxSavingStore();
  const [recommendationData, setRecommendationData] = useState<any>();
  const { setBasicID, setRegiType } = useRegistrationStore();
  // useEffect(() => { return () => { } }, [])

  return (
    <>
      <Box className="container">
        <Box className="row p-4" css={{ backgroundColor: "white", borderRadius: "8px" }}>
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Box className="row text-end">
              <Box className="col-lg-12 col-md-12 col-sm-12 mb-1">
                <Button color="yellow" size="md" onClick={setOpenTaxSaving}>
                  Initiate Tax Saving <Add></Add>
                </Button>
              </Box>
            </Box>
            <Box className="row">
              {loading ? (
                <Loader />
              ) : (
                <>
                  {taxSavingList?.map((record, index) => {
                    return (
                      <Box className="col-lg-4 col-md-6 col-12">
                        <Card css={{ p: 15, border: "solid 1px #b3daff" }}>
                          <Box className="row">
                            <Box className="col-4 col-lg-3">
                              <Box className={styles.imgbg} display="inlineBlock">
                                <img
                                  width={50}
                                  height={50}
                                  src={record.goalImg}
                                  alt="Tax Saving"
                                />
                              </Box>
                            </Box>
                            <Box className="col-8 col-lg-9 text-left">
                              {/* @ts-ignore */}
                              <Text>
                                {record.goalName}
                              </Text>
                              {/* @ts-ignore */}
                              <Text
                                size="h6" color="primary">
                                Achieved {record.goalPerc}%
                              </Text>
                              <Text size="h6" className={`badge mt-2 rounded-pill me-1 ${styles.titlepill}`} >Duration: {record.duration} yr(s)</Text>
                            </Box>
                          </Box>
                          <Box className="row my-3">
                            <Box className="col-6  text-center">
                              <Text color="primary">
                                Transaction Amount
                              </Text>
                              <Text
                                //@ts-ignore 
                                style={{ color: "#202020" }}>&#x20B9;{" "} {Number(record?.investAmount).toLocaleString("en-IN")}
                              </Text>
                            </Box>
                            <Box className="col-6  text-center">
                              <Text color="primary">
                                After Completion
                              </Text>
                              <Text
                                //@ts-ignore 
                                style={{ color: "#202020" }}>&#x20B9;{Number(record?.getAmount).toLocaleString("en-IN")}
                              </Text>
                            </Box>
                          </Box>

                          <Box className="row justify-content-center mt-3 pe-3">

                            {user?.cafstatus === "success" ? <>
                              <Button
                                className="col-auto mb-2"
                                color="yellowGroup"
                                size="md"
                                onClick={() => {
                                  //add items to localStorage
                                  //set goalCode
                                  localStorage.setItem("userGoalId", record?.userGoalId)
                                  //set section
                                  localStorage.setItem("section", "TAX_SAVING")
                                  //set amount
                                  localStorage.setItem("amount", record?.investAmount)
                                  //set duration
                                  localStorage.setItem("duration", record?.duration)
                                  router.push("/RecommendedSchemes")
                                }}
                              >
                                {/* @ts-ignore */}
                                <GroupBox align="center">
                                  <Box>
                                    {" "}
                                    <Basket2fill></Basket2fill>
                                  </Box>
                                  {/* @ts-ignore */}
                                  <Box>
                                    {/* @ts-ignore */}
                                    <Text
                                      size="h6"
                                      //@ts-ignore
                                      color="gray8"
                                      className={styles.button}
                                    >
                                      Invest
                                    </Text>
                                  </Box>
                                </GroupBox>
                              </Button>
                            </> : <>
                              <Button
                                color="yellowGroup"
                                size="md"
                                className="col-auto mb-2"
                                onClick={() => {
                                  setRegiType("PENDING");
                                  setBasicID(user?.basicid);
                                  router.push("/Registration");
                                }}
                              >
                                <GroupBox align="center">
                                  <Box>
                                    <Add />
                                  </Box>
                                  <Box>
                                    <Text
                                      size="h6"
                                      //@ts-ignore
                                      color="gray8"
                                      className={styles.button}>
                                      Complete Reg.
                                    </Text>
                                  </Box>
                                </GroupBox>
                              </Button>
                            </>}
                            <Button
                              className="col-auto mb-2 "
                              color="yellowGroup"
                              size="md"
                              onClick={() => {
                                setGoalId(record.userGoalId);
                                setOpenUpdateTaxSaving(true);
                              }}
                            >
                              {/* @ts-ignore */}
                              <GroupBox align="center">
                                <Box>
                                  {" "}
                                  <PencilFill></PencilFill>
                                </Box>
                                {/* @ts-ignore */}
                                <Box>
                                  <Text
                                    // @ts-ignore
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                    className={styles.button}
                                  >
                                    Edit
                                  </Text>
                                </Box>
                              </GroupBox>
                            </Button>
                            <Button
                              className="col-auto mb-2 "
                              color="yellowGroup"
                              size="md"
                              onClick={() => {
                                setGoalId(record.userGoalId);
                                setOpenDelete(true);
                              }}
                            >
                              {/* @ts-ignore */}
                              <GroupBox align="center">
                                <Box>
                                  {" "}
                                  <DeleteIcon></DeleteIcon>
                                </Box>
                                {/* @ts-ignore */}
                                <Box>
                                  <Text
                                    // @ts-ignore
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                    className={styles.button}
                                  >
                                    Delete
                                  </Text>
                                </Box>
                              </GroupBox>
                            </Button>
                          </Box>
                        </Card>
                      </Box>)
                  })}
                </>
              )}
            </Box>
          </Box>
        </Box>
      </Box>

      {/* ADD MODAL */}
      <DialogModal
        open={openTaxSaving}
        setOpen={setOpenTaxSaving}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        <TaxSavingAddModal setTaxSavingModalOpen={setOpenTaxSaving} reload={fetchData} />
      </DialogModal>

      {/* Invest MODAL */}
      <DialogModal
        open={openTaxSavingInvest}
        setOpen={setOpenTaxSavingInvest}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "60%" },
        }}
      >
        {/* @ts-ignore */}
        <TaxSavingInvest />
      </DialogModal>

      {/* Update MODAL */}
      <DialogModal
        open={openUpdateTaxSaving}
        setOpen={setOpenUpdateTaxSaving}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        {/* @ts-ignore */}
        <TaxSavingUpdateModal setEditModalOpen={setOpenUpdateTaxSaving} IdPass={goalId} reload={fetchData} />
      </DialogModal>

      {/* Delete MODAL */}
      <DialogModal
        open={openDelete}
        setOpen={setOpenDelete}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        {/* @ts-ignore */}
        <TaxSavingDelete setDeleteModalOpen={setOpenDelete} IdPass={goalId} reload={fetchData} />

      </DialogModal>
    </>
  );
};

export default TaxSavingPage;
