import React from 'react'
import Box from "@ui/Box/Box";
import useTaxSavingStore from "../store";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import {
    deleteGoal
} from "App/api/goalPlan";
import { encryptData } from 'App/utils/helpers';
import { toastAlert } from 'App/shared/components/Layout';
type SaveTypes = {
    setDeleteModalOpen: (values: boolean) => void;
    IdPass: (values: string) => void;
    reload: () => void;
};

const TaxSavingDelete = ({ setDeleteModalOpen, IdPass, reload }: SaveTypes) => {
    const { loading } = useTaxSavingStore();
    const TaxDelete = async (goalId: any) => {
        try {
            const enc: any = encryptData(parseInt(goalId));
            // console.log(enc);
            const result: any = await deleteGoal(enc);
            console.log(result);
            reload();
            setDeleteModalOpen(false);
            toastAlert("success", "Investment Deleted Successfully");
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
    }
    return (
        <>
            <Box className="col-md-12 col-sm-12 col-lg-12">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    <Box className="row">
                        <Box className="col-auto text-light"><Text>Delete Tax Saving</Text></Box>
                        <Box className="col"></Box>
                        <Box className="col-auto"></Box>
                    </Box>
                </Box>
                <Box
                    className="text-center modal-body modal-body-scroll p-3"
                    css={{ padding: "0.5rem" }}
                >
                    <Text css={{ mt: 20, pt: 10 }}>
                        Are you sure to delete this Tax Saving?
                    </Text>
                    <Box css={{ mt: 20, pt: 10 }} className="text-end">
                        <Button
                            type="submit"
                            color="yellow"
                            onClick={() => { setDeleteModalOpen(false) }}>
                            Cancel
                        </Button>
                        <Button
                            type="submit"
                            color="yellow"
                            //@ts-ignore
                            onClick={() => { TaxDelete(IdPass) }}
                            loading={loading}
                        >
                            Delete Now
                        </Button>
                    </Box>
                </Box>
            </Box>
        </>
    )
}

export default TaxSavingDelete