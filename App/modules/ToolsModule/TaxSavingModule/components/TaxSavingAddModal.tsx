import React, { useState, useEffect } from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import * as Yup from "yup";
import { Formik } from 'formik';
import styles from "../../Tools.module.scss";
import create from "zustand";
import useTaxSavingStore from "../store";
import { getFinCalculatorData } from "App/api/tools"
import { createGoal } from "App/api/goalPlan"
import { toastAlert } from "App/shared/components/Layout";
import { encryptData } from "App/utils/helpers";
import Loader from "App/shared/components/Loader";

const validationSchema = Yup.object().shape({
    AmountPF: Yup.number().integer().min(0, "amount should be greater than or equal to 0").max(150000, "amount cannot be greater than 1,50,000 Rs."),

    AmountLifeInsurance: Yup.number().integer().min(0, "amount should be greater than or equal to 0").max(150000, "amount cannot be greater than 1,50,000 Rs."),

    AmountSmallSaving: Yup.number().integer().min(0, "amount should be greater than or equal to 0").max(150000, "amount cannot be greater than 1,50,000 Rs."),

    AmountLoanPrincipal: Yup.number().integer().min(0, "amount should be greater than or equal to 0").max(150000, "amount cannot be greater than 1,50,000 Rs."),
});

export type VerifyNameTypes = {
    AmountPF: string;
    AmountLifeInsurance: string;
    AmountSmallSaving: string;
    AmountLoanPrincipal: String;
};

interface StoreTypes {
    verifyName: VerifyNameTypes;
}
const useRegistrationStore = create<StoreTypes>((set) => ({
    //* initial state
    verifyName: {
        AmountPF: "",
        AmountLifeInsurance: "",
        AmountSmallSaving: "",
        AmountLoanPrincipal: "",
    }
}))
type SaveTypes = {
    setTaxSavingModalOpen: (values: boolean) => void;
    reload: () => void;
};

const TaxSavingAddModal = ({ setTaxSavingModalOpen, reload }: SaveTypes) => {
    const { verifyName, loading } = useRegistrationStore((state: any) => state);
    const [show, setShow] = useState(false);
    const [loaderApi, setLoaderApi] = useState(false);
    const [createSt, setCreateSt] = useState(false);
    const [saveCalc, setSaveCalc] = useState<any>(0);
    const [goalResult, setGoalResult] = useState<any>({});
    const [message, setMessage] = useState('');

    const handleChange = (event: any) => {
        setMessage(event.target.value);
        console.log('value is:', event.target.value);
    };
    // const showCalc = (e: any) => {
    //     setShow(true);
    //     // console.log(e);
    // };
    const {
        minDate,
        maxDate,
        localUser,
        finCalculate,
        setFincalculate
    } = useTaxSavingStore();

    const calculateAndCreate = async (data: any) => {
        data = data || {};

        let Total: any = ((parseInt(data?.AmountPF) || 0) +
            (parseInt(data?.AmountLifeInsurance) || 0) +
            (parseInt(data?.AmountSmallSaving) || 0) +
            (parseInt(data?.AmountLoanPrincipal) || 0));

        setSaveCalc(Total > 150000 ? 150000 : Total);

        let calculation = 0;

        if (Total > 0 && Total <= 150000) {
            calculation = 150000 - Total;
        }
        else if (Total == 0) {
            calculation = 150000;
        }

        //Created Goal API
        try {
            if (createSt) {
                setLoaderApi(true);
                const obj: any =
                {
                    usergoalId: null,
                    basicid: localUser,
                    Relation: null,
                    childName: null,
                    age: null,
                    gender: null,
                    annualIncome: null,
                    goalCode: "FG14",
                    otherGoalName: "Tax Saving " + new Date().getFullYear().toString(),
                    typeCode: "FG104",
                    monthlyAmount: data?.AmountSmallSaving,
                    presentValue: data?.AmountLoanPrincipal,
                    risk: "M",
                    goal_StartDate: saveCalc,
                    // goal_EndDate: minDate,
                    goal_EndDate: maxDate,
                    inflationRate: finCalculate?.Inflation,
                    ror: finCalculate?.ROR,
                    PMT: finCalculate?.PMT,
                    downPaymentRate: null,
                    trnx_Type: "L",
                    getAmount: finCalculate?.getLumpsum,
                    retirementAge: data?.AmountLifeInsurance,
                    investAmount: finCalculate?.investLumpsum,
                    people: data?.AmountPF
                };
                const enc: any = encryptData(obj);
                let result: any = await createGoal(enc);

                reload();
                setTaxSavingModalOpen(false);
                toastAlert("success", "Tax Saving Successfully Added");
                setLoaderApi(false);
                setCreateSt(false);
                //Created Goal API end
            } else {
                //Fincalc api start
                if (calculation > 0) {
                    setLoaderApi(true);
                    const obj: any =
                    {
                        type: "ILAMT",
                        currentAmount: calculation,
                        startDate: Total,
                        endDate: maxDate,
                        goalCode: "FG14",
                        otherGoalName: "Tax Saving " + new Date().getFullYear().toString(),
                        childName: data?.AmountLoanPrincipal,
                        travelPeople: data?.AmountPF,
                        locationCode: "FG104",
                        budgetType: null,
                        businessStartupCost: data?.AmountLifeInsurance,
                        monthlyexpence: data?.AmountSmallSaving
                    };

                    const enc: any = encryptData(obj);
                    let result: any = await getFinCalculatorData(enc);
                    setGoalResult(result?.data);
                    setFincalculate(result?.data);
                    setShow(true);
                    setLoaderApi(false);
                }
                else {
                    setShow(false);
                }
            }
            //Fincalc api end
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
        //created Goal Api end
    }
    useEffect(() => {
        console.log(minDate);
        return () => { }
    }, [])
    return (
        <>
            <Formik
                initialValues={verifyName}
                validationSchema={validationSchema}
                enableReinitialize
                onSubmit={(values, { resetForm }) => {
                    // clickOnSubmit(values);
                    // showCalc(values);
                    calculateAndCreate(values);
                }}
            >
                {({
                    values,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    errors,
                    touched,
                    setFieldValue,
                    submitCount
                }) => (
                    <Box className="col-md-12 col-sm-12 col-lg-12">
                        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                            {/* @ts-ignore */}
                            <Text css={{ color: "var(--colors-blue1)" }}>Tax Saving</Text>
                        </Box>
                        <Box className="modal-body modal-body-scroll p-3">
                            {/* {setSaveCalc((parseInt(goalResult?.people) +
                                parseInt(goalResult?.retirementAge) +
                                parseInt(goalResult?.monthlyAmount) +
                                parseInt(goalResult?.presentValue)).toString())} */}
                            <Box className="row mx-1 px-0">
                                <Box className="col-12 px-0 mx-0" css={{ color: "#0861b6" }}>
                                    {/* @ts-ignore */}
                                    <Text size="h4" className="text-center mb-1" >Tax Saving Limit U/S.80C</Text>
                                </Box>
                            </Box>
                            <Box className="row mx-1 my-1 pb-1 px-0">
                                <Box className="col-lg-3 px-0 mx-auto" css={{ borderRadius: '8px 8px 8px 8px', overflow: 'hidden' }}>
                                    {/* @ts-ignore */}
                                    <Text size="h4" className="text-center text-black py-1" css={{ backgroundColor: "#b3daff" }} >&#x20B9; 1,50,000</Text>
                                </Box>
                            </Box>
                            <Box>
                                <Text size="h5">Provident Fund<span className='text-primary'> (Employee PF / Government PF)</span></Text>
                                <Input
                                    label="Enter Amount"
                                    name="AmountPF"
                                    placeholder="Enter Amount"
                                    error={submitCount ? errors.AmountPF : null}
                                    onChange={handleChange}
                                    value={values?.AmountPF} />
                            </Box>
                            <Box>
                                <Text size="h5">Life Insurance<span className='text-primary'> (Term Insurance / Endowment / Money Back ULIP Policies)</span></Text>
                                <Input
                                    label="Enter Amount"
                                    name="AmountLifeInsurance"
                                    placeholder="Enter Amount"
                                    error={submitCount ? errors.AmountLifeInsurance : null}
                                    onChange={handleChange}
                                    value={values?.AmountLifeInsurance} />
                            </Box>
                            <Box>
                                <Text size="h5">Small Saving<span className='text-primary'> (National Savings Cert. / Kisan Vikas Patra)</span></Text>
                                <Input
                                    // type="date"
                                    label="Enter Amount"
                                    name="AmountSmallSaving"
                                    placeholder="Enter Amount"
                                    //value={date}
                                    error={submitCount ? errors.AmountSmallSaving : null}
                                    onChange={handleChange}
                                    value={values?.AmountSmallSaving} />
                            </Box>
                            <Box>
                                <Text size="h5">Loan Principal<span className='text-primary'> (Home Loan Principal)</span></Text>
                                <Input
                                    //type="date"
                                    label="Enter Amount"
                                    name="AmountLoanPrincipal"
                                    placeholder="Enter Amount"
                                    //value={date}
                                    error={submitCount ? errors.AmountLoanPrincipal : null}
                                    onChange={handleChange}
                                    value={values?.AmountLoanPrincipal} />
                            </Box>

                            <Box className="row justify-content-end mx-0 me-1">
                                {/* <Box className="col-auto"></Box> */}
                                <Button
                                    className="mb-2 p-2"
                                    color="yellowGroup"
                                    size="md"
                                    onClick={() => { handleSubmit() }}
                                    loading={loading}>
                                    {/* onClick={() => setShow(true)} */}
                                    <Text
                                        // @ts-ignore

                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        // className={styles.font}
                                        // className={clsx({
                                        //     [styles.font]: true,
                                        //     [styles.button]: true,
                                        //   })}
                                        className={`${styles.button}`}>
                                        Calculate
                                    </Text>
                                </Button>
                            </Box>

                            <Box className="row justify-content-evenly">
                                <Box className="col-lg-6 col-md-12 col-sm-12 mb-2">
                                    <Box className="row mx-1 p-2 px-0" css={{ backgroundColor: "#b3daff", color: "#0861b6", borderRadius: "8px 8px 8px 8px" }}>
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-center mb-1" >Your Savings</Text>
                                        </Box>
                                        <hr className="m-0 p-0 text-white" />
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h4" className="text-center mb-1 text-black" >&#x20B9; {Number(saveCalc).toLocaleString("en-IN")}</Text>
                                        </Box>
                                    </Box>
                                </Box>

                                <Box className="col-lg-6 col-md-12 col-sm-12 mb-2">
                                    <Box className="row mx-1 p-2 px-0" css={{ backgroundColor: "#b3daff", color: "#0861b6", borderRadius: "8px 8px 8px 8px" }}>
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h5" className="text-center mb-1" >You need to save more</Text>
                                        </Box>
                                        <hr className="m-0 p-0 text-white" />
                                        <Box className="col-12 px-0">
                                            {/* @ts-ignore */}
                                            <Text size="h4" className="text-center mb-1 text-black" >&#x20B9; {Number(150000 - saveCalc).toLocaleString("en-IN")}</Text>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                            {<>
                                {show ? <>
                                    {loaderApi ? (
                                        <Loader />
                                    ) : (<>
                                        <Box className="row">
                                            <Box className="col-lg-12" css={{ backgroundColor: "#b3daff", color: "#0861b6" }}>
                                                {/* @ts-ignore */}
                                                <Text size="h5" className={`text-center m-1 font mx-3`} >After {finCalculate?.time} year your expected value of your investment will be &#x20B9;{Number(finCalculate?.getLumpsum).toLocaleString("en-IN")}</Text>
                                            </Box>
                                        </Box>
                                        <Box className="row justify-content-end mx-0">
                                            {/* <Box className="col-auto"></Box> */}
                                            <Button
                                                className=" mt-2 p-2"
                                                color="yellowGroup"
                                                size="md"
                                            >
                                                <Text
                                                    // @ts-ignore
                                                    size="h4"
                                                    //@ts-ignore
                                                    color="gray8"
                                                    className={styles.button}
                                                    onClick={() => {
                                                        setCreateSt(true);
                                                        handleSubmit()
                                                    }}
                                                    loading={loading}
                                                >
                                                    Save
                                                </Text>
                                            </Button>
                                        </Box>
                                    </>)
                                    }
                                </> : ""}
                            </>}
                        </Box>
                    </Box>)}
            </Formik>
        </>
    )
}

export default TaxSavingAddModal