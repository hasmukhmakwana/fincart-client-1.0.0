export type TaxSavingListTypes = {
    userGoalId: string;
    goalName: string;
    goalImg: string;
    investedAmt: string;
    currentAmt: string;
    goalPerc: string;
    getAmount: string;
    goalCode: string;
    duration: string;
    isDelete: string;
    typeCode: string;
    investAmount: string;
    trxntype: string;
}

export type TaxCalculatetype = {
    time: string;
    getLumpsum: string;
    getSip: string;
    investLumpsum: string;
    investSip: string;
    PMT: string;
    ROR: string;
    RORL: string;
    Inflation: string;
    goalName: string;
}