import React, { useState, useEffect } from 'react'
import TaxSavingPage from "./components/TaxSavingPage";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout, { toastAlert } from 'App/shared/components/Layout'
import { fetchTaxSavings } from "App/api/tools"
import { encryptData, getUser } from "App/utils/helpers";
import useTaxSavingStore from './store';

const TaxSaving = () => {
    const user: any = getUser();
    const {
        setLoader,
        setTaxSavingList,
        setMinDate,
        setMaxDate,
        setLocalUser
    } = useTaxSavingStore();
    const fetchTaxes = async () => {
        // return;
        try {
            setLoader(true);
            const enc: any = encryptData(user?.basicid, true);

            let result: any = await fetchTaxSavings(enc);
            setTaxSavingList(result?.data);
            setLoader(false);
            console.log(result?.data);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
    };
    useEffect(() => {
        (async () => {
            fetchTaxes()
        })();
        const date = new Date(new Date().setFullYear(new Date().getFullYear()));
        const MinDate = (date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDate()).padStart(2, '0'));
        const MaxDate = (date.getFullYear() + 3) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDate()).padStart(2, '0'));
        // console.log(date);
        setMinDate(MinDate);
        setMaxDate(MaxDate);
        setLocalUser(user?.basicid);
        return () => {
        }
    }, [])
    return (
        <Layout>
            <PageHeader title="Tax Saving" rightContent={<></>} />
            <TaxSavingPage fetchData={fetchTaxes} />
        </Layout>
    )
}

export default TaxSaving