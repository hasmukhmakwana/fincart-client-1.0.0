import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import Card from "@ui/Card";
import Box from "App/icons/Box";
import ReactECharts from "echarts-for-react";
import Text from "@ui/Text/Text";
function CarouselPage() {
  const InvestmentGrowth = {
    title: {
      // text: 'Sales last Week by Products',
      x: "left",
    },
    tooltip: {
      trigger: "item",
      formatter: (params: any) => {
        console.log(params, "params");
        return `
            <div style="text-align: center;">
            <img style="margin-top: -30px" width="40" src="https://cdn.shopclues.com/images/thumbnails/72640/320/320/178916041DF0DZ14UL1495806482.jpg"  />
            <h6>${params.seriesName}</h6>
            <h5>${params.value}</h5>
            </div>
            `;
      },
    },
    color: ["#FCDE9C", "#84AAD2"],
    legend: {
      type: "plain",
      orient: "horizontal",
      // right: '0',
      top: 40,
      itemWidth: 14,
      icon: "roundRect",
      data: ["Principal", "Current Value"],
    },
    grid: {
      left: "4%",
      bottom: "10%",
      right: "0",
    },
    xAxis: {
      data: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sept",
        "Oct",
        "Nov",
        "Dec",
      ],
      axisLine: {
        show: false,
      },
    },
    yAxis: {
      axisLine: {
        show: false,
      },
      splitLine: {
        show: false,
      },
    },
    series: [
      {
        name: "Principal",
        icon: "image://data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALkAAAAoCAYAAACil1u6AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyVpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ4IDc5LjE2NDAzNiwgMjAxOS8wOC8xMy0wMTowNjo1NyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIxLjAgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OEM1REE1ODMzOUI2MTFFQUIwMzNDRjY0OTMxQjBGMjEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OEM1REE1ODQzOUI2MTFFQUIwMzNDRjY0OTMxQjBGMjEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4QzVEQTU4MTM5QjYxMUVBQjAzM0NGNjQ5MzFCMEYyMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo4QzVEQTU4MjM5QjYxMUVBQjAzM0NGNjQ5MzFCMEYyMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pl5E/iYAAAzrSURBVHja7F0LcFTVGf52776zSVYSQoIJJIbEEMJDA0jUoBAQGEhRxhketsWOgVKlSKmKkraDM/iAavGFo2MsBqVKWzJVxEYM0DZSnoGCIUIg8koTEgJJzCb7uPfu7X8feWw2QJ487PlmdrK759xzzv3Pd77/++/iqEt79vhD5qDIdXo9FwUGhh8QfD6xkm+oWmww2wa8/fIcW+RdQww31AL3nhSw4s8etlMM3YYs3EZ7hCzghhuO4DJuxDUx3IREJ37rWRgYfvBEZyFgYCRnYGAkZ2BgJGdgYCRnYGAkZ2BgJGdguDx6/IsLf7wE7i15EE6dhFhRDon3ggvrD+PwUTDfPxmm0eNYlBluTpKLVefRsOZ58IeLAtv+e055ufO3wJAyEiHLV4KLupVFm+HmsSt8yTeofeLRDgneHkLxYTSsfYlFmuHmUXKxphr1K5ZCcjb4fc9FD4IhPhGSzwehrBQ+si7KBAlJCPnti9fshqIGcpiVzMFBd9ZY70NBkYBir9oWn2TE0jF6HNjuQW5FL09s0iP7ISNs1QKyd4p9eo+zJpgxoZ8P727mUdxRDFJM+OBHRtQd92D2ZqHzAzv0WDDagAiKHe/x4eAhAQV115CNQQZsetIMx0Uej7/rRdn1InnDmpV+BNf3j4B9yXKY09L9+nkKd5Jd+QzBzz4PfXDINYnR3JlWZA3zT06ZE0z4cpMLa8ok3DvGhOQYIDFDRO6HQu8ertuMmBhP4YzXIYVIXtxnd6nDA2kGJNC7+xxE8jodFsw0ITVIwsa/elFIB9pm1CkbazF3ftQxaWasmmDwI8QU+m7OQTey8sVrxnOLTMpgHazXS8ml2q9hjMwDz0WTpOuhDwuH49V3wQ0M9Nvm9AnK61ohON6kEVxCyUEvPj0B3DvehPQoHaY8bMIXqz3YRgoeR0q+/99Cr89fecyLTw5KCKoS+pDgyi5g41deZIRKyNdUNjmBSG+ScJtRJXlr2u3kkP2NWKkRvOY0jw17RSSMMiLzdg5xd5oxf08Tcq+Rogvaul3Xi+S+8x/BPPISuEgXGvPiEPTYEx0S/HrAbtTeuESsyBcg55oCUu+Nyy2I5PRIdQDfxXMYOlgPvkyHrRckRRWzf2LB+Bg9DKKE8moJdocOzhMezN8J5Dxmxi0eEcdILUfTtaA+54568avP1fH9Uy3NQUoe3E9C1CEfkGTCW9MMcNeJaDBxSAjTQfD6sD3fjTXFUjuSGZD7YxMsVTyy/sQr/d9cZEZETevnd+hzVJOAZTk8xicZMDxEwsjDEp6ba0aMSVX4eb+wIS7fhQ+bVTHcgHd+blLndvnw98/ceK1MCrQ/dxsUBRWqvZhN8ykoE2HKCsKUCB3ShuqQu1uN14IZZsxI4mCnOd1OEV9t8+C1Y61jptxhwtPpBkTadUq8Sg97W/ZDxnSyWgvHGmCncNacE1Br4xAlyvfVsT1JSTHi6YlGRFrV8Q4XefBMF+1g15S8fp960QAXghdUwZgxNaDPS59d+T90uDeRQ3pS7/9b8crvJUUFDFYDPvgZkXsHj7wzIh5Z3djSZ34Mh3AKfqSWC5dm2TAxQn3v9OoQTaqvkCNE/duf+trtBowLo3aSFjsFOm6EGb/+VsDKskAbERVK/a16hMlnjVKuQ94YWk8kbQ7xgYihx5TplFWKPf5qXy/RunUIj+VwF3jsTTQgWSYJzT2ePm+ldSfInym01dQ9PFxPY0sIozAaudZNNNB7Y9vNDaXraG43zW2hdWU+bEbRajcK2608LEi939LD/hluTU4j1rT5PH+2FXPi1b5OFwkCMTVzlg2OvCasJKJH0cF+fZq6ArdLvadkygQfhQIzNwlIGWPGsjRDS3t4jAHh8gdvx/ZEzs6vUm0hXyF4aTyTDqlpFrzjdmHRbl8fPV1xn229MOx26DguoEtBsXDF13/O9JG/q+Dxxj51bEeUEU88YsP25TasncQhWOviFf2LnPQINf3nbWzEzLWNyDnqa02ZLX8lbNHat1RrpOinu3qqbR7EKeDJ1U2YSeQql+enmKU52l1ICzt8USnfkdIfmDyMa9me+5LILw9St6m8VFVEvvkyylrz17pQ5FLXueGtJmS3zRLa3NNb5tYjyRG47ltsgeq5coYJ2VNNyt/5g3VKvGYpBG+OR1NLvNIzjBRjHRY/oBK8nHz8dGp/eKMXTjnLElnn0kGaqxH8/BG1/cE8/or2ZMkkleCn9rkw5RW1v4yEcUbE9/UjxJ7AK/Td2FsL3Mh4240vj4uok70pp8OIsRZ8NC8wcwSHk+pq9ibvjPrdP8o6UAdq36S113qkLq/JWdVchPpQpfllbwf9dpepJzAphQ7fwNZDFD/MiHui1Vrj0PGO1atZvU3G7sytQ6hNnU/Q9iaVLEf6CCqk7zQqf9MGUrwi9YqlwUUB72vx+LiAV0gMUuzBULOKvM4dheq9NJwR8J024YDQ1vZtmt1oOOdTr79MgR2uHb6Y4RZsWmJFjpYlSAu6VJh2zTdYYoDGo6qoN52C1SeSOPireXQ7lePpfqrqW8nhCNL1CcFTyAJNIrtxopjHms1qENPTzPgNFVT2WFKjIKFDcskBaxYym7HjsZvbvd5uLIzrXLfCEhHusWQv0tRHIuX7PDgVb0b67SZMU9ROxK7LPPbkezS3hOIqCePIDvUboFdkdduWJpRQLO6ZakVmjA48kT8iRNchWYR2ey2T0y4HrNGfsEHWNu3GrsfQAHl+CTX19J7ExtVXJPcFj4aeSF4qhOC5S8lYeKYQ0+Pu9+uTu8g/9/3tAI83t7WyY1BY3ySP1DtNyIylAiuWFH29uu2FR0Q4ieQOTeXacrShhryqnEpJ7clKoowaBzr65gB2zm6JOEdrSDCpHw+RP95F25Meximb5KQibf/VskZT96Yur6YMQYV1NMVw7h4XPq6TUCk/tWlj78pKKZZUSNsdeqobyJbKwiL/HqGcfgkXW5QcGBxNcZQLe6pB+mnqXU0dErT2IQnUvp/aKYNYrrCu5sNbusuFX+5WhXLMHRSNo0KXnqF3jeQD5uKL0zvwe+cIIgyHt45sxMj+QxFtH9Bh//omCZv3823OMzA2nusTjuTtFTAvljxclAlfLuFQWkOkjWneBBG764DhbS9opFROuXIcFU/LKBVOrZCQHMv5eYBulcdaKr2S0pg6ji6KaA0JdFBlT7frAvFAU3eZCGdPiIH2xO+zDnMWWDAg34MtXZy7cKcHRaNsSKXiNOtxGzJO01zBHOLCdH7x2kdEnUiH7rmnrJjeJl4ltMeVROS/HBCRms4hdZoNOYkCguXCkpOf2vD4hA7OqG+one5nxGQbchMEMgbqU52AjKPEsHW85Ak2bBwi4JKZ3kfQmtL1ePANb+ATrt7w5JbwiSgIelQhuIwady0W7fgdCisOBPQ9XnsKSzedRkVtq1XJSOFa/F9vo4Gk+PE8L8qJXQYirrwBDlqms17Auhz/pxnNBWX2+24cqYeiOHJ/v3KBDmj7R8PfN0oBKTqw6GhHcDHw/eVczw6NyHXnRFW1KwSUakXlgRKpnS1oHkfC1m/UL+yhHJL6dWduCc+sc2FPpU85LHGxBpXgooQTRzx4UVZd6vPCeupTrSr0iFg1w5w46GlR2f2Fbqw7KCrxiYsngtOJchLBV23gFUIWUs30iVZXRMdqT1a0tVk7iKE83h/2CUrGjaQDIRNccIrI+bjzBFeO/8QXnNL2FUGdvqC66SLm5C9DA+9nujDIHoXEW+Ko1tPjTEMFjtV+B53PAkvVQhibUhFEVvO9LCsVIJ0/VxkvNnbvhyHy/RGyvPESyq4wRFSSEYtjJfyxUEB1ow4/nWfFLFLSmqNuzP702v3K1yuge46ney6rk3o2jonGsaleoaxRumx85aLdSe0NlxsjVBUKv/jT4ViZacDX/+Kxl+q0pJFmrJpsgIEy18JXPFewIDQepeQm2s/KblCiyxk5whaG18dn46mvV+OSp77l+7POSuXlpw96N1xRbyDE8yCeHz+3SwTvkapfLvjtArd4mgnjSEJGDzXBSd7cYVJVreCfNxnBFTsh9c6/9SB/XebtYXzlMS4Efj3pAbmQ1tPLgDqyig67mtVPFfFXWTuN14NfXLvFuuHhicid/DJSI4ZdtW9s8K14JfNu3BHL3WCsILuy3o1C8p9uIridI3tyUcCGD5rwXh0Y+gAFn7uRc1BAjfbDmtvlwx6yJFk7fX06b5ftSnuUXDqJzSe34WT9WZQ7z8MjehFmcSAlLBETo+/CBHrpdd1T8O7aFQaGHtmV9kjuNwTJY4ewSDLcsGD/jScDIzkDAyM5AwMjOQMDIzkDAyM5AwMjOQMDIznD/zXJRcF9Qf6fUN1ouBHXxHDzQea3btjs9UvDh87I5ozWcBYShh8UwXlXTfW3n6/6nwADAND+0ihvbfm9AAAAAElFTkSuQmCC",
        type: "line",
        smooth: "true",
        symbol: "none",
        data: [5, 20, 36, 10, 10, 20, 30, 36, 10, 20, 36, 10].map((i) => i * 1),
        lineStyle: {
          width: 5,
          color: "#FCDE9C",
          shadowBlur: 1.5,
          shadowColor: "rgba(0, 0, 0, 0.12)",
          shadowOffsetY: 4,
        },
      },
      {
        name: "Current Value",
        type: "line",
        smooth: "true",
        symbol: "none",
        data: [5, 20, 36, 10, 10, 20, 30, 36, 10, 20, 36, 10].map((i) => i * 2),
        lineStyle: {
          width: 5,
          color: "#84AAD2",
          shadowBlur: 1.5,
          shadowColor: "rgba(0, 0, 0, 0.12)",
          shadowOffsetY: 4,
        },
      },
    ],
  };
  return (
    <Carousel  infiniteLoop={true} dynamicHeight={true}
    showThumbs={false}
    renderIndicator={(onClickHandler, isSelected, index, label) => {
      const defStyle = {
        marginLeft: 20,
        color: "white",
        cursor: "pointer",
        backgroundColor: "#F7B500",
        width: "100px",
        height: "8px",
        display: " inline-block",
      };
      const style = isSelected
        ? { ...defStyle, color: "red" }
        : { ...defStyle };
      return (
        <span
          style={style}
          onClick={onClickHandler}
          onKeyDown={onClickHandler}
          key={index}
          role="button"

        ></span>
      );
    }}>
        <div style={{ height:500 }} >
        <Card css={{ p: 15, mb: 20 }} >
            <Text weight="extrabold" size="h4">
              Investment Growth
            </Text>
            <Box>
              <ReactECharts
                option={InvestmentGrowth}
                style={{ height: 300 }}
                opts={{ renderer: "svg" }}
              />
            </Box>
          </Card>
        </div>
        <div>second</div>
        <div>third</div>
    </Carousel>
  );
}
export default CarouselPage;
