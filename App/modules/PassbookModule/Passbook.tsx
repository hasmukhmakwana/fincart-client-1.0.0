import React from 'react'
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import Layout from "App/shared/components/Layout";
import PassbookPage from './components/PassbookTransaction';
const Passbook = () => {
    return (
        <>
            <Layout>
                <PageHeader title="Transactions Statement" rightContent={<></>} />
                <PassbookPage />
            </Layout>
        </>
    )
}

export default Passbook