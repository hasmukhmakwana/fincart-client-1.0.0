import create from "zustand";

export type InvestorTypes = {
    Text: string;
    Value: string;
    Category: string;
}
interface StoreTypes {
    localUser: string;
    localPlanId: string;
    loading: boolean;
    investorList: InvestorTypes[];
    setLocalUser: (payload: string) => void;
    setLocalPlanId: (payload: string) => void;
    setLoader: (payload: boolean) => void;
    setInvestorList: (payload: InvestorTypes[]) => void;
}

const usePassbookStore = create<StoreTypes>((set) => ({
    localUser: "",
    localPlanId: "",
    loading: false,
    investorList: [],
    setLoader: (payload) =>
        set((state) => ({
            ...state,
            loading: payload,
        })),

    setLocalUser: (payload) =>
        set((state) => ({
            ...state,
            localUser: payload,
        })),

    setLocalPlanId: (payload) =>
        set((state) => ({
            ...state,
            localPlanId: payload,
        })),

    setInvestorList: (payload) =>
        set((state) => ({
            ...state,
            investorList: payload,
        })),
}))

export default usePassbookStore;