import React, { useState, useEffect } from 'react'
import Box from "@ui/Box/Box";
import SelectMenu from "@ui/Select/Select";
import Input from "@ui/Input";
import { GroupBox } from "@ui/Group/Group.styles";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import styles from "App/modules/PassbookModule/Passbook.module.scss"
import Search from 'App/icons/Search';
import Pill from "@ui/Pill/Pill";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { color } from 'echarts';
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import { toastAlert } from 'App/shared/components/Layout'
import usePassbookStore from '../store';
import { encryptData, getUser } from "App/utils/helpers";
import { searchPassbookTransactions, getInvestorDD } from "App/api/passbook";
import Fuse from 'fuse.js';
import Loader from "App/shared/components/Loader";
import MutualFund from 'pages/Products/MutualFund';
import Card from '@ui/Card/Card';
let recentTabs = [
    {
        title: "MutualFund",
        value: "MutualFund",
    },
    {
        title: "Other",
        value: "Other"
    }

]
const TransactionType = {
    MutualFund: [{ Id: "1", TransactionType: "Scheme Name" },
    { Id: "2", TransactionType: "Folio No." },
    { Id: "3", TransactionType: "Transaction Type" },
    { Id: "4", TransactionType: "Transaction SubType" }],

    Others: [{ Id: "1", TransactionType: "Scheme Name" },
    { Id: "2", TransactionType: "Asset Type" },
    { Id: "3", TransactionType: "Transaction Type" },
    { Id: "4", TransactionType: "Investment ID" }]
}
const PassbookPage = () => {
    const local = getUser();
    const [currentTab, setCurrentTab] = useState<any>("MutualFund");
    const [dropdownValue, setDropdownValue] = useState<any>(TransactionType.MutualFund);

    const [transactions, setTransactions] = useState<any>([]);
    const [sortedtransactions, setSortedTransactions] = useState<any>([]);
    const [loaderApi, setLoaderApi] = useState(false);
    const [fromDate, setFromDate] = useState();
    const [toDate, setToDate] = useState();
    const [openTransactionType, setOpenTransactionType] = useState<any>("1");
    const [query, updateQuery] = useState("");
    const [openInvestor, setOpenInvestor] = useState<any>(local?.basicid);
    const [investor, setInvestor] = useState<any>([]);
    const {
        loading,
        localUser,
        localPlanId,
        investorList,
        setInvestorList,
        setLoader,
    } = usePassbookStore();
    const SearchTransactions = async () => {
        try {
            setLoaderApi(true);
            let obj: any = {
                ID: "71",
                from_date: fromDate,//"2021-11-14",
                to_date: toDate,//"2022-10-16",
                offset_value: "0",
                page_size: "52"
            }
            const enc: any = encryptData(obj);
            console.log(obj);
            console.log(enc);
            let result = await searchPassbookTransactions(enc);
            setTransactions(result?.data?.Transactions);
            setSortedTransactions(result?.data?.Transactions);
            console.log(result?.data?.Transactions);
            setLoaderApi(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
        }
    }

    const fetchMemberList = async () => {
        try {
            // const enc: any = encryptData({
            //   basicId : local.basicid,
            //     fundid: "0",
            //   });
            //   console.log(enc);
            setLoaderApi(true);
            let obj = {
                basicId: local.basicid,
                fundid: "0",
            }
            const enc: any = encryptData(obj);
            let result = await getInvestorDD(enc);
            console.log("Investor");
            console.log(result?.data);
            setInvestor(result?.data?.memberList);
            setLoaderApi(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
            setLoaderApi(false);
        }
    };

    const search = async () => {
        const fuse = new Fuse(transactions, openTransactionType === "1" ?
            {
                keys: ['SchemeName'],
                includeScore: true
            } : {
                keys: ['Id'],
                includeScore: true
            });
        const results = fuse?.search(query);
        const characterResults = results.map(character => character.item);
        console.log(characterResults?.filter((i: any) => i?.Id.includes(query)), "filtersum")
        let sortedData: any = 0;
        if (openTransactionType === "1") {
            sortedData = characterResults?.filter((i: any) => i?.SchemeName.includes(query));
        }
        else {
            sortedData = characterResults?.filter((i: any) => i?.Id.includes(query));
        }
        if (characterResults.length === 0 && query === "") {
            setSortedTransactions(transactions);
        } else {
            setSortedTransactions(sortedData);
        }
    }

    // async function onSearch(key: any, keyword: any) {
    //     const Fuse = (await import("fuse.js")).default;
    //     const fuse = new Fuse(openTransactionType, {
    //         keys: [openTransactionType],
    //         includeScore: true,
    //     });
    //     return fuse.search(keyword);
    // }
    useEffect(() => {
        (async () => {
            await fetchMemberList();
        })()
        // let obj = TransactionType.MutualFund;
        // if (dropdownValue !== TransactionType.MutualFund) {
        //     obj = {
        //         ...obj,
        //         ...TransactionType.Others
        //     }
        // }
        // //console.log(obj)
        // setDropdownValue(obj)
        return () => {
        }
    }, [local])

    useEffect(() => {
        if(currentTab === "MutualFund"){
            setDropdownValue(TransactionType.MutualFund)
        }else{
setDropdownValue(TransactionType.Others);
        }
        // SearchTransactions();
        // console.log(transactions);
        return () => { }
    }, [currentTab])

    return (
        <>
            <Box className="container">
                <Box className="row p-1" css={{ backgroundColor: "#b3daff" }}>
                    <Box className="col-lg-5">
                        <SelectMenu
                            value={openInvestor}
                            items={investor}
                            label="Investor"
                            bindValue={"basicid"}
                            bindName={"memberName"}
                            onChange={(e: any) => {
                                console.log(investor);
                                setOpenInvestor(e?.basicid || "")
                            }
                            }
                        />
                    </Box>
                    <Box className="col-lg-3">
                        <Input
                            type="date"
                            label="From Date"
                            name="InvestDate"
                            color="white"
                            onChange={(e: any) => {
                                console.log(e?.target.value),
                                    setFromDate(e?.target.value)
                            }}
                        />
                    </Box>
                    <Box className="col-lg-3">
                        <Input
                            type="date"
                            label="To Date"
                            name="InvestDate"
                            color="white"
                            onChange={(e: any) => {
                                console.log(e?.target.value),
                                    setToDate(e?.target.value)
                            }}
                        />
                    </Box>
                    <Box className="col-lg-1 mt-4 text-end">
                        <Button
                            className={`col-auto mb-2 py-1`}
                            color="yellowGroup"
                            size="md"
                        >
                            <Box>
                                <Text
                                    //@ts-ignore 
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                    className={`${styles.button}`}
                                    onClick={() => { SearchTransactions() }}
                                >
                                    Search
                                </Text>
                            </Box>
                        </Button>
                    </Box>
                </Box>
                <Box className=" row justify-content-end  p-0">
                    <Box className="col-auto col-lg-auto ">
                        {/* <Box className="row justify-content-end ">
                            {/* <Box className="col-auto">
                                <Box className="input-group ">
                                    <Input type="text" className="form-control"
                                        placeholder="Recipient's username"
                                        aria-label="Recipient's username"
                                        aria-describedby="basic-addon2" />
                                    <Box className="input-group-append" css={{ border: "0px 0px 10px 10px !important" }}>
                                        <Button className="btn" type="button">Button</Button>
                                    </Box>

                                </Box>
                            </Box> 
                            <Box className="col-auto col-lg-auto col-sm-auto px-0 mt-1">
                                <Input
                                    type="text"
                                    //label="From Date"
                                    name="InvestDate"
                                    color="white"
                                    placeholder="Search"
                                />
                            </Box>
                            <Box className="col-auto col-lg-auto col-sm-auto mt-1">
                                <Button
                                    className="col-auto "
                                    color="yellowGroup"
                                    size="md"
                                >
                                    <Box>
                                        <Search></Search>
                                    </Box>
                                </Button>
                            </Box>
                        </Box> */}
                    </Box>
                </Box>
                <Box className="row  mt-2">
                    <Box className="col-12 ">
                        <StyledTabs defaultValue="MutualFund">
                            <Box className="row my-0 py-0">
                                <Box className="col-auto col-lg-auto">
                                    <TabsList
                                        aria-label="Manage your account"
                                        className="justify-content-left mt-3"
                                        css={{
                                            borderBottom: "1px solid var(--colors-gray7)",
                                            overflowX: "auto",
                                        }}
                                    >
                                        {recentTabs.map((item, index) => {
                                            return (
                                                <TabsTrigger
                                                    value={item.value}
                                                    className="tabs me-3"
                                                    key={item.value}
                                                    css={{ border: "solid 1px var(--colors-blue1)", height: "25px" }}
                                                    onClick={()=>{
                                                        console.log(item.value);
                                                        setCurrentTab(item.value)}}
                                                    
                                                >
                                                    {/* @ts-ignore */}
                                                    <Text weight="bold" size="h5">
                                                        {item.title}
                                                    </Text>
                                                </TabsTrigger>
                                            );
                                        })}
                                    </TabsList>
                                </Box>
                            </Box>
                            <Box className="row mt-2">
                                <Box className="col-lg-2 my-0">
                                    <SelectMenu
                                        //label={"Select Dividend Option"}
                                        name={"SearchTransOptions"}
                                        value={openTransactionType}
                                        items={dropdownValue}
                                        // placeholder="Select Transaction Type"
                                        bindValue={"Id"}
                                        bindName={"TransactionType"}
                                        onChange={(e: any) => {
                                            setOpenTransactionType(e?.Id || "")
                                            console.log(e?.Id)
                                        }}
                                    // setFilters({ key: "riskReturnOptions", value: e?.id || "" })
                                    //isClearable
                                    />
                                </Box>
                                <Box className="col-auto col-lg-auto col-sm-auto px-0 mb-0  ">
                                    <Box css={{ marginBottom: "0 !important" }} >
                                        <Input
                                            className="my-0 mb-0"
                                            type="text"
                                            //label="From Date"
                                            value={query}
                                            name="search"
                                            color="white"
                                            placeholder="Search"
                                            onChange={(e: any) => updateQuery(e?.target.value)} />
                                    </Box>
                                </Box>
                                <Box className="col-auto col-lg-auto col-sm-auto ">
                                    {/* <Box css={{marginBottom:"0 !important"}}> */}
                                    <Button
                                        className="col-auto my-0 "
                                        color="yellowGroup"
                                        size="md" onClick={search} >
                                        <Box>
                                            <Search></Search>
                                        </Box>
                                    </Button>
                                </Box>
                            </Box>
                            <TabsContent value="MutualFund" >
                                <Box css={{ overflow: "auto" }}>
                                    <Table>
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore
                                            className="text-capitalize">
                                            <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                <Column className="col-lg-2">Scheme</Column>
                                                <Column>Amount</Column>
                                                <Column>Status</Column>
                                                <Column>Type</Column>
                                                <Column>Date</Column>
                                                <Column>Folio</Column>
                                                <Column>Units</Column>
                                                <Column>Nav</Column>
                                                <Column>Goal</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        <DataParent>
                                            {sortedtransactions.map((item: any, index: number) => {
                                                return (
                                                    <DataRow>
                                                        <DataCell>{item?.SchemeName}</DataCell>
                                                        <DataCell className="text-center">&#x20B9;{Number(item?.Amount).toLocaleString("en-IN")}</DataCell>
                                                        {item?.status === "1" ?
                                                            <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                                Success</DataCell>
                                                            : ""}
                                                        <DataCell className="text-center">{item?.Trxn_sub_type}
                                                            {item?.Trxn_type === "Credit" ? <><br /><span className='text-success'>(+) Credit</span></> : <><br /><span className='text-danger'>(-) Debit</span></>}
                                                        </DataCell>
                                                        <DataCell className="text-center">{item?.trxnDate.trim().split(' ')[0]}</DataCell>
                                                        <DataCell className="text-center">{item?.Id}</DataCell>
                                                        <DataCell className="text-center">{item?.Units}</DataCell>
                                                        <DataCell className="text-center">{item?.NAV}</DataCell>
                                                        <DataCell className="text-center"></DataCell>
                                                    </DataRow>
                                                )
                                            })}


                                            {/* <DataRow>
                                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "orange", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>In-Process</DataCell>
                                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">102</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                            <DataRow>
                                                <DataCell>ICICI Pru - Liquid Unclaimed Dividend Transitory Scheme Direct</DataCell>
                                                <DataCell className="text-center">&#x20B9;0</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Failed</DataCell>
                                                <DataCell className="text-center">Redemption <br /><span className='text-danger'>(-) Debit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">102</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                            <DataRow>
                                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">163</DataCell>
                                                <DataCell className="text-center">
                                                    <img
                                                        width={50}
                                                        height={50}
                                                        src={"/EducationGoal_img.png"}
                                                        alt="Child Study"
                                                    /></DataCell>
                                            </DataRow>
                                            <DataRow>
                                                <DataCell>ICICI Pru - Liquid Unclaimed Dividend Transitory Scheme Direct</DataCell>
                                                <DataCell className="text-center">&#x20B9;0</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                                <DataCell className="text-center">Redemption <br /><span className='text-danger'>(-) Debit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">102</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                            <DataRow>
                                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">163</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow> */}
                                        </DataParent>
                                    </Table>
                                </Box>
                            </TabsContent>
                            <TabsContent value="Other" >
                                <Box css={{ overflow: "auto" }}>
                                    <Table>
                                        <ColumnParent
                                            className="border border-bottom"
                                            //@ts-ignore
                                            className="text-capitalize"
                                        >
                                            <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                                <Column className='col-lg-2'>Scheme</Column>
                                                <Column>Amount</Column>
                                                <Column>Status</Column>
                                                <Column>Type</Column>
                                                <Column>Date</Column>
                                                <Column>Folio</Column>
                                                <Column>Units</Column>
                                                <Column>Nav</Column>
                                                <Column>Goal</Column>
                                            </ColumnRow>
                                        </ColumnParent>
                                        <DataParent>

                                            <DataRow >

                                                <DataCell>Can Robeco-Emerging equities Reg (G)</DataCell>
                                                <DataCell className="text-center">&#x20B9;5,000</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                                    Success</DataCell>
                                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                                <DataCell className="text-center">16 Aug 2022</DataCell>
                                                <DataCell className="text-center">19915061774</DataCell>
                                                <DataCell className="text-center">202.651</DataCell>
                                                <DataCell className="text-center">118</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>

                                            <DataRow>
                                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "orange", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>In-Process</DataCell>
                                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">102</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                            <DataRow>
                                                <DataCell>ICICI Pru - Liquid Unclaimed Dividend Transitory Scheme Direct</DataCell>
                                                <DataCell className="text-center">&#x20B9;0</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Failed</DataCell>
                                                <DataCell className="text-center">Redemption <br /><span className='text-danger'>(-) Debit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">102</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                            <DataRow>
                                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">163</DataCell>
                                                <DataCell className="text-center">
                                                    <img
                                                        width={50}
                                                        height={50}
                                                        src={"/EducationGoal_img.png"}
                                                        alt="Child Study"
                                                    /></DataCell>
                                            </DataRow>
                                            <DataRow>
                                                <DataCell>ICICI Pru - Liquid Unclaimed Dividend Transitory Scheme Direct</DataCell>
                                                <DataCell className="text-center">&#x20B9;0</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                                <DataCell className="text-center">Redemption <br /><span className='text-danger'>(-) Debit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">102</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                            <DataRow>
                                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                                <DataCell className="text-center">13783804/76</DataCell>
                                                <DataCell className="text-center">0.003</DataCell>
                                                <DataCell className="text-center">163</DataCell>
                                                <DataCell className="text-center"></DataCell>
                                            </DataRow>
                                        </DataParent>
                                    </Table>
                                </Box>
                            </TabsContent>
                        </StyledTabs>
                    </Box>

                    {/* <Box css={{ overflow: "auto" }}>
                    <Table>
                        <ColumnParent
                            className="border border-bottom"
                            //@ts-ignore
                            className="text-capitalize"
                        >
                            <ColumnRow css={{ color: "var(--colors-blue1)", textAlign: "center" }}>
                                <Column className='col-lg-2'>Scheme</Column>
                                <Column>Amount</Column>
                                <Column>Status</Column>
                                <Column>Type</Column>
                                <Column>Date</Column>
                                <Column>Folio</Column>
                                <Column>Units</Column>
                                <Column>Nav</Column>
                                <Column>Goal</Column>
                            </ColumnRow>
                        </ColumnParent>
                        <DataParent>
                            
                            <DataRow>
                                <DataCell>Can Robeco-Emerging equities Reg (G)</DataCell>
                                <DataCell className="text-center">&#x20B9;5,000</DataCell>
                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>
                                    Success</DataCell>
                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                <DataCell className="text-center">16 Aug 2022</DataCell>
                                <DataCell className="text-center">19915061774</DataCell>
                                <DataCell className="text-center">202.651</DataCell>
                                <DataCell className="text-center">118</DataCell>
                                <DataCell className="text-center"></DataCell>
                            </DataRow>
                            <DataRow>
                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "orange", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>In-Process</DataCell>
                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                <DataCell className="text-center">13783804/76</DataCell>
                                <DataCell className="text-center">0.003</DataCell>
                                <DataCell className="text-center">102</DataCell>
                                <DataCell className="text-center"></DataCell>
                            </DataRow>
                            <DataRow>
                                <DataCell>ICICI Pru - Liquid Unclaimed Dividend Transitory Scheme Direct</DataCell>
                                <DataCell className="text-center">&#x20B9;0</DataCell>
                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Failed</DataCell>
                                <DataCell className="text-center">Redemption <br /><span className='text-danger'>(-) Debit</span></DataCell>
                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                <DataCell className="text-center">13783804/76</DataCell>
                                <DataCell className="text-center">0.003</DataCell>
                                <DataCell className="text-center">102</DataCell>
                                <DataCell className="text-center"></DataCell>
                            </DataRow>
                            <DataRow>
                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                <DataCell className="text-center">13783804/76</DataCell>
                                <DataCell className="text-center">0.003</DataCell>
                                <DataCell className="text-center">163</DataCell>
                                <DataCell className="text-center">
                                    <img
                                        width={50}
                                        height={50}
                                        src={"/EducationGoal_img.png"}
                                        alt="Child Study"
                                    /></DataCell>
                            </DataRow>
                            <DataRow>
                                <DataCell>ICICI Pru - Liquid Unclaimed Dividend Transitory Scheme Direct</DataCell>
                                <DataCell className="text-center">&#x20B9;0</DataCell>
                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                <DataCell className="text-center">Redemption <br /><span className='text-danger'>(-) Debit</span></DataCell>
                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                <DataCell className="text-center">13783804/76</DataCell>
                                <DataCell className="text-center">0.003</DataCell>
                                <DataCell className="text-center">102</DataCell>
                                <DataCell className="text-center"></DataCell>
                            </DataRow>
                            <DataRow>
                                <DataCell>ICICI Pru - India Opportunities Fund (G)</DataCell>
                                <DataCell className="text-center">&#x20B9;10,000</DataCell>
                                <DataCell className="text-center"><Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto"></Box>Success</DataCell>
                                <DataCell className="text-center">SIP <br /><span className='text-success'>(+) Credit</span></DataCell>
                                <DataCell className="text-center">12 Aug 2022</DataCell>
                                <DataCell className="text-center">13783804/76</DataCell>
                                <DataCell className="text-center">0.003</DataCell>
                                <DataCell className="text-center">163</DataCell>
                                <DataCell className="text-center"></DataCell>
                            </DataRow>
                        </DataParent>
                    </Table>
                </Box>  */}
                </Box>

            </Box>
        </>
    )
}

export default PassbookPage