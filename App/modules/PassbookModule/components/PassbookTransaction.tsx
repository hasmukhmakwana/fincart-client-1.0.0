import React, { useState, useEffect } from "react"
import Box from "@ui/Box/Box";
import SelectMenu from "@ui/Select/Select";
import DialogModal from '@ui/ModalDialog/ModalDialog';
import Input from "@ui/Input";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import { WEB_URL } from "App/utils/constants";
import styles from "App/modules/PassbookModule/Passbook.module.scss"
import Search from 'App/icons/Search';
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { toastAlert } from 'App/shared/components/Layout'
import { encryptData, getUser } from "App/utils/helpers";
import { searchPassbookTransactions, getInvestorDD } from "App/api/passbook";
import Fuse from 'fuse.js';
import Group from "@ui/Group/Group";
import Pagination from "@ui/DataGrid/components/Pagination";
import Loader from "App/shared/components/Loader";

const PassbookTransaction = () => {
    const local = getUser();
    const [transactions, setTransactions] = useState<any>([]);
    const [sortedtransactions, setSortedTransactions] = useState<any>([]);
    const [loaderApi, setLoaderApi] = useState(false);
    const [loader, setLoader] = useState(false);
    const [fromDate, setFromDate] = useState();
    const [toDate, setToDate] = useState();
    const [query, updateQuery] = useState("");
    const [queryType, setQueryType] = useState<any>("01");
    const [queryVal, setQueryVal] = useState<any>("");
    const [searchTxt, setSearchTxt] = useState<any>("");
    const [openInvestor, setOpenInvestor] = useState<any>(local?.basicid);
    const [open, setOpen] = useState(false);
    const [productType, setProductType] = useState<any>("MF");
    const [productTitle, setProductTitle] = useState<any>("MF");
    const [investor, setInvestor] = useState<any>([]);
    const [totalCount, setTotalCount] = useState(0);
    const [limit, setLimit] = useState(0);
    const [txnLen, setTxnLen] = useState(0);
    // const [showPagination, setShowPagination] = useState<Boolean>(true);
    const [isSearch, setIsSearch] = useState(false);
    const PageLimit = 15;

    const [infoData, setinfoData] = useState<any>([
        {
            goalCode: "FG1",
            icon: 'motorcycle.png',
            gName: "Bike",
        },
        {
            goalCode: "FG2",
            icon: 'FontAwsome (chart-line).png',
            gName: "Business",
        },
        {
            goalCode: "FG3",
            icon: 'FontAwsome (car).png',
            gName: "Car",
        },
        {
            goalCode: "FG4",
            icon: 'FontAwsome (home).png',
            gName: "House",
        },
        {
            goalCode: "FG5",
            icon: 'FontAwsome (book-reader).png',
            gName: "Study",
        },
        {
            goalCode: "FG6",
            icon: 'FontAwsome (umbrella-beach).png',
            gName: "Travel",
        },
        {
            goalCode: "FG7",
            icon: 'NPS.png',
            gName: "Wedding",
        },
        {
            goalCode: "FG8",
            icon: 'FontAwsome (coins).png',
            gName: "Wealth",
        },
        {
            goalCode: "FG9",
            icon: 'FontAwsome (wheelchair).png',
            gName: "Retirement",
        },
        {
            goalCode: "FG10",
            icon: 'FontAwsome (umbrella-beach).png',
            gName: "Sabbatical",
        },
        {
            goalCode: "FG11",
            icon: 'ChildSecurity.png',
            gName: "Family Planning",
        },
        {
            goalCode: "FG12",
            icon: 'FontAwsome (book-reader).png',
            gName: "Child Study",
        },
        {
            goalCode: "FG13",
            icon: 'NPS.png',
            gName: "Child Wedding",
        },
        {
            goalCode: "FG14",
            icon: 'Other.png',
            gName: "Other",
        },
        {
            goalCode: "FG17",
            icon: 'EmergencyFund.png',
            gName: "Emergency",
        },
        {
            goalCode: "FG23",
            icon: 'BankMandates.png',
            gName: "Term Insurance",
        },
        {
            goalCode: "FG24",
            icon: 'insurance.png',
            gName: "Health Insurance",
        },
    ]);

    const [searchType, setSeachType] = useState<any>([
        {
            code: "01",
            textType: "Scheme Name",
        },
        {
            code: "02",
            textType: "Status",
        },
        {
            code: "03",
            textType: "Transaction Type",
        },
        {
            code: "04",
            textType: "Transaction Sub Type",
        }
    ]);

    const [type, setType] = useState<any>([
        {
            code: "011",
            textType: "Credit",
        },
        {
            code: "012",
            textType: "Debit",
        }
    ]);

    const [subType, setSubType] = useState<any>([
        {
            code: "r",
            textType: "Redeem",
        },
        {
            code: "sip",
            textType: "SIP",
        },
        {
            code: "nor",
            textType: "Lumpsum",
        },
        {
            code: "switch",
            textType: "SWITCH",
        },
        {
            code: "Stp",
            textType: "STP",
        },
        {
            code: "swp",
            textType: "SWP",
        },
    ]);

    const [statusType, setStatusType] = useState<any>([
        {
            code: "1",
            textType: "Success",
        },
        {
            code: "2",
            textType: "In Process",
        },
        {
            code: "-1",
            textType: "Failed",
        }
    ]);

    const fetchMemberList = async () => {
        try {
            setLoaderApi(true);
            // let obj = {
            //     basicId: local?.basicid,
            //     fundid: "0",
            // }
            let obj = local?.basicid;
            const enc: any = encryptData(obj, true);
            let result = await getInvestorDD(enc);
            //console.log("Investor");
            // console.log(result?.data);
            setInvestor(result?.data?.memberList);
            setLoaderApi(false);
        } catch (error: any) {
            console.log(error);
            toastAlert("error", error);
            setLoaderApi(false);
        }
    };
    const today = new Date();
    const prevDate = today.getDate() - 30;
    today.setDate(prevDate);
    const prevMonth = today.toLocaleDateString('en-CA');

    const todayDate = new Date();
    const yesterdayDate = todayDate.getDate();
    todayDate.setDate(yesterdayDate);
    const yesterday = todayDate.toLocaleDateString('en-CA');

    const todayPost = new Date();
    const postDate = todayPost.getDate() + 30;
    todayPost.setDate(postDate);
    const postMonth = todayPost.toLocaleDateString('en-CA');


    const SearchTransactions = async () => {
        setSearchTxt("");
        try {
            setLoader(true);
            setTxnLen(0);
            let obj: any = {
                ID: openInvestor,//"700538",
                from_date: fromDate || prevMonth,//"2021-11-14",
                to_date: toDate || yesterday,//"2022-10-16",
                offset_value: limit,//current page numbers
                page_size: PageLimit,//Total No. of records
                assettype: productType
            }
            const enc: any = encryptData(obj);
            setProductTitle(productType);
            // console.log(obj);
            // console.log(enc);
            let result = await searchPassbookTransactions(enc);
            setTransactions(result?.data?.Transactions);
            setSortedTransactions(result?.data?.Transactions);
            //setShowPagination(true);
            //@ts-ignore
            setTotalCount(result?.msg);
            setTxnLen(result?.data?.Transactions.length);
            // console.log("Limit", limit);
            // console.log(result?.data?.Transactions);
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            {
                error?.data === 'No data found!' ? <>
                    {setSortedTransactions([])}
                    {setTransactions([])}
                    {setTotalCount(0)}
                </>
                    :
                    toastAlert("error", error)
            }
            // setSortedTransactions(null);
            setLoader(false);
            //setShowPagination(false);
        }
    }

    const searchtxt = async () => {
        try {
            setLoader(true);
            setTxnLen(0);

            let obj: any = {
                ID: openInvestor,//"700538",
                from_date: fromDate || prevMonth,//"2021-11-14",
                to_date: toDate || yesterday,//"2022-10-16",
                offset_value: limit,//current page numbers
                page_size: PageLimit,//Total No. of records
                assettype: productType,
                Investment_ID: ""
            }

            switch (queryType) {
                case "01":
                    obj = {
                        ...obj,
                        SchemeName: query
                    }
                    setSearchTxt("Scheme Name");
                    break;

                case "02":
                    obj = {
                        ...obj,
                        trxn_status: query,
                    }
                    setSearchTxt("Status");
                    break;
                case "03":
                    obj = {
                        ...obj,
                        Trxn_type: query,
                    }
                    setSearchTxt("Transaction Type");
                    break;
                case "04":
                    obj = {
                        ...obj,
                        Trxn_sub_type: query,
                    }
                    setSearchTxt("Transaction Sub-Type");
                    break;
            }
            const enc: any = encryptData(obj);
            setProductTitle(productType);
            // console.log(obj);
            // setLoader(false);
            // // console.log(enc);
            // return;
            let result = await searchPassbookTransactions(enc);
            setIsSearch(true)
            setTransactions(result?.data?.Transactions);
            setSortedTransactions(result?.data?.Transactions);
            //setShowPagination(true);
            //@ts-ignore
            setTotalCount(result?.msg);
            setTxnLen(result?.data?.Transactions.length);
            // console.log("Limit", limit);
            // console.log(result?.data?.Transactions);
            setLoader(false);
        } catch (error: any) {
            console.log(error);
            {
                error?.data === 'No data found!' ? <>
                    {setSortedTransactions([])}
                    {setTransactions([])}
                    {setTotalCount(0)}
                </>
                    : error?.msg === 'Scheme name length must be between 0-3!' ? <>
                        {toastAlert("warn", "Space is 'Not allowed' Min. 5 character")}
                    </>
                        : toastAlert("error", error)
            }
            // setSortedTransactions(null);
            setLoader(false);
            //setShowPagination(false);
        }
    }

    useEffect(() => {
        // (async () =>  await fetchMemberList())
        fetchMemberList();
        return () => { }
    }, [])
    useEffect(() => {
        // console.log(query.length);

        if (limit >= 0 && !isSearch) {
            SearchTransactions();
            updateQuery("");
            setIsSearch(false);
        }
        else {
            searchtxt();
        }
        return () => {
        }
    }, [limit])

    return (
        <>
            <Box className="container">
                <Box className="row p-1" css={{ backgroundColor: "#b3daff", borderRadius: "8px" }}>
                    <Box className="col-lg-2">
                        <SelectMenu
                            value={productType}
                            items={[{ Id: "MF", ProductType: "Mutual Fund" },
                            { Id: "PMS", ProductType: "PMS" },
                            { Id: "ULIP", ProductType: "ULIP" },
                            { Id: "Protection", ProductType: "Protection" },
                            { Id: "Other", ProductType: "Other" },
                            { Id: "Liquiloan", ProductType: "Liquiloan" },]}
                            label="Products"
                            bindValue={"Id"}
                            bindName={"ProductType"}
                            onChange={(e: any) => {
                                // console.log(e?.Id);
                                setProductType(e?.Id || "")
                            }}
                        />
                    </Box>
                    <Box className="col-lg-3">
                        <SelectMenu
                            value={openInvestor}
                            items={investor}
                            label="Investor"
                            bindValue={"basicid"}
                            bindName={"memberName"}
                            onChange={(e: any) => {
                                // console.log(investor);
                                setOpenInvestor(e?.basicid || "")
                            }} />
                    </Box>
                    <Box className="col-lg-3">
                        <Input
                            type="date"
                            label="From Date"
                            name="InvestDate"
                            color="white"
                            defaultValue={prevMonth}
                            max={yesterday}
                            onChange={(e: any) => {
                                // console.log(e?.target.value),
                                setFromDate(e?.target.value)
                            }}
                            className="mb-0"
                            note="MM/DD/YYYY"
                        />
                    </Box>
                    <Box className="col-lg-3">
                        <Input
                            type="date"
                            label="To Date"
                            name="InvestDate"
                            color="white"
                            min={fromDate}
                            defaultValue={yesterday}
                            max={postMonth}
                            onChange={(e: any) => {
                                // console.log(e?.target.value),
                                setToDate(e?.target.value)
                            }}
                            note="MM/DD/YYYY"
                        />
                    </Box>
                    <Box className="col-lg-1 mt-4 text-end">
                        <Button
                            className={`col-auto mb-2 py-1`}
                            color="yellowGroup"
                            size="md"
                            onClick={() => {
                                SearchTransactions();
                                setLimit(0);
                                setQueryVal("");
                                updateQuery("");
                                setIsSearch(false);
                                setQueryType("01");
                            }}
                        >
                            <Box>
                                <Text
                                    //@ts-ignore 
                                    size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                    className={`${styles.button}`}
                                >
                                    Search
                                </Text>
                            </Box>
                        </Button>
                    </Box>
                </Box>
                <Box className="row mt-2 justify-content-between">
                    <Box className="col-auto col-lg-auto col-sm-auto mt-3 ms-1">
                        <Text size="h4" color="blue1">{productTitle === "MF" ? "MutualFund" : productTitle}</Text>
                    </Box>
                    {isSearch ?
                        <Box className="col-auto col-lg-auto col-sm-auto mt-3 ">
                            <Text size="h6">Search Result by {searchTxt}</Text>
                        </Box>
                        : null}
                    <Box className="col-auto col-lg-auto col-sm-auto">
                        <Box className="row mt-2 justify-content-evenly">
                            <Box className="col-auto col-lg-auto col-sm-auto px-0 mb-0">
                                {/* {queryType === "01" || queryType === "04" ? <> */}
                                {query.length > 0 ?
                                    <Button
                                        name="clear"
                                        className="col-auto my-0 "
                                        color="yellowGroup"
                                        size="sm"
                                        css={{ background: "transparent" }}
                                        onClick={() => {
                                            updateQuery("");
                                            SearchTransactions();
                                            setIsSearch(false);
                                            setQueryType("01");
                                        }}
                                    >
                                        <Box>
                                            <Text
                                                size="h6"
                                                className="my-2"
                                                css={{}}
                                            >Clear X</Text>
                                        </Box>
                                    </Button>
                                    : <></>}
                                {/* </> : <></>} */}
                            </Box>
                            <Box className="col-auto col-lg-auto col-sm-auto px-1 mb-0">
                                <Box css={{ marginBottom: "0 !important" }} >
                                    <SelectMenu
                                        className="my-0 pb-2"
                                        // label="Subject"
                                        name={"TicketSubject"}
                                        value={queryType}
                                        items={searchType}
                                        onChange={(e: any) => {
                                            // console.log(e?.code);
                                            setQueryType(e?.code);
                                            if (e?.code === "02") {
                                                setQueryVal("1");
                                                updateQuery("1");
                                            } else if (e?.code === "03") {
                                                setQueryVal("011");
                                                updateQuery("Credit");
                                            } else if (e?.code === "04") {
                                                setQueryVal("r");
                                                updateQuery("r");
                                            } else {
                                                setQueryVal("");
                                                updateQuery("");
                                            }
                                        }}
                                        bindValue={"code"}
                                        bindName={"textType"}
                                    />
                                </Box>
                            </Box>
                            <Box className="col-auto col-lg-auto col-sm-auto px-0 mb-0">
                                <Box css={{ marginBottom: "0 !important" }} >
                                    {queryType === "01" ? <>
                                        <Input
                                            className="my-0 py-0"
                                            type="text"
                                            note="Min. 4 characters"
                                            value={query}
                                            name="search"
                                            color="white"
                                            placeholder="Scheme Search"
                                            onChange={(e: any) => updateQuery(e?.target?.value.length === 1 && e?.target?.value === " " ? "" : e?.target.value)}
                                        />
                                    </> : <>
                                        <SelectMenu
                                            className="my-0 mb-0"
                                            // label="Subject"
                                            name={"TicketSubject"}
                                            value={queryVal}
                                            items={queryType === "02" ? statusType : queryType === "03" ? type : subType}
                                            onChange={(e: any) => {
                                                // console.log(e?.code)
                                                setQueryVal(e?.code);
                                                updateQuery(queryType !== "03" ? e?.code : e?.textType);
                                            }}
                                            bindValue={"code"}
                                            bindName={"textType"}
                                        />
                                    </>}
                                </Box>
                            </Box>
                            <Box className="col-auto col-lg-auto col-sm-auto ">
                                {/* <Box css={{marginBottom:"0 !important"}}> */}
                                <Button
                                    className="col-auto my-0 "
                                    color="yellowGroup"
                                    size="sm"
                                    onClick={() => { setLimit(0); searchtxt(); }}
                                    disabled={queryType === "01" ? query.length < 4 : false}
                                >
                                    <Box>
                                        <Search />
                                    </Box>
                                </Button>
                            </Box>
                        </Box>
                    </Box>
                </Box>
                <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                    <Box css={{ overflow: "auto", maxHeight: "400px" }}>
                        <Table>
                            <ColumnParent css={{ position: "sticky", top: "0" }}
                                className="border border-bottom borderRadiusForTable"
                                //@ts-ignore
                                className="text-capitalize">
                                <ColumnRow className="borderRadiusForTable"
                                    css={{ color: "var(--colors-blue1)", textAlign: "center", borderRadius: "8px" }}>
                                    <Column className="col-lg-2">Scheme</Column>
                                    <Column>Amount</Column>
                                    <Column>Status</Column>
                                    <Column>Type</Column>
                                    <Column>Date</Column>
                                    <Column>Folio</Column>
                                    <Column>Units</Column>
                                    <Column>Nav</Column>
                                    <Column>Goal</Column>
                                </ColumnRow>
                            </ColumnParent>
                            <DataParent>
                                {loader ?
                                    <DataRow>
                                        <DataCell colSpan={9}>
                                            <Loader />
                                        </DataCell>
                                    </DataRow> : <>
                                        {sortedtransactions?.length === 0 ? <>
                                            <DataRow>
                                                <DataCell colSpan={9} className="text-center">No Transactions Found </DataCell>
                                            </DataRow>
                                        </> : <>
                                            {sortedtransactions?.map((item: any, index: number) => {
                                                let newdate: any = 0;
                                                if (item?.trxnDate != undefined) {
                                                    let date = item?.trxnDate.split(' ')[0];
                                                    let datearray = date.split("/");
                                                    newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                                                }

                                                let arr = (infoData.filter((ele: any) => ele?.goalCode == item?.gcode)[0])

                                                return (
                                                    <DataRow css={{ borderBottom: "solid 1px #CBC4C4" }}>
                                                        <DataCell>{item?.SchemeName || ""}</DataCell>
                                                        <DataCell className="text-center">&#x20B9;{Number(item?.Amount || 0).toLocaleString("en-IN")}</DataCell>
                                                        {item?.status === "1" ?
                                                            <DataCell className="text-center">
                                                                <Box css={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: '10px 10px 10px 10px' }} className="mx-auto">
                                                                </Box>
                                                                Success</DataCell>
                                                            : item?.status === "2" ?
                                                                <DataCell className="text-center">
                                                                    {/* @ts-ignore */}
                                                                    <Box css={{ width: "15px", height: "15px", backgroundColor: "Orange", borderRadius: '10px 10px 10px 10px' }} className="mx-auto">
                                                                    </Box>
                                                                    In Process</DataCell>
                                                                :
                                                                <DataCell className="text-center">
                                                                    <Box css={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: '10px 10px 10px 10px' }} className="mx-auto">
                                                                    </Box>
                                                                    <Text>
                                                                        Failed <span title={`Reason: ${item?.failure_reason || ""}`} className="text-bg-secondary rounded-4 px-1 pt-1" style={{ fontSize: "0.7rem", cursor: "Pointer" }}>?</span>
                                                                    </Text>
                                                                </DataCell>}
                                                        <DataCell className="text-center">
                                                            {item?.Trxn_sub_type === "R" ? "Redeem"
                                                                : item?.Trxn_sub_type === "NOR" ? "Lumpsum"
                                                                    : item?.Trxn_sub_type || ""
                                                            }
                                                            {item?.Trxn_type === "Credit" ? <><br /><span className='text-success'>(+) Credit</span></> : <><br /><span className='text-danger'>(-) Debit</span></>}
                                                        </DataCell>
                                                        <DataCell className="text-center">{newdate || "0"}</DataCell>
                                                        <DataCell className="text-center">{item?.Id || "0"}</DataCell>
                                                        <DataCell className="text-center">{item?.Units || "0"}</DataCell>
                                                        <DataCell className="text-center">{item?.NAV || "0"}</DataCell>
                                                        <DataCell className="text-center">
                                                            <Box>
                                                                {(item?.gcode) === null || (item?.gcode) === "" ? <></> :
                                                                    <img
                                                                        width={20}
                                                                        height={20}
                                                                        src={`${WEB_URL}/${arr?.icon}`}
                                                                        alt=""
                                                                    />
                                                                }
                                                            </Box>
                                                            {arr?.gName || ""}
                                                        </DataCell>
                                                    </DataRow>
                                                )
                                            })} </>}
                                    </>}
                            </DataParent>
                        </Table>
                    </Box>
                    {/* {showPagination ? */}
                    <Box className="row justify-content-end mt-2">
                        <Box className="col-auto col-lg-auto col-sm-auto px-0 mb-0">
                            <Group position="right">
                                <Box>Total Record: {totalCount} <span className="mb-2" style={{ fontSize: "0.8rem" }}> [{limit} - {limit + txnLen}]</span></Box>
                                <Pagination
                                    totalCount={totalCount}
                                    limit={PageLimit}
                                    onPageChange={(e: any) => {
                                        // console.log((e?.selected) * (PageLimit));
                                        setLimit((e?.selected) * (PageLimit));
                                        // updateQuery("");
                                    }}
                                />
                            </Group>
                        </Box>
                    </Box>
                    {/*  : ""} */}
                </Box>
                <DialogModal
                    open={open}
                    setOpen={setOpen}
                    css={{
                        "@bp0": { width: "90%" },
                        "@bp1": { width: "50%" },
                    }}
                >
                    <Box>
                        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                            {/* @ts-ignore */}
                            <Text css={{ color: "var(--colors-blue1)" }}>Unrealized Gain</Text>
                        </Box>
                        <Box className={`modal-body p-3`} >

                            <Box className="row">
                                <Box className="col-12" >
                                    <Box className="table-responsive" css={{ borderRadius: "8px" }}>
                                        {/* @ts-ignore */}
                                        <Box className="row">
                                            <Box className="col-lg-6">
                                                <Input
                                                    type="date"
                                                    label="From Date"
                                                    name="ReportGainDate"
                                                    color="white"
                                                    defaultValue={prevMonth}
                                                    max={yesterday}
                                                    onChange={(e: any) => {
                                                        // console.log(e?.target.value),
                                                        setFromDate(e?.target.value)
                                                    }}
                                                    className="mb-0"
                                                />
                                            </Box>
                                            <Box className="col-lg-6">
                                                <Input
                                                    type="date"
                                                    label="To Date"
                                                    name="ReportGainDate"
                                                    color="white"
                                                    min={fromDate}
                                                    defaultValue={yesterday}
                                                    max={postMonth}
                                                    onChange={(e: any) => {
                                                        // console.log(e?.target.value),
                                                        setToDate(e?.target.value)
                                                    }}
                                                />
                                            </Box>
                                        </Box>
                                        <Box className="row">
                                            <Box className="col-12 mt-3 text-center">
                                                <Button
                                                    className="mb-2 p-2"
                                                    color="yellowGroup"
                                                    size="md"
                                                    onClick=""
                                                >
                                                    <Text
                                                        // @ts-ignore
                                                        size="h6"
                                                        //@ts-ignore
                                                        color="gray8"
                                                        className={`${styles.button}`}
                                                    >
                                                        Calculate
                                                    </Text>
                                                </Button>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>

                        </Box>
                    </Box>
                </DialogModal>
            </Box>
        </>
    )
}

export default PassbookTransaction