import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import { GroupBox } from "@ui/Group/Group.styles";
import Input from "@ui/Input";
import Text from "@ui/Text/Text";
import React from "react";
import Radio from "@ui/Radio/Radio";
import SelectMenu from "@ui/Select/Select";
import { RadioGroup } from "@radix-ui/react-radio-group";
const RedemptionPage = () => {
  return (
    <Box background="gray">
      <Box className="container-fluid">
        <Box className="row">
          <Box className="col-md-12 col-sm-12 col-lg-12">
            <Card>
              <GroupBox
                position="apart"
                align="center"
                css={{ p: 20 }}
                borderBottom
              >
                <Box>
                  <Text weight="extrabold" size="h3">
                    Transaction Detail
                  </Text>
                </Box>
              </GroupBox>
              <Box css={{ p: 20 }}>
                <Box css={{ mb: 10 }}>
                  <Text weight="bold">
                    ADITYA BIRLA SUN LIFE LIQUID FUND-GROWTH-DIRECT GROWTH
                  </Text>
                </Box>
                <Box >
                  <Box className="row" position="middle">
                    <Box className="col-3" css={{ mb: 10 }}>
                      <Text color="gray" size="h6">
                        Folio
                      </Text>
                      <Text>1010001010</Text>
                    </Box>
                    <Box className="col-3">
                      <Text color="gray" size="h6">
                        Units
                      </Text>
                      <Text>219.84</Text>
                    </Box>
                    <Box className="col-3">
                      <Text color="gray" size="h6">
                        Current Value{" "}
                      </Text>
                      <Text>0</Text>
                    </Box>
                    <Box className="col-3"></Box>
                    <Box className="col-3">
                      <SelectMenu
                        items={[
                          { id: "1", name: "Baroda" },
                          { id: "2", name: "Surat" },
                        ]}
                        bindValue={"id"}
                        bindName={"name"}
                        label={"Reddem Option"}
                        placeholder={"All Units"}
                        onChange={()=>{}}
                      />
                    </Box>
                    <Box className="col-3">
                      <Input
                        label="Account / Units"
                        name="Account"
                        placeholder="0"
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Card>
            <Box>
              <Box>
                <RadioGroup defaultValue="Physical" className="inlineRadio">
                  <Radio
                    value="Physical"
                    label="I/We hereby confirm that this is transaction done purely at my/our sole discretion"
                    id="Physical"
                  />
                </RadioGroup>
              </Box>
              <Box className="text-end">
                <Button color="yellow">Confirm</Button>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default RedemptionPage;
