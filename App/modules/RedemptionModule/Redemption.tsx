import Holder from 'App/shared/components/Holder/Holder'
import Layout from 'App/shared/components/Layout'
import PageHeader from 'App/shared/components/PageHeader/PageHeader'
import React from 'react'
import RedemptionPage from './components/RedemptionPage'

const RedemptionModule = () => {
  return (
    <Layout>
    <PageHeader title='Redemption' rightContent={<Holder/>} />
   <RedemptionPage/>
   </Layout>
  )
}

export default RedemptionModule