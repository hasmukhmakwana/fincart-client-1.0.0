import { Button } from "@ui/Button/Button";
import { GroupBox } from "@ui/Group/Group.styles";
import Layout from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import GoalPortfolioSinglePage from "./components/GoalPortfolioSinglePage";
import style from "./goalsPortfolio.module.scss";
import Text from "@ui/Text/Text";
import router from "next/router";

const GoalPortfolioSingleModule = (...props: any) => {
    return (
        <Layout>
            <PageHeader title="Goal Portfolio" rightContent={<>
                <Button className={`btn text-center larger py-1 my-2 ${style.OtherBtn}`} color="yellow"
                    onClick={() => { router.push("/Dashboard"); }}>
                    <GroupBox>
                        <Text size="h5">Back</Text>
                    </GroupBox>
                </Button>
            </>} />
            <GoalPortfolioSinglePage />
        </Layout>
    )
}

export default GoalPortfolioSingleModule;