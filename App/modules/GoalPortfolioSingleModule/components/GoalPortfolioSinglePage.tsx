import React, { useEffect, useState } from 'react'
import Box from '@ui/Box/Box';
import Text from "App/ui/Text/Text";
import Card from "App/ui/Card/Card";
import style from "../goalsPortfolio.module.scss";
import GaugeRing from "App/modules/DashboardModule/components/MyGoals/GaugeRing";
import { StyledTabs, TabsList, TabsTrigger } from "@ui/Tabs/Tabs.styles";
import { TabsContent } from "@radix-ui/react-tabs";
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from "@ui/SimpleGrid/DataGrid.styles";
import { RadioGroup } from "@radix-ui/react-radio-group";
import Radio from "@ui/Radio/Radio";
import {
    MutualFundPage,
    ULIPPage,
    InsurancePage,
    LiquiloanPage,
    PMSPage,
    EPFPage,
    ProtectionPage
} from "App/modules/ProductModule"
import usePortfolioStore from 'App/modules/DashboardModule/store';
import { getLS } from '@ui/DataGrid/utils';
import { encryptData, formatDate } from 'App/utils/helpers';
import { getGoalWisePortfolioDetail } from 'App/api/dashboard';
import { toastAlert } from 'App/shared/components/Layout';

let recentTabs = [
    {
        title: "Mutual Fund",
        value: "Mutual Fund",
    },
    {
        title: "ULIP",
        value: "ULIP",
    },
    {
        title: "PMS",
        value: "PMS",
    },
    {
        title: "LiquiLoans",
        value: "LiquiLoans",
    },
    {
        title: "Other Insurance",
        value: "Other Insurance",
    },
    {
        title: "Protection",
        value: "Protection",
    },
    // {
    //   title: "NPS",
    //   value: "NPS",
    // },
    // {
    //   title: "EPF",
    //   value: "EPF",
    // },
    // {
    //   title: "Digital Gold",
    //   value: "Digital Gold",
    // },
];

const GoalPortfolioSinglePage = () => {
    const { selectedGoalWisePortfolioData, basicId, setLastUpdatedDate,
        last_Updated_date,
        setMutualFundPortfolioData,
        setInsurancePortfolioData,
        setUlipPortfolioData,
        setPmsPortfolioData,
        setLiquiloanPortfolioData,
        setProtectionPortfolioData,
        setEpfPortfolioData,
        setDigitalGoldPortfolioData } =
        usePortfolioStore();

    const [dataType, setDataType] = useState<any>("active");

    const [loader, setLoader] = useState<any>(false);

    const fetchOverallData = async () => {
        try {
            setLoader(true);

            const obj: any = {
                usergoalId: selectedGoalWisePortfolioData?.UserGoalId,
                isActive: dataType == "active" ? true : false,
                objective: "",
                investmentSource: "ALL"
            };

            const enc: any = encryptData(obj);

            const result: any = await getGoalWisePortfolioDetail(enc);

            setLoader(false);

            if (result != undefined) {
                setLastUpdatedDate(result?.data?.last_Updated_date);

                for (let i = 0; i < result?.data?.productWiseList.length; i++) {
                    switch (result?.data?.productWiseList[i].investment_type) {
                        case "MUTUAL FUND":
                            result?.data?.productWiseList[i].data.sort((a: any, b: any) =>
                                a.partner_name > b.partner_name ? 1 : -1
                            );
                            setMutualFundPortfolioData(
                                result?.data?.productWiseList[i].data
                            );
                            break;
                        case "ULIP":
                            setUlipPortfolioData(result?.data?.productWiseList[i].data);
                            break;
                        case "PMS":
                            setPmsPortfolioData(result?.data?.productWiseList[i].data);
                            break;
                        case "LIQUILOAN":
                            setLiquiloanPortfolioData(
                                result?.data?.productWiseList[i].data
                            );
                            break;
                        case "OTHER INSURANCE":
                            setInsurancePortfolioData(
                                result?.data?.productWiseList[i].data
                            );
                            break;
                        case "PROTECTION":
                            setProtectionPortfolioData(
                                result?.data?.productWiseList[i].data
                            );
                            break;
                        case "EPF":
                            setEpfPortfolioData(result?.data?.productWiseList[i].data);
                            break;
                        case "DIGITAL GOLD":
                            setDigitalGoldPortfolioData(
                                result?.data?.productWiseList[i].data
                            );
                            break;
                    }
                }
            }

        } catch (error) {
            console.log(error);
            toastAlert("error", error);
            setLoader(false);
        }
    };

    useEffect(() => {
        if (selectedGoalWisePortfolioData?.UserGoalId != undefined) {
            (async () => {
                await fetchOverallData();
            })();
        }
    }, [dataType]);

    return (
        <>
            <Box className="row">
                <Card css={{ p: 20, backgroundColor: "#005CB3" }}>
                    <Box className="row">
                        <Text size="h4" color="white">
                            Goal: {selectedGoalWisePortfolioData?.GName}
                        </Text>
                    </Box>
                    <Box className={`mt-2 ms-4`}>
                        <Box className="row pt-3 pb-3">
                            <Box className="col-12 col-lg-3 text-center">
                                <Box className={style.whiteBoxTitleText}>Invested Amount (Rs.)</Box>
                                <Box className={style.whiteBoxValueText}> &#x20B9;{isNaN(Number(selectedGoalWisePortfolioData?.InvestedAmount)) ? "0"
                                    : Number(selectedGoalWisePortfolioData?.InvestedAmount).toLocaleString(
                                        "en-IN"
                                    )}</Box>
                            </Box>
                            <Box className="col-12 col-lg-3 text-center">
                                <Box className={style.whiteBoxTitleText}>Current Amount (Rs.)</Box>
                                <Box className={style.whiteBoxValueText}> &#x20B9;{isNaN(Number(selectedGoalWisePortfolioData?.currWorth)) ? "0"
                                    : Number(selectedGoalWisePortfolioData?.currWorth).toLocaleString(
                                        "en-IN"
                                    )}</Box>
                            </Box>
                            <Box className="col-12 col-lg-3 text-center">
                                <Box className={style.whiteBoxTitleText}>Maturity Amount(Rs.)</Box>
                                <Box className={style.whiteBoxValueText}> &#x20B9;{isNaN(Number(selectedGoalWisePortfolioData?.Goal_MaturityAmt)) ? "0"
                                    : Number(selectedGoalWisePortfolioData?.Goal_MaturityAmt).toLocaleString(
                                        "en-IN"
                                    )}</Box>
                            </Box>
                            <Box className="col-12 col-lg-3 text-center">
                                <Box className={style.whiteBoxTitleText}>Goal Achieved</Box>
                                <Box className={style.whiteBoxValueText}>
                                    {isNaN(Number(selectedGoalWisePortfolioData?.goalAchived)) ? <GaugeRing value={0} valueColor="white" /> :
                                        <GaugeRing value={Number(selectedGoalWisePortfolioData?.goalAchived)} valueColor="white" />}
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Card>
                <Box className="row text-end mt-3"><Text size="h6"><span style={{ fontWeight: 600 }}>Last Updated On: </span>{formatDate(last_Updated_date, "d-m-y")}</Text></Box>
            </Box>
            <StyledTabs defaultValue="Mutual Fund">
                <TabsList
                    aria-label="Manage your account"
                    className="tabsCardStyle"
                    css={{ mb: 10 }}
                >
                    {recentTabs.map((item, index) => {
                        return (
                            <TabsTrigger
                                value={item.value}
                                className="tabsCards"
                                key={item.value}
                            >
                                <Box className="text-center">
                                    {/* @ts-ignore */}
                                    <Text size="bt2">{item.title}</Text>
                                </Box>
                            </TabsTrigger>
                        );
                    })}
                </TabsList>
                <TabsContent value="Mutual Fund">
                    <MutualFundPage basicId={basicId} userGoalId={selectedGoalWisePortfolioData?.UserGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={true} />
                </TabsContent>
                <TabsContent value="ULIP">
                    <ULIPPage id={basicId} userGoalId={selectedGoalWisePortfolioData?.UserGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={true} />
                </TabsContent>
                <TabsContent value="PMS">
                    <PMSPage id={basicId} userGoalId={selectedGoalWisePortfolioData?.UserGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={true} />
                </TabsContent>
                <TabsContent value="LiquiLoans">
                    <LiquiloanPage id={basicId} userGoalId={selectedGoalWisePortfolioData?.UserGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={true} />
                </TabsContent>
                <TabsContent value="Other Insurance">
                    <InsurancePage id={basicId} userGoalId={selectedGoalWisePortfolioData?.UserGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={true} />
                </TabsContent>
                <TabsContent value="Protection">
                    <ProtectionPage id={basicId} userGoalId={selectedGoalWisePortfolioData?.UserGoalId} dataType={dataType} setDataType={setDataType} isGoalWisePortfolio={true} />
                </TabsContent>
            </StyledTabs>
        </>
    )
}

export default GoalPortfolioSinglePage