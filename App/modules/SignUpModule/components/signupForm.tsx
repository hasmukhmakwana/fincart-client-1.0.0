import React from "react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { SignUpFormData } from "../signupTypes";
import Box from "App/ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Input from "App/ui/Input";
import dynamic from "next/dynamic";
import EyeIcon from "App/icons/EyeIcon";
import { useRouter } from "next/router";
const EyeSlash:any = dynamic(() => import("App/icons/EyeSlash"));

const SignUpSchema = Yup.object().shape({
  email: Yup.string().required("Email is required").email("Invalid Email"),
  name: Yup.string().required("Name is required"),
  mobile: Yup.number()
    .required("Mobile is required")
    .min(6000000000, "invalid mobile number")
    .max(9999999999, "invalid mobile number"),
  password: Yup.string()
    .required("Password is required")
    .min(6, "Password is too short - should be 6 chars minimum.")
    .max(15, "Password is too long - should be 15 chars maximum."),
});

type SignUpEventTypes = {
  initValues: SignUpFormData;
  setSignUpForm: (values: SignUpFormData) => void;
  handleData: (value: SignUpFormData) => void;
  togglePasswordType: () => void;
  passwordType: "password" | "text";
  loading: boolean;
};

function SignUpForm({
  initValues,
  setSignUpForm,
  handleData,
  togglePasswordType,
  passwordType,
  loading,
}: SignUpEventTypes) {
  const router = useRouter();

  return (
    <>
      <Formik
        initialValues={initValues}
        validationSchema={SignUpSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleData(values);
          setSignUpForm(values);
          resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount,
        }) => (
          <Form>
            <Box className="col">
              <Input
                label="EMAIL"
                required
                onChange={handleChange}
                value={values.email}
                name="email"
                error={submitCount ? errors.email : null}
              />
            </Box>
            <Box className="col">
              <Input
                type="number"
                label="MOBILE"
                required
                onChange={handleChange}
                value={values.mobile}
                name="mobile"
                error={submitCount ? errors.mobile : false}
              />
            </Box>
            <Box className="col">
              <Input
                label="NAME"
                required
                onChange={handleChange}
                value={values.name}
                name="name"
                error={submitCount ? errors.name : null}
              />
            </Box>
            <Box className="col" style={{ position: "relative" }}>
              <Input
                type={passwordType}
                label="PASSWORD"
                required
                onChange={handleChange}
                value={values.password}
                name="password"
                error={submitCount ? errors.password : null}
              />
              <Button
                onClick={togglePasswordType}
                className="input-group-text"
                color="clear"
                style={{
                  position: "absolute",
                  right: "0px",
                  top: "31px",
                }}
              >
                {passwordType == "password" ? (
                  <EyeIcon style={{ fill: "blue" }} />
                ) : (
                  <EyeSlash style={{ fill: "blue" }} />
                )}
              </Button>
            </Box>

            <Box>
              <Box className="">
                <Box className="align-item-center"></Box>
                <Button
                  type="submit"
                  full
                  color="shadowbtn"
                  onClick={handleSubmit}
                  loading={loading}
                >
                  SIGN UP
                </Button>
              </Box>
            </Box>
            <Box className="mt-3">
              <a
                className="small"
                color="shadowbtn"
                onClick={() => router.push("/Login")}
              >
                Already have an account ? Login
              </a>
            </Box>
          </Form>
        )}
      </Formik>
    </>
  );
}
export default SignUpForm;
