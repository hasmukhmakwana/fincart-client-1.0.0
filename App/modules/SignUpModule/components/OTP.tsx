import React, { useEffect, useState } from "react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { MessagePayloadTypes, SignUpFormData } from "../signupTypes";
import Box from "App/ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Input from "App/ui/Input";
import { useRouter } from "next/router";
import { encryptData } from "App/utils/helpers";
import { signup } from "App/api/signup";

const OTPSchema = Yup.object().shape({
  otpM: Yup.string().required("Mobile OTP is required"),
  otpE: Yup.string().required("Email OTP is required"),
});

type OTPEventTypes = {
  initValues: SignUpFormData;
  setSignUpForm: (values: SignUpFormData) => void;
  submitOTP: (value: SignUpFormData) => void;
  loading: boolean;
  setformType: (value: "signup" | "opt") => void;
  setMessage: (payload: MessagePayloadTypes) => void;
};

function OTP({
  initValues,
  setSignUpForm,
  submitOTP,
  loading,
  setformType,
  setMessage,
}: OTPEventTypes) {
  const router = useRouter();
  const [counter, setcounter] = useState<number>(30);

  //*useEffects
  useEffect(() => {
    if (!counter) {
      return;
    }
    const interval = setTimeout(() => setcounter((prev) => prev - 1), 1000);
    return () => {
      clearInterval(interval);
    };
  }, [counter]);

  const resendOTP = async () => {
    try {
      setMessage({
        type: "success",
        msg: "",
      });
      setcounter(30);
      const enc: any = encryptData(initValues);
      await signup(enc);
      setMessage({
        type: "success",
        msg: "OTP has been resent!!",
      });
    } catch (error: any) {
      console.log(error);
      setcounter(0);
      setMessage(error?.[0]?.msg || error?.msg || "Something went wrong!");
    }
  };

  return (
    <>
      <Formik
        initialValues={initValues}
        validationSchema={OTPSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          submitOTP(values);
          setSignUpForm(values);
          resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount,
        }) => (
          <Form>
            <Box className="col">
              <Input
                label="ENTER EMAIL OTP"
                required
                onChange={handleChange}
                value={values.otpE}
                name="otpE"
                error={submitCount ? errors.otpE : null}
              />
            </Box>
            <Box className="col">
              <Input
                label="ENTER MOBILE OTP"
                required
                onChange={handleChange}
                value={values.otpM}
                name="otpM"
                error={submitCount ? errors.otpM : null}
              />
            </Box>

            <Box>
              <Box className="">
                <Box className="align-item-center"></Box>
                <Button
                  full
                  type="submit"
                  color="shadowbtn"
                  onClick={handleSubmit}
                  loading={loading}
                >
                  VERIFY OTP
                </Button>
                <Box className="align-item-center"></Box>

                <Button
                  className="mt-3"
                  full
                  color="shadowbtn"
                  onClick={() => resendOTP()}
                  // loading={resendLoading}

                  disabled={!!counter}
                >
                  {counter ? `RESEND OTP (${counter})` : `RESEND OTP`}
                </Button>
              </Box>
            </Box>

            <Box className="mt-3">
              <a
                className="small"
                color="shadowbtn"
                onClick={() => {
                  setMessage({
                    type: "success",
                    msg: "",
                  });
                  setformType("signup");
                }}
              >
                Back to Sign Up
              </a>
            </Box>

            <Box className="mt-3">
              <a
                className="small"
                color="shadowbtn"
                onClick={() => router.push("/Login")}
              >
                Already have an account ? Login
              </a>
            </Box>
          </Form>
        )}
      </Formik>
    </>
  );
}
export default OTP;
