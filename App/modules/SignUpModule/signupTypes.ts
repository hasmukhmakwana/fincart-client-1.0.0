export type SignUpFormData = {
  email: string;
  mobile: string;
  name: string;
  otpM: string;
  otpE: string;
  ClientStatus: string;
  trackerCode: string;
  password: string;
};

export type SignUpFormTypes = {
  alertMessage?: {
    type: "success" | "danger";
    msg: string;
  };
  signupData: {
    email: string;
    mobile: string;
    name: string;
    otpM: string;
    otpE: string;
    ClientStatus: string;
    trackerCode: string;
    password: string;
  };
};

export enum SIGNUP_ACTION_TYPES {
  DO_SIGNUP = "DO_SIGNUP",
  ERROR = "ERROR",
  RESET_FORM = "RESET_FORM",
}

export type MessagePayloadTypes = {
  type: string;
  msg: string;
};
