import React, { FC, useContext, useEffect, useReducer, useState } from "react";
import style from "./signup.module.scss";
import AuthContext from "App/contexts/auth/auth.context";
import { verifyOTP, signup } from "App/api/signup";
import { useRouter } from "next/router";
import Text from "@ui/Text/Text";
import SignupForm from "./components/signupForm";
import { initialState, SignUpReducer } from "./signupReducer";
import {
  MessagePayloadTypes,
  SignUpFormData,
  SIGNUP_ACTION_TYPES,
} from "./signupTypes";
import { encryptData } from "App/utils/helpers";
import OTP from "./components/OTP";
import { WEB_URL } from "App/utils/constants";

const SignUpModule: FC = () => {
  const { authDispatch }: any = useContext(AuthContext);
  const [state, dispatch] = useReducer(SignUpReducer, initialState);

  const setSignupForm = (formValues: SignUpFormData) => {
    dispatch({ type: SIGNUP_ACTION_TYPES.DO_SIGNUP, payload: formValues });
  };

  const setMessage = (payload: MessagePayloadTypes) => {
    dispatch({
      type: SIGNUP_ACTION_TYPES.ERROR,
      payload,
    });
  };

  const router = useRouter();
  const [passwordType, setpasswordType] = useState<"password" | "text">(
    "password"
  );

  const [formType, setformType] = useState<"signup" | "opt">("signup");

  const [loading, setloading] = useState<boolean>(false);

  useEffect(() => {
    router.prefetch("/");
    return () => { };
  }, []);

  const togglePasswordType = () => {
    setpasswordType((prev) => (prev == "password" ? "text" : "password"));
  };

  /** @handleData user signup */
  const handleData = async (formValues: SignUpFormData) => {
    try {
      setloading(true);
      const encryptedData: any = encryptData(formValues);
      await signup(encryptedData);
      setloading(false);
      setformType("opt");
      setMessage({
        type: "success",
        msg: "OTP has been sent to your email and mobile",
      });
    } catch (error: any) {
      console.log(error);
      setloading(false);
      setMessage({
        type: "danger",
        msg: error?.msg || error?.[0]?.msg || "Something went wrong!",
      });
    }
  };

  /** @verifying email and mobile OTP */
  const submitOTP = async (formValues: SignUpFormData) => {
    try {
      console.log(formValues);

      setloading(true);
      const encryptedData: any = encryptData(formValues);
      let result: any = await verifyOTP(encryptedData);
      console.log("result --- ", result);
      setloading(false);
      if (result?.status === "Success") {
        router.push("/Login");
      } else {
        console.log("ERROR in submitOTP => ", result);
      }
    } catch (error: any) {
      console.log(error);
      setloading(false);
      setMessage({
        type: "danger",
        msg: error?.msg || error?.[0]?.msg || "Something went wrong!",
      });
    }
  };

  return (
    <div className={style.signupbg}>
      <header className={style.header}>
        <div className={style.headerContainer}>
          <div
            style={{
              paddingBottom: "0.5rem",
              paddingTop: "0.5rem",
            }}
          >
            <div className="container">
              <div className="row row-cols-2 align-items-center justify-content-between">
                <div className="col d-flex align-items-center">
                  <div className={`${style.logoSection}`}>
                    <img
                      // width={100}
                      // height={30}
                      src={`${WEB_URL}/logo.png`}
                      alt="logo"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div className="container">
        <div className="row">
          <div className="col-12 col-sm-8 mt-3 my-sm-auto">
            <div className="ms-auto">
              <div className="row">
                <div className="col-12">
                  <h2 className="text-white text-center">
                    MEET YOUR FUTURE<br></br>
                    GOALS WITH FINCART
                  </h2>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  &nbsp;
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="text-center" style={{ color: '#B3DAFF' }}>
                    At Fincart, our endeavor is not to sell you yet another financial product but help you
                    with a completely customized solution and guide you in your financial journey.
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  &nbsp;
                </div>
              </div>
              <div
                style={{ borderBottom: "solid 1px #FAAC05" }}
              ></div>
              <div className="row">
                <div className="col-12">
                  &nbsp;
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="text-center">
                    <img src={`${WEB_URL}/investment_icon.png`}
                      alt="logo" className="img-fluid"></img>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-4">
            <div className="mt-3 mt-sm-auto align-items-start align-items-center">
              <div className={style.signupContainer}>
                <div className={style.signupBox}>
                  <div className="text-center">
                    <img
                      width={100}
                      height={30}
                      src={`${WEB_URL}/logo.svg`}
                      alt="logo"
                    />
                  </div>
                  <div className="text-center pt-4">
                    <Text
                      className={style.pleasesignup}
                      //@ts-ignore
                      color="blue"
                      size="h4"
                      weight="bold"
                    >
                      {formType === "signup" ? "Sign Up" : "Verify OTP"}
                    </Text>
                    {state?.alertMessage?.msg && (
                      <Text
                        css={{ fontSize: "0.7rem" }}
                        className={`text-${state?.alertMessage?.type}`}
                      >
                        {state?.alertMessage?.msg}
                      </Text>
                    )}
                  </div>

                  {formType === "signup" ? (
                    <SignupForm
                      initValues={state.signupData}
                      setSignUpForm={setSignupForm}
                      handleData={handleData}
                      togglePasswordType={togglePasswordType}
                      passwordType={passwordType}
                      loading={loading}
                    />
                  ) : (
                    <OTP
                      initValues={state.signupData}
                      setSignUpForm={setSignupForm}
                      submitOTP={submitOTP}
                      loading={loading}
                      setformType={setformType}
                      setMessage={setMessage}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUpModule;
