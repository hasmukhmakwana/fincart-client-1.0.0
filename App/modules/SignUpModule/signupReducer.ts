import { SignUpFormTypes, SIGNUP_ACTION_TYPES } from "./signupTypes";

export const initialState: SignUpFormTypes = {
  signupData: {
    email: "",
    mobile: "",
    name: "",
    otpM: "",
    otpE: "",
    ClientStatus: "",
    trackerCode: "",
    password: "",
  },
  alertMessage: {
    type: "success",
    msg: "",
  },
};

export function SignUpReducer(
  state: SignUpFormTypes = initialState,
  action: { payload: any; type: SIGNUP_ACTION_TYPES }
) {
  let { type, payload } = action;
  switch (type) {
    case SIGNUP_ACTION_TYPES.DO_SIGNUP:
      return {
        ...state,
        signupData: payload,
      };

    //ERROR
    case SIGNUP_ACTION_TYPES.ERROR:
      return { ...state, alertMessage: payload };

    default:
      return { ...state };
      break;
  }
}
