import { MANDATE_TYPES } from "App/utils/constants";
import { List } from "echarts";
import create from "zustand";
import { getTransactType, goalNameCodeList } from "./GoalTypes"

interface StoreTypes {
  selectedGoalCode: string;
  selectedGoalName: string;
  open: boolean;
  openEdit: boolean;
  openDelete: boolean;
  selectedTransact: getTransactType;
  goalNameCodeist: goalNameCodeList[];
  setOpen: (payload: boolean) => void;
  setOpenEdit: (payload: boolean) => void;
  setOpenDelete: (payload: boolean) => void;
  setSelectedTransact: (payload: getTransactType) => void;
  setGoalNameCodeList: (payload: goalNameCodeList[]) => void;
  setSelectedGoalCode: (payload: string) => void;
  setSelectedGoalName: (payload: string) => void;
}

const useGoalStore = create<StoreTypes>((set) => ({
  //* initial state
  selectedGoalCode: "",
  selectedGoalName: "",
  open: false,
  openEdit: false,
  openDelete: false,
  selectedTransact: {
    trnx_Type: "",
    getAmount: "",
    investAmount: "",
  },
  goalNameCodeist: [],
  //* methods for manipulating state
  setOpen: (payload) =>
    set((state) => ({
      ...state,
      open: payload,
    })),
  setOpenEdit: (payload) =>
    set((state) => ({
      ...state,
      openEdit: payload,
    })),
  setOpenDelete: (payload) =>
    set((state) => ({
      ...state,
      openDelete: payload,
    })),
  setSelectedGoalCode: (payload) =>
    set((state) => ({
      ...state,
      selectedGoalCode: payload,
    })),
  setSelectedTransact: (payload) =>
    set((state) => ({
      ...state,
      selectedTransact: payload,
    })),
  setGoalNameCodeList: (payload) =>
    set((state) => ({
      ...state,
      goalNameCodeist: payload,
    })),
  setSelectedGoalName: (payload) =>
    set((state) => ({
      ...state,
      selectedGoalName: payload
    }))
}));


export default useGoalStore;
