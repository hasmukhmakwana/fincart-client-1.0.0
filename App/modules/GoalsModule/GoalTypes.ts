export type allGoalTypes = {
  userGoalId: string;
  goalName: string;
  goalImg: string;
  investedAmt: string;
  currentAmt: string;
  goalPerc: string;
  getAmount: string;
  goalCode: string;
  duration: number;
  isDelete: string;
  typeCode: string;
  investAmount: string;
  trxntype: string;
};

export type calcType = {
  type: string;
  currentAmount: string;
  startDate: string;
  endDate: string;
  goalCode: string;
  otherGoalName: string;
  childName: string;
  travelPeople: string;
  locationCode: string;
  budgetType: string;
  businessStartupCost: string;
  monthlyexpence: string;
  age: number;
  retirementAge: number;
}

export type saveGoalType = {
  userGoalId: string;
  basicid: string;
  Relation: string;
  childName: string;
  age: number;
  gender: string;
  annualIncome: string;
  goalCode: string;
  otherGoalName: string;
  typeCode: string;
  monthlyAmount: string;
  presentValue: number;
  risk: string;
  goal_StartDate: Date;
  goal_EndDate: Date;
  inflationRate: string;
  ror: string;
  PMT: string;
  downPaymentRate: string;
  trnx_Type: string;
  getAmount: string;
  retirementAge: number;
  investAmount: string;
  people: string;
}

export type calculatedType = {
  time: string;
  getLumpsum: string;
  getSip: string;
  investLumpsum: string;
  investSip: string;
  PMT: string;
  ROR: string;
  RORL: string;
  Inflation: string;
  goalName: any;
}

export type getTransactType = {
  trnx_Type: string;
  getAmount: string;
  investAmount: string;
}

export type goalNameCodeList = {
  currentAmt: null;
  duration: null;
  getAmount: null;
  goalCode: string;
  goalImg: null;
  goalName: string;
  goalPerc: null
  investAmount: null
  investedAmt: null
  isDelete: null
  trxntype: null
  typeCode: string;
  userGoalId: string;
}