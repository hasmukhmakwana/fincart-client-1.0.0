import style from "../Goals.module.scss";
import React from "react";
import Radio from "@ui/Radio/Radio";
import { Button } from "@ui/Button/Button";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { calculatedType, getTransactType } from "../GoalTypes";
import { useEffect, useMemo, useState } from "react";
import { GroupBox } from "@ui/Group/Group.styles";
import { RadioGroup } from "@ui/Radio";
import useGoalStore from "../store";

// const validationSchema = Yup.object().shape({
//   Password: Yup.string().required("Password is required"),
//   Email: Yup.string().required("Email is required").email("Invalid Email"),
// });

type BasicTypes = {
  handleSaveGoal: (values: calculatedType, arr: any) => void;
  calculatedDetails: calculatedType;
  setAPITrigger: (values: Boolean) => void;
  APITrigger: Boolean;
};

// main
const CalcResult = ({ handleSaveGoal, calculatedDetails, setAPITrigger, APITrigger }: BasicTypes) => {

  const {
    setSelectedTransact
  } = useGoalStore();
  const [formValues, setformValues] = useState<calculatedType>(calculatedDetails);
  const [types, setTypes] = useState("2");


  const handleSaveSubmit = () => {
    let arr = {}
    if (types === "2") {
      arr = {
        trnx_Type: "S",
        getAmount: formValues.getSip,
        investAmount: formValues.investSip
      }
    }
    else {
      arr = {
        trnx_Type: "L",
        getAmount: formValues.getLumpsum,
        investAmount: formValues.investLumpsum
      }
    }
    handleSaveGoal(formValues, arr);
  }

  return (
    <>
      {/* @ts-ignore */}

      <Box className="col p-4">
        <GroupBox css={{ mb: 10 }}>
          <RadioGroup
            defaultValue={types}
            className="inlineRadio"
            name="scheme"
            onValueChange={(e: any) => {
              setTypes(e);
            }}
          >
            <React.Fragment>
              <Radio value="1" label="Lumpsum" id="1" />
              <Radio value="2" label="SIP" id="2" />
            </React.Fragment>
          </RadioGroup>
        </GroupBox>
        <Input label={`${types === "2" ? "SIP Amount" : "Lumpsum Amount"}`} disabled value={types === "2" ? formValues.investSip : formValues.investLumpsum} onChange={() => { }} />

      </Box>

      <Box className={style.amount}>
        <Box className="row">
          <Box className="col text-center">
            {/* @ts-ignore */}
            <Text color="primary">
              After {formValues.time} year your required amount will be {types === "2" ? formValues.getSip : formValues.getLumpsum}
            </Text>
          </Box>
        </Box>
      </Box>
      <Box className="p-4">
        <Box className="row">
          <Box className="col text-end">
            <Button color="yellow" onClick={() => { setAPITrigger(true); handleSaveSubmit() }} disabled={APITrigger}>
              Save
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default CalcResult;
