import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { calcType } from "../GoalTypes";
import { Formik } from "formik";
import SelectMenu from "@ui/Select/Select";
import * as Yup from "yup";
import { useEffect, useMemo, useState } from "react";
import TravelNew from "App/icons/Travel";

const validationSchema = Yup.object().shape({
  locationCode: Yup.string().required("Location is required"),
  endDate: Yup.string().required("Target Date is required"),
});

type BasicTypes = {
  handleCalculate: (values: calcType) => void;
  calculateDetails: calcType;
};

// main
const Travel = ({ handleCalculate, calculateDetails }: BasicTypes) => {

  const locList = [
    { Value: "001", Text: "USA OR CANADA" },
    { Value: "002", Text: "AUSTRALIA OR NZ" },
    { Value: "003", Text: "EUROPE" },
    { Value: "004", Text: "SOUTH EAST ASIA" },
    { Value: "005", Text: "INDIA" },

  ]
  const [formValues, setformValues] = useState<calcType>(calculateDetails);

  const [minDate, setMinDate] = useState<string>("");
  const [maxDate, setMaxDate] = useState<string>("");

  useEffect(() => {
    const date = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
    const MinDate = (date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDay()).padStart(2, '0'));
    const MaxDate = (date.getFullYear() + 99) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDay()).padStart(2, '0'));
    setMinDate(MinDate);
    setMaxDate(MaxDate)
  }, [])

  return (
    <>
      {/* @ts-ignore */}
      <Formik
        initialValues={formValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleCalculate(values);
          console.log(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount
        }) => (
          <Box>
            <Box className="p-4">
              <Box className="row row-cols-1">
                <Box className="row pb-2">
                  <Box className="col text-center">
                    <TravelNew size={50} />
                  </Box>
                </Box>
                <Box className="col"></Box>
                <Box className="col">
                  <Text size="h6">Where do you want to Travel?</Text>
                  <SelectMenu
                    items={locList}
                    bindValue={"Value"}
                    bindName={"Text"}
                    label={"Location"}
                    placeholder={"Select Location"}
                    name="location"
                    value={values.locationCode}
                    onChange={(e: any) => {
                      setFieldValue("locationCode", e.Value);
                    }}
                    required
                    error={submitCount ? errors.locationCode : undefined}
                  />
                </Box>
                <Box className="col"></Box>
                <Box className="col">
                  <Text size="h6">How many people going?</Text>
                  <Input label="Enter Number of people" name="travelPeople" value={values.travelPeople}
                    onChange={handleChange}
                    error={submitCount ? errors.travelPeople : null} />
                </Box>
                <Box className="col"></Box>
                <Box className="col">
                  <Text size="h6">Target year for Travel?</Text>
                  <Input label="Date" type="date" name="endDate" value={values.endDate}
                    min={minDate}
                    max={maxDate}
                    onChange={handleChange}
                    error={submitCount ? errors.endDate : null} />
                </Box>
                <Box className="col"></Box>
                <Box className="row">
                  <Box className="col text-end">
                    <Button color="yellow" onClick={handleSubmit}>
                      Calculate
                    </Button>
                  </Box>
                </Box>

              </Box>
            </Box>
          </Box>

        )}
      </Formik>
    </>
  );
};
export default Travel;
