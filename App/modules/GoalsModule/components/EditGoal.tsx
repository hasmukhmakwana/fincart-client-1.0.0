
import Box from "../../../ui/Box/Box";
import { UiTypeTypes } from "../Goals";
import Layout, { toastAlert } from "App/shared/components/Layout";
import { encryptData, getUser } from "App/utils/helpers";
import React from "react";
import useGoalStore from "../store";
import ChildStudy from "./ChildStudy";
import ChildWedding from "./ChildWedding";
import Bike from "./Bike";
import Business from "./Business";
import Car from "./Car";
import House from "./House";
import Study from "./Study";
import Travel from "./Travel";
import Wedding from "./Wedding";
import Wealth from "./Wealth";
import Retirement from "./Retirement";
import Sabbatical from "./Sabbatical";
import FamilyPlanning from "./FamilyPlanning";
import Other from "./Other";
import Emergency from "./Emergency";
import Text from "@ui/Text/Text";
import {
  calculateCost,
  updateGoal,
  fetchGoalData
} from "App/api/goalPlan";
import CalcResult from "./CalcResult";
import { useEffect, useMemo, useState } from "react";
import { calcType, calculatedType } from "../GoalTypes";
import Loader from "App/shared/components/Loader";
interface AddGoalTypes {
  selectedGoal: (ui: any) => void;
  fetchGoal: () => void;
  fetchGoalNameCodeList: () => void;
  reload: () => void;
}

const EditGoal = ({ selectedGoal, fetchGoal, fetchGoalNameCodeList, reload }: AddGoalTypes) => {
  const user: any = getUser();
  //   console.log(selectedGoal);
  const {
    selectedGoalCode, setOpenEdit, selectedTransact
  } = useGoalStore();
  const [APITrigger, setAPITrigger] = useState<Boolean>(false);
  const [showCalcDetails, setShowCalcDetails] = useState<boolean>(false);
  const [editFormValues, setEditFormValues] = useState<any>(selectedGoal);
  const [loader, setLoader] = useState<boolean>(false);
  const [goalData, setGoalData] = useState<any>([]);
  const [calculateDetails, setCalculateDetails] = useState<calcType>({
    type: "CAMT",
    currentAmount: "0",
    startDate: "",
    endDate: "",
    goalCode: selectedGoalCode,
    otherGoalName: "",
    childName: "",
    travelPeople: "",
    locationCode: "",
    budgetType: "",
    businessStartupCost: "",
    monthlyexpence: "",
    age: 0,
    retirementAge: 0,
  });
  const [calculatedDetails, setCalculatedDetails] = useState<calculatedType>({
    time: "",
    getLumpsum: "",
    getSip: "",
    investLumpsum: "",
    investSip: "",
    PMT: "",
    ROR: "",
    RORL: "",
    Inflation: "",
    goalName: null,
  })

  const handleCalculate = async (formValues: calcType) => {
    // try {
    setShowCalcDetails(false);
    console.log("handleCalcType: ", formValues);
    try {
      let obj = {
        "type": "CAMT",
        "currentAmount": formValues.currentAmount === "" ? "0" : formValues.currentAmount,
        "startDate": formValues.goalCode === "FG9" ? formValues.age : formValues.startDate === "" ? null : new Date(formValues.startDate),
        "endDate": formValues.goalCode === "FG9" ? formValues.retirementAge : new Date(formValues.endDate),
        "goalCode": formValues.goalCode,
        "otherGoalName": formValues.otherGoalName === "" ? null : formValues.otherGoalName,
        "childName": formValues.childName === "" ? null : formValues.childName,
        "travelPeople": formValues.travelPeople === "" ? null : formValues.travelPeople,
        "locationCode": formValues.locationCode === "" ? null : formValues.locationCode,
        "budgetType": formValues.budgetType === "" ? null : formValues.budgetType,
        "businessStartupCost": formValues.businessStartupCost === "" ? null : formValues.businessStartupCost,
        "monthlyexpence": formValues.monthlyexpence === "" ? null : formValues.monthlyexpence
      }
      // console.log(obj);
      setCalculateDetails(formValues);
      const enc: any = encryptData(obj);
      // console.log(enc);
      const result: any = await calculateCost(enc);
      setCalculatedDetails(result?.data);
      setShowCalcDetails(true);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
    }
    setShowCalcDetails(true);

  };

  const handleSaveGoal = async (formValues: calculatedType, arr: any) => {
    console.log("save Values", formValues);
    console.log("save Values", calculateDetails);
    try {
      let obj = {
        'usergoalId': editFormValues?.userGoalId || editFormValues?.UserGoalId,
        'basicid': user?.basicid,
        'Relation': calculateDetails?.goalCode === "FG12" || calculateDetails?.goalCode === "FG13" ? "002" : null,
        'childName': calculateDetails?.childName === "" ? null : calculateDetails?.childName,
        'age': calculateDetails?.goalCode === "FG12" || calculateDetails?.goalCode === "FG13" ? "4" : calculateDetails?.age === 0 ? null : calculateDetails?.age,
        'gender': null,
        'annualIncome': null,
        'goalCode': calculateDetails?.goalCode === "" ? null : calculateDetails?.goalCode,
        'otherGoalName': calculateDetails?.otherGoalName === "" ? null : calculateDetails?.otherGoalName,
        'typeCode': calculateDetails?.locationCode === "" ? "" : calculateDetails?.locationCode,
        'monthlyAmount': calculateDetails?.monthlyexpence === "" ? null : calculateDetails?.monthlyexpence,
        'presentValue': calculateDetails?.businessStartupCost != "" ? calculateDetails?.businessStartupCost : calculateDetails?.currentAmount,
        'risk': 'M',
        'goal_StartDate': calculateDetails.startDate === "" ? null : new Date(calculateDetails.startDate),
        'goal_EndDate': calculateDetails.endDate === "" || calculateDetails.endDate === "NaN-NaN-NaN" ? null : calculateDetails?.endDate,
        'inflationRate': formValues?.Inflation,
        'ror': formValues?.ROR,
        'PMT': formValues?.PMT,
        'downPaymentRate': calculateDetails?.goalCode === "FG4" ? '25' : null,
        'trnx_Type': arr?.trnx_Type,
        'getAmount': arr?.getAmount,
        'retirementAge': calculateDetails?.retirementAge === 0 ? null : calculateDetails?.retirementAge,
        'investAmount': arr?.investAmount,
        'people': calculateDetails.travelPeople === "" ? null : calculateDetails.travelPeople
      }
      // console.log(obj);
      const enc: any = encryptData(obj);
      // console.log(enc);
      const result: any = await updateGoal(enc);
      toastAlert("success", result?.status);
      setAPITrigger(false);
      setOpenEdit(false);
      fetchGoal();
      reload();
      fetchGoalNameCodeList();
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
    }
  }

  const fetchEditGoalData = async () => {
    try {

      if (!user?.basicid) return;
      //console.log(user?.basicid);
      setLoader(true);
      const enc: any = encryptData(editFormValues?.userGoalId || editFormValues?.UserGoalId, true);
      // console.log(enc);
      const result: any = await fetchGoalData(enc);
      console.log(result?.data);
      setGoalData(result?.data);
      const date = new Date(result?.data?.goal_EndDate);
      const MinDate = (date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDay()).padStart(2, '0'));
      setCalculateDetails({
        type: "CAMT",
        currentAmount: result?.data?.presentValue,
        startDate: result?.data?.goal_StartDate,//SH
        endDate: MinDate,
        goalCode: result?.data?.goalCode,
        otherGoalName: result?.data?.otherGoalName,
        childName: result?.data?.childName,
        travelPeople: result?.data?.people,
        locationCode: result?.data?.typeCode,
        budgetType: "",
        businessStartupCost: result?.data?.goalCode === "FG2" ? result?.data?.presentValue : "",
        monthlyexpence: result?.data?.monthlyAmount,
        age: result?.data?.age,
        retirementAge: result?.data?.retirementAge,
      })
      setLoader(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };
  useEffect(() => {
    console.log(editFormValues);
    fetchEditGoalData();
  }, [editFormValues])

  useEffect(() => {
    console.log(calculateDetails);
  }, [calculateDetails])

  return (
    <Box>
      <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
        <Box className="row">
          <Box className="col-auto text-light">
            {/* @ts-ignore */}
            <Text>Edit {selectedGoal.goalName}
            </Text>
          </Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      {loader ? <Loader /> :
        <Box className="modal-body modal-body-scroll p-3" css={{ fontSize: "0.8rem" }}>
          {editFormValues?.goalCode === "FG1" ?
            <Bike handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
            : editFormValues?.goalCode === "FG2" ?
              <Business handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
              : editFormValues?.goalCode === "FG3" ?
                <Car handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                : editFormValues?.goalCode === "FG4" ?
                  <House handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                  : editFormValues?.goalCode === "FG5" ?
                    <Study handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                    : editFormValues?.goalCode === "FG6" ?
                      <Travel handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                      : editFormValues?.goalCode === "FG7" ?
                        <Wedding handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                        : editFormValues?.goalCode === "FG8" ?
                          <Wealth handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                          : editFormValues?.goalCode === "FG9" ?
                            <Retirement handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                            : editFormValues?.goalCode === "FG10" ?
                              <Sabbatical handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                              : editFormValues?.goalCode === "FG11" ?
                                <FamilyPlanning handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                                : editFormValues?.goalCode === "FG12" ?
                                  <ChildStudy handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                                  : editFormValues?.goalCode === "FG13" ?
                                    <ChildWedding handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                                    : editFormValues?.goalCode === "FG14" ?
                                      <Other handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                                      : editFormValues?.goalCode === "FG17" ?
                                        <Emergency handleCalculate={handleCalculate} calculateDetails={calculateDetails} />
                                        : ""}
          {showCalcDetails ?
            <CalcResult handleSaveGoal={handleSaveGoal} calculatedDetails={calculatedDetails} setAPITrigger={setAPITrigger} APITrigger={APITrigger} />
            : ""}
        </Box>
      }
    </Box>
  );
};
export default EditGoal;
