import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { calcType } from "../GoalTypes";
import { Formik } from "formik";
import SelectMenu from "@ui/Select/Select";
import * as Yup from "yup";
import { useEffect, useMemo, useState } from "react";
import RetirementNew from "App/icons/RetirementNew";

const validationSchema = Yup.object().shape({
  age: Yup.number().required("Age is required").positive("Age should be greater than 0"),
  retirementAge: Yup.number().required("Retirement age is required").positive("Retirement Age should be greater than 0"),
  monthlyexpence: Yup.number().required("Expense is required").positive("Expense should be greater than 0"),
});

type BasicTypes = {
  handleCalculate: (values: calcType) => void;
  calculateDetails: calcType;
};

// main
const Retirement = ({ handleCalculate, calculateDetails }: BasicTypes) => {


  const [formValues, setformValues] = useState<calcType>(calculateDetails);



  return (
    <>
      {/* @ts-ignore */}
      <Formik
        initialValues={formValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleCalculate(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount
        }) => (
          <Box>
            <Box className="p-4">
              <Box className="row row-cols-1">
                <Box className="row pb-2">
                  <Box className="col text-center">
                    <RetirementNew size={50} />
                  </Box>
                </Box>
                <Box className="col"></Box>
                <Box className="col">
                  {/* @ts-ignore */}
                  <Text size="h6">What is your current age?</Text>
                  <Input label="Enter age in years" name="age" value={values.age}
                    onChange={handleChange}
                    error={submitCount ? errors.age : null} />
                </Box>
                <Box className="col">
                  {/* @ts-ignore */}
                  <Text size="h6">When do you want to retire?</Text>
                  <Input label="Enter age in years" name="retirementAge" value={values.retirementAge}
                    onChange={handleChange}
                    error={submitCount ? errors.retirementAge : null} />
                </Box>
                <Box className="col"></Box>
                <Box className="col">
                  {/* @ts-ignore */}
                  <Text size="h6">What is your current monthly expense?</Text>
                  <Input label="Enter amount in Rupees" name="monthlyexpence" value={values.monthlyexpence}
                    onChange={handleChange}
                    error={submitCount ? errors.monthlyexpence : null} />
                </Box>
                <Box className="col"></Box>
                <Box className="row">
                  <Box className="col text-end">
                    <Button color="yellow" onClick={handleSubmit}>
                      Calculate
                    </Button>
                  </Box>
                </Box>

              </Box>
            </Box>
          </Box>

        )}
      </Formik>
    </>
  );
};
export default Retirement;
