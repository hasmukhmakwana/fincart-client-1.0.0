import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { calcType } from "../GoalTypes";
import { Formik } from "formik";
import SelectMenu from "@ui/Select/Select";
import * as Yup from "yup";
import { useEffect, useMemo, useState } from "react";
import StudyNew from "App/icons/Study";

const validationSchema = Yup.object().shape({
  locationCode: Yup.string().required("Location is required"),
  endDate: Yup.string().required("Target Date is required"),
});

type BasicTypes = {
  handleCalculate: (values: calcType) => void;
  calculateDetails: calcType;
};

// main
const Study = ({ handleCalculate, calculateDetails }: BasicTypes) => {

  const locList = [
    { Value: "001", Text: "USA OR CANADA" },
    { Value: "002", Text: "EUROPE" },
    { Value: "003", Text: "SOUTH EAST ASIA" },
    { Value: "004", Text: "INDIA" },
    { Value: "005", Text: "OTHER" },
  ]
  const [formValues, setformValues] = useState<calcType>(calculateDetails);
  // const [show,setShow]=useState<boolean>(false);
  const [show, setShow] = useState<boolean>(formValues?.locationCode === "005" ? true : false);
  const [minDate, setMinDate] = useState<string>("");
  const [maxDate, setMaxDate] = useState<string>("");

  useEffect(() => {
    const date = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
    const MinDate = (date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDay()).padStart(2, '0'));
    const MaxDate = (date.getFullYear() + 99) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDay()).padStart(2, '0'));
    setMinDate(MinDate);
    setMaxDate(MaxDate)
  }, [])

  return (
    <>
      {/* @ts-ignore */}
      <Formik
        initialValues={formValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleCalculate(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount
        }) => (
          <Box>
            <Box className="p-4">
              <Box className="row row-cols-1">
                <Box className="row pb-2">
                  <Box className="col text-center">
                    <StudyNew size={50} />
                  </Box>
                </Box>
                <Box className="col"></Box>
                <Box className="col">
                  <Text size="h6">Where do you want to Study?</Text>
                  <SelectMenu
                    items={locList}
                    bindValue={"Value"}
                    bindName={"Text"}
                    label={"Location"}
                    placeholder={"Select Location"}
                    name="location"
                    value={values.locationCode}
                    onChange={(e: any) => {
                      setFieldValue("locationCode", e.Value);
                      if (e.Value === "005") {
                        setShow(true);
                      }
                      else {
                        setShow(false);
                      }
                    }}
                    required
                    error={submitCount ? errors.locationCode : undefined}
                  />
                </Box>
                <Box className="col"></Box>
                {show ? <>
                  <Box className="col">
                    <Text size="h6">Where is this place?</Text>
                    <Input label="Enter Place Name" name="otherGoalName" value={values.otherGoalName}
                      onChange={handleChange}
                      error={submitCount ? errors.otherGoalName : null} />
                  </Box>
                  <Box className="col"></Box>
                  <Box className="col">
                    <Text size="h6">What is the approximate cost?</Text>
                    <Input label="Enter Amount in Rupees" name="currentAmount" value={values.currentAmount}
                      onChange={handleChange}
                      error={submitCount ? errors.currentAmount : null} />
                  </Box>
                  <Box className="col"></Box>
                </> : ""}
                <Box className="col">
                  <Text size="h6">Target year for Study?</Text>
                  <Input label="Date" type="date" name="endDate" value={values.endDate}
                    min={minDate}
                    max={maxDate}
                    onChange={handleChange}
                    error={submitCount ? errors.endDate : null} />
                </Box>
                <Box className="col"></Box>
                <Box className="row">
                  <Box className="col text-end">
                    <Button color="yellow" onClick={handleSubmit}>
                      Calculate
                    </Button>
                  </Box>
                </Box>

              </Box>
            </Box>
          </Box>

        )}
      </Formik>
    </>
  );
};
export default Study;
