import Card from "@ui/Card";
import { Button } from "@ui/Button/Button";
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Input from "@ui/Input";
import { calcType } from "../GoalTypes";
import { Formik } from "formik";
import SelectMenu from "@ui/Select/Select";
import * as Yup from "yup";
import { useEffect, useMemo, useState } from "react";
import OtherNew from "App/icons/Other";

const validationSchema = Yup.object().shape({
  currentAmount: Yup.number().required("Amount is required").positive("Amount should be greater than 0"),
  endDate: Yup.string().required("Target Date is required"),
});

type BasicTypes = {
  handleCalculate: (values: calcType) => void;
  calculateDetails: calcType;
};

// main
const Emergency = ({ handleCalculate, calculateDetails }: BasicTypes) => {


  const [formValues, setformValues] = useState<calcType>(calculateDetails);
  const [minDate, setMinDate] = useState<string>("");
  const [maxDate, setMaxDate] = useState<string>("");

  useEffect(() => {
    const date = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
    const MinDate = (date.getFullYear()) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDay()).padStart(2, '0'));
    const MaxDate = (date.getFullYear() + 99) + "-" + (String(date.getMonth() + 1).padStart(2, '0')) + "-" + (String(date.getDay()).padStart(2, '0'));
    setMinDate(MinDate);
    setMaxDate(MaxDate)
  }, [])


  return (
    <>
      {/* @ts-ignore */}
      <Formik
        initialValues={formValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleCalculate(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          submitCount
        }) => (
          <Box>
            <Box className="p-4">
              <Box className="row row-cols-1">
                <Box className="row pb-2">
                  <Box className="col text-center">
                  <OtherNew size={50}/>
                  </Box>
                </Box>
                <Box className="col"></Box>
                <Box className="col">
                  {/* @ts-ignore */}
                  <Text size="h6">How much money would you need to buy a Emergency?</Text>
                  <Input label="Enter Amount in Rupees" name="currentAmount" value={values.currentAmount}
                    onChange={handleChange}
                    error={submitCount ? errors.currentAmount : null} />
                </Box>
                <Box className="col"></Box>
                <Box className="col">
                  {/* @ts-ignore */}
                  <Text size="h6">Target year to buy Emergency?</Text>
                  <Input label="Date" type="date" name="endDate" value={values.endDate}
                    min={minDate}
                    max={maxDate}
                    onChange={handleChange}
                    error={submitCount ? errors.endDate : null} />
                </Box>
                <Box className="col"></Box>
                <Box className="row">
                  <Box className="col text-end">
                    <Button color="yellow" onClick={handleSubmit}>
                      Calculate
                    </Button>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
        )}
      </Formik>
    </>
  );
};
export default Emergency;
