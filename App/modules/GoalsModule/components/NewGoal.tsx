import style from "../Goals.module.scss";
import Box from "../../../ui/Box/Box";
import { Button } from "@ui/Button/Button";
import { UiTypeTypes } from "../Goals";
import Education from "App/icons/Education";
import Retirement from "App/icons/Retirement";
import HomeFill from "App/icons/HomeFill";
import Bike from "App/icons/Bike";
import Business from "App/icons/Business";
import House from "App/icons/House";
import Travel from "App/icons/Travel";
import Study from "App/icons/Study";
import Wedding from "App/icons/Wedding";
import Wealth from "App/icons/Wealth";
import RetirementNew from "App/icons/RetirementNew";
import Sabbatical from "App/icons/Sabbatical";
import Car from "App/icons/Car";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import useGoalStore from "../store";
import FamilyPlanning from "App/icons/FamilyPlanning";
import Other from "App/icons/Other";
import BikeNew from "App/icons/BikeNew";

interface NewGoalTypes {
  handleModalState: (ui: UiTypeTypes) => void;
}

let goalList = [
  {
    name: "Bike Goal",
    icon: <BikeNew size="30" />,
    goalCode: "FG1",
  },
  {
    name: "Bussiness Goal",
    icon: <Business size="30" />,
    goalCode: "FG2",
  },
  {
    name: "Car Goal",
    icon: <Car size="30" />,
    goalCode: "FG3",
  },
  {
    name: "House Planning",
    icon: <House size="30" />,
    goalCode: "FG4",
  },
  {
    name: "Your Own Education",
    icon: <Study size="30" />,
    goalCode: "FG5",
  },
  {
    name: "Travel Planning",
    icon: <Travel size="30" />,
    goalCode: "FG6",
  },
  {
    name: "Wedding Planning",
    icon: <Wedding size="30" />,
    goalCode: "FG7",
  },
  {
    name: "Wealth Goal",
    icon: <Wealth size="30" />,
    goalCode: "FG8",
  },
  {
    name: "Retirement Goal",
    icon: <RetirementNew size="30" />,
    goalCode: "FG9",
  },
  {
    name: "Sabbatical Goal",
    icon: <Sabbatical size="30" />,
    goalCode: "FG10",
  },
  {
    name: "Family Planning",
    icon: <FamilyPlanning size="30" />,
    goalCode: "FG11",
  },
  {
    name: "Child Education Goal",
    icon: <Study size="30" />,
    goalCode: "FG12",
  },
  {
    name: "Child Wedding Planning",
    icon: <Wedding size="30" />,
    goalCode: "FG13",
  },
  {
    name: "Other Goal",
    icon: <Other size="30" />,
    goalCode: "FG14",
  },
]
const NewGoal = ({ handleModalState }: NewGoalTypes) => {
  const {
    setSelectedGoalCode, goalNameCodeist, setSelectedGoalName
  } = useGoalStore();
  return (
    <Box className="row">
      <Box className="col-md-12 col-sm-12 col-lg-12">
        <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
          <Box className="row">
            <Box className="col-auto text-light"><Text>New Goal</Text></Box>
            <Box className="col"></Box>
            <Box className="col-auto"></Box>
          </Box>
        </Box>
        <Box
          className="modal-body modal-body-scroll p-4 rounded-1"
          css={{ fontSize: "0.8rem" }}
        >

          <Box className="row row-cols-1 row-cols-md-3">
            {goalList.map((item, index) => {
              if (goalNameCodeist.filter(e => e.goalCode === item.goalCode).length > 0) {
                if (item.goalCode === "FG12" || item.goalCode === "FG13" || item.goalCode === "FG14") {
                  return (

                    <Box className="col mb-3">
                      <Card borderBlue css={{ p: 0 }}>
                        <Box className="row">
                          <Box className="col text-center">
                            <Box
                              className={style.iconBox}
                              display="inlineBlock"
                            >
                              {item.icon}
                              <Text size="h6" color="primary">{item.name}</Text>
                            </Box>
                          </Box>
                        </Box>
                        <Box className="row align-items-center">
                          <Box className="col text-center">
                            <Button color="yellow" className={style.selectButton} onClick={() => { handleModalState("add"); setSelectedGoalCode(item.goalCode); setSelectedGoalName(item.name); }}>Let's Start</Button>
                          </Box>
                        </Box>
                      </Card>
                    </Box>
                  )
                }
              }
              else {
                return (
                  <Box className="col mb-3">
                    <Card borderBlue css={{ p: 0 }}>
                      <Box className="row">
                        <Box className="col text-center">
                          <Box
                            className={style.iconBox}
                            display="inlineBlock"
                          >
                            {item.icon}
                            <Text size="h6" color="primary">{item.name}</Text>
                          </Box>
                        </Box>
                      </Box>
                      <Box className="row align-items-center">
                        <Box className="col text-center">
                          <Button color="yellow" className={style.selectButton} onClick={() => { handleModalState("add"); setSelectedGoalCode(item.goalCode); setSelectedGoalName(item.name); }}>Let's Start</Button>
                        </Box>
                      </Box>
                    </Card>
                  </Box>
                );
              }
            })}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default NewGoal;
