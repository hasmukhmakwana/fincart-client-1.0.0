import React, { useState } from 'react'
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import {
  deleteGoal
} from "App/api/goalPlan";
import useGoalStore from "../store";
import { encryptData } from 'App/utils/helpers';
import { toastAlert } from 'App/shared/components/Layout';
type SaveTypes = {
  IdPass: (values: string) => void;
  fetchGoalData: () => void;
  fetchGoalNameCodeList: () => void;
  reload:() =>void;
};

const DeleteGoal = ({ IdPass, fetchGoalData, fetchGoalNameCodeList}: SaveTypes) => {
  const { openDelete, setOpenDelete } = useGoalStore();
  const [APITrigger, setAPITrigger] = useState<Boolean>(false);
  const goalDelete = async (goalId: any) => {
    try {
      const enc: any = encryptData(parseInt(goalId));
      // console.log(enc);
      const result: any = await deleteGoal(enc);
      // console.log(result);
      setOpenDelete(false);
      toastAlert("success", "Goal Deleted Successfully");
      setAPITrigger(false);
      fetchGoalData();
      // reload();
      fetchGoalNameCodeList();
    } catch (error: any) {
      console.log(error);
      toastAlert("error", error);
    }
  }

  return (
    <Box className="row">
    <Box className="col-md-12 col-sm-12 col-lg-12">
      <Box className="modal-header p-3" css={{borderRadius: "15px 15px 0 0"}}>
        <Box className="row">
          <Box className="col-auto text-light"><Text>Delete Goal</Text></Box>
          <Box className="col"></Box>
          <Box className="col-auto"></Box>
        </Box>
      </Box>
      <Box
        className="text-center modal-body modal-body-scroll p-3"
        css={{ padding: "0.5rem" }}
      >
        <Text css={{ mt: 20, pt: 10 }}>
          Are you sure to delete this goal?

        </Text>
        <Box css={{ mt: 20, pt: 10 }} className="text-end">
          <Button
            type="submit"
            color="yellow"
            onClick={() => { setOpenDelete(false) }}>
            Cancel
          </Button>
          <Button
            type="submit"
            color="yellow"
            //@ts-ignore
            onClick={() => { setAPITrigger(true); goalDelete(IdPass); }} disabled={APITrigger}
          >
            Delete
          </Button>
        </Box>
      </Box>
    </Box>
    </Box>
  )
}

export default DeleteGoal
