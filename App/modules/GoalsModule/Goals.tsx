import DialogModal from "@ui/AlertDialog/ModalDialog";
import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import Text from "@ui/Text/Text";
import Holder from "App/shared/components/Holder/Holder";
import Loader from "App/shared/components/Loader";
import Layout, { toastAlert } from "App/shared/components/Layout";
import PageHeader from "App/shared/components/PageHeader/PageHeader";
import React, { useState, useEffect } from "react";
import style from "./Goals.module.scss";
import AddGoal from "./components/AddGoal";
import EditGoal from "./components/EditGoal";
import DeleteGoal from "./components/DeleteGoal";
import NewGoal from "./components/NewGoal";
import Education from "../../icons/Education";
import Retirement from "App/icons/Retirement";
import HomeFill from "App/icons/HomeFill";
import Bike from "App/icons/Bike";
import EditIcon from "App/icons/EditIcon";
import { TrashIcon } from "@radix-ui/react-icons";
import Coins from "App/icons/Coins";
import {
  fetchAllGoalData,
  getGoalNameCodeList
} from "App/api/goalPlan";
import useGoalStore from "./store";
import { encryptData, getUser } from "App/utils/helpers";
import { use } from "echarts";
import router from "next/router";
import Add from "App/icons/Add";
import useRegistrationStore from "App/modules/RegistrationModule/store";
export type UiTypeTypes = "category" | "add";

const GoalModule = () => {
  const user: any = getUser();
  const {
    open, setOpen, openEdit, setOpenEdit, setGoalNameCodeList, openDelete, setOpenDelete
  } = useGoalStore();
  // const [open, setOpen] = useState<boolean>(false);
  const [uiType, setuiType] = useState<UiTypeTypes>("category");
  const [allGoalData, setAllGoalData] = useState<any>([]);
  const [loader, setLoader] = useState<boolean>(false);
  const [selectedGoal, setSelectedGoal] = useState<any>([]);
  const [goalId, setGoalId] = useState<string>("");
  const { setBasicID, setRegiType } = useRegistrationStore();
  const handleModalState = (ui: UiTypeTypes) => {
    setuiType(ui);
  };

  const getUiType = () => {
    const compList: any = {
      category: <NewGoal handleModalState={handleModalState} />,
      add: <AddGoal handleModalState={handleModalState} fetchGoalData={fetchGoalData} fetchGoalNameCodeList={fetchGoalNameCodeList} />,
    };

    return compList[uiType];
  };
  const fetchGoalData = async () => {
    try {
      setLoader(true);
      if (!user?.basicid) return;
      //console.log(user?.basicid);

      // const enc: any = encryptData(user?.basicid, true);
      // console.log(enc);
      const result: any = await fetchAllGoalData();
      // console.log(result?.data);
      setAllGoalData(result?.data);
      setLoader(false);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };
  const fetchGoalNameCodeList = async () => {
    try {

      if (!user?.basicid) return;
      //console.log(user?.basicid);
      setLoader(true);

      // const enc: any = encryptData(user?.basicid, true);
      // console.log(enc);
      const result: any = await getGoalNameCodeList();
      setLoader(false);
      // console.log(result?.data);
      setGoalNameCodeList(result?.data);
      //setAllGoalData(result?.data);
    } catch (error) {
      console.log(error);
      toastAlert("error", error);
      setLoader(false);
    }
  };

  useEffect(() => {
    (async () => {
      await fetchGoalData();
      await fetchGoalNameCodeList();
  })();
  }, [])
  return (
    <Layout>
      <PageHeader title="My Goals" rightContent={<></>} />
      <Box className="row p-4" css={{ backgroundColor: "white", borderRadius: "8px" }}>
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="row text-end">
            <Box className="col-lg-12 col-md-12 col-sm-12 mb-1">
              <Button color="yellow" size="md"
                onClick={() => {
                  setuiType("category");
                  setOpen(true);
                }}>
                Initiate New Goal <Add></Add>
              </Button>
            </Box>
          </Box>

          {/* <Box className="container-fluid"> */}
          <Box className="row row-cols-1 row-cols-md-2">
            {loader ?
              <Box className="col-lg-12 col-md-12 col-12">
                <Card css={{ p: 15, border: "solid 1px #b3daff" }}>
                  <Loader /></Card></Box>
              : <>
                {allGoalData.length === 0 ? <>
                  <Box className="col-lg-12 col-md-12 col-12">
                    <Card css={{ p: 15, border: "solid 1px #b3daff" }}>
                      Let's Initiate New Goal
                    </Card></Box>
                </> : <>
                  {allGoalData.map((item: any, index: number) => {
                    return (
                      <Box className="col-lg-4 col-md-6 col-12">
                        <Card css={{ p: 15, border: "solid 1px #b3daff" }}>
                          <Box className="row">
                            <Box className="col-4 col-lg-3">
                              <Box className={style.imgbg} display="inlineBlock">
                                <img
                                  width={50}
                                  height={50}
                                  src={item?.goalImg}
                                  alt="logo"
                                />
                                {/* @ts-ignore */}
                                {/* <Text size="h8" color="white">
                                {item?.goalName} Goal
                              </Text> */}
                              </Box>
                            </Box>
                            <Box className="col-8 col-lg-9 text-left">
                              <Text>{item?.goalName}</Text>
                              {/* @ts-ignore */}
                              <Text size="h6" color="primary">Achieved {item?.goalPerc} %</Text>
                              <Text size="h6" className={`badge mt-2 rounded-pill me-1 ${style.titlepill}`} >Duration: {item?.duration} yr(s)</Text>
                            </Box>
                            {/* <Box className="col-3 text-right">
                            <Text size="h6" className={`badge rounded-pill me-1 ${style.titlepill}`} >Duration: {item?.duration}</Text>
                          </Box> */}
                          </Box>
                          <Box className="row my-3">
                            <Box className="col-6  text-center">
                              <Text color="primary">
                                Invested
                              </Text>
                              <Text
                                //@ts-ignore 
                                style={{ color: "#202020" }}>&#x20B9;{Number(item?.investedAmt).toLocaleString("en-IN")}
                              </Text>
                            </Box>
                            <Box className="col-6  text-center">
                              <Text color="primary">
                                {/* @ts-ignore  */}
                                <Text>Recommended <span className="border-bottom" style={{ fontSize: "0.50rem" }}>{item?.trxntype === "L" ? <>(Lump)</> : <>(SIP)</>}</span></Text>
                              </Text>
                              <Text
                                //@ts-ignore 
                                style={{ color: "#202020" }}>&#x20B9;{Number(item?.investAmount).toLocaleString("en-IN")}
                              </Text>
                            </Box>
                          </Box>

                          <Box className="row my-3">
                            <Box className="col-6  text-center">
                              <Text color="primary">
                                Current
                              </Text>
                              <Text
                                //@ts-ignore 
                                style={{ color: "#202020" }}>&#x20B9;{Number(item?.currentAmt).toLocaleString("en-IN")}
                              </Text>
                            </Box>
                            <Box className="col-6  text-center">
                              <Text color="primary">
                                Target
                              </Text>
                              <Text
                                //@ts-ignore 
                                style={{ color: "#202020" }}>&#x20B9;{Number(item?.getAmount).toLocaleString("en-IN")}
                              </Text>
                            </Box>
                          </Box>

                          <Box className="row align-items-center">
                            <Box className="col text-center">
                              {(item?.currentAmt > 0) ?
                                <Button
                                  color="yellow"
                                  size="md"
                                  className="d-inline-flex p-1 align-items-center px-2"
                                  onClick={() => {
                                    //add items to localStorage
                                    //set goalCode
                                    localStorage.setItem("userGoalId", item?.userGoalId)
                                    //set section
                                    localStorage.setItem("section", "GOAL_PLANNING")

                                    router.push("/GoalsInvestment")
                                  }}
                                >
                                  <Coins size="15" className="me-2" />
                                  {/* @ts-ignore */}
                                  <Text size="h6"
                                    //@ts-ignore
                                    color="gray8"
                                    className={style.button}
                                  //setGoalId(record.userGoalId);
                                  // setOpenTaxSavingInvest(true);
                                  >
                                    Invest More
                                  </Text>
                                </Button> :
                                <>
                                  {user?.cafstatus === "success" ? <>
                                    <Button
                                      color="yellow"
                                      size="md"
                                      className="d-inline-flex p-1 align-items-center px-2"
                                      onClick={() => {
                                        //add items to localStorage
                                        //set goalCode
                                        localStorage.setItem("userGoalId", item?.userGoalId)
                                        //set section
                                        localStorage.setItem("section", "GOAL_PLANNING")
                                        //set amount
                                        localStorage.setItem("amount", item?.investAmount)
                                        //set duration
                                        localStorage.setItem("duration", item?.duration)
                                        router.push("/RecommendedSchemes")
                                      }}
                                    >
                                      <Coins size="15" className="me-2" />
                                      {/* @ts-ignore */}
                                      <Text
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={style.button}
                                      >
                                        Invest
                                      </Text>
                                    </Button>
                                  </> : <>
                                    <Button
                                      color="yellow"
                                      size="md"
                                      className="d-inline-flex p-1 align-items-center px-2"
                                      onClick={() => {
                                        setRegiType("PENDING");
                                        setBasicID(user?.basicid);
                                        router.push("/Registration");
                                      }}
                                    >
                                      <Add size="15" className="me-2" />
                                      <Text
                                        size="h6"
                                        //@ts-ignore
                                        color="gray8"
                                        className={style.button}>
                                        Complete Reg.
                                      </Text>
                                    </Button>
                                  </>}
                                </>
                              }

                              {item?.isDelete === "Y" ?
                                <>
                                  <Button
                                    color="yellow"
                                    size="md"
                                    className="d-inline-flex p-1 align-items-center px-2"
                                    onClick={() => {
                                      setSelectedGoal(item);
                                      setOpenEdit(true);
                                    }}
                                  >
                                    <EditIcon size="15" className="me-2" />
                                    {/* @ts-ignore */}
                                    <Text size="h6" color="black">
                                      Edit
                                    </Text>
                                  </Button>
                                  <Button
                                    color="yellow"
                                    size="md"
                                    className="d-inline-flex p-1 align-items-center px-2"
                                    onClick={() => {
                                      setGoalId(item?.userGoalId);
                                      setOpenDelete(true);
                                    }}
                                  >
                                    {/* @ts-ignore */}
                                    <TrashIcon size="15" className="me-2" />
                                    {/* @ts-ignore */}
                                    <Text size="h6" color="black">
                                      Delete
                                    </Text>
                                  </Button>
                                </>
                                : ""}
                            </Box>
                          </Box>
                        </Card>
                      </Box>
                    );
                  })}</>}
              </>}
          </Box>
          {/* </Box> */}
        </Box>
      </Box>

      <DialogModal
        open={open}
        setOpen={setOpen}
        css={{
          padding: 0,
          "@bp0": { width: "100%" },
          "@bp1": { width: uiType === "add" ? "25%" : "40%" },
          zIndex: 2
        }}
      >
        {getUiType()}
      </DialogModal>
      <DialogModal
        open={openEdit}
        setOpen={setOpenEdit}
        css={{
          padding: 0,
          "@bp0": { width: "100%" },
          "@bp1": { width: uiType === "add" ? "25%" : "40%" },
          zIndex: 2
        }}
      >
        <EditGoal selectedGoal={selectedGoal} fetchGoal={fetchGoalData} fetchGoalNameCodeList={fetchGoalNameCodeList} reload={() => { }} />
      </DialogModal>
      <DialogModal
        open={openDelete}
        setOpen={setOpenDelete}
        css={{
          "@bp0": { width: "60%" },
          "@bp1": { width: "40%" },
        }}
      >
        {/* @ts-ignore */}
        <DeleteGoal IdPass={goalId} fetchGoalData={fetchGoalData} fetchGoalNameCodeList={fetchGoalNameCodeList} />

      </DialogModal>
    </Layout>
  );
};

export default GoalModule;
