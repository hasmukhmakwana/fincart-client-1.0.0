import Box from "@ui/Box/Box";
import Label from "@ui/Label/Label";
import Loader from "App/shared/components/Loader";
import { WEB_URL } from "App/utils/constants";
import style from './PaymentStatus.module.scss';

const PaymentStatusPage = () => {
    return (
        <>
            <Box className="container">
                {/* <div className="row">
                    <div className="col-12">
                        <img
                            src={`${WEB_URL}/logo.png`}
                            alt="logo"
                        />
                    </div>
                </div> */}
                <div className="row">
                    <div className="col-12">
                        &nbsp;
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 text-center">
                        <h3 className={`${style.titleHeader}`}>Processing ...</h3>
                        <Loader></Loader>
                    </div>
                </div>
            </Box>
        </>
    );
}

export default PaymentStatusPage;