import { log } from "console";
import { useEffect, useState } from "react";

const Timer = ({ CurrentDate }: any) => {
    const [days, setDays] = useState(0);
    const [hours, setHours] = useState(0);
    const [minutes, setMinutes] = useState(0);
    const [seconds, setSeconds] = useState(0);

    const getTime = () => {
        let deadline = new Date(CurrentDate);

        var tatDate = new Date(deadline.getFullYear(), deadline.getMonth(), deadline.getDate(), deadline.getHours(), deadline.getMinutes(), deadline.getSeconds());

        const time = tatDate.getTime() - new Date().getTime();

        setDays(Math.floor(time / (1000 * 60 * 60 * 24)));
        setHours(Math.floor((time / (1000 * 60 * 60)) % 24));
        setMinutes(Math.floor((time / 1000 / 60) % 60));
        setSeconds(Math.floor((time / 1000) % 60));
    };

    useEffect(() => {
        const interval = setInterval(() => getTime(), 1000);

        return () => clearInterval(interval);
    }, []);

    return (
        <div className="timer">
            <div className="col-12">
                <div className="box mt-1">
                    <p id="day">{days < 10 ? "0" + days : days} Day(s)
                        : {hours < 10 ? "0" + hours : hours} Hrs
                        : {minutes < 10 ? "0" + minutes : minutes} Mins
                        : {seconds < 10 ? "0" + seconds : seconds} Secs</p>
                </div>
            </div>
        </div>
    );
};

export default Timer;