import { useState } from "react";
import FullPageLoader from "../components/FullPageLoader";

export default () => {
  const [loader, setLoader] = useState(false);
  return [
    loader ? <FullPageLoader /> : null,
    () => setLoader(true),
    () => setLoader(false),
  ];
};
