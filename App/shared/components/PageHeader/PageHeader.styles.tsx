//@ts-nocheck
import Box from "@ui/Box/Box";
import { styled } from "App/theme/stitches.config";


export const HeadContainer = styled(Box, {
    
    position: "relative",
    top: "0px",
    // zIndex: 1099,
    zIndex: 1,
    bg: "#f5f5f5",
    // borderRadius: "10px 10px 0px 0px",
    // display:"flex",
    // justifyContent: "space-between",
    alignItems: "center",
    "@bp0": {
        // display: 'block',
        display: 'flex',
        justifyContent: "space-between",
        padding: "10px 10px 10px 10px",
        // mb: '$10',
        mb: '$5'
    },
    "@bp2": {
        display: 'block',
        // justifyContent: "space-between",
        // padding: "20px 20px 10px 20px",
        // padding: "20px 20px 0px 20px",
        padding: "10px 20px 0px",
        textAlign: 'center',
        justifyContent: 'end',
    },
})