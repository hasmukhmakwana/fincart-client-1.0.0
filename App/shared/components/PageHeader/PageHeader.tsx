import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React from "react";
import { HeadContainer } from "./PageHeader.styles";
function PageHeader({
  title,
  rightContent,
}: {
  title: string;
  rightContent: React.ReactNode;
}) {
  return (
    <HeadContainer>
      {/* <Box>
       
      </Box> */}
      {/* <Box className="d-none d-md-flex justify-content-end">{rightContent}</Box> */}

      <Box className="container m-0 p-0">
        <Box className="row justify-content-between">
          <Box className="col-2"></Box>
          <Box className="col-8 text-center "> {/* @ts-ignore */}
            <Text weight="normal" size="h10" transform="uppercase">
              {title}
            </Text></Box>
          <Box className="col-2 px-0">
            <Box className="d-md-flex justify-content-end">{rightContent}
            </Box>
          </Box>
        </Box>
      </Box>
    </HeadContainer>
  );
}
export default PageHeader;
