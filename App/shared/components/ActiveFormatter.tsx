import Pill from "@ui/Pill/Pill";
import React, { memo } from "react";


function ActiveFormatter({ value }: { value: 0 | 1 }) {

  return (
    <span className={`pill pill_${value ? "Active" : "Inactive"}`}>
      <Pill>
        {value ? "Active" : "Inactive"}
      </Pill>
    </span>
  );
}

export default memo(ActiveFormatter);
