import React, { useState, useEffect } from 'react';

const NoInternetConnection = (props: any) => {
    // state variable holds the state of the internet connection
    const [isOnline, setOnline] = useState(true);

    // On initization set the isOnline state.
    useEffect(() => {
        setOnline(navigator.onLine)
        window.addEventListener('online', () => {
            setOnline(true)
        });
    
        window.addEventListener('offline', () => {
            setOnline(false)
        });
    
    }, [])

    // if user is online, return the child component else return a custom component
    if (isOnline) {
        return (
            props.children
        );
    } else {
        return (
            <div className="row my-4 justify-content-center" >
            <div className="col-auto m-2 p-4  rounded-4" style={{background:"var(--colors-blue)"}}>
                <div className="px-md-4 my-4 text-center" >
                    <h5 style={{color:"#005cb3"}}>No Internet Connection.<br/> Please try again later or Check Connection.</h5>
                </div>
            </div>
            </div>
        );
    }
}

export default NoInternetConnection;