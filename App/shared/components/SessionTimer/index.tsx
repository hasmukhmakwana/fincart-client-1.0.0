import Box from "@ui/Box/Box";
import ModalDialog from "@ui/AlertDialog/ModalDialog";
import { Button } from "@ui/Button/Button";
import { useEffect, useState } from "react";
import { checkSessionTimer } from "App/utils/helpers";
import { useRouter } from "next/router";
import { refreshToken } from "App/utils/api";
import useLoginStore from "App/modules/LoginModule/store";
export default ({ active, show, setShow ,setIdleLogout}: any) => {
  const router = useRouter();
  const { loginStatus, setLoginStatus } = useLoginStore();
  //const [show, setShow] = useState<boolean>(false);
  const [Timer, setTimer] = useState<number>(0);
  // const [showTime, setShowTime] = useState<number>(0);
  const [showTime, setShowTime] = useState<number>(60);


  
  useEffect(() => {
    let interval:any=null
    if(show){
       interval = setInterval(() => { setShowTime((prev) => prev - 1) }, 1000);
      if(showTime <= 0){
         
        setShow(false);
        localStorage.clear();
        router.push("/Login");
        setLoginStatus(true)
       
      }
    }
    return () => {
      clearInterval(interval);
    };
   
  }, [showTime, show]);



  // useEffect(() => {
  //   const sessionData: any = checkSessionTimer() || {};
  //   const t: number = Math.round(sessionData?.remainingTime / 1000);
  //   // console.log({ t, showT: t - 60 });const events = ['click', 'scroll', 'load', 'keydown', 'mousemove', 'keypress'];
  //   // setShowTime(t - 60);

  //   // if (t <= 60) {
  //   //   logout();
  //   //   setLoginStatus(true);
  //   // }
  //   if (t <= 180) {
  //     //setShow(true)
  //     extend();
  //   }
  //   // console.log(Timer,"  Timer");
  //   const interval = setInterval(() => { setTimer((prev) => prev + 1) }, 1000);//active && 
  //   return () => {
  //     clearInterval(interval);
  //   };
  // }, [Timer, active]);

  const logout = () => {
    setShow(false);
    localStorage.clear();
    router.push("/Login");
  };

  const extend = async () => {
    setShow(false);

    //* refresh token
    try {
      await refreshToken();
      setTimer(0);
    } catch (error) {
      console.log(error);
      logout();
    }
  };

  return (
    <Box>
      <ModalDialog
        open={show}
        setOpen={setShow}
        hideCloseBtn={true}
        css={{
          "@bp0": { width: "50%" },
          "@bp1": { width: "20%" },
        }}
      >
        <Box className="col-md-12 col-sm-12 col-lg-12">
          <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
            <Box className="row">
              <Box className="col-auto text-light">Session Expire Warning</Box>
              <Box className="col"></Box>
              <Box className="col-auto"></Box>
            </Box>
          </Box>
          <Box className="modal-body p-3">
            <Box className="my-1 mx-2">
              <Box css={{ padding: "0.3rem", fontSize: "0.9rem" }}>
                Oops! your session will expire in {showTime} seconds.
              </Box>

              <Box css={{ padding: "0.5rem", fontSize: "0.85rem" }}>
                Do you want to extend the session?
              </Box>

              <Box css={{ pt: 10 }} className="text-end">
                <Button color="yellow" onClick={extend}>
                  Extend
                </Button>
                <Button color="yellow" onClick={logout}>
                  Logout now
                </Button>
              </Box>
            </Box>
          </Box>
        </Box>
      </ModalDialog>
    </Box>
  );
};
