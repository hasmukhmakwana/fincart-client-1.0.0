import Box from "@ui/Box/Box";
import ModalDialog from "@ui/AlertDialog/ModalDialog";
import { Button } from "@ui/Button/Button";
import { useEffect, useState } from "react";
import { checkSessionTimer } from "App/utils/helpers";
import { useRouter } from "next/router";
import { refreshToken } from "App/utils/api";

export default () => {
  const router = useRouter();
  const [show, setshow] = useState<boolean>(false);
  const [Timer, setTimer] = useState<number>(0);
  const [showTime, setshowTime] = useState<number>(0);

  useEffect(() => {
    window.addEventListener("keydown", (event) => {
      alert("yes");
    });

    return () => {
      window.removeEventListener("keydown", (event) => {
        alert("removed");
      });
    };
  }, []);
  

  return (
    <Box>
      <div className="container">
        <h1>Welcome to the Keydown Listening Component</h1>
      </div>
    </Box>
  );
};
