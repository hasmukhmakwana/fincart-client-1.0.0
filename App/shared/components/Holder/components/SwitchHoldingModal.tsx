import React from 'react'
import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { Button } from "@ui/Button/Button";
import style from "../Holder.module.scss"
import { GroupBox } from "@ui/Group/Group.styles";
import useSwitchHoldingStore from "../store";

import Check from 'App/icons/Check';
import Loader from '@ui/SimpleGrid/components/Loader';

type NameEventTypes = {
    setOpen: (value: boolean) => void;
};
const SwitchHoldingModal = ({ setOpen }: NameEventTypes) => {

    const {
        switchLoading,
        investor,
        mohMember,
        setSelectmember,
    } = useSwitchHoldingStore();


    return (
        <>
            <Box className="col-12 w-auto">
                <Box className="modal-header p-3" css={{ borderRadius: "15px 15px 0 0" }}>
                    {/* @ts-ignore */}
                    <Text css={{ color: "var(--colors-blue1)" }}> Switch Investor</Text>
                </Box>
                <Box className="modal-body p-3">
                    <Box className="row border-primary m-0">
                        {switchLoading ? <><Loader /></> : <>
                            {mohMember?.length === undefined || mohMember?.length === 0 ? <>
                                <Box className="m-3">No Investor to Switch</Box>
                            </> : <>
                                {
                                    mohMember.map((record) => {
                                        return (
                                            <>
                                                <Box className="col-sm-6 col-md-7 col-lg-7 mt-1 text-wrap">
                                                    <Text css={{ mb: 15 }}>
                                                        {record?.memberName}
                                                    </Text>
                                                </Box>
                                                <Box className="col-sm-6 col-md-5 col-lg-4 text-wrap text-end m-2 p-0">
                                                    <Button
                                                        color="yellowGroup"
                                                        size="md"
                                                        onClick={() => {
                                                            // @ts-ignore
                                                            setSelectmember(record);
                                                            setOpen(false);
                                                        }}
                                                        disabled={switchLoading}
                                                    >

                                                        <GroupBox align="center">

                                                            <Box>
                                                                {" "}
                                                                <Check></Check>
                                                            </Box>
                                                            <Box>
                                                                <Text
                                                                    // @ts-ignore 
                                                                    size="h6"
                                                                    //@ts-ignore
                                                                    color="gray8"
                                                                    className={style.button}
                                                                >
                                                                    Select
                                                                </Text>
                                                            </Box>
                                                        </GroupBox>
                                                    </Button>
                                                </Box>
                                            </>)
                                    })
                                }
                            </>}
                        </>}
                    </Box>
                </Box>
            </Box>


            {/* <Box className="row border-bottom border-primary">
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                                * @ts-ignore *
                                <Text color="mediumBlue" css={{ mb: 10 }} >First Applicant</Text>
                                * @ts-ignore *
                                <Text css={{ mb: 15 }}>Aryan Ashish Kamath</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                                @ts-ignore 
                                <Text color="mediumBlue" css={{ mb: 10 }} >Second Applicant</Text>
                                @ts-ignore 
                                <Text css={{ mb: 15 }}>Swati Aryan Kamath</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                                * @ts-ignore *
                                <Text color="mediumBlue" css={{ mb: 10 }} >Third Applicant</Text>
                                * @ts-ignore *
                                <Text css={{ mb: 15 }}>-</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-11 col-sm-6 mt-lg-2 my-md-2 text-md-end text-lg-start text-wrap">
                                <Button
                                    // className="col-auto mt-3"
                                    color="yellowGroup"
                                    size="md"
                                >
                                    * @ts-ignore  *
                                    <GroupBox align="center">
                                        <Box>
                                            {" "}
                                            <Shuffle></Shuffle>
                                        </Box>

                                        <Box>
                                            <Text
                                                // @ts-ignore 
                                                weight="bold"
                                                size="h6"
                                                //@ts-ignore
                                                color="gray8"
                                                className={style.button}
                                            >
                                                Switch
                                            </Text>
                                        </Box>
                                    </GroupBox>
                                </Button>
                            </Box>
                        </Box>
                        <Box className="row border-bottom border-primary">
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                            * @ts-ignore *
                                <Text weight="normal" color="mediumBlue" css={{ mb: 10, mt: 15 }} >First Applicant</Text>
                            * @ts-ignore *
                                <Text css={{ mb: 15 }}>Swati Aryan Kamath</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                            * @ts-ignore *
                                <Text weight="normal" color="mediumBlue" css={{ mb: 10, mt: 15 }} >Second Applicant</Text>
                            * @ts-ignore *
                                <Text css={{ mb: 15 }}>Aryan Ashish Kamath</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                            * @ts-ignore *
                                <Text weight="normal" color="mediumBlue" css={{ mb: 10, mt: 15 }} >Third Applicant</Text>
                            * @ts-ignore *
                                <Text css={{ mb: 15 }}>-</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-11 col-sm-6 mt-lg-4 my-md-2 text-md-end text-lg-start text-wrap">
                                <Button
                                    //className="col-auto mt-4"
                                    color="yellowGroup"
                                    size="md"
                                >
                                * @ts-ignore  *
                                    <GroupBox align="center">
                                        <Box>
                                            {" "}
                                            <Shuffle></Shuffle>
                                        </Box>

                                        <Box>
                                            <Text
                                                // @ts-ignore 
                                                weight="bold"
                                                size="h6"
                                                //@ts-ignore
                                                color="gray8"
                                                className={style.button}
                                            >
                                                Switch
                                            </Text>
                                        </Box>
                                    </GroupBox>
                                </Button>
                            </Box>
                        </Box>
                        <Box className="row">
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                            * @ts-ignore *
                                <Text weight="normal" color="mediumBlue" css={{ mb: 10, mt: 15 }} >First Applicant</Text>
                            * @ts-ignore *
                                <Text css={{ mb: 15 }}>Aryan Ashish Kamath</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                            * @ts-ignore *
                                <Text weight="normal" color="mediumBlue" css={{ mb: 10, mt: 15 }} >Second Applicant</Text>
                            * @ts-ignore *
                                <Text css={{ mb: 15 }}>Swati Aryan Kamath</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-4 col-sm-6 mt-sm-1 mt-1 text-wrap">
                            * @ts-ignore *
                                <Text weight="normal" color="mediumBlue" css={{ mb: 10, mt: 15 }} >Third Applicant</Text>
                            * @ts-ignore *
                                <Text css={{ mb: 15 }}>Sakshi Aryan Kamath</Text>
                            </Box>
                            <Box className="col-lg-3 col-md-11 col-sm-6 mt-lg-4 my-md-2 text-md-end text-lg-start text-wrap">
                                <Button
                                    //className="col-auto mt-4"
                                    color="yellowGroup"
                                    size="md"
                                >
                                * @ts-ignore  *
                                    <GroupBox align="center">
                                        <Box>
                                            {" "}
                                            <Shuffle></Shuffle>
                                        </Box>

                                        <Box>
                                            <Text
                                                // @ts-ignore 
                                                weight="extrabold"
                                                size="h6"
                                                //@ts-ignore
                                                color="gray8"
                                                className={style.button}
                                            >
                                                Switch
                                            </Text>
                                        </Box>
                                    </GroupBox>
                                </Button>
                            </Box>
                        </Box> */
            }
        </>
    )
}

export default SwitchHoldingModal