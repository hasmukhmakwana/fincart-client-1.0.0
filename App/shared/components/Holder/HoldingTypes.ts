export type MOHTypes = {
    Text: string;
    Value: string;
    Category: string;
}

export type MOHmember = {
    memberName: string;
    basicid: string;
    isNominee: boolean;
}