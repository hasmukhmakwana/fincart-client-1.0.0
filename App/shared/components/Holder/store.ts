import create from "zustand";

import { MOHTypes, MOHmember } from "./HoldingTypes"

interface HoldingTypes {
    switchLoading: boolean;
    investor: string;
    mohList: MOHTypes[];
    mohMember: MOHmember[];
    selectMember: MOHmember;
    setSelectmember: (payload: MOHmember) => void;
    setSwitchLoader: (payload: boolean) => void;
    setInvestor: (payload: string) => void;
    setMOHList: (payload: MOHTypes[]) => void;
    setMOHmember: (payload: MOHmember[]) => void;
}


const useSwitchHoldingStore = create<HoldingTypes>((set) => ({
    //* initial state
    switchLoading: false,
    investor: "",
    investmentProfile: [],
    mohList: [],
    mohMember: [],
    mandateList: [],
    transactionList: [],
    selectMember: {
       memberName: "All",
          basicid: "0",
          isNominee: false,
    },

    setSelectmember: (payload) =>
        set((state) => ({
            ...state,
            selectMember: payload,
        })),

    setInvestor: (payload) =>
        set((state) => ({
            ...state,
            investor: payload,
        })),

    setMOHList: (payload) =>
        set((state) => ({
            ...state,
            mohList: payload,
        })),

    setMOHmember: (payload) =>
        set((state) => ({
            ...state,
            mohMember: payload,
        })),

    setSwitchLoader: (payload) =>
        set((state) => ({
            ...state,
            loading: payload,
        })),
}));

export default useSwitchHoldingStore;