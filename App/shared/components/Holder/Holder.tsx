import Box from "@ui/Box/Box";
import { GroupBox } from "@ui/Group/Group.styles";
import Text from "@ui/Text/Text";
import { useRouter } from "next/router";
import React, { useState } from "react";
import style from "./Holder.module.scss";
import SwitchHoldingModal from "./components/SwitchHoldingModal";
import DialogModal from "@ui/ModalDialog/ModalDialog";
import useSwitchHoldingStore from "./store";
import { getUser } from "App/utils/helpers";
import Loader from "../Loader";


const Holder = ({memberList}:any) => {
  let local: any = getUser();
  const [openSwitch, setOpenSwitch] = useState(false);
  const {
    switchLoading,
    selectMember,
  } = useSwitchHoldingStore();
  const router = useRouter();

  
  
  return (
    <>
      <Box>
        <Box className="row">
          {switchLoading ? <><Loader /></> : <>
            <Box className="col-auto pe-0">
              <GroupBox>
                <Box className={`${style.holderName}`}>
                  {/* @ts-ignore */}
                  <Text size="bt" color="primary">{selectMember?.memberName}</Text>
                  {/* {console.log(selectMember,"selectMember holder")} */}
                </Box>
              </GroupBox>
            </Box>
            <Box className="col-auto pe-2 ps-1">
              <GroupBox>
                <Box>
                  <img
                    width={25}
                    height={25}
                    className={`${style.icon}`}
                    src={"/SwitchHoldingIcon.png"}
                    alt="Switch Holding"
                    // @ts-ignore
                    onClick={setOpenSwitch}
                  />
                </Box>
              </GroupBox>
            </Box>
          </>}
        </Box>
      </Box>
      <DialogModal
        open={openSwitch}
        setOpen={setOpenSwitch}
        css={{
          "@bp0": { width: "auto" },
          "@bp1": { width: "auto" },
        }}
      >
        <SwitchHoldingModal setOpen={setOpenSwitch} />
      </DialogModal>
    </>
  );
};

export default Holder;
