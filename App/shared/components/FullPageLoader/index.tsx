import Box from "@ui/Box/Box";
import styles from "./Loader.module.scss";

export default () => {
  return (
    <Box className={styles.full_page_loader_main_div}>
      <button className="btn border-dark" disabled>
        <span
          className="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        ></span>
      </button>
    </Box>
  );
};
