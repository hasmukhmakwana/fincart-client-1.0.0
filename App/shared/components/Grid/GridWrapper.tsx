import Box from "@ui/Box/Box";
import Card from "@ui/Card";
import React from "react";

function GridWrapper({ children }: { children: React.ReactNode }) {
  return (
    <Box className="pageContent_">
      {/* <Card css={{ p: "$5 $0 $5 $0" }}>{children}</Card> */}
      {/* <Card css={{ p: "$0 $0 $0 $0" }}>{children}</Card> */}
      {children}
    </Box>
  );
}

export default GridWrapper;
