import Box from '@ui/Box/Box'
import { Button } from '@ui/Button/Button'
import React from 'react'
import Text from '@ui/Text/Text'
// import useLoginStore from './store'
const SessionEnd = ({ setOpen }: any) => {
    return (
        <Box className="col-md-12 col-sm-12 col-lg-12">
            <Box className="modal-header p-3" css={{borderRadius: "15px 15px 0 0"}}>
                <Box className="row justify-content-start">
                    <Box className="col-auto">
                    <Text css={{ color: "var(--colors-blue1)" }}>Your session has timed out !</Text></Box>
                </Box>
            </Box>
            <Box className="modal-body p-3">
                <Box className="my-1 mx-2">
                    <Box css={{ padding: "0.3rem", fontSize: "0.9rem" }}>
                        As the system was idle for long time, it was logged out
                    </Box>
                    <Box css={{ pt: 10 }} className="text-end">
                        <Button color="yellow" onClick={() => { setOpen() }}>
                            OK
                        </Button>
                    </Box>
                </Box>
            </Box>
        </Box>

    )
}

export default SessionEnd;