import create from "zustand";

interface StoreTypes {
  userInfo: any;
  setUserInfo: (payload: any) => void;
  cartCount: number;
  setCartCount: (payload: number) => void;
  showFeedback: boolean;
  setShowFeedback: (payload: boolean) => void;
}

const useHeaderStore = create<StoreTypes>((set) => ({
  //* initial state
  userInfo: null,
  cartCount: 0,
  showFeedback: false,
  //* methods for manipulating state
  setUserInfo: (payload) =>
    set((state) => ({
      ...state,
      userInfo: payload || null,
    })),
  setCartCount: (payload) =>
    set((state) => ({
      ...state,
      cartCount: payload || 0,
    })),
  setShowFeedback: (payload) =>
    set((state) => ({
      ...state,
      showFeedback: payload || false,
    }))
}));

export default useHeaderStore;
