import { useRouter, NextRouter } from "next/router";
import LogoutIcon from "App/icons/LogoutIcon";
import styles from "./Header.module.scss";
import { Button } from "App/ui/Button/Button";
import Badge from "App/ui/Badge/Badge";
import DropdownStyled from "App/ui/Dropdown/Dropdown";
import Text from "App/ui/Text/Text";
import { DropdownGroup, TriggerButton } from "./Header.styles";
import { useContext, useEffect, useState } from "react";
import Bell from "App/icons/Bell";
import Lock from "App/icons/Lock";
import Link from "next/link";
import BellFill from "App/icons/BellFill";
import CartFill from "App/icons/CartFill";
import Gauge from "App/icons/Gauge";
import BoxesStacked from "App/icons/BoxesStacked";
import Coins from "App/icons/Coins";
import Personfill from "App/icons/Personfill";
import BarChart from "App/icons/BarChart";
import Tools from "App/icons/Tools";
import Support from "App/icons/Support";
import { GroupBox } from "@ui/Group/Group.styles";
import Box from "@ui/Box/Box";
import { WEB_URL } from "App/utils/constants";
import useHeaderStore from "./store";
import { getUser } from "App/utils/helpers";
import SettingIcon from "App/icons/SettingIcon";
import PhoneSquare from "App/icons/PhoneSquare";
import UiContext from "App/contexts/ui/ui.context";
import clsx from "clsx";
import ListIcon from "App/icons/ListIcon";
import { getNotificationData } from "App/api/notification";
import EmailIcon from "App/icons/EmailIcon";
import useContactUsStore from 'App/modules/SupportModule/ContactUs/store';

//action type for dropdown
export type actionTypes = {
  name: "Profile" | "Change Password" | "Logout";
  id: number;
  icon?: any;
};
export type navActionTypes = {
  name: string;
  id: number;
  url: string;
};

//app actions
export const ACTIONS: actionTypes[] = [
  { name: "Change Password", id: 2, icon: <Lock /> },
  { name: "Logout", id: 3, icon: <LogoutIcon /> },
];

const NOTIFICATION_ACTION: any = [
  { name: "Sample notification", id: 1, icon: <Bell /> },
];

const REMINDERS_ACTION: any = [];

const CART_ACTION: any = [{ name: "Sample Cart", id: 1, icon: <Bell /> }];

export const PRODUCT_ACTIONS: navActionTypes[] = [
  { name: "Mutual Fund", id: 1, url: "/Products/MutualFund" },
  { name: "ULIP", id: 2, url: "/Products/ULIP" },
  { name: "PMS", id: 3, url: "/Products/PMS" },
  { name: "LiquiLoans", id: 4, url: "/Products/Liquiloan" },
  { name: "Insurance", id: 5, url: "/Products/Insurance" },
  { name: "Protection", id: 5, url: "/Products/Protection" },
  // { name: "NPS", id: 6, url: "" },
  // { name: "EPF", id: 7, url: "/Products/EPF" },
  // { name: "Digital Gold", id: 8, url: "" },
];

export const PLAN_ACTIONS: navActionTypes[] = [
  { name: "Financial Plan", id: 1, url: "/FinancialPlan" },
  { name: "Goals", id: 2, url: "/Goals/MyGoals" }
];

export const Profile_ACTIONS: navActionTypes[] = [
  { name: "View", id: 1, url: "/Profile" },
  { name: "Investment Account", id: 2, url: "/Investment" },
  { name: "Mandates", id: 3, url: "/Mandate/MandateList" },

];

export const Report_ACTIONS: navActionTypes[] = [
  // { name: "Overall", id: 1, url: "" },
  // { name: "Capital Gains", id: 2, url: "" },
  // { name: "Product Wise", id: 3, url: "" },
  { name: "Transactions Statement", id: 1, url: "/Passbook" },
  { name: "Overall MF Report", id: 2, url: "/OverallReport" },
];

export const Tools_ACTIONS: navActionTypes[] = [
  // { name: "Short Financial Plan", id: 1, url: "" },
  { name: "Calculators", id: 2, url: "/Tools/Calculator" },
  { name: "Quick SIP", id: 3, url: "/Tools/QuickSIP" },
  { name: "Tax Saving", id: 4, url: "/Tools/TaxSaving" },
  { name: "Upload CAS", id: 5, url: "/Products/UploadCas" },
];

export const Support_ACTIONS: navActionTypes[] = [
  { name: "FAQ", id: 1, url: "/Support/FAQ" },
  { name: "Raise a Ticket", id: 2, url: "/Support/RaiseTicket" },
  { name: "Feedback", id: 3, url: "/Support/Feedback" },
  { name: "Contact Us", id: 4, url: "/Support/ContactUs" },
];

export const Testing_ACTIONS: navActionTypes[] = [
  { name: "Registration", id: 1, url: "/Registration" },
  { name: "Create Mandate", id: 2, url: "/Mandate/RegisterMandate" },
  { name: "Mandate List", id: 3, url: "/Mandate/MandateList" },
];

function Header(props: any) {
  //@ts-ignore
  const { uiState, uiDispatch } = useContext(UiContext);
  const router: NextRouter = useRouter();
  const { setContactUsVal } = useContactUsStore();
  const { userInfo, cartCount, setCartCount } = useHeaderStore();
  const [activeLink, setActiveLink] = useState<string | null>(null);
  const [userData, setuserData] = useState<any>(userInfo || getUser());


  const [remindersAction, setRemindersAction] = useState<any[]>([]);
  // const [cartCount, setCartCount] = useState(0);

  const fetchNotification = async () => {
    try {
      let result = await getNotificationData();

      let currentObjColl: any = [];

      if (result?.data?.sysmSipTopReminders != undefined &&
        result?.data?.sysmSipTopReminders.length != 0) {

        let iCounter = 1;

        result?.data?.sysmSipTopReminders.map((record: any) => {
          let currentObj = {
            name: `${record?.trxnType} of ${record?.amount} on ${record?.sysmDate}`,
            id: iCounter,
            url: '/UpcomingTransactions'
          }
          currentObjColl.push(currentObj);
          iCounter += 1;
        })

        result?.data?.birthdayAlert.map((record: any) => {
          let currentObj = {
            name: `Happy Birthday ${record?.userName}`,
            id: iCounter,
            url: '/Notification'
          }

          currentObjColl.push(currentObj);
          iCounter += 1;
        })

        setRemindersAction(currentObjColl);
      }

      if (result?.data?.cartCount != undefined &&
        result?.data?.cartCount.length != 0) {
        let iCartCount = 0;

        iCartCount = (parseInt(result?.data?.cartCount?.lumpsumInCart) || 0) +
          (parseInt(result?.data?.cartCount?.sipInCart) || 0) +
          (parseInt(result?.data?.cartCount?.instasipInCart) || 0) +
          (parseInt(result?.data?.cartCount?.systematicCartCount) || 0);

        setCartCount(iCartCount);
      }

    } catch (error: any) {
      console.log(error);
    }
  };
  useEffect(() => {
    if (userData !== null && userData !== undefined) {
      (async () => {
        await fetchNotification();
      })();
    }
    return () => {
    }
  }, [userData, cartCount])

  useEffect(() => {
    setActiveLink(router.pathname);
    if (userInfo === null || userInfo === undefined) {
      setuserData(getUser());
    }
    else {
      setuserData(userInfo);
    }
  }, [userInfo]);

  const handleChange = (id: string | number) => {
    switch (id) {
      case 1:
        adminProfile();
        break;
      case 3:
        logout();
        break;
      case 2:
        ChangePassword();
        break;
      default:
        break;
    }
  };

  const logout = () => {
    localStorage.clear();
    router.push("/Login");
  };

  const adminProfile = () => {
    router.push("/Profile");
  };

  const ChangePassword = () => {
    router.push("/ChangePass");
  };

  return (
    <header
      className={clsx(styles.header, {
        [styles.headerClose]: !uiState.sidebarOpen,
      })}
    >
      <div className={styles.headerContainer}>
        <div
          style={{
            paddingBottom: "0.5rem",
            paddingTop: "0.5rem",
          }}
        >
          <div className="container">
            <div className="row row-cols-2 align-items-center justify-content-between">
              <div className="col-7 d-flex align-items-center">
                <div className={`${styles.logoSection}`}>
                  <Link href={`/`}>
                    <a>
                      <img
                        // width={100}
                        // height={30}
                        src={`${WEB_URL}/logo.png`}
                        alt="logo"
                      />
                    </a>
                  </Link>
                </div>
                <div className="ms-4 d-none d-md-flex">
                  <div className="row align-items-center">
                    <div className="col-6">
                      <GroupBox className="align-items-center">
                        <Box className="badgeBlue mb-0">
                          <Personfill color="primary" />
                        </Box>
                        <Box className="holderName nowrap">
                          {/* @ts-ignore */}
                          <Text size="h5" color="primary">
                            {userData?.name}
                          </Text>
                          {/* @ts-ignore */}
                          <Text color="gray" size="h6">
                            {userData?.userid}
                          </Text>
                        </Box>
                      </GroupBox>
                    </div>
                    {userData?.RmName != "" &&
                      <div className="col-6 border-start headerLine">
                        <div className="row">
                          {userData?.RmName != "" &&
                            <div className="col-12">
                              <div className={`${styles.relationship}`}>
                                Relationship Manager <span className={`${styles.managerName}`}>{userData?.RmName}</span>
                              </div>
                            </div>
                          }
                          {userData?.RmMobile != "" &&
                            <div className="col-12">
                              <div className={`${styles.managerDetails}`}>
                                <PhoneSquare />{userData?.RmMobile}
                              </div>
                            </div>
                          }
                          {userData?.RmEmail != "" &&
                            <div className="col-12">
                              <div className={`${styles.managerDetails}`}>
                                <EmailIcon></EmailIcon><span>{userData?.RmEmail}</span>
                              </div>
                            </div>
                          }
                        </div>
                      </div>
                    }
                  </div>
                </div>
              </div>
              <div className="col-5">
                <div className="d-flex align-items-center justify-content-end me-md-3">
                  <DropdownGroup>
                    <DropdownStyled
                      arr={remindersAction}
                      activeLink={activeLink || ""}
                      trigger={
                        <TriggerButton
                          css={{ position: "relative" }}
                          aria-label="Customise options"
                        >
                          <BellFill color={"gray"} />
                          <Badge
                            css={{
                              right: 5,
                              top: 11,
                              pt: 1,
                              pr: 7,
                              pl: 7,
                              pb: 1,
                            }}
                            position={"topRight"}
                            type={"dot"}
                            color={"blue2"}
                            rounded
                          >
                            {remindersAction.length}
                          </Badge>
                        </TriggerButton>
                      }
                      triggerProps={{ asChild: true }}
                      onSelect={(e: any) =>
                        router.push(e?.url)
                      }
                    />
                    <Button
                      css={{ position: "relative" }}
                      aria-label="Customise options"
                      className={`d-md-flex d-sm-flex ${styles.buttonContainer}`}
                      onClick={() => router.push("/Cart")}
                    >
                      <CartFill color={"gray"} />
                      <Badge
                        css={{
                          right: 5,
                          top: 11,
                          pt: 1,
                          pr: 7,
                          pl: 7,
                          pb: 1,
                        }}
                        position={"topRight"}
                        type={"dot"}
                        color={"bluedark"}
                        rounded
                      >
                        {cartCount}
                      </Badge>
                    </Button>

                    {/* hamburger menu icon only for mobile */}
                    <Button
                      className="d-flex d-sm-none"
                      color={"clear"}
                      onClick={() => uiDispatch({ type: "TOGGLE_SIDEBAR" })}
                    >
                      <ListIcon stroke={"darkblue"} />
                    </Button>
                    {/* settings icon only for desktop */}
                    <DropdownStyled
                      triggerProps={{
                        className: "d-none d-sm-flex",
                        asChild: true,
                      }}
                      onSelect={(e: any) => handleChange(e.id)}
                      arr={ACTIONS}
                      activeLink={activeLink || ""}
                      trigger={
                        <TriggerButton>
                          <Box className="align-self-center">
                            <SettingIcon color="gray" size="10" />
                          </Box>
                        </TriggerButton>
                      }
                    />
                  </DropdownGroup>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className={`d-none d-md-flex justify-content-center ${styles.headerContainerCenter}`}
          style={{ minHeight: "0px", borderTop: "solid 2px #b3daff" }}
        >
          <Box
            className={`d-md-flex d-sm-flex ${styles.boxNavbar}`}
            css={{ p: 0, m: 0, justifyContent: "center" }}
          >
            <Button
              css={{
                "@bp0": { order: -1 },
                "@bp1": { order: 0 },
              }}
              color={"clear"}
              className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
              onClick={() => router.push("/Dashboard")}
            >
              <Box css={{ p: 5 }}>
                <Gauge color={activeLink === "/" ? "#005CB3" : "#339CFF"} />
              </Box>
              <Box
                className="d-none d-md-flex"
                css={{ p: 11, marginTop: "2px" }}
              >
                {/* @ts-ignore */}
                <Text
                  //@ts-ignore
                  color={activeLink === "/" ? "primary" : "mediumBlue"}
                  size="bt"
                  weight="normal"
                >
                  Dashboard
                </Text>
              </Box>
            </Button>
            <DropdownStyled
              triggerProps={{
                className: "d-md-flex",
                asChild: true,
              }}
              onSelect={(e: any) => router.push(e.url)}
              arr={PLAN_ACTIONS}
              activeLink={activeLink || ""}
              trigger={
                <TriggerButton
                  className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
                >
                  <Box css={{ p: 5 }}>
                    <Coins
                      color={
                        (activeLink === "/FinancialPlan" ||
                          activeLink === "/Goals/MyGoals")
                          ? "#005CB3"
                          : "#339CFF"
                      }
                      height={"25px"}
                      width={"25px"}
                    />
                  </Box>
                  <Box className="d-none d-md-flex" css={{ p: 11 }}>
                    <Text
                      //@ts-ignore
                      color={
                        (activeLink === "/FinancialPlan" ||
                          activeLink === "/Goals/MyGoals")
                          ? "primary"
                          : "mediumBlue"
                      }
                      size="bt"
                      weight="normal"
                    >
                      Plan
                    </Text>
                  </Box>
                </TriggerButton>
              }
            />
            <DropdownStyled
              triggerProps={{
                className: "d-md-flex",
                asChild: true,
              }}
              onSelect={(e: any) => router.push(e.url)}
              arr={PRODUCT_ACTIONS}
              activeLink={activeLink || ""}
              trigger={
                <TriggerButton
                  className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
                >
                  <Box css={{ p: 5 }}>
                    <BoxesStacked
                      color={
                        activeLink === "/Products/MutualFund" ||
                          activeLink === "/Products/Liquiloan" ||
                          activeLink === "/Products/Insurance" ||
                          activeLink === "/Products/Protection" ||
                          activeLink === "/Products/EPF" ||
                          activeLink === "/Products/PMS" ||
                          activeLink === "/Products/ULIP"
                          ? "#005CB3"
                          : "#339CFF"
                      }
                      height={"25px"}
                      width={"25px"}
                    />
                  </Box>
                  <Box className="d-none d-md-flex" css={{ p: 11 }}>
                    <Text
                      //@ts-ignore
                      color={
                        activeLink === "/Products/MutualFund" ||
                          activeLink === "/Products/Liquiloan" ||
                          activeLink === "/Products/Insurance" ||
                          activeLink === "/Products/Protection" ||
                          activeLink === "/Products/EPF" ||
                          activeLink === "/Products/PMS" ||
                          activeLink === "/Products/ULIP"
                          ? "primary"
                          : "mediumBlue"
                      }
                      size="bt"
                      weight="normal"
                    >
                      Invest
                    </Text>
                  </Box>
                </TriggerButton>
              }
            />
            {/* <Button
              css={{
                "@bp0": { order: -1 },
                "@bp1": { order: 0 },
                p: 0,
              }}
              color={"clear"}
              className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
              onClick={() => router.push("/Goals/MyGoals")}
            >
              <Box css={{ p: 5 }}>
                <Bullseye
                  color={
                    activeLink === "/Goals/MyGoals" ? "#005CB3" : "#339CFF"
                  }
                />
              </Box>
              <Box
                className="d-none d-md-flex"
                css={{ p: 11, marginTop: "2px" }}
              >
                <Text
                  //@ts-ignore
                  color={
                    activeLink === "/Goals/MyGoals" ? "primary" : "mediumBlue"
                  }
                  size="bt"
                  weight="normal"
                >
                  Goals
                </Text>
              </Box>
            </Button>
            <Button
              css={{
                "@bp0": { order: -1 },
                "@bp1": { order: 0 },
                p: 0,
              }}
              color={"clear"}
              className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
              onClick={() => router.push("/FinancialPlan")}
            >
              <Box css={{ p: 5 }}>
                <Coins
                  color={
                    activeLink === "/FinancialPlan" ? "#005CB3" : "#339CFF"
                  }
                />
              </Box>
              <Box
                className="d-none d-md-flex"
                css={{ p: 11, marginTop: "2px" }}
              >
                <Text
                  //@ts-ignore
                  color={
                    activeLink === "/FinancialPlan" ? "primary" : "mediumBlue"
                  }
                  size="bt"
                  weight="normal"
                >
                  Financial Plan
                </Text>
              </Box>
            </Button> */}
            <DropdownStyled
              triggerProps={{
                className: "d-md-flex",
                asChild: true,
              }}
              onSelect={(e: any) => router.push(e.url)}
              arr={Report_ACTIONS}
              activeLink={activeLink || ""}
              trigger={
                <TriggerButton
                  className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
                >
                  <Box css={{ p: 5, marginTop: "-5px" }}>
                    <BarChart
                      color={(activeLink === "/Report" ||
                        activeLink === "/Passbook") ? "#005CB3" : "#339CFF"}
                      height={"25px"}
                      width={"25px"}
                    />
                  </Box>
                  <Box
                    className="d-none d-md-flex"
                    css={{ p: 11, marginTop: "2px" }}
                  >
                    <Text
                      //@ts-ignore
                      color={
                        (activeLink === "/Report" ||
                          activeLink === "/Passbook") ? "primary" : "mediumBlue"
                      }
                      size="bt"
                      weight="normal"
                    >
                      Reports
                    </Text>
                  </Box>
                </TriggerButton>
              }
            />
            <DropdownStyled
              triggerProps={{
                className: "d-md-flex",
                asChild: true,
              }}
              onSelect={(e: any) => router.push(e.url)}
              arr={Tools_ACTIONS}
              activeLink={activeLink || ""}
              trigger={
                <TriggerButton
                  className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
                >
                  <Box css={{ p: 5 }}>
                    <Tools
                      color={
                        activeLink === "/Tools/QuickSIP" ||
                          activeLink === "/Tools/Calculator" ||
                          activeLink === "/Tools/TaxSaving" ||
                          activeLink === "/Products/UploadCas"
                          ? "#005CB3"
                          : "#339CFF"
                      }
                    />
                  </Box>
                  <Box
                    className="d-none d-md-flex"
                    css={{ p: 11, marginTop: "2px" }}
                  >
                    <Text
                      //@ts-ignore
                      color={
                        activeLink === "/Tools/QuickSIP" ||
                          activeLink === "/Tools/Calculator" ||
                          activeLink === "/Tools/TaxSaving" ||
                          activeLink === "/Products/UploadCas"
                          ? "primary"
                          : "mediumBlue"
                      }
                      size="bt"
                      weight="normal"
                    >
                      Tools
                    </Text>
                  </Box>
                </TriggerButton>
              }
            />
            <DropdownStyled
              triggerProps={{
                className: "d-md-flex",
                asChild: true,
              }}
              onSelect={(e: any) => router.push(e.url)}
              arr={Profile_ACTIONS}
              activeLink={activeLink || ""}
              trigger={
                <TriggerButton
                  className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
                >
                  <Box css={{ p: 5, marginTop: "-5px" }}>
                    <Personfill
                      color={
                        activeLink === "/Profile" ||
                          activeLink === "/Mandate/MandateList" ||
                          activeLink === "/Investment"
                          ? "#005CB3"
                          : "#339CFF"
                      }
                    />
                  </Box>
                  <Box
                    className="d-none d-md-flex"
                    css={{ p: 11, marginTop: "2px" }}
                  >
                    <Text
                      //@ts-ignore
                      color={
                        activeLink === "/Profile" ||
                          activeLink === "/Mandate/MandateList" ||
                          activeLink === "/Investment"
                          ? "primary"
                          : "mediumBlue"
                      }
                      size="bt"
                      weight="normal"
                    >
                      My Profile
                    </Text>
                  </Box>
                </TriggerButton>
              }
            />
            <DropdownStyled
              triggerProps={{
                className: "d-md-flex",
                asChild: true,
              }}
              onSelect={(e: any) => {
                router.push(e.url)
                if (e.name === "Contact Us") {
                  setContactUsVal({
                    SubjectId: "",
                    Description: ""
                  }
                  );
                }
              }}
              arr={Support_ACTIONS}
              activeLink={activeLink || ""}
              trigger={
                <TriggerButton
                  className={`d-md-flex d-sm-flex ${styles.boxContainer}`}
                >
                  <Box css={{ p: 5 }}>
                    <Support
                      color={activeLink === "/Support/FAQ" ||
                        activeLink === "/Support/RaiseTicket" ||
                        activeLink === "/Support/Feedback" ||
                        activeLink === "/Support/ContactUs" ? "#005CB3" : "#339CFF"} />
                  </Box>
                  <Box
                    className="d-none d-md-flex"
                    css={{ p: 11, marginTop: "2px" }}
                  >
                    <Text
                      //@ts-ignore
                      color={
                        activeLink === "/Support/FAQ" ||
                          activeLink === "/Support/RaiseTicket" ||
                          activeLink === "/Support/Feedback" ||
                          activeLink === "/Support/ContactUs" ? "primary" : "mediumBlue"
                      }
                      size="bt"
                      weight="normal"
                    >
                      Help
                    </Text>
                  </Box>
                </TriggerButton>
              }
            />
          </Box>
        </div>
      </div>
    </header >
  );
}

export default Header;
