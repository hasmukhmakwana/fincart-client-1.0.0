import Header from "App/shared/components/Header/Header";
import Sidebar from "App/shared/components/Sidebar/Sidebar";
import React, { FC, useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import SessionTimer from "./SessionTimer";
import { checkSessionTimer } from "App/utils/helpers";
import { useRouter } from "next/router";
import ModalDialog from "@ui/AlertDialog/ModalDialog";
import SessionEnd from "./SessionEnd";
import { refreshToken } from "App/utils/api";
// method for alert();
import NoInternetConnection from './NoInternetConnection'
type MSGTYPE = "success" | "error" | "info" | "warn";
export const toastAlert = (type: MSGTYPE, display: any) => {
  let msg =
    display?.message ||
    display?.msg ||
    display?.[0]?.msg ||
    display?.data ||
    display;

  if (typeof msg === "object") {
    msg = JSON.stringify(msg || "Something went wrong!");
  }

  if (type == "success") return toast.success(msg);
  else if (type == "warn") return toast.warn(msg);
  else if (type == "info") return toast.info(msg);
  else if (type == "error") return toast.error(msg);
  else return toast.info("An Alert Problem");
};


const Layout: FC = ({ children }) => {
  const router = useRouter();
  const [active, setActive] = useState(true)
  const sessionData: any = checkSessionTimer() || {};
  const [show, setShow] = useState(false);
  // const [showIdle, setShowIdle] = useState(false);
  const [idleLogout, setIdleLogout] = useState(false);
  const t: number = Math.round(sessionData?.remainingTime / 1000);
  let time: NodeJS.Timeout;
  const [Timer, setTimer] = useState<number>(0);
  const [extendRun, setExtendRun] = useState<boolean>(false);
  const expireT: number = Math.round(sessionData?.TOKEN?.expires_in)

  useEffect(() => {
    document.addEventListener("visibilitychange", (event) => {
      if (document.visibilityState == "visible") {
        setActive(true);
      } else {
        resetTimer();
        setActive(false);
      }
    });
    window.onload = resetTimer;
    document.onmousemove = resetTimer;
    document.onclick = resetTimer;

    function resetTimer() {
      clearTimeout(time);
      time = setTimeout(() => {
        setShow(true);
      }, (expireT * 1000 - 60000))
    }
    return () => {

      document.removeEventListener("visibilitychange", (event) => {

      });

    }
  }, [])

  useEffect(() => {

    if (t <= 30) {
      extend();
    }
    const interval = setInterval(() => { setTimer((prev) => prev + 1) }, 1000);


    return () => {
      clearInterval(interval);
    }
  }, [Timer]);

  const idlePopUpClose = () => {
    if (idleLogout) {

    }
    setIdleLogout(false);
  }

  const logout = () => {
    setShow(false);
    localStorage.clear();
    router.push("/Login");
  };

  const extend = async () => {
    setExtendRun(true);
    try {
      await refreshToken();

    } catch (error) {
      console.log(error);
      logout();
    }
  };

  return (
    <>

      <Header />
      <NoInternetConnection>
        <div className="wrapper">
          <div id="content" className="px-md-4">
            <div className="container">{children}</div>
          </div>
          <div className="d-flex d-sm-none">
            <Sidebar />
          </div>

          {/* use for alert */}
          <ToastContainer
            position="top-center"
            autoClose={3000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
          <SessionTimer active={active} show={show} setShow={setShow} setIdleLogout={setIdleLogout} />
          <ModalDialog
            open={idleLogout}
            setOpen={idlePopUpClose}
            hideCloseBtn={true}
            css={{
              "@bp0": { width: "50%" },
              "@bp1": { width: "20%" },
            }}
          >
            <SessionEnd setOpen={idlePopUpClose} />
          </ModalDialog>
        </div>
      </NoInternetConnection>
    </>
  );
};

export default Layout;
