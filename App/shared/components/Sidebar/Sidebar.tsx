import React, { SyntheticEvent, useContext, useEffect, useState } from "react";
import UiContext from "App/contexts/ui/ui.context";
import { useRouter, NextRouter } from "next/router";
import RightArrow from "App/icons/RightArrow";
import {
  ChildLi,
  MainSidebar,
  Parent,
  ParentContainer,
  ParentElement,
  OnlyParent,
} from "./Sidebar.styles";
import UserIcon from "App/icons/UserIcon";
import Text from "App/ui/Text/Text";
import DownArrow from "App/icons/DownArrow";
import Box from "App/ui/Box/Box";
import Link from "next/link";
import { getLS } from "@ui/DataGrid/utils";
import { MENU_PREFIX, WEB_URL } from "App/utils/constants";
import BoxIcon from "App/icons/Box";
import { MENU } from "./metaData";
import { GroupBox } from "@ui/Group/Group.styles";
import styles from "./Sidebar.module.scss";
import Holder from "../Holder/Holder";
import Phone from "App/icons/Phone";
import { getUser } from "App/utils/helpers";
import useHeaderStore from "../Header/store";

export type SidebarProps = {
  user: any;
  arr: any[];
};
export type MenuObjProps = {
  parentId: number;
  childId: number;
};

function Sidebar() {
  const router: NextRouter = useRouter();
  const { uiState, uiDispatch } = useContext(UiContext);

  const [menuData, setmenuData] = useState<any[]>([]);

  const [activeLink, setActiveLink] = useState<string | null>(null);
  const { userInfo } = useHeaderStore();
  const [userData, setuserData] = useState<any>(userInfo || null);

  const [menuObj, setmenuObj] = useState<MenuObjProps>({
    parentId: 0,
    childId: 0,
  });
  const logout = () => {
    localStorage.clear();
    router.push("/Login");
  };

  useEffect(() => {
    //get menu frm localstorage
    const menu = getLS(MENU_PREFIX);
    setmenuData(menu || []);
    setActiveLink(router.pathname);
    if (!userData) setuserData(getUser());
  }, []);

  useEffect(() => {
    setmenuObj({ parentId: uiState.parentId, childId: uiState.childId });
  }, [uiState]);

  const handleMenu = (parentId: number | null, childId: number | null) => {
    uiDispatch({
      type: "MENU_SIDEBAR",
      parentId,
      childId,
    });
  };

  return (
    <MainSidebar open={uiState?.sidebarOpen}>
      <Box className="d-flex d-md-none">
        <Box className={styles.userPanel}>
          <div className={`${styles.relationshipRow}`}>
            <div className="d-flex d-md-none">
              <div className={`${styles.relationship}`}>
                Relationship Manager
              </div>
              <div className={`${styles.managerName}`}>
              {userData?.RmName} <Phone className="phone" />
              </div>
            </div>
          </div>
          <GroupBox className="d-flex d-md-none">
            <Box className={styles.userImg}>
              <img
                width={30}
                height={30}
                src={`${WEB_URL}/imagesavatar.png`}
                alt="logo"
              />
            </Box>
            <Box>
              {/* @ts-ignore */}
              <Box>
                {/* @ts-ignore */}
                <Text weight="extrabold" color="gray10">
                {userData?.name}
                </Text>
                {/* @ts-ignore */}
                <Text color="gray" size="h6">
                {userData?.userid}
                </Text>
              </Box>
            </Box>
          </GroupBox>
          <Box className={`${styles.holder}`}>
            <Holder />
          </Box>
        </Box>
      </Box>
      <ParentContainer>
        {/* {menuData.map((item: any, itemIndex: number) => { */}
        {MENU.map((item: any, itemIndex: number) => {
          return (
            <Parent key={itemIndex} onClick={() => handleMenu(item.id, null)}>
              {item?.link ? (
                item?.link==="/Logout"?
                // <Link href={item?.link}>
                //   <a>
                    <OnlyParent active={item?.link === activeLink} >
                      <Box className="d-flex" onClick={logout} >
                        {item.icon}
                        {/* <BoxIcon size={20} className="mt-0 me-1" /> */}
                        <Text weight="normal" size="h5" css={{ pl: "$4" }}>
                          {item.name}
                        </Text>
                      </Box>
                    </OnlyParent>
                //   </a>
                // </Link>
                :
                <Link href={item?.link}>
                  <a>
                    <OnlyParent active={item?.link === activeLink}>
                      <Box className="d-flex">
                        {item.icon}
                        {/* <BoxIcon size={20} className="mt-0 me-1" /> */}
                        <Text weight="normal" size="h5" css={{ pl: "$4" }}>
                          {item.name}
                        </Text>
                      </Box>
                    </OnlyParent>
                  </a>
                </Link>
              ) : (
                <ParentElement>
                  <Box className="d-flex">
                    {item.icon}
                    <Text weight="normal" size="h5" css={{ pl: "$4" }}>
                      {item.name}
                    </Text>
                  </Box>
                  {item.children?.length ? (
                    item.id === menuObj.parentId ? (
                      <DownArrow size={14} className="mt-1 mr-1" />
                    ) : (
                      <RightArrow size={14} className="mt-1 mr-1" />
                    )
                  ) : null}
                </ParentElement>
              )}
              {item.id === menuObj.parentId &&
                item.children &&
                item.children.map((subItem: any, subItemIdex: number) => {
                  return (
                    <div onClick={() => handleMenu(item.id, subItem.id)}>
                      <Link href={subItem?.link || "/"}>
                        <a>
                          <ChildLi
                            key={subItemIdex}
                            active={subItem.link === activeLink}
                          >
                            {subItem.name}
                          </ChildLi>
                        </a>
                      </Link>
                    </div>
                  );
                })}
            </Parent>
          );
        })}
      </ParentContainer>
    </MainSidebar>
  );
}

export default React.memo(Sidebar);
