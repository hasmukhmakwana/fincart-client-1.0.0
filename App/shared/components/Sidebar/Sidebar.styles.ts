import RightArrow from "App/icons/RightArrow";
import { styled } from "App/theme/stitches.config";

export const MainSidebar = styled("div", {
  background: "$bg1",
  transition: "$default",
  height: "",
  overflowY: "auto",
  overflowX: "hidden",
  whiteSpace: "nowrap",
  left: 0,
  position: "sticky",
  top: 0,
  '@bp0': {
    left: 0,
    position: "fixed",
    top: 64,
    zIndex: 9999,
    height: 'calc(100vh - 64px)',
    // height: '100vh',
  },
  '@bp1': {
    left: 0,
    position: "sticky",
    top: 0,
  },
  // my: "$10",
  variants: {
    open: {
      true: {
        width: "300px",
        boxShadow: '200px 0px 0px rgba(0,0,0,0.8)',
      },
      false: {
        width: "0px",
      },
    },
  },
  defaultVariants: {
    open: true,
  },
});

export const ParentElement = styled("div", {
  display: "flex",
  justifyContent: "space-between",
  py: "$5",
  // mb: "$5",
  color:"#339CFF",
  'svg': {
    width: 20,
    height: 20,
    fill: '#339CFF !important',
  }
});

export const IconContainer = styled("div", {
  marginRight: 2,
});

export const ParentContainer = styled("ul", {
  listStyle: "none",
  margin: 0,
  padding: 0,
});

export const Parent = styled("li", {
  listStyle: "none",
  px: "$9",
  py: "$3",
  // borderBottomWidth: 1,
  // borderBottomStyle: "solid",
  // borderBottomColor: "$gray5",
  cursor: "pointer",
  "&:hover": {
    bg: "$bgHover",
  },
});

export const ChildLi = styled("a", {
  borderRight: 0,
  borderTop: 0,
  borderBottom: 0,
  outline: 0,
  display: "block",
  textDecoration: "none",
  paddingLeft: "$10",
  marginLeft: "$4",
  // height: "34px",
  lineHeight: "34px",
  borderTopRightRadius: "$sm",
  borderBottomRightRadius: "$sm",
  userSelect: "none",
  fontWeight: "$thin",
  position: "relative",
  "&:before": {
    content: "",
    width: 7,
    height: 7,
    display: "block",
    position: "absolute",
    left: -4,
    top: 15,
    // borderRadius: "$pill",
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "$borderLight",
    transform: "rotate(45deg)",
    backgroundColor: "White",
  },
  variants: {
    active: {
      true: {
        borderLeft: "1.5px",
        borderStyle: "solid",
        borderColor: "$bgSolid1",
        backgroundColor: "$bgActive",
        color: "$bgSolid2",
        "&:before": {
          backgroundColor: "$bgSolid2",
          borderColor: "$bgSolid2",
        },
      },
      false: {
        borderLeft: "1.5px",
        borderStyle: "solid",
        borderColor: "$borderLight",
        color: "$gray10",
      },
    },
  },
});

export const OnlyParent = styled("a", {
  display: "flex",
  justifyContent: "space-between",
  py: "$5",
  color: '#A5A5A5',
  variants: {
    active: {
      true: {
        // backgroundColor: "$borderLight",
        color: '#005CB3'
      },
      false: {
        // backgroundColor: "$borderLight",
        color: '#339CFF'
      },
    },
  },
  'svg': {
    width: 20,
    height: 20,
    fill: '#339CFF !important',
  }
});
