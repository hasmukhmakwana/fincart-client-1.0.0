import UserIcon from "App/icons/UserIcon";
import PartyIcon from "App/icons/PartyIcon";
import PermissionIcon from "App/icons/PermissionIcon";
import Box from "App/icons/Box";
import Dash from "App/icons/Dash";
import AddressBook from "App/icons/AddressBook";
import HomeIcon from "App/icons/HomeIcon";
import Dashboard from "App/icons/Dashboard";
import Personfill from "App/icons/Personfill";
import LogoutIcon from "App/icons/LogoutIcon";
import Lock from "App/icons/Lock";
import Goal from "App/icons/Goal";
import Portfolio from "App/icons/Portfolio";
import Reports from "App/icons/Reports";
import Coins from "App/icons/Coins";
import Gauge from "App/icons/Gauge";
import BoxesStacked from "App/icons/BoxesStacked";
import Bullseye from "App/icons/Bullseye";
import BarChart from "App/icons/BarChart";
import Tools from "App/icons/Tools";
import Support from "App/icons/Support";


export type SINGLE_MENU = {
  id: number;
  name: string;
  icon: any;
  link: string | null;
  children?: any[];
  permission?: any;
};

export const permission = {
  view: false,
  add: false,
  edit: false,
  delete: false,
};

export const permissionAll = {
  viewAll: false,
  addAll: false,
  editAll: false,
  deleteAll: false,
};

export const MENU: SINGLE_MENU[] = [
  {
    id: 1,
    name: "Dashboard",
    icon: <Gauge/>,
    link: "/",
  },
  {
    id: 2,
    name: "Products",
    icon: <BoxesStacked/>,
    link: null,
    children: [
      {
        id: 201,
        name: "Mutual Fund",
        icon: <BoxesStacked />,
        link: "/Products/MutualFund",
      },
      {
        id: 202,
        name: "ULIP",
        icon: <BoxesStacked />,
        link: "/Products/ULIP",
      },
      {
        id: 203,
        name: "PMS",
        icon: <BoxesStacked />,
        link: "/Products/PMS",
      },
      {
        id: 204,
        name: "Liquiloans",
        icon: <BoxesStacked />,
        link: "/Products/Liquiloan",
      },
      {
        id: 205,
        name: "Insurance",
        icon: <BoxesStacked />,
        link: "/Products/Insurance",
      },
      {
        id: 206,
        name: "NPS",
        icon: <BoxesStacked />,
        link: "",
      },
      {
        id: 207,
        name: "EPF",
        icon: <BoxesStacked />,
        link: "/Products/EPF",
      },
      {
        id: 208,
        name: "Digital Gold",
        icon: <BoxesStacked />,
        link: "",
      },
    ],
  },
  {
    id: 3,
    name: "Goals",
    icon: <Bullseye/>,
    link: "/Goals/MyGoals",
  },
  {
    id: 4,
    name: "Financial Plan",
    icon: <Coins/>,
    link: "/FinancialPlan",
  },
  {
    id: 5,
    name: "Profile",
    icon: <Personfill/>,
    link: null,
    children: [
      {
        id: 501,
        name: "View",
        icon: <Personfill />,
        link: "/Profile",
      },
      {
        id: 502,
        name: "Investment Account",
        icon: <Personfill/>,
        link: "/Investment",
      },
      {
        id: 503,
        name: "Mandates",
        icon: <Personfill/>,
        link: "/Mandate/MandateList",
      },
      // {
      //   id: 504,
      //   name: "Register",
      //   icon: <Personfill/>,
      //   link: "/Registration",
      // },
    ],
  },
  {
    id: 6,
    name: "Reports",
    icon: <BarChart/>,
    link: null,
    children: [
      {
        id: 601,
        name: "Overall",
        icon: <BarChart />,
        link: "",
      },
      {
        id: 602,
        name: "Capital Gains",
        icon: <BarChart/>,
        link: "",
      },
      {
        id: 603,
        name: "Product Wise",
        icon: <BarChart/>,
        link: "",
      },
    ],
  },

  {
    id: 7,
    name: "Tools",
    icon: <Tools/>,
    link: null,
    children: [
      {
        id: 701,
        name: "Quick SIP",
        icon: <Tools />,
        link: "/Tools/QuickSIP",
      },
      {
        id: 702,
        name: "Calculator",
        icon: <Tools />,
        link: "/Tools/Calculator",
      },

      {
        id: 703,
        name: "Short Finacial Plan",
        icon: <Tools />,
        link: "",
      },
      {
        id: 704,
        name: "Tax Saving",
        icon: <Tools />,
        link: "/Tools/TaxSaving",
      },
     
    ],
  },

  {
    id: 8,
    name: "Support",
    icon: <Support/>,
    link: null,
    children: [
      {
        id: 801,
        name: "FAQ",
        icon: <Support />,
        link: "",
      },
      {
        id: 802,
        name: "Raise a Ticket",
        icon: <Support />,
        link: "",
      },
      {
        id: 803,
        name: "Feedback",
        icon: <Support/>,
        link: "",
      },
      {
        id: 804,
        name: "Contact Us",
        icon: <Support />,
        link: "",
      },

    ],
  },
  {
    id: 9,
    name: "Change Password",
    icon: <Lock/>,
    link: "/ChangePass",
  },
  {
    id: 10,
    name: "Logout",
    icon: <LogoutIcon/>,
    link: "/Logout",
  },
];




// // http://localhost:3000/
// // http://localhost:3000/Landing
// // http://localhost:3000/Login
// // http://localhost:3000/Home
// // http://localhost:3000/Member
// // http://localhost:3000/GoalPlanning
// // http://localhost:3000/GoalPlanningInitiateGoal
// // http://localhost:3000/GoalPlanningSteps
// // http://localhost:3000/Invest
// // http://localhost:3000/Portfolio
// // http://localhost:3000/SchemeList
// // http://localhost:3000/SubscriptionRenewal
// // http://localhost:3000/Checkout
// // http://localhost:3000/Redemption
// // http://localhost:3000/SwitchTransaction
// // http://localhost:3000/SystematicTransferPlan
// // http://localhost:3000/RegisterMandate
// // http://localhost:3000/Registration
// // http://localhost:3000/RegistrationNonKyc

