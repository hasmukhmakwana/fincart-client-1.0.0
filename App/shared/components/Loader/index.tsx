import Box from "@ui/Box/Box";
import styles from "./Loader.module.scss";

export default () => {
  return (
    <Box className={styles.loader_main_div}>
      <button className={`btn, ${styles.loaderStyle}`} disabled>
        <span
          className="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
        ></span>
        {/* &nbsp; Loading... */}
      </button>
    </Box>
  );
};
