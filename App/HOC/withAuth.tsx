import { checkSessionTimer } from "App/utils/helpers";
import { useRouter } from "next/router";
import { useEffect } from "react";

function WithAuth(Comp: any) {
  return function WrappedWithToast(props: any) {
    const router = useRouter();
    const sessionData: any = checkSessionTimer() || {};
    // console.log(sessionData);

    useEffect(() => {
      if (!sessionData?.status) {
        localStorage.clear();
        router.push("/Login");
      }
    }, [sessionData]);

    return <Comp {...props} />;
  };
}

export default WithAuth;
