const mode = {
    development: "development",
    staging: "staging",
    production: "production",
    live: "live"
}
export default mode.production