import { cssFocusVisible, styled } from "../../theme/stitches.config";
export const StyledDivider = styled(
  "hr",
  {
    border: 0,
    height: "2px",
    background: "var(--colors-yellow)",
    variants: {
      display: {
        block: {
          display: "block",
        },
        flex: {
          display: "flex",
          flexWrap: "wrap",
          width: "100%",
          "&>*": {
            flex: "1 0 0%",
            width: "100%",
            maxWidth: "100%",
          },
        },
        table: {
          display: "table",
        },
        inlineBlock: {
          display: "inline-block",
        },
      },
      align: {
        right: {
          justifyContent: "right",
        },
        center: {
          justifyContent: "center",
        },
        left: {
          justifyContent: "flex-start",
        },
      },
      alignSelt: {
        middle: {
          alignSelf: "center",
        },
        bottom: {
          alignSelf: "end",
        },
      },
      position: {
        middle: {
          alignItems: "center",
        },
        bottom: {
          alignItems: "end",
        },
      },
      height: {
        full: {
          height: "100vh",
        },
      },
      background: {
        default: {
          backgroundColor: "$blue",
          color: "$whiteA12",
        },
        gray: {
          backgroundColor: "$gray2",
        },
        orangeLight: {
          backgroundColor: "$orangeLight",
        },
      },
      img: {
        //@ts-ignore
        width: "200px",
      },
    },
    defaultVariants: {},
  },
  cssFocusVisible
);
