import React from "react";
import { StyledDivider } from "./Divider.styles";

const Divider = (props: any) => {
  return <StyledDivider {...props}>{props.children}</StyledDivider>;
};

export default Divider;
