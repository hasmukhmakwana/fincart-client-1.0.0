import Box from '@ui/Box/Box';
import { useId } from '@ui/utils/hooks/useId';
import Label from 'App/ui/Label/Label';
import React from 'react'
import { SmallText, StyledInput } from './Input.styles'
import { StyledInputWrapper } from './InputWrapper.styles';

function Input(props: any) {
  const {label, children,error, note, parentCSS, ...rest} = props;
  const inputID = useId(rest.id)
  return (
    <StyledInputWrapper css={{...parentCSS}}>
    {label ?  <Label css={{mb: 5}} htmlFor={inputID} label={label} required={props.required} /> : null }
    <StyledInput error={!!error} {...rest} id={inputID} />
    <Box>
    {note && !error ? <SmallText type="note">{note}</SmallText> : null } 
    {error ? <SmallText type="error">{error}</SmallText> : null}
    </Box>
    </StyledInputWrapper>
  )
}

export default Input