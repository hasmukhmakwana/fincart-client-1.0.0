import { cssFocusVisible, styled } from "../../theme/stitches.config";

export const StyledInputWrapper = styled(
    'div', {
    textAlign: 'left',
    marginBottom: 15,
},
    cssFocusVisible
);
