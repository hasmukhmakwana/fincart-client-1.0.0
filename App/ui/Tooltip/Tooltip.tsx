import React from "react";
import {
  StyledArrow,
  TooltipContent,
  TooltipTrigger,
  TooltipWrapper,
} from "./Tooltip.styles";

export function Tooltip({
  children,
  content,
  open,
  defaultOpen,
  onOpenChange,
  side = "top",
  ...props
}: any) {
  return (
    <TooltipWrapper delayDuration={100}>
      <TooltipTrigger asChild>{children}</TooltipTrigger>
      <TooltipContent side={side} align="center" {...props}>
        {content}
        <StyledArrow />
      </TooltipContent>
    </TooltipWrapper>
    );
}
