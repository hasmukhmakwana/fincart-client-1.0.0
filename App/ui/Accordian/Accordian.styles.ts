//@ts-nocheck
import { keyframes, styled } from "App/theme/stitches.config";
import * as AccordionPrimitive from '@radix-ui/react-accordion';

export const slideDown = keyframes({
    from: { height: 0 },
    to: { height: 'var(--radix-accordion-content-height)' },
  });

  export const slideUp = keyframes({
    from: { height: 'var(--radix-accordion-content-height)' },
    to: { height: 0 },
  });

  export const StyledAccordion = styled(AccordionPrimitive.Root, {
    borderRadius: 6,
    // width: 300,
    width: '100%',
    backgroundColor: mauve.mauve6,
    // boxShadow: `0 2px 10px ${blackA.blackA4}`,
  });

  export const StyledItem = styled(AccordionPrimitive.Item, {
    overflow: 'hidden',
    marginTop: 1,

    '&:first-child': {
      marginTop: 0,
      borderTopLeftRadius: 4,
      borderTopRightRadius: 4,
    },

    '&:last-child': {
      borderBottomLeftRadius: 4,
      borderBottomRightRadius: 4,
    },

    '&:focus-within': {
      position: 'relative',
      zIndex: 1,
      // boxShadow: `0 0 0 2px ${mauve.mauve12}`,
    },
  });

  export const StyledHeader = styled(AccordionPrimitive.Header, {
    all: 'unset',
    display: 'flex',
  });

  export const StyledTrigger = styled(AccordionPrimitive.Trigger, {
    all: 'unset',
    fontFamily: 'inherit',
    backgroundColor: 'transparent',
    padding: '0 20px',
    // height: 45,
    minHeight: 45,
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontSize: 15,
    lineHeight: 1,
    color: violet.violet11,
    // boxShadow: `0 1px 0 ${mauve.mauve6}`,
    '&[data-state="closed"]': { backgroundColor: 'white' },
    '&[data-state="open"]': { backgroundColor: 'white' },
    '&:hover': { backgroundColor: mauve.mauve2 },
  });

  export const StyledContent = styled(AccordionPrimitive.Content, {
    overflow: 'hidden',
    fontSize: 15,
    color: mauve.mauve11,
    backgroundColor: mauve.mauve2,

    '&[data-state="open"]': {
      animation: `${slideDown} 300ms cubic-bezier(0.87, 0, 0.13, 1)`,
    },
    '&[data-state="closed"]': {
      animation: `${slideUp} 300ms cubic-bezier(0.87, 0, 0.13, 1)`,
    },
  });

  export const StyledContentText = styled('div', {
    padding: '15px 20px',
  });

  export const StyledChevron = styled(ChevronDownIcon, {
    color: violet.violet10,
    transition: 'transform 300ms cubic-bezier(0.87, 0, 0.13, 1)',
    '[data-state=open] &': { transform: 'rotate(180deg)' },
  });
