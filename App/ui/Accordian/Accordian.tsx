//@ts-nocheck
import React from 'react';
import { violet, blackA, mauve } from '@radix-ui/colors';
import * as AccordionPrimitive from '@radix-ui/react-accordion';
import { keyframes, styled } from 'App/theme/stitches.config';

const slideDown = keyframes({
  from: { height: 0 },
  to: { height: 'var(--radix-accordion-content-height)' },
});

const slideUp = keyframes({
  from: { height: 'var(--radix-accordion-content-height)' },
  to: { height: 0 },
});

const arrowDown = keyframes({
  from: { transform: "rotate(-180deg)" },
  to: { transform: "rotate(0deg)" },
});

const arrowUp = keyframes({
  from: { transform: "rotate(0deg)" },
  to: { transform: "rotate(-180deg)" },
});

const StyledAccordion = styled(AccordionPrimitive.Root, {
  borderRadius: 6,
//   width: 300,
  width: '100%',
//   backgroundColor: mauve.mauve6,
//   boxShadow: `0 2px 10px ${blackA.blackA4}`,
});

const StyledItem = styled(AccordionPrimitive.Item, {
  overflow: 'hidden',
  // marginBottom: 10,

//   '&:first-child': {
//     marginTop: 0,
//     borderTopLeftRadius: 4,
//     borderTopRightRadius: 4,
//   },

//   '&:last-child': {
//     borderBottomLeftRadius: 4,
//     borderBottomRightRadius: 4,
//   },

  '&:focus-within': {
    position: 'relative',
    // zIndex: 1,
    // boxShadow: `0 0 0 2px ${mauve.mauve12}`,
  },
});

const StyledHeader = styled(AccordionPrimitive.Header, {
  all: 'unset',
  display: 'flex',
  backgroundColor: mauve.mauve6,
//   boxShadow: `0 2px 10px ${blackA.blackA4}`,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: `${mauve.mauve5}`,
    '& p' : {
      margin: 0,
    },
});

const StyledTrigger = styled(AccordionPrimitive.Trigger, {
  all: 'unset',
  fontFamily: 'inherit',
  backgroundColor: 'transparent',
  padding: '0 20px',
  // height: 45,
  minHeight: 35,
  // width: '90%',
   flex: 1,
   display: 'flex',
  alignItems: 'center',
  // alignContent:'stretch',
 justifyContent: 'space-between',

  fontSize: 15,
  lineHeight: 1,
  color: "$darkblue",
//   boxShadow: `0 1px 0 ${mauve.mauve6}`,
  '&[data-state="closed"]': { backgroundColor:  "$lightBlue",
    '.action svg': {
      // transform: "rotate(0deg)",
      animation: `${arrowDown} 150ms ease`,
    }
  },
  '&[data-state="open"]': { backgroundColor:  "$goalBlue",
  '.action svg': {
    transform: "rotate(-180deg)",
    animation: `${arrowUp} 150ms ease`,
    fill: "white !important",

  },
  'svg': {
    fill: "white !important",
  },
    '& p': {
      color: "white"
    }
  },
  // '&:hover': { backgroundColor: mauve.mauve2 },
  '& p' : {
    margin: 0,
  },
});

const StyledContent = styled(AccordionPrimitive.Content, {
  overflow: 'hidden',
  fontSize: 15,
  color: mauve.mauve11,
//   backgroundColor: mauve.mauve2,

  '&[data-state="open"]': {
    animation: `${slideDown} 300ms cubic-bezier(0.87, 0, 0.13, 1)`,
  },
  '&[data-state="closed"]': {
    animation: `${slideUp} 300ms cubic-bezier(0.87, 0, 0.13, 1)`,
  },
});

const StyledContentText = styled('div', {
  //padding: '15px 20px'
  padding:'0px 1px'
});

// const StyledChevron = styled(ChevronDownIcon, {
//   color: violet.violet10,
//   transition: 'transform 300ms cubic-bezier(0.87, 0, 0.13, 1)',
//   '[data-state=open] &': { transform: 'rotate(180deg)' },
// });

// Exports
export const Accordion = StyledAccordion;
export const AccordionItem = StyledItem;
export const AccordionTrigger = React.forwardRef(({ children, ...props }:any, forwardedRef) => (
  <StyledHeader>
    <StyledTrigger {...props} ref={forwardedRef}>
      {children}
      {/* <StyledChevron aria-hidden /> */}
    </StyledTrigger>
  </StyledHeader>
));
export const AccordionContent = React.forwardRef(({ children, ...props }:any, forwardedRef) => (
  <StyledContent {...props} ref={forwardedRef}>
    <StyledContentText>{children}</StyledContentText>
  </StyledContent>
));
