import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Group from "@ui/Group/Group";
import Spacer from "@ui/Spacer/Spacer";
import Text from "@ui/Text/Text";
import React, { useEffect } from "react";
import ColumnOptions from "./components/ColumnOptions";
import Settings from "./components/DensityOption";
import Pagination from "./components/Pagination";
import Data from "./Data";
import {
  GridContainer,
  LimitWrapper,
  StyledLimit,
  Table,
} from "./DataGrid.styles";
import DataHeader from "./DataHeader";
import Filters from "./Filters";
import { gridReducer, initialState } from "./reducer";
import { itemsPerPage } from "./staticData";
import { ACTION_TYPE, DENSITY_TYPE, gridProps } from "./type";
import { getValidColumns } from "./utils";

function Datagrid({
  header,
  data,
  actionButtons,
  bordered,
  clickOnAction,
  emitChange,
  pageCount,
  loading,
}: gridProps) {
  const [state, dispatch] = React.useReducer(gridReducer, initialState);


  useEffect(() => {
    
  dispatch({type: ACTION_TYPE.HYDRATE_STATE, payload: null})
    return () => {
      
    }
  }, [])
  

  useEffect(() => {
    
    if (state.emitFilterValues && emitChange) {
      emitChange(state.gridOutsideConfig);
    }
  }, [state.gridOutsideConfig]);

  useEffect(() => {
    dispatch({ type: ACTION_TYPE.SET_INITIAL_COLUMN, payload: header });
    return () => {};
  }, [header]);

  //handle column change
  const handleColumnChange = (visibility: boolean, index: number) => {
    let arr: any = state?.columns;
    arr[index].visible = visibility;
    dispatch({ type: ACTION_TYPE.COLUMN_CHANGE, payload: arr });
  };

  const handleDensityChange = (density: DENSITY_TYPE) => {
    dispatch({ type: ACTION_TYPE.DENSITY_CHANGE, payload: density });
  };

  const handlePageChange = ({ selected }: { selected: number }) => {
    dispatch({ type: ACTION_TYPE.PAGE_CHANGE, payload: selected + 1 });
  };

  //handle item per page change
  const handleLimitChange = (itemLimit: number) => {
    dispatch({ type: ACTION_TYPE.SET_ITEM_PER_PAGE, payload: itemLimit });
  };

  const handleFilter = (value: any) => {
    dispatch({ type: ACTION_TYPE.FILTER_CHANGE, payload: value });
  };

  const hideFilterChange = () => {
    dispatch({ type: ACTION_TYPE.HIDE_FILTER_CHANGE, payload: null });
  };

  const handleStyleChange = (payload:any) => {
    dispatch({ type: ACTION_TYPE.STYLE_CHANGE, payload });
  };



  return (
    <Box css={{ width: "100%" }}>
      <Group position="right" css={{ mb: "$5", position: "absolute", right: "0", top: "0", width: "30px", height: "100%", display: "block", backgroundColor: "#fff", borderWidth: 1, borderColor: "#ddd", borderStyle: "solid", borderRadius: "0px 7px 7px 0px" }}>
        <Button size="md" disabled={loading} css={{ width: "100%", margin: "15px 0px", backgroundColor: "transparent"}}>
          <Text css={{ writingMode: "vertical-lr", lineHeight: '0', margin: '10px 5px', color: "#000"}}>Export</Text>
        </Button>
        <ColumnOptions
          columns={state?.columns || []}
          handleColumnChange={handleColumnChange}
        />
        <Settings
        currentStyle={state.currentStyle}
        handleStyleChange={handleStyleChange}
          hideFilter={state.hideFilter}
          hideFilterChange={hideFilterChange}
          densitySelected={state.densitySelected}
          setdensitySelected={handleDensityChange}
        />
      </Group>
      <GridContainer css={{marginRight: "30px", minHeight: "280px"}}>
        <Table bordered={bordered}>
          <DataHeader header={getValidColumns(state?.columns)} />
          {!state.hideFilter ? (
            <Filters
              headers={getValidColumns(state?.columns)}
              handleFilter={handleFilter}
            />
          ) : null}

          <Data
            currentStyle={state.currentStyle}
            density={state.densitySelected}
            headers={getValidColumns(state?.columns)}
            data={data}
            actionButtons={actionButtons}
            onActionClick={clickOnAction}
            loading={loading}
          />
        </Table>
      </GridContainer>
      <Spacer y={10} />
      <Group css={{alignItems: "center", marginRight: "30px", p:"$0 $10 $0 $10"}} position="apart">
        <LimitWrapper>
          {itemsPerPage.map((item) => {
            return (
              <StyledLimit
                key={item.id}
                active={state.gridOutsideConfig.limit == item.name}
                onClick={() => handleLimitChange(item.name)}
              >
                {item.name}
              </StyledLimit>
            );
          })}
        </LimitWrapper>
      <Group position="right">

        <div>Total Record: {pageCount}</div>
        <Pagination
          totalCount={pageCount}
          limit={state.gridOutsideConfig.limit}
          onPageChange={handlePageChange}
        />
        </Group>
      </Group>
    </Box>
  );
}

export default Datagrid;
