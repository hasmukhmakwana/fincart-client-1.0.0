import Group from "@ui/Group/Group";
import { Tooltip } from "@ui/Tooltip/Tooltip";
import React from "react";
import { ActionButtonWrapper, SingleAction } from "../DataGrid.styles";
import { actionButtonType, actionTitleTypes, POSITION_TYPE } from "../type";
import ConfirmPopup from "./ConfirmPopup";
type propTypes = {
  actionButtons: actionButtonType[];
  onActionClick: (title: actionTitleTypes, data_index: number) => void;
  position?: POSITION_TYPE;
  data_index: number;
  data:any
};
function ActionButtons({
  actionButtons,
  onActionClick,
  position = "center",
  data_index,
  data
}: propTypes) {

  return (
    <ActionButtonWrapper>
      <Group position={position}>
        {actionButtons.map((item) => {
          if(item.visible != undefined) {
            if (typeof item.visible == "boolean" && !item.visible) return null;
            if(typeof item.visible == 'function' && !item.visible(data)) return null
          }          
          
          if(item.confirmBox) {
            return <ConfirmPopup
            item={item}
            onConfirm={() => onActionClick(item.title, data_index)}
             />
          }
          return (
            <SingleAction
              key={item.title}
              onClick={() => onActionClick(item.title, data_index)}
            >
              {item.icon}
            </SingleAction>
          );
        })}
      </Group>
    </ActionButtonWrapper>
  );
}

export default React.memo(ActionButtons);
