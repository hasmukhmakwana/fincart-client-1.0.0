import { Button } from '@ui/Button/Button'
import Group from '@ui/Group/Group'
import Spacer from '@ui/Spacer/Spacer'
import Text from '@ui/Text/Text'
import React, { useState } from 'react'
import Popover from '../../Popover'
import { actionButtonType } from '../type'

function ConfirmPopup({item, onConfirm}:{item: actionButtonType, onConfirm: () => void}) {
  return (
    <Popover defaultOpen={false}>
      {/* @ts-ignore */}
    <Popover.Trigger asChild>
      <span>{item.icon}</span>
      {/* @ts-ignore */}
    </Popover.Trigger>
      {/* @ts-ignore */}
    <Popover.Content side="top">
     <Text>{item.confirmBox?.title}</Text>
     <Spacer y={5} />
      <Group>
      {/* @ts-ignore */}
      <Popover.Close asChild>
        <Button bordered size="xs" onClick={onConfirm}><Text>{item.confirmBox?.confirmText}</Text></Button>
      {/* @ts-ignore */}
        </Popover.Close>
      {/* @ts-ignore */}
        <Popover.Close asChild>
      {/* @ts-ignore */}
        <Button bordered size="xs" color="error"><Text>{item.confirmBox?.cancelText}</Text></Button>
      {/* @ts-ignore */}
        </Popover.Close>
      </Group>
      {/* @ts-ignore */}
    </Popover.Content>
  </Popover>
  )
}

export default ConfirmPopup