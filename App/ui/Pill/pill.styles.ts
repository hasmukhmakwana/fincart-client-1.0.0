import { styled } from "App/theme/stitches.config";


export const StyledPill = styled('span', {
    variants: {
        color: {
            primary: {
                color: "$bgSolid1"
            }
        }
    },
    defaultVariants: {
        color: "primary"
    }
})