import React from 'react'
import { StyledPill } from './pill.styles'

function Pill({children, ...rest}:any) {
    return (
    <StyledPill>{children}</StyledPill>
    )
}

export default Pill
