import { cssFocusVisible, styled } from "../../theme/stitches.config";
export const StyledBox = styled(
  "div",
  {
    "&.badgeSelect": {
      backgroundColor: "$blue1",
      boxShadow: "0px 20px 20px -11px rgba(0, 0, 255, 0.5)",
      borderRadius: "5px",
      padding: "5px 10px",
      minWidth: "40px",
      color: "white",
      // max-width:60px;
      // border:'1px solid #ccc',
    },
    "&.badgeBlue": {
      backgroundColor: "$blue",
      borderRadius: "5px",
      padding: "5px 10px",
      marginBottom: "5px",
    },
    "&.shadowIcon": {
      backgroundColor: "white",
      boxShadow: "0px 5px 15px -5px rgba(0, 0, 0, 0.25)",
      borderRadius: "5px",
      padding: "5px 10px",
      // minWidth:'20px',
      color: "$blue1",
      // max-width:60px;
      // border:'1px solid #ccc',
    },
    "&.modal-header": {
      display: "block",
      backgroundColor: "$blue",
      padding: "20px",
      "& .text-light": {
        color: "$blue1 !important",
        fontWeight: 600,
      },
      "& .btn-close": {
        position: "absolute",
        top: "15px",
        right: "15px",
        backgroundColor: "transparent",
        borderRadius: "30px",
        borderColor: "white",
        borderWidth: 1,
        borderStyle: "solid",
      },
      "&.cartBtn": {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      },
    },
    // '&.graphYellow': {
    //   borderBottom:'1px dashed $gray9',
    //   transition: "all 0.5s",
    //   'svg': {
    //     fill: "$yellow10 !important"
    //   }
    // },
    '&.svgblue': {
      // borderBottom:'1px dashed $gray9',
      transition: "all 0.5s",
      'svg': {
        fill: "$blue1 !important"
      }
    },
    variants: {
      display: {
        block: {
          display: "block",
        },
        flex: {
          display: "flex",
          flexWrap: "wrap",
          width: "100%",
          "&>*": {
            flex: "1 0 0%",
            width: "100%",
            maxWidth: "100%",
          },
        },
        table: {
          display: "table",
        },
        inlineBlock: {
          display: "inline-block",
        },
      },
      align: {
        right: {
          justifyContent: "right",
        },
        center: {
          justifyContent: "center",
        },
        left: {
          justifyContent: "flex-start",
        },
      },
      alignSelt: {
        middle: {
          alignSelf: "center",
        },
        bottom: {
          alignSelf: "end",
        },
      },
      position: {
        middle: {
          alignItems: "center",
        },
        bottom: {
          alignItems: "end",
        },
      },
      height: {
        full: {
          height: "100vh",
        },
      },
      background: {
        default: {
          backgroundColor: "$blue",
          color: "$whiteA12",
        },
        gray: {
          backgroundColor: "$gray2",
        },
        orangeLight:{
          backgroundColor:"$orangeLight",
        },
        white:{
          backgroundColor: "$white",
          color: "$black",
        }
      },
      img: {
        //@ts-ignore
        width: "200px",
      },
    },
    defaultVariants: {},
  },
  cssFocusVisible
);
