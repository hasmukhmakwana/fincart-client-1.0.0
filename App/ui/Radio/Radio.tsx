import React from "react";
import Box from "../Box/Box";
import Label from "../Label/Label";
import {  RadioVariantsProps, RadioGroupRadio, RadioGroupIndicator } from "./Radio.styles";

interface Props {
  checked?: boolean;
  value?: string | number;
  disabled?: boolean;
  preventDefault?: boolean;
  // onChange?: (e:any) => void;
  size?: string;
  color?: string;
  textColor?: any;
}

type LabelProps = {
  label: string;
};

type NativeAttrs = Omit<React.InputHTMLAttributes<unknown>, keyof Props>;
type RadioProps = NativeAttrs & RadioVariantsProps & LabelProps & Props;

const Radio = ({ label, ...rest }: RadioProps) => {
  return (
      <Box>
        {/* @ts-ignore */}
        <RadioGroupRadio value={rest.value} id={rest.id}>
          <RadioGroupIndicator />
        </RadioGroupRadio>
        {label ? (
        <Label label={label} htmlFor={rest.id} required={rest.required} />
      ) : null}
      </Box>      
  );
};

export default Radio;
