import {
  Dialog,
  DialogContent,
  DialogClose,
  IconButton,
} from "./ModalDialog.styles";
import Cross from "App/icons/Cross";

interface DialogProps {
  children: React.ReactNode;
  open: boolean;
  setOpen: (value: boolean) => void;
  css?: any;
  hideCloseBtn?: boolean;
}

const DialogModal = ({ children, ...props }: DialogProps) => (
  <Dialog open={props.open} onOpenChange={props.setOpen}>
    <DialogContent css={{ ...props.css }}>
      {children}
      {!props?.hideCloseBtn && (
        <DialogClose asChild>
          <IconButton aria-label="Close">
            <Cross color="white"/>
          </IconButton>
        </DialogClose>
      )}
    </DialogContent>
  </Dialog>
);

export default DialogModal;
