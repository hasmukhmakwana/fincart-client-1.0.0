import { PopoverComp } from "./Popover";
import {Popover, PopoverClose, PopoverTrigger} from "./Popover.styles"

//@ts-ignore
Popover.Trigger = PopoverTrigger;
//@ts-ignore
Popover.Content = PopoverComp;
//@ts-ignore
Popover.Close = PopoverClose;

export default Popover