import Group from "@ui/Group/Group";
import React from "react";
import { SingleItemType } from "./Dropdown";
import { DropdownMenuItem, RightSlot } from "./Dropdown.styles";

type SingleItemProps = {
  item: SingleItemType;
  onSelect: (item: SingleItemType, index: number) => void;
  index: number;
  active: boolean;
};

function SingleItem({ active, item, onSelect, index }: SingleItemProps) {
  const css = { backgroundColor: "$primary", color: "white" };
  return (
    <DropdownMenuItem
      css={active ? css : {}}
      key={item.name}
      onClick={() => onSelect(item, index)}
    >
      {item.name} {item.icon ? <RightSlot>{item.icon}</RightSlot> : null}
    </DropdownMenuItem>
  );
}

export default SingleItem;
