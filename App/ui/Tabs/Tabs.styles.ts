//@ts-nocheck
import { mauve, violet } from "@radix-ui/colors";
import * as TabsPrimitive from "@radix-ui/react-tabs";
import { styled } from "../../theme/stitches.config";

const Tabs = styled(TabsPrimitive.Root, {
  display: "flex",
  flexDirection: "column",
  // width: 300,
  // boxShadow: `0 2px 10px ${blackA.blackA4}`,
  "&.horizontal": {
    flexDirection: "row",
    '@bp0': {
      flexDirection: "column", // Styles for bp1
    },
    '@bp3': {
      flexDirection: "row", // Styles for bp3
    },
  },
});

const StyledList = styled(TabsPrimitive.List, {
  flexShrink: 0,
  display: "flex",
  // borderBottom: `1px solid ${mauve.mauve6}`,
  "&.horizontal": {
    // flexDirection: "column",
    '@bp0': {
      flexDirection: "row", // Styles for bp1
      overflowX: 'auto',
    },
    '@bp3': {
      flexDirection: "column", // Styles for bp3
      overflowX: 'visible',
    },
    padding: '0px 0px 10px 0px', // remove
    position: 'sticky',
    zIndex: 1,
    top: 59,
    backgroundColor: "$backgroundColor",
    button: {
      "&.activebtn": {
        transition: "all 0.5s",
        backgroundColor: "$blueActiveTab",
        color: "white",
        borderRadius: "10px",
        padding: "7px 10px",
        border: "0",
        marginBottom: "15px",
        fontSize: 13,
        fontWeight: 'bolder'
      },
      '&.activebtn:hover': {
        transition: "all 0.5s",
      },
      '&:hover': {
        opacity: 0.85,
      },
      "&.btn": {
        transition: "all 0.5s",
        backgroundColor: "$blueTab",
        color: "$gray",
        borderRadius: "10px",
        padding: "7px 10px",
        border: "0",
        boxShadow: "none",
        '@bp0': {
          margin: "0px 5px",
        },
        '@bp3': {
          marginBottom: "15px",
          width: 230,
        },
        fontSize: 13,
        fontWeight: 'bolder'
      },
      '&.btn:hover': {
        transition: "all 0.5s",
      },
    },
  },
  "&.tabsStyle": {
    borderBottom: "1px solid #000"
  },
  "&.tabsCenterStyle": {
    justifyContent: 'center'
  },
  "&.tabsCardStyle": {
    overflowX: 'auto',
    marginBottom: 0,
  }
});

const StyledTrigger = styled(TabsPrimitive.Trigger, {
  all: "unset",
  fontFamily: "inherit",
  backgroundColor: "white",
  padding: "0 20px",
  height: 45,
  flex: 1,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  fontSize: 15,
  lineHeight: 1,
  color: mauve.mauve11,
  userSelect: "none",
  boxShadow: "none",
  '@bp0': {
    overflowX: "auto",
  },
  "&:first-child": { borderTopLeftRadius: 6 },
  "&:last-child": { borderTopRightRadius: 6 },
  "&:hover": { color: violet.violet11 },
  '&[data-state="active"]': {
    color: "$blue1",
    boxShadow: "inset 0 -1px 0 0 currentColor, 0 1px 0 0 currentColor",
  },
  // "&:focus": { position: "relative", boxShadow: `0 0 0 2px black` },
  "&.tabs": {
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    flex: "none",
    '&[data-state="active"]': {
      backgroundColor: "$blue1",
      color: "$white",
      boxShadow: "none",
      "& p": {
        color: "#fff !important",
      }
    },
  },
  "&.tabsCenter": {
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottom: "1px solid #000",
    flex: "none",
    '&[data-state="active"]': {
      backgroundColor: "$blue1",
      color: "$white",
      boxShadow: "none",
      "& p": {
        color: "#fff !important",
      }
    },
  },
  "&.tabsCard": {
    borderRadius: 6,
    marginLeft: 10,
    flex: "none",
    paddingBottom: 10,
    paddingTop: 10,
    fontSize: "0.825rem",
    '@bp0': {
      flexDirection: "column",
      height: "auto",
      marginLeft: 5,
      //padding: "$10",
      paddingLeft: "$10",
      paddingRight: "$10",
      paddingTop: "$5",
      paddingBottom: "$5"
    },
    '@bp2': {
      flexDirection: "row",
      height: "auto",
      marginLeft: 5,
      //padding: "$10",
      paddingLeft: "$10",
      paddingRight: "$10",
      paddingTop: "$5",
      paddingBottom: "$5"
    },
    "& .iconTick": {
      fill: "$blue1 !important",
      marginRight: 15,
      '@bp0': {
        margin: 'auto',
      },
    },
    '&[data-state="active"]': {
      backgroundColor: "$blue1",
      color: "$white",
      boxShadow: "none",
      "& .iconTick": {
        fill: "#fff !important",
      },
      "& p": {
        color: "#fff !important",
      }
    },
  },
  "&.tabsCards": {
    height: 35,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "$primary",
    borderRadius: 10,
    marginLeft: 10,
    flex: "none",
    paddingBottom: 0,
    paddingTop: 0,
    "& .iconTick": {
      fill: "$blue1 !important",
      marginRight: 15,
    },
    '&[data-state="active"]': {
      backgroundColor: "$blue1",
      color: "$white",
      boxShadow: "none",
      "& .iconTick": {
        fill: "#fff !important",
      },
      "& p": {
        color: "#fff !important",
      }
    },
  },
});

const StyledContent = styled(TabsPrimitive.Content, {
  flexGrow: 1,
  // padding: '0px',
  '@bp3': {
    marginLeft: '15px',
  },
  backgroundColor: "transparent",
  borderBottomLeftRadius: 6,
  borderBottomRightRadius: 6,
  outline: "none",
  // "&:focus": { boxShadow: `0 0 0 2px black` },
});


const StyledContentNoValue = styled({
  // @ts-ignore
  flexGrow: 1,
  // padding: '0px',
  '@bp3': {
    marginLeft: '15px',
  },
  backgroundColor: "transparent",
  borderBottomLeftRadius: 6,
  borderBottomRightRadius: 6,
  outline: "none",
  // "&:focus": { boxShadow: `0 0 0 2px black` },
});

// Exports
export const StyledTabs = Tabs;
export const TabsList = StyledList;
export const TabsTrigger = StyledTrigger;
export const TabsContent = StyledContent;
export const TabsContentNoValue = StyledContentNoValue;
