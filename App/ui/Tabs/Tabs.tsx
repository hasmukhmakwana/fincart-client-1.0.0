import React from 'react'
import { StyledTabs, TabsContent, TabsList, TabsTrigger } from './Tabs.styles'

const Tabs = () => {
  return (
    <StyledTabs defaultValue="tab1">
    <TabsList aria-label="Manage your account">
      <TabsTrigger value="tab1">Account</TabsTrigger>
      <TabsTrigger value="tab2">Password</TabsTrigger>
    </TabsList>
    <TabsContent value="tab1">
      tab1
    </TabsContent>
    <TabsContent value="tab2">
      tab2
    </TabsContent>
  </StyledTabs>  )
}

export default Tabs


