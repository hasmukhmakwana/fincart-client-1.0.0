import React from "react";
import { ButtonVariantsProps, StyledButton } from "./Button.styles";

type ButtonProps = React.ButtonHTMLAttributes<unknown> &
  ButtonVariantsProps & { loading?: boolean };

export const Button = (props: any) => {
  let { hide, type, loading = false, children, disabled, ...rest } = props;
  return hide ? (
    <></>
  ) : (
    <StyledButton
      {...rest}
      disabled={disabled || loading}
      type={type || "button"}
    >
      {loading ? "LOADING..." : children}
    </StyledButton>
  );
};
