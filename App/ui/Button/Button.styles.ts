import {
  cssFocusVisible,
  styled,
  VariantProps,
} from "../../theme/stitches.config";

export const StyledButton = styled(
  "button",
  {
    // dflex: 'center',
    appearance: "none",
    boxSizing: " border-box",
    fontWeight: "$medium",
    us: "none",
    lineHeight: "$sm",
    ta: "center",
    whiteSpace: "nowrap",
    transition: "$default",
    position: "relative",
    // overflow: 'hidden',
    border: "none",
    cursor: "pointer",
    pe: "auto",
    p: 0,
    "&.actionBtn": {
      transition: "all 0.5s",
      maxWidth: "36px",
      height: "36px",
      overflow: "hidden",
      svg: {
        fill: "white",
      },
    },
    "&.takeactionBtn": {
      transition: "all 0.5s",
      height: "30px",
      overflow: "hidden",
      borderRadius: '3px',
      fontSize: '12px',
      fontWeight: '900',
      color: 'black',
      svg: {
        fill: "white",
      },
    },
    "&.actionBtn:hover": {
      transition: "all 0.5s",
      maxWidth: "200px",
    },
    "&:hover": {
      opacity: 0.85,
      ".textIndent": {
        // transition: "all 0.5s",
        // width: "auto"
      },
    },
    "@motion": {
      transition: "none",
    },
    ".textIndent": {
      paddingLeft: 10,
      // transition: "all 0.5s",
      // display: "inline-block",
      // width: "0"
    },
    variants: {
      full: {
        true: {
          width: "100%",
        },
      },
      bordered: {
        true: {
          bg: "transparent",
          borderStyle: "solid",
          color: "$text",
          borderWidth: "2px",
        },
      },
      ghost: {
        true: {
          bg: "transparent",
          borderStyle: "solid",
          color: "black",

          "&:hover": {
            color: "black",
            borderColor: "$indigo9",
          },
        },
      },
      color: {
        default: {
          backgroundColor: "$indigo9",
          color: "$whiteA12",
        },
        lightBlue: {
          backgroundColor: "$lightBlue",
          color: "#212529",
          borderWidth: 1,
          borderStyle: "solid",
          borderColor: "$lightBlue",
          "&:hover": {
            color: "#ffffff",
            background: "transparent",
            borderColor: "#fff",
            ">p": {
              color: "#fff",
            }
          }
        },
        yellow: {
          backgroundColor: "$yellow",
          color: "$yellow10",
          svg: {
            fill: "$yellow10",
          },
        },
        orangeLight: {
          backgroundColor: "$orangeLight",
          color: "$gray10",
          svg: {
            fill: "$gray11 !important",
          },
        },
        orangelightGroup: {
          backgroundColor: "$orangeLight",
          color: "$gray10",
          paddingRight:'10px',
          svg: {
            fill: "$gray11 !important",
           marginLeft:'5px',
           marginTop:'3px'
          },
        },

        yellowGroup: {
          backgroundColor: "$yellowButton",
          color: "$gray10",
          paddingRight:'15px',
          svg: {
            fill: "$gray !important",
           marginLeft:'2px',
           marginTop:'0px'
          },
        },

        // yellowGroup: {
        //   backgroundColor: "$yellowButton",
        //   color: "$gray10",
        //   paddingRight:'10px',

        //   svg: {
        //     fill: "$gray !important",
        //   margin:'15px'
        //   },
        // },


        brown: {
          backgroundColor: "$yellow10",
          color: "$yellow",
          svg: {
            fill: "$yellow",
          },
        },
        white: {
          backgroundColor: "transparent",
          color: "$gray1",
          svg: {
            fill: "$gray10",
          },
        },

        // added shadow button for login page

        shadowbtn: {
          backgroundColor: "$indigo9",
          color: "$whiteA12",
          boxShadow: "0px 20px 20px -11px rgba(0, 0, 255, 0.5)",
        },

        // end
        error: {
          backgroundColor: "$ebgSolid1",
          color: "$whiteA12",
        },
        gradient: {
          backgroundImage: "$gradient",
          color: "$whiteA12",
        },
        clear: {
          bg: "transparent",
        },
      },
      size: {
        ss: {
          $$buttonPadding: "$space$5",
          px: "$5",
          height: "$12",
          lh: "$space$11",
          width: "auto",
          minWidth: "$36",
          fontSize: "$xs",
          br: "$sm",
        },
        xs: {
          $$buttonPadding: "$space$3",
          px: "$3",
          height: "$10",
          lh: "$space$10",
          width: "auto",
          minWidth: "$20",
          fontSize: "$tiny",
          br: "$xs",
        },
        sm: {
          $$buttonPadding: "$space$5",
          px: "$5",
          height: "$12",
          lh: "$space$14",
          width: "auto",
          minWidth: "$36",
          fontSize: "$xs",
          br: "$sm",
        },
        md: {
          $$buttonPadding: "$space$7",
          px: "$6",
          height: "$14",
          lh: "$space$14",
          width: "auto",
          minWidth: "$48",
          fontSize: "$xs",
          br: "$md",
        },
        lg: {
          $$buttonPadding: "$space$9",
          px: "$9",
          height: "$15",
          lh: "$space$15",
          width: "auto",
          minWidth: "$60",
          fontSize: "$base",
          br: "$base",
        },
        xl: {
          $$buttonPadding: "$space$10",
          px: "$10",
          height: "$17",
          lh: "$space$17",
          width: "auto",
          minWidth: "$72",
          fontSize: "$sm",
          br: "$xl",
        },
        fullwidth: {
          $$buttonPadding: "$space$7",
          px: "$6",
          height: "$14",
          lh: "$space$14",
          width: "100%",
          minWidth: "$48",
          fontSize: "$xs",
          br: "$md",
        },
      },
      borderWeight: {
        light: {
          bw: "$light",
        },
        normal: {
          bw: "$normal",
        },
        bold: {
          bw: "$bold",
        },
        extrabold: {
          bw: "$extrabold",
        },
        black: {
          bw: "$black",
        },
      },
      flat: {
        true: {
          color: "$text",
        },
      },
      light: {
        true: {
          bg: "transparent",
        },
      },
      shadow: {
        true: {
          bs: "$sm",
        },
      },
      disabled: {
        true: {
          pe: "none",
          opacity: 0.7,
          // "&:hover": {
          //   cursor: "wait"
          // },
        },
      },
      clickable: {
        false: {
          cursor: "default",
          pe: "none",
        },
      },
      animated: {
        true: {
          "&:active": {
            transform: "scale(0.94)",
          },
        },
        false: {
          transition: "none",
        },
      },
      auto: {
        true: {
          width: "auto",
          minWidth: "min-content",
        },
      },
      rounded: {
        true: {
          br: "$pill",
        },
      },
    },
    compoundVariants: [
      // size / auto
      {
        auto: true,
        size: "xs",
        css: {
          px: "$5",
          minWidth: "min-content",
        },
      },
      {
        auto: true,
        size: "sm",
        css: {
          px: "$8",
          minWidth: "min-content",
        },
      },
      {
        auto: true,
        size: "md",
        css: {
          px: "$9",
          minWidth: "min-content",
        },
      },
      {
        auto: true,
        size: "lg",
        css: {
          px: "$10",
          minWidth: "min-content",
        },
      },
      {
        auto: true,
        size: "xl",
        css: {
          px: "$11",
          minWidth: "min-content",
        },
      },
      // animated / disabled
      {
        animated: true,
        disabled: true,
        css: {
          "&:active": {
            transform: "none",
          },
        },
      },
      // shadow / color
      {
        shadow: true,
        color: "default",
        css: {
          normalShadow: "$primaryShadow",
        },
      },
      {
        shadow: true,
        color: "primary",
        css: {
          normalShadow: "$primaryShadow",
        },
      },
      {
        shadow: true,
        color: "secondary",
        css: {
          normalShadow: "$secondaryShadow",
        },
      },
      {
        shadow: true,
        color: "warning",
        css: {
          normalShadow: "$warningShadow",
        },
      },
      {
        shadow: true,
        color: "success",
        css: {
          normalShadow: "$successShadow",
        },
      },
      {
        shadow: true,
        color: "error",
        css: {
          normalShadow: "$errorShadow",
        },
      },
      {
        shadow: true,
        color: "gradient",
        css: {
          normalShadow: "$primaryShadow",
        },
      },
      // light / color
      {
        light: true,
        color: "default",
        css: {
          bg: "transparent",
          color: "$text",
        },
      },
      {
        light: true,
        color: "primary",
        css: {
          bg: "transparent",
          color: "$primary",
        },
      },
      {
        light: true,
        color: "secondary",
        css: {
          bg: "transparent",
          color: "$secondary",
        },
      },
      {
        light: true,
        color: "warning",
        css: {
          bg: "transparent",
          color: "$warning",
        },
      },
      {
        light: true,
        color: "success",
        css: {
          bg: "transparent",
          color: "$success",
        },
      },
      {
        light: true,
        color: "error",
        css: {
          bg: "transparent",
          color: "$error",
        },
      },
      // bordered / color
      {
        bordered: true,
        color: "default",
        css: {
          bg: "transparent",
          borderColor: "$border",
          color: "$pText2",
        },
      },
      {
        bordered: true,
        color: "primary",
        css: {
          bg: "transparent",
          borderColor: "$primary",
          color: "$primary",
        },
      },
      {
        bordered: true,
        color: "secondary",
        css: {
          bg: "transparent",
          borderColor: "$secondary",
          color: "$secondary",
        },
      },
      {
        bordered: true,
        color: "success",
        css: {
          bg: "transparent",
          borderColor: "$success",
          color: "$success",
        },
      },
      {
        bordered: true,
        color: "warning",
        css: {
          bg: "transparent",
          borderColor: "$warning",
          color: "$warning",
        },
      },
      {
        bordered: true,
        color: "error",
        css: {
          bg: "transparent",
          borderColor: "$eborder",
          color: "$epText1",
        },
      },
      {
        bordered: true,
        color: "gradient",
        css: {
          bg: "transparent",
          color: "$text",
          padding: "$1",
          bgClip: "content-box, border-box",
          borderColor: "$primary",
          backgroundImage:
            "linear-gradient($background, $background), $gradient",
          border: "none",
        },
      },
      // ghost / color
      {
        ghost: true,
        color: "default",
        css: {
          "&:hover": {
            bg: "$indigo9",
          },
        },
      },
      {
        ghost: true,
        color: "primary",
        css: {
          "&:hover": {
            bg: "$primary",
          },
        },
      },
      {
        ghost: true,
        color: "secondary",
        css: {
          "&:hover": {
            bg: "$secondary",
          },
        },
      },
      {
        ghost: true,
        color: "success",
        css: {
          "&:hover": {
            bg: "$success",
          },
        },
      },
      {
        ghost: true,
        color: "warning",
        css: {
          "&:hover": {
            bg: "$warning",
          },
        },
      },
      {
        ghost: true,
        color: "error",
        css: {
          "&:hover": {
            bg: "$error",
          },
        },
      },
      {
        ghost: true,
        color: "gradient",
        css: {
          "&:hover": {
            bg: "$gradient",
          },
        },
      },
      // flat / color
      {
        flat: true,
        color: "default",
        css: {
          bg: "$primaryLight",
          color: "$primary",
        },
      },
      {
        flat: true,
        color: "primary",
        css: {
          bg: "$primaryLight",
          color: "$primary",
        },
      },
      {
        flat: true,
        color: "secondary",
        css: {
          bg: "$secondaryLight",
          color: "$secondary",
        },
      },
      {
        flat: true,
        color: "success",
        css: {
          bg: "$successLight",
          color: "$success",
        },
      },
      {
        flat: true,
        color: "warning",
        css: {
          bg: "$warningLight",
          color: "$warning",
        },
      },
      {
        flat: true,
        color: "error",
        css: {
          bg: "$errorLight",
          color: "$error",
        },
      },
      // border-weight / gradient-color / bordered
      {
        bordered: true,
        color: "gradient",
        borderWeight: "light",
        css: {
          padding: "$borderWeights$light",
        },
      },
      {
        bordered: true,
        color: "gradient",
        borderWeight: "normal",
        css: {
          padding: "$borderWeights$normal",
        },
      },
      {
        bordered: true,
        color: "gradient",
        borderWeight: "bold",
        css: {
          padding: "$borderWeights$bold",
        },
      },
      {
        bordered: true,
        color: "gradient",
        borderWeight: "extrabold",
        css: {
          padding: "$borderWeights$extrabold",
        },
      },
      {
        bordered: true,
        color: "gradient",
        borderWeight: "black",
        css: {
          padding: "$borderWeights$black",
        },
      },
      // size / auto / gradient-color / bordered
      {
        auto: true,
        color: "gradient",
        bordered: true,
        size: "xs",
        css: {
          px: "$1",
          py: "$1",
          ".nextui-button-text": {
            px: "$5",
          },
        },
      },
      {
        auto: true,
        color: "gradient",
        bordered: true,
        size: "sm",
        css: {
          px: "$1",
          py: "$1",
          ".nextui-button-text": {
            px: "$8",
          },
        },
      },
      {
        auto: true,
        color: "gradient",
        bordered: true,
        size: "md",
        css: {
          px: "$1",
          py: "$1",
          ".nextui-button-text": {
            px: "$9",
          },
        },
      },
      {
        auto: true,
        color: "gradient",
        bordered: true,
        size: "lg",
        css: {
          px: "$1",
          py: "$1",
          ".nextui-button-text": {
            px: "$14",
          },
        },
      },
      {
        auto: true,
        color: "gradient",
        bordered: true,
        size: "xl",
        css: {
          px: "$1",
          py: "$1",
          ".nextui-button-text": {
            px: "$12",
          },
        },
      },
      // rounded && size
      {
        rounded: true,
        size: "xs",
        css: {
          br: "$pill",
        },
      },
      {
        rounded: true,
        size: "sm",
        css: {
          br: "$pill",
        },
      },
      {
        rounded: true,
        size: "md",
        css: {
          br: "$pill",
        },
      },
      {
        rounded: true,
        size: "lg",
        css: {
          br: "$pill",
        },
      },
      {
        rounded: true,
        size: "xl",
        css: {
          br: "$pill",
        },
      },
    ],
    defaultVariants: {
      color: "default",
      borderWeight: "normal",
      animated: true,
      size: "md",
    },
    "& + button": {
      marginLeft: "10px",
    },
  },

  cssFocusVisible
);

export type ButtonVariantsProps = VariantProps<typeof StyledButton>;
