import {
  Dialog,
  DialogContent,
  DialogClose,
  IconButton,
} from "./ModalDialog.styles";
import Cross from "App/icons/Cross";
import { Button } from "@ui/Button/Button";

interface DialogProps {
  children: React.ReactNode;
  open: boolean;
  setOpen: (value: boolean) => void;
  css?: any;
  hideCloseBtn?: boolean;
}

const DialogModal = ({ children, ...props }: DialogProps) => (
  <Dialog open={props.open} onOpenChange={props.setOpen}>
    <DialogContent css={{ ...props.css }}>
      {children}
      {!props?.hideCloseBtn && (
        <DialogClose asChild>
          <IconButton aria-label="Close">
            <Cross color="white"/>
          </IconButton>
        </DialogClose>
      )}
    </DialogContent>
    {/* <DialogClose asChild>
      <Button aria-label="Cancel"></Button></DialogClose> */}
  </Dialog>
);

export default DialogModal;
