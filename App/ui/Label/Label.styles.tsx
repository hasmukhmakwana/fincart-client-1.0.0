import * as LabelPrimitive from "@radix-ui/react-label";
import { styled, VariantProps } from "../../theme/stitches.config";

export const StyledLabel = styled(LabelPrimitive.Root, {
  userSelect: "none",
  lineHeight: 1,
  fontSize: '$xs',
  color:'$mediumBlue', //$gray8 - AR
  letterSpacing:"$widest",
  fontWeight:'$semibold',
  // paddingLeft: 15,
  // marginBottom: 5,
  
  display: "inline-block",
  variants: {
    showCursor: {
      true: {
        cursor: "pointer",
      },
    },
  },
});

export const RequiredSpan = styled("span", {
  color: "red",

});

export type LabelVariantProps = VariantProps<typeof StyledLabel>;
