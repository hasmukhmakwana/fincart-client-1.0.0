//@ts-nocheck
import { cssFocusVisible, styled, VariantProps } from "../../theme/stitches.config";

export const StyledText = styled(
  'p', {
  my: 0,
  variants: {
    size: {
      h1: {
        fontSize: '$xl',
        lineHeight: '$xl',
      },
      h2: {
        fontSize: '$lg',
        lineHeight: '$lg',
      },
      h3: {
        fontSize: '$md',
        lineHeight: '$md',
      },
      h9: {
        fontSize: '$lg',
        lineHeight: '$xs',
      },
      h4: {
        fontSize: '$sm',
        lineHeight: '$sm',
      },
      h5: {
        fontSize: '$base',
        lineHeight: '$md',
      },
      h6: {
        fontSize: '$xs',
        lineHeight: '$md',
      },
      h7: {
        fontSize: '$miny',
        lineHeight: '$md',
      },
      h8: {
        fontSize: '$tiny',
        lineHeight: '$md',
      },
      h10: {
        fontSize: '1.3rem',
        lineHeight: '$lg',
      },
      bt: {
        fontSize: '$bt',
        lineHeight: '$md',
      },
      bt1: {
        fontSize: '0.8rem',
        lineHeight: '$md',
      },
      bt2: {
        fontSize: '0.875rem',
        lineHeight: '$md'
      }
    },
    color: {
      default: {
        color: "$grayDark"
      },
      primary: {
        color: "$primary"
      },
      white: {
        color: "$ebg1"
      },
      blue: {
        color: "$blue1"
      },
      gray: {
        color: "$gray8"
      },
      blue1: {
        color: "$blue1"
      },
      blue2: {
        color: "$blue2"
      },
      grayDark: {
        color: "$gray8"
      },
      graylight: {
        color: "$gray4"
      },
      blueActiveTab: {
        color: "$blueActiveTab"
      },
      blueTab: {
        color: "$blueTab"
      },
      green: {
        color: "$green"
      },
      mediumBlue: {
        color: "$mediumBlue"
      },
      lightGreen: {
        color: '$lightGreen'
      },
      lightBlue: {
        color: '$lightBlue'
      },
      goalBlue: {
        color: '$goalBlue'
      },
      yellow: {
        color: '$yellowButton'
      },
      black: {
        color: '$black'
      }

    },
    weight: {
      hairline: {
        fontWeight: '$hairline'
      },
      thin: {
        fontWeight: '$thin'
      },
      light: {
        fontWeight: '$light'
      },
      normal: {
        fontWeight: '$normal'
      },
      medium: {
        fontWeight: '$medium'
      },
      semibold: {
        fontWeight: '$semibold'
      },
      bold: {
        fontWeight: '$bold'
      },
      extrabold: {
        fontWeight: '$extrabold'
      },
    },
    style: {
      normal: {
        fontStyle: 'normal'
      },
      italic: {
        fontStyle: 'italic'
      },
    },
    transform: {
      uppercase: {
        textTransform: 'uppercase'
      },
      lowercase: {
        textTransform: 'lowercase'
      },
      capitalize: {
        textTransform: 'capitalize'
      },
    },
    decoration: {
      overline: {
        textDecoration: 'overline'
      },
      linethrough: {
        textDecoration: 'line-through'
      },
      underline: {
        textDecoration: 'underline'
      },
    },
    shadow: {
      true: {
        ts: '$sm'
      }
    },
    link: {
      true: {
        cursor: 'pointer',
        color: 'red',
        "&:hover": {
          color: 'orange'
        }
      }
    },
    border: {
      default: {
        borderBottom: '1px solid $gray5',
        padding: '20px'
      },
      default2: {
        borderBottom: '1px solid $yellowBorder',
      },
    },
    space: {
      default: {
        letterSpacing: "$normal",
      },
      letterspace: {
        letterSpacing: "$widest",
      }
    }
  },
  defaultVariants: {
    color: 'default',
    weight: 'normal',
    style: 'normal',
    size: 'h5',
  }
},
  cssFocusVisible
);

export type TextVariantsProps = VariantProps<typeof StyledText>;
