import { blackA, violet } from "@radix-ui/colors";
import Text from "App/ui/Text/Text";
import { styled } from "App/theme/stitches.config";
import React from "react";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "./Dropdown.styles";

type propTypes = {
  arr: { name: string; id: string | number; icon?: any }[];
  trigger: React.ReactNode;
  onSelect: (id: string | number, index: number) => void;
  triggerProps?: any
};

function DropdownStyled({ arr, onSelect, trigger, triggerProps }: propTypes) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger {...triggerProps}>
        {trigger}
      </DropdownMenuTrigger>
      <DropdownMenuContent sideOffset={5}>
        {arr.map((item, index) => {
          return (
            <DropdownMenuItem
              key={item.name}
              onClick={() => onSelect(item.id, index)}
            >
              {item.icon} {item.name}
            </DropdownMenuItem>
          );
        })}
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export default DropdownStyled;
