import { styled, VariantProps } from "App/theme/stitches.config";

export const GroupBox = styled("div", {
  display: "flex",
  gap: 100,
  variants: {
    position: {
      left: {
        justifyContent: "flex-start",
      },
      right: {
        justifyContent: "flex-end",
      },
      center: {
        justifyContent: "center",
      },
      apart: {
        justifyContent: "space-between",
      },
    },
    align:{
      center:{
        alignItems:'center'
      },
      start:{
        alignItems:'start'
      },
      end:{
        alignItems: 'end'
      }
    },
    space: {
      xs: {
        gap: "$2",
      },
      sm: {
        gap: "$4",
      },
      md: {
        gap: "$6",
      },
      lg: {
        gap: "$8",
      },
      xl: {
        gap: "$10",
      },
      xxl: {
        gap: "$12",
      },
      xxxl: {
        gap: "$15",
      },
      none:{
        gap:'$0'
      }
    },
    verticle: {
      true: {
        flexDirection: "row",
      },
    },

    horizontal: {
      true: {
        flexDirection: "column",
      },
    },
    bg:{
      true:{
          backgroundColor: '$blue',
          padding:'10px 5px',
          marginTop:'15px',
          marginBottom:'15px'
      }
    },
    borderBottom:{
     true:{
      borderBottomWidth: 1,
      borderBottomStyle: "solid",
      borderBottomColor: "$gray5",
      // padding:'20px'
     }
    },
    borderyellowBottom:{
      true:{
       borderBottomWidth: 1,
       borderBottomStyle: "solid",
       borderBottomColor: "$orangeLight",
       // padding:'20px'
      }
     },
    // border:{
    //   default: {
    //     borderBottom: '1px solid $gray5',
    //     padding:'20px'
    //   },
    // },
    noWrap: {
      true: {
        flexWrap: "nowrap",
      },
      false: {
        flexWrap: "wrap",
      },
    },
  },
  "&.rightBtn": {
    button: {
      marginTop: "10px",
    },
    space: {
      lg: {
        gap: "$8",
      },
    },
    alignItems: "center",
    padding: "0 20px",
  },
  '&.graphYellow': {
    borderBottom:'2px dashed $gray7',
    paddingBottom:'10px',
    paddingTop:'10px',
    transition: "all 0.5s",
    'svg': {
      fill: "$yellow10 !important"
    }
  },
  '&.goalBox': {
    borderBottom:'1px dashed $gray7',
    paddingBottom:'10px',
    paddingTop:'10px'
  },



  defaultVariants: {
    position: "left",
    verticle: true,
    space: "md",

  },
});
export type GroupVariantsProps = VariantProps<typeof GroupBox>;
