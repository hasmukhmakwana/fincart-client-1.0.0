import { cssFocusVisible, styled, VariantProps } from "../../theme/stitches.config";

export const StyledCheckbox = styled(
  'input', {
    // my: 0,
    position: 'relative',
    borderWidth: 1,
    borderStyle: 'solid',
    // borderColor: '$borderLight',
    borderColor: '$gray',
    verticalAlign: 'middle',
    "&:focus-visible" : {
      outline: "none",
    },
    "&:before" : {
        content: '',
        position: 'absolute',
        display: 'block',
        borderRadius: 2,
        borderWidth: 1,
        borderStyle: 'solid',
        height: '100%',
          width: '100%',
          // borderColor: '$indigo9',
          borderColor: '$gray',
          backgroundColor: 'white'
    },
    "&:checked:before" : {
          borderColor: '$blue1',
          backgroundColor: '$blue1'
    },
    "&:checked:after" : {
      content: '',
      position: 'absolute',
      display: 'block',
      borderRadius: '2',
      borderWidth: 2,
      borderStyle: 'solid',
      borderTopColor: 'transparent',
      borderRightColor: 'transparent',
      borderLeftColor: 'white',
      borderBottomColor: 'white',
      transform: 'rotate(-45deg)',
      // height: 4,
      // width: 8,
      // marginLeft: 4,
      // marginTop: 3,
      height: '40%',
      width: '70%',
      marginLeft: '15%',
      marginTop: '15%',
      // backgroundColor: '$indigo9',
    },
    variants: {
      size: {
        xs: {
          height: '10',
          width: '10',
        },
        sm: {
          height: 15,
          width: 15,
        },
        md: {
          height: 20,
          width: 20,
        },
        lg: {
          height: 25,
          width: 25,
        },
        xl: {
          height: 30,
          width: 30,
        }
      },
      color: {
        default: {
          backgroundColor: "transparent",
          color: '$indigo4',
          "&:before" : {
                // borderColor: '$indigo9',
                borderColor: '#A5A5A5',
                backgroundColor: 'white'
          },
          "&:checked:after" : {
            // backgroundColor: '$indigo9',
          },
        }
      },
      error: {
        true: {
          borderColor: 'red',
        }
      },
      success: {
        true: {
          borderColor: 'green',
        }
      },
    },
    defaultVariants: {
      color: 'default',
      // weight: 'normal',
      // style: 'normal',
      size: 'sm',
    }
  },
    cssFocusVisible
  );


export type CheckboxVariantsProps = VariantProps<typeof StyledCheckbox>;
