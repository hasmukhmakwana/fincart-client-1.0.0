import Label from "@ui/Label/Label";
import { useId } from "@ui/utils/hooks/useId";
import React from "react";
import {
  SwitchThumb,
  Flex,
  StyledSwitch,
  SwitchVariantProps,
} from "./Switch.styles";

type defaultProps = {
  label: string;
  id?: string;
  labelLeft?: boolean;
  checked: boolean;
  onChange: (val: boolean) => void;
  size?: string;
  disabled?: boolean;
};
type propTypes = defaultProps & SwitchVariantProps;

function Switch({
  label,
  id,
  labelLeft,
  checked = false,
  onChange,
  size,
  disabled,
}: propTypes) {
  let SwitchID = useId(id);
  return (
    <Flex css={{ alignItems: "center" }}>
      {labelLeft ? (
        <Label htmlFor={SwitchID} css={{ paddingLeft: 15 }} label={label} />
      ) : null}
      <StyledSwitch
        size={size}
        defaultChecked
        id={SwitchID}
        checked={checked}
        onCheckedChange={onChange}
        disabled={disabled}
      >
        <SwitchThumb size={size} />
      </StyledSwitch>
      {!labelLeft ? (
        <Label htmlFor={SwitchID} css={{ paddingLeft: 10 }} label={label} />
      ) : null}
    </Flex>
  );
}

export default Switch;
