import Box from "@ui/Box/Box";
import React, { useEffect } from "react";
import Data from "./Data";
import { GridContainer, Table } from "./DataGrid.styles";
import DataHeader from "./DataHeader";
import Filters from "./Filters";
import { gridReducer, initialState } from "./reducer";
import { ACTION_TYPE, gridProps } from "./type";
import { getValidColumns } from "./utils";

function DataGrid({
  header,
  data,
  actionButtons,
  bordered,
  clickOnAction,
  loading,
}: gridProps) {
  const [state, dispatch] = React.useReducer(gridReducer, initialState);

  const handleFilter = (value: any) => {
    dispatch({ type: ACTION_TYPE.FILTER_CHANGE, payload: value });
  };

  useEffect(() => {
    dispatch({ type: ACTION_TYPE.SET_INITIAL_COLUMN, payload: header });
    return () => {};
  }, [header]);

  return (
    <Box className="mt-3" css={{ width: "100%" }}>
      <GridContainer css={{borderRadius: "8px"}}>
        <Table bordered={bordered}>
          <DataHeader header={getValidColumns(state?.columns)} />
          {/* <Filters
            headers={getValidColumns(state?.columns)}
            handleFilter={handleFilter}
          /> */}
          <Data
            headers={getValidColumns(state?.columns)}
            data={data}
            actionButtons={actionButtons}
            onActionClick={clickOnAction}
            loading={loading}
          />
        </Table>
      </GridContainer>
    </Box>
  );
}

export default DataGrid;
