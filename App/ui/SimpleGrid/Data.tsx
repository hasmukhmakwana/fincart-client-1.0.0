import ActionButtons from "./components/ActionButtons";
import { DataParent, DataRow } from "./DataGrid.styles";
import SingleCell from "./components/SingleCell";
import {
  actionButtonType,
  actionTitleTypes,
  headerObject,
  IS_ACTION,
} from "./type";
import Loader from "./components/Loader";
import { DataCell } from "@ui/DataGrid/DataGrid.styles";
import NoData from "./components/NoData";

type propTypes = {
  headers: headerObject[];
  data: any[];
  actionButtons: actionButtonType[];
  onActionClick: (title: actionTitleTypes) => void;
  loading: boolean;
};

function Data({
  data,
  headers,
  actionButtons,
  onActionClick,
  loading,
}: propTypes) {
  //handle loading...
  if (loading) {
    return (
      <DataParent>
        <DataRow>
          <DataCell position="center" colSpan={headers?.length}>
            <Loader />
          </DataCell>
        </DataRow>
      </DataParent>
    );
  }

  //handle no data
  if (!data || !data.length) {
    return (
      <DataParent>
        <DataRow>
          <DataCell position="center" colSpan={headers?.length}>
            <NoData />
          </DataCell>
        </DataRow>
      </DataParent>
    );
  }

  return (
    <DataParent>
      {data.map((d, data_index) => {
        return (
          <DataRow>
            {headers.map((h) => {
              if (h.field == IS_ACTION) {
                return (
                  <ActionButtons
                    position={h.align}
                    actionButtons={actionButtons}
                    onActionClick={onActionClick}
                    data_index={data_index}
                    data={d}
                  />
                );
              }
              return (
                <SingleCell
                  cell={d[h.field]}
                  position={h.align}
                  formatter={h.formatter}
                  header={h}
                />
              );
            })}
          </DataRow>
        );
      })}
    </DataParent>
  );
}

export default Data;
