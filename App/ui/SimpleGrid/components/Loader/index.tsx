export default () => {
  return (
    <button className="btn border-dark" disabled>
      <span
        className="spinner-border spinner-border-sm"
        role="status"
        aria-hidden="true"
      ></span>
      &nbsp; Loading...
    </button>
  );
};
