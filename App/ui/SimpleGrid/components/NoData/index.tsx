export default () => {
  return (
    <button className="btn border-dark" disabled>
      <span></span>
      &nbsp; No Data Available!
    </button>
  );
};
