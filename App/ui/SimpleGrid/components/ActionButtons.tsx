import Box from "@ui/Box/Box";
import Group from "@ui/Group/Group";
import React from "react";
import { ActionButtonWrapper, SingleAction } from "../DataGrid.styles";
import { actionButtonType, actionTitleTypes, POSITION_TYPE } from "../type";
type propTypes = {
  actionButtons: actionButtonType[];
  onActionClick: (title: actionTitleTypes, data_index: number) => void;
  position?: POSITION_TYPE;
  data_index: number;
  data: any;
};
function ActionButtons({
  actionButtons,
  onActionClick,
  position = "center",
  data_index,
  data,
}: propTypes) {
  return (
    <ActionButtonWrapper>
      <Group position={position}>
        {actionButtons.map((items) => {
          if (items.title == "Delete" && data.Active == "Y") {
            return (<></>)
          }
          else
          return (
            <SingleAction
              key={items.title}
              onClick={() => onActionClick(items.title, data_index)}
              title={items?.tooltip || ""}
            >
              {items.icon}
            </SingleAction>
          );
        })}
      </Group>
    </ActionButtonWrapper>
  );
}

export default React.memo(ActionButtons);
